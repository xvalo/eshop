<?php

define('WWW_DIR', __DIR__);
define('APP_DIR', __DIR__.'/../app/');

$setMaintenance = false;
$allowedIps = ['127.0.01'];

if ($setMaintenance && !in_array(get_client_ip(),$allowedIps)) {
	require __DIR__ . '/.maintenance.php';
}

$container = require __DIR__ . '/../app/bootstrap.php';

$container->getByType(Nette\Application\Application::class)
	->run();

function get_client_ip() {
	$ipAddress = '';
	if (isset($_SERVER['HTTP_CLIENT_IP']))
		$ipAddress = $_SERVER['HTTP_CLIENT_IP'];
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ipAddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_X_FORWARDED']))
		$ipAddress = $_SERVER['HTTP_X_FORWARDED'];
	else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		$ipAddress = $_SERVER['HTTP_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_FORWARDED']))
		$ipAddress = $_SERVER['HTTP_FORWARDED'];
	else if(isset($_SERVER['REMOTE_ADDR']))
		$ipAddress = $_SERVER['REMOTE_ADDR'];
	else
		$ipAddress = 'UNKNOWN';
	return $ipAddress;
}
