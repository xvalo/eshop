function initCustomerMenu() {
    $('.link-name').click(function(event) {
        event.preventDefault();

        $(this).toggleClass('opened');

        if($(window).width() < 750) {
            $('.top-main-menu').hide();
        }

        $('.user-menu').slideToggle();
    });
}