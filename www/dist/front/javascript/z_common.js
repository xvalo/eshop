$(document).ready(function() {
    if ($('#cartStepOneForm').length || $('#frm-signInForm-signInForm').length) {
        $.nette.ext('history', false);
        $.nette.ext('redirect', {
            success: function (payload) {
                if (payload.redirectUrl) {
                    console.log(payload.redirectUrl);
                    window.location.href = payload.redirectUrl;
                    return false;
                }
            }
        });
    }

    $.nette.ext('front', {
        success: function (payload, status, jqXHR, settings) {
            if (payload.productAdded == true) {
                $('#added-product-name').text(payload.productNameAddedToBasket);
                $('#added-product-price').text(payload.productPriceAddedToBasket+',- Kč');
                openModal('modal-added-to-cart');
            }

            if (payload.userLoggedIn == true) {
                closeModal();
                initCustomerMenu();
            }

            if (payload.scrollTo && payload.scrollTo.length > 0) {
                $('html,body').animate({scrollTop: $('#'+payload.scrollTo).offset().top},'slow');
            }
        }
    });

    $.nette.ext('ajaxLoaderSpinner', {
        before: function (jqXHR, settings) {
            $('.cssload-container').show();
        },
        complete: function (jqXHR, settings) {
            $('.cssload-container').hide();
        }
    });
    $.nette.init();

	// podpora SVG pro prohlizece
    svg4everybody();

    $('.aside-menu .sub-menu .arrow-open').click(function(event) {
        event.preventDefault();
        $(this).toggleClass('active');
        $(this).parent().toggleClass('opened');
        $(this).parent().next().slideToggle();
    });

    $('.aside-menu .main-menu .sub-menu-title').click(function(event) {
        event.preventDefault();
        $(this).toggleClass('opened');
        $(this).next().slideToggle();
    });



    $('.burger-link').click(function(event) {
        event.preventDefault();

        if($(window).width() < 750) {
            $('.user-menu').hide();
        }

        $('.top-main-menu').slideToggle();
    });

    initCustomerMenu();

    $(window).resize(function() {
        modalPosition($('.modal'));
    });

    if ($('#modal-notice').length) {
       openModal('modal-notice');
    }

    if ($('#modal-cashback-open').length) {
        openModal('modal-cashback');
    }

    if ($('#modal-reg-success').length) {
        openModal('modal-reg-success');
    }

    // registrace - zobrazeni poli pro firmu
    var showFirmPurchase = function(slideEffect, showWhat, basedOn) {
        if(basedOn.is(':checked')) {
            if(slideEffect) {
                showWhat.slideDown();
            } else {
                showWhat.show();
            }
        }
        else {
            if(slideEffect) {
                showWhat.slideUp();
            } else {
                showWhat.hide();
            }
        }
    };

    $('input.check-firm-purchase').change(function() {
        showFirmPurchase(true, $('.firm-purchase'), $('input.check-firm-purchase'));
    });

    if($('.firm-purchase').size()) {
        showFirmPurchase(false, $('.firm-purchase'), $('input.check-firm-purchase'));
    }

    $('input.check-del-address').change(function() {
        showFirmPurchase(true, $('.del-address'), $('input.check-del-address'));
    });

    if($('.del-address').size()) {
        showFirmPurchase(false, $('.del-address'), $('input.check-del-address'));
    }

    $('input.check-loyalty-card').change(function() {
        showFirmPurchase(true, $('.loyalty-card'), $('input.check-loyalty-card'));
    });

    if($('.loyalty-card').size()) {
        showFirmPurchase(false, $('.loyalty-card'), $('input.check-loyalty-card'));
    }


    // detail produktu - fotky produktu ve slideru
    $('.product-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        customPaging: function(slick, i) {
            var thumb = $(slick.$slides[i]).data('thumb');
            return '<a><img src="'+thumb+'"></a>';
        },
        arrows: true,
        fade: false,
        dots: true,
        nextArrow: '<button type="button" class="slick-next"></button>',
        prevArrow: '<button type="button" class="slick-prev"></button>'
    });

    // prepocitani vysky slideru - blblo to pri prvnim nacteni stranky
    var changeSlider = function() {
        var dotsHeight;

        $('.product-slider .slick-dots').show();
        dotsHeight = $('.product-slider .slick-dots').height();
        $('.product-slider .slick-dots').css("position", "absolute");
        $('.product-slider').css("height", $('.product-slider').height());
        $('.product-slider').css("margin-bottom", dotsHeight + 50);
        $('.product-slider .slick-dots').css("bottom", (0 - dotsHeight - 25));
        $('.product-slider .image, .product-slider a, .product-slider .helper').css("display", "inline-block");
    };

    $(window).trigger('resize');

    setTimeout(function(){
        changeSlider();
    }, 100);

    $(window).on('resize', function() {
        changeSlider();
    });

    lightbox.option({
      'alwaysShowNavOnTouchDevices': true
    });

	// nastaveni kvuli fotkam ve slideru, aby byly vycentrovane
	// cistymi CSS to neslo
    $('.product-slider .slider .slider-item').css('height', '100%');

    $('.eu-cookies button').click(function() {
        var date = new Date();
        date.setFullYear(date.getFullYear() + 10);
        document.cookie = 'eu-cookies=1; path=/; expires=' + date.toGMTString();
        $('.eu-cookies').hide();
    });

    validateCzechPostDelivery();

    if ($('#filter-form').length) {
        setActiveFilters();
        initRemoveProducerButton();
        setPaginationFilterQueryUrl();
    }
});

$(document).on('click', 'form .recalculate', function(){
    $('#cartStepOneForm').submit();
    return false;
});

$(document).on('click', '.svg-search-open', function() {
  $('.search-wrap').toggle();
});

$(document).on('change', 'form#cartStepOneForm .cart-table input', function(){
    $('#recalculate-cart-items').trigger('click');
    return false;
});

$(document).on('change', 'form .price-variant-change', function(){
    $('#addProductToCartForm').submit();
    return false;
});

$(document).on('change', 'form .form-data-change', function(){
    $('#change-state-button').trigger('click');
    return false;
});

$(document).on('click', '.modal .modal-close, .modal-overlay, .close-modal-btn', function() {
    closeModal();
});

$(document).on('click', '.choose-producers', function(){
    var producers = [];

    $('.producer-selector').each(function(){
        var id = parseInt($(this).val());
        var isChecked = $(this).is(':checked');
        if (isChecked) {
            producers.push(id);
        }
    });

    setFilterQueryStringInUrl('producers', producers.join(","));
    return false;
});

$(document).on('click', '.product-form-sort', function(){
    setFilterQueryStringInUrl('sortBy', $(this).data('sort'));
    return false;
});

$(document).on('change', '.filter-form-checkbox', function(){
    setFilterQueryStringInUrl($(this).data('name'), $(this).is(':checked') ? 1 : 0);
    return false;
});

$(document).on('click', '#choose-producer', function(){
    openModal('modal-product-producers');
});


// detail produktu: +- 1 ks
$(document).on('click', '.plus-minus-input .plus-piece', function(){
    var $input = $('.plus-minus-input input')
    var count = $input.val();
    count++;
    $input.val(count);
    $input.trigger('change');
});

$(document).on('click', '.plus-minus-input .minus-piece', function(){
    var $input = $('.plus-minus-input input')
    var count = $input.val();
    count--;
    $input.val(count);
    $input.trigger('change');
});

$(document).on('click', '.modal-trigger', function() {
    var modalID = $(this).attr('data-modal-id');
    openModal(modalID);
});

function closeModal()
{
    $('.modal-overlay').remove();
    $('.modal').hide();
    $('body').removeClass('no-scroll');
}

function openModal(modalID)
{
    $('#page').append('<div class="modal-overlay"></div>');

    //var modalID = $(this).attr('data-modal-id');
    var modal = $('#' + modalID);
    modal.show();

    modalPosition(modal);

    $('body').addClass('no-scroll');

    return false;   // kdyz to je submit button
}

function modalPosition(modal)
{
    var top, left;

    //top = $(window).scrollTop() + 40;
    left = Math.max($(window).width() - modal.outerWidth(), 0) / 2;

    modal.css('top', top).css('left', left);

    if(modal.is("#modal-product-producers")) {
        if($("#modal-product-producers").height() > ($(window).height() - 40)) {
            modal.css('bottom', '20px');
            $("#modal-product-producers .notice-text").height($(window).height() - 185);
        } else if(modal.css('bottom') === '20px') {
            $("#modal-product-producers .notice-text").height($(window).height() - 185);
        }
    }
}

function findOffice() {
    var input = $('#find-office');
    $.nette.ajax({
        url: input.data('handle-link'),
        data: {
            'like': $('#find-office').val()
        },
        success: function(payload){
            fillCzechPostSelect($("#cp-office-select"), payload.offices);
        }
    });

}

function findBox() {

    var input = $('#find-box');
    $.nette.ajax({
        url: input.data('handle-link'),
        data: {
            'like': $('#find-box').val()
        },
        success: function(payload){
            fillCzechPostSelect($("#cp-box-select"), payload.boxes);
        }
    });

}

function fillCzechPostSelect(element, data) {
    element.empty(); // remove old options

    $("<option />", {
        value: 0,
        text: ' --- Vyberte pobočku --- ',
        selected: true
    }).appendTo(element);

    $.each(data, function(key,value) {
        $("<option />", {
            value: key,
            text: value,
            selected: false
        }).appendTo(element);
    });
}

function validateCzechPostDelivery(){
    $('#cartStepOneForm-submit').on('click', function(){
        var selectedDeliveryElement = $('input:radio[name="delivery"]:checked');

        if (selectedDeliveryElement.hasClass('cp-office-radio')) {
            var selectedValue = $('#cp-office-select option:selected').val();
            if (selectedValue == null) {
                alert('Vyhledejte pobočku dle města nebo PSČ, kam chcete balík doručit.');
                $('#find-office').focus();
                return false;
            }

            if (parseInt(selectedValue) === 0) {
                alert('Vyberte ze seznamu nalezených poboček tu správnou, kam chcete balík doručit.');
                $('#cp-office-select').focus();
                return false;
            }
        }

        if (selectedDeliveryElement.hasClass('cp-box-radio')) {
            var selectedValue = $('#cp-box-select option:selected').val();
            if (selectedValue == null) {
                alert('Vyhledejte pobočku dle města nebo PSČ, kam chcete balík doručit.');
                $('#find-box').focus();
                return false;
            }

            if (parseInt(selectedValue) === 0) {
                alert('Vyberte ze seznamu nalezených poboček tu správnou, kam chcete balík doručit.');
                $('#cp-box-select').focus();
                return false;
            }
        }
    });

}

function setActiveFilters(){
    var queryParameters = getUrlQueryParameters();

    if ( "specialOffer" in queryParameters && queryParameters["specialOffer"] == 1) {
        $('input[type="checkbox"][name="specialOffer"]').prop('checked', true);
    }

    if ( "onlyAvailable" in queryParameters && queryParameters["onlyAvailable"] == 1) {
        $('input[type="checkbox"][name="onlyAvailable"]').prop('checked', true);
    }
}

function setFilterQueryStringInUrl(type, value){
    var queryParameters = getUrlQueryParameters();

    queryParameters[type] = value;
    location.search = $.param(queryParameters);
}

function setPaginationFilterQueryUrl(){
    var queryParameters = getUrlQueryParameters();
    var queryString = $.map(queryParameters, function(value, name) {
            return name + '=' + value;
        })
        .join('&');

    if (queryString.length > 0) {
        $('.pagination-link').each(function () {
            var href = $(this).attr('href') + '?' + queryString;
            $(this).attr('href', href);
        });
    }

}

function getUrlQueryParameters() {
    var queryParameters = {};
    var queryString = location.search.substring(1);
    var re = /([^&=]+)=([^&]*)/g;
    var m;

    // Creates a map with the query string parameters
    while (m = re.exec(queryString)) {
        queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
    }

    return queryParameters;
}

function initRemoveProducerButton(){
    $('.remove-producer').click(function(){
        var queryParameters = getUrlQueryParameters();
        var producers  = queryParameters["producers"].split(",");
        var id = $(this).data('producer').toString();
        producers.splice($.inArray(id, producers),1);
        setFilterQueryStringInUrl('producers', producers.join(","));
        return false;
    });
}
