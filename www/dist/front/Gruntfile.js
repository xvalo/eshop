module.exports = function(grunt) {
	require('jit-grunt')(grunt);

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-uglify-es');

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // Concat: spojovani JS
    // --------------------

    concat: {
		dist: {
			src: ['javascript/*.js'],
			dest: 'dist/jscripts.js'
		}
    },

    // Uglify: minifikace JS
    // ---------------------

    uglify: {
      script: {
          src: 'dist/jscripts.js',
          dest: 'dist/jscripts.min.js'
      }
    },
    // CSS
    // ===

    // LESS kompilace
    // --------------

    less: {
      default: {
        files: {
          'dist/style_temp.css': 'css/layout.less'
        }
      },
      sourcemaps: {
        files: {
          'dist/css/style_temp.css': 'css/layout.less'
        }
      }
    },	
// autoprefixer	
    autoprefixer: {
      options: {
        browsers: ['last 3 versions', 'ios 6', 'ie 7', 'ie 8', 'ie 9'],
        map: false // Updatni SourceMap
      },
      style: {
          src: 'dist/style_temp.css',
          dest: 'dist/style_temp.css'
      }
    },	
// minifikace css
    cssmin: {
      css: {
        files: {
          'dist/style_final.css': 'dist/style_temp.css'
        }
      }
    },	
// watch
// -----

	watch: {
		less: {
			files: ['css/**/*.less', 'javascript/**/*.less'],
	        tasks: ['css']
		},
      js: {
        files: 'javascript/**/*.js',
        tasks: ['js']
      }
    },
  

criticalcss: {
	custom: {
		options: {
			url: "http://cukr_akadem.xred.cz",
			width: 1300,
			height: 900,
			outputfile: "css/critical.css",
			filename: "css/styles.css",
			ignoreConsole: true
		}
	}
},
pixrem: {
	options: {
		rootvalue: '100%'
	},
	dist: {
		src: 'dist/style_temp.css',
		dest: 'dist/style_temp.css'
	}
},
devUpdate: {
        main: {
            options: {
                updateType: 'report' //just report outdated packages 
            }
        }
    },
	svg_sprite: {                
        basic: {
            // Target basics
            expand: true,
            src: ['images/svg/pre-svg/*.svg'],
            dest: 'images/svg',

            // Target options
            options: {
                mode: {
                    symbol: true
                }
            }
        }
    }
  });

//  Custom tasky
// ==============

  //grunt.registerTask('css', ['less:default', 'autoprefixer', 'cssmin']);
  //grunt.registerTask('css', ['less:default', 'pixrem', 'autoprefixer']);
  grunt.registerTask('css', ['less:default', 'pixrem', 'autoprefixer', 'cssmin']);
  grunt.registerTask('js', ['concat', 'uglify']);
  grunt.registerTask('critical', ['criticalcss']);
  grunt.registerTask('svgsprite', ['svg_sprite']);
  grunt.registerTask('update', ['devUpdate']);  
  //grunt.registerTask('default', ['copy', 'css', 'js', 'browserSync', 'watch']);


  // Default task(s).
  grunt.registerTask('default', ['js','css','watch']);
  grunt.registerTask('build', ['js','css']);
};