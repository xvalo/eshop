$(function(){
    $.nette.ext('orderActions', {
        success: function (payload) {
            initOrderAutocomplete();
            invalidateProductQuantityChange();
            applyOrderDiscount();
            orderSubmitButton();

            if (payload.extraItemAdded !== undefined && payload.extraItemAdded === true) {
                $('.extra-items').find('input').each(function(){
                    $(this).val(null);
                });

                $('.extra-items').find('select').val(21);
            }
        }
    });

    initOrderAutocomplete();
    invalidateProductQuantityChange();
    applyOrderDiscount();
    orderSubmitButton();

    $('[data-dependentselectbox]').dependentSelectBox();

    $('#customer-selector').select2({
        ajax: {
            allowClear: true,
            delay: 250,
            dataType: 'json',
            url: $('#customer-selector').data('customer-search-url'),
            data: function (params) {
                return {
                    q: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            minimumInputLength: 3
        }
    });

    $('#customer-selector').on('select2:select', function(event) {
        var customerID = event.params.data.id;
        var url = $('#customer-selector').data('load-url');

        $.nette.ajax({
            type: 'GET',
            url: url,
            data: {
                'orderEdit-customer': customerID
            }
        });

    });

    $('#frm-paymentForm input[name="amount"]').keypress(function(event){
        if(event.keyCode === 13){
            $('#btn-send-paid-cash').trigger('click');
            return false;
        }
    });
});

function invalidateProductQuantityChange() {
    $('input.product-count').change(function(){
        var key = $(this).data('key');
        var quantity = $(this).val();
        var url = $(this).closest('form').data('change-quantity-url');
        $.nette.ajax({
            url: url+'&orderEdit-key='+key+'&orderEdit-quantity='+quantity,
        });
    });
}

function initOrderAutocomplete() {
    var autocompleteOptions = {
        url: function(phrase) {
            var url = $("#products-variants").data('items-search-url');
            return url+'?q='+phrase;
        },
        getValue: 'title',
        requestDelay: 500,
        list: {
            onClickEvent: function() {
                var id = $("#products-variants").getSelectedItemData().id;
                var url =  $("#products-variants").data('add-item-url');
                $.nette.ajax({
                    url: url+'&orderEdit-id='+id
                });
                $("#products-variants").val(null);
                $('#eac-container-products-variants ul').hide();
            }
        }
    };

    $('#products-variants').easyAutocomplete(autocompleteOptions);

    $('#products-variants').keydown(function(event){
        if(event.keyCode === 13) {
            event.preventDefault();
            return false;
        }
    });
}

function applyOrderDiscount() {
    $('#order-discount').change(function(){
        $.nette.ajax({
            url: $(this).data('url')+'&orderEdit-discount='+$(this).val(),
        });
    });
}

function orderSubmitButton() {
    $('#order-form-submit').click(function(){
        $('#frm-orderEdit-form').submit();
    });
}