$(function(){
    $.nette.ext('consoleLog', {
        success: function (payload) {
            initEasyAutocomplete();
            initTinyMce();
        },
        error: function (jqXHR, status, error) {
            console.log('Ajax failed!');
            console.log(error);
            console.log(jqXHR);
        }
    });

    $.nette.ext('hardRedirect', {
        success: function (payload) {
            if (payload.hardRedirect) {
                window.location.href = payload.hardRedirect;
                return false;
            }
        }
    });

    $.nette.init();

    initEasyAutocomplete();

    $.fn.datepicker.dates['cs'] = {
        days: ["Neděle", "Pondělí", "Úterý", "Středa", "Čtvrtek", "Pátek", "Sobota"],
        daysShort: ["Ned", "Pon", "Úte", "Stř", "Čtv", "Pát", "Sob"],
        daysMin: ["Ne", "Po", "Út", "St", "Čt", "Pá", "So"],
        months: ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"],
        monthsShort: ["Led", "Úno", "Bře", "Dub", "Kvě", "Čvn", "Čvc", "Srp", "Zář", "Říj", "Lis", "Pro"],
        today: "Dnes",
        clear: "Zrušit",
        format: "dd. mm. yyyy",
        weekStart: 1
    };

    $('.datepicker').datepicker({
        language: 'cs',
        todayBtn: 'linked'
    });

    initTinyMce();

    $('#snippet--stock-form-items').on('keyup keypress', function(e){
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            $('#bill-add-item-button').click();
        }
    });

    if ($('#receipt-download-link').length) {
        window.location.href = $('#receipt-download-link').data('url');
    }

    // Javascript to enable link to tab
    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href="#'+url.split('#')[1]+'"]').tab('show') ;
    }

    // With HTML5 history API, we can easily prevent scrolling!
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        if(history.pushState) {
            history.pushState(null, null, e.target.hash);
        } else {
            window.location.hash = e.target.hash; //Polyfill for old browsers
        }
    })
});

// Basic form submit after change of some inputs
$(document).on('change', 'form .onchange-submit', function(){
    $('#frm-form').submit();
    $('#main-form').submit();
});

$(document).on('change', '.change-order-line-count', function(){
    var variantsToAddInput = $('input[name="variantsToAdd"]');
    var values = JSON.parse(variantsToAddInput.val());
    var id = $(this).data('order-line');

    values[id] = $(this).val();
    variantsToAddInput.val(JSON.stringify(values));

    $('#frm-form').submit();
});

// Load of customer
$(document).on('change', '#frm-form-customer', function(){
    var customerId = $(this).val();
    var url = $(this).data('load-url');
    $.nette.ajax({
        type: 'GET',
        url: url,
        data: {
            'customer': customerId
        }
    });
});

// Remove product line
$(document).on('click', '.remove-order-line', function(){
    removeOrderLine($(this).data('remove-line'));
});

$(document).on('click', '#sendPackageShipping', function(){
    var url = $(this).data('url');
    var packageNo = $('#packageNo').val();

    $.nette.ajax({
        type: 'GET',
        url: url,
        data: {
            'packageNo': packageNo
        }
    });
});


function initEasyAutocomplete()
{
    var autocompleteOptions = {
        url: function(phrase) {
            var url = $("#products-variants").data('items-search-url');
            return url+'?q='+phrase;
        },
        getValue: 'title',
        requestDelay: 500,
        list: {
            onClickEvent: function() {
                var id = $("#products-variants").getSelectedItemData().id;
                addVariant(id);
                $("#products-variants").val(null);
                $('#eac-container-products-variants ul').hide();
            }
        }
    };

    $('#products-variants').easyAutocomplete(autocompleteOptions);

    $('#products-variants').keydown(function(event){
        if(event.keyCode === 13) {
            event.preventDefault();
            return false;
        }
    });
}

function initTinyMce()
{
    tinymce.init({
        selector: '.mceEditor',
        language: 'cs',
        height: 600,
        menubar: false,
        plugins: [
            'advlist autolink lists link image preview anchor code',
            'searchreplace visualblocks fullscreen',
            'insertdatetime media table contextmenu paste'
        ],
        toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
        content_css: '//www.tinymce.com/css/codepen.min.css',
        paste_convert_word_fake_lists: false,
        relative_urls : false,
        remove_script_host: false,
        apply_source_formatting : true,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });
}

function addVariant(id)
{
    $.ajax({
        url: '/api/product/get-price-detail/'+id
    }).done(function(data){
        var variantsToAddInput = $('input[name="variantsToAdd"]');
        var values = {};

        if(variantsToAddInput.val() != ''){
            values = JSON.parse(variantsToAddInput.val());
        }

        values[data.id] = 1;
        variantsToAddInput.val(JSON.stringify(values));

        $('#main-form').submit();
    });
}

function removeOrderLine(id){
    $('tr#order-line-'+id).remove();
    var values = getProductsVariantInputData();
    delete values[id];
    writeProductsVariantData(values);
    $('#frm-form').submit();
}

function getProductsVariantInputData(){
    return JSON.parse($('input[name="variantsToAdd"]').val());
}

function writeProductsVariantData(data){
    $('input[name="variantsToAdd"]').val(JSON.stringify(data));
}