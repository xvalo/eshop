$(function(){
    $('#choose-all-variants').on('change', function(){
        var isSelected = $(this).is(':checked');

        $('.attribute-variant-selection').each(function(){
            $(this).attr("checked", isSelected);
        });
    });
});