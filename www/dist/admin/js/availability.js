$(function(){
    $('.select2availability').select2({
        templateResult: renderOption,
        templateSelection: renderOption,
    });
});

function renderOption(data) {
    // We only really care if there is an element to pull classes from
    if (!data.element) {
        return data.text;
    }

    var $element = $(data.element);
    var $wrapper = $('<span class="label" style="background-color: #'+$element.data('color')+' !important;"></span>');
    $wrapper.text(data.text);

    return $wrapper;
}