$(function(){
    $('.code-update').click(function(){
        var parent = $(this).parent().parent();
        var codeVariantId = parent.data('code-variant-id');
        var code = parent.find('.code-column').data('code');
        var supplier = parent.find('.supplier-column').data('code-supplier');
        var priceSynchronized = parent.find('.price-synchronized-column').data('synchronized') === 1;
        var availabilitySynchronized = parent.find('.availability-synchronized-column').data('synchronized') === 1;

        $('#frm-variantDetailControl-codeForm-code').val(code);
        $('#frm-variantDetailControl-codeForm-supplier').val(supplier);
        $('#frm-variantDetailControl-codeForm-isPriceSynchronized').attr('checked', priceSynchronized);
        $('#frm-variantDetailControl-codeForm-isAvailabilitySynchronized').attr('checked', availabilitySynchronized);
        $('#variant-code-form input[type="hidden"][name="id"]').val(codeVariantId);

        $('#modal-code').modal('show');
    });

    $('.price-update').click(function(){
        var parent = $(this).parent().parent();
        var priceVariantId = parent.data('price-variant-id');
        var name = parent.find('.name-column').data('name');
        var price = parent.find('.price-column').data('price');
        var type = parent.find('.type-column').data('type');
        var synchronized = parent.find('.synchronized-column').data('synchronized') === 1;
        var countedFrom = parent.find('.counted-from-column').data('counted-from');
        var additionalValue = parent.find('.additional-value-column').data('additional-value');
        var purchasePrice = parent.find('.purchase-price-column').data('purchase-price');

        $('#frm-variantDetailControl-priceForm-name').val(name);
        $('#frm-variantDetailControl-priceForm-price').val(price);
        $('#frm-variantDetailControl-priceForm-type').val(type);
        $('#frm-variantDetailControl-priceForm-synchronized').attr('checked', synchronized);
        $('#frm-variantDetailControl-priceForm-countedFrom').val(countedFrom);
        $('#frm-variantDetailControl-priceForm-value').val(additionalValue);
        $('#frm-variantDetailControl-priceForm-purchasePriceWithoutVat').val(purchasePrice);
        $('#frm-variantDetailControl-priceForm input[type="hidden"][name="id"]').val(priceVariantId);

        tooglePriceParts(type);
        $('#modal-price').modal('show');
    });

    $('.attribute-variant-update').click(function(){
        var parent = $(this).parent().parent();
        var variantId = parent.data('variant-id');
        var name = parent.find('.name-column').data('name');
        var price = parent.find('.price-column').data('price');
        var isNews = parent.find('.news-column').data('isnews') === 1;
        var isDiscount = parent.find('.discount-column').data('isdiscount') === 1;
        var isActive = parent.find('.active-column').data('isactive') === 1;

        $('#attribute-variant-form input[name="name"]').val(name);
        $('#attribute-variant-form input[name="price"]').val(price);
        $('#attribute-variant-form input[name="isNews"]').attr('checked', isNews);;
        $('#attribute-variant-form input[name="isDiscount"]').attr('checked', isDiscount);;
        $('#attribute-variant-form input[name="isActive"]').attr('checked', isActive);
        $('#attribute-variant-form input[type="hidden"][name="id"]').val(variantId);

        $('#modal-attribute-variant').modal('show');
    });

    $('.dismiss-modal').click(function () {
        $('.modal').find('input, select').each(function () {
            switch (this.type) {
                case 'button':
                case 'text':
                case 'password':
                case 'file':
                case 'email':
                case 'date':
                case 'hidden':
                case 'number':
                case 'select-one':
                    $(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
                    break;
            }
        });
    });

    $('#modal-price').on('shown.bs.modal', function () {
        var priceType = $('#frm-variantDetailControl-priceForm-type').val() ? $('#frm-variantDetailControl-priceForm-type').val() : 'R';
        tooglePriceParts(priceType);
    });

    $('#frm-variantDetailControl-priceForm-type').change(function(){
        var priceType = $(this).val();
        tooglePriceParts(priceType);
    });

    $('.stock-item-update').click(function(){
        var parent = $(this).parent().parent();

        var stockId = parent.data('stock-id');
        var stockName = parent.data('stock-name');
        var count = parent.find('.stock-item-count').data('stock-item-count');
        var limit = parent.find('.stock-item-limit').data('stock-item-limit');
        var position = parent.find('.stock-item-position').data('stock-item-position');
        var alertEnabled = parent.find('.stock-item-alert-enabled').data('stock-item-alert-enabled') === 1;

        $('#modal-stock-title-name').text(stockName);
        $('#frm-variantDetailControl-stockItemForm-count').val(count);
        $('#frm-variantDetailControl-stockItemForm-limit').val(limit);
        $('#frm-variantDetailControl-stockItemForm-position').val(position);
        $('#frm-variantDetailControl-stockItemForm-alertEnabled').attr('checked', alertEnabled);
        $('#frm-variantDetailControl-stockItemForm input[type="hidden"][name="stockId"]').val(stockId);

        $('#modal-stock-item').modal('show');
    });

    $('.product-select-finder').select2({
        ajax: {
            url: $('.product-select-finder').data('search-link'),
            dataType: 'json',
            data: function (params) {
                return {
                    q: params.term
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            delay: 250,
            minimumInputLength: 3,
            allowClear: true,
        }
    });

    $('.product-select-finder').on('select2:select', function(event) {
        var productID = event.params.data.id;
        var url = $('.product-select-finder').data('create-relation-link');

        $.nette.ajax({
            type: 'GET',
            url: url,
            data: {
                'relatedProducts-productId': productID
            }
        });

    });
});

function tooglePriceParts(priceTypeToShow) {
    $('.notshow').hide();
    if (priceTypeToShow === 'R') {
        $('.price-regular').show();
    } else if (priceTypeToShow === 'P') {
        $('.price-percentage').show();
    } else if (priceTypeToShow === 'S') {
        $('.price-subtracted').show();
    } else {
        $('.price-regular').show();
    }
}