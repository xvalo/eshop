<?php

namespace App\FrontModule\Components;

use App\Category\Entity\Category;

interface IFilterFormFactory
{
	/**
	 * @param Category $category
	 * @return FilterForm
	 */
    function create(Category $category);
}