<?php
namespace App\FrontModule\Components;

use App\Category\Entity\Category;
use App\Lists\ProducerRepository;
use Nette;

class FilterForm extends Nette\Application\UI\Control
{
    /** @var ProducerRepository */
    private $producerRepository;

    /** @var Category */
    private $category;

	public function __construct(
		Category $category,
        ProducerRepository $producerRepository
    ){
    	parent::__construct();

    	$this->category = $category;
        $this->producerRepository = $producerRepository;
	}

    public function render()
    {
        $this->template->setFile(__DIR__.'/filter.latte');

	    $pickedProducers = $this->presenter->getParameter('producers', false);
	    $producers = $this->getProducers();

        $this->template->producers = $producers;
        $this->template->availableProducers = Nette\Utils\Json::encode($producers);
        $this->template->pickedProducers = $pickedProducers ? explode(',', $pickedProducers) : [];

        $this->template->render();
    }


    private function getProducers()
    {
        $producers = [];

        if (!isset($this->category)) {
            return $producers;
        }

        foreach ($this->producerRepository->getListByCategory($this->category) as $producer) {
            $producers[$producer['id']] = $producer['name'];
        }

        return $producers;
    }
}