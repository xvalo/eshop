<?php

namespace App\Components;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class SearchForm extends Control
{
    public function render()
    {
        $this->template->setFile(__DIR__ . '/searchForm.latte');
        $this->template->render();
    }

    protected function createComponentSearchForm()
    {
        $form = new Form();

        $form->addText('text')
                ->setRequired('Musíte vyplnit vyhledávaný dotaz.');
        $form->addSubmit('search');

        $form->onSuccess[] = function(Form $form) {
            $value = $form->getValues()->text;
            $this->presenter->redirect('Search:default', ['q' => $value]);
        };

        return $form;
    }
}