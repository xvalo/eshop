<?php

namespace App\Components;

use App\Product\Product;
use Nette\ComponentModel\Container;

interface IAddProductToCartFactory
{
	/**
	 * @param Product $product
	 * @param Container $parent
	 * @param string $name
	 * @return AddProductToCart
	 */
	public function create(Product $product, Container $parent, string $name);
}