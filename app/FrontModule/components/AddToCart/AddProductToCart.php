<?php

namespace App\Components;

use App\Classes\Product\Variant\ProductVariantService;
use App\Cart\CartManager;
use App\Product\Availability\ColorProvider;
use App\Product\Price\Price;
use App\Product\Variant;
use App\Product\Product;
use App\Product\ProductAvailability;
use App\Utils\Numbers;
use App\WatchDog\WatchDogService;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\ComponentModel\Container;
use Nette\Utils\Json;
use Tracy\Debugger;

class AddProductToCart extends Control
{
	/** @var Product */
	private $product;

	/** @var CartManager */
	private $cartManager;

	/** @var Price[] */
	private $variantPrices;

	/** @var array */
	private $variantsLabels;

	/** @var array */
	private $prices;

	/** @var int */
	private $sentPriceVariant;

	/** @var int */
	private $sentCount;

	/** @var WatchDogService */
	private $watchDogService;

	/** @var ProductVariantService */
	private $productVariantService;

	/** @var ColorProvider */
	private $colorProvider;

	public function __construct(
		Product $product,
		Container $parent = NULL,
		string $name = NULL,
		CartManager $cartManager,
		WatchDogService $watchDogService,
		ProductVariantService $productVariantService,
		ColorProvider $colorProvider
	){
		parent::__construct($parent, $name);

		$this->product = $product;
		$this->watchDogService = $watchDogService;
		$this->productVariantService = $productVariantService;
		$this->cartManager = $cartManager;
		$this->colorProvider = $colorProvider;

		$this->processVariants($product->getPriceVariants());

		$postData = $this->presenter->getRequest()->getPost();
		if (!empty($postData) && isset($postData['priceVariant'])) {
			$this->sentPriceVariant = $postData['priceVariant'];
			$this->sentCount = (!isset($postData['count']) || $postData['count'] < 0) ? 1 : $postData['count'];
		} else {
			reset($this->prices);
			reset($this->variantsLabels);
			$this->sentPriceVariant = key($this->variantsLabels);
			$this->sentCount = 1;
		}
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/AddProductToCart.latte');

		/** @var Price $price */
		$price = $this->variantPrices[$this->sentPriceVariant];
		/** @var Variant $variant */
		$variant = $price->getVariant();

		$this->template->product = $this->product;
		$this->template->availability = $variant->getAvailability()->getTitle();
		$this->template->availabilityClass = $this->colorProvider->getAvailabilityClass($variant->getAvailability()->getColor());
		$this->template->canBeOrdered = $variant->getAvailability()->isProductOrderable();
		$this->template->availabilityDescription = $variant->getAvailability()->getDescription();

		$this->template->price = $this->prices[$this->sentPriceVariant] * $this->sentCount;
		$this->template->normalPrice = $this->product->getNormalPrice() * $this->sentCount;

		$this->template->render();
	}

	/**
	 * @return Form
	 */
	protected function createComponentAddProductToCartForm()
	{
		$form = new Form;

		$form->addText('count')
			->setDefaultValue(1);

		if (count($this->variantPrices) > 1) {
			$form->addSelect('priceVariant', null, $this->variantsLabels);
		} else {
			$form->addHidden('priceVariant', $this->product->getPriceVariant()->getPrice()->getId());
		}

		$form->addHidden('prices', Json::encode($this->prices));

		$form->addText('email')
			->addCondition(Form::FILLED)
			->addRule(Form::EMAIL, 'Zadejte email ve správném tvaru.');

		$form->addSubmit('addToCart')
			->setValidationScope(false);
		$form->addSubmit('saveWatchDog')
			->setValidationScope([$form['email']]);

		$form->onSuccess[] = function (Form $form) {
			if ($form['saveWatchDog']->isSubmittedBy()) {
				$this->setWatchDogOnProduct($form);
			} else {
				$this->addProductToCartFromSucceeded($form);
			}
		};

		$form->setDefaults([
			'priceVariant' => $this->sentPriceVariant,
			'count' => $this->sentCount
		]);

		return $form;
	}

	protected function createComponentWatchDogForm()
	{
		$form = new Form;

		$form->addText('email')
			->addCondition(Form::FILLED)
			->addRule(Form::EMAIL, 'Zadejte email ve správném tvaru.');
		$form->addHidden('productVariant');

		$form->addSubmit('save');

		$form->setDefaults([
			'productVariant' => $this->sentPriceVariant
		]);

		$form->onSuccess[] = function (Form $form) {
			try {
				$values = $form->getValues();

				$price = $this->productVariantService->getPrice($values->productVariant);

				$this->watchDogService->createItem(
					$price->getVariant(),
					$values->email
				);

				$this->presenter->flashMessage('Hlídací pes byl nastaven. Jakmile se změní dostupnost produktu, budeme vás informovat na email, který jste zadal/a.');
			} catch (\Exception $e) {
				Debugger::log($e);
				$this->presenter->flashMessage('Při ukládání došlo k chybě. Zkuste to znovu nebo se na nás, prosím, obraťte.');
			}

			$this->presenter->redirect('this');
		};

		return $form;
	}

	private function processVariants($pricesVariants)
	{
		/** @var Variant $variant */
		foreach ($pricesVariants as $variant) {
			foreach ($variant->getPrices() as $price) {
				$this->variantPrices[$price->getId()] = $price;
				$this->variantsLabels[$price->getId()] = "{$variant->getName()} {$price->getName()} (" . Numbers::czechFormatedNumber($price->getPrice(), false) . ",- Kč)";
				$this->prices[$price->getId()] = $price->getPrice();
			}
		}
	}

	private function addProductToCartFromSucceeded(Form $form)
	{
		$productPriceAddedToBasket = false;
		try {
			if ($form['addToCart']->isSubmittedBy()) {
				$values = $form->getValues();
				$variantPrice = $this->productVariantService->getPrice((int)$values->priceVariant); //$this->product->getPriceVariant($values->priceVariant);
				$item = $this->cartManager->add($variantPrice, [], (int) $values->count);
				$productPriceAddedToBasket = $item->getItemUnitPrice() * (int) $values->count;
				$productCountAddedToBasket = (int) $values->count;
			}
		} catch (\Exception $e) {
			Debugger::log($e);
			$this->presenter->flashMessage('Produkt se nepodařilo vložit do košíku. Někde nastala chyba.');
			$this->presenter->redirect('this');
		}

		if ($this->presenter->isAjax()) {
			if ($productPriceAddedToBasket) {
				$this->presenter->payload->productAdded = true;
				$this->presenter->payload->productPriceAddedToBasket = Numbers::czechFormatedNumber($productPriceAddedToBasket, false);
				$this->presenter->payload->productCountAddedToBasket = $productCountAddedToBasket;
			}

			$this->redrawControl('product-to-cart');
			$this->redrawControl('product-info');
			$this->presenter->redrawControl('cart-header-info');
		} else {
			$this->presenter->redirect('this');
		}
	}
}