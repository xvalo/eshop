<?php

namespace App\FrontModule\Components\OpenGraphData;

use App\Core\Application\Settings\ApplicationSettingsManager;
use Nette\Application\UI\Control;

class OpenGraph extends Control
{
	/**
	 * @var OpenGraphDataObject
	 */
	private $dataObject;

	/**
	 * @var ApplicationSettingsManager
	 */
	private $applicationSettingsManager;

	public function __construct(
		OpenGraphDataObject $dataObject,
		ApplicationSettingsManager $applicationSettingsManager
	){
		parent::__construct();
		$this->dataObject = $dataObject;
		$this->applicationSettingsManager = $applicationSettingsManager;
	}

	public function render()
	{
		$this->template->og = $this->createOpenGraph();
		$this->template->setFile(__DIR__ . '/openGraph.latte');
		$this->template->render();
	}

	private function createOpenGraph()
	{
		$og = new \ChrisKonnertz\OpenGraph\OpenGraph();

		$og->title($this->dataObject->getTitle())
			->url($this->dataObject->getUrl())
			->description($this->dataObject->getDescription())
			->type('website')
			->siteName($this->applicationSettingsManager->getWebsiteTitle())
			->locale('cs_CZ');

		$baseUrl = $this->presenter->link('//:Front:Homepage:default');

		/** @var Image $image */
		foreach ($this->dataObject->getImages() as $image) {
			$og->image($baseUrl . ltrim($image->getRelativePath(), '/'));
		}

		return $og;
	}
}

interface IOpenGraphFactory
{
	/**
	 * @param OpenGraphDataObject $dataObject
	 * @return OpenGraph
	 */
	public function create(OpenGraphDataObject $dataObject);
}