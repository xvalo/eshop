<?php

namespace App\FrontModule\Components\OpenGraphData;

use App\File\Image;

class OpenGraphDataObject
{
	/**
	 * @var string
	 */
	private $title;

	/**
	 * @var string
	 */
	private $url;

	/**
	 * @var string
	 */
	private $description;

	/**
	 * @var array|Image[]
	 */
	private $images;

	public function __construct(string $title, string $url, string $description, array $images = [])
	{
		$this->title = $title;
		$this->url = $url;
		$this->description = $description;
		$this->images = $images;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return $this->url;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return $this->description;
	}

	/**
	 * @return array
	 */
	public function getImages(): array
	{
		return $this->images;
	}
}