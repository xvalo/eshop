<?php

namespace App\FrontModule\Components\RelatedProducts;

use App\Product\Product;

interface IRelatedProductsListFactory
{
	/**
	 * @param Product $product
	 * @return RelatedProductsList
	 */
	public function create(Product $product);
}