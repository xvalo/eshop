<?php

namespace App\FrontModule\Components\RelatedProducts;

use App\Product\Product;
use App\Product\Related\RelatedProductService;
use Nette\Application\UI\Control;
use Ublaboo\ImageStorage\ImageStorage;

class RelatedProductsList extends Control
{
	/** @var Product */
	private $product;

	/** @var RelatedProductService */
	private $relatedProductService;

	/** @var ImageStorage */
	private $imageStorage;

	public function __construct(
		Product $product,
		RelatedProductService $relatedProductService,
		ImageStorage $imageStorage
	){
		parent::__construct();
		$this->product = $product;
		$this->relatedProductService = $relatedProductService;
		$this->imageStorage = $imageStorage;
	}

	public function render()
	{
		$this->template->products = $this->relatedProductService->getProductRelatedProducts($this->product);

		$this->template->imageStorage = $this->imageStorage;
		$this->template->setFile(__DIR__.'/list.latte');
		$this->template->render();
	}
}