<?php

namespace App\FrontModule\Components;

use DK\Menu\UI\Control;

class AccountMenuControl extends Control
{

}

interface IAccountMenuFactory
{
    /** @return AccountMenuControl */
    public function create();
}