<?php

namespace App\FrontModule\Components;

use App\Category\CategoryService;
use DK\Menu\Container;
use DK\Menu\Menu;
use DK\Menu\UI\Control;

class CategoryMenuControl extends Control
{
    const MENU_NAME = 'category';
    const MENU_ID = 1;

    public function __construct(Menu $menu, CategoryService $service)
    {
        parent::__construct($menu);

		if (!$menu->hasItems()) {
			$this->setMenuItems($this->getMenu(), $service->getCategoryMenuList());
		}
    }

    public function render()
    {
        $this->template->menuId = self::MENU_ID;
        parent::render();
    }

    protected function setMenuItems(Container $parent, array $items)
    {
        foreach ($items as $data) {
            $item = $parent->addItem($data['title'], $data['target'], $data['parameters'], $data['name']);

            if (count($data['items']) > 0) {
                $this->setMenuItems($item, $data['items']);
            }
        }
    }
}

interface ICategoryMenuFactory
{
    /** @return CategoryMenuControl */
    public function create();
}