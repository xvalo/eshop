<?php

namespace App\FrontModule\Components\Pages;

use Nette\Application\UI\Control;
use App\Page\PageFacade;
use Contributte\Cache\CacheFactory;

class PagesList extends Control
{
	const FOOTER_PAGES_CACHE_NAME = 'footer-pages';

	/**
	 * @var \Nette\Caching\Cache
	 */
	private $cache;

	/**
	 * @var PageFacade
	 */
	private $pageFacade;

	public function __construct(PageFacade $pageFacade, CacheFactory $cacheFactory)
	{
		parent::__construct();

		$this->cache = $cacheFactory->create(self::FOOTER_PAGES_CACHE_NAME);
		$this->pageFacade = $pageFacade;
	}

	public function renderHeader()
	{
		$pages = $this->cache->load('pages-header');

		if ($pages === null) {
			$pages = $this->pageFacade->getHeaderPages();
			$this->cache->save('pages', $pages);
		}

		$this->template->pages = $pages;

		$this->template->setFile(__DIR__.'/list.latte');
		$this->template->render();
	}

	public function renderFooter()
	{
		$pages = $this->cache->load('pages-footer');

		if ($pages === null) {
			$pages = $this->pageFacade->getFooterPages();
			$this->cache->save('pages', $pages);
		}

		$this->template->pages = $pages;

		$this->template->setFile(__DIR__.'/list.latte');
		$this->template->render();
	}
}

interface IPagesListFactory
{
	/**
	 * @return PagesList
	 */
	public function create();
}