<?php

namespace App\FrontModule\Components\TwitterCardData;

use App\Core\Socials\TwitterCard\TwitterCardDataObject;
use Nette\Application\UI\Control;

class TwitterCard extends Control
{
	/**
	 * @var TwitterCardDataObject
	 */
	private $dataObject;

	public function __construct(TwitterCardDataObject $dataObject)
	{
		parent::__construct();
		$this->dataObject = $dataObject;
	}

	public function render()
	{
		$this->template->data = $this->dataObject;
		$this->template->setFile(__DIR__ . '/twitterCard.latte');
		$this->template->render();
	}
}

interface ITwitterCardFactory
{
	/**
	 * @param TwitterCardDataObject $dataObject
	 * @return TwitterCard
	 */
	public function create(TwitterCardDataObject $dataObject);
}