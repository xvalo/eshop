<?php

namespace App\Components;

use App\Category\Entity\Category;
use App\Classes\Product\Variant\ProductVariantService;
use App\FrontModule\Components\IFilterFormFactory;
use App\Lists\Producer;
use App\Cart\CartManager;
use App\Product\Availability\AvailabilityProvider;
use App\Product\Listing\Filters\ProductFilter;
use App\Product\Listing\ProductsListingState;
use App\Product\Listing\ProductsProvider;
use App\Product\ProductService;
use App\Utils\Numbers;
use Nette\Application\UI\Control;
use Nette\Utils\Paginator;
use Ublaboo\ImageStorage\ImageStorage;
use Ublaboo\ImageStorage\ImageStoragePresenterTrait;

class ProductsList extends Control
{
	use ImageStoragePresenterTrait;

	const PLACE_HOMEPAGE = 'H';
	const PLACE_CATEGORY = 'C';
	const PLACE_SEARCH = 'S';
	const PLACE_PRODUCER = 'P';
	const PLACE_NEWS = 'N';

	/** @var ProductService */
	private $productService;

	/** @var CartManager */
	private $cartManager;

	/** @var IFilterFormFactory */
	private $filterFormFormFactory;

	/** @var ProductVariantService */
	private $productVariantService;

	/** @var Category|null */
	private $category;

	/** @var string */
	private $type;

	/** @var ProductsProvider */
	private $productsProvider;

	/** @var ProductsListingState */
	private $productsState = null;

	/** @var Producer */
	private $producer;

	/** @var AvailabilityProvider */
	private $availabilityProvider;

	public function __construct(
		Category $category = null,
		Producer $producer = null,
		ProductService $productService,
		ImageStorage $imageStorage,
		CartManager $cartManager,
		IFilterFormFactory $filterFormFormFactory,
		ProductVariantService $productVariantService,
		ProductsProvider $productsProvider,
		AvailabilityProvider $availabilityProvider
	){
		parent::__construct();

		$this->category = $category;
		$this->productService = $productService;
		$this->imageStorage = $imageStorage;
		$this->cartManager = $cartManager;
		$this->filterFormFormFactory = $filterFormFormFactory;
		$this->productVariantService = $productVariantService;
		$this->productsProvider = $productsProvider;
		$this->producer = $producer;
		$this->availabilityProvider = $availabilityProvider;
	}

	public function attached($presenter)
	{
		parent::attached($presenter);

		if ($this->type === self::PLACE_CATEGORY) {
			$filters = $this->presenter->getParameter('filters', false);
			$filters = $filters ? explode(',', $filters) : [];
			$producers = $this->presenter->getParameter('producers', false);
			$producers = $producers ? explode(',', $producers) : [];

			$this->productsState = $this->productsProvider->getCategoryProducts(
				(int)$this->presenter->getParameter('page', 1),
				$this->presenter->getParameter('sortBy', ProductFilter::SORT_DEFAULT),
				$this->category->getId(),
				$this->presenter->getParameter('specialOffer', false),
				$this->presenter->getParameter('onlyAvailable', false),
				$filters,
				$producers,
				[
					'min' => $this->presenter->getParameter('minPrice', null),
					'max' => $this->presenter->getParameter('maxPrice', null)
				]
			);
		}

		$this->setPaginatorState();
	}

	public function render()
	{
		$this->template->setFile(__DIR__.'/ProductsList.latte');

		$this->template->products = $this->productsState->getProducts();
		$this->template->pagination = $this->productsState->getPagination();

		$this->template->showPagination = $this->isPaginationActive();
		$this->template->showFilter = $this->isFilterActive();
		$this->template->categoryId = $this->category !== null ? $this->category->getId() : null;

		$this->template->availabilityList = $this->availabilityProvider->getList();

		$this->template->render();
	}

	public function setType(string $type)
	{
		$this->type = $type;
		return $this;
	}

	public function handleAddProductToBasket($productId)
	{
		try {
			$product = $this->productService->getProduct($productId);
			$price = $product->getPriceVariant()->getPrice();
			$this->cartManager->add($price);

			$this->presenter->payload->productAdded = true;
			$this->presenter->payload->productNameAddedToBasket = $product->getName();
			$this->presenter->payload->productPriceAddedToBasket = Numbers::czechFormatedNumber($price->getPrice(), false);
		} catch (\Exception $e) {
			$this->flashMessage('Došlo k neočekávané chybě. Pokuste se prosím o akci znovu nebo nás prosím kontaktujte a nahlašte chybu. Děkujeme.');
		}

		if ($this->presenter->isAjax()) {
			$this->presenter->redrawControl('cart-header-info');
		} else {
			$this->redirect('this');
		}
	}

	protected function createComponentFilterForm()
	{
		return $this->filterFormFormFactory->create($this->category);
	}

	protected function isPaginationActive()
	{
		return $this->type !== self::PLACE_HOMEPAGE;
	}

	protected function isFilterActive()
	{
		return $this->type === self::PLACE_CATEGORY;
	}

	public function setPaginatorState()
	{
		if ($this->productsState === null) {
			return false;
		}
		/** @var Paginator $paginator */
		$paginator = $this['visualPaginator']->getPaginator();
		$paginator->setItemCount( $this->productsState->getPagination()->getItemsCount() );
		$paginator->setItemsPerPage( $this->productsState->getPagination()->getItemsPerPage() );
	}

	protected function getVisualPaginatorPage()
	{
		$this['visualPaginator']->getPaginator()->getPage();
	}

	protected function createComponentVisualPaginator()
	{
		$control = new \IPub\VisualPaginator\Components\Control(__DIR__.'/pagination.latte', null, $this);
		$control->disableAjax();

		/*
		$that = $this->getPresenter();

		$control->onShowPage[] = (function ($component, $page) use ($that) {
			if ($that->isAjax()){
				$this->presenter->payload->url = $this->link('this');
				$this->presenter->payload->scrollTo = 'products-list';
				$this->redrawControl('products-filter');
				$this->redrawControl('products');
			}
		});
		*/

		return $control;
	}

}