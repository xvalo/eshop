<?php

namespace App\Components;

use App\Category\Entity\Category;
use App\Lists\Producer;

interface IProductListFactory
{
	/**
	 * @param Category|null $category
	 * @param Producer|null $producer
	 * @return ProductsList
	 */
	public function create(Category $category = null, Producer $producer = null);
}