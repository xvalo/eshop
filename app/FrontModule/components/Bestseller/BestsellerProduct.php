<?php

namespace App\FrontModule\Components\Bestseller;

use App\Product\Bestseller\BestsellerProvider;
use Nette\Application\UI\Control;
use Ublaboo\ImageStorage\ImageStorage;

class BestsellerProduct extends Control
{
	/**
	 * @var ImageStorage
	 */
	private $imageStorage;

	/**
	 * @var BestsellerProvider
	 */
	private $bestsellerProvider;

	public function __construct(BestsellerProvider $bestsellerProvider, ImageStorage $imageStorage)
	{
		parent::__construct();
		$this->imageStorage = $imageStorage;
		$this->bestsellerProvider = $bestsellerProvider;
	}

	public function render()
	{
		$product = $this->bestsellerProvider->getRandomBestsellerProduct();

		if ($product === null || $product === false) {
			return;
		}

		$this->template->product = $product;

		$this->template->imageStorage = $this->imageStorage;
		$this->template->setFile(__DIR__.'/product.latte');
		$this->template->render();
	}
}

interface IBestsellerProductFactory
{
	/**
	 * @return BestsellerProduct
	 */
	public function create();
}