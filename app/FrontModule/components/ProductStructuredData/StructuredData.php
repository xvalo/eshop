<?php

namespace App\FrontModule\Components\ProductStructuredData;

use App\File\Image;
use App\Product\Availability\AvailabilityProvider;
use App\Product\Price\Price;
use App\Product\Product;
use App\Product\Variant;
use Nette\Application\LinkGenerator;
use Nette\Application\UI\Control;
use Nette\Utils\Strings;
use Spatie\SchemaOrg\ItemAvailability;
use Spatie\SchemaOrg\Offer;
use Spatie\SchemaOrg\Schema;

class StructuredData extends Control
{
	/**
	 * @var Product
	 */
	private $product;

	/**
	 * @var LinkGenerator
	 */
	private $linkGenerator;

	/**
	 * @var AvailabilityProvider
	 */
	private $availabilityProvider;

	public function __construct(
		Product $product,
		LinkGenerator $linkGenerator,
		AvailabilityProvider $availabilityProvider
	){
		parent::__construct();

		$this->product = $product;
		$this->linkGenerator = $linkGenerator;
		$this->availabilityProvider = $availabilityProvider;
	}

	public function render()
	{
		$this->template->schemaScript = $this->createSchema();

		$this->template->setFile(__DIR__ . '/structuredData.latte');
		$this->template->render();
	}

	/**
	 * @return string
	 * @throws \Nette\Application\UI\InvalidLinkException
	 */
	private function createSchema(): string
	{
		$productSchema = Schema::product();
		$productSchema->name($this->product->getName())
			->description(strip_tags(html_entity_decode($this->product->getDescription())))
			->sku($this->product->getNumber());

		if ($this->product->getProducer()) {
			$productSchema->brand($this->product->getProducer()->getName());
		}

		$images = [];

		/** @var Image $image */
		foreach ($this->product->getImages() as $image) {
			$images[] = $this->linkGenerator->link('Front:Homepage:default') . ltrim($image->getRelativePath(), '/');
		}

		$productSchema->image( $images );

		$offers = [];
		$category = $this->product->getCategory();

		/** @var Variant $variant */
		foreach ($this->product->getPriceVariants() as $variant) {
			/** @var Price $price */
			foreach ($variant->getPrices() as $price) {
				$offer = new Offer();
				$offer->name(
					trim($variant->getName() . ' ' . $price->getName())
				);

				$offer->availability(
					$this->getItemAvailability($variant)
				);

				$offer->price($price->getPrice());
				$offer->priceCurrency('CZK');

				$url = $this->product->hasMorePrices()
					? $this->linkGenerator->link('Front:Product:default', ['category' => $category->getUrl(), 'url' => $this->product->getUrl(), 'variant' => Strings::webalize($variant->getName()) ,'variantId' => $variant->getId()])
					: $this->linkGenerator->link('Front:Product:default', ['category' => $category->getUrl(), 'url' => $this->product->getUrl()]);

				$offer->url($url);

				$offers[] = $offer;
			}
		}

		$productSchema->offers( $offers );

		return $productSchema->toScript();
	}

	/**
	 * @param Variant $productVariant
	 * @return string
	 */
	private function getItemAvailability(Variant $productVariant): string
	{
		if ($productVariant->getAvailability()->getId() === $this->availabilityProvider->getOnStockStatus()->getId()) {
			return ItemAvailability::InStock;
		} elseif ($productVariant->getAvailability()->isProductOrderable()) {
			return ItemAvailability::OnlineOnly;
		}

		return ItemAvailability::OutOfStock;
	}
}

interface IStructuredDataFactory
{
	/**
	 * @param Product $product
	 * @return StructuredData
	 */
	public function create(Product $product);
}