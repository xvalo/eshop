<?php

namespace App\FrontModule\Components;

use App\Cart\CartManager;
use App\User\CustomerManager;
use Nette;

class SignInForm extends Nette\Application\UI\Control
{

    /**
     * @var CustomerManager
     */
    private $manager;

    public function __construct(CustomerManager $manager)
    {
        $this->manager = $manager;
    }

    protected function createComponentSignInForm()
    {
        $form = new Nette\Application\UI\Form;

        $form->addText('email')
            ->setRequired('Tuto položku musíte vyplnit');

        $form->addPassword('password')
            ->setRequired('Tuto položku musíte vyplnit');

        $form->addSubmit('send');

        $manager = $this->manager;

        $form->onSuccess[] = function (Nette\Application\UI\Form $form) use ($manager){
            $presenter = $this->presenter;
            try {
                //$this->backlink = $presenter->storeRequest();
                $values = $form->getValues();

                $manager->authenticate([$values->email, $values->password]);

                $presenter->getUser()->setExpiration(new \DateTime('+ 2 week'), false);

                # maintain cart session association
                if($presenter->getCartManager()->hasItems()){
                    $currentSessionId = $presenter->getSession()->getId();
                    $cartSession = $presenter->getSession()->getSection(CartManager::CART_TRANSITION_SESSION);
                    $cartSession->oldSessionId = $currentSessionId;
                }

	            if ($presenter->isAjax()) {
		            $this->parent->payload->userLoggedIn = true;
	            } else {
                	$this->redirect('Homepage:default');
	            }

            } catch (Nette\Security\AuthenticationException $e) {
                $form->addError($e->getMessage());
            }

            if ($presenter->isAjax()) {
	            $this->parent->redrawControl('header-user-menu');
                $this->redrawControl('login-form');
                $this->redrawControl('login-form-errors');
            }
        };
        return $form;
    }

    public function render()
    {
        $this->template->setFile(__DIR__.'/SignInForm.latte');

        $this->template->registrationForm = $this['signInForm'];

        $this->template->render();
    }
}

interface ISignInFormFactory
{
    /** @return SignInForm */
    function create();
}