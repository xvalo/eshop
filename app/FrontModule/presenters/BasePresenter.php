<?php

namespace App\FrontModule\Presenters;

use App\Category\CategoryService;
use App\Components\SearchForm;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\FrontModule\Components\Bestseller\IBestsellerProductFactory;
use App\FrontModule\Components\ICategoryMenuFactory;
use App\FrontModule\Components\ISignInFormFactory;
use App\FrontModule\Components\OpenGraphData\IOpenGraphFactory;
use App\FrontModule\Components\Pages\IPagesListFactory;
use App\FrontModule\Components\TwitterCardData\ITwitterCardFactory;
use App\Lists\ListsService;
use App\Cart\CartManager;
use App\User\CustomerManager;
use App\Model\Services\ApplicationConfigurationService;
use App\Product\Availability\AvailabilityProvider;
use App\Product\ProductService;
use Nette;
use Ublaboo\ImageStorage\ImageStoragePresenterTrait;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    use ImageStoragePresenterTrait;

    /** @persistent */
    public $backlink = '';

    /** @var ICategoryMenuFactory @inject */
    public $categoryMenuFactory;

    /** @var ListsService @inject */
    public $listsService;

    /** @var  CategoryService @inject */
    public $categoryService;

    /** @var  ISignInFormFactory @inject */
    public $signInFormFactory;

    /** @var CustomerManager @inject */
    public $customerManager;

    /** @var ProductService @inject */
    public $productService;

    /** @var ApplicationConfigurationService @inject */
    public $applicationConfigurationService;

    /** @var ApplicationSettingsManager @inject */
    public $applicationSettingsManager;

    /** @var AvailabilityProvider @inject */
    public $availabilityProvider;

    /** @var IOpenGraphFactory @inject */
    public $openGraphFactory;

    /** @var ITwitterCardFactory @inject */
    public $twitterCardFactory;

    /** @var IPagesListFactory @inject */
    public $pagesListFactory;

    /** @var IBestsellerProductFactory @inject */
    public $bestsellerFactory;

    /** @var CartManager @inject */
    public $cartManager;

    /** @var \stdClass */
    public $cartTemplateData;

    protected $activeMenu;

    protected function startup()
    {
        parent::startup();
    }

    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->webTitle = $this->applicationSettingsManager->getWebsiteTitle();
        $this->template->mainEmail = $this->applicationSettingsManager->getMainEmail();
	    $this->template->domain = $this->applicationSettingsManager->getDomain();

        # cart
        $this->setCartTemplateVariables();

        $this->template->cartData = $this->cartTemplateData;

        $this->template->leftMenuVisible = $this->isLeftMenuVisible();
    }

    /**
     * User fo Front module
     * @return \Nette\Security\User
     */
    public function getUser()
    {
        $user = parent::getUser();
        $user->getStorage()->setNamespace(CustomerManager::USER_NAMESPACE);
        return $user;
    }

    public function getCartManager()
    {
        return $this->cartManager;
    }

    public function getBacklink()
    {
        return $this->backlink;
    }

    /**
     * @return \App\FrontModule\Components\CategoryMenuControl
     */
    protected function createComponentCategoryMenu()
    {
        return $this->categoryMenuFactory->create();
    }

    protected function createComponentSignInForm()
    {
        return $this->signInFormFactory->create();
    }

	/**
	 * @return SearchForm
	 */
    protected function createComponentSearchForm()
    {
        return new SearchForm();
    }

	protected function createComponentPagination()
	{
		$control = new \IPub\VisualPaginator\Components\Control(__DIR__.'/templates/components/pagination.latte');
		$control->disableAjax();

		return $control;
	}

	/**
	 * @return \App\FrontModule\Components\Pages\PagesList
	 */
	protected function createComponentPagesList()
	{
		return $this->pagesListFactory->create();
	}

	/**
	 * @return \App\FrontModule\Components\Bestseller\BestsellerProduct
	 */
	protected function createComponentBestseller()
	{
		return $this->bestsellerFactory->create();
	}

    public function setCartTemplateVariables()
    {
        $this->cartTemplateData = (object)[
            'count' => $this->cartManager->getTotalUnits(),
            'totalPrice' => $this->cartManager->getItemsTotalPrice(),
        ];
    }

    ////////////////// PRIVATE METHODS ////////////////////////

    private function isLeftMenuVisible()
    {
        return !$this->isLinkCurrent('Sign:*')
                && !$this->isLinkCurrent('Cart:*')
                && !$this->isLinkCurrent('Account:*')
                && !$this->isLinkCurrent('Error:*');
    }
}