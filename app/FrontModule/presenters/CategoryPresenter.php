<?php

namespace App\FrontModule\Presenters;

use App\Category\Entity\Category;
use App\Components\IAddProductToCartFactory;
use App\Components\IProductListFactory;
use App\Lists\ProducerRepository;
use App\Product\Listing\Filters\ProductFilter;
use App\Product\Listing\ProductsListingState;
use App\Product\Listing\ProductsProvider;
use Doctrine\ORM\NoResultException;
use Nette\Utils\Paginator;
use Nette\Utils\Strings;

class CategoryPresenter extends BasePresenter
{

	/** @var IProductListFactory @inject */
	public $productsListFactory;

	/** @var IAddProductToCartFactory @inject */
	public $addProductToCartFactory;

	/** @var Category */
	private $category;

	/** @var ProductsListingState */
	private $productsState;

	/** @var ProductsProvider @inject */
	public $productsProvider;

	/** @var ProducerRepository @inject */
	public $producerRepository;

	public function actionDefault($category)
	{
		$this->fixOldHandleActions($category);

		try {
			$this->category = $this->categoryService->getCategoryByUrl($category);
		} catch (NoResultException $e) {
			$this->error('Kategorie nebyla nalezena.');
		}

		if (!$this->category->isActive()) {
			$this->redirect('Homepage:default');
		}

		if ($this->category->getUrl() != $category) {
			$this->redirect(301, 'Category:default', ['category' => $this->category->getUrl()]);
		}

		$filters = $this->getParameter('filters', false);
		$filters = $filters ? explode(',', $filters) : [];
		$producers = $this->getParameter('producers', false);
		$producers = $producers ? explode(',', $producers) : [];

		$this->productsState = $this->productsProvider->getCategoryProducts(
			$this['pagination']->getPaginator()->getPage(),
			$this->getParameter('sortBy', ProductFilter::SORT_DEFAULT),
			$this->category->getId(),
			$this->getParameter('specialOffer', false),
			$this->getParameter('onlyAvailable', false),
			$filters,
			$producers,
			[
				'min' => $this->getParameter('minPrice', null),
				'max' => $this->getParameter('maxPrice', null)
			]
		);

		/** @var Paginator $paginator */
		$paginator = $this['pagination']->getPaginator();
		$paginator->setItemCount( $this->productsState->getPagination()->getItemsCount() );
		$paginator->setItemsPerPage( $this->productsState->getPagination()->getItemsPerPage() );
	}

	public function renderDefault($category)
	{
		$this->template->category = $this->category;
		$this->template->childrenCategories = $this->categoryService->getChildrenCategories($this->category);
		$this->template->products = $this->productsState->getProducts();
		$this->template->availabilityList = $this->availabilityProvider->getList();
		$this->template->pagination = $this->productsState->getPagination();

		/** @var Paginator $paginator */
		$paginator = $this['pagination']->getPaginator();

		$this->template->prevLink = $paginator->getPage() > 1 ? $this->link('//Category:default', ['category' => $category, 'pagination-page' => $paginator->getPage() - 1]) : null;
		$this->template->nextLink = $paginator->getPage() < $paginator->getPageCount() ? $this->link('//Category:default', ['category' => $category, 'pagination-page' => $paginator->getPage() + 1]) : null;
		$this->template->paginationPage = $paginator->getPage();
		$this->template->availableProducers = $this->producerRepository->getListByCategory($this->category);
	}

	private function fixOldHandleActions(string $category)
	{
		$oldHandleLink = $this->getHttpRequest()->getQuery('do');

		if ($oldHandleLink
			&& (
				Strings::contains($oldHandleLink, 'productsList-visualPaginator')
				|| Strings::contains($oldHandleLink, 'productsList-addProductToBasket')
				|| Strings::contains($oldHandleLink, 'visualPaginator-showPage')
			)
		) {
			/** @var Paginator $paginator */
			$paginator = $this['pagination']->getPaginator();
			$this->redirect(301, 'Category:default', ['category' => $category, 'pagination-page' => $paginator->getPage()]);
		}
	}
}