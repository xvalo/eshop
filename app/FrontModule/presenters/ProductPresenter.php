<?php

namespace App\FrontModule\Presenters;

use App\Category\Entity\Category;
use App\Components\IAddProductToCartFactory;
use App\Components\IProductListFactory;
use App\Core\Socials\TwitterCard\TwitterCardDataObject;
use App\FrontModule\Components\OpenGraphData\OpenGraphDataObject;
use App\FrontModule\Components\ProductStructuredData\IStructuredDataFactory;
use App\Product\Product;
use App\WatchDog\WatchDogService;
use Doctrine\ORM\NoResultException;

class ProductPresenter extends BasePresenter
{
    /** @var WatchDogService @inject */
    public $watchDogService;

    /** @var IProductListFactory @inject */
    public $productsListFactory;

    /** @var IAddProductToCartFactory @inject */
    public $addProductToCartFactory;

    /** @var IStructuredDataFactory @inject */
    public $structuredDataFactory;

    /** @var Category */
    private $category;

    /** @var Product */
    private $product;

    public function actionDefault($category, $url)
    {

        try {
        	$this->category = $this->categoryService->getCategoryByUrl($category);
            $this->product = $this->productService->getProductByUrl($url, $this->category);
        } catch (NoResultException $e) {
            $this->error('Produkt nebyl nalezen.');
        }

        if (!$this->product->isActive()) {
            $this->redirect('Category:default', [$this->product->getCategory()->getUrl()]);
        }

        if ($this->product->getUrl() != $url || $this->product->getCategory()->getUrl() != $category) {
            $this->redirect(301, 'Product:default', ['category' => $this->product->getCategory()->getUrl(), 'url' => $this->product->getUrl()]);
        }

        $this->setProductBreadcrumbs();
    }

    public function renderDefault()
    {
        $this->template->product = $this->product;
    }

    protected function createComponentAddProductToCart($name = null)
    {
    	return $this->addProductToCartFactory->create($this->product, $this, $name);
    }

	/**
	 * @return \App\FrontModule\Components\ProductStructuredData\StructuredData
	 */
	protected function createComponentStructuredData(): \App\FrontModule\Components\ProductStructuredData\StructuredData
	{
		return $this->structuredDataFactory->create($this->product);
    }

	protected function createComponentOpenGraph()
	{
		return $this->openGraphFactory->create(
			new OpenGraphDataObject(
				$this->product->getName(),
				$this->link('//Product:default', ['category' => $this->category->getUrl(), 'url' => $this->product->getUrl()]),
				substr(strip_tags($this->product->getDescription()), 0, 300),
				$this->product->getImages()->toArray()
			)
		);
    }

	protected function createComponentTwitterCard()
	{
		return $this->twitterCardFactory->create(
			new TwitterCardDataObject(
				$this->product->getName(),
				html_entity_decode(substr(strip_tags($this->product->getDescription()), 0, 300)),
				$this->product->getMainImage() ? $this->link('//Homepage:default') . ltrim($this->product->getMainImage()->getRelativePath(), '/') : null
			)
		);
    }

    private function setProductBreadcrumbs()
    {
        $categoriesLine = $this->product->getCategoriesLine();
        $menuItem = null;

        foreach($categoriesLine as $category){
            if(is_null($menuItem)){
                $menuItem = $this['categoryMenu']->getMenu()->getItem($category->getId());
            }else{
                $menuItem = $menuItem->getItem($category->getId());
            }
        }

        $menuItem->addItem($this->product->getName(), ':Front:Product:default', $this->product->getUrlArgs())
                    ->setVisual(false);
    }
}