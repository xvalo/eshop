<?php

namespace App\FrontModule\Presenters;

use App\Cart\Entity\Item;
use App\Classes\Order\Creation\Factory\OrderPartsFactory;
use App\Classes\Order\Creation\OrderCreationService;
use App\Customer\Customer;
use App\Customer\CustomerService;
use App\Lists\Delivery;
use App\Order\Delivery\CzechPostService;
use App\Order\Delivery\DeliveryService;
use App\FrontModule\Forms\ICartFormFactory;
use App\Mailing\MailingService;
use App\Cart\CartManager;
use App\Mailing\EmailManager;
use App\Order\OrderService;
use App\Order\Payment\Entity\PaymentType;
use App\Page\PageFacade;
use Nette\Application\UI\Form;
use Nette\Http\SessionSection;
use Tracy\Debugger;

class CartPresenter extends BasePresenter
{
	const ORDER_FINAL_DATA_SECTION = 'orderFinalData';

	/** @var CartManager @inject */
	public $cartManager;

	/** @var ICartFormFactory @inject */
	public $cartStepOneFormFactory;

	/** @var OrderService @inject */
	public $orderService;

	/** @var CustomerService @inject */
	public $customerService;

	/** @var EmailManager @inject */
	public $emailManager;

	/** @var MailingService @inject */
	public $mailingService;

	/** @var OrderCreationService @inject */
	public $orderCreationService;

	/** @var DeliveryService @inject */
	public $deliveryService;

	/** @var OrderPartsFactory @inject */
	public $orderPartsFactory;

	/** @var CzechPostService @inject */
	public $czechPostService;

	/** @var PageFacade @inject */
	public $pageFacade;

	/** @var SessionSection */
	private $cartExtraItemsSection;

	/** @var SessionSection */
	private $orderFinalizedSection;

	private $deliveryTypes;
	private $paymentTypes = [];

	private $formDefaults = [];

    protected function startup()
    {
        parent::startup();

        $this->cartExtraItemsSection = $this->getSession(CartManager::CART_EXTRA_ITEMS_SESSION);
        $this->orderFinalizedSection = $this->getSession(self::ORDER_FINAL_DATA_SECTION);
    }

	public function actionDefault()
	{
		$this['categoryMenu']->getMenu()->addItem('Košík', ':Front:Cart:default')
			->setVisual(false);

		if (count($this->cartManager->getItems()) == 0) {
			$this->forward('noItems');
			$this->terminate();
		}

		$this->setDeliveryTypes();

		if ($deliveryId = $this->getHttpRequest()->getPost('delivery')) {
			$this->cartManager->setDelivery((int)$deliveryId);
			$this->cartManager->updateCartState();
		}

		/** @var Delivery $deliveryObject */
		$deliveryObject = $this->cartManager->getDelivery()?:$this->deliveryService->getDelivery((int) key($this->deliveryTypes));
		$this->paymentTypes = $deliveryObject->getPaymentTypesList(true, $this->cartManager->getItemsTotalPrice());


		if($this->getUser()->isLoggedIn()){
			/** @var Customer $customer */
			$customer = $this->customerService->getOne($this->getUser()->getId());
			$invoiceAddress = $customer->getInvoiceInformation();
			$deliveryAddress = $customer->getDeliveryAddress();
			$this->formDefaults['contactInformation'] = [
				'name' => $invoiceAddress->getName(),
				'lastName' => $invoiceAddress->getLastName(),
				'email' => $customer->getEmail(),
				'phone' => $customer->getPhone()
			];
			$this->formDefaults['invoiceInformation'] = [
				'street' => $invoiceAddress->getStreet(),
				'city' => $invoiceAddress->getCity(),
				'zip' => $invoiceAddress->getZip(),
				'country' => $invoiceAddress->getCountry(),
				'company' => $invoiceAddress->getCompany(),
				'ico' => $invoiceAddress->getIco(),
				'dic' => $invoiceAddress->getDic()
			];
			$this->formDefaults['deliveryAddress'] = [
				'name' => $deliveryAddress->getName(),
				'lastName' => $deliveryAddress->getLastName(),
				'street' => $deliveryAddress->getStreet(),
				'city' => $deliveryAddress->getCity(),
				'zip' => $deliveryAddress->getZip(),
				'country' => $deliveryAddress->getCountry()
			];
		}
	}

	public function renderDefault()
	{
		$this->template->cartItems = $this->cartManager->getItems();
		$this->template->itemsTotalPrice = $this->cartManager->getItemsTotalPrice();
		$this->template->totalPrice = $this->cartManager->getTotalPrice();
		$this->template->itemsTotalPriceWithoutVat = $this->cartManager->getTotalPriceWithoutVat();
		$this->template->deliveries = $this->deliveryTypes;
		$this->template->paymentTypes = $this->paymentTypes;
		$this->template->selectedDeliveryId = $this->cartManager->getDelivery() ? $this->cartManager->getDelivery()->getId() : null;

		$this->template->termsConditionLink = $this->link('Page:default', [$this->pageFacade->getPageByUrl('obchodni-podminky')->getUrl()]);
		$this->template->gdprLink = $this->link('Page:default', [$this->pageFacade->getPageByUrl('ochrana-osobnich-udaju')->getUrl()]);
	}

	public function renderOrderCreated()
	{
		$this->template->orderNo = $this->orderFinalizedSection->orderNo;
		$this->template->totalPrice = (float)$this->orderFinalizedSection->totalPrice;
	}


    public function handleRemoveItem($id)
    {
        $this->cartManager->removeItem($id);
	    $this->setDeliveryTypes();

        if($this->isAjax()){
            $this->redrawCart();
        }else{
            $this->redirect('this');
        }
    }

	public function handleFindBox($like)
	{
		$boxes = $this->czechPostService->getBoxes($like);
		$this->payload->boxes = $boxes;
		$this->sendPayload();
	}

	public function handleFindOffice($like)
	{
		$offices = $this->czechPostService->getOffices($like);
		$this->payload->offices = $offices;
		$this->sendPayload();
	}

	protected function createComponentCartForm($name = null)
	{
		$form = $this->cartStepOneFormFactory->create($this, $name, $this->deliveryTypes, $this->paymentTypes);

		$form->onSuccess[] = function(Form $form){
			$values = $form->getValues(true);

			if ($values['delivery'] !== null) {
				$this->cartManager->setDelivery($values['delivery']);
			}
			if ($values['paymentType'] !== null) {
				$this->cartManager->setPaymentType($values['paymentType']);
			}
			$this->cartManager->updateItemsCount($values['items']);

			if($form['submit']->isSubmittedBy()){

				$specialDelivery = null;

				if ($this->cartManager->getDelivery()->getId() === $this->applicationConfigurationService->getCzechPostOfficeDeliveryBinding() && $this->getHttpRequest()->getPost('cp-office')) {
					$specialDelivery = $this->czechPostService->getOffice((int)$this->getHttpRequest()->getPost('cp-office'));
				} elseif ($this->cartManager->getDelivery()->getId() == $this->applicationConfigurationService->getCzechPostBoxDeliveryBinding() && $this->getHttpRequest()->getPost('cp-box')) {
					$specialDelivery = $this->czechPostService->getBox((int)$this->getHttpRequest()->getPost('cp-box'));
				}

				Debugger::log("DELIVERY DEBUG - EMAIL: {$values['contactInformation']['email']}; ID:{$this->cartManager->getDelivery()->getId()}; SPEC. DELIVERY: {$this->getHttpRequest()->getPost('cp-office')}|{$this->getHttpRequest()->getPost('cp-box')}", Debugger::DEBUG);

				$this->processOrderCreation(
					$values,
					$this->cartManager->getDelivery(),
					$this->cartManager->getPaymentType(),
					$this->getProductsInCart(),
					$specialDelivery
				);
				$this->payload->redirectUrl = $this->link('Cart:orderCreated');
				$this->sendPayload();
				$this->terminate();
			}

			$this->cartManager->updateCartState();
			$this->redrawCart();
		};

		if ($this->cartManager->getDelivery() === null) {
			$deliveryId = key($this->deliveryTypes);
			$this->cartManager->setDelivery((int)$deliveryId);
			$this->cartManager->updateCartState();
		} else {
			$deliveryId = $this->cartManager->getDelivery()->getId();
			if (!array_key_exists($deliveryId, $this->deliveryTypes)) {
				$deliveryId = key($this->deliveryTypes);
			}
		}

		if (!$this->cartManager->getPaymentType() || !$this->cartManager->getDelivery()->isPaymentTypeAttached($this->cartManager->getPaymentType())) {
			$paymentTypeId = key($this->paymentTypes);
			$this->cartManager->setPaymentType((int)$paymentTypeId);
			$this->cartManager->updateCartState();
		} else {
			$paymentTypeId = $this->cartManager->getPaymentType()->getId();
		}

		$this->formDefaults = [
			'delivery' => $deliveryId,
			'paymentType' => $paymentTypeId
		];

		$form->setDefaults($this->formDefaults);

		return $form;
	}

    private function redrawCart()
    {
	    if(!$this->cartManager->hasItems()) {
		    $this->payload->redirectUrl = $this->link('Cart:default');
	    }

        $this->redrawControl('cart-form');
        $this->redrawControl('items');
        $this->redrawControl('itemsPrice');
        $this->redrawControl('services');
	    $this->redrawControl('delivery');
	    $this->redrawControl('paymentType');
        $this->redrawControl('totalPrice');

        $this->redrawControl('cart-header-info');
    }

	private function processOrderCreation(array $formData, Delivery $delivery, PaymentType $paymentType, array $products, $specialDelivery = null)
	{
		if(empty($products)) {
			$this->flashMessage('Nemáte v košíku žádné produkty. Přidejte si do košíku nějaké produkty před odesláním objednávky.');
			$this->redirect('this');
		}

		$contactInformationData = $formData['contactInformation'];
		$deliveryAddressData = $formData['deliveryAddress'];
		$invoiceInformationData = $formData['invoiceInformation'];
		unset($formData['contactInformation'], $formData['deliveryAddress'], $formData['invoiceInformation']);

		$deliveryAddress = ($deliveryAddressData['deliverySame']) ? $deliveryAddressData : $invoiceInformationData + $contactInformationData;
		/** @var Customer $customer */
		$customer = $this->getUser()->isLoggedIn() ? $this->customerService->getOne($this->getUser()->getId()) : null;

		$order = $this->orderCreationService->createEshopOrder(
			$this->orderPartsFactory->createContactInformation($contactInformationData['name'], $contactInformationData['lastName'], $contactInformationData['email'], $contactInformationData['phone']),
			$this->orderPartsFactory->createInvoiceInformation($invoiceInformationData['company'], $invoiceInformationData['ico'], $invoiceInformationData['dic'], $invoiceInformationData['street'], $invoiceInformationData['city'], $invoiceInformationData['zip'], $invoiceInformationData['country']),
			$this->orderPartsFactory->createDeliveryAddress($deliveryAddress['name'], $deliveryAddress['lastName'], $deliveryAddress['company'], $deliveryAddress['street'], $deliveryAddress['city'], $deliveryAddress['zip'], $deliveryAddress['country']),
			$products,
			$delivery,
			$paymentType,
			$customer,
			$formData['customerNote'],
			null,
			$specialDelivery
		);

		if (!$formData['declineMailing']) {
			$this->mailingService->setMailing($contactInformationData['email'], true, $formData['mailing'], $formData['ads']);
		}

		$this->cartManager->clearCart();
		$this->emailManager->sendOrderConfirmationEmail($order);
		$this->orderFinalizedSection->orderNo = $order->getOrderNo();
		$this->orderFinalizedSection->totalPrice = $order->getTotalPrice();
	}

	private function getProductsInCart()
	{
		$variantsToAdd = [];
		/** @var Item $item */
		foreach($this->cartManager->getItems() as $item){
			$variantsToAdd[$item->getHashKey()] = [
				'id' => $item->getPrice()->getId(),
				'count' => $item->getCount(),
				'attributes' => array_map(function($data){
					return $data->getProductAttributeVariant();
				}, $item->getAttributeVariants()->toArray())
			];
		}

		return $variantsToAdd;
	}

	private function setDeliveryTypes()
	{
		$categories = [];

		/** @var Item $item */
		foreach ($this->cartManager->getItems() as $item) {
			$category = $item->getPrice()->getVariant()->getProduct()->getCategory();
			if (!array_key_exists($category->getId(), $categories)) {
				$categories[$category->getId()] = $category;
			}
		}

		$this->deliveryTypes = $this->deliveryService->getCartAvailableDeliveries($categories, $this->cartManager->getItemsTotalPrice());
	}

}