<?php

namespace App\FrontModule\Presenters;

use App\News\Entity\News;
use App\News\NewsService;
use Nette\Utils\Strings;

class NewsPresenter extends BasePresenter
{
	/** @var NewsService @inject */
	public $newService;

	/** @var News */
	private $news;

	public function actionDefault()
	{
		$this['categoryMenu']->getMenu()->addItem('Články', 'News:default')->setVisual(false);
	}

	public function renderDefault()
	{
		$list = $this->newService->getNewsList();

		if (count($list) === 0) {
			$this->setView('noNews');
		}

		$this->template->list = $list;
	}

	public function actionDetail($id, $url)
	{
		if ($id === null) {
			$this->news = $this->newService->getNewsByUrl($url);

			if (!$this->news) {
				$this->error('Článek nebyl nalezen');
			}

			$this->redirect(301, 'News:detail', ['id' => $this->news->getId(), 'url' => $this->news->getUrl()]);
		}

		$this->news = $this->newService->getNewsById($id);

		if (!$this->news) {
			$this->error('Článek nebyl nalezen');
		}

		if (!Strings::compare($this->news->getUrl(), $url)) {
			$this->redirect(301, 'News:detail', ['id' => $this->news->getId(), 'url' => $this->news->getUrl()]);
		}

		$this['categoryMenu']->getMenu()
			->addItem('Články', 'News:default')
			->addItem($this->news->getTitle(), 'News:detail', ['url' => $this->news->getUrl()])
			->setVisual(false);
	}

	public function renderDetail()
	{
		$this->template->news = $this->news;
	}
}