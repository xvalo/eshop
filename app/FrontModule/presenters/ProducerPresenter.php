<?php

namespace App\FrontModule\Presenters;

use App\Lists\Producer;
use App\Product\Listing\Filters\ProductFilter;
use App\Product\Listing\ProductsListingState;
use App\Product\Listing\ProductsProvider;
use Nette\Utils\Json;
use Nette\Utils\Paginator;
use Nette\Utils\Strings;

class ProducerPresenter extends BasePresenter
{

    /** @var Producer */
    private $producer;

	/** @var ProductsProvider @inject */
	public $productsProvider;

	/** @var ProductsListingState */
	private $productsState;

    public function actionDefault()
    {
        $this['categoryMenu']->getMenu()->addItem('Výrobci', 'Producer:default')->setVisual(false);
    }

    public function renderDefault()
    {
        $this->template->groups = $this->listsService->getProducersAlphabeticalGroups();
    }

    public function actionDetail($url)
    {
    	$this->fixOldHandleActions($url);

        $this->producer = $this->listsService->getProducerByUrl($url);

        if (!isset($this->producer)) {
            $this->redirect(301, 'Producer:default');
        }

	    $this->productsState = $this->productsProvider->getProducerProducts(
		    (int)$this['pagination']->getPaginator()->getPage(),
		    $this->producer,
		    $this->getParameter('sortBy', ProductFilter::SORT_DEFAULT),
		    $this->getParameter('specialOffer', false),
		    $this->getParameter('onlyAvailable', false)
	    );

	    /** @var Paginator $paginator */
	    $paginator = $this['pagination']->getPaginator();
	    $paginator->setItemCount( $this->productsState->getPagination()->getItemsCount() );
	    $paginator->setItemsPerPage( $this->productsState->getPagination()->getItemsPerPage() );
    }

    public function renderDetail($url)
    {
        $this->template->producer = $this->producer;
        $this->template->producerData = Json::encode([
        	[ 'id' => $this->producer->getId(), 'label' => $this->producer->getName() ]
        ]);
	    $this->template->products = $this->productsState->getProducts();
	    $this->template->availabilityList = $this->availabilityProvider->getList();
	    $this->template->pagination = $this->productsState->getPagination();

	    /** @var Paginator $paginator */
	    $paginator = $this['pagination']->getPaginator();

	    $this->template->prevLink = $paginator->getPage() > 1 ? $this->link('//Producer:detail', ['url' => $url, 'pagination-page' => $paginator->getPage() - 1]) : null;
	    $this->template->nextLink = $paginator->getPage() < $paginator->getPageCount() ? $this->link('//Producer:detail', ['url' => $url, 'pagination-page' => $paginator->getPage() + 1]) : null;
	    $this->template->paginationPage = $paginator->getPage();
    }

	private function fixOldHandleActions($url)
	{
		$oldHandleLink = $this->getHttpRequest()->getQuery('do');

		if ($oldHandleLink
			&& (
				Strings::contains($oldHandleLink, 'productsList-visualPaginator')
				|| Strings::contains($oldHandleLink, 'productsList-addProductToBasket')
				|| Strings::contains($oldHandleLink, 'visualPaginator-showPage')
			)
		) {
			/** @var Paginator $paginator */
			$paginator = $this['pagination']->getPaginator();
			$this->redirect(301, 'Producer:detail', [ 'url' => $url,  'pagination-page' => $paginator->getPage()]);
		}
	}
}