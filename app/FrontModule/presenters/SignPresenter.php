<?php

namespace App\FrontModule\Presenters;

use App\Admin\Forms\SignFormFactory;
use App\Customer\CustomerService;
use App\FrontModule\Forms\IForgottenPasswordFactory;
use App\FrontModule\Forms\IRegistrationFormFactory;
use App\User\CustomerManager;
use App\Mailing\EmailManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Nette\Application\UI\Form;
use Nette\Http\IResponse;

class SignPresenter extends BasePresenter
{
	/** @var SignFormFactory @inject */
	public $factory;

	/** @var IRegistrationFormFactory @inject */
	public $registrationFormFactory;

	/** @var IForgottenPasswordFactory @inject */
	public $forgottenPasswordFactory;

	/** @var CustomerManager @inject */
	public $manager;

	/** @var CustomerService @inject */
	public $customerService;

	/** @var EmailManager @inject */
	public $emailManager;

	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('Byl/a jste odhlášen/a.');
		$this->redirect('Homepage:default');
	}

	public function actionAuth($hash)
	{
		if (!is_string($hash)) {
			$this->error('Neplatná adresa.', IResponse::S404_NOT_FOUND);
		}

        if (!$this->customerService->authenticateCustomer($hash)) {
            $this->error('Email už je potvrzený, nebo nebyl nalezen a nebo nastala chyba.', IResponse::S400_BAD_REQUEST);
        }

        $this->flashMessage('Potvrzení vašeho emailu proběhlo v pořádku. Nyní se můžete přihlásit.');
        $this->redirect('Homepage:default');
	}

	protected function createComponentRegistrationForm($name = null)
	{
		$form = $this->registrationFormFactory->create($this, $name);

		$form->onSuccess[] = function(Form $form){
			try {
				$values = $form->getValues(true);

				$invoiceInformation = $this->customerService->createInvoiceInformationObject($values['invoiceInformation']);
				$deliveryAddress = ($values['deliveryAddress']['differentAsInvoiceInformation'])
						? $this->customerService->createDeliveryAddressObject($values['deliveryAddress'])
						: $this->customerService->createDeliveryAddressObject($values['invoiceInformation']);
				$customer = $this->customerService->createCustomer($values['basic'], $invoiceInformation,
						$deliveryAddress);

				$this->emailManager->sendAuthorizationEmail($customer);

				$this->redirect('Homepage:default', ['registration-completed' => true]);
			} catch (UniqueConstraintViolationException $e) {
				$this->flashMessage('Uživatel s tímto emailem je už zaregistrovaný. Pokud je toto váš email, ale ještě jste se neregistroval/a, obraťte se na nás.');
			}
		};

		return $form;
	}

    protected function createComponentRestorePasswordForm()
    {
        $form = $this->forgottenPasswordFactory->create();

		$form->onSuccess[] = function(Form $form){
			$customer = $this->customerService->getOneByEmail($form->getValues()->email);

			if(is_object($customer)) {
				if ($password = $this->customerService->restorePassword($customer)) {
					$this->emailManager->sendRestorePasswordEmail($customer, $password);
					$this->flashMessage('Obnova hesla proběhla v pořádku a nové heslo vám přišlo na email');
				} else {
					$this->flashMessage('Něco se pokazilo a nepodařilos e nám obnovit vaše heslo. Zkuste to znovu a nebo se obraťte na podporu. Děkujeme.');
				}
			} else {
				$this->flashMessage('Nepodařilo se nám najít zadaný email.');
				$this->redirect('Sign:restorePassword');
			}

			$this->redirect('Homepage:default');
		};

		return $form;
    }
}