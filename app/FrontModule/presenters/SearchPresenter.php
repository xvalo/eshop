<?php

namespace App\FrontModule\Presenters;

use App\Lists\ProducerRepository;
use App\Product\Listing\Filters\ProductFilter;
use App\Product\Listing\ProductsListingState;
use App\Product\Listing\ProductsProvider;
use App\Search\SearchService;
use Nette\Utils\Paginator;

class SearchPresenter extends BasePresenter
{
	/** @var SearchService @inject */
	public $searchService;

	/** @var ProductsProvider @inject */
	public $productsProvider;

	/** @var ProductsListingState */
	private $productsState;

	/** @var ProducerRepository @inject */
	public $producerRepository;

	public function actionDefault($q)
	{
		if (!is_string($q)) {
			$this->error('Pro vyhledávání musíte zadat vyhledávaná klíčová slova.', IResponse::S400_BAD_REQUEST);
		}

		$this->searchService->saveSearchPhrase($q);

		$this->productsState = $this->productsProvider->getSearchProducts(
			(int)$this['pagination']->getPaginator()->getPage(),
			$q,
			$this->getParameter('sortBy', ProductFilter::SORT_DEFAULT),
			$this->getParameter('specialOffer', false),
			$this->getParameter('onlyAvailable', false)
		);

		/** @var Paginator $paginator */
		$paginator = $this['pagination']->getPaginator();
		$paginator->setItemCount( $this->productsState->getPagination()->getItemsCount() );
		$paginator->setItemsPerPage( $this->productsState->getPagination()->getItemsPerPage() );
	}

	public function renderDefault($q)
	{
		$this->template->products = $this->productsState->getProducts();
		$this->template->availabilityList = $this->availabilityProvider->getList();
		$this->template->availableProducers = '[]';

		$this->template->pagination = $this->productsState->getPagination();

		/** @var Paginator $paginator */
		$paginator = $this['pagination']->getPaginator();

		$this->template->prevLink = $paginator->getPage() > 1 ? $this->link('//Search:default', ['q' => $q, 'pagination-page' => $paginator->getPage() - 1]) : null;
		$this->template->nextLink = $paginator->getPage() < $paginator->getPageCount() ? $this->link('//Search:default', ['q' => $q, 'pagination-page' => $paginator->getPage() + 1]) : null;
		$this->template->paginationPage = $paginator->getPage();
		$this->template->searchPhrase = $q;
	}
}