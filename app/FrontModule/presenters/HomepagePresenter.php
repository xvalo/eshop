<?php

namespace App\FrontModule\Presenters;

use App\Mailing\MailingService;
use App\Product\Listing\ProductsProvider;
use App\Utils\StringEncryption;
use Nette\Application\UI\Form;

class HomepagePresenter extends BasePresenter
{
    /** @var MailingService @inject */
    public $mailingService;

    /** @var ProductsProvider @inject */
    public $productsProvider;

    public function renderDefault()
    {
        $this->template->authorized = $this->getHttpRequest()->getQuery('authorized', false);
        $this->template->registrationCompleted = $this->getHttpRequest()->getQuery('registration-completed', false);
        $this->template->products = $this->productsProvider->getHomepageProducts()->getProducts();
        $this->template->availabilityList = $this->availabilityProvider->getList();
    }

	public function actionContact()
	{
		$this['categoryMenu']->getMenu()->addItem('Kontakt', 'Homepage:contact')->setVisual(false);
	}

	public function actionMailingConfirmation($hash)
	{
		try {
			$this->template->email = urldecode(StringEncryption::decrypt($hash));
		} catch (\Exception $e) {
			$this->error('Něco se nepovedlo');
		}
    }

	protected function createComponentMailingConfirmationForm()
	{
		$form = new Form();

		$form->addCheckbox('newsletter');
		$form->addCheckbox('ads');
		$form->addCheckbox('declineMailing');
		$form->addHidden('email');

		$form->addSubmit('save');

		$form->onSuccess[] = function(Form $form) {
			$values = $form->getValues();
			if (!$values->declineMailing) {
				$this->mailingService->setMailing($values->email, false, $values->newsletter, $values->ads);
				$this->flashMessage('Změny bylu uloženy. Děkujeme za potvrzení.');
			} else {
				$this->flashMessage('Nadále už od nás nebudete ostávat newslettery.');
			}
			$this->redirect('Homepage:default');
		};

		return $form;
    }
}