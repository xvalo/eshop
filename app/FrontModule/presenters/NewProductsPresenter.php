<?php

namespace App\FrontModule\Presenters;

use App\Lists\ProducerRepository;
use App\Product\Listing\Filters\ProductFilter;
use App\Product\Listing\ProductsListingState;
use App\Product\Listing\ProductsProvider;
use Nette\Utils\Paginator;
use Nette\Utils\Strings;

class NewProductsPresenter extends BasePresenter
{
	/** @var ProductsProvider @inject */
	public $productsProvider;

	/** @var ProductsListingState */
	private $productsState;

	/** @var ProducerRepository @inject */
	public $producerRepository;

	public function actionDefault()
	{
		$this->fixOldHandleActions();

		$this->productsState = $this->productsProvider->getNewsProducts(
			(int)$this['pagination']->getPaginator()->getPage(),
			$this->getParameter('sortBy', ProductFilter::SORT_BY_LATEST),
			$this->getParameter('specialOffer', false),
			$this->getParameter('onlyAvailable', false)
		);

		/** @var Paginator $paginator */
		$paginator = $this['pagination']->getPaginator();
		$paginator->setItemCount( $this->productsState->getPagination()->getItemsCount() );
		$paginator->setItemsPerPage( $this->productsState->getPagination()->getItemsPerPage() );
	}

	public function renderDefault()
	{
		$this->template->products = $this->productsState->getProducts();
		$this->template->availabilityList = $this->availabilityProvider->getList();
		$this->template->availableProducers = '[]';

		$this->template->pagination = $this->productsState->getPagination();

		/** @var Paginator $paginator */
		$paginator = $this['pagination']->getPaginator();

		$this->template->prevLink = $paginator->getPage() > 1 ? $this->link('//NewProducts:default', ['pagination-page' => $paginator->getPage() - 1]) : null;
		$this->template->nextLink = $paginator->getPage() < $paginator->getPageCount() ? $this->link('//NewProducts:default', ['pagination-page' => $paginator->getPage() + 1]) : null;
		$this->template->paginationPage = $paginator->getPage();
	}

	private function fixOldHandleActions()
	{
		$oldHandleLink = $this->getHttpRequest()->getQuery('do');

		if ($oldHandleLink
			&& (
				Strings::contains($oldHandleLink, 'productsList-visualPaginator')
				|| Strings::contains($oldHandleLink, 'productsList-addProductToBasket')
				|| Strings::contains($oldHandleLink, 'visualPaginator-showPage')
			)
		) {
			/** @var Paginator $paginator */
			$paginator = $this['pagination']->getPaginator();
			$this->redirect(301, 'NewProducts:default', ['pagination-page' => $paginator->getPage()]);
		}
	}
}