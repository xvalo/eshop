<?php

namespace App\FrontModule\Presenters;


use App\Classes\Order\Customer\OrdersService;
use App\Customer\Customer;
use App\Customer\CustomerService;
use App\FrontModule\Components\IAccountMenuFactory;
use App\FrontModule\Forms\IChangePasswordFormFactory;
use App\FrontModule\Forms\IChangePersonalInfoFormFactory;
use App\InternalExceptions\IncorrectPasswordException;
use App\Order\Order;
use Nette\Application\UI\Form;
use Nette\Http\IResponse;
use Nette\Utils\ArrayHash;

class AccountPresenter extends BasePresenter
{
    /** @var CustomerService @inject */
    public $customerService;

    /** @var IAccountMenuFactory @inject */
    public $accountMenuFactory;

    /** @var IChangePasswordFormFactory @inject */
    public $changePasswordFormFactory;

    /** @var IChangePersonalInfoFormFactory @inject */
    public $changePersonalInfoFormFactory;

    /** @var OrdersService @inject */
    public $customerOrdersService;

    /** @var Customer */
    private $customer;

    /** @var Order */
    private $order;

    protected function startup()
    {
        parent::startup();

        if (!$this->getUser()->isLoggedIn()) {
        	$this->flashMessage('Pro přístup se musíte přihlásit');
        	$this->redirect('Homepage:default');
        }

        $this->customer = $this->customerService->getOne($this->getUser()->getId());
    }

    public function beforeRender()
    {
        parent::beforeRender();
        $this->setLayout('accountLayout');
    }

    public function renderMyAccount()
    {
        $this->template->customer = $this->customer;
    }

    public function actionBillingInfo()
    {

    }

    public function actionDeliveryAddresses()
    {

    }

	public function renderMyOrders()
	{
		$this->template->orders = $this->customerOrdersService->getCustomerOrders($this->customer);
    }

	public function actionOrderDetail($orderId)
	{
		$this->order = $this->customerOrdersService->getOrderDetail($this->customer, $orderId);

		if (!$this->order) {
			$this->error('Objednávka nebyla nalezena', IResponse::S404_NOT_FOUND);
		}

		$this['accountMenu']->getMenu()->addItem('Objednávky', 'Account:myOrders')->setVisual(false)
			->addItem('Objednávka č. '.$this->order->getOrderNo(), 'Account:orderDetail', ['orderId' => $this->order->getId()])->setVisual(false);

	}

	public function renderOrderDetail()
	{
		$this->template->order = $this->order;
    }

    /**
     * @return \App\FrontModule\Components\AccountMenuControl
     */
    protected function createComponentAccountMenu()
    {
        return $this->accountMenuFactory->create();
    }

    protected function createComponentChangePasswordForm()
    {
        $form = $this->changePasswordFormFactory->create();

        $form->onSuccess[] = function(Form $form, ArrayHash $values){
            try {
                $this->customerService->changePassword($this->customer, $values->oldPassword, $values->newPassword);
            } catch (IncorrectPasswordException $e) {
                $this->flashMessage('Zadal/a jste špatné staré heslo. Zadejte prosím správné.');
                $this->redirect('this');
            } catch (\Exception $e) {
                $this->flashMessage('Při ukládání změn došlo k chybě. Zkuste to prosím znovu, nebo nás prosím kontaktujte.');
                $this->redirect('this');
            }

            $this->flashMessage('Heslo bylo úspěšně změněno.');
            $this->redirect('myAccount');
        };

        return $form;
    }

    protected function createComponentChangePersonalInfoForm()
    {
        $form = $this->changePersonalInfoFormFactory->create();

        $form->onSuccess[] = function(Form $form){
            try {
                $data = $form->getValues(true);
                $this->customerService->update($this->customer, $this->getProcessedPersonalInfoData($data));
            } catch (\Exception $e) {
                $this->flashMessage('Při ukládání změn došlo k chybě. Zkuste to prosím znovu, nebo nás prosím kontaktujte.');
                $this->redirect('this');
            }

            $this->flashMessage('Údaje byly uloženy.');
            $this->redirect('myAccount');
        };

        $form->setDefaults($this->getPersonalInfoFormDefaults());

        return $form;
    }

    private function getPersonalInfoFormDefaults()
    {
        $invoiceInformation = $this->customer->getInvoiceInformation();
        $deliveryAddress = $this->customer->getDeliveryAddress();
        return [
            'basic' => [
                'email' => $this->customer->getEmail(),
                'phone' => $this->customer->getPhone()
            ],
            'invoiceInformation' => [
                'name' => $invoiceInformation->getName(),
                'lastName' => $invoiceInformation->getLastName(),
                'street' => $invoiceInformation->getStreet(),
                'city' => $invoiceInformation->getCity(),
                'zip' => $invoiceInformation->getZip(),
                'country' => $invoiceInformation->getCountry(),
                'company' => $invoiceInformation->getCompany(),
                'ico' => $invoiceInformation->getIco(),
                'dic' => $invoiceInformation->getDic()
            ],
            'deliveryAddress' => [
                'name' => $deliveryAddress->getName(),
                'lastName' => $deliveryAddress->getLastName(),
                'street' => $deliveryAddress->getStreet(),
                'city' => $deliveryAddress->getCity(),
                'zip' => $deliveryAddress->getZip(),
                'country' => $deliveryAddress->getCountry(),
                'company' => $deliveryAddress->getCompany()
            ]
        ];
    }

    private function getProcessedPersonalInfoData(array $data)
    {
        $data['phone'] = $data['basic']['phone'];
        $data['email'] = $data['basic']['email'];
        $data['name'] = $data['invoiceInformation']['name'];
        $data['lastName'] = $data['invoiceInformation']['lastName'];

        unset($data['save'], $data['basic']);

        return $data;
    }

    
}