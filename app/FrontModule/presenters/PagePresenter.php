<?php

namespace App\FrontModule\Presenters;

use App\Page\Entity\Page;
use App\Page\PageFacade;
use App\Page\PageService;

class PagePresenter extends BasePresenter
{
	/**
	 * @var PageService @inject
	 */
	public $pageService;

	/**
	 * @var PageFacade @inject
	 */
	public $pageFacade;

	/**
	 * @var Page
	 */
	private $page;

	public function actionDefault($page)
	{
		$this->page = $this->pageFacade->getPageByUrl($page);

		if (!$this->page->isActive()) {
			$this->error('Stránka nebyla nalezena');
		}
	}

	public function renderDefault()
	{
		$this->template->page = $this->page;

		$this['categoryMenu']->getMenu()
			->addItem($this->page->getTitle(), 'Page:default', ['page' => $this->page->getUrl()])
			->setVisual(false);
	}
}