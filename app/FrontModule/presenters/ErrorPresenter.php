<?php

namespace App\FrontModule\Presenters;

use Nette;
use Nette\Utils\Strings;
use Nette\Application\Request as AppRequest;

class ErrorPresenter extends BasePresenter
{
    public function startup()
    {
        parent::startup();
        if (!$this->getRequest()->isMethod(Nette\Application\Request::FORWARD)) {
            $this->error();
        }
    }

    public function renderDefault(Nette\Application\BadRequestException $exception, AppRequest $request)
    {
        $this->template->message = $exception->getMessage();
    }
}