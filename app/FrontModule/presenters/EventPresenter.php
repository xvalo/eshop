<?php

namespace App\FrontModule\Presenters;

use App\Event\Event;
use App\Event\EventService;

class EventPresenter extends BasePresenter
{
    /** @var EventService @inject */
    public $eventService;

    /** @var Event */
    private $event;

    public function actionDefault()
    {
        $this['categoryMenu']->getMenu()->addItem('Akce', 'Event:default')->setVisual(false);
    }

    public function renderDefault()
    {
	    $events = $this->eventService->getActiveEvents();

	    if (count($events) === 0) {
		    $this->setView('noEvents');
	    }

	    $this->template->events = $events;
    }

	public function actionDetail($id)
	{
		$this->event = $this->eventService->getOne($id);

		if (!$this->event) {
			$this->error('Událost nebyla nalezena.');
		}

		$this['categoryMenu']->getMenu()
				->addItem('Akce', 'Event:default')
				->addItem($this->event->getTitle(), 'Event:detail', ['id' => $this->event->getId()])
				->setVisual(false);
    }

	public function renderDetail()
	{
		$this->template->event = $this->event;
    }
}