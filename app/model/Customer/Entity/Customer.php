<?php

namespace App\Customer;

use App\Model\BaseEntity;
use App\Traits\ActivityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Customer extends BaseEntity
{
	use ActivityTrait;

	/**
	 * @ORM\Column(type="string", length=60)
	 */
	private $password;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $lastName;

	/**
	 * @ORM\Column(type="string", length=100, unique=true)
	 */
	private $email;

	/**
	 * @ORM\Column(type="string", length=15)
	 */
	private $phone;

	/**
	 * @ORM\OneToOne(targetEntity="\App\Customer\InvoiceInformation", cascade={"persist"})
	 * @ORM\JoinColumn(name="invoice_information_id", referencedColumnName="id", nullable=false)
	 */
	private $invoiceInformation;

	/**
	 * @ORM\OneToOne(targetEntity="\App\Customer\DeliveryAddress", cascade={"persist"})
	 * @ORM\JoinColumn(name="delivery_address_id", referencedColumnName="id", nullable=false)
	 */
	private $deliveryAddresses;

	/**
	 * @ORM\Column(type="string", length=60)
	 */
	private $authorizationHash;

	/**
	 * @ORM\OneToOne(targetEntity="\App\Mailing\Mailing", mappedBy="customer")
	 */
	private $mailing;

	/**
	 * @ORM\Column(type="string", length=20, nullable=true)
	 */
	private $lyoneesCardId;

	/**
	 * @ORM\Column(type="string", length=60, nullable=true)
	 */
	private $lyoneesCardEan;

	public function __construct($email, $password, $name, $lastName, $phone, InvoiceInformation $invoiceInformation, DeliveryAddress $deliveryAddress)
	{
		$this->password = $password;
		$this->name = $name;
		$this->lastName = $lastName;
		$this->email = $email;
		$this->active = false;
		$this->phone = $phone;
		$this->invoiceInformation = $invoiceInformation;
		$this->deliveryAddresses = $deliveryAddress;

		$this->authorizationHash = $this->generateAuthorizationHash();
	}

	public function unsetPassword()
	{
		unset($this->password);
		return;
	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 */
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;
	}

	public function getFullName()
	{
		return $this->name . " " . $this->lastName;
	}

	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	 * @return string
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * @param string $phone
	 */
	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	/**
	 * @return InvoiceInformation
	 */
	public function getInvoiceInformation()
	{
		return $this->invoiceInformation;
	}

	/**
	 * @return DeliveryAddress
	 */
	public function getDeliveryAddress()
	{
		return $this->deliveryAddresses;
	}

	public function getAuthorizationHash()
	{
		return $this->authorizationHash;
	}

	public function getMailing()
	{
		return $this->mailing;
	}

	/**
	 * @return string
	 */
	public function getLyoneesCardId()
	{
		return $this->lyoneesCardId;
	}

	/**
	 * @param string $lyoneesCardId
	 */
	public function setLyoneesCardId($lyoneesCardId)
	{
		$this->lyoneesCardId = $lyoneesCardId;
	}

	/**
	 * @return string
	 */
	public function getLyoneesCardEan()
	{
		return $this->lyoneesCardEan;
	}

	/**
	 * @param string $lyoneesCardEan
	 */
	public function setLyoneesCardEan($lyoneesCardEan)
	{
		$this->lyoneesCardEan = $lyoneesCardEan;
	}

	private function generateAuthorizationHash()
	{
		return md5(mt_rand(1,100) . time() . $this->getFullName() . $this->getEmail());
	}
}