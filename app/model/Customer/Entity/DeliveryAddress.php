<?php

namespace App\Customer;

use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="customer_delivery_address")
 */
class DeliveryAddress extends BaseEntity
{

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $lastName;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $company;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $street;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $city;

	/**
	 * @ORM\Column(type="string", length=10)
	 */
	private $zip;

	/**
	 * @ORM\Column(type="string", length=20)
	 */
	private $country;

	/**
	 * DeliveryAddress constructor.
	 * @param $name
	 * @param $lastName
	 * @param $company
	 * @param $street
	 * @param $city
	 * @param $zip
	 * @param $country
	 */
	public function __construct($name, $lastName, $company, $street, $city, $zip, $country)
	{
		$this->name = $name;
		$this->lastName = $lastName;
		$this->street = $street;
		$this->company = $company;
		$this->city = $city;
		$this->zip = $zip;
		$this->country = $country;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * @param string $lastName
	 */
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;
	}

	/**
	 * @return string
	 */
	public function getCompany()
	{
		return $this->company;
	}

	/**
	 * @param string $company
	 */
	public function setCompany($company)
	{
		$this->company = $company;
	}

	/**
	 * @return string
	 */
	public function getStreet()
	{
		return $this->street;
	}

	/**
	 * @param string $street
	 */
	public function setStreet($street)
	{
		$this->street = $street;
	}

	/**
	 * @return string
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity($city)
	{
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getZip()
	{
		return $this->zip;
	}

	/**
	 * @param string $zip
	 */
	public function setZip($zip)
	{
		$this->zip = $zip;
	}

	/**
	 * @return string
	 */
	public function getCountry()
	{
		return $this->country;
	}

	/**
	 * @param string $country
	 */
	public function setCountry($country)
	{
		$this->country = $country;
	}


}