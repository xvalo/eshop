<?php

namespace App\Customer;

use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="customer_invoice_information")
 */
class InvoiceInformation extends BaseEntity
{

    /**
     * @ORM\Column(type="string", length=100)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     * @var string
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @var string
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     * @var int
     */
    private $ico;

    /**
     * @ORM\Column(type="string", length=12, nullable=true)
     * @var string
     */
    private $dic;

    /**
     * @ORM\Column(type="string", length=100)
     * @var string
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=100)
     * @var string
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=10)
     * @var string
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=20)
     * @var string
     */
    private $country;

    /**
     * InvoiceInformation constructor.
     * @param $name
     * @param $lastName
     * @param $company
     * @param $ico
     * @param $dic
     * @param $street
     * @param $city
     * @param $zip
     * @param $country
     */
    public function __construct($name, $lastName, $company, $ico, $dic, $street, $city, $zip, $country)
    {
        $this->company = $company;
        $this->ico = $ico;
        $this->dic = $dic;
        $this->street = $street;
        $this->city = $city;
        $this->zip = $zip;
        $this->country = $country;
        $this->name = $name;
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return int
     */
    public function getIco()
    {
        return $this->ico;
    }

    /**
     * @param int $ico
     */
    public function setIco($ico)
    {
        $this->ico = $ico;
    }

    /**
     * @return string
     */
    public function getDic()
    {
        return $this->dic;
    }

    /**
     * @param string $dic
     */
    public function setDic($dic)
    {
        $this->dic = $dic;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }


}