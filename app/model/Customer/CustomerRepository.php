<?php

namespace App\Customer;


use App\Model\BaseRepository;

class CustomerRepository extends BaseRepository
{
    public function getCustomerByEmail($email)
    {
        return $this->findOneBy(['email' => $email]);
    }

    public function getGridData()
    {
        $qb = $this->em->createQueryBuilder()
                    ->select('C')
                    ->from('\App\Customer\Customer', 'C');

        return $qb;
    }

	public function getRegisteredCustomersCount()
	{
		return $this->repository->countBy(['active' => 1]);
    }

	public function getCustomersCount()
	{
		$qb = $this->em->createQueryBuilder()
				->select('COUNT(DISTINCT C.email)')
				->from('\App\Order\ContactInformation', 'C');

		return $qb->getQuery()->getSingleScalarResult();
    }

	public function searchCustomers($phrase)
	{
		$qb = $this->em->createQueryBuilder();

		$qb->select('C.id, C.name, C.lastName, C.email, C.phone')
			->from('\App\Customer\Customer', 'C');

		$where = $qb->expr()->andX(
			$qb->expr()->eq('C.active', ':activity'),
			$qb->expr()->orX(
				$qb->expr()->like('C.name', ':phrase'),
				$qb->expr()->like('C.lastName', ':phrase'),
				$qb->expr()->like('C.lyoneesCardId', ':phrase'),
				$qb->expr()->like('C.lyoneesCardEan', ':phrase'),
				$qb->expr()->like('C.email', ':phrase'),
				$qb->expr()->like('C.phone', ':phrase')
			)
		);

		$qb->where($where);

		$qb->setParameters([
			'activity' => true,
			'phrase' => '%'.$phrase.'%'
		]);

		return $qb->getQuery()->execute();
    }
}