<?php

namespace App\Customer;

use App\InternalExceptions\IncorrectPasswordException;
use App\InvalidArgumentException;
use App\Model\BaseEntity;
use App\Model\BaseService;
use App\Order\OrderRepository;
use App\Utils\Strings;
use Nette\Security\Passwords;
use Tracy\Debugger;
use Tracy\ILogger;

/**
 * Class CustomerService
 * @package App\Customer
 * @property CustomerRepository $repository
 */
class CustomerService extends BaseService
{
	/**
	 * @var OrderRepository
	 */
	private $orderRepository;

	/**
	 * CustomerService constructor.
	 * @param CustomerRepository $repository
	 */
	public function __construct(
		CustomerRepository $repository,
		OrderRepository $orderRepository
	){
		parent::__construct($repository);
		$this->orderRepository = $orderRepository;
	}

	public function getOneByEmail($email)
	{
		return $this->repository->findOneBy(['email' => $email, 'active' => true]);
	}

	public function createCustomer(
        array $basicData,
		InvoiceInformation $invoiceInformation,
		DeliveryAddress $deliveryAddress
	){
		$password = Passwords::hash($basicData['password']);
		$customer = new Customer($basicData['email'],
								$password,
								$invoiceInformation->getName(),
								$invoiceInformation->getLastName(),
								$basicData['phone'],
								$invoiceInformation,
								$deliveryAddress);

		$this->repository->persist($customer);
		$this->repository->flush();

		return $customer;
	}

	public function createDeliveryAddressObject(array $data)
	{
		return new DeliveryAddress(
				$data['name'],
				$data['lastName'],
				$data['company'],
				$data['street'],
				$data['city'],
				$data['zip'],
				$data['country']
		);
	}

	public function createInvoiceInformationObject(array $data)
	{
		return new InvoiceInformation(
				$data['name'],
				$data['lastName'],
				$data['company'],
				$data['ico'],
				$data['dic'],
				$data['street'],
				$data['city'],
				$data['zip'],
				$data['country']
		);
	}

	public function update($customer, array $data)
	{
		$this->setDataToEntity($customer, $data);
		$this->repository->flush();
		return $customer;
	}

    public function authenticateCustomer($hash)
    {
        /** @var Customer $customer */
        $customer = $this->repository->findOneBy(['authorizationHash' => $hash, 'active' => false]);

        if ($customer instanceof Customer) {
            try {
                $customer->activate();
                $this->repository->flush();
                return true;
            } catch (\Exception $e) {
                Debugger::log($e->getMessage(), ILogger::ERROR);
                return false;
            }
        }

        return false;
    }

	public function changePassword(Customer $customer, $oldPassword, $newPassword)
	{
		if (!Passwords::verify($oldPassword, $customer->getPassword())) {
			throw new IncorrectPasswordException;
		}

		$customer->setPassword(Passwords::hash($newPassword));
		$this->repository->flush();
	}

    public function restorePassword(Customer $customer)
    {
		try {
			$password = Strings::generateRandomString(8);
			$customer->setPassword(Passwords::hash($password));
			$this->repository->flush();

			return $password;
		} catch (\Exception $e) {
			Debugger::log($e->getMessage(), ILogger::ERROR);
			return false;
		}
    }

	public function getFormDefaults(Customer $customer)
	{
		return [
			'active' => $customer->isActive(),
			'lyoneesCardId' => $customer->getLyoneesCardId(),
			'lyoneesCardEan' => $customer->getLyoneesCardEan()
		];
    }

	public function findCustomers($phrase = null)
	{
		if (is_null($phrase)) {
			return [];
		}

		$data = [];
		foreach ($this->repository->searchCustomers($phrase) as $item) {
			$data[] = [
				'id' => 'C-'.$item['id'],
				'text' => $item['name'] . ' ' . $item['lastName'] . ' (' . $item['email'] . ' / ' . $item['phone'] . ')'
			];
		}

		foreach ($this->orderRepository->searchCustomers($phrase) as $item) {
			$data[] = [
				'id' => 'O-'.$item['id'],
				'text' => $item['name'] . ' ' . $item['lastName'] . ' (' . $item['email'] . ' / ' . $item['phone'] . ')'
			];
		}

		return $data;
    }

	private function setDataToEntity(Customer $entity, array $data)
	{
		foreach($data as $key => $value){
			if (is_array($value)) {
				$getter = 'get' . \Nette\Utils\Strings::firstUpper($key);
				$this->setDataToEntity($entity->{$getter}(), $value);
			} else {
				$setter = 'set' . \Nette\Utils\Strings::firstUpper($key);
				switch ($key) {
					case 'active':
						if ($value) {
							$entity->activate();
						} else {
							$entity->deactivate();
						}
						break;
					case 'mailing':
						break;
					default:
						$entity->{$setter}($value);
						break;
				}
			}
		}
	}

}