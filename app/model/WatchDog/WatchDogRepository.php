<?php

namespace App\WatchDog;

use App\Product\Availability\AvailabilityProvider;
use Kdyby\Doctrine\EntityManager;

class WatchDogRepository
{
	/** @var EntityManager */
	private $em;

	/** @var AvailabilityProvider */
	private $availabilityProvider;

	public function __construct(
		EntityManager $em,
		AvailabilityProvider $availabilityProvider
	){
		$this->em = $em;
		$this->availabilityProvider = $availabilityProvider;
	}

	public function saveWatchDogItem(WatchDog $item)
	{
		if ($item->getId() === null) {
			$this->em->persist($item);
		}

		$this->em->flush();
	}

    public function getGridData()
    {
        $qb = $this->em->createQueryBuilder()
                    ->select('W')
                    ->from(WatchDog::class, 'W');

        $qb->groupBy('W.priceVariant');

        return $qb;
    }

    public function getItemsToNotify()
    {
        $qb = $this->em->createQueryBuilder()
                ->select('W')
                ->from(WatchDog::class, 'W')
                    ->leftJoin('W.priceVariant', 'P');

        $where = $qb->expr()->andX(
            $qb->expr()->isNull('W.sent'),
            $qb->expr()->lt('P.availability', $this->availabilityProvider->getOutOfStockStatus())
        );

        $qb->where($where);

        return $qb->getQuery()->execute();
    }
}