<?php

namespace App\WatchDog;

use App\Model\BaseEntity;
use App\Product\Variant;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 */
class WatchDog extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="\App\Product\Variant")
     * @ORM\JoinColumn(name="price_variant_id", referencedColumnName="id")
     */
    private $priceVariant;

    /**
     * @ORM\Column(type="string", length=160)
     */
    private $email;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $sent;

    public function __construct(Variant $priceVariant, $email)
    {
        $this->priceVariant = $priceVariant;
        $this->email = $email;
        $this->created = new DateTime();
    }

    /**
     * @return Variant
     */
    public function getVariant()
    {
        return $this->priceVariant;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getSent()
    {
        return $this->sent;
    }

    public function setSent()
    {
        $this->sent = new DateTime();
    }
}