<?php

namespace App\WatchDog;

use App\Mailing\EmailManager;
use App\Product\Variant;
use App\Utils\Strings;

class WatchDogService
{
    /** @var EmailManager */
    private $emailManager;

    /** @var WatchDogRepository */
	private $watchDogRepository;

	public function __construct(
        WatchDogRepository $watchDogRepository,
        EmailManager $emailManager
    ) {
        $this->emailManager = $emailManager;
		$this->watchDogRepository = $watchDogRepository;
	}

    public function createItem(Variant $priceVariant, $email)
    {
        $watchDog = new WatchDog($priceVariant, $email);
        $this->watchDogRepository->saveWatchDogItem($watchDog);
    }

    public function sendNotifications()
    {
        /** @var WatchDog $item */
        foreach ($this->watchDogRepository->getItemsToNotify() as $item) {
            if (Strings::isValidEmail($item->getEmail())) {
                $this->emailManager->sendWatchDogProductAvailableEmail($item->getVariant(), $item->getEmail());
            }
            $item->setSent();
	        $this->watchDogRepository->saveWatchDogItem($item);
        }
    }
}