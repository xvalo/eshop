<?php

namespace App\Order;


class OrderLineState
{
    const IN_STOCK = 1;
    const ORDERED = 2;
    const SOLD_OUT = 3;
    const PICKING = 4;
    const DAMAGED = 5;
    const SENT = 6;
    const NOT_SENT = 7;

    public static $statesLabels = [
        self::IN_STOCK => "Skladem",
        self::ORDERED => "Objednáno",
        self::SOLD_OUT => "Vyprodáno",
        self::PICKING => "Vychystáno",
        self::DAMAGED => "Poškozeno",
        self::SENT => "Odesláno",
        self::NOT_SENT => "Neodesláno"
    ];

    public static function getStatusLabel($status)
    {
        return self::$statesLabels[$status];
    }
}