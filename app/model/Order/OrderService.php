<?php

namespace App\Order;

use App\Classes\Product\Reserved\ReservedProductService;
use App\Lists\DeliveryRepository;
use App\Stock\StockManager;
use App\Order\OrderLine\OrderLineRepository;
use App\Order\OrderLine\ProductOrderLine;
use App\Product\Price\Price;
use App\Product\Variant;
use App\Product\VariantRepository;
use App\ReservedProduct\ReservedProduct;

class OrderService
{
    /** @var OrderRepository */
    private $repository;

    /** @var VariantRepository */
    private $priceVariantRepository;

    /** @var DeliveryRepository */
    private $deliveryRepository;

	/** @var OrderLineRepository */
	private $orderLineRepository;

	/** @var ReservedProductService */
	private $reservedProductService;

	/** @var StockManager */
	private $stockManager;

	/**
	 * @param OrderRepository $repository
	 * @param OrderLineRepository $orderLineRepository
	 * @param VariantRepository $priceVariantRepository
	 * @param DeliveryRepository $deliveryRepository
	 * @param ReservedProductService $reservedProductService
	 */
    public function __construct(
	    OrderRepository $repository,
	    OrderLineRepository $orderLineRepository,
	    VariantRepository $priceVariantRepository,
	    DeliveryRepository $deliveryRepository,
	    ReservedProductService $reservedProductService,
	    StockManager $stockManager
    ){
        $this->repository = $repository;
        $this->priceVariantRepository = $priceVariantRepository;
        $this->deliveryRepository = $deliveryRepository;
	    $this->orderLineRepository = $orderLineRepository;
	    $this->reservedProductService = $reservedProductService;
	    $this->stockManager = $stockManager;
    }

    public function getOrder($id)
    {
        return $this->repository->find($id);
    }

    public function changeOrderLineStatus($orderLineId, $state)
    {
        /** @var ProductOrderLine $orderLine */
        $orderLine = $this->orderLineRepository->find($orderLineId);
        $orderLine->setState($state);

        if (in_array($state, [OrderLineState::SOLD_OUT, OrderLineState::PICKING, OrderLineState::SENT, OrderLineState::DAMAGED])) {
	        $this->reservedProductService->deactivateReservation($orderLine);
        }

        $this->repository->flush();
    }

    public function removeOrderLineFromOrder(Order $order, $orderLineId)
    {
	    $orderLine = $this->orderLineRepository->find($orderLineId);

	    if ($orderLine instanceof ProductOrderLine) {
		    $deactivatedReservations = $this->reservedProductService->deactivateReservation($orderLine);
		    /** @var ReservedProduct $reservation */
		    foreach ($deactivatedReservations as $reservation) {
			    $this->stockManager->returnStockItem($orderLine->getPrice()->getVariant(), $reservation->getStockItem()->getStock(), $order, $reservation->getCount(), false);
		    }
	    }

        $order->removeOrderLine($orderLine);
        $this->repository->flush();
    }


    public function countEditedOrderTotalPrice($newOrderLinesData, Order $order = null, $discount = 0)
    {
        $totalPrice = 0;

        if($order){
            $totalPrice += $order->getTotalPrice();
        }

        foreach ($newOrderLinesData as $id => $count) {
            /** @var Price $variantPrice */
            $variantPrice = $this->priceVariantRepository->findPrice((int)$id);
            $totalPrice += ($variantPrice->getPrice() * $count);
        }

        if($discount || ($order && $order->getDiscount() != $discount)){
            if($order){
                $totalPrice = $totalPrice + $order->getDiscount() - $discount;
            }else{
                $totalPrice -= $discount;
            }
        }

        return $totalPrice;
    }
}