<?php

namespace App\Order;

use App\Classes\Order\OrderSalesStatisticsPeriod;
use App\File\OrderDocumentType;
use App\Model\BaseRepository;
use App\Order\OrderLine\ExtraItemOrderLine;
use App\Order\OrderLine\OrderLine;
use App\Order\OrderLine\ProductOrderLine;
use Doctrine\ORM\Query\Expr;
use Kdyby\Doctrine\Dql\Join;
use Nette\Utils\DateTime;
use Tracy\Debugger;

class OrderRepository extends BaseRepository
{
    public function getGridData()
    {
        $qb = parent::getGridData();

		$qb->where($qb->expr()->isNull('O.canceled'));

        return $qb;
    }

	public function getCanceledGridData()
	{
		$qb = parent::getGridData();

		$qb->where($qb->expr()->isNotNull('O.canceled'));

		return $qb;
    }

	public function getSpecialDeliveryGridData(array $delivery)
	{
		$qb = parent::getGridData();

		$qb->innerJoin('O.documents', 'D')
			->innerJoin('\App\Order\OrderLine\DeliveryOrderLine', 'DOL', Join::WITH, 'DOL.order = O.id');

		$where = $qb->expr()->andX(
			$qb->expr()->isNull('O.canceled'),
			$qb->expr()->in('DOL.delivery', $delivery),
			$qb->expr()->in('DOL.deleted', ':deletedOrderLine'),
			$qb->expr()->isNull('O.shipped'),
			$qb->expr()->eq('D.deleted', ':deleted'),
			$qb->expr()->eq('D.type', ':type')
		);

		$qb->where($where);

		$qb->setParameters([
			'deleted' => false,
			'deletedOrderLine' => false,
			'type' => OrderDocumentType::INVOICE
		]);

		return $qb;
    }

    public function getOrderLinesForOrder(Order $order)
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('L')
            ->from(OrderLine::class , 'L')
	        ->where(
	        	$qb->expr()->andX(
	        		$qb->expr()->eq('L.order', ':order'),
			        $qb->expr()->eq('L.deleted', ':deleted'),
			        $qb->expr()->orX(
				        $qb->expr()->isInstanceOf('L', ProductOrderLine::class),
				        $qb->expr()->isInstanceOf('L', ExtraItemOrderLine::class)
			        )
		        )
	        );

        $qb->setParameters([
        	'order' => $order,
	        'deleted' => false
        ]);

        return $qb;

    }

    public function searchOrders($q)
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select(['o.id AS id', 'o.orderNo AS orderNo', 'c.name AS customerName', 'c.lastName AS customerLastName', 'c.email AS customerEmail', 'o.created AS created'])
            ->from('\App\Order\Order', 'o')
                ->leftJoin('o.contactInformation', 'c');

        $where = $qb->expr()->orX(
            $qb->expr()->like('o.orderNo', ':phrase'),
            $qb->expr()->like('o.packageNo', ':phrase'),
            $qb->expr()->like('o.customerNote', ':phrase'),
            $qb->expr()->like('c.name', ':phrase'),
            $qb->expr()->like('c.lastName', ':phrase'),
	        $qb->expr()->like('c.email', ':phrase'),
	        $qb->expr()->like('c.phone', ':phrase')
        );

        $qb->where($where);

        $qb->setParameter('phrase', '%'.$q.'%');

        return $qb->getQuery()->execute();
    }

	public function getOrdersSales($source, $period)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('SUM(O.totalPrice) AS orders')
			->from('\App\Order\Order', 'O');

		$sourceCondition = $source == 1
					? $qb->expr()->orX(
						$qb->expr()->eq('O.source', ':source'),
						$qb->expr()->eq('O.source', 0)
					)
					: $qb->expr()->eq('O.source', ':source');

		$where = $qb->expr()->andX(
			$qb->expr()->isNull('O.canceled'),
			$qb->expr()->gte('O.created', ':created'),
			$sourceCondition
		);

		$qb->where($where);

		switch ($period) {
			case OrderSalesStatisticsPeriod::MONTH:
				$qb->addSelect("CONCAT(MONTH(O.created),'/',YEAR(O.created)) AS period, YEAR(O.created) AS crYear, MONTH(O.created) AS crMonth")
					->groupBy('crYear, crMonth')
					->orderBy('crYear, crMonth');
				break;
			case OrderSalesStatisticsPeriod::QUARTER:
				$qb->addSelect("CONCAT(QUARTER(O.created), '. kv. / ', YEAR(O.created)) AS period, YEAR(O.created) AS crYear, QUARTER(O.created) AS crQuarter")
					->groupBy('crYear, crQuarter')
					->orderBy('crYear, crQuarter');
				break;
			case OrderSalesStatisticsPeriod::YEAR:
				$qb->addSelect('YEAR(O.created) AS period')
					->groupBy('period')
					->orderBy('period');
				break;
		}

		//$qb->groupBy('crYear, crMonth');

		$minusOneMonthDate = (new DateTime(date('Y-m-t')))->sub(\DateInterval::createFromDateString(OrderSalesStatisticsPeriod::getTimePeriod($period)));
		$minusOneMonthDate->setTime(23, 59, 59);

		$qb->setParameters([
			'created' => $minusOneMonthDate,
			'source' => $source
		]);

		return $qb->getQuery()->execute();
    }

	public function searchCustomers($phrase)
	{
		$qb = $this->em->createQueryBuilder()
				->select('O.id, CI.name, CI.lastName, CI.phone, CI.email')
				->from('\App\Order\Order', 'O')
					->join('O.contactInformation', 'CI');

		$where = $qb->expr()->andX(
			$qb->expr()->isNull('O.customer'),
			$qb->expr()->orX(
				$qb->expr()->like('CI.name', ':phrase'),
				$qb->expr()->like('CI.lastName', ':phrase'),
				$qb->expr()->like('CI.email', ':phrase'),
				$qb->expr()->like('CI.phone', ':phrase'),
				$qb->expr()->like('O.lyonessCardId', ':phrase'),
				$qb->expr()->like('O.lyonessCardEan', ':phrase')

			)
		);

		$qb->where($where);

		$qb->groupBy('CI.email');
		$qb->orderBy('O.created', 'DESC');

		$qb->setParameters([
			'phrase' => '%'.$phrase.'%'
		]);


		return $qb->getQuery()->execute();
    }
}