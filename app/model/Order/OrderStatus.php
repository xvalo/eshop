<?php

namespace App\Order;


class OrderStatus
{
    const CREATED = 1;
    const IN_PROCESS = 2;
    const DONE = 3;

    public static $statesLabels = [
        self::CREATED => 'Nová',
        self::IN_PROCESS => 'Vyřizuje se',
        self::DONE => 'Vyřízená'
    ];

    public static function getStatusLabel($status)
    {
        return self::$statesLabels[$status];
    }
}