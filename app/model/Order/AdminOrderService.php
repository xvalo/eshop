<?php

namespace App\Order;

use App\Admin\Admin;
use App\Admin\AdminService;
use App\Classes\Order\Creation\OrderCreationService;
use App\Classes\Order\Creation\OrderLine\OrderLineFactory;
use App\Classes\Order\OrderDeliverability;
use App\Classes\Product\Reserved\ReservedProductService;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\InvalidArgumentException;
use App\InvalidStateException;
use App\Lists\SystemVariableRepository;
use App\Mailing\EmailManager;
use App\Mailing\OrderEmailType;
use App\Stock\StockManager;
use App\Model\BaseService;
use App\Model\Services\ApplicationConfigurationService;
use App\Order\OrderLine\OrderLine;
use App\Order\OrderLine\OrderLineRepository;
use App\Order\OrderLine\ProductOrderLine;
use App\ReservedProduct\ReservedProduct;
use Carrooi\Security\User\User;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class AdminOrderService
 * @package App\Order
 * @property OrderRepository $repository
 */
class AdminOrderService extends BaseService
{
    /** @var ApplicationConfigurationService */
    private $applicationConfigurationService;

    /** @var EmailManager */
    private $emailManager;

    /** @var SystemVariableRepository */
    private $systemVariableRepository;

    /** @var OrderLineRepository */
    private $orderLineRepository;

	/** @var User */
	private $user;

	/** @var AdminService */
	private $adminService;

	/** @var ReservedProductService */
	private $reservedProductService;

	/** @var StockManager */
	private $stockManager;
	/**
	 * @var OrderCreationService
	 */
	private $orderCreationService;
	/**
	 * @var ApplicationSettingsManager
	 */
	private $applicationSettingsManager;

	public function __construct(
		OrderRepository $repository,
		OrderLineRepository $orderLineRepository,
		ApplicationConfigurationService $applicationConfigurationService,
		EmailManager $emailManager,
		SystemVariableRepository $systemVariableRepository,
		User $user,
		AdminService $adminService,
		ReservedProductService $reservedProductService,
		StockManager $stockManager,
		OrderCreationService $orderCreationService,
		ApplicationSettingsManager $applicationSettingsManager
    ){
        parent::__construct($repository);
        $this->applicationConfigurationService = $applicationConfigurationService;
        $this->emailManager = $emailManager;
        $this->systemVariableRepository = $systemVariableRepository;
        $this->orderLineRepository = $orderLineRepository;
	    $this->user = $user;
	    $this->adminService = $adminService;
		$this->reservedProductService = $reservedProductService;
		$this->stockManager = $stockManager;
		$this->orderCreationService = $orderCreationService;
		$this->applicationSettingsManager = $applicationSettingsManager;
	}

    public function getFormDefaults(Order $order)
    {
        $defaults = [
            'contactInformation' => [
                'name' => $order->getContactInformation()->getName(),
                'lastName' => $order->getContactInformation()->getLastName(),
                'email' => $order->getContactInformation()->getEmail(),
                'phone' => $order->getContactInformation()->getPhone(),
	            'lyonessCardId' => $order->getLyonessCardId(),
	            'lyonessCardEan' => $order->getLyonessCardEan()
            ],
            'invoiceInformation' => [
                'ico' => $order->getInvoiceInformation()->getIco(),
                'dic' => $order->getInvoiceInformation()->getDic(),
                'company' => $order->getInvoiceInformation()->getCompany(),
                'street' => $order->getInvoiceInformation()->getStreet(),
                'city' => $order->getInvoiceInformation()->getCity(),
                'zip' => $order->getInvoiceInformation()->getZip(),
                'country' => $order->getInvoiceInformation()->getCountry()
            ],
            'status' => $order->getStatus(),
            'delivery' => ($order->getDelivery()) ? $order->getDelivery()->getId() : null,
	        'paymentType' => ($order->getPaymentType()) ? $order->getPaymentType()->getId() : null,
            'packageNo' => $order->getPackageNo(),
            'discount' => $order->getDiscount()
        ];

        $deliveryAddress = $order->getDeliveryAddress();
        if($deliveryAddress){
            $defaults['deliveryAddress'] = [
                'name' => $order->getDeliveryAddress()->getName(),
                'lastName' => $order->getDeliveryAddress()->getLastName(),
                'street' => $order->getDeliveryAddress()->getStreet(),
                'city' => $order->getDeliveryAddress()->getCity(),
                'zip' => $order->getDeliveryAddress()->getZip(),
                'country' => $order->getDeliveryAddress()->getCountry(),
                'company' => $order->getDeliveryAddress()->getCompany(),
            ];
        }

        return $defaults;
    }

    public function setOrderCanceled(Order $order, $sendNotification)
    {
    	if ($sendNotification && strlen($order->getContactInformation()->getEmail()) == 0) {
		    throw new InvalidStateException('Email nemůže být odeslán, protože není zadána emailová adresa. Stonro se neprovedlo.');
	    }

	    /** @var ProductOrderLine $productOrderLine */
	    foreach ($order->getProducts() as $productOrderLine) {
    		$deactivatedReservations = $this->reservedProductService->deactivateReservation($productOrderLine);
    		/** @var ReservedProduct $reservation */
		    foreach ($deactivatedReservations as $reservation) {
			    $this->stockManager->returnStockItem($productOrderLine->getPrice()->getVariant(), $reservation->getStockItem()->getStock(), $order, $reservation->getCount(), false);
		    }
	    }

        $order->setCanceled();
        $this->repository->flush();

        if ($sendNotification) {
	        $this->emailManager->sendOrderCanceledEmail($order);
	        $this->saveEmailMessageFilePath($order, $this->emailManager->getLastSentEmailFilePath(), OrderEmailType::CANCELED);
        }
    }

    public function removeCanceledState(Order $order)
    {
        $order->removeCanceled();
        //$this->orderCreationService->updateStockItems($order);
        $this->repository->flush();
    }

    public function setOrderShipped(Order $order, $packageNo = null)
    {
        if (
            $order->getDelivery() && $order->getDelivery()->getId() != $this->applicationConfigurationService->getPersonalPickupDeliveries() && !$packageNo
        ) {
            throw new InvalidStateException;
        }

        $order->setPackageNo($packageNo);
        $order->setShipped();

	    /** @var ProductOrderLine $productOrderLine */
	    foreach ($order->getProducts() as $productOrderLine) {
		    $this->reservedProductService->deactivateReservation($productOrderLine);
	    }

	    $this->repository->flush();

        if ($order->getDelivery()) {
	        if ($order->getDelivery()->getId() == $this->applicationConfigurationService->getPersonalPickupDeliveries()) {
		        $this->emailManager->sendOrderToBePickedUpEmail($order);
		        $this->saveEmailMessageFilePath($order, $this->emailManager->getLastSentEmailFilePath(), OrderEmailType::TO_BE_PICKED);
	        } else {
		        $this->emailManager->sendOrderShippedEmail($order);
		        $this->saveEmailMessageFilePath($order, $this->emailManager->getLastSentEmailFilePath(), OrderEmailType::SHIPPED);
	        }

        }
    }

    public function setOrderPayed(Order $order)
    {
        $order->setPayed();
        $this->repository->flush();
    }

    public function setOrderStatus(Order $order, $status)
    {
        $order->setStatus($status);
        $this->repository->flush();
    }

    public function removeReceiptFile(Order $order)
    {
        $order->getReceipt()->delete();

        if (OrderDeliverability::isOrderByDeliveryInvoicable($order->getPaymentType())) {
        	$number = $this->applicationSettingsManager->getNextInvoiceNumber();
        	$this->applicationSettingsManager->setNextInvoiceNumber(--$number);
        } else {
	        $number = $this->applicationSettingsManager->getNextBillNumber();
	        $this->applicationSettingsManager->setNextBillNumber(--$number);
        }

        $this->repository->flush();
    }

    public function changeOrderLineQuantity($orderLineId, $quantity)
    {
        $line = $this->orderLineRepository->find($orderLineId);
        $line->setCount($quantity);
        $this->repository->flush();
    }

    public function searchOrders($q)
    {
        return $this->repository->searchOrders($q);
    }

    public function sendCustomerGeneralMail(Order $order, $body)
    {
        $this->emailManager->sendCustomerGeneralEmail($order, $body);
        $this->saveEmailMessageFilePath($order, $this->emailManager->getLastSentEmailFilePath(), OrderEmailType::GENERAL);
    }

	public function sendOrderRejectedMail(Order $order)
	{
		$this->emailManager->sendOrderRejectedEmail($order);
		$this->saveEmailMessageFilePath($order, $this->emailManager->getLastSentEmailFilePath(), OrderEmailType::REJECTED);
    }

	public function sendOrderNotPaidMail(Order $order)
	{
		$this->emailManager->sendOrderNotPaidEmail($order);
		$this->saveEmailMessageFilePath($order, $this->emailManager->getLastSentEmailFilePath(), OrderEmailType::NOT_PAID);
	}

	public function setOrderPayment(Order $order, $paidBy, $receivedAmount, $applyCharge = false)
	{
		$cardPaymentCharge = null;

		// TODO: Odstranit poplatek za platbu kartou jakmile už se to nebude vůbec používat.
		if ($paidBy == Payment::TYPE_BY_CARD && $applyCharge) {
			$cardPaymentOrderLine = OrderLineFactory::createCardPaymentOrderLine($order);
			$cardPaymentCharge = $cardPaymentOrderLine->getUnitPrice();
			$order->addOrderLine($cardPaymentOrderLine);
			$receivedAmount += $cardPaymentCharge;
		}

		$order->setPayment(new Payment($order, $paidBy, $receivedAmount, $cardPaymentCharge))
				->setPayed()
				->setShipped();

		$this->repository->flush();
    }

	/**
	 * @param Order $order
	 * @param array $orderLinesChanges
	 * @return ArrayCollection
	 */
	public function returnOrderLines(Order $order, array $orderLinesChanges): ArrayCollection
	{
		$linesChanged = new ArrayCollection();

		/** @var OrderLine $orderLine */
		foreach ($order->getOrderLines() as $orderLine) {
			$amountToReturn = (int)$orderLinesChanges[$orderLine->getId()]['toReturn'];
			if ($amountToReturn > 0) {
				if ($amountToReturn > $orderLine->getCount()) {
					throw new InvalidArgumentException();
				}

				$returnedOrderLine = OrderLineFactory::createReturnedOrderLine($orderLine, $amountToReturn);
				$orderLine->addReturnedLine( $returnedOrderLine );
				$orderLine->setCount($orderLine->getCount() - $amountToReturn);
				$linesChanged->add($returnedOrderLine);
			}
		}

		$this->repository->flush();
		return $linesChanged;
    }

	public function addComment($comment, Order $order, Admin $author)
	{
		$order->addComment(new OrderComment($order, $author, $comment));
		$this->repository->flush();
    }

	private function saveEmailMessageFilePath(Order $order, $path, $type)
	{
		$admin = $this->adminService->getAdmin($this->user->getId());
		$order->addMailMessage(new MailMessage($order, $admin, $type, $path));
		$this->repository->flush();
    }
}