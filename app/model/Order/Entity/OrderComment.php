<?php

namespace App\Order;

use App\Admin\Admin;
use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 */
class OrderComment extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\Order", inversedBy="comments")
	 * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
	 */
	private $order;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Admin\Admin")
	 * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
	 */
	private $author;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $created;

	/**
	 * @ORM\Column(type="text")
	 */
	private $content;

	public function __construct(Order $order, Admin $author, $content)
	{
		$this->order = $order;
		$this->author = $author;
		$this->content = $content;
		$this->created = new DateTime();
	}

	/**
	 * @return mixed
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @return mixed
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * @return mixed
	 */
	public function getContent()
	{
		return $this->content;
	}
}