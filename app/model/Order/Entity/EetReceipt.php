<?php

namespace App\Order;

use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class EetReceipt extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\Order", inversedBy="eetReceipts")
	 * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
	 */
	private $order;

	/**
	 * @ORM\Column(type="string", length=40)
	 */
	private $uuid;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $fik;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $bkp;

	/**
	 * @ORM\Column(type="string", length=500, nullable=true)
	 */
	private $pkp;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $sendDate;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $responseTime;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $processed;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $test;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $errorData;

	public function __construct(
		Order $order,
		string $uuid,
		\DateTimeImmutable $sendDate,
		\DateTimeImmutable $responseTime,
		bool $processed,
		string $bkp = null,
		string $pkp = null,
		string $fik = null,
		bool $test = false,
		string $errorData = null
	){

		$this->order = $order;
		$this->uuid = $uuid;
		$this->sendDate = $sendDate;
		$this->responseTime = $responseTime;
		$this->processed = $processed;
		$this->bkp = $bkp;
		$this->pkp = $pkp;
		$this->fik = $fik;
		$this->test = $test;
		$this->errorData = $errorData;
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}

	/**
	 * @return string|null
	 */
	public function getFik()
	{
		return $this->fik;
	}

	/**
	 * @return string|null
	 */
	public function getBkp()
	{
		return $this->bkp;
	}

	/**
	 * @return string|null
	 */
	public function getPkp()
	{
		return $this->pkp;
	}

	/**
	 * @return bool
	 */
	public function isProcessed(): bool
	{
		return $this->processed;
	}

	/**
	 * @return string
	 */
	public function getUuid()
	{
		return $this->uuid;
	}

	/**
	 * @return \DateTime
	 */
	public function getSendDate()
	{
		return $this->sendDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getResponseTime()
	{
		return $this->responseTime;
	}

	/**
	 * @return bool
	 */
	public function isTest()
	{
		return $this->test;
	}

	/**
	 * @return string
	 */
	public function getErrorData()
	{
		return $this->errorData;
	}
}