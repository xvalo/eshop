<?php

namespace App\Order\OrderLine;

use App\Attribute\Product\ProductAttributeVariant;
use App\InvalidArgumentException;
use App\Order\Order;
use App\Order\OrderLineState;
use App\Product\Price\Price;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ProductOrderLine extends OrderLine
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Product\Price\Price")
	 * @ORM\JoinColumn(name="variant_price_id", referencedColumnName="id")
	 * @var Price
	 */
	private $price;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 * @var float
	 */
	private $state;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Order\OrderLine\OrderProductAttribute", mappedBy="product", indexBy="id", cascade={"persist"}, orphanRemoval=true)
	 */
	private $attributesVariants;

	/**
	 * @param Order $order
	 * @param Price $price
	 * @param int $count
	 * @param ProductAttributeVariant[] $attributes
	 */
	public function __construct(Order $order, Price $price, int $count, array $attributes = [])
	{
		$this->price = $price;
		$this->attributesVariants = new ArrayCollection();

		$totalPrice = $price->getPrice();

		/** @var ProductAttributeVariant $attributeVariant */
		foreach ($attributes as $attributeVariant) {
			$totalPrice += $attributeVariant->getPrice();
			$this->attributesVariants->add(new OrderProductAttribute($this, $attributeVariant));
		}

		parent::__construct($order, $totalPrice, $count, $price->getVariant()->getProduct()->getVat());
	}

	/**
	 * @return Price
	 */
	public function getPrice(): Price
	{
		return $this->price;
	}

	/**
	 * @return int|null
	 */
	public function getState()
	{
		return $this->state;
	}

	/**
	 * @param int $state
	 */
	public function setState(int $state = null)
	{
		if (!is_null($state) && !array_key_exists($state, OrderLineState::$statesLabels)) {
			throw new InvalidArgumentException('Invalid Order Line state.');
		}
		$this->state = $state;
	}

	public function getTitle()
	{
		$attributesTitles = array_map(function(OrderProductAttribute $item){
			$attribute = $item->getProductAttributeVariant()->getProductAttribute()->getAttribute();
			$variant = $item->getProductAttributeVariant()->getAttributeVariant();

			return (string)$attribute . ': ' . $variant->getName();
		}, $this->attributesVariants->toArray());

		return $this->getPrice()->getPriceName() . (!empty($attributesTitles) ? ' ('.implode('; ',$attributesTitles).')' : '');
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return (string)$this->getPrice();
	}
}