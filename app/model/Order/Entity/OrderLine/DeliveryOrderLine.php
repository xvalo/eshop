<?php

namespace App\Order\OrderLine;

use App\Lists\Delivery;
use App\Order\Order;
use App\Utils\Vat;
use App\Order\Delivery\Entity\CzechPostOffice;
use App\Order\Delivery\Entity\CzechPostBranch;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class DeliveryOrderLine extends OrderLine
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Lists\Delivery")
	 * @ORM\JoinColumn(name="delivery_id", referencedColumnName="id", nullable=true)
	 */
	private $delivery;

	/**
	 * @ORM\Column(type="boolean")
	 * @var bool
	 */
	private $freeDelivery;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\Delivery\Entity\CzechPostBranch")
	 * @ORM\JoinColumn(name="czech_post_box_id", referencedColumnName="id", nullable=true)
	 * @var CzechPostBranch|null
	 */
	private $czechPostBox;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\Delivery\Entity\CzechPostOffice")
	 * @ORM\JoinColumn(name="czech_post_office_id", referencedColumnName="id", nullable=true)
	 * @var CzechPostOffice|null
	 */
	private $czechPostOffice;

	public function __construct(Order $order, Delivery $delivery = null, bool $freeDelivery = false, CzechPostBranch $czechPostBox = null, CzechPostOffice $czechPostOffice = null)
	{
		parent::__construct($order, ($delivery ? $delivery->getPrice() : 0), 1, Vat::TYPE_21);

		$this->delivery = $delivery;
		$this->freeDelivery = $freeDelivery;
		$this->czechPostBox = $czechPostBox;
		$this->czechPostOffice = $czechPostOffice;
	}

	/**
	 * @return Delivery|null
	 */
	public function getDelivery()
	{
		return $this->delivery;
	}

	/**
	 * @param Delivery|null $delivery
	 */
	public function setDelivery(Delivery $delivery = null)
	{
		$this->delivery = $delivery;
		$this->setUnitPrice($delivery->getPrice());
	}

	/**
	 * @param float $productsTotalPrice
	 * @return bool
	 */
	public function updatePriceWithFreePriceLimit(float $productsTotalPrice): bool
	{
		$this->setFreeDelivery(false);
		$this->setUnitPrice($this->getDelivery()->getPrice());

		if (
			$this->getDelivery() !== null
			&& $this->getDelivery()->getFreePriceLimit() > 0
			&& $this->getDelivery()->getFreePriceLimit() <= $productsTotalPrice
		) {
			$this->setUnitPrice(0);
			$this->setFreeDelivery(true);
			return true;
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function isFreeDelivery(): bool
	{
		return $this->freeDelivery;
	}

	/**
	 * @param bool $freeDelivery
	 */
	public function setFreeDelivery(bool $freeDelivery)
	{
		$this->freeDelivery = $freeDelivery;
	}

	/**
	 * @return CzechPostBranch|null
	 */
	public function getCzechPostBox()
	{
		return $this->czechPostBox;
	}

	/**
	 * @param CzechPostBranch|null $czechPostBox
	 */
	public function setCzechPostBox(CzechPostBranch $czechPostBox = null)
	{
		$this->czechPostBox = $czechPostBox;
	}

	/**
	 * @return CzechPostOffice|null
	 */
	public function getCzechPostOffice()
	{
		return $this->czechPostOffice;
	}

	/**
	 * @param CzechPostOffice|null $czechPostOffice
	 */
	public function setCzechPostOffice(CzechPostOffice $czechPostOffice = null)
	{
		$this->czechPostOffice = $czechPostOffice;
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return ($this->getDelivery()) ? 'Doprava - ' . $this->getDelivery()->getName() : '';
	}
}