<?php

namespace App\Order\OrderLine;

use App\Order\Order;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ExtraItemOrderLine extends OrderLine
{
	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	private $description;

	public function __construct(Order $order, float $unitPrice, int $count, int $vat, string $description)
	{
		parent::__construct($order, $unitPrice, $count, $vat);

		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description)
	{
		$this->description = $description;
	}

	public function getState()
	{
		return 0;
	}

	public function __toString()
	{
		return $this->getDescription();
	}
}