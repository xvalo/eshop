<?php

namespace App\Order\OrderLine;

use App\Order\Order;
use App\Utils\Vat;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class CardPaymentOrderLine extends OrderLine
{
	const FEE_MULTIPLIER = 0.02;

	public function __construct(Order $order)
	{
		parent::__construct($order, round($order->getTotalPrice() * self::FEE_MULTIPLIER), 1, Vat::TYPE_21);
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return 'Platba kartou - poplatek';
	}
}