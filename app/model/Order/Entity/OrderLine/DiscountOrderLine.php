<?php

namespace App\Order\OrderLine;

use App\Order\Order;
use App\Utils\Vat;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class DiscountOrderLine extends OrderLine
{
	public function __construct(Order $order, float $discountAmount)
	{
		parent::__construct($order, $discountAmount, 1, Vat::TYPE_21);
	}

	/**
	 * @return int
	 */
	public function getDiscount(): int
	{
		return parent::getUnitPrice();
	}

	/**
	 * @param int $discount
	 */
	public function setDiscount(int $discount)
	{
		$this->setUnitPrice($discount);
	}

	public function getUnitPrice()
	{
		return -1 * parent::getUnitPrice();
	}

	public function __toString()
	{
		return 'Sleva';
	}
}