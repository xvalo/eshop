<?php

namespace App\Order\OrderLine;

use App\Attribute\Product\ProductAttributeVariant;
use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="order_product_attribute_variant"
 * )
 */
class OrderProductAttribute extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\OrderLine\ProductOrderLine", inversedBy="attributesVariants")
	 * @ORM\JoinColumn(name="product_order_line_id", referencedColumnName="id")
	 * @var ProductOrderLine
	 */
	private $product;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Attribute\Product\ProductAttributeVariant")
	 * @ORM\JoinColumn(name="attribute_variant_id", referencedColumnName="id")
	 * @var ProductAttributeVariant
	 */
	private $productAttributeVariant;

	public function __construct(ProductOrderLine $productOrderLine, ProductAttributeVariant $attributeVariant)
	{
		$this->product = $productOrderLine;
		$this->productAttributeVariant = $attributeVariant;
	}

	/**
	 * @return ProductAttributeVariant
	 */
	public function getProductAttributeVariant(): ProductAttributeVariant
	{
		return $this->productAttributeVariant;
	}
}