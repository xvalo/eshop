<?php

namespace App\Order\OrderLine;

use App\Order\Order;
use App\Order\Payment\Entity\PaymentType;
use App\Utils\Vat;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class PaymentTypeOrderLine extends OrderLine
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\Payment\Entity\PaymentType")
	 * @ORM\JoinColumn(name="payment_type_id", referencedColumnName="id", nullable=true)
	 */
	private $paymentType;

	public function __construct(Order $order, PaymentType $paymentType, float $price)
	{
		parent::__construct($order, $price, 1, Vat::TYPE_21);

		$this->paymentType = $paymentType;
	}

	/**
	 * @return PaymentType
	 */
	public function getPaymentType()
	{
		return $this->paymentType;
	}

	/**
	 * @param PaymentType $paymentType
	 * @param float $price
	 */
	public function setPaymentType(PaymentType $paymentType, float $price)
	{
		$this->paymentType = $paymentType;
		$this->setUnitPrice($price);
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return 'Způsob platby - ' . $this->getPaymentType()->getName();
	}
}