<?php

namespace App\Order\OrderLine;

use App\Model\BaseEntity;
use Nette\Utils\DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class OrderLineReturned extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\OrderLine\OrderLine", inversedBy="returned")
	 * @ORM\JoinColumn(name="order_line_id", referencedColumnName="id")
	 * @var OrderLine
	 */
	private $orderLine;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $count;

	/**
	 * @ORM\Column(type="datetime")
	 * @var DateTime
	 */
	private $created;

	/**
	 * OrderLineReturned constructor.
	 * @param OrderLine $line
	 * @param int $count
	 */
	public function __construct(OrderLine $line, int $count)
	{
		$this->orderLine = $line;
		$this->count = $count;
		$this->created = new DateTime();
	}

	/**
	 * @return OrderLine
	 */
	public function getOrderLine(): OrderLine
	{
		return $this->orderLine;
	}

	/**
	 * @return int
	 */
	public function getCount(): int
	{
		return $this->count;
	}

	/**
	 * @return DateTime
	 */
	public function getCreated(): DateTime
	{
		return $this->created;
	}

	/**
	 * @return float
	 */
	public function getUnitPrice(): float
	{
		return $this->getOrderLine()->getUnitPrice();
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return $this->orderLine->__toString();
	}
}