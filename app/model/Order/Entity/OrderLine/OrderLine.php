<?php

namespace App\Order\OrderLine;

use App\Model\BaseEntity;
use App\Order\Order;
use App\Traits\RemovableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({
 *     "product" = "\App\Order\OrderLine\ProductOrderLine",
 *     "delivery" = "\App\Order\OrderLine\DeliveryOrderLine",
 *     "paymentType" = "\App\Order\OrderLine\PaymentTypeOrderLine",
 *     "discount" = "\App\Order\OrderLine\DiscountOrderLine",
 *     "cardPayment" = "\App\Order\OrderLine\CardPaymentOrderLine",
 *     "attributeVariant" = "\App\Attribute\Order\AttributeVariantOrderLine",
 *     "extraItem" = "\App\Order\OrderLine\ExtraItemOrderLine"
 * })
 */
abstract class OrderLine extends BaseEntity
{
	use RemovableTrait;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\Order", inversedBy="orderLines")
	 * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
	 */
	private $order;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
	private $unitPrice;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $count;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Order\OrderLine\OrderLineReturned", mappedBy="orderLine", cascade={"persist"})
	 */
	private $returned;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $vat;

	public function __construct(Order $order, float $unitPrice, int $count, int $vat)
	{
		$this->order = $order;
		$this->unitPrice = $unitPrice;
		$this->count = $count;
		$this->vat = $vat;
		$this->returned = new ArrayCollection();
	}

	/**
	 * @return int
	 */
	public function getCount()
	{
		return (int)$this->count;
	}

	/**
	 * @return float
	 */
	public function getUnitPrice()
	{
		return $this->unitPrice;
	}

	public function setCount($quantity)
	{
		$this->count = $quantity;
	}

	/**
	 * @return Order
	 */
	public function getOrder()
	{
		return $this->order;
	}

	/**
	 * @param OrderLineReturned $line
	 */
	public function addReturnedLine(OrderLineReturned $line)
	{
		$this->returned->add($line);
	}

	/**
	 * @return ReadOnlyCollectionWrapper
	 */
	public function getReturnedLines(): ReadOnlyCollectionWrapper
	{
		return new ReadOnlyCollectionWrapper($this->returned);
	}

	/**
	 * @param float $price
	 */
	protected function setUnitPrice(float $price)
	{
		$this->unitPrice = $price;
	}

	/**
	 * @return int
	 */
	public function getVat(): int
	{
		return $this->vat;
	}

	abstract public function __toString();
}