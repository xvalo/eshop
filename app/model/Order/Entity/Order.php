<?php

namespace App\Order;

use App\Classes\Order\OrderDeliverability;
use App\Customer\Customer;
use App\File\OrderDocument;
use App\File\OrderDocumentType;
use App\InvalidArgumentException;
use App\Lists\Delivery;
use App\Order\OrderLine\DeliveryOrderLine;
use App\Order\OrderLine\DiscountOrderLine;
use App\Order\OrderLine\ExtraItemOrderLine;
use App\Order\OrderLine\OrderLine;
use App\Order\OrderLine\PaymentTypeOrderLine;
use App\Order\OrderLine\ProductOrderLine;
use App\Order\Payment\Entity\PaymentType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="`order`")
 */
class Order
{
	use MagicAccessors;
	use Identifier;

	/**
	 * @ORM\Column(type="string", length=100)
	 */
	private $orderNo;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $created;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $updated;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Customer\Customer")
	 * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=true)
	 */
	private $customer;

	/**
	 * @ORM\Column(type="integer", length=2)
	 */
	private $status;

	/**
	 * @ORM\Column(type="string", length=100, nullable=true)
	 * @var string
	 */
	private $packageNo;

	/**
	 * @ORM\OneToOne(targetEntity="\App\Order\ContactInformation", cascade={"persist"})
	 * @ORM\JoinColumn(name="contact_information_id", referencedColumnName="id")
	 */
	private $contactInformation;

	/**
	 * @ORM\OneToOne(targetEntity="\App\Order\InvoiceInformation", cascade={"persist"})
	 * @ORM\JoinColumn(name="invoice_information_id", referencedColumnName="id")
	 */
	private $invoiceInformation;

	/**
	 * @ORM\OneToOne(targetEntity="\App\Order\DeliveryAddress", cascade={"persist"})
	 * @ORM\JoinColumn(name="delivery_address_id", referencedColumnName="id", nullable=true)
	 */
	private $deliveryAddress;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 */
	private $totalPrice;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Order\OrderLine\OrderLine", mappedBy="order", cascade={"persist"}, fetch="EAGER")
	 */
	private $orderLines;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $payed;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $canceled;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $shipped;

	/**
	 * @ORM\Column(type="text")
	 */
	private $customerNote;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $lyoneesCard = false;

	/**
	 * @ORM\Column(type="string", length=15, nullable=true)
	 */
	private $birthDay;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $setUpMailing = false;

	/**
	 * @ORM\Column(type="integer", length=2)
	 */
	private $source;

	/**
	 * @ORM\OneToOne(targetEntity="\App\Order\Payment", cascade={"persist"})
	 * @ORM\JoinColumn(name="payment_id", referencedColumnName="id", nullable=true)
	 */
	private $payment;

	/**
	 * @ORM\Column(type="string", length=40, nullable=true)
	 */
	private $lyonessCardId;

	/**
	 * @ORM\Column(type="string", length=40, nullable=true)
	 */
	private $lyonessCardEan;

	/**
	 * @ORM\OneToMany(targetEntity="\App\File\OrderDocument", mappedBy="order", cascade={"persist"})
	 */
	private $documents;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Order\OrderComment", mappedBy="order", cascade={"persist"})
	 */
	private $comments;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Order\MailMessage", mappedBy="order", cascade={"persist"})
	 */
	private $mailMessages;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Order\EetReceipt", mappedBy="order", cascade={"persist"})
	 */
	private $eetReceipts;

	public function __construct(
			$source,
			ContactInformation $contactInformation,
			InvoiceInformation $invoiceInformation,
			DeliveryAddress $deliveryAddress = null,
			Customer $customer = null,
			$customerNote = ''
	){
		$this->source = $source;
		$this->contactInformation = $contactInformation;
		$this->invoiceInformation = $invoiceInformation;

		$this->deliveryAddress = $deliveryAddress;
		$this->customer = $customer;
		$this->customerNote = $customerNote;

		$this->created = new DateTime();
		$this->orderLines = new ArrayCollection();
		$this->status = OrderStatus::CREATED;
		$this->documents = new ArrayCollection();
		$this->comments = new ArrayCollection();
		$this->mailMessages = new ArrayCollection();
		$this->eetReceipts = new ArrayCollection();
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function setStatus($status)
	{
		$statuses = [
			OrderStatus::CREATED,
			OrderStatus::IN_PROCESS,
			OrderStatus::DONE
		];

		if(!in_array($status, $statuses))
		{
			throw new InvalidArgumentException;
		}

		$this->status = $status;
	}

	public function addOrderLine(OrderLine $orderLine)
	{
		$this->orderLines->add($orderLine);
	}

	public function removeOrderLine(OrderLine $orderLine)
	{
		$this->orderLines->removeElement($orderLine);
		$orderLine->delete();
	}

	public function getOrderLines()
	{
		return new ReadOnlyCollectionWrapper($this->orderLines->filter(function (OrderLine $line) {
			return !$line->isDeleted();
		}));
	}

	public function getProducts()
	{
		$collection = new ArrayCollection();

		/** @var OrderLine $orderLine */
		foreach($this->orderLines as $orderLine){
			if (!$orderLine->isDeleted() && $orderLine instanceof ProductOrderLine) {
				$collection->add($orderLine);
			}
		}

		return new ReadOnlyCollectionWrapper($collection);
	}

	public function getExtraItems()
	{
		$collection = new ArrayCollection();

		/** @var OrderLine $orderLine */
		foreach($this->orderLines as $orderLine){
			if (!$orderLine->isDeleted() && $orderLine instanceof ExtraItemOrderLine) {
				$collection->add($orderLine);
			}
		}

		return new ReadOnlyCollectionWrapper($collection);
	}

	/**
	 * @return PaymentTypeOrderLine|null
	 */
	public function getPaymentTypeOrderLine()
	{
		/** @var OrderLine $orderLine */
		foreach($this->orderLines as $orderLine){
			if (!$orderLine->isDeleted() && $orderLine instanceof PaymentTypeOrderLine) {
				return $orderLine;
			}
		}

		return null;
	}

	/**
	 * @return DeliveryOrderLine|null
	 */
	public function getDeliveryOrderLine()
	{
		/** @var OrderLine $orderLine */
		foreach($this->orderLines as $orderLine){
			if (!$orderLine->isDeleted() && $orderLine instanceof DeliveryOrderLine) {
				return $orderLine;
			}
		}

		return null;
	}

	/**
	 * @return Delivery|null
	 */
	public function getDelivery()
	{
		if ($orderLine = $this->getDeliveryOrderLine()) {
			return $orderLine->getDelivery();
		}

		return null;
	}

	public function getPaymentType()
	{
		if ($orderLine = $this->getPaymentTypeOrderLine()) {
			return $orderLine->getPaymentType();
		}

		return null;
	}

	public function getDiscount()
	{
		/** @var OrderLine $orderLine */
		foreach($this->orderLines as $orderLine){
			if (!$orderLine->isDeleted() && $orderLine instanceof DiscountOrderLine) {
				return $orderLine->getDiscount();
			}
		}

		return 0;
	}

	public function setOrderNo($orderNo)
	{
		$this->orderNo = $orderNo;
	}

	public function getOrderNo()
	{
		return $this->orderNo;
	}

	public function setCreated(DateTime $date)
	{
		$this->created = $date;
	}

	/** @return DateTime */
	public function getCreated()
	{
		return DateTime::from($this->created->getTimestamp());
	}

	public function getUpdated()
	{
		return isset($this->updated) ? DateTime::from($this->updated->getTimestamp()) : null;
	}

	public function getCustomer()
	{
		return $this->customer;
	}

	public function getContactInformation()
	{
		return $this->contactInformation;
	}

	public function getDeliveryAddress()
	{
		return $this->deliveryAddress;
	}

	public function setDeliveryAddress(DeliveryAddress $deliveryAddress = null)
	{
		$this->deliveryAddress = $deliveryAddress;
	}

	public function getInvoiceInformation()
	{
		return $this->invoiceInformation;
	}

	public function getTotalPrice()
	{
		return $this->totalPrice;
	}

	public function getPackageNo()
	{
		return $this->packageNo;
	}

	public function setDelivery(Delivery $delivery = null)
	{
		/** @var OrderLine $orderLine */
		foreach($this->orderLines as $orderLine){
			if (!$orderLine->isDeleted() && $orderLine instanceof DeliveryOrderLine) {
				if ($delivery === null) {
					$this->orderLines->removeElement($orderLine);
				} else {
					$orderLine->setDelivery($delivery);
				}

				return true;
			}
		}

		return false;
	}

	public function setPaymentType(PaymentType $paymentType = null, float $price = 0)
	{
		/** @var OrderLine $orderLine */
		foreach($this->orderLines as $orderLine){
			if (!$orderLine->isDeleted() && $orderLine instanceof PaymentTypeOrderLine) {
				if ($paymentType === null) {
					$this->orderLines->removeElement($orderLine);
				} else {
					$orderLine->setPaymentType($paymentType, $price);
				}

				return true;
			}
		}

		return false;
	}

	public function setPackageNo(string $number = null)
	{
		$this->packageNo = $number;
	}

	public function setDiscount(float $discount)
	{
		/** @var OrderLine $orderLine */
		foreach($this->orderLines as $orderLine){
			if (!$orderLine->isDeleted() && $orderLine instanceof DiscountOrderLine) {
				$orderLine->setDiscount($discount);
				return true;
			}
		}

		return false;
	}

	public function isPayed()
	{
		return !is_null($this->payed);
	}

	public function setPayed()
	{
		$this->payed = new DateTime();
		return $this;
	}

	public function isCanceled()
	{
		return !is_null($this->canceled);
	}

	public function setCanceled()
	{
		$this->canceled = new DateTime();
		return $this;
	}

	public function removeCanceled()
	{
		$this->canceled = null;
		return $this;
	}

	public function isShipped()
	{
		return /*is_null($this->getDelivery()) || */ $this->shipped instanceof \DateTime;
	}

	public function setShipped()
	{
		$this->shipped = new DateTime();
	}

	public function getShipped()
	{
		return $this->shipped;
	}

	public function removeShipped()
	{
		$this->shipped = null;
	}

	public function setTotalPrice($price)
	{
		$this->totalPrice = $price;
	}

	public function setUpdated(DateTime $updated)
	{
		$this->updated = $updated;
		return $this;
	}

	public function getCustomerNote()
	{
		return $this->customerNote;
	}

	public function setCustomerNote($note)
	{
		$this->customerNote = $note;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function hasLyoneesCard()
	{
		return $this->lyoneesCard;
	}

	public function setLyoneesCard()
	{
		$this->lyoneesCard = true;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getBirthDay()
	{
		return $this->birthDay;
	}

	/**
	 * @param mixed $birthDay
	 */
	public function setBirthDay($birthDay)
	{
		$this->birthDay = $birthDay;
		return $this;
	}

	public function setUpMailing()
	{
		$this->setUpMailing = true;
		return $this;
	}

	/**
	 * @return OrderDocument
	 */
	public function getReceipt()
	{
		$documentType = OrderDeliverability::isOrderByDeliveryInvoicable($this->getPaymentType()) ? OrderDocumentType::INVOICE : OrderDocumentType::BILL;
		return $this->getReceiptByType($documentType);
	}

	public function getCancelReceipt()
	{
		return $this->getReceiptByType(OrderDocumentType::CANCEL);
	}

	public function getBillCorrectionDocument()
	{
		return $this->getReceiptByType(OrderDocumentType::CORRECTIVE);
	}

	public function getReceiptByType($type)
	{
		$condition = Criteria::create()->where(
			Criteria::expr()->andX(
				Criteria::expr()->eq('deleted', 0),
				Criteria::expr()->eq('type', $type)
			)
		);

		return $this->documents->matching($condition)->first();
	}

	public function getDocuments()
	{
		$condition = Criteria::create()->where(
			Criteria::expr()->eq('deleted', 0)
		);
		return new ReadOnlyCollectionWrapper($this->documents->matching($condition));
	}

	public function getOneDocument($id)
	{
		return $this->documents->matching(
			Criteria::create()->where(
				Criteria::expr()->eq('id', $id)
			)
		)->first();
	}

	public function removeDocument(OrderDocument $document)
	{
		$this->documents->removeElement($document);
	}

	public function addDocument(OrderDocument $document)
	{
		if ($this !== $document->getOrder()) {
			throw new InvalidArgumentException('Document has different Order from the one you are trying to attach it.');
		}
		$this->documents->add($document);
	}

	public function getComments()
	{
		return new ReadOnlyCollectionWrapper($this->comments);
	}

	public function addComment(OrderComment $comment)
	{
		$this->comments->add($comment);
		return $this;
	}

	public function getMailMessages()
	{
		return new ReadOnlyCollectionWrapper($this->mailMessages);
	}

	public function addMailMessage(MailMessage $message)
	{
		$this->mailMessages->add($message);
		return $this;
	}

	public function setPayment(Payment $payment)
	{
		$this->payment = $payment;
		return $this;
	}

	/**
	 * @return Payment
	 */
	public function getPayment()
	{
		return $this->payment;
	}

	public function getUnavailableProducts()
	{
		return $this->getProducts()->filter(function(ProductOrderLine $line){
			return $line->getState() == OrderLineState::DAMAGED || $line->getState() == OrderLineState::SOLD_OUT;
		});
	}

	public function isEditable()
	{
		return !$this->isCanceled() /*&& !$this->isShipped()*/ && !$this->getReceipt();
	}

	public function isCreatedToday()
	{
		$today = new DateTime();
		$today->setTime(0, 0, 0);

		$created = $this->getCreated()->setTime(0, 0, 0);

		$diff = $today->diff($created);

		return $diff->days == 0;
	}

	/**
	 * @return mixed
	 */
	public function getLyonessCardId()
	{
		return $this->lyonessCardId;
	}

	/**
	 * @param mixed $lyonessCardId
	 */
	public function setLyonessCardId($lyonessCardId)
	{
		$this->lyonessCardId = $lyonessCardId;
	}

	/**
	 * @return mixed
	 */
	public function getLyonessCardEan()
	{
		return $this->lyonessCardEan;
	}

	/**
	 * @param mixed $lyonessCardEan
	 */
	public function setLyonessCardEan($lyonessCardEan)
	{
		$this->lyonessCardEan = $lyonessCardEan;
		return $this;
	}

	public function getEetReceipts()
	{
		return new ReadOnlyCollectionWrapper($this->eetReceipts);
	}

	/**
	 * @param EetReceipt $receipt
	 * @return $this
	 */
	public function addEetReceipt(EetReceipt $receipt)
	{
		$this->eetReceipts->add($receipt);
		return $this;
	}

	/**
	 * @return EetReceipt
	 */
	public function getLastProcessedEetReceipt()
	{
		$criteria = Criteria::create()
						//->where(Criteria::expr()->eq('processed', 1))
						->orderBy(['sendDate' => Criteria::DESC])
						->setMaxResults(1);

		return $this->eetReceipts->matching($criteria)->first();
	}

}