<?php

namespace App\Order;

use App\InvalidStateException;
use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Payment extends BaseEntity
{

	const TYPE_CASH = 1;
	const TYPE_BY_CARD = 2;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\Order", inversedBy="orderLines")
	 * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
	 */
	private $order;

	/**
	 * @ORM\Column(type="integer")
	 * @var float
	 */
	private $paidBy;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
	private $receivedAmount;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
	 * @var float
	 */
	private $paymentByCardCharge;

	public function __construct(Order $order, $paidBy, $receivedAmount, $paymentByCardCharge = null)
	{
		$this->order = $order;
		$this->paidBy = $paidBy;
		$this->receivedAmount = $receivedAmount;
		$this->paymentByCardCharge = $paymentByCardCharge;
	}

	/**
	 * @return float
	 */
	public function getPaidBy()
	{
		return $this->paidBy;
	}

	public function isPaidByCard()
	{
		return $this->getPaidBy() == self::TYPE_BY_CARD;
	}

	public function isPaidCash()
	{
		return $this->getPaidBy() == self::TYPE_CASH;
	}

	/**
	 * @return float
	 */
	public function getReceivedAmount()
	{
		return $this->receivedAmount;
	}

	/**
	 * @return float
	 */
	public function getPaymentByCardCharge()
	{
		return $this->paymentByCardCharge;
	}

	public function getLabel()
	{
		if ($this->isPaidByCard()) {
			return "Kartou";
		} elseif ($this->isPaidCash()) {
			return "Hotově";
		} else {
			throw new InvalidStateException('Neplatný typ platby.');
		}
	}
}