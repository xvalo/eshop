<?php

namespace App\Order;

use App\Admin\Admin;
use App\Mailing\OrderEmailType;
use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 */
class MailMessage extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\Order", inversedBy="mailMessages")
	 * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
	 */
	private $order;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Admin\Admin")
	 * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
	 */
	private $author;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $created;

	/**
	 * @ORM\Column(type="integer", length=3)
	 */
	private $type;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $path;

	public function __construct(Order $order, Admin $author, $type, $path)
	{
		$this->order = $order;
		$this->author = $author;
		$this->path = $path;
		$this->type = $type;
		$this->created = new DateTime();
	}

	/**
	 * @return mixed
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @return mixed
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * @return mixed
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * @return mixed
	 */
	public function getType()
	{
		return $this->type;
	}

	public function getTypeLabel()
	{
		return OrderEmailType::$labels[$this->type];
	}
}