<?php

namespace App\Model;

use App\Traits\IRemovable;
use Nette\SmartObject;
use Nette\Utils\Strings;

abstract class BaseService
{
	use SmartObject;

    /**
     * @var BaseRepository
     */
    protected $repository;

    public function __construct(BaseRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $id
     * @return null|BaseEntity
     */
    public function getOne($id)
    {
        return $this->repository->find($id);
    }

    public function update($entity, array $data)
    {
        foreach($data as $key => $value){
            $setter = 'set'. Strings::firstUpper($key);
            switch($key){
                default:
                    $entity->{$setter}($value);
                    break;
            }
        }

        $this->repository->flush();
        return $entity;
    }

    public function remove(IRemovable $entity)
    {
        $entity->delete();
        $this->repository->flush();
    }
}