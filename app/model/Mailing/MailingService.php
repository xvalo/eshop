<?php

namespace App\Mailing;

use App\Customer\CustomerRepository;
use App\Model\BaseService;

class MailingService extends BaseService
{
    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * MailingService constructor.
     * @param MailingRepository $repository
     * @param CustomerRepository $customerRepository
     */
    public function __construct(
        MailingRepository $repository,
        CustomerRepository $customerRepository
    ){
        parent::__construct($repository);
        $this->customerRepository = $customerRepository;
    }

	/**
	 * @param string $email
	 * @param bool $orderMailing
	 * @param bool $newsletterConfirmed
	 * @param bool $adsConfirmed
	 * @throws \Exception
	 */
    public function setMailing(string $email, bool $orderMailing, bool $newsletterConfirmed, bool $adsConfirmed)
    {
        /** @var Mailing $mailing */
        $mailing = $this->repository->findOneBy(['email' => $email]);

        if(!$mailing){
            $customer = $this->customerRepository->getCustomerByEmail($email);
            $mailing = new Mailing($email, $orderMailing, $newsletterConfirmed, $adsConfirmed, $customer);
            $this->repository->persist($mailing);
        } else {
        	if ($orderMailing) {
		        $mailing->setOrderMailing($orderMailing);
	        }

        	if ($newsletterConfirmed) {
		        $mailing->setNewsletterConfirmed($newsletterConfirmed);
	        }

	        if ($adsConfirmed) {
		        $mailing->setAdsConfirmed($adsConfirmed);
	        }
        }
	    $this->repository->flush();
    }
}