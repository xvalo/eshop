<?php

namespace App\Mailing;

use App\Customer\Customer;
use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     indexes={
 *		    @ORM\Index(name="email_index", columns={"email"})
 *     },
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="email_unique",columns={"email"})
 *     }
 * )
 */
class Mailing extends BaseEntity
{
    /**
     * @ORM\Column(type="string", length=200)
     */
    private $email;

    /**
     * @ORM\OneToOne(targetEntity="\App\Customer\Customer", inversedBy="mailing")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=true)
     * @var Customer
     */
    private $customer;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTime|null
     */
    private $orderMailing;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var DateTime|null
	 */
    private $newsletterConfirmed;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 * @var DateTime|null
	 */
    private $adsConfirmed;

    public function __construct(string $email, bool $orderMailing, bool $newsletterConfirmed, bool $adsConfirmed, Customer $customer = null)
    {
        $this->email = $email;
        $this->customer = $customer;
        $this->newsletterConfirmed = $newsletterConfirmed ? new DateTime() : null;
        $this->adsConfirmed = $adsConfirmed ? new DateTime() : null;
        $this->orderMailing = $orderMailing ? new DateTime() : null;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return Customer|null
     */
    public function getCustomer()
    {
        return $this->customer;
    }

	/**
	 * @param bool $orderMailing
	 */
    public function setOrderMailing(bool $orderMailing)
    {
        $this->orderMailing = $orderMailing ? new DateTime() : null;
    }

	/**
	 * @return DateTime|null
	 */
	public function getOrderMailing()
	{
		return $this->orderMailing;
    }

	/**
	 * @return DateTime|null
	 */
	public function getNewsletterConfirmed()
	{
		return $this->newsletterConfirmed;
	}

	/**
	 * @param bool $newsletterConfirmed
	 */
	public function setNewsletterConfirmed(bool $newsletterConfirmed)
	{
		$this->newsletterConfirmed = $newsletterConfirmed ? new DateTime() : null;
	}

	/**
	 * @return DateTime|null
	 */
	public function getAdsConfirmed()
	{
		return $this->adsConfirmed;
	}

	/**
	 * @param bool $adsConfirmed
	 */
	public function setAdsConfirmed(bool $adsConfirmed)
	{
		$this->adsConfirmed = $adsConfirmed ? new DateTime() : null;
	}


}