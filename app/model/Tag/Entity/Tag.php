<?php

namespace App\Tag;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 */
class Tag
{
	use MagicAccessors;
	use Identifier;

	/**
	 * @ORM\Column(type="string", length=20)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\ManyToMany(targetEntity="\App\Product\Product", inversedBy="tags")
	 * @ORM\JoinTable(name="tags_products")
	 */
	private $products;

	public function __construct($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getProducts()
	{
		return new ReadOnlyCollectionWrapper($this->products);
	}
}