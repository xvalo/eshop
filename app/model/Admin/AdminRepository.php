<?php

namespace App\Admin;


use App\Model\BaseRepository;

class AdminRepository extends BaseRepository
{
	/**
	 * @param string $login
	 * @return mixed|null|object
	 */
	public function getUserByLogin($login){
		return $this->findOneBy(["login" => $login]);
	}
}