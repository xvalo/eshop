<?php

namespace App\Admin;

use App\Model\BaseEntity;
use App\Traits\ActivityTrait;
use App\Traits\RemovableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;

/**
 * @ORM\Entity
 */
class Admin extends BaseEntity
{
	use ActivityTrait;
	use RemovableTrait;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $login;

	/**
	 * @ORM\Column(type="string", length=60)
	 * @var string
	 */
	private $password;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $lastName;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $superAdmin;

	/**
	 * @ORM\ManyToMany(targetEntity="\App\Admin\Privilege")
	 * @ORM\JoinTable(name="admins_privileges",
	 *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="privilege_id", referencedColumnName="id")}
	 *      )
	 */
	private $privileges;

	public function __construct($login, $password, $name, $lastName, $superAdmin = false, $active = true)
	{
		$this->login = $login;
		$this->password = $password;
		$this->name = $name;
		$this->lastName = $lastName;
		$this->superAdmin = $superAdmin;
		$this->active = $active;

		$this->privileges = new ArrayCollection();
	}

	public function setPasswordHash($hash)
	{
		$this->password = $hash;
	}

	public function getPassword()
	{
		return $this->password;
	}

	public function unsetPassword()
	{
		unset($this->password);
		return;
	}

	public function isSuperAdmin()
	{
		return $this->superAdmin;
	}

	public function getLogin()
	{
		return $this->login;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * @param string $login
	 */
	public function setLogin($login)
	{
		$this->login = $login;
	}

	/**
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$this->password = $password;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	public function activateSuperAdmin()
	{
		$this->superAdmin = true;
	}

	public function deactivateSuperAdmin()
	{
		$this->superAdmin = false;
	}

	/**
	 * @param string $lastName
	 */
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;
	}

	public function addPrivilege(Privilege $privilege)
	{
		$this->privileges->add($privilege);
	}

	public function getPrivileges()
	{
		return new ReadOnlyCollectionWrapper($this->privileges);
	}

	public function clearPrivileges()
	{
		$this->privileges->clear();
	}

	public function getPrivilegesIds()
	{
		$ids = [];

		/** @var Privilege $privilege */
		foreach ($this->privileges as $privilege) {
			$ids[] = $privilege->getId();
		}

		return $ids;
	}

}
