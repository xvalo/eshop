<?php

namespace App\Admin;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 */
class Privilege
{

	use MagicAccessors;
	use Identifier;
	
	/**
	 * @ORM\Column(type="string", length=60)
	 * @var string
	 */
	protected $name;
			
	/**
	 * @ORM\Column(type="string", length=20)
	 * @var string
	 */
	protected $code;

	/**
	 * Privilege constructor.
	 * @param $name
	 * @param $code
	 */
	public function __construct($name, $code)
	{
		$this->name = $name;
		$this->code = $code;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}
}
