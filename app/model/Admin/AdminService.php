<?php

namespace App\Admin;

use Nette\Security\Passwords;

class AdminService
{

	/**
	 * @var AdminRepository
	 */
	private $repository;
	/**
	 * @var PrivilegeRepository
	 */
	private $privilegeRepository;

	/**
	 * AdminFacade constructor.
	 * @param AdminRepository $repository
	 * @param PrivilegeRepository $privilegeRepository
	 */
	public function __construct(
		AdminRepository $repository,
		PrivilegeRepository $privilegeRepository
	){
		$this->repository = $repository;
		$this->privilegeRepository = $privilegeRepository;
	}

	/**
	 * @param int $id
	 * @return null|Admin
	 */
	public function getAdmin($id)
	{
		return $this->repository->find($id);
	}

	public function createAdmin(array $data)
	{
		$user = new Admin($data['login'], Passwords::hash($data['password']), $data['name'], $data['lastName'], $data['superAdmin'], $data['active']);

		foreach ($data['privileges'] as $privilegeId) {
			$user->addPrivilege($this->privilegeRepository->find($privilegeId));
		}

		$this->repository->persist($user);
		$this->repository->flush();

		return $user;
	}

	public function updateAdmin(array $data, Admin $user = null)
	{
		$user->setLogin($data['login']);
		$user->setName($data['name']);
		$user->setLastName($data['lastName']);

		if($data['superAdmin'])
		{
			$user->activateSuperAdmin();
		}
		else
		{
			$user->deactivateSuperAdmin();
		}

		if($data['active'])
		{
			$user->activate();
		}
		else
		{
			$user->deactivate();
		}

		if(strlen($data['password']) > 0)
		{
			$user->setPasswordHash(Passwords::hash($data['password']));
		}

		$user->clearPrivileges();
		foreach ($data['privileges'] as $privilegeId) {
			$user->addPrivilege($this->privilegeRepository->find($privilegeId));
		}

		$this->repository->flush();

		return $user;
	}

	public function getFormDefaults(Admin $user)
	{

		return [
			'id' => $user->getId(),
			'login' => $user->getLogin(),
			'name' => $user->getName(),
			'lastName' => $user->getLastName(),
			'superAdmin' => $user->isSuperAdmin(),
			'active' => $user->isActive(),
			'privileges' => $user->getPrivilegesIds()
		];
	}

}