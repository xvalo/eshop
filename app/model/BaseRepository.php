<?php

namespace App\Model;

use App\Utils\Strings;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;

abstract class BaseRepository {
	
	/** @var EntityManager */
	protected $em;

	/** @var \Kdyby\Doctrine\EntityRepository  */
	protected $repository;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		if (class_exists($this->getEntityClass())) {
			$this->repository = $this->em->getRepository($this->getEntityClass());
		}
	}


	/**
	 * Persist item to repository
	 * @param $entity
	 * @return $this
	 */
	public function persist($entity)
	{
		$this->em->persist($entity);
		return $this;
	}

	/**
	 * Flushes all changes after persisting
	 * @return $this
	 * @throws \Exception
	 */
	public function flush()
	{
		$this->em->flush();
		return $this;
	}

	/**
	 * Delete item
	 * @param $item
	 */
	public function delete($item){
		$this->em->remove($item);
		return;
	}

	/**
	 * @param $id
	 * @return null|object
	 */
	public function find($id){
		return $this->repository->find($id);
	}

	/**
	 * @param $key
	 * @param $value
	 * @param array $where
	 * @param array $orderBy
	 * @return array
	 */
	public function fetchPairs($key, $value, $where = [], $orderBy = []){
		return $this->repository->findPairs($where, $value, $orderBy, $key);
	}

	/**
	 * @param array $where
	 * @param null $orderBy
	 * @param null $limit
	 * @param null $offset
	 * @return array
	 */
	public function findBy($where = [], $orderBy = null, $limit = null, $offset = null){
		return $this->repository->findBy($where, $orderBy, $limit, $offset);
	}

	/**
	 * @param array $where
	 * @param null $orderBy
	 * @return mixed|null|object
	 */
	public function findOneBy($where = [], $orderBy = null){
		return $this->repository->findOneBy($where, $orderBy);
	}

	public function getGridData()
	{
		$entityName = $this->getEntityClassWithoutNamespace();
		$class = $this->getEntityClass();

		$qb = $this->em->createQueryBuilder()
				->select($entityName[0])
				->from($class, $entityName[0])
				->where($entityName[0].'.deleted = 0');


		return $qb;
	}

	/**
	 * Return QueryBuilder object of table selection
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	public function getQueryBuilder(){
		$entityName = $this->getEntityClassWithoutNamespace();
		$class = $this->getEntityClass();

		return $this->em->createQueryBuilder()
				->select($entityName[0])
				->from($class, $entityName[0]);
	}

	// ----------- PRIVATE METHODS ------------------

	private function getEntityClass(){
		return str_replace("Repository", "", get_called_class());
	}

	private function getEntityClassWithoutNamespace(){
		return str_replace("Repository", "", Strings::trimNamespace(get_called_class()));
	}

}
