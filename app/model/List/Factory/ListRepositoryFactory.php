<?php

namespace App\Lists\Factory;

use App\Lists\ListsTypes;
use App\Lists\ProducerRepository;
use App\Lists\StockRepository;
use App\Lists\SystemVariableRepository;
use Nette\Application\BadRequestException;

class ListRepositoryFactory
{
	/** @var ProducerRepository */
	private $producerRepository;

    /** @var StockRepository */
    private $stockRepository;

    /** @var SystemVariableRepository */
	private $systemVariableRepository;

	public function __construct(
        ProducerRepository $producerRepository,
        StockRepository $stockRepository,
		SystemVariableRepository $systemVariableRepository
    ){

		$this->producerRepository = $producerRepository;
        $this->stockRepository = $stockRepository;
		$this->systemVariableRepository = $systemVariableRepository;
	}

	public function getRepository($repository)
	{
		switch($repository)
		{
			case ListsTypes::LIST_PRODUCER:
				return $this->producerRepository;
            case ListsTypes::LIST_STOCK:
                return $this->stockRepository;
			case ListsTypes::LIST_SYSTEM_VARIABLE:
				return $this->systemVariableRepository;
			default:
				throw new BadRequestException;
		}
	}
}