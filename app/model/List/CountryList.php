<?php

namespace App\Lists;


class CountryList
{
    const CZECH_REPUBLIC = 1;
    const SLOVAK_REPUBLIC = 2;
    const AUSTRIA = 3;
    const POLAND = 4;
    const GERMANY = 5;

    public static $statesLabels = [
        self::CZECH_REPUBLIC => 'Česká republika',
        self::SLOVAK_REPUBLIC => 'Slovenská republika',
        self::AUSTRIA => 'Rakousko',
        self::POLAND => 'Polsko',
        self::GERMANY => 'Německo'
    ];

    public static function getStatusLabel($status)
    {
        return self::$statesLabels[$status];
    }
}