<?php

namespace App\Lists;

use App\Model\BaseEntity;
use App\Traits\ActivityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Vat extends BaseEntity
{
	use ActivityTrait;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="integer", length=5)
	 * @var int
	 */
	private $value;

	/**
	 * Producer constructor.
	 * @param $name
	 * @param $value
	 */
	public function __construct($name, $value)
	{
		$this->name = $name;
		$this->value = $value;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return int
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param int $value
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}
}