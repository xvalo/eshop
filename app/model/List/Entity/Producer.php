<?php

namespace App\Lists;

use App\Model\BaseEntity;
use App\Traits\IRemovable;
use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Producer extends BaseEntity implements IRemovable
{
	use RemovableTrait;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	private $value;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $url;

	/**
	 * Producer constructor.
	 * @param $name
	 * @param $value
	 */
	public function __construct($name, $value)
	{
		$this->name = $name;
		$this->value = $value;
		$this->url = Strings::webalize($this->getName());
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param string $value
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}

	public function getUrl()
	{
		return $this->url;
	}

	/** @ORM\PreUpdate */
	public function preUpdate()
	{
		$this->url = Strings::webalize($this->getName());
	}

}