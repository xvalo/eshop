<?php
namespace App\Lists;

use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class SystemVariable extends BaseEntity
{

	const TYPE_STRING = 'string';
	const TYPE_INTEGER = 'int';
	const TYPE_BOOLEAN = 'bool';

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=400, nullable=true)
     * @var string
     */
    private $value;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
    private $key;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
    private $type = self::TYPE_STRING;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

	/**
	 * @return string
	 */
	public function getKey(): string
	{
		return $this->key;
	}

	/**
	 * @param string $key
	 */
	public function setKey(string $key)
	{
		$this->key = $key;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType(string $type)
	{
		$this->type = $type;
	}
}