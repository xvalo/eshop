<?php

namespace App\Lists;

class ListsTypes
{
	const LIST_PRODUCER = 'producer';
	const LIST_STOCK = 'stock';
	const LIST_SYSTEM_VARIABLE = 'systemVariable';
}