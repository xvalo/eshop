<?php

namespace App\Lists;

use App\Lists\Factory\ListRepositoryFactory;
use App\Traits\IRemovable;
use App\Utils\Strings;
use Kdyby\Doctrine\EntityManager;

class ListsService
{
	/**
	 * @var ListRepositoryFactory
	 */
	private $repositoryFactory;

	/**
	 * @var EntityManager
	 */
	private $em;

	public function __construct(
		ListRepositoryFactory $repositoryFactory,
		EntityManager $em
	){
		$this->repositoryFactory = $repositoryFactory;
		$this->em = $em;
	}

	public function getProducersMenuList()
	{
		/** @var ProducerRepository $repository */
		$repository = $this->repositoryFactory->getRepository(ListsTypes::LIST_PRODUCER);

		$items = [];

		/** @var Producer $producer */
		foreach($repository->findBy(['deleted' => false], ['name' => 'ASC']) as $producer){
			$items[] = [
				'name' => $producer->getId(),
				'target' => ':Front:Product:producer',
				'title' => $producer->getName(),
				'parameters' => ['producer' => $producer->getUrl()],
				'items' => []
			];
		}

		return $items;
	}

	public function getProducersAlphabeticalGroups()
	{
		/** @var ProducerRepository $repository */
		$repository = $this->repositoryFactory->getRepository(ListsTypes::LIST_PRODUCER);

		$groups = [];

		/** @var Producer $producer */
		foreach($repository->findBy(['deleted' => false]) as $producer){

			$name = Strings::removeAccents(trim($producer->getName()));
			$group = is_numeric($name[0]) ? '#' : strtoupper($name[0]);

			$groups[$group][] = [
				'title' => $producer->getName(),
				'url' => $producer->getUrl()
			];
		}
		ksort($groups);

		return $groups;
	}

	public function getProducer(int $id)
	{
		return $this->repositoryFactory->getRepository(ListsTypes::LIST_PRODUCER)->find($id);
	}

	/**
	 * @param $url
	 * @return null|Producer
	 * @throws \Nette\Application\BadRequestException
	 */
	public function getProducerByUrl($url)
	{
		return $this->repositoryFactory->getRepository(ListsTypes::LIST_PRODUCER)->findOneBy(['url' => $url]);
	}

	/**
	 * @param $id
	 * @return null|Stock
	 */
	public function getStock($id)
	{
		return $this->repositoryFactory->getRepository(ListsTypes::LIST_STOCK)->find($id);
	}

	/**
	 * @return Stock[]
	 */
	public function getStocksList()
	{
		return $this->repositoryFactory->getRepository(ListsTypes::LIST_STOCK)->fetchPairs('id', 'name', ['deleted' => false]);
	}

	/**
	 * @param IRemovable $item
	 * @throws \Exception
	 */
	public function removeItem(IRemovable $item)
	{
		$item->delete();
		$this->em->flush();
	}
}