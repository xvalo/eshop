<?php

namespace App\Lists;


use App\Model\BaseRepository;

class DeliveryRepository extends BaseRepository
{
    public function getGridData()
    {
        $qb = parent::getGridData();

        $qb->orderBy($qb->expr()->asc('D.priority'));

        return $qb;
    }
}