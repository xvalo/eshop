<?php

namespace App\Lists;

use App\Model\BaseRepository;

class SystemVariableRepository extends BaseRepository
{
	public function getVariableByKey(string $key)
	{
		return $this->repository->findOneBy(['key' => $key]);
	}

    public function getGridData()
    {
        $qb = $this->em->createQueryBuilder()
                    ->select('v')
                    ->from('\App\Lists\SystemVariable', 'v');

        return $qb;
    }
}