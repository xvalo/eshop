<?php

namespace App\Lists;

use App\Category\CategoryFacade;
use App\Category\Entity\Category;
use App\Model\BaseRepository;
use App\Product\Product;
use Contributte\Cache\CacheFactory;
use Kdyby\Doctrine\Dql\Join;
use Kdyby\Doctrine\EntityManager;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;

class ProducerRepository extends BaseRepository
{
	const FRONT_FILTER_LIST = 'producers-front-category-list';
	const TAG_PREFIX = 'category-';
	const CACHE_FRONT_LIST_TAG = 'producers/front-list';

	/** @var CategoryFacade */
	private $categoryFacade;

	/** @var Cache  */
	private $cache;

	public function __construct(
		EntityManager $em,
		CategoryFacade $categoryFacade,
		CacheFactory $cacheFactory
	){
		parent::__construct($em);

		$this->categoryFacade = $categoryFacade;
		$this->cache = $cacheFactory->create(self::FRONT_FILTER_LIST);
	}

	/**
	 * @param Category $category
	 * @return string
	 * @throws \Throwable
	 */
    public function getListByCategory(Category $category): string
    {
    	$key = 'cat'.$category->getId();
    	$producersListCached = $this->cache->load($key);

    	if ($producersListCached === null) {
		    $categoriesIds = [$category->getId()] + $this->categoryFacade->getCategoryTreeChildrenIds($category);

		    $qb = $this->em->createQueryBuilder()
			    ->select('P.id, P.name AS label')
			    ->from(Producer::class, 'P')
			    ->leftJoin(Product::class, 'PR', Join::WITH, 'PR.producer = P.id')
			    ->join(Category::class, 'C', Join::WITH, 'PR.category = C.id')
		        ->groupBy('P.id');


		    $qb->where(
			    $qb->expr()->andX(
				    $qb->expr()->in('C.id', ':categories'),
				    $qb->expr()->eq('PR.active', ':active')
			    )
		    );

		    $qb->orderBy('P.name', 'ASC');

		    $qb->setParameters([
			    'categories' => $categoriesIds,
			    'active' => true
		    ]);

		    $data = $qb->getQuery()->execute();

		    $producersListCached = json_encode($data);

		    $this->cache->save($key, $producersListCached, [
		    	Cache::TAGS => [ self::CACHE_FRONT_LIST_TAG ]
		    ]);
	    }

	    return $producersListCached;
    }
}