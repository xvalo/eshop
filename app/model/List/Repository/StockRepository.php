<?php

namespace App\Lists;

use App\Model\BaseRepository;

class StockRepository extends BaseRepository
{
	public function getListOfStocks()
	{
		return $this->fetchPairs('id', 'name');
	}

	public function getMainStock()
	{
		return $this->findOneBy([], ['value' => 'ASC']);
	}
}