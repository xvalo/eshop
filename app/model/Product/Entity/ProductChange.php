<?php

namespace App\Product;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;

/**
 * @ORM\Entity
 */
class ProductChange
{
	use MagicAccessors;
	use Identifier;

	private $product;

	private $change;

	private $date;

	private $author;
}