<?php

namespace App\Product;

use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="product_old_url",
 *     indexes={@ORM\Index(name="prod_old_url_idx", columns={"url"})}
 * )
 */
class OldUrl extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="\App\Product\Product", inversedBy="oldUrls")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    public function __construct(Product $product, $url)
    {
        $this->product = $product;
        $this->url = $url;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }


}