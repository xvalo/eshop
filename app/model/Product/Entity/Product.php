<?php

namespace App\Product;

use App\Category\Entity\Category;
use App\File\Image;
use App\InvalidArgumentException;
use App\Lists\Producer;
use App\Model\BaseEntity;
use App\Product\Availability\Entity\Availability;
use App\Product\Image\Entity\ProductImage;
use App\Traits\ActivityTrait;
use App\Traits\IRemovable;
use App\Traits\RemovableTrait;
use App\Utils\Vat;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;
use Nette\Utils\Strings;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="product",
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="product_url_unique",columns={"category_id","url"})
 *     },
 *     indexes={
 *          @ORM\Index(name="prod_url_idx", columns={"url"}),
 *      }
 * )
 */
class Product extends BaseEntity implements IRemovable
{
	use ActivityTrait;
	use RemovableTrait;

	const BASE_NUMBER = 100000;

	/**
	 * @ORM\Column(type="integer", length=10)
	 */
	private $number;

	/**
	 * @ORM\Column(type="string", length=160)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=200, nullable=true)
	 */
	private $annotation;

	/**
	 * @ORM\Column(type="text")
	 */
	private $description;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Category\Entity\Category", inversedBy="products")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
	 */
	private $category;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Lists\Producer")
	 * @ORM\JoinColumn(name="producer_id", referencedColumnName="id", nullable=true)
	 */
	private $producer;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $specialOffer = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $news = false;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $mainPage = false;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $url;

	/**
	 * @ORM\ManyToMany(targetEntity="\App\Tag\Tag", mappedBy="products")
	 */
	private $tags;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Product\Variant", mappedBy="product", cascade={"persist"})
	 */
	private $variants;

	/**
	 * @ORM\OneToMany(targetEntity="App\Product\Image\Entity\ProductImage", cascade={"persist"}, mappedBy="product")
	 */
	private $images;

	/**
	 * @ORM\OneToOne(targetEntity="\App\File\Image")
	 * @ORM\JoinColumn(name="main_image_id", referencedColumnName="id", nullable=true)
	 */
	private $mainImage;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $bestSeller = false;

	/**
	 * @ORM\Column(type="integer", length=3)
	 */
	private $vat = Vat::TYPE_21;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 */
	private $minPrice;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 */
	private $normalPrice;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Product\Availability\Entity\Availability")
	 */
	private $generalAvailability;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Product\OldUrl", mappedBy="product", cascade={"persist"}, fetch="EXTRA_LAZY")
	 */
	private $oldUrls;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $stockItem = false;


	/**
	 * Product constructor.
	 * @param $name
	 * @param $description
	 * @param Category $category
	 * @param Producer $producer
	 * @param int $vat
	 * @param string|null $annotation
	 */
	public function __construct($name, $description, Category $category, Producer $producer = null, int $vat = Vat::TYPE_21, string $annotation = null)
	{
		$this->number = self::BASE_NUMBER;
		$this->name = $name;
		$this->description = $description;
		$this->category = $category;
		$this->producer = $producer;
		$this->vat = $vat;
		$this->url = Strings::webalize($name);

		$this->variants = new ArrayCollection();
		$this->images = new ArrayCollection();
		$this->tags = new ArrayCollection();
		$this->oldUrls = new ArrayCollection();
		$this->annotation = $annotation;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param mixed $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return string|null
	 */
	public function getAnnotation()
	{
		return $this->annotation;
	}

	/**
	 * @param string|null $annotation
	 */
	public function setAnnotation(string $annotation = null)
	{
		$this->annotation = $annotation;
	}

	/**
	 * @return Category
	 */
	public function getCategory()
	{
		return $this->category;
	}

	/**
	 * @param Category $category
	 */
	public function setCategory(Category $category)
	{
		$this->category = $category;
	}

	public function getCategoriesLine(Category $category = null)
	{
		if(is_null($category)){
			$category = $this->getCategory();
		}
		$categories = [ $category->getId() => $category ];

		$parent = $category->getParent();
		if(!is_null($parent)){
			$categories = array_merge($this->getCategoriesLine($parent), $categories);
		}

		return $categories;
	}

	/**
	 * @return Producer
	 */
	public function getProducer()
	{
		return $this->producer;
	}

	/**
	 * @param Producer $producer
	 */
	public function setProducer(Producer $producer = null)
	{
		$this->producer = $producer;
	}

	/**
	 * @return boolean
	 */
	public function isSpecialOffer()
	{
		return $this->specialOffer;
	}

	/**
	 * @param boolean $specialOffer
	 */
	public function setSpecialOffer($specialOffer)
	{
		$this->specialOffer = $specialOffer;
	}

	/**
	 * @return boolean
	 */
	public function isMainPage()
	{
		return $this->mainPage;
	}

	/**
	 * @param boolean $mainPage
	 */
	public function setMainPage($mainPage)
	{
		$this->mainPage = $mainPage;
	}

	public function addVariant(Variant $variant)
	{
		$this->variants->add($variant);
	}

	public function getPriceVariants()
	{
		$criteria = Criteria::create()
					->where(Criteria::expr()->eq('deleted', 0))
					->orderBy(['variantOrder' => Criteria::ASC]);

		$variants = $this->variants->matching($criteria);

		return new ReadOnlyCollectionWrapper($variants);
	}

	/**
	 * @param $id
	 * @return Variant
	 */
	public function getPriceVariant($id = null)
	{
		if ($this->hasMorePrices() && !$id) {
			throw new InvalidArgumentException('Product has more price variants. You have to specify price variant ID.');
		}

		if ($id) {
			$criteria = Criteria::create()->where(Criteria::expr()->eq('id', intval($id)));
			return $this->variants->matching($criteria)->first();
		} else {
			return $this->getPriceVariants()->first();
		}
	}

	public function hasMorePrices()
	{
		if ($this->getPriceVariants()->count() > 1) {
			return true;
		}

		return ($this->getPriceVariants()->first())->getPrices()->count() > 1;
	}

	public function addImage(Image $image, string $name = null)
	{
		$this->images->add(new ProductImage($this, $image, $name));
	}

	public function removeImage(Image $image)
	{
		return $this->images->removeElement($image);
	}

	public function getImages()
	{
		return new ReadOnlyCollectionWrapper($this->images);
	}

	public function getImagesIds()
	{
		$finalIdsArray = [];

		$ids = $this->images->map(function($image){
					return $image->getImage()->getId();
				})->toArray();

		foreach($ids as $id){
			$finalIdsArray[$id] = $id;
		}

		return $finalIdsArray;
	}

	/**
	 * @return Image
	 */
	public function getMainImage()
	{
		//return $this->mainImage ?: $this->images->first();
		return $this->mainImage;
	}

	public function setMainImage(Image $mainImage = null)
	{
		$this->mainImage = $mainImage;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	public function setUrl($url)
	{
		$this->url = $url;
	}

	public function setNumber($number)
	{
		$this->number = $number;
	}

	public function getNumber()
	{
		return $this->number;
	}

	public function isBestSeller()
	{
		return $this->bestSeller;
	}

	public function setBestSeller($isBestSeller)
	{
		$this->bestSeller = $isBestSeller;
	}

	public function getUrlArgs()
	{
		return [ 'category' => $this->getCategory()->getUrl(), 'url' => $this->getUrl() ];
	}

	/**
	 * @return mixed
	 */
	public function getVat()
	{
		return $this->vat;
	}

	/**
	 * @param mixed $vat
	 */
	public function setVat($vat)
	{
		$this->vat = $vat;
	}

	/**
	 * @return mixed
	 */
	public function getMinPrice()
	{
		return $this->minPrice;
	}

	/**
	 * @param mixed $minPrice
	 */
	public function setMinPrice($minPrice)
	{
		$this->minPrice = $minPrice;
	}

	/**
	 * @return Availability
	 */
	public function getGeneralAvailability()
	{
		return $this->generalAvailability;
	}

	/**
	 * @param Availability $generalAvailability
	 */
	public function setGeneralAvailability(Availability $generalAvailability)
	{
		$this->generalAvailability = $generalAvailability;
	}

	public function addOldUrl(OldUrl $url)
	{
		$this->oldUrls->add($url);
	}

	/**
	 * @return bool
	 */
	public function isNews()
	{
		return $this->news;
	}

	/**
	 * @param bool $news
	 */
	public function setNews($news)
	{
		$this->news = $news;
	}

	/**
	 * @return float
	 */
	public function getNormalPrice()
	{
		return $this->normalPrice;
	}

	/**
	 * @param float $normalPrice
	 */
	public function setNormalPrice($normalPrice)
	{
		$this->normalPrice = $normalPrice;
	}

	public function getVariantNextPositionNumber()
	{
		$criteria = Criteria::create()
					->orderBy(['variantOrder' => Criteria::DESC])
					->setMaxResults(1);

		/** @var Variant $lastVariant */
		$lastVariant = $this->getPriceVariants()->matching($criteria)->first();

		return $lastVariant ? $lastVariant->getVariantOrder()+1 : 1;
	}

	/**
	 * @return bool
	 */
	public function isStockItem(): bool
	{
		return $this->stockItem;
	}

	/**
	 * @param bool $stockItem
	 */
	public function setStockItem(bool $stockItem)
	{
		$this->stockItem = $stockItem;
	}

}