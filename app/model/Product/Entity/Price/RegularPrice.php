<?php

namespace App\Product\Price;

use App\Product\Variant;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class RegularPrice extends Price
{
	/**
	 * @ORM\Column(type="boolean")
	 * @var bool
	 */
	private $synchronized = false;

	/**
	 * @param Variant $variant
	 * @param float $price
	 * @param string $name
	 * @param bool $synchronized
	 * @param float $purchasePriceWithoutVat
	 */
	public function __construct(Variant $variant, float $price, string $name = '', bool $synchronized = false, float $purchasePriceWithoutVat = 0)
	{
		parent::__construct($variant, $price, $name, $purchasePriceWithoutVat);

		if ($synchronized) {
			$this->enableSynchronization();
		} else {
			$this->disableSynchronization();
		}
	}

	/**
	 * @return bool
	 */
	public function isSynchronized(): bool
	{
		return $this->synchronized;
	}

	public function enableSynchronization()
	{
		$this->synchronized = true;
	}

	public function disableSynchronization()
	{
		$this->synchronized = false;
	}

	public function getType()
	{
		return self::TYPE_REGULAR;
	}
}