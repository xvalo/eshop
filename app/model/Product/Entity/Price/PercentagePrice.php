<?php

namespace App\Product\Price;

use App\Product\Variant;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class PercentagePrice extends Price
{
	/**
	 * @ORM\OneToOne(targetEntity="\App\Product\Price\Price")
	 * @ORM\JoinColumn(name="counted_from_id", referencedColumnName="id")
	 * @var Price
	 */
	private $countedFrom;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
	private $percentage;

	/**
	 * @param Variant $variant
	 * @param Price $countedFrom
	 * @param float $percentage
	 * @param string $name
	 */
	public function __construct(Variant $variant, Price $countedFrom, float $percentage, string $name = '', float $purchasePriceWithoutVat = 0)
	{
		$price = round($countedFrom->getPrice() * $percentage, 2);
		parent::__construct($variant, $price, $name, $purchasePriceWithoutVat);

		$this->countedFrom = $countedFrom;
		$this->percentage = $percentage;
	}

	/**
	 * @return Price
	 */
	public function getCountedFrom(): Price
	{
		return $this->countedFrom;
	}

	/**
	 * @param Price $countedFrom
	 * @return $this
	 */
	public function setCountedFrom(Price $countedFrom)
	{
		$this->countedFrom = $countedFrom;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getPercentage(): float
	{
		return $this->percentage;
	}

	/**
	 * @param float $percentage
	 * @return $this
	 */
	public function setPercentage(float $percentage)
	{
		$this->percentage = $percentage;
		return $this;
	}

	public function getType()
	{
		return self::TYPE_PRECENTAGE;
	}
}