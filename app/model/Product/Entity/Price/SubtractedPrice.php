<?php

namespace App\Product\Price;

use App\Product\Variant;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class SubtractedPrice extends Price
{
	/**
	 * @ORM\OneToOne(targetEntity="\App\Product\Price\Price")
	 * @ORM\JoinColumn(name="counted_from_id", referencedColumnName="id")
	 * @var Price
	 */
	private $countedFrom;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
	private $subtracted;

	/**
	 * @param Variant $variant
	 * @param Price $countedFrom
	 * @param float $subtracted
	 * @param string $name
	 * @param float $purchasePriceWithoutVat
	 */
	public function __construct(Variant $variant, Price $countedFrom, float $subtracted, string $name = '', float $purchasePriceWithoutVat = 0)
	{
		$price = $countedFrom->getPrice() - $subtracted;
		parent::__construct($variant, $price, $name, $purchasePriceWithoutVat);

		$this->countedFrom = $countedFrom;
		$this->subtracted = $subtracted;
	}

	/**
	 * @return Price
	 */
	public function getCountedFrom(): Price
	{
		return $this->countedFrom;
	}

	/**
	 * @param Price $countedFrom
	 * @return $this
	 */
	public function setCountedFrom(Price $countedFrom)
	{
		$this->countedFrom = $countedFrom;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getSubtracted(): float
	{
		return $this->subtracted;
	}

	/**
	 * @param float $subtracted
	 * @return $this
	 */
	public function setSubtracted(float $subtracted)
	{
		$this->subtracted = $subtracted;
		return $this;
	}

	public function getType()
	{
		return self::TYPE_SUBTRACTED;
	}
}