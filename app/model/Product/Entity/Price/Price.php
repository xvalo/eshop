<?php

namespace App\Product\Price;

use App\Model\BaseEntity;
use App\Product\Variant;
use App\Traits\RemovableTrait;
use App\Utils\Vat;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"R" = "\App\Product\Price\RegularPrice", "P" = "\App\Product\Price\PercentagePrice", "S" = "\App\Product\Price\SubtractedPrice"})
 * @ORM\Table(name="variant_price")
 */
abstract class Price extends BaseEntity
{
	use RemovableTrait;

	const TYPE_REGULAR = 'R';
	const TYPE_PRECENTAGE = 'P';
	const TYPE_SUBTRACTED = 'S';

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Product\Variant", inversedBy="prices")
	 * @ORM\JoinColumn(name="variant_id", referencedColumnName="id")
	 * @var Variant
	 */
	private $variant;

	/**
	 * @ORM\Column(type="string", length=160)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
	private $price;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float|null
	 */
	private $priceWithoutVat = 0;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float|null
	 */
	private $purchasePrice = 0;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float|null
	 */
	private $purchasePriceWithoutVat = 0;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 */
	private $margin = 0;

	/**
	 * @param Variant $variant
	 * @param float $price
	 * @param string $name
	 * @param float $purchasePriceWithoutVat
	 */
	public function __construct(Variant $variant, float $price, string $name = '', float $purchasePriceWithoutVat = 0)
	{
		$this->variant = $variant;
		$this->price = $price;
		$this->name = $name;
		$this->purchasePriceWithoutVat = $purchasePriceWithoutVat;

		$vat = Vat::countVat($price, $variant->getProduct()->getVat());
		$this->priceWithoutVat = $price - $vat;
	}

	abstract public function getType();

	/**
	 * @return Variant
	 */
	public function getVariant(): Variant
	{
		return $this->variant;
	}

	/**
	 * @param Variant $variant
	 * @return $this
	 */
	public function setVariant(Variant $variant)
	{
		$this->variant = $variant;
		return $this;
	}

	/**
	 * @return float
	 */
	public function getPrice(): float
	{
		return $this->price;
	}

	/**
	 * @param float $price
	 * @return $this
	 */
	public function setPrice(float $price)
	{
		$this->price = $price;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getPriceWithoutVat()
	{
		return $this->priceWithoutVat;
	}

	/**
	 * @param float $priceWithoutVat
	 * @return $this
	 */
	public function setPriceWithoutVat(float $priceWithoutVat = 0)
	{
		$this->priceWithoutVat = $priceWithoutVat;
		return $this;
	}

	/**
	 * @return float|null
	 */
	public function getPurchasePrice()
	{
		return $this->purchasePrice;
	}

	/**
	 * @param float $purchasePrice
	 */
	public function setPurchasePrice(float $purchasePrice = 0)
	{
		$this->purchasePrice = $purchasePrice;
	}

	/**
	 * @return float|null
	 */
	public function getPurchasePriceWithoutVat()
	{
		return $this->purchasePriceWithoutVat;
	}

	/**
	 * @param float $purchasePriceWithoutVat
	 */
	public function setPurchasePriceWithoutVat(float $purchasePriceWithoutVat = 0)
	{
		$this->purchasePriceWithoutVat = $purchasePriceWithoutVat;
	}

	/**
	 * @return float|null
	 */
	public function getMargin()
	{
		return $this->margin;
	}

	/**
	 * @param float $margin
	 */
	public function setMargin(float $margin)
	{
		$this->margin = $margin;
	}


	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return bool
	 */
	public function isSynchronized(): bool
	{
		return false;
	}

	public function getPriceName()
	{
		return $this->getVariant()->getVariantName() . (strlen($this->getName()) ? ' - ' . $this->getName() : "");
	}

	public function __toString()
	{
		return $this->getPriceName();
	}

}