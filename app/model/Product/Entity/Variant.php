<?php

namespace App\Product;

use App\InvalidArgumentException;
use App\Lists\Stock;
use App\Model\BaseEntity;
use App\Product\Availability\Entity\Availability;
use App\Product\Code\Code;
use App\Product\Code\VariantCode;
use App\Product\Price\Price;
use App\Stock\StockItem;
use App\Traits\RemovableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;

/**
 * @ORM\Entity
 * @ORM\Table(name="product_variant")
 */
class Variant extends BaseEntity
{
	use RemovableTrait;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Product\Product", inversedBy="variants")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
	 * @var Product
	 */
	private $product;

	/**
	 * @ORM\Column(type="string", length=160, nullable=true)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10, nullable=true)
	 * @var float
	 */
	private $normalPrice;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Product\Availability\Entity\Availability")
	 * @var Availability
	 */
	private $availability;

	/**
	 * @ORM\Column(type="string", length=60, nullable=true)
	 * @var string
	 */
	private $ean;

	/**
	 * @ORM\Column(type="integer", length=5)
	 * @var int
	 */
	private $variantOrder;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Stock\StockItem", mappedBy="product")
	 */
	private $stockItems;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Product\Price\Price", mappedBy="variant", cascade={"persist"})
	 */
	private $prices;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Product\Code\VariantCode", mappedBy="variant", cascade={"persist"})
	 */
	private $codes;

	public function __construct(Product $product, string $name, Availability $availability, float $normalPrice = null, string $ean = null)
	{
		$this->product = $product;
		$this->name = $name;
		$this->availability = $availability;
		$this->normalPrice = $normalPrice;
		$this->ean = $ean;
		$this->variantOrder = $product->getVariantNextPositionNumber();

		$this->stockItems = new ArrayCollection();
		$this->prices = new ArrayCollection();
		$this->codes = new ArrayCollection();
	}

	/**
	 * @return Product
	 */
	public function getProduct()
	{
		return $this->product;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return float
	 */
	public function getNormalPrice()
	{
		return $this->normalPrice;
	}

	/**
	 * @param string $normalPrice
	 */
	public function setNormalPrice($normalPrice)
	{
		$this->normalPrice = $normalPrice;
	}

	/**
	 * @return Availability
	 */
	public function getAvailability(): Availability
	{
		return $this->availability;
	}

	/**
	 * @param Availability $availability
	 */
	public function setAvailability(Availability $availability)
	{
		$this->availability = $availability;
	}

	/**
	 * @return string
	 */
	public function getEan()
	{
		return $this->ean;
	}

	/**
	 * @param string $ean
	 */
	public function setEan($ean)
	{
		$ean = (is_string($ean) && strlen($ean) == 0) ? null : $ean;
		$this->ean = $ean;
	}

	/**
	 * @return int
	 */
	public function getVariantOrder()
	{
		return $this->variantOrder;
	}

	/**
	 * @param int $variantOrder
	 */
	public function setVariantOrder($variantOrder)
	{
		$this->variantOrder = $variantOrder;
	}

	public function addStockItem(StockItem $item)
	{
		$this->stockItems->add($item);
	}

	/**
	 * @return ReadOnlyCollectionWrapper
	 */
	public function getStocksItems()
	{
		return new ReadOnlyCollectionWrapper($this->stockItems);
	}

	public function getNonZeroCountStockItems()
	{
		return new ReadOnlyCollectionWrapper($this->stockItems->filter(function(StockItem $item) {
			return $item->getCount() > 0;
		}));
	}

	/**
	 * @param Stock $stock
	 * @return StockItem|bool
	 */
	public function getStockItem(Stock $stock)
	{
		$criteria = Criteria::create()
					->where(Criteria::expr()->eq('stock', $stock));
		return $this->stockItems->matching($criteria)->first();
	}

	public function getStockCount(Stock $stock)
	{
		$stockItem = $this->getStockItem($stock);
		return $stockItem ? $stockItem->getCount() : 0;
	}

	public function getStockLimitToAlert(Stock $stock)
	{
		$stockItem = $this->getStockItem($stock);
		return $stockItem ? $stockItem->getLimitToAlert() : 0;
	}

	public function getStockPosition(Stock $stock)
	{
		$stockItem = $this->getStockItem($stock);
		return $stockItem ? $stockItem->getPosition() : '';
	}

	public function isAvailableOnStock()
	{
		return count($this->getNonZeroCountStockItems()) > 0;
	}

	public function addCode(VariantCode $code)
	{
		$this->codes->add($code);
		return $this;
	}

	public function getCodes()
	{
		$criteria = Criteria::create()->where(Criteria::expr()->eq('deleted', 0));
		return new ReadOnlyCollectionWrapper($this->codes->matching($criteria));
	}

	public function getCodesString()
	{
		$codes = [];

		/** @var Code $code */
		foreach ($this->getCodes() as $code) {
			$codes[] = $code->getCode();
		}

		return implode(' | ', $codes);
	}

	public function addPrice(Price $price)
	{
		$this->prices->add($price);
		return $this;
	}

	public function getPrices()
	{
		$criteria = Criteria::create()->where(Criteria::expr()->eq('deleted', 0));
		return new ReadOnlyCollectionWrapper($this->prices->matching($criteria));
	}

	public function getAllPrices()
	{
		return new ReadOnlyCollectionWrapper($this->prices);
	}

	public function getPrice(int $id = null)
	{
		if ($this->getPrices()->count() > 1 && is_null($id)) {
			throw new InvalidArgumentException('Variant has more prices, you have to set $id');
		}

		if ($id) {
			$criteria = Criteria::create()->where(Criteria::expr()->eq('id', intval($id)));
			return $this->getPrices()->matching($criteria)->first();
		}

		return $this->getPrices()->first();
	}

	public function getVariantName()
	{
		return $this->getProduct()->getName() . ((strlen($this->getName())) ? ' - '.$this->getName() : '');
	}

	public function __toString()
	{
		return $this->getVariantName();
	}
}