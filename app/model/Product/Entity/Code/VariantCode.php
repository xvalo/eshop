<?php

namespace App\Product\Code;

use App\InvalidArgumentException;
use App\Stock\Supplier\Entity\Supplier;
use App\Model\BaseEntity;
use App\Product\Variant;
use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="variant_code")
 */
class VariantCode extends BaseEntity
{
	use RemovableTrait;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Product\Variant", inversedBy="codes")
	 * @ORM\JoinColumn(name="variant_id", referencedColumnName="id")
	 * @var Variant
	 */
	private $variant;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Product\Code\Code", cascade={"persist"})
	 * @ORM\JoinColumn(name="code_id", referencedColumnName="id")
	 * @var Code
	 */
	private $code;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Stock\Supplier\Entity\Supplier")
	 * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id", nullable=true)
	 * @var Supplier|null
	 */
	private $supplier = null;

	/**
	 * @ORM\Column(type="boolean")
	 * @var bool
	 */
	private $priceSynchronized;

	/**
	 * @ORM\Column(type="boolean")
	 * @var bool
	 */
	private $availabilitySynchronized;

	/**
	 * @param Variant $variant
	 * @param Code $code
	 * @param Supplier $supplier
	 * @param bool $priceSynchronized
	 * @param bool $availabilitySynchronized
	 */
	public function __construct(Variant $variant, Code $code, Supplier $supplier = null, bool $priceSynchronized = false, bool $availabilitySynchronized = false)
	{
		$this->variant = $variant;
		$this->code = $code;
		$this->supplier = $supplier;
		$this->priceSynchronized = $priceSynchronized;
		$this->availabilitySynchronized = $availabilitySynchronized;
	}

	/**
	 * @return Variant
	 */
	public function getVariant(): Variant
	{
		return $this->variant;
	}

	/**
	 * @param Variant $variant
	 * @return $this
	 */
	public function setVariant(Variant $variant)
	{
		$this->variant = $variant;
		return $this;
	}

	/**
	 * @return Code
	 */
	public function getCode(): Code
	{
		return $this->code;
	}

	/**
	 * @param Code $code
	 * @return $this
	 */
	public function setCode(Code $code)
	{
		$this->code = $code;
		return $this;
	}

	/**
	 * @return Supplier|null
	 */
	public function getSupplier()
	{
		return $this->supplier;
	}

	/**
	 * @param Supplier|null $supplier
	 * @return $this
	 * @throws InvalidArgumentException
	 */
	public function setSupplier($supplier)
	{
		if (is_null($supplier) || $supplier instanceof Supplier) {
			$this->supplier = $supplier;
			return $this;
		}

		throw new InvalidArgumentException('Invalid type of supplier in argument.');
	}

	/**
	 * @return bool
	 */
	public function isPriceSynchronized(): bool
	{
		return $this->priceSynchronized;
	}

	/**
	 * @param bool $priceSynchronized
	 */
	public function setPriceSynchronized(bool $priceSynchronized)
	{
		$this->priceSynchronized = $priceSynchronized;
	}

	/**
	 * @return bool
	 */
	public function isAvailabilitySynchronized(): bool
	{
		return $this->availabilitySynchronized;
	}

	/**
	 * @param bool $availabilitySynchronized
	 */
	public function setAvailabilitySynchronized(bool $availabilitySynchronized)
	{
		$this->availabilitySynchronized = $availabilitySynchronized;
	}
}