<?php

namespace App\Product\Code;

use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Code extends BaseEntity
{
	/**
	 * @ORM\Column(type="string", length=100)
	 * @var string
	 */
	private $code;

	public function __construct(string $code)
	{
		$this->code = $code;
	}

	/**
	 * @return string
	 */
	public function getCode(): string
	{
		return $this->code;
	}

	public function __toString()
	{
		return $this->getCode();
	}
}