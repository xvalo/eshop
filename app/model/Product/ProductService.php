<?php

namespace App\Product;

use App\Category\CategoryFacade;
use App\Category\Entity\Category;
use App\File\Image;
use App\File\ImageRepository;
use App\Lists\Producer;
use App\Lists\ProducerRepository;
use App\Product\Availability\Entity\Availability;
use App\Product\Code\VariantCode;
use App\Product\Price\Price;
use App\Utils\Numbers;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Nette\Utils\ArrayHash;

class ProductService
{
	/** @var ProductRepository */
	private $repository;

	/** @var ProducerRepository */
	private $producerRepository;

	/** @var VariantRepository */
	private $variantRepository;

	/** @var ImageRepository */
	private $imageRepository;

	/** @var CategoryFacade */
	private $categoryFacade;


	/**
	 * ProductFacade constructor.
	 * @param ProductRepository $repository
	 * @param ProducerRepository $producerRepository
	 * @param VariantRepository $variantRepository
	 * @param ImageRepository $imageRepository
	 * @param CategoryFacade $categoryFacade
	 */
	public function __construct(
		ProductRepository $repository,
		ProducerRepository $producerRepository,
		VariantRepository $variantRepository,
		ImageRepository $imageRepository,
		CategoryFacade $categoryFacade
	){
		$this->repository = $repository;
		$this->producerRepository = $producerRepository;
		$this->variantRepository = $variantRepository;
		$this->imageRepository = $imageRepository;
		$this->categoryFacade = $categoryFacade;
	}

	/**
	 * @param $id
	 * @return null|Product
	 */
	public function getProduct($id)
	{
		return $this->repository->find($id);
	}

	/**
	 * @param string $url
	 * @param Category $category
	 * @return Product
	 * @throws \Doctrine\ORM\NoResultException
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function getProductByUrl(string $url, Category $category): Product
	{
		return $this->repository->getByUrl($url, $category);
	}

	public function deleteProduct($id)
	{
		/** @var Product $product */
		$product = $this->repository->find($id);

		/** @var Variant $variant */
		foreach ($product->getPriceVariants() as $variant) {
			$this->deletePriceVariant($variant->getId(), false);
		}

		$product->delete();
		$this->repository->flush();
	}

	/**
	 * @param $data
	 * @return Product
	 * @throws \Exception
	 * @throws UniqueConstraintViolationException
	 */
	public function createProduct($data)
	{
		/** @var Category $category */
		$category = $this->categoryFacade->getCategory(intval($data->category));
		/** @var Producer $producer */
		$producer = $data->producer ? $this->producerRepository->find($data->producer) : null;

		$product = new Product($data->name, $data->description, $category, $producer, $data->vat, $data->annotation);

		$this->repository->persist($product);
		$this->repository->flush();

		$product->setNumber($product->getNumber() + $product->getId());
		$this->repository->flush();

		return $product;
	}

	/**
	 * @param $data
	 * @param Product $product
	 */
	public function updateProduct($data, Product $product)
	{
		$product->setName($data->name);
		$product->setDescription($data->description);
		$product->setMainPage($data->mainPage);
		$product->setSpecialOffer($data->specialOffer);
		$product->setNews($data->news);
		$product->setBestSeller($data->bestSeller);
		$product->setVat($data->vat);
		$product->setAnnotation($data->annotation);
		$product->setStockItem($data->stockItem);

		if($data->category != $product->getCategory()->getId())
		{
			/** @var Category $category */
			$category = $this->categoryFacade->getCategory((int)$data->category);
			$product->setCategory($category);
		}

		$producerId = ($product->getProducer()) ? $product->getProducer()->getId() : null;
		if($data->producer != $producerId)
		{
			/** @var Producer $producer */
			$producer = ($data->producer) ? $this->producerRepository->find($data->producer) : null;
			$product->setProducer($producer);
		}

		if (isset($data->mainImage)) {
			$product->setMainImage($this->imageRepository->find($data->mainImage));
		}

		if($data->active)
		{
			$product->activate();
		}
		else
		{
			$product->deactivate();
		}

		$this->repository->flush();
	}

	public function updatePrice(Variant $price, Availability $availability, string $name = null, string $ean = null, float $normalPrice = 0)
	{
		$price->setEan($ean);
		$price->setName($name);
		$price->setAvailability($availability);
		$price->setNormalPrice(Numbers::floatFromCzechNumber($normalPrice));

		$this->variantRepository->saveVariant($price);
	}

	public function getFormDefaults(Product $product)
	{
		return [
			'name' => $product->getName(),
			'producer' => ($product->getProducer() && !$product->getProducer()->isDeleted()) ? $product->getProducer()->getId() : null,
			'description' => $product->getDescription(),
			'category' => $product->getCategory()->getId(),
			'active' => $product->isActive(),
			'specialOffer' => $product->isSpecialOffer(),
			'news' => $product->isNews(),
			'mainPage' => $product->isMainPage(),
			'bestSeller' => $product->isBestSeller(),
			'mainImage' => $product->getMainImage() ? $product->getMainImage()->getId() : null,
			'vat' => $product->getVat(),
			'annotation' => $product->getAnnotation(),
			'stockItem' =>  $product->isStockItem()
		];
	}

	public function getFilteredProducts($query)
	{
		return $this->repository->filterProducts($query);
	}

	public function getFilteredProductsCount($query)
	{
		return $this->repository->getFilteredProductCount($query);
	}

	public function searchProductsAndVariants($q, $active = true, $groupByVariants = true)
	{
		$foundProducts = $this->repository->searchProductAndVariants($q, $active, $groupByVariants);
		$products = [];

		foreach ($foundProducts as $item) {
			$products[] = [
				'id' => $item['id'],
				'variantId' => $item['variantId'],
				'productId' => $item['productId'],
				'number' => $item['number'],
				'code' => $item['code'],
				'name' => $item['product'],
				'variant' => $item['variant'],
				'priceName' => $item['priceName'],
				'title' => $item['product'] .
							' (' . $item['code'] . (strlen($item['variant']) > 0 ? ' - ' .$item['variant'] : '') . (strlen($item['priceName']) > 0 ? ' - ' .$item['priceName'] : '') . ')'
			];
		}

		return $products;
	}

	public function searchProducts($q)
	{
		$products = $this->repository->searchProductByName($q);
		$data = [];

		foreach ($products as $product) {
			$data[] = [
				"id" => $product['id'],
				"text" => '('.$product['number'].') '.$product['product']
			];
		}

		return $data;
	}

	/**
	 * @param $id
	 * @return null|Variant
	 */
	public function getPriceVariant($id)
	{
		return $this->variantRepository->getVariant((int)$id);
	}

	public function getVariantForApi($id)
	{
		$price = $this->variantRepository->findPrice((int)$id);

		return [
			'id' => $price->getId(),
			'name' => $price->getPriceName(),
			'price' => $price->getPrice(),
			'code' => $price->getVariant()->getCodesString()
		];
	}

	public function getPriceVariants($ids)
	{
		return $this->variantRepository->getPrices($ids);
	}

	public function deletePriceVariant($id, $flushChanges = true)
	{
		/** @var Variant $priceVariant */
		$priceVariant = $this->variantRepository->getVariant($id);

		/** @var VariantCode $variantCode */
		foreach ($priceVariant->getCodes() as $variantCode) {
			$variantCode->delete();
		}

		/** @var Price $variantPrice */
		foreach ($priceVariant->getPrices() as $variantPrice) {
			$variantPrice->delete();
		}

		$priceVariant->delete();

		if($flushChanges) {
			$this->repository->flush();
		}
	}

	public function removeImage(Product $product, Image $image)
	{
		$imagePath = WWW_DIR.$image->getRelativePath();
		$product->removeImage($image);
		$this->imageRepository->delete($image);
		$this->repository->flush();
		unlink($imagePath);
	}

	public function changeVariantsOrder(Variant $item, Variant $previousItem = null, Variant $nextItem = null)
	{
		$itemsToMoveUp = $this->variantRepository->getItemsToMoveUp($item, $previousItem);

		/** @var Variant $variant */
		foreach ($itemsToMoveUp as $variant) {
			$variant->setVariantOrder($variant->getVariantOrder() - 1);
		}

		$itemsToMoveDown = $this->variantRepository->getItemsToMoveDown($item, $nextItem);

		/** @var Variant $variant */
		foreach ($itemsToMoveDown as $variant) {
			$variant->setVariantOrder($variant->getVariantOrder() + 1);
		}

		if ($previousItem) {
			$item->setVariantOrder($previousItem->getVariantOrder() + 1);
		} else if ($nextItem) {
			$item->setVariantOrder($nextItem->getVariantOrder() - 1);
		} else {
			$item->setVariantOrder(1);
		}

		$this->variantRepository->saveVariant($item);
	}

	public function setPriceVariantsOrder(Product $product)
	{
		$counter = 1;

		/** @var Variant $priceVariant */
		foreach ($product->getPriceVariants() as $priceVariant) {
			$priceVariant->setVariantOrder($counter);
			$counter++;
		}

		$this->repository->flush();
	}

	public function getBestSellerProducts()
	{
		return $this->repository->getMostPopularProducts();
	}
}