<?php

namespace App\Product;

use App\Category\Entity\Category;
use App\Model\BaseRepository;
use App\Order\OrderLine\ProductOrderLine;
use Kdyby\Doctrine\Dql\Join;

class ProductRepository extends BaseRepository
{

    public function getFilteredProducts(array $filters = [])
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('p')
            ->from('\App\Product\Product', 'p')
            ->leftJoin('p.category', 'c')
            ->leftJoin('p.mainImage', 'i');

        $qb->where('p.active = 1')
            ->andWhere('p.deleted = 0');

        $parameters = [];

        foreach($filters as $alias => $group){
            foreach($group as $column => $value){
                if(is_array($value)){
                    $qb->andWhere($alias.'.'.$column . ' IN (:' . $column.')');
                }else{
                    $qb->andWhere($alias.'.'.$column . ' = :' . $column);
                }
                $parameters[$column] = $value;
            }
        }

        $qb->setParameters($parameters);

        return $qb;
    }

    public function searchProductAndVariants($q, $active = true, $groupByVariants = true)
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select(['PR.id AS id, V.id AS variantId, P.id AS productId, P.name AS product, V.name AS variant, PR.name AS priceName, C.code AS code, P.number AS number'])
            ->from(Product::class, 'P')
                ->join('P.variants', 'V')
                ->join('V.codes', 'VC')
                ->join('VC.code', 'C')
                ->join('V.prices', 'PR');

        $activityCondition = $active ? $qb->expr()->eq('P.active', $active) : $qb->expr()->eq(1, 1);
        $where = $qb->expr()->andX(
            $activityCondition,
            $qb->expr()->eq('P.deleted', 0),
	        $qb->expr()->eq('V.deleted', 0),
	        $qb->expr()->eq('VC.deleted', 0),
	        $qb->expr()->eq('PR.deleted', 0),
            $qb->expr()->orX(
                $qb->expr()->like('P.name', ':phrase'),
                $qb->expr()->like('P.number', ':phrase'),
                $qb->expr()->like('V.ean', ':phrase'),
                $qb->expr()->like('V.name', ':phrase'),
                $qb->expr()->like('C.code', ':phrase'),
	            $qb->expr()->like('PR.name', ':phrase')
            )
        );

        $qb->where($where);

        if ($groupByVariants) {
	        $qb->groupBy('V.id');
        }

        $qb->setParameter('phrase', '%'.$q.'%');

        return $qb->getQuery()->execute();
    }

	public function searchProductByName($like)
	{
		$qb = $this->em->createQueryBuilder();

		$qb->select(['P.id, P.name AS product, P.number AS number, I.path AS imagePath'])
			->from(Product::class, 'P')
				->join('P.category', 'C')
				->leftJoin('P.mainImage', 'I');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('P.deleted', ':deleted'),
				$qb->expr()->orX(
					$qb->expr()->like('P.name', ':phrase'),
					$qb->expr()->like('P.number', ':phrase')
				)
			)
		);

		$qb->setParameters([
			'deleted' => false,
			'phrase' => '%'.$like.'%'
		]);

		return $qb->getQuery()->execute();

    }

    public function filterProducts($query)
    {
        return $this->em->fetch($query);
    }

    public function getFilteredProductCount($query)
    {
        return $this->filterProducts($query)->count();
    }

	public function getProductsCount()
	{
		return $this->repository->countBy(['deleted' => 0]);
    }

	/**
	 * @param string $url
	 * @param Category $category
	 * @return Product
	 * @throws \Doctrine\ORM\NoResultException
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
    public function getByUrl(string $url, Category $category): Product
    {
        $qb = $this->em->createQueryBuilder()
                    ->select('p')
                    ->from('\App\Product\Product', 'p')
                        ->join('p.category', 'c')
                        ->leftJoin('p.oldUrls', 'u');

        $where = $qb->expr()->andX(
            $qb->expr()->eq('p.deleted', ':deleted'),
            $qb->expr()->eq('p.active', ':active'),
            $qb->expr()->eq('p.category', ':category'),
            $qb->expr()->eq('c.active', ':active'),
            $qb->expr()->orX(
                $qb->expr()->like('p.url', ':url'),
                $qb->expr()->like('u.url', ':url')
            )
        );

        $qb->where($where);

        $qb->setParameters([
        	'url' => $url,
	        'active' => true,
	        'deleted' => false,
	        'category' => $category
        ]);

        return $qb->getQuery()->getSingleResult();
    }

	public function getMostPopularProducts()
	{
		$qb = $this->em->createQueryBuilder()
			->select('P.name, COUNT(P.id) AS productCount')
			->from(Product::class, 'P')
				->innerJoin('P.variants', 'V')
				->innerJoin('V.prices', 'PR')
				->innerJoin(ProductOrderLine::class, 'O', Join::WITH, 'O.price = PR.id')
			->groupBy('P.id')
			->having('COUNT(P.id) >= 1')
			->orderBy('productCount', 'DESC');

		return $qb->getQuery()->execute();
	}

	public function getProductsInCategory(Category $category)
	{
		$qb = $this->em->createQueryBuilder()
			->select('P')
			->from(Product::class, 'P');
		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('P.category', ':category'),
				$qb->expr()->eq('P.deleted', ':deleted')
			)
		)->setParameters([
			'category' => $category,
			'deleted' => false
		]);

		return $qb->getQuery()->execute();

	}

	/**
	 * @param int[] $categoriesIds
	 * @return array
	 */
	public function getProductsIdsByCategories(array $categoriesIds)
	{
		return array_column($this->em->getConnection()->executeQuery('SELECT id FROM product WHERE category_id IN (?)', [implode(',', $categoriesIds)])->fetchAll(), 'id');
	}
}