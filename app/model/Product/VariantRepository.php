<?php

namespace App\Product;

use App\Stock\Supplier\Entity\Supplier;
use App\Product\Availability\AvailabilityProvider;
use App\Product\Code\Code;
use App\Product\Code\VariantCode;
use App\Product\Price\Price;
use App\Product\Price\RegularPrice;
use Kdyby\Doctrine\Dql\Join;
use Kdyby\Doctrine\EntityManager;

class VariantRepository
{
	/** @var EntityManager */
	private $em;

	/** @var \Kdyby\Doctrine\EntityRepository  */
	private $variantRepository;

	/** @var AvailabilityProvider */
	private $availabilityProvider;

	public function __construct(
		EntityManager $em,
		AvailabilityProvider $availabilityProvider
	){
		$this->em = $em;
		$this->availabilityProvider = $availabilityProvider;
		$this->variantRepository = $this->em->getRepository(Variant::class);
	}

	public function getVariant(int $id)
	{
		return $this->variantRepository->find($id);
	}

	public function saveVariant(Variant $variant)
	{
		if ($variant->getId() === null) {
			$this->em->persist($variant);
		}

		$this->em->flush();
	}

	public function saveCode(Code $code)
	{
		if ($code->getId() === null) {
			$this->em->persist($code);
		}

		$this->em->flush();
	}

	public function getProductVariants(Product $product)
	{
		return $this->em->createQueryBuilder()
					->select('V')
					->from(Variant::class, 'V')
					->where('V.product = :product')
						->andWhere('V.deleted = 0')
					->orderBy('V.variantOrder')
					->setParameter('product', $product);
	}

	public function getPriceVariantsToUpdate()
	{
		$qb = $this->em->createQueryBuilder()
				->select('P AS price, IDENTITY(V.availability) AS availability')
				->from(RegularPrice::class, 'P')
					->join('P.variant', 'V')
					->join('V.product', 'PROD');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('PROD.deleted', ':prodDeleted'),
				$qb->expr()->eq('V.deleted', ':variantDeleted'),
				$qb->expr()->eq('P.deleted', ':priceDeleted'),
				$qb->expr()->neq('V.availability', ':availability'),
				$qb->expr()->eq('P.synchronized', ':synchronized')
			)
		);

		$qb->setParameters([
			'prodDeleted' => false,
			'variantDeleted' => false,
			'priceDeleted' => false,
			'availability' => $this->availabilityProvider->getOnStockStatus(),
			'synchronized' => true
		]);

		return $qb->getQuery()->execute();
	}

	public function getProductsOnStockLimit()
	{
		$qb = $this->em->createQueryBuilder()
			->select('P.id, P.name AS product, SI.count, S.name AS shop')
			->from(Variant::class, 'PV')
				->join('PV.product', 'P')
				->join('P.category', 'C')
				->leftJoin('P.stockItems', 'SI')
				->join('SI.stock', 'S')
			->where('P.deleted = 0')
			->andWhere('SI.count <= SI.limitToAlert');

		return $qb->getQuery()->execute();
	}

	public function getPriceVariantByCode($code)
	{
		$qb = $this->em->createQueryBuilder()
			->select('P')
			->from(Variant::class, 'P')
			->where('P.code = :code')
				->andWhere('P.deleted = 0');

		$qb->setParameters([
			'code' => $code
		]);

		return $qb->getQuery()->getSingleResult();
	}

	public function getPriceVariantByEan($ean)
	{
		$qb = $this->em->createQueryBuilder()
			->select('P')
			->from('\App\Product\Variant', 'P')
			->where('P.ean = :ean')
				->andWhere('P.deleted = 0');

		$qb->setParameters([
			'ean' => $ean
		]);

		return $qb->getQuery()->getSingleResult();
	}

	public function getPriceVariant(Product $product = null, string $code = null, string $ean = null, bool $sorted = false, bool $onlyActive = true)
	{
		$qb = $this->em->createQueryBuilder()
			->select('V')
				->from(Variant::class, 'V');

		if ($product) {
			$qb->andWhere(
				$qb->expr()->eq('V.product', ':product')
			);
			$qb->setParameter('product', $product);
		}

		if ($code) {
			$qb->join('V.codes', 'VC')
				->join('VC.code', 'C');
			$qb->andWhere(
				$qb->expr()->like('C.code', ':code')
			);
			$qb->setParameter('code', $code);
		}

		if ($ean) {
			$qb->andWhere(
				$qb->expr()->like('V.ean', ':ean')
			);
			$qb->setParameter('ean', $code);
		}

		if ($sorted) {
			$qb->orderBy('V.variantOrder');
		}

		if ($onlyActive) {
			$qb->andWhere(
				$qb->expr()->like('V.deleted', ':deleted')
			);
			$qb->setParameter('deleted', 0);
		}

		return $qb->getQuery()->getSingleResult();
	}

	public function findCode(string $code)
	{
		return $this->em->getRepository(Code::class)->findOneBy(['code' => $code]);
	}

	public function getAllCodes()
	{
		$qb = $this->em->createQueryBuilder()
				->select('C.code')
				->from(Code::class, 'C');
		return $qb->getQuery()->getArrayResult();
	}

	public function getVariantCodes(Variant $variant)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('C')
			->from(VariantCode::class, 'C')
			->where($qb->expr()->eq('C.variant', ':variant'));

		$qb->setParameters([
			'variant' => $variant
		]);

		return $qb->getQuery()->execute();
	}

	/**
	 * @param Supplier $supplier
	 * @param Code $code
	 * @return RegularPrice[]|null
	 */
	public function getPricesByCode(Supplier $supplier, Code $code)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('P')
			->from(RegularPrice::class, 'P')
				->join('P.variant', 'V')
				->join('V.codes', 'VC')
			->where(
				$qb->expr()->andX(
					$qb->expr()->eq('VC.supplier', ':supplier'),
					$qb->expr()->eq('VC.code', ':code'),
					$qb->expr()->eq('VC.deleted', ':deleted'),
					$qb->expr()->eq('P.synchronized', ':synchronized')
				)
			);

		$qb->setParameters([
			'code' => $code,
			'supplier' => $supplier,
			'deleted' => false,
			'synchronized' => true
		]);

		return $qb->getQuery()->execute();
	}

	/**
	 * @param Supplier $supplier
	 * @param Code $code
	 * @return mixed
	 * @throws \Doctrine\ORM\NoResultException
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function findVariantCode(Supplier $supplier, Code $code)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('VC')
			->from(VariantCode::class, 'VC')
			->where(
				$qb->expr()->andX(
					$qb->expr()->eq('VC.supplier', ':supplier'),
					$qb->expr()->eq('VC.code', ':code'),
					$qb->expr()->eq('VC.deleted', ':deleted')
				)
			);

		$qb->setParameters([
			'code' => $code,
			'supplier' => $supplier,
			'deleted' => false
		]);

		return $qb->getQuery()->getSingleResult();
	}

	/**
	 * @param int $id
	 * @return null|VariantCode
	 */
	public function getVariantCodeRelationById(int $id)
	{
		return $this->em->getRepository(VariantCode::class)->find($id);
	}

	public function isExistingCodeSupplierRelation(Supplier $supplier, Code $code)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('COUNT(VC)')
			->from(VariantCode::class, 'VC')
			->where(
				$qb->expr()->andX(
					$qb->expr()->eq('VC.supplier', ':supplier'),
					$qb->expr()->eq('VC.code', ':code'),
					$qb->expr()->eq('VC.deleted', ':deleted')
				)
			);

		$qb->setParameters([
			'code' => $code,
			'supplier' => $supplier,
			'deleted' => false
		]);

		$count = $qb->getQuery()->getSingleScalarResult();

		return $count > 0;
	}

	/**
	 * @param int $id
	 * @return Price|null
	 */
	public function findPrice(int $id)
	{
		return $this->em->getRepository(Price::class)->find($id);
	}

	public function getPrices(array $ids)
	{
		return $this->em->getRepository(Price::class)->findBy(['id' => $ids]);
	}

	public function getVariantCodeWithoutSupplier(string $code)
	{
		$qb = $this->em->createQueryBuilder()
				->select('VC')
				->from(VariantCode::class, 'VC')
				->join('VC.code', 'C');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->like('C.code', ':code'),
				$qb->expr()->isNull('VC.supplier'),
				$qb->expr()->eq('VC.deleted', ':deleted')
			)
		);
		$qb->setParameters([
			'code' => $code,
			'deleted' => false
		]);

		return $qb->getQuery()->getSingleResult();
	}

	public function getAllPrices()
	{
		return $this->em->getRepository(Price::class)->findBy([]);
	}

	public function getItemsToMoveUp(Variant $variant, Variant $previousItem = null)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('V')
			->from(Variant::class, 'V')
			->where(
				$qb->expr()->andX(
					$qb->expr()->lte('V.variantOrder', ':lower'),
					$qb->expr()->gt('V.variantOrder', ':higher'),
					$qb->expr()->eq('V.product', ':product')
				)
			);

		$qb->setParameters([
			'lower' => $previousItem ? $previousItem->getVariantOrder() : 0,
			'higher' => $variant->getVariantOrder(),
			'product' => $variant->getProduct()
		]);

		return $qb->getQuery()->getResult();
	}

	public function getItemsToMoveDown(Variant $variant, Variant $nextItem = null)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('V')
			->from(Variant::class, 'V')
			->where(
				$qb->expr()->andX(
					$qb->expr()->gte('V.variantOrder', ':higher'),
					$qb->expr()->lt('V.variantOrder', ':lower'),
					$qb->expr()->eq('V.product', ':product')
				)
			);

		$qb->setParameters([
			'higher' => $nextItem ? $nextItem->getVariantOrder() : 0,
			'lower' => $variant->getVariantOrder(),
			'product' => $variant->getProduct()
		]);

		return $qb->getQuery()->getResult();
	}

	public function getVariantsCodesToSynchronize()
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('P.id AS priceId, P.price as priceValue, V.id as variantId, IDENTITY(V.availability) AS availability, C.code AS code, VC.priceSynchronized AS priceSynchronized, IDENTITY(VC.supplier) AS supplierId')
			->from(RegularPrice::class, 'P')
			->join('P.variant', 'V')
			->join(VariantCode::class, 'VC', Join::WITH, 'VC.variant = V AND VC.supplier IS NOT NULL')
			->join('VC.code', 'C');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('V.deleted', ':variantDeleted'),
				$qb->expr()->eq('P.deleted', ':priceDeleted'),
				$qb->expr()->neq('V.availability', ':availability'),
				$qb->expr()->eq('P.synchronized', ':synchronized')
			)
		);

		$qb->setParameters([
			'variantDeleted' => false,
			'priceDeleted' => false,
			'availability' => $this->availabilityProvider->getOnStockStatus(),
			'synchronized' => true
		]);

		return $qb->getQuery()->getArrayResult();
	}

}