<?php
namespace App\Event;

use App\Model\BaseService;
use Nette\Utils\DateTime;

class EventService extends BaseService
{
    public function __construct(EventRepository $repository)
    {
        parent::__construct($repository);
    }

    public function create(array $data)
    {
        $eventDate = DateTime::createFromFormat('d. m. Y', $data['eventDate']);
        $timeFrom = new DateTime($data['timeFrom'].':00');
        $timeTo = new DateTime($data['timeTo'].':00');

        $event = new Event(
            $data['title'],
            $eventDate,
            $data['place'],
            $timeFrom,
            $timeTo,
            $data['description']
        );

        if ($data['active']) {
            $event->activate();
        }

        $this->repository->persist($event);
        $this->repository->flush();
    }

    public function update($event, array $data)
    {
        if ($data['active']) {
            $event->activate();
        } else {
            $event->deactivate();
        }

        $data['eventDate'] = DateTime::createFromFormat('d. m. Y', $data['eventDate']);
        $data['timeFrom'] = new DateTime($data['timeFrom'].':00');
        $data['timeTo'] = new DateTime($data['timeTo'].':00');

        unset($data['active']);

        return parent::update($event, $data);
    }

    public function getFormDefaults(Event $event)
    {
        return [
            'title' => $event->getTitle(),
            'place' => $event->getPlace(),
            'description' => $event->getDescription(),
            'eventDate' => $event->getEventDate()->format('d. m. Y'),
            'timeFrom' => $event->getTimeFrom()->format('H:i'),
            'timeTo' => $event->getTimeTo()->format('H:i'),
            'active' => $event->isActive()
        ];
    }

    public function getActiveEvents()
    {
        return $this->repository->findBy(['active' => 1, 'deleted' => 0], ['eventDate' => 'DESC']);
    }

}