<?php

namespace App\Event;

use App\Model\BaseEntity;
use App\Traits\ActivityTrait;
use App\Traits\RemovableTrait;
use Nette\Utils\DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Event extends BaseEntity
{
    use ActivityTrait;
    use RemovableTrait;

    /**
     * @ORM\Column(type="string", length=160)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime")
     */
    private $eventDate;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $place;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="time")
     */
    private $timeFrom;

    /**
     * @ORM\Column(type="time")
     */
    private $timeTo;

    public function __construct($title, DateTime $eventDate, $place, $timeFrom, $timeTo, $description)
    {
        $this->title = $title;
        $this->eventDate = $eventDate;
        $this->place = $place;
        $this->timeFrom = $timeFrom;
        $this->timeTo = $timeTo;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return DateTime
     */
    public function getEventDate()
    {
        return $this->eventDate;
    }

    /**
     * @param DateTime $eventDate
     */
    public function setEventDate(DateTime $eventDate)
    {
        $this->eventDate = $eventDate;
    }

    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     */
    public function setPlace($place)
    {
        $this->place = $place;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return DateTime
     */
    public function getTimeFrom()
    {
        return $this->timeFrom;
    }

    /**
     * @param DateTime $timeFrom
     */
    public function setTimeFrom(DateTime $timeFrom)
    {
        $this->timeFrom = $timeFrom;
    }

    /**
     * @return DateTime
     */
    public function getTimeTo()
    {
        return $this->timeTo;
    }

    /**
     * @param DateTime $timeTo
     */
    public function setTimeTo(DateTime $timeTo)
    {
        $this->timeTo = $timeTo;
    }


}