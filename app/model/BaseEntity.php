<?php

namespace App\Model;

use Doctrine\ORM\Mapping as ORM;

abstract class BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var integer
     */
    protected $id;

    /**
     * @return integer
     */
    final public function getId()
    {
        return $this->id;
    }

    public function __clone()
    {
        $this->id = NULL;
    }
}