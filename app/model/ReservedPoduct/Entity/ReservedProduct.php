<?php

namespace App\ReservedProduct;

use App\Model\BaseEntity;
use App\Order\OrderLine\ProductOrderLine;
use App\Product\Variant;
use App\Stock\StockItem;
use App\Traits\ActivityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ReservedProduct extends BaseEntity
{
	use ActivityTrait;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Product\Variant")
	 * @ORM\JoinColumn(name="price_variant_id", referencedColumnName="id")
	 */
	private $product;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\OrderLine\ProductOrderLine")
	 */
	private $orderLine;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Stock\StockItem", cascade={"persist"})
	 * @ORM\JoinColumn(name="stock_item_id", referencedColumnName="id")
	 */
	private $stockItem;

	/**
	 * @ORM\Column(type="integer", length=3)
	 */
	private $count;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $created;

	public function __construct(Variant $product, ProductOrderLine $orderLine, StockItem $stockItem, int $count)
	{
		$this->product = $product;
		$this->orderLine = $orderLine;
		$this->stockItem = $stockItem;
		$this->count = $count;
		$this->created = new \DateTime();
		$this->activate();
	}

	/**
	 * @return Variant
	 */
	public function getProduct(): Variant
	{
		return $this->product;
	}

	/**
	 * @return ProductOrderLine
	 */
	public function getOrderLine(): ProductOrderLine
	{
		return $this->orderLine;
	}

	/**
	 * @return int
	 */
	public function getCount(): int
	{
		return $this->count;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreated(): \DateTime
	{
		return $this->created;
	}

	/**
	 * @return StockItem
	 */
	public function getStockItem(): StockItem
	{
		return $this->stockItem;
	}
}