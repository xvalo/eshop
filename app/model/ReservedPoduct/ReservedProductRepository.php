<?php

namespace App\ReservedProduct;

use App\Model\BaseRepository;

class ReservedProductRepository extends BaseRepository
{
	public function getReservedProducts()
	{
		$qb = $this->em->createQueryBuilder()
				->select('R')
				->from('\App\ReservedProduct\ReservedProduct', 'R');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('R.active', ':active')
			)
		);

		$qb->orderBy('R.created', 'DESC');

		$qb->setParameters([
			'active' => true
		]);

		return $qb->getQuery()->execute();
	}
}