<?php

namespace App\File;

use App\Model\BaseRepository;

class OrderDocumentRepository extends BaseRepository
{
    public function getGridDataByType(array $types)
    {
        $qb = $this->em->createQueryBuilder()
                    ->select('D')
                    ->from(OrderDocument::class, 'D');

        $qb->where(
        	$qb->expr()->andX(
        		$qb->expr()->eq('D.deleted', ':deleted'),
		        $qb->expr()->in('D.type', ':types')
	        )
        );

        $qb->setParameters([
        	'deleted' => false,
	        'types' => $types
        ]);

        return $qb;
    }
}