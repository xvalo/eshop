<?php

namespace App\File;

use App\Model\BaseRepository;
use App\Product\Image\Entity\ProductImage;
use App\Product\Product;

class ImageRepository extends BaseRepository
{
    public function getProductByImagePath($path)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('p')
                ->from('\App\Product\Product', 'p')
                    ->leftJoin('p.images', 'i')
                ->where('i.path LIKE :path');

        $qb->setParameter('path', $path);

        return $qb->getQuery()->getSingleResult();
    }

    public function getImageByPath($path)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('i')
            ->from('\App\File\Image', 'i')
            ->where('i.path LIKE :path');

        $qb->setParameter('path', $path);

        return $qb->getQuery()->getSingleResult();
    }

	public function saveImage(Image $image)
	{
		if ($image->getId() === null) {
			$this->em->persist($image);
		}

		$this->em->flush();
    }

	/**
	 * @param Product $product
	 * @param Image $image
	 * @return mixed
	 */
	public function removeProductImageRelation(Product $product, Image $image)
	{
		$qb = $this->em->createQueryBuilder()
			->delete(ProductImage::class, 'PI')
			->where('PI.product = :product AND PI.image = :image')
			->setParameters([
				'product' => $product,
				'image' => $image
			]);

		return $qb->getQuery()->execute();
    }
}