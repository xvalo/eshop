<?php

namespace App\File;

use App\Model\BaseService;

class OrderDocumentService extends BaseService
{
    public function __construct(OrderDocumentRepository $repository)
    {
        parent::__construct($repository);
    }
}