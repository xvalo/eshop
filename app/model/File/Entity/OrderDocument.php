<?php

namespace App\File;

use App\Model\BaseEntity;
use App\Order\Order;
use App\Traits\IRemovable;
use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 */
class OrderDocument extends BaseEntity implements IRemovable
{
    use RemovableTrait;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @ORM\Column(type="integer", length=2)
     * @var string
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Order\Order", inversedBy="documents")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

	/**
	 * @ORM\Column(type="datetime")
	 */
    private $created;

    /**
     * @param $number
     * @param Order $order
     * @param $path
     * @param $type
     */
    public function __construct($number, Order $order, $path, $type)
    {
        $this->number = $number;
        $this->order = $order;
        $this->path = $path;
        $this->type = $type;
        $this->created = new DateTime();
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

	public function getCreated()
	{
		return $this->created;
    }

    public function getRelativePath()
    {
        return '/public/data/'.$this->getPath();
    }

	public function isInvoice()
	{
		return $this->getType() == OrderDocumentType::INVOICE;
    }

	public function isReceipt()
	{
		return $this->getType() == OrderDocumentType::INVOICE || $this->getType() == OrderDocumentType::BILL;
    }

	public function getLabel()
	{
		switch ($this->getType()) {
			case OrderDocumentType::INVOICE:
				return 'Faktura';
			case OrderDocumentType::BILL:
				return 'Prodejka';
			case OrderDocumentType::CANCEL:
				return 'Storno doklad';
			case OrderDocumentType::PRODUCTS_LIST:
				return 'Soupis položek';
			case OrderDocumentType::CORRECTIVE:
				return 'Opravný daňový doklad';
		}
    }

	public function isCreatedToday()
	{
		$today = new DateTime();
		$today->setTime(0, 0, 0);

		$created = $this->getCreated()->setTime(0, 0, 0);

		$diff = $today->diff($created);

		return $diff->days == 0;
	}

}