<?php

namespace App\File;

use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Image extends BaseEntity
{
	const IMAGE_NAMESPACE = 'images';

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $path;

	/**
	 * @ORM\Column(type="integer", length=2)
	 * @var string
	 */
	private $type;

	/**
	 * @ORM\OneToMany(targetEntity="App\Product\Image\Entity\ProductImage", mappedBy="image")
	 */
	private $products;

	/**
	 * Image constructor.
	 * @param $name
	 * @param $path
	 * @param $type
	 */
	public function __construct($name, $path, $type)
	{
		$this->name = $name;
		$this->path = $path;
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	public function getRelativePath()
	{
		return '/public/data/'.$this->getPath();
	}
}