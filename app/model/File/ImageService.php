<?php

namespace App\File;

use App\Model\Services\ApplicationConfigurationService;
use Nette\Http\FileUpload;
use Nette\InvalidArgumentException;
use Nette\InvalidStateException;
use Ublaboo\ImageStorage\ImageStorage;

class ImageService
{
	/** @var ImageRepository */
	private $repository;

	/** @var ImageStorage */
	private $storage;

	/** @var ApplicationConfigurationService */
	private $applicationConfigurationService;

	/**
	 * @param ImageRepository $repository
	 * @param ImageStorage $storage
	 * @param ApplicationConfigurationService $applicationConfigurationService
	 */
	public function __construct(
		ImageRepository $repository,
		ImageStorage $storage,
		ApplicationConfigurationService $applicationConfigurationService
	){
		$this->repository = $repository;
		$this->storage = $storage;
		$this->applicationConfigurationService = $applicationConfigurationService;
	}

	/**
	 * @param string $sourcePath
	 * @param string $storage
	 * @param bool $setWatermark
	 * @return Image
	 * @throws \Nette\Utils\UnknownImageFileException
	 */
	public function createImageFromSource(string $sourcePath, string $storage, bool $setWatermark = false)
	{
		$info = pathinfo($sourcePath);
		$image = $this->saveImageFile($sourcePath, $info['basename'], $storage, $setWatermark);
		return new Image($image->name, $image->identifier, exif_imagetype($sourcePath));
	}

	public function removeImage(Image $image)
	{
		$this->storage->delete($image->getRelativePath());
		$this->repository->delete($image);
		$this->repository->flush();
	}

	/**
	 * @param FileUpload $data
	 * @param string $storage
	 * @param bool $setWatermark
	 * @return Image
	 * @throws \Nette\Utils\UnknownImageFileException
	 */
	public function createImage(FileUpload $data, string $storage, bool $setWatermark = false, string $name = null)
	{
		if(!$data->isOk())
		{
			throw new InvalidStateException('Chyba při nahrávání souboru');
		}

		if(!$data->isImage())
		{
			throw new InvalidArgumentException('Nahrávaný soubor není obrázek.');
		}

		$image = $this->saveImageFile($data->getTemporaryFile(), $data->getName(), $storage, $setWatermark);
		$name = $name ?: $image->name;

		return new Image($name, $image->identifier, exif_imagetype($data->getTemporaryFile()));

	}

	/**
	 * @param string $sourcePath
	 * @param string $name
	 * @param string $storage
	 * @param bool $setWatermark
	 * @return \Ublaboo\ImageStorage\Image
	 * @throws \Nette\Utils\UnknownImageFileException
	 */
	private function saveImageFile(string $sourcePath, string $name, string $storage, bool $setWatermark = false)
	{
		$image = \Nette\Utils\Image::fromFile($sourcePath);

		if (!$image) {
			throw new \App\InvalidArgumentException('Cesta k sobouru neexistuje');
		}

		if ($setWatermark && $this->applicationConfigurationService->isWatermarkAllowed()) {
			$image = $this->setWatermarkToImage($image);
		}

		return $this->storage->saveContent((string)$image, $name, $storage);
	}

	/**
	 * @param \Nette\Utils\Image $image
	 * @return \Nette\Utils\Image
	 * @throws \Nette\Utils\UnknownImageFileException
	 */
	private function setWatermarkToImage(\Nette\Utils\Image $image)
	{
		$watermark = \Nette\Utils\Image::fromFile($this->applicationConfigurationService->getWatermarkImagePath());
		$watermark->resize('75%', null);
		$image->place($watermark, '50%', '50%', 25);

		return $image;
	}
}