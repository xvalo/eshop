<?php

namespace App\File;

class OrderDocumentType
{
    const INVOICE = 1;
    const BILL = 2;
    const CANCEL = 3;
    const CORRECTIVE = 4;
    const PRODUCTS_LIST = 5;

	public static $settingsVariableKeys = [
		self::INVOICE => 'invoiceNumber',
		self::BILL => 'billNumber',
		self::CANCEL => 'invoiceNumber',
		self::CORRECTIVE => 'invoiceCorrectionNumber',
		self::PRODUCTS_LIST => null
	];
}