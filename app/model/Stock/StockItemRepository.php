<?php

namespace App\Stock;

use App\Model\BaseRepository;
use App\Product\Variant;

class StockItemRepository extends BaseRepository
{
	public function getStockItemsToAlert()
	{
		$qb = $this->em->createQueryBuilder()
				->select('i')
				->from('\App\Stock\StockItem', 'i');

		$where = $qb->expr()->andX(
			$qb->expr()->eq('i.alert', ':alert'),
			$qb->expr()->orX(
				$qb->expr()->lt('i.count', 'i.limitToAlert'),
				$qb->expr()->eq('i.count', ':zeroCount')
			)
		);

		$qb->where($where);

		$qb->setParameters([
			'alert' => true,
			'zeroCount' => 0
		]);

		return $qb->getQuery()->execute();
	}

	/**
	 * @param array|int[] $ids
	 * @return array
	 */
	public function getStockItemsByIds(array $ids): array
	{
		return $this->em->getRepository(StockItem::class)->findBy(['id' => $ids]);
	}

	public function getProductStockCount(Variant $variant)
	{
		$qb = $this->em->createQueryBuilder()
				->select('SUM(I.count)')
				->from(StockItem::class, 'I');

		$qb->where($qb->expr()->eq('I.product', ':product'));
		$qb->setParameters([
			'product' => $variant
		]);

		return $qb->getQuery()->getSingleScalarResult();
	}

	public function getStockGridData()
	{
		$qb = $this->em->createQueryBuilder();

		$qb->select('P.id AS id, P.name AS productName, SUM(I.count) as onStock, C.name AS categoryName, P.active AS active, PP.name AS producer')
			->from(StockItem::class, 'I')
			->innerJoin('I.product', 'V')
			->innerJoin('V.product', 'P')
			->innerJoin('P.category', 'C')
			->innerJoin('P.producer', 'PP')
			->groupBy('P.id');

		return $qb->getQuery()->execute();
	}
}