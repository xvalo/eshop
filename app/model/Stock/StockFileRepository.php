<?php

namespace App\Stock;

use App\Model\BaseRepository;

class StockFileRepository extends BaseRepository
{
    public function getGridDataByType($type)
    {
        $qb = $this->em->createQueryBuilder()
                        ->select('SF')
                            ->from(StockFile::class, 'SF')
                        ->where('SF.type = :type');

        $qb->setParameters([
            'type' => $type
        ]);

        return $qb;
    }
}