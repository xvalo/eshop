<?php

namespace App\Stock;

use App\InternalExceptions\ProductNotFoundException;
use App\InternalExceptions\StockProductIdentifierNotFilledException;
use App\Lists\Stock;
use App\Lists\StockRepository;
use App\Managers\InventoryValuationManager;
use App\Model\BaseService;
use App\Product\Price\Price;
use App\Product\Price\RegularPrice;
use App\Product\Variant;
use App\Product\VariantRepository;
use App\Stock\Document\ValueObject\InventoryItemValueObject;
use App\Stock\Document\ValueObject\InventoryValuationData;
use Nette\Utils\DateTime;

/**
 * Class StockService
 * @package App\Stock
 * @property StockRepository $repository
 */
class StockService extends BaseService
{
    /** @var StockFileRepository */
    private $stockFileRepository;

	/** @var VariantRepository */
	private $priceVariantRepository;

	/** @var StockItemRepository */
	private $stockItemRepository;

	/** @var InventoryValuationManager */
	private $inventoryValuationManager;
	/**
	 * @var StockChangeRepository
	 */
	private $stockChangeRepository;

	/**
	 * StockService constructor.
	 * @param StockRepository $repository
	 * @param VariantRepository $priceVariantRepository
	 * @param StockFileRepository $stockFileRepository
	 * @param StockItemRepository $stockItemRepository
	 * @param InventoryValuationManager $inventoryValuationManager
	 * @param StockChangeRepository $stockChangeRepository
	 */
    public function __construct(
	    StockRepository $repository,
	    VariantRepository $priceVariantRepository,
	    StockFileRepository $stockFileRepository,
	    StockItemRepository $stockItemRepository,
	    InventoryValuationManager $inventoryValuationManager,
		StockChangeRepository $stockChangeRepository
    ) {
        parent::__construct($repository);
        $this->stockFileRepository = $stockFileRepository;
	    $this->priceVariantRepository = $priceVariantRepository;
	    $this->stockItemRepository = $stockItemRepository;
	    $this->inventoryValuationManager = $inventoryValuationManager;
	    $this->stockChangeRepository = $stockChangeRepository;
    }

	/**
	 * @param array $changes
	 * @return array
	 */
    public function processStockChangeOperation(array $changes):array
    {
        $processed = [];

        foreach ($changes as $change) {
            if (strlen($change['code']) > 0) {
                $product = $this->priceVariantRepository->getPriceVariantByCode($change['code']);
            } elseif (strlen($change['ean']) > 0) {
                $product = $this->priceVariantRepository->getPriceVariantByEan($change['ean']);
            } else {
                throw new StockProductIdentifierNotFilledException;
            }

            if (is_null($product)) {
                throw new ProductNotFoundException;
            }

            $processed[] = [
                'product' => $product,
                'quantity' => $change['quantity'],
                'note' => $change['note']
            ];
        }

        return $processed;
    }

	/**
	 * @param Variant $priceVariant
	 * @param Stock $stock
	 */
	public function changeStockItemAlert(Variant $priceVariant, Stock $stock)
	{
		$stockItem = $priceVariant->getStockItem($stock);

		if ($stockItem->isAlertEnabled()) {
			$stockItem->disableAlert();
		} else {
			$stockItem->enableAlert();
		}

		$this->repository->flush();
    }

    /**
     * @param $id
     * @return null|StockFile
     */
    public function getStockFile($id)
    {
        return $this->stockFileRepository->find($id);
    }

	/**
	 * @param null $id
	 * @return Stock
	 */
	public function getStock($id = null): Stock
	{
		if ($id) {
			return $this->repository->find($id);
		}

		return $this->repository->findOneBy([], ['name' => 'ASC']);
    }

	public function getStocksList()
	{
		return $this->repository->getListOfStocks();
    }

	/**
	 * @param string $fileName
	 * @param DateTime $dateOfEvaluation
	 * @return string
	 */
	public function evaluateInventory(string $fileName, DateTime $dateOfEvaluation): string
	{
		$orderChangesAfterDate = [];

		/** @var StockChange $change */
		foreach ($this->stockChangeRepository->getOrdersStockChangesAfterDate($dateOfEvaluation) as $change) {
			$changedCount = (-1) * $change->getChangedCount();
			$stockItem = $change->getStockItem();

			if (!array_key_exists($stockItem->getId(), $orderChangesAfterDate)) {
				$orderChangesAfterDate[$stockItem->getId()] = [
					'count' => 0,
					'stockItem' => $change->getStockItem()
				];
			}

			$orderChangesAfterDate[$stockItem->getId()]['count'] += $changedCount;
		}

		$inventoryValuationData = new InventoryValuationData();
		$stockItems = $this->stockChangeRepository->getActiveItemsTillDate($dateOfEvaluation);

		/** @var StockItem $item */
		foreach ($stockItems as $item) {
			$variant = $item->getProduct();
			/** @var RegularPrice $price */
			$price = $variant->getPrices()->filter(function (Price $price){
				return $price instanceof RegularPrice;
			})->first();

			if ($price === false) {
				continue;
			}

			$count = $item->getCount();

			if (array_key_exists($item->getId(), $orderChangesAfterDate)) {
				$count += $orderChangesAfterDate[$item->getId()]['count'];
				unset($orderChangesAfterDate[$item->getId()]);
			}

			$inventoryValuationData->addInventoryItem(
				new InventoryItemValueObject(
					$variant->getCodesString(),
					$variant->getProduct()->getNumber(),
					$variant->getVariantName(),
					$price->getPrice(),
					$count,
					$item->getStock()->getName(),
					$price->getPurchasePrice(),
					$price->getPurchasePriceWithoutVat(),
					$price->getPriceWithoutVat()
				)
			);

			$inventoryValuationData->addPurchasePrice($price->getPurchasePrice() ? $price->getPurchasePrice()*$count : 0.0);
			$inventoryValuationData->addPurchasePriceWithoutVat($price->getPurchasePriceWithoutVat() ? $price->getPurchasePriceWithoutVat()*$count : 0.0);
			$inventoryValuationData->addSalePrice($price->getPrice() ? $price->getPrice()*$count : 0.0);
			$inventoryValuationData->addSalePriceWithoutVat($price->getPriceWithoutVat() ? $price->getPriceWithoutVat()*$count : 0.0);
		}

		foreach ($orderChangesAfterDate as $data) {
			/** @var StockItem $item */
			$item = $data['stockItem'];
			$variant = $item->getProduct();
			/** @var RegularPrice $price */
			$price = $variant->getAllPrices()->filter(function (Price $price){
				return $price instanceof RegularPrice;
			})->first();

			$count = $data['count'];

			$inventoryValuationData->addInventoryItem(
				new InventoryItemValueObject(
					$variant->getCodesString(),
					$variant->getProduct()->getNumber(),
					$variant->getVariantName(),
					$price->getPrice(),
					$count,
					$item->getStock()->getName(),
					$price->getPurchasePrice(),
					$price->getPurchasePriceWithoutVat(),
					$price->getPriceWithoutVat()
				)
			);

			$inventoryValuationData->addPurchasePrice($price->getPurchasePrice() ? $price->getPurchasePrice()*$count : 0.0);
			$inventoryValuationData->addPurchasePriceWithoutVat($price->getPurchasePriceWithoutVat() ? $price->getPurchasePriceWithoutVat()*$count : 0.0);
			$inventoryValuationData->addSalePrice($price->getPrice() ? $price->getPrice()*$count : 0.0);
			$inventoryValuationData->addSalePriceWithoutVat($price->getPriceWithoutVat() ? $price->getPriceWithoutVat()*$count : 0.0);
		}

		return $this->inventoryValuationManager->createPDF($fileName, $inventoryValuationData, $dateOfEvaluation);
    }
}