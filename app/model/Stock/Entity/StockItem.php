<?php

namespace App\Stock;

use App\Lists\Stock;
use App\Model\BaseEntity;
use App\Product\Variant;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class StockItem extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="\App\Product\Variant", inversedBy="stockItems", cascade={"persist"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Lists\Stock")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    private $stock;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private $count = 0;

    /**
     * @ORM\Column(type="integer", length=3)
     */
    private $limitToAlert = 0;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $position = '';

	/**
	 * @ORM\Column(type="boolean")
	 */
    private $alert = false;

    public function __construct(Variant $product, Stock $stock, $count = 0, $limitToAlert = 0, $position = '')
    {
        $this->product = $product;
        $this->stock = $stock;
        $this->count = $count;
        $this->limitToAlert = $limitToAlert;
        $this->position = $position;
    }

    /**
     * @return Variant
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    public function addItems($amount)
    {
        $this->count += $amount;
    }

    public function subtractItems($amount)
    {
        $this->count -= $amount;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getLimitToAlert()
    {
        return $this->limitToAlert;
    }

    /**
     * @param int $limitToAlert
     */
    public function setLimitToAlert($limitToAlert)
    {
        $this->limitToAlert = $limitToAlert;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

	/**
	 * @return boolean
	 */
	public function isAlertEnabled()
	{
		return $this->alert;
	}

	public function enableAlert()
	{
		$this->alert = true;
	}

	public function disableAlert()
	{
		$this->alert = false;
	}
}