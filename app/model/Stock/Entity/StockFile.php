<?php

namespace App\Stock;

use App\Admin\Admin;
use App\Lists\Stock;
use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class StockFile extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="\App\Lists\Stock")
     * @ORM\JoinColumn(name="stock_id", referencedColumnName="id")
     */
    private $stock;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Admin\Admin")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $type;

    /**
     * StockFile constructor.
     *
     * @param Stock $stock
     * @param Admin $user
     * @param $file
     * @param $type
     */
    public function __construct(Stock $stock, Admin $user, $file, $type)
    {
        $this->stock = $stock;
        $this->user = $user;
        $this->file = $file;
        $this->created = new \DateTime();
        $this->type = $type;
    }

    /**
     * @return Stock
     */
    public function getStock(): Stock
    {
        return $this->stock;
    }

    /**
     * @return Admin
     */
    public function getUser(): Admin
    {
        return $this->user;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }
}