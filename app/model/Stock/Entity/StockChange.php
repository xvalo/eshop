<?php

namespace App\Stock;

use App\Admin\Admin;
use App\Model\BaseEntity;
use App\Order\Order;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class StockChange extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="\App\Stock\StockItem", cascade={"persist"})
     * @ORM\JoinColumn(name="stock_item_id", referencedColumnName="id")
     */
    private $stockItem;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Order\Order")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=true)
     */
    private $order;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private $changedCount;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\ManyToOne(targetEntity="\App\Admin\Admin")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    private $user;

    public function __construct(StockItem $stockItem, $changedCount, Admin $user = null, Order $order = null, \DateTime $created = null)
    {
        $this->stockItem = $stockItem;
        $this->changedCount = $changedCount;
        $this->user = $user;
        $this->order = $order;
        $this->created = $created ?: new \DateTime();
    }

    /**
     * @return StockItem
     */
    public function getStockItem()
    {
        return $this->stockItem;
    }

    /**
     * @return int
     */
    public function getChangedCount()
    {
        return $this->changedCount;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return Order|null
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return Admin
     */
    public function getUser()
    {
        return $this->user;
    }

}