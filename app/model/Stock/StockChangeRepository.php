<?php

namespace App\Stock;

use App\Model\BaseRepository;
use DateInterval;
use Kdyby\Doctrine\Dql\Join;
use Nette\Utils\DateTime;

class StockChangeRepository extends BaseRepository
{
	public function getOrdersStockChangesAfterDate(DateTime $date)
	{
		$qb = $this->repository->createQueryBuilder();
		$date = $date->add(DateInterval::createFromDateString('1 days'))->setTime(0,0,0);

		$qb->select('SCH')
			->from(StockChange::class, 'SCH')
			->where(
				$qb->expr()->andX(
					$qb->expr()->gte('SCH.created', ':date')
				)
			);

		$qb->setParameters([
			'date' => $date
		]);

		return $qb->getQuery()->execute();
	}

	public function getActiveItemsTillDate(DateTime $date)
	{
		$qb = $this->repository->createQueryBuilder();
		$date->setTime(23, 59, 59);

		$qb->select('SI')
			->from(StockItem::class, 'SI')
			->join(StockChange::class, 'SCH', Join::WITH, 'SI = SCH.stockItem')
			->where(
				$qb->expr()->andX(
					$qb->expr()->lte('SCH.created', ':date')
				)
			)
			->groupBy('SI.id');

		$qb->setParameters([
			'date' => $date
		]);

		return $qb->getQuery()->execute();
	}
}