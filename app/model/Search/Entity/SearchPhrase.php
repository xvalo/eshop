<?php

namespace App\Search;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use App\Model\BaseEntity;

/**
 * @ORM\Entity
 */
class SearchPhrase extends BaseEntity
{
    /**
     * @ORM\Column(type="string", length=250)
     * @var string
     */
    private $phrase;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    public function __construct($phrase)
    {
        $this->phrase = $phrase;
        $this->created = new DateTime();
    }

    /**
     * @return string
     */
    public function getPhrase()
    {
        return $this->phrase;
    }

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }


}