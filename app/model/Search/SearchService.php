<?php

namespace App\Search;

use App\Model\BaseService;
use App\Product\Listing\Search\SearchFacade;

/**
 * Class SearchService
 * @package App\Search
 * @property $repository SearchPhraseRepository
 */
class SearchService extends BaseService
{
	/**
	 * @var SearchFacade
	 */
	private $searchFacade;

	public function __construct(
    	SearchPhraseRepository $repository,
		SearchFacade $searchFacade
    ){
        parent::__construct($repository);
		$this->searchFacade = $searchFacade;
	}

    public function saveSearchPhrase($phrase)
    {
        $searchPhrase = new SearchPhrase($phrase);
        $this->repository->persist($searchPhrase);
        $this->repository->flush();
    }

	public function getMostPopularPhrases()
	{
		$qb = $this->repository->getMostPopularPhrases(100);
		return $qb->getQuery()->execute();
    }

	public function searchByPhrase(string $phrase)
	{
		dump($this->searchFacade->searchFulltext($phrase));die;
    }
}