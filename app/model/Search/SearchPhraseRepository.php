<?php

namespace App\Search;


use App\Model\BaseRepository;

class SearchPhraseRepository extends BaseRepository
{
	public function getMostPopularPhrases($limit = null)
	{
		$qb = $this->em->createQueryBuilder()
				->select('P.phrase, COUNT(P.phrase) AS searchCount')
				->from('\App\Search\SearchPhrase', 'P')
				->groupBy('P.phrase')
				->orderBy('searchCount', 'DESC');

		if ($limit) {
			$qb->setMaxResults($limit);
		}

		return $qb;
	}
}