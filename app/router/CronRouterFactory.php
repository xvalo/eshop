<?php

namespace App\Router;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use Nette\SmartObject;

class CronRouterFactory
{
	use SmartObject;

	public function __construct()
	{

	}

	public function create()
	{
		$cronRouter = new RouteList( 'Cron' );

		$cronRouter[] = new Route('cron/site-map/generate', 'SiteMap:generate');
		$cronRouter[] = new Route('cron/feed/generate-heureka', 'Feed:generateHeureka');
		$cronRouter[] = new Route('cron/feed/generate-zbozi', 'Feed:generateZbozi');
		$cronRouter[] = new Route('cron/stock-update/update-colosus', 'StockUpdate:updateColosus');
		$cronRouter[] = new Route('cron/<presenter>/<action>', 'Homepage:default');

		return $cronRouter;
	}
}