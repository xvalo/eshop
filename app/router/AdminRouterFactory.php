<?php

namespace App\Router;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use Nette\SmartObject;

class AdminRouterFactory
{
	use SmartObject;

	public function __construct()
	{

	}

	public function create()
	{
		$adminRouter = new RouteList( 'Admin' );

		$adminRouter[] = new Route( 'admin/list/supplier', 'List:supplier');
		$adminRouter[] = new Route( 'admin/delivery', 'Delivery:default');
		$adminRouter[] = new Route( 'admin/delivery/payment-type', 'Delivery:paymentType');


		$adminRouter[] = new Route( 'admin/list/<type>', [
			'presenter' => 'List',
			'action' => 'default'
		]);

		$adminRouter[] = new Route( 'admin/product/<id \d+>[/<action images|related|filters>]', [
			'presenter' => 'Product',
			'action' => 'edit'
		]);

		$adminRouter[] = new Route( 'admin/product/<id \d+>/variants[/<variantId \d+>]', [
			'presenter' => 'Product',
			'action' => 'variants'
		]);

		$adminRouter[] = new Route( 'admin/product/<id \d+>/attributes[/<attributeId \d+>]', [
			'presenter' => 'Product',
			'action' => 'attributes'
		]);

		$adminRouter[] = new Route( 'admin/category/<id \d+>[/<action attributes|delivery|filters>[/<attributeId \d+>]]', [
			'presenter' => 'Category',
			'action' => 'edit'
		]);

		$adminRouter[] = new Route( 'admin/attribute/<id \d+>[/<action variants|attached-to>]', [
			'presenter' => 'Attribute',
			'action' => 'edit'
		]);

		$adminRouter[] = new Route( 'admin/filter/<id \d+>[/<action values>]', [
			'presenter' => 'Filter',
			'action' => 'edit'
		]);

		$adminRouter[] = new Route( 'admin/<presenter>/<action>[/<id \d+>]', [
			'presenter' => 'Dashboard',
			'action' => 'default'
		]);

		return $adminRouter;
	}
}