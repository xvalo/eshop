<?php

namespace App\Router;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use Nette\SmartObject;

class ApiRouterFactory
{
	use SmartObject;

	public function __construct()
	{
		
	}

	public function create()
	{
		$apiRouter = new RouteList( 'Api' );

		$apiRouter[] = new Route('api/cart/add-product/<productId>', 'Cart:addToCart');

		$apiRouter[] = new Route('api/product/searchVariants?q=<q>', 'Product:searchVariants');
		$apiRouter[] = new Route('api/product/search-prices?q=<q>', 'Product:searchPrices');
		$apiRouter[] = new Route('api/product/get-price-detail/<id>', 'Product:getPriceDetail');
		$apiRouter[] = new Route('api/customer/searchCustomer?q=<q>', 'Customer:searchCustomer');
		$apiRouter[] = new Route('api/<presenter>/<action>[/<id>]', 'Default:default');

		return $apiRouter;
	}
}