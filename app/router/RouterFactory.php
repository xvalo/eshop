<?php

namespace App\Router;

use Nette;
use Nette\Application\Routers\RouteList;

class RouterFactory
{

	/** @var AdminRouterFactory */
	private $adminRouterFactory;

	/** @var ApiRouterFactory */
	private $apiRouterFactory;

	/** @var CronRouterFactory */
	private $cronRouterFactory;

	/** @var FrontRouterFactory */
	private $frontRouterFactory;

	public function __construct(
		AdminRouterFactory $adminRouterFactory,
		ApiRouterFactory $apiRouterFactory,
		CronRouterFactory $cronRouterFactory,
		FrontRouterFactory $frontRouterFactory
	){
		$this->adminRouterFactory = $adminRouterFactory;
		$this->apiRouterFactory = $apiRouterFactory;
		$this->cronRouterFactory = $cronRouterFactory;
		$this->frontRouterFactory = $frontRouterFactory;
	}

	/**
	 * @return Nette\Application\IRouter
	 */
	public function create()
	{
		$router = new RouteList;

		$router[] = $this->apiRouterFactory->create();
		$router[] = $this->cronRouterFactory->create();
		$router[] = $this->adminRouterFactory->create();
		$router[] = $this->frontRouterFactory->create();

		return $router;
	}

}
