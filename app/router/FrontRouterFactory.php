<?php

namespace App\Router;

use App\Page\Entity\Page;
use App\Page\PageFacade;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use Nette\SmartObject;

class FrontRouterFactory
{
	use SmartObject;

	/** @var PageFacade */
	private $pageFacade;

	public function __construct(
		PageFacade $pageFacade
	){
		$this->pageFacade = $pageFacade;
	}

	public function create()
	{
		$frontRouter = new RouteList( 'Front' );

		// Registration and password resend
		$frontRouter[] = new Route('registrace', 'Sign:register');
		$frontRouter[] = new Route('auth/<hash>', 'Sign:auth');
		$frontRouter[] = new Route('obnova-hesla', 'Sign:restorePassword');
		$frontRouter[] = new Route('odhlasit', 'Sign:out');

		// Account pages
		$frontRouter[] = new Route('ucet/muj', 'Account:myAccount');
		$frontRouter[] = new Route('ucet/objednavky', 'Account:myOrders');
		$frontRouter[] = new Route('ucet/detail-objednavky/<orderId>', 'Account:orderDetail');
		$frontRouter[] = new Route('ucet/zmena-hesla', 'Account:changePassword');
		$frontRouter[] = new Route('ucet/editace', 'Account:changeInfo');

		$frontRouter[] = new Route('info/<page>', 'Page:default');

		$frontRouter[] = new Route('jak-nakupovat', 'Homepage:howToBuy');
		$frontRouter[] = new Route('obchodni-podminky', 'Homepage:termsConditions');
		$frontRouter[] = new Route('obchodni-podminky-do-16-12-2017', 'Homepage:termsConditionsOld');
		$frontRouter[] = new Route('ochrana-osobnich-udaju', 'Homepage:personalPrivacy');
		$frontRouter[] = new Route('partneri', 'Homepage:partners');
		$frontRouter[] = new Route('kontakt', 'Homepage:contact');
		$frontRouter[] = new Route('aktualizace-odberu?hash=<hash>', 'Homepage:mailingConfirmation');

		$frontRouter[] = new Route('vyhledavani/<q>', 'Search:default', Route::ONE_WAY);
		$frontRouter[] = new Route('hledat/<q>[/strana-<pagination-page \d+>/]', 'Search:default');

		// News
		$frontRouter[] = new Route('clanky/', 'News:default');
		$frontRouter[] = new Route('clanky/<url>[-<id \d+>]/', 'News:detail');

		// Cart
		$frontRouter[] = new Route('kosik', 'Cart:default');
		$frontRouter[] = new Route('kosik', 'Cart:noItems');
		$frontRouter[] = new Route('kosik/objednano', 'Cart:orderCreated');

		// Producer
		$frontRouter[] = new Route('vyrobci/', 'Producer:default');
		$frontRouter[] = new Route('vyrobce/<url>[/strana-<pagination-page \d+>/]', 'Producer:detail');

		// Products and categories
		$frontRouter[] = new Route('nejnovejsi-produkty/', 'NewProducts:default', Route::ONE_WAY);
		$frontRouter[] = new Route('novinky[/strana-<pagination-page \d+>/]', 'NewProducts:default');

		$frontRouter[] = new Route('<category>[/strana-<pagination-page \d+>/]', 'Category:default');
		$frontRouter[] = new Route('<category>/<url>[/<variant>-<variantId \d+>]/', 'Product:default');

		$frontRouter[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');


		return $frontRouter;
	}
}