<?php

namespace App\AdminModule\Grids;

use App\Order\Payment\Entity\PaymentType;
use App\Order\Payment\PaymentTypeFacade;
use App\Utils\Selects;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;

class PaymentTypeGrid extends BaseGrid
{
	/** @var PaymentTypeFacade  */
	private $facade;

	public function __construct(
		IContainer $parent,
		$name,
		PaymentTypeFacade $paymentTypeFacade
	){

		parent::__construct($parent, $name);

		$this->facade = $paymentTypeFacade;

		$this->setDataSource($paymentTypeFacade->getGridSource());

		$this->addColumnText('name', 'Název')
			->setSortable()
			->setFilterText('name');

		$this->addColumnText('active', 'Aktivní')
			->setSortable()
			->setReplacement(Selects::yesNo())
			->setFilterSelect(Selects::yesNo());

		$this->setInlineAdd();
		$this->setInlineEdit();

		$this->addAction('delete', '', 'delete!')
			->setIcon('trash')
			->setTitle('Delete')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Opravdu chcete smazat položku %s?', 'name');

		//$this->setSortable();

	}

	private function setInlineAdd()
	{
		$this->addInlineAdd()->onControlAdd[] = function(Container $container)
		{
			$container->addText('name');
			$container->addSelect('active', null, Selects::yesNo(false));
		};

		$this->getInlineAdd()->onSubmit[] = function($values)
		{
			$lastPriority = $this->facade->getHighestPriority();
			$paymentType = new PaymentType(
				$values->name,
				$lastPriority ? ++$lastPriority : 1
			);

			if ($values->active) {
				$paymentType->activate();
			}

			$this->facade->savePaymentType($paymentType);
			$this->reload();
		};
	}

	private function setInlineEdit()
	{
		$this->addInlineEdit()->onControlAdd[] = function(Container $container)
		{
			$container->addText('name');
			$container->addSelect('active', null, Selects::yesNo(false));
		};

		$this->getInlineEdit()->onSetDefaults[] = function(Container $container, PaymentType $paymentType)
		{
			$container->setDefaults([
					'name' => $paymentType->getName(),
					'active' => $paymentType->isActive() ? 1 : 0
			]);
		};

		$this->getInlineEdit()->onSubmit[] = function($id, $values)
		{
			/** @var PaymentType $paymentType */
			$paymentType = $this->facade->getPaymentType((int)$id);

			$paymentType->setName($values->name);

			if ($values->active) {
				$paymentType->activate();
			} else {
				$paymentType->deactivate();
			}

			$this->facade->savePaymentType($paymentType);
			$this->reload();
		};
	}
}