<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\IContainer;

interface IPaymentTypeGridFactory
{
	/**
	 * @param IContainer $parent
	 * @param string $name
	 * @return PaymentTypeGrid
	 */
	public function create(IContainer $parent, $name);
}