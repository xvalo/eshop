<?php

namespace App\AdminModule\Grids;

interface IAvailabilityGridFactory
{
	/**
	 * @return AvailabilityGrid
	 */
	public function create();
}