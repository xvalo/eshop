<?php

namespace App\AdminModule\Grids;

use App\Product\Availability\AvailabilityFacade;
use App\Product\Availability\ColorProvider;
use App\Product\Availability\Entity\Availability;
use App\Utils\Selects;
use Nette\Utils\Html;

class AvailabilityGrid extends BaseGrid
{
	public function __construct(
		AvailabilityFacade $availabilityFacade,
		ColorProvider $colorProvider
	){
		parent::__construct();

		$this->setDataSource($availabilityFacade->getGridSource());

		$this->addColumnText('title', 'Název')
			->setSortable()
			->setRenderer(function(Availability $item) use ($colorProvider) {
				$span = Html::el('span')->setText($item->getTitle());

				if ($item->getColor()) {
					$span->setAttribute('class', 'label')
						->setAttribute('style', "background-color: #{$colorProvider->getAvailabilityColor($item->getColor())} !important;");
				}

				return $span;
			})
			->setFilterText('title');

		$this->addColumnText('description', 'Popis')
			->setSortable()
			->setFilterText('description');

		$this->addColumnText('productOrderable', 'Produkt dostupný')
			->setSortable()
			->setReplacement(Selects::yesNo())
			->setFilterSelect(Selects::yesNo());

		$this->addToolbarButton('new', 'Přidat novou dostupnost')
			->setTitle('Přidat novou dostupnost')
			->setIcon('plus')
			->setClass('btn btn-success');

		$this->addAction('edit', '', 'edit', ['id'])
			->setIcon('pencil');

		$this->setSortable()
			->setPagination(false);
	}
}