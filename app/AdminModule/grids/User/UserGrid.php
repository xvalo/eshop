<?php

namespace App\AdminModule\Grids;

use App\Admin\AdminRepository;

class UserGrid extends BaseGrid
{
	
	public function __construct($parent, $name, AdminRepository $repository)
	{
		parent::__construct($parent, $name, $repository);

		$this->addColumnText('login', 'Login')
				->setSortable()
				->setFilterText('login');

		$this->addColumnText('name', 'Jméno')
				->setSortable()
				->setFilterText('name');

		$this->addColumnText('lastName', 'Přijmení')
				->setSortable()
				->setFilterText('lastName');

		$this->addAction('edit', '', 'edit', ['id'])
				->setIcon('pencil');

		$this->addAction('delete', '', 'delete!')
				->setIcon('trash')
				->setTitle('Delete')
				->setClass('btn btn-xs btn-danger ajax')
				->setConfirm('Do you really want to delete example %s?', 'login');
	}
	
}
