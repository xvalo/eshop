<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\Container;

interface IUserGridFactory {
	
	/**
	 * @param Container $parent
	 * @param string $name
	 * @return UserGrid
	 */
	public function create($parent, $name);
	
}
