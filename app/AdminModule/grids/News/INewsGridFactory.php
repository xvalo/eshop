<?php

namespace App\AdminModule\Grids;

interface INewsGridFactory
{
	/**
	 * @return NewsGrid
	 */
	public function create();
}