<?php

namespace App\AdminModule\Grids;

use App\News\NewsFacade;
use App\Utils\Selects;

class NewsGrid extends BaseGrid
{
	public function __construct(
		NewsFacade $repository
	){
		parent::__construct();

		$this->setDataSource($repository->getGridSource());

		$this->addColumnText('title', 'Název')
			->setSortable()
			->setFilterText('name');

		$this->addColumnText('active', 'Aktivní')
			->setSortable()
			->setReplacement(Selects::yesNo())
			->setFilterSelect(Selects::yesNo());

		$this->addColumnDateTime('displayedFrom', 'Zobrazit od')
			->setSortable()
			->setFormat('d. m. Y')
			->setFilterDate('displayedFrom');

		$this->addColumnDateTime('displayedTo', 'Zobrazit do')
			->setSortable()
			->setFormat('d. m. Y')
			->setFilterDate('displayedTo');

		$this->addAction('edit', '', 'edit', ['id'])
			->setIcon('pencil');

		$this->addAction('delete', '', 'delete!')
			->setIcon('trash')
			->setTitle('Smazat')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Opravdu chcete smazat článek %s?', 'title');
	}
}