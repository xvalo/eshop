<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\Container;

interface IEventGridFactory
{
	/**
	 * @param Container $parent
	 * @param string $name
	 * @return EventGrid
	 */
	public function create($parent, $name);
}