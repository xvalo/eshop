<?php

namespace App\AdminModule\Grids;

use App\Event\EventRepository;
use App\Utils\Selects;

class EventGrid extends BaseGrid
{
	public function __construct($parent, $name, EventRepository $repository)
	{
		parent::__construct($parent, $name, $repository);

		$this->addColumnText('title', 'Název')
			->setSortable()
			->setFilterText('title');

		$this->addColumnText('place', 'Místo konání')
				->setSortable()
				->setFilterText('place');

		$this->addColumnDateTime('eventDate', 'Datum konání')
				->setSortable()
				->setFilterDate('eventDate');

		$this->addColumnText('active', 'Aktivní')
				->setSortable()
				->setReplacement(Selects::yesNo())
				->setFilterSelect(Selects::yesNo());

		$this->addAction('edit', '', 'edit', ['id'])
			->setIcon('pencil');

		$this->addAction('delete', '', 'delete!')
			->setIcon('trash')
			->setTitle('Delete')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Do you really want to delete example %s?', 'title');
	}
}