<?php

namespace App\AdminModule\Grids;

use App\Classes\Product\Variant\ProductVariantService;
use App\Model\Services\ApplicationConfigurationService;
use App\Product\Availability\AvailabilityProvider;
use App\Product\Code\VariantCode;
use App\Product\Product;
use App\Product\ProductService;
use App\Product\Variant;
use App\Product\VariantRepository;
use App\Utils\Numbers;
use Nette\Forms\Container;
use Nette\Utils\Html;

class ProductVariantGrid extends BaseGrid
{
	public function __construct(
		$parent,
        $name,
        Product $product,
        VariantRepository $repository,
		ProductService $service,
		ProductVariantService $productVariantService,
		AvailabilityProvider $availabilityProvider,
		ApplicationConfigurationService $applicationConfigurationService
	){
		parent::__construct($parent, $name);

		$self = $this;
		$availabilityColors = $applicationConfigurationService->getAvailabilityColors();

		$this->setDataSource($repository->getProductVariants($product));

		$this->addColumnText('code', 'Kód')
			->setRenderer(function(Variant $item){
				$codes = [];
				/** @var VariantCode $variantCode */
				foreach ($item->getCodes() as $variantCode) {
					$codes[] = $variantCode->getCode()->getCode();
				}
				return implode(' | ', $codes);
			})
			->setSortable();

		$this->addColumnText('ean', 'EAN')
			->setSortable();

		$this->addColumnText('name', 'Název')
			->setSortable();

		$this->addColumnText('price', 'Cena')
			->setSortable()
			->setRenderer(function(Variant $item){
				if ($item->getPrices()->count()) {
					return Numbers::czechFormatedNumber(($item->getPrices()->first())->getPrice()) . ',- Kč';
				}

				return '';
			});

		$this->addColumnText('normalPrice', 'Běžná cena')
			->setSortable()
			->setRenderer(function(Variant $item){
				return Numbers::czechFormatedNumber((($item->getNormalPrice() > 0 ? $item->getNormalPrice() : 0 ))) . ',- Kč';
			});

		$this->addColumnText('availability', 'Dostupnost')
			->setRenderer(function(Variant $item) use ($availabilityColors) {
				$availability = $item->getAvailability();
				$el = Html::el('span')->setText($availability->getTitle());
				$el->setAttribute('class', 'btn btn-sm '.$availabilityColors[$availability->getColor()]['bootstrap-class']);
				return $el;
			});

		$this->addInlineAdd()->onControlAdd[] = function(Container $container) use ($availabilityProvider)
		{
			$container->addText('ean');
			$container->addText('name');
			$container->addText('normalPrice');
			$container->addSelect('availability', null, $availabilityProvider->getSelectList());
		};

		$this->addInlineEdit()->onControlAdd[] = function(Container $container) use ($availabilityProvider)
		{
			$container->addText('name');
			$container->addText('ean');
			$container->addText('normalPrice');
			$container->addSelect('availability', null, $availabilityProvider->getSelectList());
		};

		$this->getInlineEdit()->onSetDefaults[] = function(Container $container, Variant $price)
		{
			$container->setDefaults([
				'ean' => $price->getEan(),
				'name' => $price->getName(),
				'normalPrice' => Numbers::czechFormatedNumber($price->getNormalPrice()),
				'availability' => $price->getAvailability()->getId()
			]);
		};

		$this->getInlineEdit()->onSubmit[] = function($id, $values) use ($self, $repository, $service, $availabilityProvider)
		{
			/** @var Variant $price */
			$price = $repository->getVariant($id);
			$service->updatePrice($price, $availabilityProvider->getAvailability((int) $values->availability), $values->name, $values->ean, Numbers::floatFromCzechNumber($values->normalPrice));
			//$self->reload();
		};

		$this->getInlineAdd()->onSubmit[] = function($values) use ($self, $product, $productVariantService, $availabilityProvider)
		{
			$availability = $availabilityProvider->getAvailability((int) $values->availability);
			$productVariantService->createVariant($product, $values->name, $availability, floatval($values->normalPrice), $values->ean);
			$self->reload();
		};

		$this->addActionCallback('delete', '', function($id) use ($service, $self){
			$service->deletePriceVariant($id);
			$self->reload();
		})
			->setIcon('trash')
			->setTitle('Delete')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Opravdu chcete smazat cenovou variantu %s?', 'name');

		$this->addAction('showDetail', '', 'variants', ['variantId' => 'id', 'id' => 'product.id'])
			->setIcon('eye')
			->setTitle('Upravit detail')
			->setClass('btn btn-xs btn-primary');

		$this->getInlineEdit()->setShowNonEditingColumns();
		$this->setSortable();
		$this->setPagination(false);
	}
}