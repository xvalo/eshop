<?php

namespace App\AdminModule\Grids;

use App\Product\Product;
use Nette\ComponentModel\Container;

interface IProductVariantGridFactory
{
	/**
	 * @param Container $parent
	 * @param string $name
	 * @param Product $product
	 * @return ProductVariantGrid
	 */
	public function create($parent, $name, Product $product);
}