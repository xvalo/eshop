<?php

namespace App\AdminModule\Grids;

use App\Attribute\Core\Attribute;
use App\Product\Product;

interface IProductAttributeVariantGridFactory
{
	/**
	 * @param Attribute $attribute
	 * @param Product $product
	 * @return ProductAttributeVariantGrid
	 */
	public function create(Attribute $attribute, Product $product);
}