<?php

namespace App\AdminModule\Grids;

use App\Attribute\AttributeFacade;
use App\Attribute\AttributeService;
use App\Attribute\Core\Attribute;
use App\Attribute\Core\AttributeVariant;
use App\Attribute\Image\ImageManager;
use App\Utils\Selects;
use Nette\Forms\Container;
use Nette\Forms\Form;

class AttributeVariantGrid extends BaseGrid
{
	public function __construct(
		Attribute $attribute,
		AttributeFacade $attributeFacade,
		AttributeService $attributeService,
		ImageManager $imageManager
	){
		parent::__construct();

		$this->setDataSource($attributeFacade->getAttributeVariantsGridSource($attribute));

		$self = $this;

		$this->addColumnText('image', 'Obrázek')
			->setTemplate(__DIR__ . '/templates/image.latte');

		$this->addColumnText('name', 'Název')
			->setSortable()
			->setFilterText('name');

		$this->addColumnText('price', 'Cena')
				->setSortable()
				->setFilterText('price');

		$this->addColumnText('isDiscount', 'Sleva')
			->setSortable()
			->setReplacement(Selects::yesNo())
			->setFilterSelect(Selects::yesNo());

		$this->addColumnText('isNews', 'Novinka')
			->setSortable()
			->setReplacement(Selects::yesNo())
			->setReplacement(Selects::yesNo())
			->setFilterSelect(Selects::yesNo());

		$this->addAction('remove', null, 'removeAttributeVariant!', ['variantId' => 'id'])
			->setIcon('trash')
			->setTitle('Smazat')
			->setClass('btn btn-xs btn-danger')
			->setConfirm('Opravdu chcete smazat variantu vlastnosti?');

		$this->addInlineAdd()->onControlAdd[] = function(Container $container)
		{
			$container->addUpload('image')
				->addRule(Form::IMAGE, 'Vložte prosím obrázek.')
				->setRequired(false);
			$container->addText('name')
				->setAttribute('placeholder', 'Název')
				->setMaxLength(200);

			$container->addText('price')
				->setAttribute('placeholder', 'Cena')
				->setType('number');
			$container->addSelect('isDiscount', null, Selects::yesNo(false));
			$container->addSelect('isNews', null, Selects::yesNo(false));

			$container->setDefaults([
				'isDiscount' => 0,
				'isNews' => 0
			]);

		};

		$this->getInlineAdd()->onSubmit[] = function($values) use ($self, $attribute, $attributeService, $imageManager)
		{
			$image = $values['image']->isOk() ? $imageManager->createImage($values['image']) : null;
			$attributeService->createAttributeVariant(
				$attribute,
				$values['name'],
				(float)$values['price'],
				$values['isDiscount'],
				$values['isNews'],
				true,
				$image
			);

			$self->redrawControl();
		};

		$this->addInlineEdit()->onControlAdd[] = function(Container $container)
		{
			$container->addUpload('image')
				->addRule(Form::IMAGE, 'Vložte prosím obrázek.')
				->setRequired(false);

			$container->addText('name')
				->setAttribute('placeholder', 'Název')
				->setMaxLength(200);

			$container->addText('price')
				->setAttribute('placeholder', 'Cena')
				->setType('number');

			$container->addSelect('isDiscount', null, Selects::yesNo(false));
			$container->addSelect('isNews', null, Selects::yesNo(false));
		};

		$this->getInlineEdit()->onSetDefaults[] = function(Container $container, AttributeVariant $attributeVariant)
		{
			$container->setDefaults([
				'name' => $attributeVariant->getName(),
				'price' => $attributeVariant->getPrice(),
				'isDiscount' => $attributeVariant->isDiscount()?1:0,
				'isNews' => $attributeVariant->isNews()?1:0
			]);
		};

		$this->getInlineEdit()->onSubmit[] = function($id, $values) use ($self, $attribute, $attributeService, $imageManager)
		{
			$attributeVariant = $attributeService->getAttributeVariant((int) $id);
			$image = $values['image']->isOk() ? $imageManager->createImage($values['image']) : true;

			$attributeService->updateAttributeVariant(
				$attributeVariant,
				$values['name'],
				(float)$values['price'],
				$values['isDiscount'],
				$values['isNews'],
				$image
			);

			$self->redrawControl();
		};

		$this->getInlineAdd()->setText('Přidat novou variantu')
			->setTitle('Přidat novou vlastnost')
			->setIcon('plus')
			->setClass('btn btn-success');
	}
}