<?php

namespace App\AdminModule\Grids;

use App\Attribute\AttributeFacade;
use App\Attribute\Core\Attribute;
use App\Attribute\Product\ProductAttributeVariant;
use App\Attribute\ProductAttributeService;
use App\Product\Product;
use Nette\Forms\Container;

class ProductAttributeVariantGrid extends BaseGrid
{
	public function __construct(
		Attribute $attribute,
		Product $product,
		AttributeFacade $attributeFacade,
		ProductAttributeService $productAttributeService
	){
		parent::__construct();

		$self = $this;

		$this->setDataSource($attributeFacade->getProductAttributeVariantsGridSource($attribute, $product));

		$this->addColumnText('image', 'Obrázek')
			->setTemplate(__DIR__ . '/templates/imageAttached.latte');

		$this->addColumnText('name', 'Název')
			->setRenderer(function (ProductAttributeVariant $item){
				return $item->getAttributeVariant()->getName();
			})
			->setSortable();

		$this->addColumnText('price', 'Cena')
			->setRenderer(function (ProductAttributeVariant $item){
				return $item->getPrice();
			})
			->setSortable();

		$this->addColumnText('isDiscount', 'Sleva')
			->setSortable()
			->setRenderer(function (ProductAttributeVariant $item){
				return $item->getAttributeVariant()->isDiscount() ? 'Ano' : 'Ne';
			});

		$this->addColumnText('isNews', 'Novinka')
			->setSortable()
			->setRenderer(function (ProductAttributeVariant $item){
				return $item->getAttributeVariant()->isNews() ? 'Ano' : 'Ne';
			});

		$this->addAction('remove', null, 'removeAttributeVariant!')
			->setIcon('trash')
			->setTitle('Smazat')
			->setClass('btn btn-xs btn-danger')
			->setConfirm('Opravdu chcete smazat variantu vlastnosti?');

		$this->addGroupTextAction('Nastavit cenu')
			->onSelect[] = function(array $variantsIds, $price) use ($productAttributeService)
		{

			if ((float) $price < 0) {
				$this->flashMessage('Nastavovaná cena nesmí být záporná.', 'danger');
				if ($this->presenter->isAjax()) {
					$this->redrawControl('flashes');
					$this->reload();
				} else {
					$this->redirect('this');
				}

				return;
			}

			$productAttributeVariants = [];

			foreach ($variantsIds as $id) {
				$productAttributeVariants[] = $productAttributeService->getProductAttributeVariant((int) $id);
			}

			$productAttributeService->setPriceToProductAttributeVariants($productAttributeVariants, (float)$price);

			$this->flashMessage(
				'Cena byla úspěšně nastavena',
				'success'
			);

			if ($this->presenter->isAjax()) {
				$this->redrawControl('flashes');
				$this->reload();
			} else {
				$this->redirect('this');
			}
		};

		$this->addInlineEdit()->onControlAdd[] = function(Container $container) use ($attributeFacade)
		{
			$container->addText('price')
				->setAttribute('placeholder', 'Cena')
				->setType('number');
		};

		$this->getInlineEdit()->onSetDefaults[] = function(Container $container, ProductAttributeVariant $attributeVariant)
		{
			$container->setDefaults([
				'price' => $attributeVariant->getPrice(),
			]);
		};

		$this->getInlineEdit()->onSubmit[] = function($id, $values) use ($self, $attribute, $attributeFacade)
		{
			$attributeVariant = $attributeFacade->getAttributeVariantProduct((int)$id);
			$attributeVariant->setPrice((float)$values['price']);
			$attributeFacade->saveAttribute($attribute);

			$self->redrawControl();
		};

		$this->addToolbarButton('this#set-price-modal', 'Hromadně nastavit cenu')
			->setTitle('Hromadně nastavit cenu')
			->setIcon('dollar')
			->setClass('btn btn-default')
			->addAttributes(['data-toggle' => 'modal', 'data-target' => '#set-price-modal']);

		$this->addToolbarButton('Attribute:detail', 'Přidat novou vlastnost', ['id' => $attribute->getId()])
			->setTitle('Přidat novou vlastnost')
			->setIcon('plus')
			->setClass('btn btn-success');

		$this->addToolbarButton('this#modal-existing-variants', 'Přidat existující variantu vlastnosti')
			->setClass('btn btn-primary')
			->setIcon('plus')
			->addAttributes(['data-toggle' => 'modal', 'data-target' => '#modal-existing-variants']);

		$this->getInlineEdit()->setShowNonEditingColumns(false);
		$this->useHappyComponents(FALSE);
	}
}