<?php

namespace App\AdminModule\Grids;

use App\Attribute\Core\Attribute;

interface IAttributeVariantGridFactory
{
	/**
	 * @param Attribute $attribute
	 * @return AttributeVariantGrid
	 */
	public function create(Attribute $attribute);
}