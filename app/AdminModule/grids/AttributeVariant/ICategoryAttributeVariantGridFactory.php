<?php

namespace App\AdminModule\Grids;

use App\Attribute\Core\Attribute;
use App\Category\Entity\Category;

interface ICategoryAttributeVariantGridFactory
{
	/**
	 * @param Attribute $attribute
	 * @param Category $category
	 * @return CategoryAttributeVariantGrid
	 */
	public function create(Attribute $attribute, Category $category);
}