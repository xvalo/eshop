<?php

namespace App\AdminModule\Grids;

use App\Attribute\AttributeFacade;
use App\Attribute\Core\Attribute;
use App\Attribute\Product\CategoryAttributeVariant;
use App\Category\Entity\Category;

class CategoryAttributeVariantGrid extends BaseGrid
{
	public function __construct(
		Attribute $attribute,
		Category $category,
		AttributeFacade $attributeFacade
	){
		parent::__construct();

		$this->setDataSource($attributeFacade->getCategoryAttributeVariantsGridSource($attribute, $category));

		$this->addColumnText('image', 'Obrázek')
			->setTemplate(__DIR__ . '/templates/imageAttached.latte');

		$this->addColumnText('name', 'Název')
			->setRenderer(function (CategoryAttributeVariant $item){
				return $item->getAttributeVariant()->getName();
			})
			->setSortable();

		$this->addColumnText('price', 'Cena')
			->setRenderer(function (CategoryAttributeVariant $item){
				return $item->getPrice();
			})
			->setSortable();

		$this->addColumnText('isDiscount', 'Sleva')
			->setSortable()
			->setRenderer(function (CategoryAttributeVariant $item){
				return $item->getAttributeVariant()->isDiscount() ? 'Ano' : 'Ne';
			});

		$this->addColumnText('isNews', 'Novinka')
			->setSortable()
			->setRenderer(function (CategoryAttributeVariant $item){
				return $item->getAttributeVariant()->isNews() ? 'Ano' : 'Ne';
			});

		$this->addAction('remove', null, 'removeAttributeVariant!')
			->setIcon('trash')
			->setTitle('Smazat')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Opravdu chcete smazat variantu vlastnosti');

		$this->addToolbarButton('this#set-price-modal', 'Hromadně nastavit cenu')
			->setTitle('Hromadně nastavit cenu')
			->setIcon('dollar')
			->setClass('btn btn-default')
			->addAttributes(['data-toggle' => 'modal', 'data-target' => '#set-price-modal']);

		$this->addToolbarButton('Attribute:detail', 'Přidat novou vlastnost', ['id' => $attribute->getId()])
			->setTitle('Přidat novou vlastnost')
			->setIcon('plus')
			->setClass('btn btn-success');

		$this->addToolbarButton('this#modal-existing-variants', 'Přidat existující variantu vlastnosti')
			->setClass('btn btn-primary')
			->setIcon('plus')
			->addAttributes(['data-toggle' => 'modal', 'data-target' => '#modal-existing-variants']);
	}
}