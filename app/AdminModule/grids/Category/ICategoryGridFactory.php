<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\IContainer;

interface ICategoryGridFactory
{
	/**
	 * @param IContainer $parent
	 * @param string $name
	 * @return CategoryGrid
	 */
	public function create(IContainer $parent, $name);
}