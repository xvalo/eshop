<?php

namespace App\AdminModule\Grids;

use App\Category\CategoryFacade;
use App\Category\Entity\Category;
use App\Utils\Selects;
use Kdyby\Doctrine\EntityManager;
use Nette\ComponentModel\IContainer;

class CategoryGrid extends BaseGrid
{

	/** @var EntityManager */
	private $em;

	/** @var CategoryFacade */
	private $categoryFacade;

	public function __construct(
		IContainer $parent,
		$name,
		EntityManager $em,
		CategoryFacade $categoryFacade
	){
		parent::__construct($parent, $name);

		$this->em = $em;
		$this->categoryFacade = $categoryFacade;

		$this->setDataSource($this->getCategories());

		$this->setTemplateFile(__DIR__."/../templates/datagrid_tree.latte");

		$this->setTreeView([$this, 'getChildren'], [$this, 'hasChildren']);

		$this->addColumnLink('name', 'Kategorie', 'Product:default', 'name', ['grid-filter[category]' => 'id']);

		$this->addColumnText('postscript', 'Dovětek')
				->setRenderer(function(Category $category){
					return $category->getPostscript();
				});

		$this->addColumnNumber('activeProducts', 'Aktivních produktů')
				->setRenderer(function(Category $category){
					return $category->getProductsCount();
				});

		$this->addColumnText('active', 'Aktivní')
				->setSortable()
				->setReplacement(Selects::yesNo());

		$this->addAction('edit', '', 'edit', ['id'])
				->setIcon('pencil');

		$this->addAction('delete', '', 'delete!')
				->setIcon('trash')
				->setTitle('Delete')
				->setClass('btn btn-xs btn-danger ajax')
				->setConfirm('Opravdu chcete smazat kategorii %s?', 'name');

		$this->setSortable();

		$this->em = $em;
		$this->categoryFacade = $categoryFacade;
	}

	public function getChildren($id)
	{
		return $this->getCategories($this->categoryFacade->getCategory(intval($id)));
	}

	public function hasChildren(Category $category)
	{
		return $category->hasChildren();
	}

	private function getCategories(Category $category = null)
	{
		$qb = $this->em->createQueryBuilder()
				->select('C')
				->from(Category::class, 'C');

		if($category)
		{
			$qb->where('C.parent = :parent')
				->setParameter('parent', $category);
		}
		else
		{
			$qb->where('C.parent IS NULL');
		}

		$qb->andWhere('C.deleted = :deleted')
			->setParameter('deleted', false)
			->orderBy($qb->expr()->asc('C.levelOrder'));

		return $qb;
	}
}