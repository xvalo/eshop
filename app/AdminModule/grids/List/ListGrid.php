<?php

namespace App\AdminModule\Grids;

use App\Lists\Factory\ListRepositoryFactory;
use App\Lists\ListsTypes;
use App\Lists\Producer;
use App\Lists\Stock;
use Nette\Forms\Container;

class ListGrid extends BaseGrid
{
	private $type;

	private $repository;

	public function __construct($parent, $name, $type, ListRepositoryFactory $factory)
	{
		$this->type = $type;
		$this->repository = $factory->getRepository($this->type);

		parent::__construct($parent, $name, $this->repository);

		$this->addColumnText('name', 'Název')
			->setSortable()
			->setFilterText('name');

		$this->addColumnText('value', 'Hodnota')
				->setSortable()
				->setFilterText('value');

		if ($type != ListsTypes::LIST_SYSTEM_VARIABLE) {
			$this->setInlineAdd();
			$this->addAction('delete', '', 'delete!')
					->setIcon('trash')
					->setTitle('Delete')
					->setClass('btn btn-xs btn-danger ajax')
					->setConfirm('Opravdu chcete smazat položku %s?', 'name');
		}

		$this->setInlineEdit();
	}

	private function setInlineAdd()
	{
		$this->addInlineAdd()->onControlAdd[] = function(Container $container)
		{
			$container->addText('name');
			$container->addText('value');
		};

		$this->getInlineAdd()->onSubmit[] = function($values)
		{
			$item = null;
			switch($this->type)
			{
				case ListsTypes::LIST_PRODUCER:
					$item = new Producer($values->name, $values->value);
					break;
				case ListsTypes::LIST_STOCK:
					$item = new Stock($values->name, $values->value);
					break;
			}
			$this->repository->persist($item);
			$this->repository->flush();
			if ($this->parent->isAjax()) {
				$this->reload();
			} else {
				$this->parent->redirect('this');
			}
		};
	}

	private function setInlineEdit()
	{
		$this->addInlineEdit()->onControlAdd[] = function(Container $container)
		{
			$container->addText('name');
			$container->addText('value');
		};

		$this->getInlineEdit()->onSetDefaults[] = function(Container $container, $item)
		{
			$container->setDefaults([
					'name' => $item->getName(),
					'value' => $item->getValue()
			]);
		};

		$this->getInlineEdit()->onSubmit[] = function($id, $values)
		{
			$item = $this->repository->find($id);
			$item->setName($values->name);
			$item->setValue($values->value);
			$this->repository->flush();
			if ($this->parent->isAjax()) {
				$this->reload();
			} else {
				$this->parent->redirect('this');
			}

		};
	}
}