<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\Container;

interface IListGridFactory
{
	/**
	 * @param Container $parent
	 * @param string $name
	 * @param $type
	 * @return ListGrid
	 */
	public function create($parent, $name, $type);
}