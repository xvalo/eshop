<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\Container;

interface IOrderGridFactory
{
	/**
	 * @param Container $parent
	 * @param string $name
	 * @param string $type
	 * @return OrderGrid
	 */
	public function create($parent, $name, $type = OrderGrid::TYPE_COMMON);
}