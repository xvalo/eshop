<?php

namespace App\AdminModule\Grids;

use App\File\OrderDocumentType;
use App\Lists\DeliveryRepository;
use App\Model\Services\ApplicationConfigurationService;
use App\Order\Delivery\DeliveryService;
use App\Order\Order;
use App\Order\OrderLine\DeliveryOrderLine;
use App\Order\OrderRepository;
use App\Order\OrderStatus;
use App\Utils\HtmlHelper;
use App\Utils\Selects;
use Doctrine\ORM\Query\Expr\Join;
use Kdyby\Doctrine\QueryBuilder;
use Nette\Utils\DateTime;
use Nette\Utils\Html;

class OrderGrid extends BaseGrid
{
	const TYPE_COMMON = 'commonGrid';
	const TYPE_CANCELED = 'canceledGrid';
	const TYPE_GLS_DELIVERY = 'glsGrid';
	const TYPE_CP_DELIVERY = 'cpGrid';

	/** @var OrderRepository */
	private $repository;

	/** @var ApplicationConfigurationService */
	private $applicationConfigurationService;

	public function __construct(
		$parent,
		$name,
		$type = self::TYPE_COMMON,
		OrderRepository $repository,
		ApplicationConfigurationService $applicationConfigurationService,
		DeliveryService $deliveryService
	){
		parent::__construct($parent, $name);

		$this->repository = $repository;
		$this->applicationConfigurationService = $applicationConfigurationService;

		$this->setDataSource($this->getSourceByType($type));

		$this->setDefaultSort(['created' => 'DESC']);

		$this->addColumnText('orderNo', 'Číslo objednávky')
			->setSortable()
			->setRenderer(function(Order $item) use ($parent){
				return Html::el('a')->href($parent->link('Order:detail', ['id' => $item->getId()]))->setText($item->getOrderNo());
			})
			->setFilterText('orderNo');

		$statuses[null] = '';
		$statuses += OrderStatus::$statesLabels;

		$this->addColumnText('status', 'Stav')
				->setSortable()
				->setRenderer(function(Order $item){
					return OrderStatus::getStatusLabel($item->getStatus());
				})
				->setFilterSelect($statuses, 'status');

		$this->addColumnText('shipped', 'Expedováno')
				->setSortable()
				->setRenderer(function(Order $item){
					return $item->isShipped() ? HtmlHelper::iconYes() : HtmlHelper::iconNo();
				});

		$this->addFilterSelect('shipped', 'Expedováno', Selects::yesNo())
				->setCondition(function (QueryBuilder $qb, $value) {
					if ($value == 1) {
						$qb->andWhere($qb->expr()->isNotNull('O.shipped'));
					} elseif ($value == 0) {
						$qb->andWhere($qb->expr()->isNull('O.shipped'));
					}
				});

		$this->addColumnText('payed', 'Zaplaceno')
				->setSortable()
				->setRenderer(function(Order $item){
					return $item->isPayed() ? HtmlHelper::iconYes() : HtmlHelper::iconNo();
				});

		$this->addFilterSelect('payed', 'Zaplaceno', Selects::yesNo())
			->setCondition(function (QueryBuilder $qb, $value) {
				if ($value == 1) {
					$qb->andWhere($qb->expr()->isNotNull('O.payed'));
				} elseif ($value == 0) {
					$qb->andWhere($qb->expr()->isNull('O.payed'));
				}
			});

		$this->addColumnText('customer', 'Zakaznik')
				->setSortable()
				->setRenderer(function(Order $item){
					return $item->getContactInformation()->getName()
							. ' '
							. $item->getContactInformation()->getLastName();
				});

		$this->addFilterText('customer', 'Zákazník')
			->setCondition(function(QueryBuilder $qb, $value) {
				$name = trim($value);

				$qb->join('O.contactInformation', 'CI');

				$qb->andWhere(
					$qb->expr()->orX(

						$qb->expr()->like("CONCAT(CI.name, ' ', CI.lastName)", ':name'),
						$qb->expr()->like("CONCAT(CI.lastName, ' ', CI.name)", ':name')
					)
				);

				$qb->setParameter('name', '%'.$name.'%');
			});

		$this->addColumnDateTime('created', 'Vytvořeno')
				->setSortable()
				->setFormat('d. m. Y. H:i')
				->setFilterDateRange('created');


		if ($type == self::TYPE_CP_DELIVERY || $type == self::TYPE_GLS_DELIVERY) {
			$this->addColumnDateTime('documentCreated', 'Faktura vytvořena')
				->setSortable()
				->setRenderer(function(Order $item){
					return $item->getReceipt()->getCreated()->format('d. m. Y. H:i');
				});


			$this->addFilterDateRange('documentCreated', 'Faktura vytvořena')
				->setCondition(function (QueryBuilder $qb, $value) {

					$qb->innerJoin('O.documents', 'D')
						->andWhere($qb->expr()->gte('D.deleted', ':deleted'))
						->andWhere($qb->expr()->gte('D.type', ':type'));

					$qb->setParameters([
						'deleted' => 0,
						'type' => OrderDocumentType::INVOICE
					]);

					if (strlen($value->from)) {
						$fromParts = explode('. ', $value->from);

						$from = (new DateTime())
									->setDate($fromParts[2], $fromParts[1], $fromParts[0])
									->setTime(0, 0, 0);
						$qb->andWhere($qb->expr()->gte('D.created', ':from'));
						$qb->setParameter('from', $from);
					}

					if (strlen($value->to)) {
						$toParts = explode('. ', $value->to);
						$to = (new DateTime())
									->setDate($toParts[2], $toParts[1], $toParts[0])
									->setTime(0, 0, 0);
						$qb->andWhere($qb->expr()->lte('D.created', ':to'));
						$qb->setParameter('to', $to);
					}
				});

		}


		if ($type == self::TYPE_COMMON) {
			$this->addColumnText('delivery', 'Doprava')
					->setSortable()
					->setRenderer(function (Order $item) use ($applicationConfigurationService){
						return $item->getDelivery() ? $item->getDelivery()->getName() : 'Na prodejně';
					});

			$deliveryTypes = [null => '', 0 => 'Na prodejně'] + $deliveryService->getDeliveriesList();

			$this->addFilterSelect('delivery', 'Doprava', $deliveryTypes)
					->setCondition(function (QueryBuilder $qb, $value) use ($applicationConfigurationService) {

						switch ($value) {
							case "0":
								$subQuery = $qb->getEntityManager()->createQueryBuilder();
								$subQuery->select('OL')
									->from(DeliveryOrderLine::class, 'OL')
									->where(
										$subQuery->expr()->andX(
											$subQuery->expr()->eq('OL.order', 'O.id')
										)
									);
								$qb->andWhere(
									$qb->expr()->not(
										$qb->expr()->exists(
											$subQuery->getDQL()
										)
									)
								);

								break;
							default:
								$qb->join('\App\Order\OrderLine\DeliveryOrderLine', 'D', Join::WITH, 'O.id = D.order');
								$qb->andWhere($qb->expr()->eq('D.delivery', $value));
								break;
						}
					});
		}

		$this->addColumnNumber('totalPrice', 'Celková cena')
				->setSortable();

		$this->addColumnText('packageNo', 'Číslo balíku')
				->setSortable()
				->setFilterText('packageNo');

		$this->addAction('detail', '', 'detail', ['id'])
				->setIcon('search');

		$this->addAction('edit', '', 'edit', ['id'])
			->setIcon('pencil');

		$this->allowRowsAction('edit', function (Order $item){
			return $item->isEditable();
		});

        $this->setRememberState(false);
        $this->setStrictSessionFilterValues(FALSE);
	}

	private function getSourceByType($type)
	{
		switch ($type) {
			case self::TYPE_COMMON:
				return $this->repository->getGridData();
			case self::TYPE_CANCELED:
				return $this->repository->getCanceledGridData();
			case self::TYPE_GLS_DELIVERY:
				return $this->repository->getSpecialDeliveryGridData($this->applicationConfigurationService->getGlsDeliveries());
			case self::TYPE_CP_DELIVERY:
				return $this->repository->getSpecialDeliveryGridData($this->applicationConfigurationService->getCzechPostDeliveries());
		}
	}
}