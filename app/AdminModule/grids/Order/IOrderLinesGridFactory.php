<?php

namespace App\AdminModule\Grids;

use App\Order\Order;
use Nette\ComponentModel\Container;

interface IOrderLinesGridFactory
{
    /**
     * @param Container $parent
     * @param string $name
     * @param Order $order
     * @return OrderLinesGrid
     */
    public function create($parent, $name, Order $order);
}