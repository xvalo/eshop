<?php

namespace App\AdminModule\Grids;

use App\Order\AdminOrderService;
use App\Order\Order;
use App\Order\OrderLine\ExtraItemOrderLine;
use App\Order\OrderLine\OrderLine;
use App\Order\OrderLine\ProductOrderLine;
use App\Order\OrderLineState;
use App\Order\OrderRepository;
use App\Order\OrderService;
use App\Product\Availability\AvailabilityProvider;
use Nette\Utils\Html;

class OrderLinesGrid extends BaseGrid
{
    public function __construct(
        $parent,
        $name,
        Order $order,
        OrderRepository $repository,
        OrderService $service,
        AdminOrderService $adminOrderService,
		AvailabilityProvider $availabilityProvider
    ){
        parent::__construct($parent, $name);

        $this->setDataSource($repository->getOrderLinesForOrder($order));
        $this->setSortable(false);

        $presenter = $this->presenter;

        $this->addColumnText('description', 'Položka')
            ->setRenderer(function(OrderLine $item) {
                return $item instanceof ProductOrderLine ? '['.$item->getPrice()->getVariant()->getCodesString() . '] ' . $item->getTitle() : (string)$item;
			});

        $this->addColumnText('availability', 'Dostupnost')
            ->setRenderer(function(OrderLine $item) use ($presenter, $availabilityProvider){
            	if ($item instanceof ExtraItemOrderLine) {
            		return ' ------- ';
	            }

	            $priceVariant = $item->getPrice()->getVariant();
            	$availability = $priceVariant->getAvailability();
                return Html::el('a', ['target' => '_blank'])
                                ->href($presenter->link('Product:edit', ['id' => $priceVariant->getProduct()->getId()]))
                                ->setText($availability->getTitle());
            });

        $countColumn = $this->addColumnNumber('count', 'Počet');

        $this->addColumnNumber('unitPrice', 'Jednotková cena');

        $this->addColumnStatus('state', 'Stav')
                ->addOption(0, '---------')
                    ->setClass('btn-default')
                    ->endOption()
                ->addOption(OrderLineState::IN_STOCK, OrderLineState::getStatusLabel(OrderLineState::IN_STOCK))
                    ->setClass('btn-success')
                    ->endOption()
                ->addOption(OrderLineState::ORDERED, OrderLineState::getStatusLabel(OrderLineState::ORDERED))
                    ->setClass('btn-info')
                    ->endOption()
                ->addOption(OrderLineState::SOLD_OUT, OrderLineState::getStatusLabel(OrderLineState::SOLD_OUT))
                    ->setClass('btn-danger')
                    ->endOption()
                ->addOption(OrderLineState::PICKING, OrderLineState::getStatusLabel(OrderLineState::PICKING))
                    ->setClass('btn-success')
                    ->endOption()
                ->addOption(OrderLineState::DAMAGED, OrderLineState::getStatusLabel(OrderLineState::DAMAGED))
                    ->setClass('btn-danger')
                    ->endOption()
                ->addOption(OrderLineState::SENT, OrderLineState::getStatusLabel(OrderLineState::SENT))
                    ->setClass('btn-success')
                    ->endOption()
                ->addOption(OrderLineState::NOT_SENT, OrderLineState::getStatusLabel(OrderLineState::NOT_SENT))
                    ->setClass('btn-warning')
                    ->endOption();

        $this->addColumnCallback('state', function($column, $item) {
        	if ($item instanceof ExtraItemOrderLine) {
		        $column->setTemplate(NULL);
        		$column->setRenderer(function(){
        			return '---------';
		        });
	        }
        });

        if ($order->isEditable()) {
	        $self = $this;

        	$countColumn->setEditableCallback(function($id, $value) use ($self, $adminOrderService) {
		        $adminOrderService->changeOrderLineQuantity($id, $value);
	        })->setEditableInputType('text', ['class' => 'form-control']);

			$this->addAction('delete', '', 'deleteOrderLine', ['orderLineId' => 'id'])
				->setIcon('trash')
				->setTitle('Smazat')
				->setClass('btn btn-xs btn-danger')
				->setConfirm('Opravdu chcete smazat tuto položku?');
		}

        $this->setPagination(false);
    }
}