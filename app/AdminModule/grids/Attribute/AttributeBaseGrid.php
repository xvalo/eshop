<?php

namespace App\AdminModule\Grids;

use App\Attribute\AttributeFacade;
use App\Attribute\Core\Attribute;
use App\Category\Entity\Category;
use App\InvalidArgumentException;
use App\Product\Product;
use App\Utils\HtmlHelper;
use App\Utils\Selects;
use Nette\Forms\Container;

abstract class AttributeBaseGrid extends BaseGrid
{
	/** @var Category|Product */
	protected $attachedToEntity;

	public function __construct(
		$attachedToEntity,
		AttributeFacade $attributeFacade
	){
		parent::__construct();

		if (!($attachedToEntity instanceof Category) && !($attachedToEntity instanceof Product)) {
			throw new InvalidArgumentException('Invalid type of entity');
		}

		$this->attachedToEntity = $attachedToEntity;

		$this->addColumnText('name', 'Název')
			->setRenderer(function(Attribute $item){
				return $item->getCompleteName();
			})
			->setSortable()
			->setFilterText('name');

		$this->addColumnText('title', 'Na webu')
			->setSortable()
			->setFilterText('title');

		$this->addColumnText('parent', 'Nadřazená vlastnost')
			->setRenderer(function (Attribute $item){
				return $item->getParent() ? $item->getParent()->getCompleteName() : '';
			});

		$this->addColumnText('description', 'Popis')
			->setFilterText('description');

		$this->addColumnText('forced', 'Povinná')
			->setSortable()
			->setRenderer(function(Attribute $item) use ($attachedToEntity) {
				return $item->isForced($attachedToEntity) ? HtmlHelper::iconYes() : HtmlHelper::iconNo();
			})
			->setFilterSelect(Selects::yesNo());

		$this->addColumnText('active', 'Aktivní')
			->setSortable()
			->setRenderer(function(Attribute $item) use ($attachedToEntity) {
				return $item->isActive($attachedToEntity) ? HtmlHelper::iconYes() : HtmlHelper::iconNo();
			})
			->setFilterSelect(Selects::yesNo());

		$this->addInlineAdd()->onControlAdd[] = function(Container $container) use ($attributeFacade)
		{
			$container->addText('name')
				->setAttribute('placeholder', 'Název')
				->setMaxLength(60);
			$container->addText('title')
				->setAttribute('placeholder', 'Název na webu')
				->setMaxLength(60);
			$container->addText('description')
				->setAttribute('placeholder', 'Popis')
				->setMaxLength(200);
			$container->addSelect('forced', null, Selects::yesNo(false));
			$container->addSelect('active', null, Selects::yesNo(false));

		};

		$this->addInlineEdit()->onControlAdd[] = function(Container $container) use ($attributeFacade)
		{
			$container->addSelect('forced', null, Selects::yesNo(false));
			$container->addSelect('active', null, Selects::yesNo(false));
		};

		$this->getInlineEdit()->onSetDefaults[] = function(Container $container, Attribute $attribute)
		{
			$container->setDefaults([
				'name' => $attribute->getName(),
				'title' => $attribute->getTitle(),
				'description' => $attribute->getDescription(),
				'forced' => $attribute->isForced($this->attachedToEntity) ? 1 : 0,
				'active' => $attribute->isActive($this->attachedToEntity) ? 1 : 0
			]);
		};

		$this->getInlineAdd()->setText('Přidat novou vlastnost')
			->setTitle('Přidat novou vlastnost')
			->setClass('btn btn-success');

		$this->addToolbarButton('this#attributes', 'Přidat existující vlastnost')
			->setClass('btn btn-primary')
			->setIcon('plus')
			->addAttributes(['data-toggle' => 'modal', 'data-target' => '#modal-existing-attributes']);

		$this->allowRowsAction('showAttributeDetail', function(Attribute $attribute) {
			return !$attribute->hasChildren();
		});

		$this->setRememberState(false);
		$this->setStrictSessionFilterValues(false);
		$this->getInlineEdit()->setShowNonEditingColumns();
		$this->setPagination(false);
	}
}