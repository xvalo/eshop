<?php

namespace App\AdminModule\Grids;

use App\Category\Entity\Category;
use Nette\ComponentModel\IContainer;

interface ICategoryAttributeGridFactory
{
	/**
	 * @param Category $category
	 * @return CategoryAttributeGrid
	 */
	public function create(Category $category);
}