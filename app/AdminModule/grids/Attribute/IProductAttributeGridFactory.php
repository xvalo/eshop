<?php

namespace App\AdminModule\Grids;

use App\Product\Product;

interface IProductAttributeGridFactory
{
	/**
	 * @param Product $product
	 * @return ProductAttributeGrid
	 */
	public function create(Product $product);
}