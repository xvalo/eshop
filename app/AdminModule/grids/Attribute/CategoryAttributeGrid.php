<?php

namespace App\AdminModule\Grids;

use App\Attribute\AttributeFacade;
use App\Attribute\AttributeService;
use App\Attribute\CategoryAttributeService;
use App\Category\CategoryFacade;
use App\Category\Entity\Category;
use Kdyby\Doctrine\EntityManager;

class CategoryAttributeGrid extends AttributeBaseGrid
{
	public function __construct(
		Category $category,
		EntityManager $em,
		AttributeFacade $attributeFacade,
		AttributeService $attributeService,
		CategoryFacade $categoryFacade,
		CategoryAttributeService $categoryAttributeService
	){
		parent::__construct($category, $attributeFacade);

		$self = $this;

		$this->setDataSource($attributeFacade->getGridSourceForCategory($category));

		$this->getInlineAdd()->onSubmit[] = function($values) use ($self, $category, $attributeService, $categoryAttributeService)
		{
			$parent = null;
			if (isset($values['parent']) && intval($values['parent']) > 0) {
				$parent = $attributeService->getAttribute(intval($values['parent']));
			}

			$attribute = $attributeService->createAttribute($values['name'], $values['title'], $values['description'], $parent);

			$categoryAttributeService->attachAttributes(
				[$attribute->getId() => ['attach' => true, 'force' => $values['forced']]],
				$category
			);

			$this->flashMessage('Vlastnost byla úspěšně uložena', 'success');
			$self->redrawControl();
		};

		$this->getInlineEdit()->onSubmit[] = function($id, $values) use ($self, $category, $attributeService, $categoryAttributeService)
		{
			$attribute = $attributeService->getAttribute(intval($id));
			$categoryAttributeService->changeAttributeState($attribute, $category, $values['active'], $values['forced']);
			$this->flashMessage('Vlastnost byla úspěšně uložena', 'success');
			$self->redrawControl();
		};

		$this->addActionCallback('showAttributeDetail', 'Detail')
			->setIcon('eye')
			->setTitle('Detail attributu kategorie')
			->onClick[] = function($item_id) use ($category) {
			$this->presenter->redirect(':Admin:Category:attributes', ['attributeId' => $item_id, 'id' => $category->getId()]);
		};

		$this->addActionCallback('delete', '', function($id) use ($self, $category, $attributeService, $categoryAttributeService)
		{
			$categoryAttributeService->removeAttribute($attributeService->getAttribute((int)$id), $category);
			$self->redrawControl();
		})
			->setIcon('trash')
			->setTitle('Delete')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Opravdu chcete smazat vlastnost %s?', 'name');
	}
}