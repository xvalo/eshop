<?php

namespace App\AdminModule\Grids;

use App\Attribute\AttributeFacade;
use App\Attribute\AttributeService;
use App\Attribute\Core\Attribute;

class AttributesListGrid extends BaseGrid
{
	public function __construct(
		AttributeFacade $attributeFacade,
		AttributeService $attributeService
	){
		parent::__construct();

		$this->setDataSource($attributeFacade->getGridSource());

		$this->addColumnText('name', 'Název')
			->setRenderer(function(Attribute $item){
				return $item->getCompleteName();
			})
			->setSortable()
			->setFilterText('name');

		$this->addColumnText('title', 'Na webu')
			->setSortable()
			->setFilterText('title');

		$this->addColumnText('parent', 'Nadřazená vlastnost')
			->setRenderer(function (Attribute $item){
				return $item->getParent() ? $item->getParent()->getCompleteName() : '';
			})
			->setFilterSelect($attributeFacade->getAvailableParentAttributesList())
				->setPrompt('--- vyberte rodiče ---');

		$this->addColumnText('description', 'Popis')
			->setFilterText('description');

		$this->addAction('remove', null, 'removeAttribute!')
			->setIcon('trash')
			->setTitle('Smazat')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Opravdu chcete smazat vlastnost %s? S ní budou smazány i přiřazené podlvasnosti.', 'name');

		$this->addToolbarButton('Attribute:new', 'Přidat novou vlastnost')
			->setIcon('plus')
			->setClass('btn btn-success');

		$this->addAction('edit', '', 'edit', ['id'])
			->setIcon('pencil')
			->setClass('btn btn-xs btn-success')
			->setTitle('Upravit')
			->setText('Upravit');

		$this->setRememberState(false);
		$this->setStrictSessionFilterValues(false);
	}
}