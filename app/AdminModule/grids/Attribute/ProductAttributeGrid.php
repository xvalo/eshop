<?php

namespace App\AdminModule\Grids;

use App\Attribute\AttributeFacade;
use App\Attribute\AttributeService;
use App\Attribute\ProductAttributeService;
use App\Product\Product;

class ProductAttributeGrid extends AttributeBaseGrid
{
	public function __construct(
		Product $product,
		AttributeFacade $attributeFacade,
		AttributeService $attributeService,
		ProductAttributeService $productAttributeService
	){
		parent::__construct($product, $attributeFacade);

		$self = $this;

		$this->setDataSource($attributeFacade->getGridSourceForProduct($product));

		$this->getInlineAdd()->onSubmit[] = function($values) use ($self, $product, $attributeService, $productAttributeService)
		{
			$parent = null;
			if (isset($values['parent']) && intval($values['parent']) > 0) {
				$parent = $attributeService->getAttribute(intval($values['parent']));
			}

			$attribute = $attributeService->createAttribute($values['name'], $values['title'], $values['description'], $parent);

			$productAttributeService->attachAttributes(
				[$attribute->getId() => ['attach' => true, 'force' => $values['forced']]],
				$product
			);

			$self->redrawControl();
		};

		$this->getInlineEdit()->onSubmit[] = function($id, $values) use ($self, $product, $attributeService, $productAttributeService)
		{
			$attribute = $attributeService->getAttribute(intval($id));
			$productAttributeService->changeAttributeState($attribute, $product, $values['active'], $values['forced']);
			$self->redrawControl();
		};

		$this->addActionCallback('showAttributeDetail', 'Detail')
			->setIcon('eye')
			->setTitle('Detail attributu produktu')
			->onClick[] = function($item_id) use ($product) {
			$this->presenter->redirect(':Admin:Product:attributes', ['attributeId' => $item_id, 'id' => $product->getId()]);
		};

		$this->addActionCallback('delete', '', function($id) use ($self, $product, $productAttributeService, $attributeService)
		{
			$productAttributeService->removeAttribute($attributeService->getAttribute((int)$id), $product);
			$self->redrawControl();
		})
			->setIcon('trash')
			->setTitle('Delete')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Opravdu chcete smazat vlastnost %s?', 'name');

	}
}