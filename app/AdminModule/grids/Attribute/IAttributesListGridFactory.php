<?php

namespace App\AdminModule\Grids;

interface IAttributesListGridFactory
{
	/**
	 * @return AttributesListGrid
	 */
	public function create();
}