<?php

namespace App\AdminModule\Grids;


use App\Tag\TagRepository;

class TagGrid extends BaseGrid
{
    public function __construct($parent, $name, TagRepository $repository)
    {
        parent::__construct($parent, $name, $repository);

        $this->addColumnText('name', 'Název')
            ->setSortable()
            ->setFilterText('name');

        $this->addAction('edit', '', 'edit', ['id'])
            ->setIcon('pencil');

        $this->addAction('delete', '', 'delete!')
            ->setIcon('trash')
            ->setTitle('Delete')
            ->setClass('btn btn-xs btn-danger ajax')
            ->setConfirm('Do you really want to delete example %s?', 'title');
    }
}