<?php

namespace App\AdminModule\Grids;


use Nette\ComponentModel\IContainer;

interface ITagGridFactory
{
    /**
     * @param IContainer $parent
     * @param string $name
     * @return TagGrid
     */
    public function create($parent, $name);
}