<?php

namespace App\AdminModule\Grids;

interface IFilterGridFactory
{
	/**
	 * @return FilterGrid
	 */
	public function create();
}