<?php

namespace App\AdminModule\Grids;

use App\Filter\Entity\Filter;
use App\Filter\Entity\Value;
use App\Filter\FilterFacade;
use Nette\Forms\Container;

class FilterValuesGrid extends BaseGrid
{
	public function __construct(
		Filter $filter,
		FilterFacade $filterFacade
	){
		parent::__construct();

		$self = $this;

		$this->setDataSource($filterFacade->getFilterValuesGridSource($filter));

		$this->addColumnText('value', 'Hodnota')
			->setSortable()
			->setFilterText('value');


		$this->addInlineAdd()->onControlAdd[] = function(Container $container)
		{
			$container->addText('value');
		};

		$this->getInlineAdd()->onSubmit[] = function($values) use ($self, $filterFacade, $filter)
		{
			$sequence = $filterFacade->getValueMaxSequenceNumber($filter);
			$sequence = $sequence !== null ? $sequence + 1 : 1;

			$filter->addValue($values['value'], $sequence);

			$filterFacade->saveFilter($filter);
			$self->reload();
		};

		$this->addInlineEdit()->onControlAdd[] = function(Container $container)
		{
			$container->addText('value');
		};

		$this->getInlineEdit()->onSetDefaults[] = function(Container $container, Value $value)
		{
			$container->setDefaults([
				'value' => $value->getValue()
			]);
		};

		$this->getInlineEdit()->onSubmit[] = function($id, $values) use ($self, $filterFacade)
		{
			$value = $filterFacade->getValue((int)$id);
			$value->setValue($values['value']);

			$filterFacade->saveFilterValue($value);

			$self->reload();
		};

		$this->getInlineAdd()
			->setIcon('plus')
			->setText('Přidat novou hodnotu')
			->setClass('btn btn-success');

		$this->addAction('remove', null, 'removeValue!', ['id' => 'filter.id', 'value' => 'id'])
			->setIcon('trash')
			->setTitle('Smazat')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Opravdu chcete smazat hodnotu %s?', 'value');

		$this->setSortable();
	}
}