<?php

namespace App\AdminModule\Grids;

use App\Filter\FilterFacade;
use App\Utils\Selects;

class FilterGrid extends BaseGrid
{
	public function __construct(
		FilterFacade $filterFacade
	){
		parent::__construct();

		$this->setDataSource($filterFacade->getGridSource());

		$this->addColumnText('name', 'Název')
			->setSortable()
			->setFilterText('name');

		$this->addColumnText('title', 'Název na webu')
			->setSortable()
			->setFilterText('name');

		$this->addColumnText('description', 'Popis')
			->setFilterText('description');

		$this->addColumnText('active', 'Aktivní')
			->setSortable()
			->setReplacement(Selects::yesNo())
			->setFilterSelect(Selects::yesNo());


		$this->addAction('remove', null, 'remove!')
			->setIcon('trash')
			->setTitle('Smazat')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Opravdu chcete smazat filter %s? S ním budou smazány i přiřazené hodnoty.', 'name');

		$this->addToolbarButton('Filter:new', 'Přidat nový filtr')
			->setIcon('plus')
			->setClass('btn btn-success');

		$this->addAction('edit', '', 'edit', ['id'])
			->setIcon('pencil')
			->setClass('btn btn-xs btn-success')
			->setTitle('Upravit')
			->setText('Upravit');
	}
}