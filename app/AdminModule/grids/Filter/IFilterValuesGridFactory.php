<?php

namespace App\AdminModule\Grids;

use App\Filter\Entity\Filter;

interface IFilterValuesGridFactory
{
	/**
	 * @param Filter $filter
	 * @return FilterValuesGrid
	 */
	public function create(Filter $filter);
}