<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\Container;

interface IOrderDocumentGridFactory
{
    /**
     * @param Container $parent
     * @param string $name
     * @param array $documentTypes
     * @return OrderDocumentGrid
     */
    public function create($parent, $name, $documentTypes);
}