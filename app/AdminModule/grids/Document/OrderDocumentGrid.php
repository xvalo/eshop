<?php

namespace App\AdminModule\Grids;

use App\File\OrderDocument;
use App\File\OrderDocumentRepository;
use Kdyby\Doctrine\QueryBuilder;
use Nette\Utils\Html;

class OrderDocumentGrid extends BaseGrid
{
    public function __construct($parent, $name, $documentTypes, OrderDocumentRepository $repository)
    {
        parent::__construct($parent, $name);

        $this->setDataSource($repository->getGridDataByType($documentTypes));
        $this->setDefaultSort(['created' => 'DESC']);

        $this->addColumnText('number', 'Číslo', 'number')
            ->setSortable()
            ->setFilterText('number');

        $this->addColumnText('order', 'Objednávka')
            ->setSortable()
            ->setRenderer(function (OrderDocument $item) use ($parent){
                return Html::el('a')->href($parent->link('Order:detail', ['id' => $item->getOrder()->getId()]))->setText($item->getOrder()->getOrderNo());
            });

        $this->addFilterText('order', 'Objednávka')
	        ->setCondition(function(QueryBuilder $qb, $value){
				$qb->join('d.order', 'O')
					->andWhere(
						$qb->expr()->like('O.orderNo', ':number')
					);

				$qb->setParameter('number', '%'.$value.'%');
	        });

        $this->addColumnDateTime('created', 'Datum vytvoření')
	        ->setSortable()
            ->setFormat('d. m. Y H:i')
	        ->setFilterDateRange('created');

        $this->addColumnText('customer', 'Zákaznik')
            ->setSortable()
            ->setRenderer(function (OrderDocument $item) {
                $contactInformation = $item->getOrder()->getContactInformation();
                return $contactInformation->getName() . ' ' . $contactInformation->getLastName();
            });

	    $this->addFilterText('customer', 'Zákazník')
		    ->setCondition(function(QueryBuilder $qb, $value) {
			    $name = trim($value);

			    $qb->join('d.order', 'O')
				    ->join('O.contactInformation', 'CI');

			    $qb->andWhere(
				    $qb->expr()->orX(

					    $qb->expr()->like("CONCAT(CI.name, ' ', CI.lastName)", ':name'),
					    $qb->expr()->like("CONCAT(CI.lastName, ' ', CI.name)", ':name')
				    )
			    );

			    $qb->setParameter('name', '%'.$name.'%');
		    });

        $this->addAction('download', '', 'download!', ['document' => 'id'])
                ->setIcon('download');

        $this->setRememberState(false);
        $this->setStrictSessionFilterValues(FALSE);
    }
}