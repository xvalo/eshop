<?php

namespace App\AdminModule\Grids;

use App\Product\Waiting\Entity\Product;
use App\Product\Waiting\WaitingProductFacade;
use App\Stock\Supplier\SupplierService;
use Nette\ComponentModel\IContainer;
use Nette\Utils\Html;

class WaitingProductsGrid extends BaseGrid
{
	public $onProductSave = [];

	public function __construct(
		IContainer $parent,
		string $name,
		WaitingProductFacade $waitingProductFacade,
		SupplierService $supplierService
	){
		parent::__construct($parent, $name);

		$this->setDefaultPerPage(15);
		$this->setDataSource($waitingProductFacade->getUnprocessedWaitingProductsQB());

		$this->addColumnText('img', 'Obrázek')
			->setTemplate(__DIR__ . '/image.latte');

		$this->addColumnText('name', 'Produkt')
			->setRenderer(function(Product $waitingProduct){
				return $waitingProduct->getData()->getName();
			});

		$this->addColumnText('code', 'Kód')
				->setSortable()
				->setFilterText('code');

		$this->addColumnText('producer', 'Výrobce')
			->setRenderer(function(Product $waitingProduct){
				return $waitingProduct->getData()->getProducer();
			});

		$this->addColumnText('supplier', 'Dodavatel')
			->setSortable()
			->setFilterSelect([null => '--- Vyberte dodavatele ---'] + $supplierService->getSuppliersList(), 'supplier');

		$this->addColumnText('url', 'Odkaz')
			->setRenderer(function(Product $waitingProduct){
				$url = $waitingProduct->getData()->getUrl();
				return $url ? Html::el('a')->href($url)->addText($url)->setAttribute('target', '_blank') : ' --- ';
			});

		$this->addAction('create', 'Vytvořit produkt', 'create')
			->setIcon('plus')
			->setTitle('Vytvořit produkt')
			->setOpenInNewTab();

		$this->addAction('delete', 'Smazat', 'delete!')
			->setIcon('trash')
			->setTitle('Smazat')
			->setClass('btn btn-xs btn-danger ajax')
			->setConfirm('Opravdu chcete tento čekající produkt smazat?');

		$this->setRememberState(false);
		$this->setStrictSessionFilterValues(false);


	}
}