<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\IContainer;

interface IWaitingProductsGridFactory
{
    /**
     * @param IContainer $parent
     * @param string $name
     * @return WaitingProductsGrid
     */
    public function create(IContainer $parent, string $name);
}