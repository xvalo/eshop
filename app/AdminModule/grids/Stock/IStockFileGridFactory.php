<?php

namespace App\AdminModule\Grids;


use Nette\ComponentModel\IContainer;

interface IStockFileGridFactory
{
    /**
     * @param IContainer $parent
     * @param string $name
     * @param $type
     * @return StockFileGrid
     */
    public function create(IContainer $parent, $name, $type);
}