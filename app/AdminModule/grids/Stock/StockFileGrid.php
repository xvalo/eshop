<?php

namespace App\AdminModule\Grids;

use App\Stock\StockFile;
use App\Stock\StockFileRepository;
use Doctrine\ORM\QueryBuilder;
use Kdyby\Translation\Translator;
use Nette\ComponentModel\IContainer;

class StockFileGrid extends BaseGrid
{
    public function __construct(
        IContainer $parent,
        $name,
        $type,
        StockFileRepository $repository
    ) {
        parent::__construct($parent, $name, $repository);

        $this->setDataSource($repository->getGridDataByType($type));

        $this->addColumnDateTime('created', 'Vytvořeno')
                ->setSortable()
                ->setFilterDateRange('created');

        $this->addColumnText('stock', 'Sklad')
                ->setSortable()
                ->setRenderer(function(StockFile $item){
                    return $item->getStock()->getName();
                })
                ->setFilterText('stock');

        $this->addColumnText('user', 'Zaměstnanec')
                ->setSortable()
                ->setRenderer(function(StockFile $item){
                    return $item->getUser()->getName() . ' ' . $item->getUser()->getLastName();
                });

        $this->addFilterText('user', 'Zaměstnanec')
	        ->setCondition(function (QueryBuilder $qb, $value) {
	        	$qb->join('SF.user', 'U')
			        ->andWhere(
			        	$qb->expr()->orX(
			        		$qb->expr()->like('U.name', ':name'),
					        $qb->expr()->like('U.lastName', ':lastName')
				        )
			        );
		        $qb->setParameter('name', "%$value%");
		        $qb->setParameter('lastName', "%$value%");
	        });


        $this->addAction('download', '', 'download!', ['id'])
                ->setTitle('Stáhnout')
                ->setIcon('download')
                ->setClass('btn btn-xs btn-success');
    }
}