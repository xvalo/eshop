<?php

namespace App\AdminModule\Grids;

use App\Stock\StockItemRepository;
use App\Utils\Selects;

class StockGrid extends BaseGrid
{
	public function __construct(
		StockItemRepository $repository
	){
		parent::__construct();

		$this->setDataSource($repository->getStockGridData());
		$this->setDefaultSort(['countOnStock' => 'DESC']);

		$this->addColumnLink('name', 'Produkt', 'edit', 'productName')
			->setSortable()
			->setFilterText('productName')
			->setSplitWordsSearch(FALSE);

		$this->addColumnText('countOnStock', 'Počet kusů', 'onStock')
			->setSortable();

		$this->addColumnText('category', 'Kategorie', 'categoryName')
			->setSortable();

		$this->addColumnText('producer', 'Výrobce', 'producer')
			->setSortable()
			->setFilterText('producer');

		$this->addColumnText('active', 'Aktivní', 'active')
			->setSortable()
			->setReplacement(Selects::yesNo())
			->setFilterSelect(Selects::yesNo());

		$this->addAction('edit', '', 'edit', ['id'])
			->setIcon('pencil');
	}
}

interface IStockGridFactory
{
	/**
	 * @return StockGrid
	 */
	public function create();
}