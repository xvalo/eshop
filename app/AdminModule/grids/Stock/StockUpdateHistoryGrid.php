<?php

namespace App\AdminModule\Grids;

use App\Product\Availability\AvailabilityProvider;
use App\Stock\Supplier\SupplierService;
use App\Stock\Update\Source\History\SourceUpdateHistoryFacade;
use App\Stock\Update\Source\History\StockStateUpdateHistory;
use Doctrine\ORM\QueryBuilder;
use Nette\ComponentModel\IContainer;
use Nette\Utils\Html;

class StockUpdateHistoryGrid extends BaseGrid
{
    public function __construct(
    	IContainer $parent,
    	string $name,
    	SourceUpdateHistoryFacade $facade,
	    AvailabilityProvider $availabilityProvider,
		SupplierService $supplierService
    ){
        parent::__construct($parent, $name);

        $this->setDataSource($facade->getSourceHistoryQueryBuilder());
        $this->setDefaultSort(['created' => 'DESC']);

	    $this->addColumnText('code', 'Kód')
		    ->setSortable()
		    ->setRenderer(function(StockStateUpdateHistory $item){
			    return $item->getPriceVariant()->getVariant()->getCodesString();
		    });

	    $this->addColumnText('number', 'Číslo', 'priceVariant.variant.product.number')
			->setSortable();

        $this->addColumnText('variant', 'Cenová varianta')
                ->setSortable()
                ->setRenderer(function(StockStateUpdateHistory $item) use ($parent){
                    $price = $item->getPriceVariant();
                    return Html::el('a', ['target' => '_blank'])
                        ->href($parent->link('Product:variants', ['id' => $price->getVariant()->getProduct()->getId(), 'variantId' => $price->getVariant()->getId()]))
                        ->setText($price->getPriceName());
                });

	    $this->addColumnText('availability', 'Dostupnost', 'availability.title')
		    ->setSortable()
	        ->setFilterSelect(['' => 'Všechny'] + $availabilityProvider->getSelectList());

	    $this->addColumnText('price', 'Cena')
		    ->setSortable()
		    ->setRenderer(function(StockStateUpdateHistory $item){
			    return (int)$item->getPrice() . ',- Kč';
		    });

	    $this->addColumnText('supplier', 'Dodavatel')
		    ->setSortable()
		    ->setRenderer(function(StockStateUpdateHistory $item){
			    return $item->getSupplier()->getName();
		    })
	        ->setFilterSelect(['' => 'Všechny'] + $supplierService->getSuppliersList());

        $this->addColumnDateTime('created', 'Vytvořeno')
                ->setSortable()
	            ->setFormat('d. m. Y H:i:s')
                ->setFilterDateRange('created');

        $this->addFilterText('number', 'Číslo produktu')
            ->setCondition(function(QueryBuilder $builder, $value) {
            	$builder->andWhere(
	                	$builder->expr()->like('P.number', ':productNumber')
	                )->setParameter('productNumber', "%$value%");
            });

        $this->addFilterText('variant', 'Varianta')
	        ->setCondition(function(QueryBuilder $builder, $value) {
	        	$builder->andWhere(
	        		$builder->expr()->orX(
						$builder->expr()->like('PV.name', ':name'),
				        $builder->expr()->like('V.name', ':name'),
				        $builder->expr()->like('P.name', ':name')
			        )
		        )->setParameter('name', "%$value%");
	        });

        $this->setItemsDetail(__DIR__.'/historyDetail.latte')
            ->setTemplateParameters([
            	'suppliers' => $supplierService->getSuppliersList(),
	            'availabilityList' => $availabilityProvider->getSelectList()
            ]);

        $this->setRememberState(false);
        $this->setStrictSessionFilterValues(false);

    }
}