<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\IContainer;

interface IStockUpdateHistoryGridFactory
{
    /**
     * @param IContainer $parent
     * @param string $name
     * @return StockUpdateHistoryGrid
     */
    public function create(IContainer $parent, string $name);
}