<?php

namespace App\AdminModule\Grids;

use App\Customer\Customer;
use App\Customer\CustomerRepository;
use App\Customer\CustomerService;

class CustomerGrid extends BaseGrid
{
	public function __construct(
			$parent,
			$name,
			CustomerRepository $repository,
			CustomerService $service
	){
		parent::__construct($parent, $name, $repository);

		$this->addColumnText('name', 'Jméno')
				->setSortable()
				->setFilterText('name');

		$this->addColumnText('lastName', 'Příjmení')
				->setSortable()
				->setFilterText('lastName');

		$this->addColumnText('email', 'E-mail')
				->setSortable()
				->setFilterText('email');

		$this->addColumnStatus('active', 'Aktivní')
				->addOption(1, 'Ano')
					->setClass('btn-success')
					->endOption()
				->addOption(0, 'Ne')
					->setClass('btn-danger')
					->endOption()
				->onChange[] = function ($id, $newValue) use ($service) {
					/** @var Customer $customer */
					$customer = $service->getOne($id);
					$values['active'] = $newValue == "1";

					$service->update($customer, $values);
					$this->reload();
				};

		$this->addAction('edit', '', 'edit', ['id'])
			->setIcon('pencil');

		/*
		$this->addColumnStatus('mailing', 'Newsletter')
				->addOption(1, 'Ano')
					->setClass('btn-success')
					->endOption()
				->addOption(0, 'Ne')
					->setClass('btn-danger')
					->endOption()
				->onChange[] = function ($id, $newValue) use ($service) {

					$customer = $service->getOne($id);
					$service->changeCustomerMailingStatus($customer, $newValue);
					$this->reload();
				};
		*/
	}
}