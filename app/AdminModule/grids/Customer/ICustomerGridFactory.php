<?php
/**
 * Created by PhpStorm.
 * User: Boris
 * Date: 04.10.2016
 * Time: 18:35
 */

namespace App\AdminModule\Grids;

use Nette\ComponentModel\Container;

interface ICustomerGridFactory
{
	/**
	 * @param Container $parent
	 * @param string $name
	 * @return CustomerGrid
	 */
	public function create($parent, $name);
}