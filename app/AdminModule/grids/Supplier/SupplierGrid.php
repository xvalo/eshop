<?php

namespace App\AdminModule\Grids;

use App\Classes\Stock\Supplier\SupplierFacade;
use App\Stock\Supplier\Entity\Supplier;
use Nette\Utils\Html;

class SupplierGrid extends BaseGrid
{

	public function __construct(
		SupplierFacade $supplierFacade
	){
		parent::__construct();

		$this->setDataSource($supplierFacade->getGridSource());

		$this->addColumnText('name', 'Název')
			->setSortable()
			->setFilterText('name');

		$this->addColumnText('prefix', 'Prefix')
			->setSortable()
			->setFilterText('prefix');

		$this->addColumnText('active', 'Aktivní')
			->setRenderer(function(Supplier $item) {
				$element = Html::el('span', ['class' => ['label', ($item->isActive() ? 'label-success':'label-danger')]]);
				$element->addHtml(Html::el('i', [ 'class' => ['fa', ($item->isActive() ? 'fa-check' : 'fa-close')]]));

				return $element;
			})
			->setSortable();

		$this->addColumnText('synchronized', 'Syn. zapnuta')
			->setRenderer(function(Supplier $item) {
				$element = Html::el('span', ['class' => ['label', ($item->isSynchronized() ? 'label-success':'label-danger')]]);
				$element->addHtml(Html::el('i', [ 'class' => ['fa', ($item->isSynchronized() ? 'fa-check' : 'fa-close')]]));

				return $element;
			})
			->setSortable();

		$this->addColumnText('note', 'Poznámka')
			->setSortable()
			->setFilterText('note');

		$this->addAction('edit', '', 'edit', ['id'])
			->setIcon('pencil');

		$this->addAction('delete', '', 'delete!')
			->setIcon('trash')
			->setTitle('Delete')
			->setClass('btn btn-xs btn-danger ajax')
				->setConfirm('Opravdu chcete smazat položku %s?', 'name');

		$this->addToolbarButton('new', 'Přidat nového dodavatele')
			->setTitle('Přidat nového dodavatele')
			->setIcon('plus')
			->setClass('btn btn-success');

	}
}