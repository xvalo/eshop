<?php

namespace App\AdminModule\Grids;

interface ISupplierGridFactory
{
	/**
	 * @return SupplierGrid
	 */
	public function create();
}