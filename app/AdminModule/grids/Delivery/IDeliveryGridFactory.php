<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\IContainer;

interface IDeliveryGridFactory
{
	/**
	 * @param IContainer $parent
	 * @param string $name
	 * @return DeliveryGrid
	 */
	public function create(IContainer $parent, $name);
}