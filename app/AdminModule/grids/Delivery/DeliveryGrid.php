<?php

namespace App\AdminModule\Grids;

use App\Lists\Delivery;
use App\Order\Delivery\DeliveryFacade;
use App\Order\Payment\PaymentTypeFacade;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;

class DeliveryGrid extends BaseGrid
{
	/** @var DeliveryFacade */
	private $deliveryFacade;

	public function __construct(
		IContainer $parent,
		$name,
		DeliveryFacade $deliveryFacade,
		PaymentTypeFacade $paymentTypeFacade
	){
		parent::__construct($parent, $name);

		$this->deliveryFacade = $deliveryFacade;

		$this->setDataSource($deliveryFacade->getGridSource());

		$this->addColumnText('name', 'Název')
			->setSortable()
			->setFilterText('name');

		$this->addColumnText('price', 'Cena')
				->setSortable()
				->setFilterText('price');

		$this->addColumnText('weight', 'Váha upřednostnění')
			->setSortable()
			->setFilterText('weight');

		$this->addColumnText('freePriceLimit', 'Limit pro dopravu zdarma')
			->setSortable()
			->setFilterText('freePriceLimit');

		$this->addColumnText('trackUrl', 'URL zledování zásilky')
			->setSortable()
			->setFilterText('trackUrl');

		$this->setInlineAdd();
		$this->setInlineEdit();

		$this->addAction('paymentTypeSetting', '')
				->setIcon('dollar')
				->setTitle('Nastavení typů plateb')
				->setClass('btn btn-xs btn-primary');

		$this->addAction('delete', '', 'delete!')
				->setIcon('trash')
				->setTitle('Smazat')
				->setClass('btn btn-xs btn-danger ajax')
				->setConfirm('Opravdu chcete smazat položku %s?', 'name');

		$this->setSortable();

	}

	private function setInlineAdd()
	{
		$this->addInlineAdd()->onControlAdd[] = function(Container $container)
		{
			$container->addText('name');
			$container->addText('price')
				->addRule(Form::NUMERIC, 'Cena dopravy musí být číslo.')
				->setRequired(true);
			$container->addText('weight')
				->addRule(Form::NUMERIC, 'Váha priority musí být číslo')
				->setRequired(true);
			$container->addText('freePriceLimit')
				->addRule(Form::NUMERIC, 'Limit pro dopravu musí být číslo.')
				->setRequired(false);
			$container->addText('trackUrl');
		};

		$this->getInlineAdd()->onSubmit[] = function($values)
		{
			$delivery = new Delivery(
				$values->name,
				floatval($values->price),
				(int) $this->deliveryFacade->getHighestPriority()+1,
				floatval($values->freePriceLimit),
				isset($values->trackUrl) && strlen($values->trackUrl) > 0 ? $values->trackUrl : null,
				(int)$values->weight
			);

			$this->deliveryFacade->saveDelivery($delivery);
			if ($this->parent->isAjax()) {
				$this->reload();
			} else {
				$this->parent->redirect('this');
			}
		};
	}

	private function setInlineEdit()
	{
		$this->addInlineEdit()->onControlAdd[] = function(Container $container)
		{
			$container->addText('name');
			$container->addText('price')
				->addRule(Form::NUMERIC, 'Cena dopravy musí být číslo.')
				->setRequired(true);
			$container->addText('weight')
				->addRule(Form::NUMERIC, 'Váha priority musí být číslo')
				->setRequired(true);
			$container->addText('freePriceLimit')
				->addRule(Form::NUMERIC, 'Limit pro dopravu musí být číslo.')
				->setRequired(false);
			$container->addText('trackUrl');
		};

		$this->getInlineEdit()->onSetDefaults[] = function(Container $container, Delivery $delivery)
		{
			$container->setDefaults([
				'name' => $delivery->getName(),
				'price' => $delivery->getPrice(),
				'freePriceLimit' => $delivery->getFreePriceLimit(),
				'trackUrl' => $delivery->getTrackUrl(),
				'weight' => $delivery->getWeight()
			]);
		};

		$this->getInlineEdit()->onSubmit[] = function($id, $values)
		{
			/** @var Delivery $delivery */
			$delivery = $this->deliveryFacade->getDelivery((int)$id);

			$delivery->setName($values->name);
			$delivery->setPrice(floatval($values->price));
			$delivery->setFreePriceLimit(floatval($values->freePriceLimit));
			$delivery->setTrackUrl(isset($values->trackUrl) && strlen($values->trackUrl) > 0 ? $values->trackUrl : null);
			$delivery->setWeight((int)$values->weight);

			$this->deliveryFacade->saveDelivery($delivery);
			if ($this->parent->isAjax()) {
				$this->reload();
			} else {
				$this->parent->redirect('this');
			}

		};
	}
}