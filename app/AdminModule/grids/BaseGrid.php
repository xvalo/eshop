<?php

namespace App\AdminModule\Grids;

use App\Model\BaseRepository;
use Ublaboo\DataGrid\DataGrid;

class BaseGrid extends DataGrid
{
	public function __construct($parent = null, $name = null, BaseRepository $repository = null)
	{
		parent::__construct($parent, $name);

		$this->setPrimaryKey('id');

		if($repository)
		{
			$this->setDataSource($repository->getGridData());
		}

		$this->setItemsPerPageList([15,100,200,500,1000]);
		$this->setDefaultPerPage(100);
		$this->setRememberState(false);
	}
}