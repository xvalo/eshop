<?php

namespace App\AdminModule\Grids;


use App\WatchDog\WatchDog;
use App\WatchDog\WatchDogRepository;
use Nette\Utils\Html;

class WatchDogGrid extends BaseGrid
{
    public function __construct($parent, $name, WatchDogRepository $repository)
    {
        parent::__construct($parent, $name, $repository);

        $this->addColumnText('priceVariant', 'Produkt')
                ->setSortable()
                ->setRenderer(function(WatchDog $item) use ($parent){
                    $variant = $item->getVariant();
                    return Html::el('a', ['target' => '_blank'])
                        ->href($parent->link('Product:edit', ['id' => $variant->getProduct()->getId()]))
                        ->setText($variant->getVariantName());
                });

//        $this->addColumnText('countOfNew', 'Nově sledováno')
//            ->setSortable();
//
//        $this->addColumnText('count', 'Celkový zájem')
//            ->setSortable();

        $this->setRememberState(false);
        $this->setStrictSessionFilterValues(FALSE);
    }
}