<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\Container;

interface IWatchDogGridFactory
{
    /**
     * @param Container $parent
     * @param string $name
     * @return WatchDogGrid
     */
    public function create($parent, $name);
}