<?php

namespace App\AdminModule\Grids;

use App\Lists\Stock;
use App\Product\Product;
use Nette\ComponentModel\Container;

interface IStockPriceVariantGridFactory
{
	/**
	 * @param Container $parent
	 * @param string $name
	 * @param Product $product
	 * @param Stock $stock
	 * @return StockPriceVariantGrid
	 */
	public function create($parent, $name, Product $product, Stock $stock);
}