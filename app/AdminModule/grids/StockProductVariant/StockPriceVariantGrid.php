<?php

namespace App\AdminModule\Grids;

use App\Lists\Stock;
use App\Lists\StockRepository;
use App\Stock\StockManager;
use App\Product\Variant;
use App\Product\VariantRepository;
use App\Product\Product;
use App\Product\ProductService;
use App\Stock\StockItem;
use Nette\Forms\Container;

class StockPriceVariantGrid extends BaseGrid
{
	public function __construct(
		$parent,
		$name,
		Product $product,
		Stock $stock,
		VariantRepository $repository,
		ProductService $service,
		StockRepository $stockRepository,
		StockManager $stockManager
	){
		parent::__construct($parent, $name);

		$this->setDataSource($repository->getProductVariants($product));

		$this->addColumnText('code', 'Kód')
			->setRenderer(function(Variant $item){
				return $item->getCodesString();
			})
			->setSortable();

		$this->addColumnText('ean', 'EAN')
			->setSortable();

		$this->addColumnText('name', 'Název')
			->setSortable();

		$this->setPagination(false);

		$self = $this;

		$this->addColumnText('stockCount', 'Stav')
			->setSortable()
			->setRenderer(function(Variant $item) use ($stock){
				return $item->getStockCount($stock) . ' ks';
			});

		$this->addColumnText('stockLimit', 'Limit')
			->setSortable()
			->setRenderer(function(Variant $item) use ($stock){
				return $item->getStockLimitToAlert($stock) . ' ks';
			});

		$this->addColumnText('stockPosition', 'Pozice')
			->setSortable()
			->setRenderer(function(Variant $item) use ($stock){
				return $item->getStockPosition($stock);
			});

		$this->addAction('setAlert', '', 'changeAlert!', ['priceVariant' => 'id'])
				->setIcon('bell')
				->setTitle(function(Variant $item) use ($stock) {
					return $item->getStockItem($stock)->isAlertEnabled() ? 'Zrušit upozornění na limit' : 'Nastavit upozornění na limit';
				})
				->setClass(function(Variant $item) use ($stock) {
					return $item->getStockItem($stock)->isAlertEnabled() ? 'btn btn-xs btn-success' : 'btn btn-xs btn-danger';
				})
				->addAttributes(['stock' => $stock->getId()]);

		$this->allowRowsAction('setAlert', function (Variant $item) use ($stock) {
			return $item->getStockItem($stock) instanceof StockItem;
		});

		$this->addInlineEdit()->onControlAdd[] = function(Container $container) use ($stock)
		{
			$container->addText('name')
				->setDisabled(true);
			$container->addText('ean');

			$container->addText('stockCount')
				->setType('number');
			$container->addText('stockLimit')
				->setType('number');
			$container->addText('stockPosition');

		};

		$this->getInlineEdit()->onSetDefaults[] = function(Container $container, Variant $item) use ($stock)
		{
			$defaults = [
				'ean' => $item->getEan(),
				'name' => $item->getName(),
				'stockCount' => $item->getStockCount($stock),
				'stockLimit' => $item->getStockLimitToAlert($stock),
				'stockPosition' => $item->getStockPosition($stock)
			];

			$container->setDefaults($defaults);
		};

		$this->getInlineEdit()->onSubmit[] = function($id, $values) use ($self, $repository, $stockManager, $stock)
		{
			/** @var Variant $variant */
			$variant = $repository->find($id);

			$stockItem = $variant->getStockItem($stock);
			$currentCount = ($stockItem) ? $stockItem->getCount() : 0;
			$value = (int)$values['stockCount'] - $currentCount;

			$stockManager->changeStockItemState($variant, $stock, $value, $values['stockPosition'], $values['stockLimit']);

			$variant->setEan($values['ean']);
			$repository->flush();

		};

		$this->getInlineEdit()->setShowNonEditingColumns();

	}
}