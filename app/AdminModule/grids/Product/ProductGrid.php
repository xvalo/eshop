<?php

namespace App\AdminModule\Grids;

use App\Category\Entity\Category;
use App\Lists\Producer;
use App\Product\Variant;
use App\Product\Product;
use App\Utils\Selects;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\QueryBuilder;
use Nette\Utils\Strings;

class ProductGrid extends BaseGrid
{
	const TYPE_COMMON = 'common';
	const TYPE_STOCK = 'stock';

	/** @var EntityManager*/
	private $em;

	public function __construct(
		$type,
		EntityManager $em
	){
		parent::__construct();

		$this->em = $em;
		$this->setDataSource(
			$this->em->getRepository(Product::class)->createQueryBuilder('P')
				->where('P.deleted = :deleted')
				->setParameter('deleted', false)
		);

		$this->addColumnNumber('code', 'Výrobní č.')
				->setSortable()
				->setRenderer(function(Product $item){
					$codes = [];

					/** @var Variant $priceVariant */
					foreach ($item->getPriceVariants() as $priceVariant) {
						$codes[] = $priceVariant->getCodesString();
					}
					return Strings::truncate(implode(', ', $codes), 20, '...');
				});

		$this->addFilterText('code', 'Výrobní č.')
				->setCondition(function (QueryBuilder $qb, $value) {
					$qb->leftJoin('P.variants', 'V')
						->join('V.codes', 'VC')
						->join('VC.code', 'C')
						->where($qb->expr()->like('C.code', ':code'));
					$qb->setParameter('code', '%'.$value.'%');
				});

		$this->addColumnLink('name', 'Produkt', 'edit')
			->setSortable()
			->setFilterText('name')
				->setSplitWordsSearch(FALSE);

		$this->addColumnText('category', 'Kategorie')
				->setSortable()
				->setRenderer(function(Product $product){
					return $product->getCategory()->getName();
				})
				->setFilterSelect($this->getCategoryList(), 'category');

		$this->addColumnText('producer', 'Výrobce')
				->setSortable()
				->setRenderer(function(Product $product){
					return $product->getProducer() ? $product->getProducer()->getName() : '';
				})
				->setFilterSelect($this->getProducerList(), 'producer');

		$this->addColumnText('active', 'Aktivní')
			->setSortable()
			->setReplacement(Selects::yesNo())
			->setFilterSelect(Selects::yesNo());

		$this->addAction('edit', '', 'edit', ['id'])
			->setIcon('pencil');

		if ($type == self::TYPE_COMMON) {
			$this->addColumnText('specialOffer', 'Akční nabídka')
				->setSortable()
				->setReplacement(Selects::yesNo())
				->setFilterSelect(Selects::yesNo());

			$this->addColumnText('mainPage', 'Hl. strana')
				->setSortable()
				->setReplacement(Selects::yesNo())
				->setFilterSelect(Selects::yesNo());

			$this->addColumnText('bestSeller', 'Nejprod.')
				->setSortable()
				->setReplacement(Selects::yesNo())
				->setFilterSelect(Selects::yesNo());

			$this->addColumnText('stockItem', 'Sklad')
				->setSortable()
				->setReplacement(Selects::yesNo())
				->setFilterSelect(Selects::yesNo());

			$this->addAction('delete', '', 'delete!')
				->setIcon('trash')
				->setTitle('Delete')
				->setClass('btn btn-xs btn-danger ajax')
				->setConfirm('Opravdu chcete smazat produkt %s?', 'name');
		}

        $this->setRememberState(false);
        $this->setStrictSessionFilterValues(FALSE);
	}

	private function getCategoryList()
	{
		return  ['' => ''] + $this->em->getRepository(Category::class)->findPairs(['deleted' => 0], 'name', ['name' => 'ASC'], 'id');
	}

	private function getProducerList()
	{
		return  ['' => ''] + $this->em->getRepository(Producer::class)->findPairs(['deleted' => 0], 'name', ['name' => 'ASC'], 'id');
	}
}