<?php

namespace App\AdminModule\Grids;

interface IProductGridFactory
{
	/**
	 * @param $type
	 * @return ProductGrid
	 */
	public function create($type);
}