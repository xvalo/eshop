<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\IContainer;

interface IPageGridFactory
{
	/**
	 * @param IContainer $parent
	 * @param string $name
	 * @return PageGrid
	 */
	public function create(IContainer $parent, $name);
}