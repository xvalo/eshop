<?php

namespace App\AdminModule\Grids;

use App\Page\PageFacade;
use App\Utils\Selects;
use Nette\ComponentModel\IContainer;

class PageGrid extends BaseGrid
{
	public function __construct(
		IContainer $parent,
		$name,
		PageFacade $pageFacade
	){
		parent::__construct($parent, $name);

		$this->setDataSource($pageFacade->getGridSource());

		$this->addColumnText('title', 'Název')
			->setSortable()
			->setFilterText('name');

		$this->addColumnText('url', 'URL')
			->setSortable()
			->setFilterText('url');

		$this->addColumnText('inMenu', 'V menu')
			->setSortable()
			->setReplacement(Selects::yesNo())
			->setFilterSelect(Selects::yesNo());

		$this->addColumnText('inFooter', 'V patičce')
			->setSortable()
			->setReplacement(Selects::yesNo())
			->setFilterSelect(Selects::yesNo());

		$this->addColumnText('active', 'Aktivní')
			->setSortable()
			->setReplacement(Selects::yesNo())
			->setFilterSelect(Selects::yesNo());


		$this->addAction('edit', '', 'edit', ['id'])
			->setIcon('pencil');
	}
}