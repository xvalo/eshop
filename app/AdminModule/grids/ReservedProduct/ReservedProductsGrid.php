<?php

namespace App\AdminModule\Grids;

use App\Classes\Product\Reserved\ReservedProductService;

class ReservedProductsGrid extends BaseGrid
{
	public function __construct($parent, $name, ReservedProductService $service)
	{
		parent::__construct($parent, $name);

		$this->setDataSource($service->getReservationsByProduct());

		$this->addColumnText('code', 'Kód')
				->setSortable()
				->setFilterText('code');

		$this->addColumnText('name', 'Produkt')
				->setSortable()
				->setFilterText('name');

		$this->addColumnText('count', 'Rezervováno')
			->setSortable()
			->setFilterText('ordersCount');

		$this->setItemsDetail(__DIR__ . '/detail.latte');

		$this->setRememberState(false);
		$this->setStrictSessionFilterValues(FALSE);
	}
}