<?php

namespace App\AdminModule\Grids;

use Nette\ComponentModel\Container;

interface IReservedProductsGridFactory
{
    /**
     * @param Container $parent
     * @param string $name
     * @return ReservedProductsGrid
     */
    public function create($parent, $name);
}