<?php

namespace App\Admin\Forms;

use App\Category\CategoryService;
use App\Lists\ProducerRepository;
use App\Product\Waiting\Create\Price;
use App\Product\Waiting\Create\Variant;
use App\Product\Waiting\Create\Product;
use App\Product\Waiting\Entity\Product as WaitingProduct;
use App\Product\Waiting\WaitingProductService;
use App\Utils\Vat;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;

class WaitingProductForm
{
	/**
	 * @var ProducerRepository
	 */
	private $producerRepository;

	/**
	 * @var CategoryService
	 */
	private $categoryService;

	/**
	 * @var WaitingProductService
	 */
	private $waitingProductService;

	public function __construct(
		ProducerRepository $producerRepository,
		CategoryService $categoryService,
		WaitingProductService $waitingProductService
	){
		$this->producerRepository = $producerRepository;
		$this->categoryService = $categoryService;
		$this->waitingProductService = $waitingProductService;
	}

	public function create(callable $onSuccess, WaitingProduct $waitingProduct)
	{
		$form = new Form();

		$producerList = array_map(function($value) {
			return Strings::trim($value);
		}, $this->producerRepository->fetchPairs('id', 'name', ['deleted' => 0], ['name' => 'ASC']));

		$product = $form->addContainer('product');
		$variant = $form->addContainer('variant');
		$price = $form->addContainer('price');
		$images = $form->addContainer('images');

		$product->addText('name', 'Název')
			->setRequired('Zadejte název produktu');

		$product->addSelect('producer', 'Výrobce', $producerList)
			->setPrompt(' ---- Vyberte výrobce ---- ');

		$product->addTextArea('annotation', 'Anotace produktu', 60, 4)
			->setMaxLength(200);

		$product->addTextArea('description', 'Popis', 60, 7)
			->setRequired('Zadejte popis produktu');

		$product->addSelect('category', 'Kategorie', $this->categoryService->getCategoriesMenuList())
			->setAttribute('class', 'form-control')
			->setPrompt(' ---- Vyberte kategorii -----')
			->setRequired('Vyberte kategorii');

		$product->addSelect('vat', 'Sazba DPH', Vat::$labels)
			->setRequired('Vybete sazbu DPH')
			->setDefaultValue(Vat::TYPE_21);

		$variant->addText('name');
		$variant->addText('ean');
		$variant->addText('normalPrice');

		$price->addText('name');
		$price->addText('price')
			->setRequired('Zadejte cenu')
			->addRule(Form::FILLED, 'Zadejte prosím cenu.')
			->addRule(Form::FLOAT, 'Zadávejte prosím pouze číselnou hodnotu.')
			->addRule(Form::MIN, 'Zadejte prosím cenu větší než 0.', 0)
			->addRule(Form::NOT_EQUAL, 'Cena nesmí být nulová.', 0);
		$price->addText('purchasePrice');

		$images->addCheckboxList('urls', null, $waitingProduct->getData()->getImages());

		$form->addSubmit('save');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess, $waitingProduct) {

			$images = array_map(function($key) use ($waitingProduct) {
				return $waitingProduct->getData()->getImages()[$key];
			},  $values->images->urls);

			$productData = new Product(
				$values->product->name,
				$values->product->annotation,
				$values->product->description,
				$values->product->category,
				(int)$values->product->vat,
				$waitingProduct->getSupplier(),
				$images,
				$values->product->producer
			);

			$variantData = new Variant(
				$values->variant->name,
				$values->variant->ean,
				(float)$values->variant->normalPrice
			);

			$priceData = new Price(
				(float)$values->price->price,
				$values->price->name,
				(float)$values->price->purchasePrice
			);

			$product = $this->waitingProductService->createNewProduct($productData, $variantData, $priceData, $waitingProduct->getCode());

			$this->waitingProductService->setProductProcessed($waitingProduct);

			$onSuccess( $product );
		};

		$productDefaultData = $waitingProduct->getData();
		$producer = false;

		if ($productDefaultData->getProducer() !== null) {
			$producer = array_search($productDefaultData->getProducer(), $producerList);
		}

		$form->setDefaults([
			'product' => [
				'name' => $productDefaultData->getName(),
				'description' => nl2br($productDefaultData->getDescription()),
				'producer' => $producer !== false && is_numeric($producer) ? $producer : null
			],
			'variant' => [
				'ean' => $productDefaultData->getEan()
			],
			'price' => [
				'price' => $productDefaultData->getPrice(),
				'purchasePrice' => $productDefaultData->getPurchasePrice()
			]
		]);

		return $form;
	}
}