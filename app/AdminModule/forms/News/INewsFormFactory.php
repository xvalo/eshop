<?php

namespace App\AdminModule\Forms;


interface INewsFormFactory
{
	/**
	 * @param $parent
	 * @param $name
	 * @return NewsForm
	 */
	public function create($parent, $name);
}