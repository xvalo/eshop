<?php

namespace App\AdminModule\Forms;


class NewsForm extends BaseForm
{
	public function __construct($parent, $name)
	{
		parent::__construct($parent, $name);

		$this->addText('title', 'Název');

		$this->addTextArea('perex', 'Úvodní text');

		$this->addTextArea('text', 'Text článku');

		$this->addText('displayedFromTo', 'Zobrazit od / do');

		$this->addUpload('mainImage', 'Úvodní obrázek');

		$this->addCheckbox('active', 'Aktivní');

		$this->addSubmit('save', 'Uložit');
	}
}