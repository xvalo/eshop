<?php

namespace App\AdminModule\Forms;

use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;

class BaseForm extends Form
{
	public function __construct(IContainer $parent = null, $name = null)
	{
		parent::__construct($parent, $name);

		$renderer = $this->getRenderer();

		$this->getElementPrototype()->class('form-horizontal'); //ajax

		$renderer->wrappers['controls']['container'] = 'div class="form-body"';
		$renderer->wrappers['pair']['container'] = 'div class=form-group';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['control']['.required'] = '';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.submit'] = 'btn btn-success';
		$renderer->wrappers['control']['container'] = 'div class=col-md-9';
		$renderer->wrappers['label']['container'] = 'div class="col-md-3 control-label"';
		$renderer->wrappers['label']['requiredsuffix'] = '<span class="required"> * </span>';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
	}

	protected function beforeRender()
	{
		parent::beforeRender();

		foreach ($this->getControls() as $control)
		{
			$type = $control->getOption('type');
			if ($type === 'button')
			{
				$control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
				$usedPrimary = TRUE;
			}
			elseif (in_array($type, ['text', 'textarea', 'select'], TRUE))
			{
				$control->getControlPrototype()->addClass('form-control');
			}
			elseif (in_array($type, ['checkbox', 'radio'], TRUE))
			{
				$control->getSeparatorPrototype()->setName('div')->addClass($type);
			}
		}

	}

}