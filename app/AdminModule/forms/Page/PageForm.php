<?php

namespace App\AdminModule\Forms;


use Nette\Forms\Form;

class PageForm extends BaseForm
{
	public function __construct($parent, $name)
	{
		parent::__construct($parent, $name);

		$this->addText('title', 'Název')
			->addRule(Form::MAX_LENGTH, 'Název stránky může mít maximálně 100 znaků', 100)
			->setRequired('Zadejte název stránky');
		$this->addText('url', 'URL')
			->addRule(Form::MAX_LENGTH, 'URL může mít maximálně 100 znaků', 100)
			->setRequired('Zadejte URL stránky');
		$this->addTextArea('content', 'Obsah stránky')
			->setRequired(false);

		$this->addText('seoTitle', 'SEO název')
			->addRule(Form::MAX_LENGTH, 'SEO název může mít maximálně 100 znaků', 100)
			->setRequired(false);
		$this->addText('seoKeywords', 'SEO klíčová slova')
			->addRule(Form::MAX_LENGTH, 'SEO klíčová slova můžou mít maximálně 100 znaků', 100)
			->setRequired(false);

		$this->addCheckbox('active', 'Aktivní');
		$this->addCheckbox('inMenu', 'V menu');
		$this->addCheckbox('inFooter', 'V patičce');
		$this->addSubmit('save', 'Uložit');
	}
}