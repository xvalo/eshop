<?php

namespace App\AdminModule\Forms;


interface IPageFormFactory
{
	/**
	 * @param $parent
	 * @param $name
	 * @return PageForm
	 */
	public function create($parent, $name);
}