<?php

namespace App\Admin\Forms;

use Nette\Application\UI\Form;
use Nette\SmartObject;

class AdminSearchFactory
{
	use SmartObject;

    public function create()
    {
        $form = new Form();

        $form->addText('search')
                ->setRequired(true)
                ->addRule(Form::FILLED, 'Zadejte frázi k vyhledání.')
                ->addRule(Form::MIN_LENGTH, 'Zadejte alespoň 3 znaky k vyhledání', 3);
        $form->addSubmit('send');

        return $form;
    }
}