<?php

namespace App\AdminModule\Forms;

interface IDeliveryToCategoryFormFactory
{
	/**
	 * @return DeliveryToCategoryForm
	 */
	public function create();
}