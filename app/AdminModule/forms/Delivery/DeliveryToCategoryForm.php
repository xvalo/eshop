<?php


namespace App\AdminModule\Forms;

use App\Order\Delivery\DeliveryService;

class DeliveryToCategoryForm extends BaseForm
{
	public function __construct(
		DeliveryService $deliveryService
	){
		parent::__construct();

		$this->addCheckboxList('delivery', null, $deliveryService->getDeliveriesList());

		$this->addSubmit('save');
	}
}