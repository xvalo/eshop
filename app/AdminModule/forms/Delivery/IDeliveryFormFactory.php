<?php

namespace App\AdminModule\Forms;

use Nette\ComponentModel\IComponent;

interface IDeliveryFormFactory
{
	/**
	 * @param IComponent $parent
	 * @param $name
	 * @return DeliveryForm
	 */
	public function create(IComponent $parent, $name);
}