<?php

namespace App\AdminModule\Forms;

use App\Order\Payment\Entity\PaymentType;
use App\Order\Payment\PaymentTypeFacade;
use Nette\ComponentModel\IComponent;

class DeliveryForm extends BaseForm
{
	public function __construct(
		IComponent $parent,
		$name,
		PaymentTypeFacade $paymentTypeFacade
	){
		parent::__construct($parent, $name);

		$paymentTypes = $this->addContainer('paymentTypes');

		/** @var PaymentType $type */
		foreach ($paymentTypeFacade->getPaymentTypesList() as $id => $name) {
			$typeContainer = $paymentTypes->addContainer($id);
			$typeContainer->addCheckbox('isAvailable', $name);
			$typeContainer->addText('price');
		}

		$this->addSubmit('save', 'Uložit');
	}
}