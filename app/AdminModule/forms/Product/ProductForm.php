<?php

namespace App\AdminModule\Forms;

use App\Lists\ProducerRepository;
use App\Utils\Vat;

class ProductForm extends BaseForm
{
	/**
	 * ProductForm constructor.
	 * @param \Nette\ComponentModel\IContainer $parent
	 * @param null|string $name
	 * @param array $categories
	 * @param boolean $isNewItem
	 * @param array $pictureIds
	 * @param ProducerRepository $producerRepository
	 */
	public function __construct($parent, $name, array $categories, $isNewItem, array $pictureIds = [], ProducerRepository $producerRepository)
	{
		parent::__construct($parent, $name);

		$this->addText('name', 'Název')
				->setRequired('Zadejte název');

		$this->addSelect('producer', 'Výrobce', $producerRepository->fetchPairs('id', 'name', ['deleted' => 0], ['name' => 'ASC']))
				->setPrompt(' ---- Vyberte výrobce ---- ');

		$this->addTextArea('annotation', 'Anotace produktu', 60, 4)
			->setMaxLength(200);

		$this->addTextArea('description', 'Popis', 60, 7)
				->setRequired('Zadejte popis produktu');

		$this->addSelect('category', 'Kategorie', $categories)
				->setAttribute('class', 'form-control selectpicker')
				->setAttribute('data-live-search', 'true')
				->setPrompt(' ---- Vyberte kategorii -----')
				->setRequired('Vyberte kategorii');

		$this->addSelect('vat', 'Sazba DPH', Vat::$labels)
				->setRequired('Vybete sazbu DPH');

		if(!$isNewItem) {
			$this->addCheckbox('active', 'Aktivní');
			$this->addCheckbox('specialOffer', 'Akční nabídka');
			$this->addCheckbox('news', 'Novinka');
			$this->addCheckbox('bestSeller', 'Nejprodávanější');
			$this->addCheckbox('mainPage', 'Hlavní strana');
			$this->addCheckbox('stockItem', 'Sklad');

			$this->addRadioList('mainImage', null, $pictureIds);
		}

		$this->getElementPrototype()->onsubmit('tinyMCE.triggerSave()');

		$this->addSubmit('submit', 'Uložit');

	}
}