<?php

namespace App\AdminModule\Forms;

interface IProductFormFactory
{
	/**
	 * @param $parent
	 * @param $name
	 * @param array $categories
	 * @param boolean $isNewItem
	 * @param array $pictureIds
	 * @return ProductForm
	 */
	public function create($parent, $name, array $categories, $isNewItem, array $pictureIds);
}