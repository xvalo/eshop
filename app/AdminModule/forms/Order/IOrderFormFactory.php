<?php

namespace App\AdminModule\Forms;

interface IOrderFormFactory
{
    /**
     * @return OrderForm
     */
    public function create();
}