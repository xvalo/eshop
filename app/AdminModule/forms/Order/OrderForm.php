<?php

namespace App\AdminModule\Forms;

use App\Lists\CountryList;
use App\Lists\DeliveryRepository;
use App\Utils\Vat;
use App\Order\OrderStatus;
use App\Order\Payment\PaymentTypeFacade;
use Nette\Application\UI\Form;

class OrderForm extends BaseForm
{
    public function __construct(
	    DeliveryRepository $deliveryRepository,
		PaymentTypeFacade $paymentTypeFacade
    ){
        parent::__construct();

        $contactInfo = $this->addContainer('contactInformation');
            $contactInfo->addText('name');
            $contactInfo->addText('lastName');
            $contactInfo->addText('email');
            $contactInfo->addText('phone');
            $contactInfo->addText('lyonessCardId');
		    $contactInfo->addText('lyonessCardEan');

        $deliveryAddress = $this->addContainer('deliveryAddress');
            $deliveryAddress->addText('name');
            $deliveryAddress->addText('lastName');
            $deliveryAddress->addText('street');
            $deliveryAddress->addText('company');
            $deliveryAddress->addText('city');
            $deliveryAddress->addText('zip');
            $deliveryAddress->addSelect('country', null, CountryList::$statesLabels);

        $invoiceInfo = $this->addContainer('invoiceInformation');
            $invoiceInfo->addText('ico');
            $invoiceInfo->addText('dic');
            $invoiceInfo->addText('company');
            $invoiceInfo->addText('street');
            $invoiceInfo->addText('city');
            $invoiceInfo->addText('zip');
            $invoiceInfo->addSelect('country', null, CountryList::$statesLabels);


        $this->addHidden('customer');
        $this->addSelect('status', null, OrderStatus::$statesLabels);
	    $this->addText('packageNo');
	    $this->addContainer('variants');

        $deliveries = [ null => 'Zakoupeno na prodejně' ] + $deliveryRepository->fetchPairs('id', 'name', ['deleted' => 0]);
        $this->addSelect('delivery', null, $deliveries);

	    $this->addDependentSelectBox('paymentType', null, $this['delivery'])
		    ->setDependentCallback(function ($values) use ($deliveryRepository) {
			    $data = new \NasExt\Forms\DependentData;

			    $delivery = $values['delivery'] !== "" ? $deliveryRepository->find((int)$values['delivery']) : null;
			    $paymentTypes = $delivery ? array_column($delivery->getPaymentTypesList(), 'name', 'id') : [];
			    $data->setItems($paymentTypes);

			    return $data;
		    })
		    ->setDisabledWhenEmpty(true);

	    $extraItem = $this->addContainer('extraItem');
	    $extraItem->addSubmit('addItem');
	    $extraItem->addText('description')
		    ->addConditionOn($extraItem['addItem'], Form::SUBMITTED)
	            ->setRequired('Zadejte popis položky');
	    $extraItem->addText('unitPrice')
		    ->addConditionOn($extraItem['addItem'], Form::SUBMITTED)
	            ->setRequired('Zadejte cenu položky');
	    $extraItem->addText('count')
		    ->setType('number')
		    ->addConditionOn($extraItem['addItem'], Form::SUBMITTED)
	            ->setRequired('Zadejte počet kusů');
	    $extraItem->addSelect('vat', null, Vat::$labels)
		    ->setDefaultValue(Vat::TYPE_21);

        $this->addSubmit('save');
    }
}