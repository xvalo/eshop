<?php

namespace App\AdminModule\Forms;

use Nette\Application\UI\Form;

class EventForm extends BaseForm
{
    public function __construct($parent, $name)
    {
        parent::__construct($parent, $name);

        $this->addText('title', 'Název akce')
            ->setRequired('Zadejte název akce');

        $this->addText('place', 'Místo akce')
            ->setRequired('Zadejte místo akce');

        $this->addText('eventDate', 'Datum akce')
            ->setRequired('Zadejte datum akce')
            ->addRule(Form::FILLED, 'Datum akce musí být zadáno')
            ->addRule(Form::PATTERN, 'Zadejte datum ve tvaru dd. mm. RRRR', '[0-9]{1,2})\. ([0-9]{1,2})\. ([0-9]{4}');

        $this->addTextArea('description', 'Popis')
            ->setRequired('Vložte popis akce');

        $this->addText('timeFrom', 'Čas od')
            ->setRequired('Zadejte čas, kdy bude akce začínat');

        $this->addText('timeTo', 'Čas do');

        $this->addCheckbox('active', 'Aktivní');

        $this->addSubmit('save', 'Uložit');
    }
}