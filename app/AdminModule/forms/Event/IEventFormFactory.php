<?php

namespace App\AdminModule\Forms;

interface IEventFormFactory
{
    /**
     * @param $parent
     * @param $name
     * @return EventForm
     */
    public function create($parent, $name);
}