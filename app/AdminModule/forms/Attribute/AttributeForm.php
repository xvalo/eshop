<?php

namespace App\AdminModule\Forms;

use App\Attribute\AttributeFacade;
use App\Attribute\AttributeService;
use App\Attribute\Core\Attribute;
use Nette\Application\UI\Form;

class AttributeForm
{
	/** @var AttributeFacade */
	private $attributeFacade;

	/** @var AttributeService */
	private $attributeService;

	public function __construct(
		AttributeFacade $attributeFacade,
		AttributeService $attributeService
	){
		$this->attributeFacade = $attributeFacade;
		$this->attributeService = $attributeService;
	}

	public function create(callable $onSuccess, Attribute $attribute = null)
	{
		$form = new Form();

		$form->addText('name')
			->setRequired('Zadejte název vlastnosti');

		$form->addSelect('parent', null, $this->attributeFacade->getAvailableParentAttributesList())
			->setPrompt('––– Bez nadřazené vlastnosti ---');

		$form->addText('title')
			->setRequired('Zadejte název vlastnosti, který se zobrazí zákazníkovi na webu.');

		$form->addText('description');

		$form->addSubmit('save');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess, $attribute) {

			if ($attribute) {
				$parent = (isset($values->parent) && (int)$values->parent) ? $this->attributeService->getAttribute((int)$values->parent) : null;

				$attribute->setName($values->name);
				$attribute->setTitle($values->title);
				$attribute->setDescription($values->description);
				$attribute->setParent($parent);

				$this->attributeFacade->saveAttribute($attribute);
			} else {
				$attribute = $this->attributeService->createAttribute(
					$values->name,
					$values->title,
					$values->description,
					(isset($values->parent) && (int)$values->parent) ? $this->attributeService->getAttribute((int)$values->parent) : null
				);
			}
			$onSuccess( $attribute );
		};

		if ($attribute) {
			$form->setDefaults([
				'name' => $attribute->getName(),
				'title' => $attribute->getTitle(),
				'description' => $attribute->getDescription(),
				'parent' => $attribute->getParent() ? $attribute->getParent()->getId() : null
			]);
		}

		return $form;
	}
}