<?php

namespace App\AdminModule\Forms;

use App\Lists\StockRepository;
use Nette\ComponentModel\IContainer;
use Nette\Forms\Container;

class StockOperationForm extends BaseForm
{
    public function __construct(
        IContainer $parent,
        $name,
        StockRepository $stockRepository
    ) {
        parent::__construct($parent, $name);

        $this->addSelect('stock', null, $stockRepository->fetchPairs('id', 'name', [], ['name' => 'ASC']))
                ->setRequired('Vyberte sklad');

        $this->addText('date');

	    $this->addHidden('variantsToAdd')
		    ->setAttribute('class', 'onchange-submit');

	    $this->addSubmit('save');
    }
}