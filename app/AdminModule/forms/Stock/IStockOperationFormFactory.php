<?php

namespace App\AdminModule\Forms;

use Nette\ComponentModel\IContainer;

interface IStockOperationFormFactory
{
    /**
     * @param IContainer $parent
     * @param string $name
     * @return StockOperationForm
     */
    public function create(IContainer $parent, $name);
}