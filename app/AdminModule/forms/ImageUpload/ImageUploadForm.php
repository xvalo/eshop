<?php

namespace App\AdminModule\Forms;

use Nette\Application\UI\Form;

class ImageUploadForm extends BaseForm
{
	public function __construct($parent, $name)
	{
		parent::__construct($parent, $name);

		$this->addUpload('images', 'Obrázky', true)
				->addRule(Form::IMAGE, 'Nahrávané soubory smí být pouze obrázky.')
				->setRequired();

		$this->addSubmit('save', 'Nahrát obrázky');
	}
}