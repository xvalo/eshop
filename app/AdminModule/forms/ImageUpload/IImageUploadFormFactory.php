<?php

namespace App\AdminModule\Forms;


interface IImageUploadFormFactory
{
	/**
	 * @param $parent
	 * @param $name
	 * @return ImageUploadForm
	 */
	public function create($parent, $name);
}