<?php

namespace App\Admin\Forms;

use App\Classes\Stock\Supplier\MissingSupplierSourceException;
use App\Stock\Supplier\Entity\Supplier;
use App\Stock\Supplier\SupplierService;
use Nette\Application\UI\Form;

class SupplierForm
{
	/** @var SupplierService */
	private $supplierService;

	public function __construct(
		SupplierService $supplierService
	){
		$this->supplierService = $supplierService;
	}

	public function create(callable $onSuccess, callable $onError, Supplier $supplier = null)
	{
		$form = new Form();

		$form->addText('name')
			->setRequired('Zadejte název dodavatele');

		$form->addText('prefix');

		$form->addTextArea('note');

		$form->addCheckbox('isSynchronized');

		$form->addText('source')
			->addConditionOn($form['isSynchronized'], Form::EQUAL, true)
				->addRule(Form::FILLED, 'Pokud je vybrána synchornizace, musí být vyplněn i zdroj dat odkud bude probíhat synchronizace.');

		$form->addCheckbox('isHeurekaSource');
		$form->addCheckbox('isActive');

		$form->addSubmit('save');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess, $onError, $supplier) {

			try {
				if ($supplier) {
					$supplier = $this->supplierService->updateSupplier(
						$supplier,
						$values->name,
						$values->prefix,
						$values->note,
						$values->isSynchronized,
						$values->source,
						$values->isHeurekaSource,
						$values->isActive
					);
				} else {
					$supplier = $this->supplierService->createSupplier(
						$values->name,
						$values->prefix,
						$values->note,
						$values->isSynchronized,
						$values->source,
						$values->isHeurekaSource,
						$values->isActive
					);
				}

				$onSuccess($supplier);
			} catch (MissingSupplierSourceException $exception) {
				$onError('U dodavatele je nastavena aktivní synchronizace, ale nebyl zadán zdroj data. Zadejte prosím zdroj.');
			}
		};

		if ($supplier !== null) {
			$form->setDefaults([
				'name' => $supplier->getName(),
				'prefix' => $supplier->getPrefix(),
				'note' => $supplier->getNote(),
				'source' => $supplier->getSource(),
				'isSynchronized' => $supplier->isSynchronized(),
				'isHeurekaSource' => $supplier->isHeurekaSource(),
				'isActive' => $supplier->isActive()
			]);
		}

		return $form;
	}
}