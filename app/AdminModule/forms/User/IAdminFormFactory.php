<?php
/**
 * Created by PhpStorm.
 * User: Boris
 * Date: 30.09.2016
 * Time: 22:56
 */

namespace App\AdminModule\Forms;


use Nette\ComponentModel\IContainer;

interface IAdminFormFactory
{
	/**
	 * @param IContainer $parent
	 * @param string $name
	 * @return AdminForm
	 */
	public function create($parent, $name);
}