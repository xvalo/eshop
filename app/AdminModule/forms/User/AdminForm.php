<?php

namespace App\AdminModule\Forms;

use App\Admin\PrivilegeRepository;
use Nette\Application\UI\Form;

class AdminForm extends BaseForm
{
	public function __construct($parent, $name, PrivilegeRepository $privilegeRepository)
	{
		parent::__construct($parent, $name);

		$this->addHidden('id');

		$this->addText('login', 'Login')
				->setRequired('Musíte zadat přihlašovací jméno');

		$this->addPassword('password', 'Heslo')
				->addConditionOn($this['id'], Form::BLANK)
					->setRequired('Heslo musí být vyplněno.')
					->addRule(Form::MIN_LENGTH, 'Heslo musí mít minimálně %s znaků', 8);

		$this->addPassword('password_check', 'Heslo pro kontrolu')
				->setOmitted(true)
				->addConditionOn($this['password'], Form::FILLED)
					->addRule(Form::FILLED, 'Zadejte prosím heslo znovu pro ověření.')
					->addRule(Form::EQUAL, 'Zřejmě došlo k překlepu, zkuste prosím hesla zadat znovu.', $this['password']);


		$this->addText('name', 'Jméno')
				->setRequired('Musíte zadat jméno uživatele');

		$this->addText('lastName', 'Příjmení')
				->setRequired('Musíte zadat příjmení uživatele');

		$this->addCheckbox('superAdmin', 'Super administrátor');

		$this->addCheckboxList('privileges', 'Oprávnění', $privilegeRepository->fetchPairs('id', 'name'));

		$this->addCheckbox('active', 'Aktivní');

		$this->addSubmit('submit', 'Uložit');
	}
}