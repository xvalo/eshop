<?php

namespace App\Admin\Forms;

use App\Product\Availability\AvailabilityService;
use App\Product\Availability\ColorProvider;
use App\Product\Availability\Entity\Availability;
use Nette\Application\UI\Form;
use Nette\Utils\Html;

class ProductAvailabilityForm
{
	/** @var AvailabilityService */
	private $availabilityService;

	/** @var ColorProvider */
	private $colorProvider;

	public function __construct(
		AvailabilityService $availabilityService,
		ColorProvider $colorProvider
	){
		$this->availabilityService = $availabilityService;
		$this->colorProvider = $colorProvider;
	}

	public function create(callable $onSuccess, Availability $availability = null)
	{
		$form = new Form();

		$form->addText('title');
		$form->addText('description');

		$form->addSelect('color', null, $this->createColorOptions());

		$form->addText('heurekaAvailability')
			->setType('number');
		$form->addText('zboziAvailability')
			->setType('number');

		$form->addCheckbox('isOrderable');

		$form->addSubmit('save');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess, $availability) {
			if ($availability) {
				$this->availabilityService->updateAvailability(
					$availability,
					$values->title,
					$values->isOrderable,
					$values->description,
					$values->color,
					$values->heurekaAvailability,
					$values->zboziAvailability
				);
			} else {
				$this->availabilityService->createAvailability(
					$values->title,
					$values->isOrderable,
					$values->description,
					$values->color,
					$values->heurekaAvailability,
					$values->zboziAvailability
				);
			}
			$onSuccess();
		};

		if ($availability) {
			$form->setDefaults([
				'title' => $availability->getTitle(),
				'description' => $availability->getDescription(),
				'color' => $availability->getColor(),
				'heurekaAvailability' => $availability->getHeurekaAvailability(),
				'zboziAvailability' => $availability->getZboziAvailability(),
				'isOrderable' => $availability->isProductOrderable()
			]);
		}

		return $form;
	}

	private function createColorOptions()
	{
		$options = [];

		foreach ($this->colorProvider->getColorsList() as $key => $data) {
			$option = Html::el()
				->setText($data['label'])
				->setAttribute('data-color', $data['color']);

			$options[$key] = $option;
		}

		return $options;
	}
}