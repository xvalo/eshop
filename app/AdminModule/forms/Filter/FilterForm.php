<?php

namespace App\AdminModule\Forms;

use App\Filter\Entity\Filter;
use App\Filter\FilterFacade;
use Nette\Application\UI\Form;

class FilterForm
{
	/** @var FilterFacade */
	private $filterFacade;

	public function __construct(
		FilterFacade $filterFacade
	){
		$this->filterFacade = $filterFacade;
	}

	public function create(callable $onSuccess, Filter $filter = null)
	{
		$form = new Form();

		$form->addText('name')
			->setRequired('Zadejte název vlastnosti');

		$form->addText('title')
			->setRequired('Zadejte název vlastnosti, který se zobrazí zákazníkovi na webu.');

		$form->addText('description');
		$form->addCheckbox('active');

		$form->addSubmit('save');

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess, $filter) {

			if ($filter) {
				$filter->setName($values->name);
				$filter->setTitle($values->title);
				$filter->setDescription($values->description);

				if ($values->active) {
					$filter->activate();
				} else {
					$filter->deactivate();
				}
			} else {
				$filter = new Filter($values->name, $values->title, $values->description);

				if ($values->active) {
					$filter->activate();
				} else {
					$filter->deactivate();
				}
			}

			$this->filterFacade->saveFilter($filter);
			$onSuccess( $filter );
		};

		if ($filter) {
			$form->setDefaults([
				'name' => $filter->getName(),
				'title' => $filter->getTitle(),
				'description' => $filter->getDescription(),
				'active' => $filter->isActive()
			]);
		}

		return $form;
	}
}