<?php

namespace App\AdminModule\Forms;

interface ICategoryFilterForm
{
	/**
	 * @return CategoryFilterForm
	 */
	public function create();
}