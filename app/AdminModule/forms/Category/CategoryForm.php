<?php

namespace App\AdminModule\Forms;

use App\Category\CategoryService;
use App\Category\Entity\Category;
use App\Core\Forms\FormFactory;
use App\Category\Heureka\CategoryService as HeurekaCategoryService;
use App\Category\Zbozi\CategoryService as ZboziCategoryService;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Nette\Application\UI\Form;
use Nette\Utils\Html;

class CategoryForm
{
	/** @var FormFactory */
	private $formFactory;

	/** @var CategoryService */
	private $categoryService;

	/** @var HeurekaCategoryService */
	private $heurekaCategoryService;

	/** @var ZboziCategoryService */
	private $zboziCategoryService;

	public function __construct(
		FormFactory $formFactory,
		CategoryService $categoryService,
		HeurekaCategoryService $heurekaCategoryService,
		ZboziCategoryService $zboziCategoryService
	){
		$this->formFactory = $formFactory;
		$this->categoryService = $categoryService;
		$this->heurekaCategoryService = $heurekaCategoryService;
		$this->zboziCategoryService = $zboziCategoryService;
	}

	public function create(callable $onSuccess, Category $category = null)
	{
		$form = $this->formFactory->create();

		$form->addText('name', 'Název')
			->setRequired(true);

		$form->addText('postscript', 'Dovětek');

		$form->addTextArea('description', 'Popis')
			->setAttribute('class', 'mceEditor');

		$form->addSelect('parent', 'Nadřazená kategorie', $this->createParentSelectList($this->categoryService->getCategoryTreeList()))
			->setPrompt('--- Bez nadřazené kategorie ---');

		$form->addSelect('heurekaCategory', null, $this->createHeurekaSelectList($this->heurekaCategoryService->getCategoryList()))
			->setPrompt('--- Vyberte kategorii z Heuréky ---');

		$form->addSelect('zboziCategory', null, $this->zboziCategoryService->getCategoryList())
			->setPrompt('--- Vyberte kategorii ze Zboží ---');

		$form->addText('seoTitle', 'SEO title', null, 60);
		$form->addText('seoDescription', 'SEO description', null, 150);

		$form->addCheckbox('active', 'Aktivní');

		$form->getElementPrototype()->onsubmit('tinyMCE.triggerSave()');

		$form->addSubmit('submit', 'Uložit');

		if ($category !== null) {
			$form->setDefaults(
				$this->categoryService->getFormDefaults($category)
			);
		}

		$form->onSuccess[] = function (Form $form, $values) use ($onSuccess, $category) {
			try {
				if ($category !== null) {
					$category = $this->categoryService->updateCategory($category, $values);
				} else {
					$category = $this->categoryService->createCategory($form->getValues());
				}
			} catch (UniqueConstraintViolationException $e) {
				$form->addError('Kategorie s tímto názvem a přiřaznou nadřazenou kategorií už existuje a nejde znovu vytvořit. Vznikly by tak duplicitní URL adresy. Použijte prosím jiný název.');
				return;
			} catch (\Exception $e) {
				$form->addError('Kategorii se nepodařilo uložit.');
				return;
			}
			$onSuccess($category);
		};

		return $form;
	}

	private function createParentSelectList(array $categoryTree, int $level = 0)
	{
		$options = [];

		foreach ($categoryTree as $item) {
			$option = Html::el()
				->setText($item['name']);

			if ($level) {
				$option->setAttribute('class', 'l'.$level);
			}

			$options[$item['id']] = $option;

			if (isset($item['children'])) {
				$options += $this->createParentSelectList($item['children'], $level + 1);
			}
		}

		return $options;
	}

	private function createHeurekaSelectList(array $categoryTree, int $level = 0)
	{
		$options = [];

		foreach ($categoryTree as $item) {
			$option = Html::el()
				->setText($item['disabled'] ? $item['name'] : $item['fullName'])
				->setAttribute('disabled', $item['disabled']);

			if ($level) {
				$option->setAttribute('class', 'l'.$level);
			}

			$options[$item['id']] = $option;

			if (isset($item['children'])) {
				$options += $this->createHeurekaSelectList($item['children'], $level + 1);
			}
		}

		return $options;
	}
}