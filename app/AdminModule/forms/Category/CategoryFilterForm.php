<?php


namespace App\AdminModule\Forms;


use App\Filter\FilterService;

class CategoryFilterForm extends BaseForm
{
	public function __construct(
		FilterService $filterService
	){
		parent::__construct();

		$this->addCheckboxList('filters', null, $filterService->getFiltersList());

		$this->addSubmit('save');
	}
}