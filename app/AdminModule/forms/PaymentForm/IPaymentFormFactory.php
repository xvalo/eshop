<?php

namespace App\AdminModule\Forms;


interface IPaymentFormFactory
{
	/**
	 * @param $parent
	 * @param $name
	 * @return PaymentForm
	 */
	public function create($parent, $name);
}