<?php

namespace App\AdminModule\Forms;

use Nette\Application\UI\Form;

class PaymentForm  extends Form
{
	public function __construct($parent, $name)
	{
		parent::__construct($parent, $name);

		$this->addText('amount')
				->setRequired('Zadejte obdrženou částku');

		$this->addSubmit('paidByCard')
				->setValidationScope(false);

		$this->addSubmit('paidCash');
	}
}