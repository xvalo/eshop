<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\IAttributeAttachControlFactory;
use App\AdminModule\Components\IProductAttributeDetailControlFactory;
use App\AdminModule\Components\IVariantDetailControlFactory;
use App\AdminModule\Components\ProductFiltersSetting\ISettingsControlFactory;
use App\AdminModule\Components\RelatedProducts\IRelatedProductsControlFactory;
use App\AdminModule\Forms\IImageUploadFormFactory;
use App\AdminModule\Forms\IProductFormFactory;
use App\AdminModule\Grids\IProductAttributeGridFactory;
use App\AdminModule\Grids\IProductGridFactory;
use App\AdminModule\Grids\IProductVariantGridFactory;
use App\AdminModule\Grids\IReservedProductsGridFactory;
use App\AdminModule\Grids\IWaitingProductsGridFactory;
use App\AdminModule\Grids\ProductGrid;
use App\Attribute\AttributeFacade;
use App\Attribute\Core\Attribute;
use App\Category\CategoryService;
use App\File\ImageService;
use App\InvalidArgumentException;
use App\Product\Image\ImageManager;
use App\Product\Product;
use App\Product\ProductService;
use App\Product\Variant;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Nette\Application\UI\Form;

/**
 * Class ProductPresenter
 * @package App\AdminModule\Presenters
 * @property Product $edited
 */
class ProductPresenter extends BasePresenter
{
	/** @var ProductService @inject */
	public $productService;

	/** @var IProductGridFactory @inject */
	public $gridFactory;

	/** @var IProductFormFactory @inject */
	public $formFactory;

	/** @var IReservedProductsGridFactory @inject */
	public $reservedProductsGridFactory;

	/** @var IProductVariantGridFactory @inject */
	public $variantsGridFactory;

	/** @var IVariantDetailControlFactory @inject */
	public $variantDetailControlFactory;

	/** @var IProductAttributeGridFactory @inject */
	public $productAttributeGridFactory;

	/** @var IProductAttributeDetailControlFactory @inject */
	public $attributeDetailControlFactory;

	/** @var IAttributeAttachControlFactory @inject */
	public $attributeAttachControlFactory;

	/** @var IImageUploadFormFactory @inject */
	public $imageFormFactory;

	/** @var IRelatedProductsControlFactory @inject */
	public $relatedProductsControlFactory;

	/** @var ISettingsControlFactory @inject */
	public $filtersSettingsControlFactory;

	/** @var CategoryService @inject */
	public $categoryService;

	/** @var ImageService @inject */
	public $imageService;

	/** @var ImageManager @inject */
	public $imageManager;

	/** @var AttributeFacade @inject */
	public $attributeFacade;

	/** @var Variant */
	private $variant;

	/** @var Attribute */
	private $attribute;

	public function beforeRender()
	{
		parent::beforeRender();

		$this->template->isEdited = isset($this->edited);
		$this->template->action = $this->getAction();
	}

	public function actionEdit($id)
	{
		$this->template->product = $this->edited = $this->productService->getProduct($id);

		$this['form']->setDefaults($this->productService->getFormDefaults($this->edited));
		$this['form']->onSuccess[] = function(Form $form)
		{
			$this->productService->updateProduct($form->getValues(), $this->edited);
			$this->redirect('edit', $this->edited->getId());
		};
	}

	public function actionVariants($id, $variantId = null)
	{
		$this->template->product = $this->edited = $this->productService->getProduct($id);

		if (is_numeric($variantId)) {
			try {
				$this->variant = $this->edited->getPriceVariant($variantId);
			} catch (InvalidArgumentException $e) {
				$this->flashMessage('Požadovaná varianta nebyla u produktu nalezena', 'danger');
				$this->redirect('edit', ['id' => $id]);
			}
		}
	}

	public function renderVariants()
	{
		$this->template->variant = $this->variant;
	}

	public function actionAttributes($id, $attributeId = null)
	{
		$this->template->product = $this->edited = $this->productService->getProduct($id);

		if (is_numeric($attributeId)) {
			try {
				$this->attribute = $this->attributeFacade->getAttribute(intval($attributeId));
			} catch (InvalidArgumentException $e) {
				$this->flashMessage('Požadovaná vlastnost nebyla u produktu nalezena', 'danger');
				$this->redirect('edit', ['id' => $id]);
			}
		}
	}

	public function actionImages($id)
	{
		$this->template->product = $this->edited = $this->productService->getProduct($id);
	}


	public function actionNew()
	{
		$this['form']->onSuccess[] = function(Form $form)
		{
			try {
				$product = $this->productService->createProduct($form->getValues());
				$this->redirect('edit', $product->getId());
			} catch (UniqueConstraintViolationException $e) {
				$this->flashMessage('Produkt s tímto názvem už ve vybrané kategorii existuje a nejde znovu vytvořit. Vznikly by tak duplicitní URL adresy. Použijte prosím jiný název.', 'danger');
			}
		};

		$this['form']->setDefaults(['vat'=>\App\Utils\Vat::TYPE_21]);

		$this->setView('edit');
	}

	public function actionRelated($id)
	{
		$this->template->product = $this->edited = $this->productService->getProduct($id);
	}

	public function actionFilters($id)
	{
		$this->template->product = $this->edited = $this->productService->getProduct($id);
	}

	public function handleDelete($id)
	{
		$this->productService->deleteProduct($id);
		$this['grid']->reload();
	}

	public function handleDeleteImage($imageId)
	{
		try {
			$this->imageManager->removeImageFromProduct($this->edited, (int)$imageId);
			$this->flashMessage('Obrázek byl smazán', 'success');
		} catch (\Exception $e) {
			$this->flashMessage('Při mazání došlo k chybě.');
		}

		$this->redirect('this');
	}

	public function handleSort($item_id, $prev_id, $next_id)
	{
		$this->productService->changeVariantsOrder(
			$this->productService->getPriceVariant($item_id),
			$prev_id ? $this->productService->getPriceVariant($prev_id) : null,
			$next_id ? $this->productService->getPriceVariant($next_id) : null
		);

		$this['variantsGrid']->redrawControl();
	}

	/**
	 * @param null $name
	 * @return \App\AdminModule\Forms\ProductForm
	 */
	protected function createComponentForm($name = null)
	{
		$isNewItem = $this->edited ? false : true;
		$imagesIds = ($isNewItem) ? [] : $this->edited->getImagesIds();
		return $this->formFactory->create($this, $name, $this->categoryService->getCategoriesMenuList(), $isNewItem, $imagesIds);
	}

	protected function createComponentImageForm($name = null)
	{
		$form = $this->imageFormFactory->create($this, $name);

		$form->onSuccess[] = function(Form $form)
		{
			$values = $form->getValues();
			foreach($values->images as $imageData)
			{
				$this->imageManager->attachImageToProduct($imageData, $this->edited);
			}

			$this->redirect('this');
		};

		return $form;
	}

	/**
	 * @return \App\AdminModule\Grids\ProductGrid
	 */
	protected function createComponentGrid()
	{
		return $this->gridFactory->create(ProductGrid::TYPE_COMMON);
	}

	protected function createComponentReservedProductsGrid($name = null)
	{
		return $this->reservedProductsGridFactory->create($this, $name);
	}

	protected function createComponentVariantsGrid($name = null)
	{
		return $this->variantsGridFactory->create($this, $name, $this->edited);
	}

	protected function createComponentVariantDetailControl($name = null)
	{
		return $this->variantDetailControlFactory->create($this, $name, $this->variant);
	}

	protected function createComponentAttributeAttach()
	{
		return $this->attributeAttachControlFactory->create($this->edited);
	}

	protected function createComponentAttributesGrid()
	{
		return $this->productAttributeGridFactory->create($this->edited);
	}

	protected function createComponentAttributeDetailControl()
	{
		return $this->attributeDetailControlFactory->create($this->edited, $this->attribute);
	}

	protected function createComponentRelatedProducts()
	{
		return $this->relatedProductsControlFactory->create($this->edited);
	}

	protected function createComponentFiltersSettings()
	{
		return $this->filtersSettingsControlFactory->create($this->edited);
	}

}