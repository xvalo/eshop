<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\IEventFormFactory;
use App\AdminModule\Grids\IEventGridFactory;
use App\Event\EventService;
use Nette\Application\UI\Form;

class EventPresenter extends BasePresenter
{
    /**
    * @var IEventFormFactory
    * @inject
    */
    public $formFactory;

    /**
     * @var IEventGridFactory
     * @inject
     */
    public $gridFactory;

    /**
     * @var EventService
     * @inject
     */
    public $eventService;

    public function actionEdit($id)
    {
        $this->edited = $this->eventService->getOne($id);
        $this['form']->setDefaults($this->eventService->getFormDefaults($this->edited));
    }

	public function renderEdit()
	{
		$this->template->event = $this->edited;
    }

    public function actionNew()
    {
        $this->setView('edit');
    }

    public function handleDelete()
    {
        
    }

    /**
     * @param null $name
     * @return \App\AdminModule\Forms\EventForm
     */
    public function createComponentForm($name = null)
    {
        $form = $this->formFactory->create($this, $name);

        $form->onSuccess[] = function(Form $form){
            try {
                if ($this->edited) {
                    $this->eventService->update($this->edited, $form->getValues(true));
                } else {
                    $this->eventService->create($form->getValues(true));
                }
                $this->flashMessage('Událost byla uložena.', 'success');
            } catch (\Exception $e) {
                $this->flashMessage('Při ukládání došlo k chybě.', 'danger');
            }
            $this->redirect('default');
        };

        return $form;
    }

    /**
     * @param null $name
     * @return \App\AdminModule\Grids\NewsGrid
     */
    public function createComponentGrid($name = null)
    {
        return $this->gridFactory->create($this, $name);
    }
}