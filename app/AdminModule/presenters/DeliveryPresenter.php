<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\IDeliveryFormFactory;
use App\AdminModule\Grids\IDeliveryGridFactory;
use App\AdminModule\Grids\IPaymentTypeGridFactory;
use App\Lists\Delivery;
use App\Order\Delivery\DeliveryService;
use App\Order\Delivery\Entity\DeliveryPaymentType;
use Nette\Application\UI\Form;

/**
 * Class DeliveryPresenter
 * @package App\AdminModule\Presenters
 * @property Delivery $edited
 */
class DeliveryPresenter extends BasePresenter
{
	/** @var IDeliveryGridFactory @inject */
	public $deliveryGridFactory;

	/** @var IPaymentTypeGridFactory @inject */
	public $paymentTypeGridFactory;

	/** @var IDeliveryFormFactory @inject */
	public $deliveryFormFactory;

	/** @var DeliveryService @inject */
	public $deliveryService;

	public function actionDefault()
	{
		
	}

	public function actionPaymentTypeSetting($id)
	{
		if (!is_numeric($id)) {
			$this->error();
		}

		$this->edited = $this->deliveryService->getDelivery((int) $id);

		if ($this->edited === null) {
			$this->error();
		}

	}

	public function renderPaymentTypeSetting()
	{
		$this->template->delivery = $this->edited;
	}

	public function handleDelete($id)
	{

	}

	public function handleSort($item_id, $prev_id, $next_id)
	{
		$this->deliveryService->sortPriorities(intval($item_id), intval($prev_id), intval($next_id));
		$this['deliveryGrid']->redrawControl();
	}

	protected function createComponentDeliveryGrid($name = null)
	{
		return $this->deliveryGridFactory->create($this, $name);
	}

	protected function createComponentPaymentGrid($name = null)
	{
		return $this->paymentTypeGridFactory->create($this, $name);
	}

	protected function createComponentDeliveryForm($name = null)
	{
		$form = $this->deliveryFormFactory->create($this, $name);

		$form->onSuccess[] = function(Form $form) {
			$values = $form->getValues();

			foreach ($values->paymentTypes as $paymentTypeId => $data) {
				if ($data->isAvailable) {
					$this->deliveryService->setPaymentTypeToDelivery($this->edited, (int) $paymentTypeId, (float)$data->price);
				} else {
					$this->deliveryService->removePaymentTypeFromDelivery($this->edited, (int) $paymentTypeId);
				}
			}

			$this->flashMessage('Nastavení dopravy vylo uloženo.');
			$this->redirect('this');
		};

		$defaults = [];

		/** @var DeliveryPaymentType $paymentType */
		foreach ($this->edited->getPaymentTypes() as $paymentType) {
			$defaults['paymentTypes'][$paymentType->getPaymentType()->getId()] = [
				'isAvailable' => true,
				'price' => $paymentType->getPrice()
			];
		}

		$form->setDefaults($defaults);

		return $form;
	}
}