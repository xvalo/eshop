<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Grids\IOrderDocumentGridFactory;
use App\File\OrderDocument;
use App\File\OrderDocumentRepository;
use App\File\OrderDocumentType;
use App\InvalidArgumentException;

class DocumentPresenter extends BasePresenter
{
    /** @var IOrderDocumentGridFactory @inject */
    public $orderDocumentGridFactory;

    /** @var OrderDocumentRepository @inject */
    public $orderDocumentRepository;

    private $documentTypes;

	public function startup()
	{
		parent::startup();
    }

	public function actionInvoices()
	{
		$this->documentTypes = [OrderDocumentType::INVOICE, OrderDocumentType::CANCEL, OrderDocumentType::CORRECTIVE];
    }

	public function renderInvoices()
	{
		$this->setView('default');
    }

	public function actionBills()
	{
		$this->documentTypes = [OrderDocumentType::BILL];
    }

	public function renderBills()
	{
		$this->setView('default');
    }

    public function handleDownload($document)
    {
        /** @var OrderDocument $orderDocument */
        $orderDocument = $this->orderDocumentRepository->find($document);

        if(!$orderDocument) {
            throw new InvalidArgumentException;
        }

        $this->sendPdfResponse($orderDocument->getPath(), $orderDocument->getNumber());
    }

    protected function createComponentGrid($name = null)
    {
        return $this->orderDocumentGridFactory->create($this, $name, $this->documentTypes);
    }
}