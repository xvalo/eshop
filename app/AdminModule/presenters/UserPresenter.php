<?php

namespace App\AdminModule\Presenters;

use App\Admin\Admin;
use App\Admin\AdminService;
use App\AdminModule\Forms\IAdminFormFactory;
use App\AdminModule\Grids\IUserGridFactory;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

/**
 * Class UserPresenter
 * @package App\AdminModule\Presenters
 * @property Admin $edited
 */
class UserPresenter extends BasePresenter
{

	/**
	 * @var IAdminFormFactory
	 * @inject
	 */
	public $userFormFactory;

	/**
	 * @var AdminService
	 * @inject
	 */
	public $adminService;

	/**
	 * @var IUserGridFactory
	 * @inject
	 */
	public $gridFactory;

	public function actionEdit($id)
	{
		$this->edited = $this->adminService->getAdmin($id);
		$this['userForm']->setDefaults($this->adminService->getFormDefaults($this->edited));
	}

	public function actionNew()
	{
		$this->setView('edit');
	}

	public function handleDelete($id)
	{

	}

	public function createComponentUserForm($name = null)
	{
		$form = $this->userFormFactory->create($this, $name);

		$form->onSuccess[] = function(Form $form)
		{
			$values = $form->getValues(true);
			unset($values['password_check']);

			if ($this->edited) {
				$user = $this->adminService->updateAdmin($values, $this->edited);
			} else {
				$user = $this->adminService->createAdmin($values);
			}

			$this->redirect('edit', $user->getId());
		};

		return $form;
	}

	public function createComponentGrid($name)
	{
		return $this->gridFactory->create($this, $name);
	}
}