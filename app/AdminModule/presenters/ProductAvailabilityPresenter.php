<?php

namespace App\AdminModule\Presenters;

use App\Admin\Forms\ProductAvailabilityForm;
use App\AdminModule\Grids\IAvailabilityGridFactory;
use App\Product\Availability\AvailabilityNotAvailableToMoveException;
use App\Product\Availability\AvailabilityService;
use App\Product\Availability\Entity\Availability;
use Tracy\Debugger;

class ProductAvailabilityPresenter extends BasePresenter
{
	/** @var IAvailabilityGridFactory @inject */
	public $availabilityGridFactory;

	/** @var ProductAvailabilityForm @inject */
	public $productAvailabilityFormFactory;

	/** @var AvailabilityService @inject */
	public $availabilityService;

	/** @var Availability */
	private $availability;

	public function actionEdit($id)
	{
		$this->availability = $this->availabilityService->getAvailability((int) $id);
	}

	public function renderEdit()
	{
		$this->template->availability = $this->availability;
	}

	public function renderNew()
	{
		$this->setView('edit');
	}

	/**
	 * @param  int      $item_id
	 * @param  int|NULL $prev_id
	 * @param  int|NULL $next_id
	 * @return void
	 */
	public function handleSort($item_id, $prev_id = null, $next_id = null)
	{

		try {
			$availabilityToMove = $this->availabilityService->getAvailability((int)$item_id);

			$this->availabilityService->checkIsAvailabilityAbleToMove($availabilityToMove);

			$this->availabilityService->changeAvailabilityPriority(
				$availabilityToMove,
				$prev_id ? $this->availabilityService->getAvailability((int)$prev_id) : null,
				$next_id ? $this->availabilityService->getAvailability((int)$next_id) : null
			);

			$this->flashMessage("Pořadí bylo upraveno", 'success');
		} catch (AvailabilityNotAvailableToMoveException $e) {
			$this->flashMessage("Základní dostupnost skladem a vyprodáno nemůže být přesunuta.", 'danger');
		} catch (\Exception $e) {
			Debugger::log($e, Debugger::ERROR);
			$this->flashMessage("Při změně pořadí došlo k chybě.", 'danger');
		}
		$this->redrawControl('flashes');

		$this['grid']->redrawControl();
	}

	/**
	 * @return \App\AdminModule\Grids\AvailabilityGrid
	 */
	protected function createComponentGrid()
	{
		return $this->availabilityGridFactory->create();
	}

	/**
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentForm()
	{
		return $this->productAvailabilityFormFactory->create(function(){
			$this->redirect('default');
		}, $this->availability);
	}
}