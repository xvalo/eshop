<?php

namespace App\AdminModule\Presenters;

use App\Product\ProductService;
use App\Search\SearchService;
use App\Services\Statistics\GeneralStatisticsService;
use App\Services\Statistics\SalesService;

class StatisticPresenter extends BasePresenter
{
	/** @var  SalesService @inject */
	public $salesService;

	/** @var GeneralStatisticsService @inject */
	public $generalStatisticsService;

	/** @var SearchService @inject */
	public $searchService;

	/** @var ProductService @inject */
	public $productService;

	public function renderDefault()
	{
		$this->template->eshopMonthSales = json_encode($this->salesService->getMonthEshopSales());
		$this->template->storeMonthSales = json_encode($this->salesService->getMonthStoreSales());
		$this->template->eshopQuarterSales = json_encode($this->salesService->getQuarterEshopSales());
		$this->template->storeQuarterSales = json_encode($this->salesService->getQuarterStoreSales());
		$this->template->eshopYearSales = json_encode($this->salesService->getYearEshopSales());
		$this->template->storeYearSales = json_encode($this->salesService->getYearStoreSales());

		$this->template->productsCount = $this->generalStatisticsService->getProductsCount();
		$this->template->categoriesCount = $this->generalStatisticsService->getCategoriesCount();
		$this->template->registeredCustomersCount = $this->generalStatisticsService->getRegisteredCustomersCount();
		$this->template->customersCount = $this->generalStatisticsService->getCustomersCount();

		$this->template->mostPopularPhrases = $this->searchService->getMostPopularPhrases();
		$this->template->bestSellerProducts = $this->productService->getBestSellerProducts();
	}
}