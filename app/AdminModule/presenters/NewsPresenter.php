<?php
namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\INewsFormFactory;
use App\AdminModule\Grids\INewsGridFactory;
use App\News\Entity\News;
use App\News\NewsService;
use Nette\Application\UI\Form;

/**
 * Class NewsPresenter
 * @package App\AdminModule\Presenters
 * @property News $edited
 */
class NewsPresenter extends BasePresenter
{
	/** @var INewsFormFactory @inject */
	public $formFactory;

	/** @var INewsGridFactory @inject */
	public $gridFactory;

	/** @var NewsService @inject */
	public $newsService;

	public function actionEdit($id)
	{
		$this->edited = $this->newsService->getNewsById($id);
		$this['form']->setDefaults($this->newsService->getFormDefaults($this->edited));
	}

	public function renderEdit()
	{
		$this->template->news = $this->edited;
	}

	public function actionNew()
	{
		$this->setView('edit');
	}

	public function handleDelete($id)
	{
		try {
			$news = $this->newsService->getNewsById($id);
			$this->newsService->remove($news);
			$this->flashMessage('Článek byl smazán.', 'sucess');
		} catch (\Exception $e) {
			$this->flashMessage('Při mazání článku došlo k chybě.', 'error');
		}

		$this->redirect('this');
	}

	/**
	 * @param null $name
	 * @return \App\AdminModule\Forms\NewsForm
	 */
	public function createComponentForm($name = null)
	{
		$form = $this->formFactory->create($this, $name);

		$form->onSuccess[] = function(Form $form){
			try {
				if ($this->edited) {
					$this->newsService->update($this->edited, $form->getValues(true));
				} else {
					$this->newsService->create($form->getValues(true));
				}
				$this->flashMessage('Článek byl uložen.', 'success');
			} catch (\Exception $e) {
				$this->flashMessage('Při ukládání došlo k chybě.', 'danger');
			}
			$this->redirect('default');
		};

		return $form;
	}

	/**
	 * @return \App\AdminModule\Grids\NewsGrid
	 */
	public function createComponentGrid()
	{
		return $this->gridFactory->create();
	}
}