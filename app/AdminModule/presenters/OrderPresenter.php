<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\IOrderCommentsControlFactory;
use App\AdminModule\Components\IOrderDocumentsControlFactory;
use App\AdminModule\Components\IOrderEditControlFactory;
use App\AdminModule\Components\IOrderEmailsControlFactory;
use App\AdminModule\Components\IOrderLinesControlFactory;
use App\AdminModule\Forms\IOrderFormFactory;
use App\AdminModule\Forms\IPaymentFormFactory;
use App\AdminModule\Grids\IOrderGridFactory;
use App\AdminModule\Grids\OrderGrid;
use App\Classes\Eet\EetReceiptService;
use App\Classes\Order\OrderDeliverability;
use App\Classes\Order\OrderDocumentService;
use App\Customer\CustomerService;
use App\Order\Delivery\DeliveryService;
use App\InvalidStateException;
use App\Lists\CountryList;
use App\Lists\ListsService;
use App\Order\AdminOrderService;
use App\Order\Order;
use App\Order\OrderService;
use App\Order\OrderStatus;
use App\Order\Payment;
use App\Product\ProductService;
use Nette\Application\BadRequestException;
use Nette\Application\Responses\CsvResponse;
use Nette\Application\UI\Form;
use Tracy\Debugger;

/**
 * Class OrderPresenter
 * @package App\AdminModule\Presenters
 * @property Order $edited
 */
class OrderPresenter extends BasePresenter
{
    /** @var IOrderFormFactory @inject */
    public $formFactory;

    /** @var IOrderGridFactory @inject */
    public $gridFactory;

    /** @var IOrderLinesControlFactory @inject */
    public $orderLinesControlFactory;

    /** @var IOrderEditControlFactory @inject */
    public $orderEditControlFactory;

    /** @var IPaymentFormFactory @inject */
    public $paymentFormFactory;

    /** @var IOrderDocumentsControlFactory @inject */
    public $orderDocumentsListFactory;

    /** @var IOrderCommentsControlFactory @inject */
    public $orderCommentsFactory;

    /** @var IOrderEmailsControlFactory @inject */
    public $orderEmailsListFactory;

    /** @var OrderService @inject */
    public $orderService;

    /** @var AdminOrderService @inject */
    public $adminOrderService;

    /** @var EetReceiptService @inject */
    public $eetReceiptService;

    /** @var ListsService @inject */
    public $listsService;

    /** @var CustomerService @inject */
    public $customerService;

    /** @var ProductService @inject */
    public $productService;

    /** @var OrderDocumentService @inject */
    public $orderDocumentService;

    /** @var DeliveryService @inject */
    public $deliveryService;

    public function beforeRender()
    {
        parent::beforeRender();

        $this->template->isEdited = false;
        if($this->edited)
        {
            $this->template->isEdited = true;
        }
    }

    public function actionNew()
    {
        $this->setView('edit');
    }

    public function actionEdit($id)
    {
        $this->edited = $this->orderService->getOrder($id);

        if (!$this->edited->isEditable()) {
        	$this->flashMessage('Objednávku nelze editovat. Byla buď stornována nebo už odeslána zákazníkovi', 'danger');
        	$this->redirect('detail', $id);
		}
    }

	public function renderEdit()
	{
		$this->template->form = $this['orderEdit'];
		$this->template->getLatte()->addProvider('formsStack', [$this['orderEdit']]);
    }

    public function actionDetail($id)
    {
        $this->edited = $this->orderService->getOrder($id);
    }

    public function renderDetail()
    {
        $this->template->order = $this->edited;
        $this->template->title = 'Detail objednávky';
        $this->template->subtitle = 'č. '.$this->edited->getOrderNo();
        $this->template->nextStatus = $this->getNextOrderStatus();
        $this->template->isOrderBillable = OrderDeliverability::isOrderByDeliveryBillable($this->edited->getPaymentType());
	    $this->template->isOrderInvoicable = OrderDeliverability::isOrderByDeliveryInvoicable($this->edited->getPaymentType());
	    $this->template->czechPostBoxBinding = (int)$this->applicationConfigurationService->getCzechPostBoxDeliveryBinding();
	    $this->template->czechPostOfficeBinding = (int)$this->applicationConfigurationService->getCzechPostOfficeDeliveryBinding();

        if ($this->getParameter('downloadReceipt', false)) {
        	$document = $this->edited->getReceipt();
        	if ($document) {
		        $this->template->receiptDownloadLink = $this['documentsList']->link('downloadDocument!', ['documentId' => $document->getId()]);
	        }
        }
    }

    public function actionCanceled()
    {
        $this->setView('default');
    }

    protected function createComponentGrid($name = null)
    {
        $gridType = $this->getAction() == 'canceled' ? OrderGrid::TYPE_CANCELED : OrderGrid::TYPE_COMMON;
        return $this->gridFactory->create($this, $name, $gridType);
    }

    protected function createComponentExportGlsGrid($name = null)
    {
        $grid = $this->gridFactory->create($this, $name, OrderGrid::TYPE_GLS_DELIVERY);

        $grid->addExportCallback('Exportovat CSV', function ($data_source, $grid){
            $data = [];
            /** @var Order $item */
            foreach ($data_source as $item) {
                $deliveryAddress = $item->getDeliveryAddress();
                $data[] = [
	                'dobirka' => $item->getPaymentType()->getId() != OrderDeliverability::TRANSFER_PAYMENT ? $item->getTotalPrice() : 0,
                    'jmeno' => $deliveryAddress->getName() . ' ' . $deliveryAddress->getLastName(),
                    'firma' => $deliveryAddress->getCompany(),
                    'telefon' => $item->getContactInformation()->getPhone(),
                    'email' => $item->getContactInformation()->getEmail(),
                    'ulice' => $deliveryAddress->getStreet(),
                    'mesto' => $deliveryAddress->getCity(),
                    'psc' => $deliveryAddress->getZip(),
                    'zeme' => CountryList::getStatusLabel($deliveryAddress->getCountry()),
                    'cena' => $item->getTotalPrice(),
                    'cislo-objednavky' => $item->getOrderNo(),
                    'variabilni-symbol' => ($item->getReceipt()) ? $item->getReceipt()->getNumber() : null
                ];
            }

	        $fileName = 'GLS_export.csv';
            $response = new CsvResponse($data, $fileName);
            $response->setGlue(CsvResponse::SEMICOLON);
            $response->setOutputCharset('windows-1250');
            $this->sendResponse($response);
        }, true);

        return $grid;
    }

    protected function createComponentExportCpGrid($name = null)
    {
        $grid = $this->gridFactory->create($this, $name, OrderGrid::TYPE_CP_DELIVERY);

        $grid->addExportCallback('Exportovat CSV', function ($data_source, $grid){
            $data = [];
            /** @var Order $item */
            foreach ($data_source as $item) {
            	$postDeliveryType = ($item->getDeliveryOrderLine()->getCzechPostOffice() ? 'NP' : ($item->getDeliveryOrderLine()->getCzechPostBox() ? 'NB' : 'DR'));
            	$specialDelivery = ($item->getDeliveryOrderLine()->getCzechPostOffice() ?: ($item->getDeliveryOrderLine()->getCzechPostBox() ?: null));

                $deliveryAddress = $item->getDeliveryAddress();
                $name = $specialDelivery
	                ? $item->getContactInformation()->getName() . ' ' . $item->getContactInformation()->getLastName()
	                : $deliveryAddress->getName() . ' ' . $deliveryAddress->getLastName();
                $data[] = [
                    $name,
	                $specialDelivery ? '' : $deliveryAddress->getCompany(),
                    $item->getContactInformation()->getPhone(),
                    $item->getContactInformation()->getEmail(),
                    $specialDelivery ? null : $deliveryAddress->getStreet(),
                    $specialDelivery ? $specialDelivery->getCity() : $deliveryAddress->getCity(),
                    $specialDelivery ? $specialDelivery->getZip() : $deliveryAddress->getZip(),
                    CountryList::getStatusLabel($specialDelivery ? CountryList::CZECH_REPUBLIC : $deliveryAddress->getCountry()),
                    $item->getTotalPrice(),
                    $item->getOrderNo(),
                    ($item->getReceipt()) ? $item->getReceipt()->getNumber() : null,
					$item->getPaymentType()->getId() != OrderDeliverability::TRANSFER_PAYMENT ? $item->getTotalPrice() : 0,
					$postDeliveryType,
	                $item->getPaymentType() !== null && $item->getPaymentType()->getId() === OrderDeliverability::CASH_ON_DELIVERY ? '41+45' : '45'
                ];
            }

	        $date = date('dmY');
            $fileName = 'CP_export_'.$date.'.csv';
            $response = new CsvResponse($data, $fileName, false);
            $response->setGlue(CsvResponse::SEMICOLON);
            $response->setOutputCharset('utf-8');
            $this->sendResponse($response);
        }, true);

        return $grid;
    }

    protected function createComponentOrderLines($name = null)
    {
        return $this->orderLinesControlFactory->create($this->edited, $this, $name);
    }

	protected function createComponentPaymentForm($name = null)
	{
		$form = $this->paymentFormFactory->create($this, $name);

		$form->onSuccess[] = function (Form $form) {
			$redirectArgs = [];
			try {
				if ($form['paidByCard']->isSubmittedBy()) {
					$paidBy = Payment::TYPE_BY_CARD;
					$receivedAmount = $this->edited->getTotalPrice();
				} elseif ($form['paidCash']->isSubmittedBy()) {
					$paidBy = Payment::TYPE_CASH;
					$receivedAmount = (float)$form->getValues(true)['amount'];
				} else {
					throw new InvalidStateException('Wrong paiment type');
				}

				$this->adminOrderService->setOrderPayment($this->edited, $paidBy, $receivedAmount);
				$nextBillDocumentNumber = $this->applicationSettingsManager->getNextBillNumber();
				$this->eetReceiptService->createReceipt($this->edited, $nextBillDocumentNumber);
				$this->orderDocumentService->createBillDocument($this->edited, $nextBillDocumentNumber);

				$this->flashMessage('Platba byla uložena.', 'success');
				$redirectArgs['downloadReceipt'] = true;
			} catch (\Exception $e) {
				Debugger::log($e);
				$this->flashMessage('Při zadávání platby došlo k chybě.', 'error');
			}

			$this->redirect('this', $redirectArgs);
		};

		return $form;
    }

	protected function createComponentDocumentsList($name = null)
	{
		return $this->orderDocumentsListFactory->create($this->edited, $this, $name);
    }

	protected function createComponentEmailsList($name = null)
	{
		return $this->orderEmailsListFactory->create($this->edited, $this, $name);
	}

	protected function createComponentOrderComments($name = null)
	{
		return $this->orderCommentsFactory->create($this->edited, $this, $name);
	}

    public function handleChangeStatus($toStatus)
    {
        if (is_null($toStatus)) {
            throw new BadRequestException;
        }

        $this->adminOrderService->setOrderStatus($this->edited, $toStatus);
        $this->flashMessage('Stav objednávky byl změněn.', 'success');
        $this->redirect('this');
    }

    public function handleSetCanceled($sendNotification = true)
    {
    	try {
		    $this->adminOrderService->setOrderCanceled($this->edited, $sendNotification);
		    $this->flashMessage('Objednávka byla stornována.', 'success');
	    } catch (InvalidStateException $e) {
    		$this->flashMessage($e->getMessage(), 'danger');
	    }
        $this->redirect('this');
    }

    public function handleRemoveCanceled()
    {
        $this->adminOrderService->removeCanceledState($this->edited);
        $this->flashMessage('Objednávka byla obnovena.', 'success');
        $this->redirect('this');
    }

    public function handleSetPayed()
    {
        $this->adminOrderService->setOrderPayed($this->edited);
        $this->flashMessage('Stav zaplaceno byl nastaven.', 'success');
        $this->redirect('this');
    }

    public function handleSetShipped($packageNo = null)
    {
        if ($this->edited->getDelivery() && $this->edited->getDelivery()->getId() != $this->applicationConfigurationService->getPersonalPickupDeliveries() && !$packageNo) {
            $this->flashMessage('Objednávka má nastavenou dopravu, ale nebylo zadáno číslo balíku. Zadejte ho prosím.', 'warning');
        } else {
            $this->adminOrderService->setOrderShipped($this->edited, $packageNo);
            $this->flashMessage('Stav odesláno byl nastaven.', 'success');
        }

        $this->redirect('this');
    }

	public function handleSendEetReceipt()
	{
		if (
			$this->edited->getLastProcessedEetReceipt()
			|| !OrderDeliverability::isOrderByDeliveryInvoicable($this->edited->getPaymentType())
			|| $this->edited->getReceipt()
		){
			$this->flashMessage('Platba nemůže být odeslána. Objednávka je ve špatném stavu (faktura je vygenerována, účtenka byla odeslána nebo není vůbec možné účtenku k této objednávce odeslat)', 'warning');
			$this->redirect('this');
		}

		try {
			$nextInvoiceNumber = $this->applicationSettingsManager->getNextInvoiceNumber();
			$this->eetReceiptService->createReceipt($this->edited, $nextInvoiceNumber);
			$this->orderDocumentService->createInvoiceDocument($this->edited, $nextInvoiceNumber);

			$this->flashMessage('Platba byla odeslána a faktura vygenerována.', 'success');
		} catch (\Exception $e) {
			Debugger::log($e, Debugger::ERROR);
			$this->flashMessage('Při zadávání platby došlo k chybě. Obraťte se prosím na podporu a nahlašte chybu', 'danger');
		}
		$this->redirect('this');
    }

    // ======= PRIVATE METHODS =======

    private function getNextOrderStatus()
    {
        switch ($this->edited->getStatus()) {
            case OrderStatus::CREATED:
                return OrderStatus::IN_PROCESS;
            case OrderStatus::IN_PROCESS:
                return OrderStatus::DONE;
            case OrderStatus::DONE:
                return null;
			default:
				throw new InvalidStateException('Objednávka má neznámý stav.');
        }
    }

	protected function createComponentOrderEdit()
	{
		return $this->orderEditControlFactory->create($this->edited);
    }
}