<?php

namespace App\AdminModule\Presenters;

use App\File\Image;
use App\Gallery\PicturesManager;
use App\Gallery\PicturesProvider;
use IPub\VisualPaginator\Components\Control;
use IPub\VisualPaginator\TVisualPaginator;
use Nette\Application\UI\Form;

class GalleryPresenter extends BasePresenter
{
	use TVisualPaginator;

	/**
	 * @var PicturesProvider @inject
	 */
	public $picturesProvider;

	/**
	 * @var PicturesManager @inject
	 */
	public $picturesManger;

	/** @persistent */
	public $searchPhrase;

	public function renderDefault()
	{
		/** @var Control $visualPaginator */
		$visualPaginator = $this['paginator'];
		$paginator = $visualPaginator->getPaginator();
		$paginator->setItemsPerPage(30);
		$paginator->setItemCount($this->picturesProvider->getPicturesCount($this->searchPhrase));

		$this->template->pictures = $this->picturesProvider->getPictures($paginator->getItemsPerPage(), $paginator->getOffset(), $this->searchPhrase);
		$this->template->domain = $this->applicationSettingsManager->getDomain();
	}

	public function handleRemoveImage($id)
	{
		$image = $this->picturesProvider->getImage((int) $id);
		$messageType = 'danger';
		$message = 'Obrázek nebyl nalezen, nepodařilo se ho uložit.';

		if ($image instanceof Image)  {
			$this->picturesManger->removeImage($image);
			$messageType = 'success';
			$message = 'Obrázek byl smazán.';
		}

		$this->flashMessage($message, $messageType);
		$this->redirect('this');
	}

	protected function createComponentPaginator()
	{
		$control = $this->visualPaginatorFactory->create();

		$control->disableAjax();
		$control->setTemplateFile('bootstrap.latte');

		return $control;
	}

	protected function createComponentSearchForm()
	{
		$form = new Form();

		$form->addText('phrase');
		$form->addSubmit('search');
		$form->addSubmit('reset');

		$form->onSuccess[] = function(Form $form) {
			if ($form->isSubmitted()->getName() === 'search') {
				$this->searchPhrase = $form->getValues()->phrase;
			} elseif ($form->isSubmitted()->getName() === 'reset') {
				$this->searchPhrase = null;
			}

			$this->redirect('this');
		};

		$form->setDefaults([
			'phrase' => $this->searchPhrase
		]);



		return $form;
	}
}