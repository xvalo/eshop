<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\ICustomerFormFactory;
use App\AdminModule\Grids\ICustomerGridFactory;
use App\Customer\Customer;
use App\Customer\CustomerService;
use Nette\Application\UI\Form;

/**
 * Class CustomerPresenter
 * @package App\AdminModule\Presenters
 * @property Customer $edited
 */
class CustomerPresenter extends BasePresenter
{
	/** @var ICustomerGridFactory @inject */
	public $gridFactory;

	/** @var ICustomerFormFactory @inject */
	public $formFactory;

	/** @var CustomerService @inject */
	public $customerService;

	public function actionEdit($id)
	{
		$this->edited = $this->customerService->getOne($id);
		$this['form']->setDefaults($this->customerService->getFormDefaults($this->edited));
	}

	public function renderEdit()
	{
		$this->template->customer = $this->edited;
	}

	public function actionDetail($id)
	{

	}

	public function renderDetail()
	{
		
	}

	/**
	 * @param null $name
	 * @return \App\AdminModule\Grids\CustomerGrid
	 */
	protected function createComponentGrid($name = null)
	{
		return $this->gridFactory->create($this, $name);
	}

	protected function createComponentForm($name = null)
	{
		$form = $this->formFactory->create($this, $name);

		$form->onSuccess[] = function(Form $form) {
			try {
				$values = $form->getValues(true);
				$this->customerService->update($this->edited, $values);
				$this->flashMessage('Změny byly uloženy', 'success');
			} catch (\Exception $e) {
				$this->flashMessage('Při ukládání došlo k chybě.', 'danger');
			}
			$this->redirect('this');
		};

		return $form;
	}
}