<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\FilterForm;
use App\AdminModule\Grids\IFilterGridFactory;
use App\AdminModule\Grids\IFilterValuesGridFactory;
use App\Filter\Entity\Filter;
use App\Filter\FilterService;

class FilterPresenter extends BasePresenter
{
	/** @var IFilterGridFactory @inject */
	public $filterGridFactory;

	/** @var IFilterValuesGridFactory @inject */
	public $filterValuesGridFactory;

	/** @var FilterService @inject */
	public $filterService;

	/** @var FilterForm @inject */
	public $filterFormFactory;

	/** @var Filter */
	private $filter;

	public function beforeRender()
	{
		parent::beforeRender();

		$this->template->isEdited = isset($this->filter);
		$this->template->action = $this->getAction();
	}

	public function actionEdit($id)
	{
		$this->filter = $this->filterService->getFilter((int)$id);
	}

	public function renderEdit()
	{
		$this->template->filter = $this->filter;
	}

	public function actionValues($id)
	{
		$this->filter = $this->filterService->getFilter((int)$id);
	}

	public function renderValues()
	{
		$this->template->filter = $this->filter;
	}

	public function actionNew()
	{
		$this->setView('edit');
	}

	public function handleRemove($id)
	{
		$filter = $this->filterService->getFilter((int) $id);

		if (!$filter) {
			$this->error('Filtr neexistuje');
		}

		$this->filterService->removeFilter($filter);
		$this->flashMessage('Filtr byl smazán', 'success');

		if ($this->isAjax()) {
			$this['grid']->reload();
		} else {
			$this->redirect('default');
		}
	}

	public function handleRemoveValue($id, $value)
	{
		$filter = $this->filterService->getFilter((int) $id);
		$value = $this->filterService->getFilterValue((int) $value);

		$this->filterService->removeFilterValue($filter, $value);

		if ($this->isAjax()) {
			$this['filterValuesGrid']->reload();
		} else {
			$this->redirect('default');
		}


	}

	public function handleSort($item_id, $prev_id, $next_id)
	{
		$this->filterService->changeFilterValuesOrder(
			$this->filterService->getFilterValue($item_id),
			$prev_id ? $this->filterService->getFilterValue($prev_id) : null,
			$next_id ? $this->filterService->getFilterValue($next_id) : null
		);

		$this['filterValuesGrid']->reload();
	}

	/**
	 * @return \App\AdminModule\Grids\FilterGrid
	 */
	protected function createComponentGrid()
	{
		return $this->filterGridFactory->create();
	}

	protected function createComponentFilterValuesGrid()
	{
		return $this->filterValuesGridFactory->create($this->filter);
	}

	/**
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentForm()
	{
		return $this->filterFormFactory->create(function(Filter $filter){
			$this->flashMessage('Filtr byl úspěšně uložen', 'success');
			$this->redirect('Filter:edit', ['id' => $filter->getId()]);
		}, $this->filter);
	}
}