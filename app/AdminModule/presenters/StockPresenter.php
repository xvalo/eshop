<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\IStockOperationFormFactory;
use App\AdminModule\Grids\IStockFileGridFactory;
use App\AdminModule\Grids\IStockGridFactory;
use App\AdminModule\Grids\IStockPriceVariantGridFactory;
use App\Lists\Stock;
use App\Stock\StockManager;
use App\Product\Price\RegularPrice;
use App\Product\Product;
use App\Product\ProductService;
use App\Stock\StockService;
use App\Stock\VariantStockItem\Session\StockOperationManager;
use App\Stock\VariantStockItem\Session\StockOperationVariantData;
use Doctrine\ORM\NoResultException;
use Nette\Application\UI\Form;
use Nette\Utils\DateTime;
use Tracy\Debugger;

/**
 * Class StockPresenter
 * @package App\AdminModule\Presenters
 * @property Product $edited
 */
class StockPresenter extends BasePresenter
{
	/** @var IStockOperationFormFactory @inject */
	public $stockOperationFormFactory;

	/** @var  IStockFileGridFactory @inject */
	public $stockFileGridFactory;

	/** @var IStockGridFactory @inject */
	public $stockGridFactory;

	/** @var IStockPriceVariantGridFactory @inject */
	public $priceVariantGrid;

	/** @var ProductService @inject */
	public $productService;

	/** @var StockService @inject */
	public $stockService;

	/** @var StockManager @inject */
	public $stockManager;

	/** @var StockOperationManager @inject */
	public $stockOperationManager;

	private $type;

	private $selectedStock;

	private $defaultPageTitles = [
		StockOperationManager::OPERATION_TYPE_IN => 'Příjem zboží do skladu',
		StockOperationManager::OPERATION_TYPE_OUT => 'Výdejky zboží ze skladu',
		StockOperationManager::OPERATION_TYPE_OFF => 'Odpis zboží ze skladu'
	];

	private $archivePageTitles = [
		StockOperationManager::OPERATION_TYPE_IN => 'Archiv příjemek',
		StockOperationManager::OPERATION_TYPE_OUT => 'Archiv výdejek',
		StockOperationManager::OPERATION_TYPE_OFF => 'Archiv odpisů'
	];

	public function actionEdit($id, $stock = null)
	{
		$this->edited = $this->productService->getProduct($id);
		$this->selectedStock = $this->stockService->getStock($stock);
	}

	public function renderEdit()
	{
		$this->template->product = $this->edited;
		$this->template->stockList = $this->stockService->getStocksList();
		$this->template->selectedStock = $this->selectedStock;
	}

	public function actionArchive($type)
	{
		$this->type = $type;
	}

	public function renderArchive()
	{
		$this->template->actionTitle = $this->archivePageTitles[$this->type];
		$this->template->newAction = $this->type;
	}

	public function actionChange($type)
	{
		$this->type = $type;

		if (!$this->stockOperationManager->exists($type)) {
			$this->stockOperationManager->reset($type);
		}

		$variantsToAdd = $this->getHttpRequest()->getPost('variantsToAdd')
			? json_decode($this->getHttpRequest()->getPost('variantsToAdd'), true)
			: [];

		if (!empty($variantsToAdd)) {
			$prices = $this->productService->getPriceVariants(array_keys($variantsToAdd));
			/** @var RegularPrice $selectedPrice */
			$selectedPrice = reset($prices);

			$this->stockOperationManager->addVariant(
				$this->type,
				new StockOperationVariantData(
					$selectedPrice->getId(),
					$selectedPrice->getVariant()->getCodesString(),
					$selectedPrice->getVariant()->getVariantName(),
					1,
					$selectedPrice->getPurchasePriceWithoutVat()?:0,
					$selectedPrice->getPrice()?:0,
					$selectedPrice->getVariant()->getProduct()->getVat()
				)
			);

			$this['operationForm']['variantsToAdd']->setValue(null);
		} else {
			$this->stockOperationManager->updateVariants(
				$this->type,
				$this['operationForm']->getHttpData(Form::DATA_TEXT | Form::DATA_KEYS, 'variants-count[]'),
				$this['operationForm']->getHttpData(Form::DATA_TEXT | Form::DATA_KEYS, 'variants-purchasePrice[]'),
				$this['operationForm']->getHttpData(Form::DATA_TEXT | Form::DATA_KEYS, 'variants-price[]')
			);
		}
	}

	public function renderChange()
	{
		$this->template->form = $this['operationForm'];
		$this->template->actionTitle = $this->defaultPageTitles[$this->type];
		$this->template->variants = $this->stockOperationManager->getVariants($this->type);
		$this->template->showPrice = $this->type == StockOperationManager::OPERATION_TYPE_IN;
	}

	public function handleDownload($id)
	{
		$stockFile = $this->stockService->getStockFile($id);

		if (is_null($stockFile)) {
			$this->error('Požadovaný dokument nebyl nalezen.');
		}

		$filePath = $this->applicationConfigurationService->getStockFilesDirectoryPath() . $stockFile->getStock()->getId() . '/' . $stockFile->getFile();

		$this->sendPdfResponse($filePath, $stockFile->getFile());
	}

	public function handleChangeAlert($priceVariant, $stock)
	{
		$stock = $this->stockService->getStock($stock);

		$this->stockService->changeStockItemAlert($this->edited->getPriceVariant($priceVariant), $stock);

		$this->flashMessage("Upozornění bylo změněno.", 'info');

		if ($this->isAjax()) {
			$this->redrawControl('flashes');
			$this['priceVariantGrid']->reload();
		} else {
			$this->redirect('this');
		}

	}

	public function handleRemoveStockOperationLine($priceId)
	{
		try {
			$this->stockOperationManager->removeVariant($this->type, intval($priceId));
			$this->flashMessage('Položka byla odebrána', 'success');
		} catch (\Exception $e) {
			Debugger::log($e);
			$this->flashMessage('Položku se nepodařilo odebrat.', 'danger');
		}

		$this->redirect('this');
	}

	protected function createComponentOperationForm($name = null)
	{
		$form = $this->stockOperationFormFactory->create($this, $name);

		$form->onSuccess[] = function(Form $form) {
			try {
				$values = $form->getValues();

				if($form['save']->isSubmittedBy()) {
					$values = $form->getValues();
					$created = strlen($values->date) ? DateTime::createFromFormat('d. m. Y', $values->date) : null;
					$quantityChanges = $form->getHttpData(Form::DATA_TEXT | Form::DATA_KEYS, 'variants-count[]');
					$purchasePriceChanges = $form->getHttpData(Form::DATA_TEXT | Form::DATA_KEYS, 'variants-purchasePrice[]');
					$priceChanges = $form->getHttpData(Form::DATA_TEXT | Form::DATA_KEYS, 'variants-price[]');

					/** @var Stock $stock */
					$stock = $this->stockService->getOne($values->stock);
					$type = ($this->type == StockOperationManager::OPERATION_TYPE_IN) ? StockManager::CHANGE_ADD : StockManager::CHANGE_SUBTRACT;
					$changes = $this->stockManager->bulkStockStateUpdate($stock, $quantityChanges, $purchasePriceChanges, $priceChanges, $type);

					$this->stockManager->generateFile($changes, $this->type, $stock, $created);
					$this->stockOperationManager->reset($this->type);

					$this->redirect('archive', ['type' => $this->type]);
				}

				$this->stockOperationManager->setDate($this->type, $values->date);
				$this->stockOperationManager->setStock($this->type, $values->stock);
			} catch (NoResultException $e) {
				$form->addError('Některý z produktů nebyl nalezen. Opravte prosím svůj výběr.');
			}
		};

		$form->onError[] = function(Form $form){
			foreach ($form->getErrors() as $error) {
				$this->flashMessage($error, 'warning');
			}
		};

		$form->setDefaults([
			'date' => $this->stockOperationManager->getDate($this->type),
			'stock' => $this->stockOperationManager->getStock($this->type),
			'variantsToAdd' => null
		]);

		return $form;
	}

	protected function createComponentInventoryValuationForm()
	{
		$form = new Form();

		$form->addText('date');
		$form->addSubmit('generate');

		$form->onSuccess[] = function (Form $form) {
			$values = $form->getValues();
			/** @var DateTime $dateOfEvaluation */
			$dateOfEvaluation = strlen($values->date) ? DateTime::createFromFormat('d. m. Y', $values->date) : new DateTime();
			$fileName = 'oceneni-skladu-'.date('d-m-Y', $dateOfEvaluation->getTimestamp());
			$this->sendPdfResponse($this->stockService->evaluateInventory($fileName, $dateOfEvaluation), $fileName);
			$this->terminate();
		};

		return $form;
	}

	protected function createComponentStockGrid()
	{
		return $this->stockGridFactory->create();
	}

	protected function createComponentPriceVariantGrid($name = null)
	{
		return $this->priceVariantGrid->create($this, $name, $this->edited, $this->selectedStock);
	}

	protected function createComponentFilesGrid($name = null)
	{
		return $this->stockFileGridFactory->create($this, $name, $this->type);
	}
}