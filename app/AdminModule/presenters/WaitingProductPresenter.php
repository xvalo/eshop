<?php

namespace App\AdminModule\Presenters;
use App\Admin\Forms\WaitingProductForm;
use App\AdminModule\Grids\IWaitingProductsGridFactory;
use App\Product\Product;
use App\Product\Waiting\WaitingProductService;
use Tracy\Debugger;

class WaitingProductPresenter extends BasePresenter
{
	/**
	 * @var IWaitingProductsGridFactory @inject
	 */
	public $waitingProductsGrid;

	/**
	 * @var WaitingProductService @inject
	 */
	public $waitingProductsService;

	/**
	 * @var WaitingProductForm @inject
	 */
	public $waitingProductFormFactory;

	/**
	 * @var \App\Product\Waiting\Entity\Product
	 */
	private $processedProduct;

	public function actionCreate($id)
	{
		$this->processedProduct =  $this->waitingProductsService->getWaitingProduct( (int) $id );

		if ($this->processedProduct === null) {
			$this->error('Waiting product was not found');
		}
	}

	public function renderCreate()
	{
		$this->template->product = $this->processedProduct;
	}

	public function handleDelete($id)
	{
		try {
			$this->waitingProductsService->removeWaitingProduct( (int)$id );
			$this->flashMessage('Produkt byl z koše čekajících produktů smazán', 'success');
		} catch (\Exception $e) {
			Debugger::log($e);
			$this->flashMessage('Při mazání došlo k chybě.', 'danger');
		}

		if ($this->isAjax()) {
			$this['grid']->reload();
			$this->redrawFlashes();
		} else {
			$this->redirect('this');
		}
	}

	protected function createComponentGrid($name = null)
	{
		$grid = $this->waitingProductsGrid->create($this, $name);

		$grid->onProductSave[] = function(Product $product) {
			$this->payload->hardRedirect = $this->link(':Admin:Product:edit', ['id' => $product->getId()]);;
			$this->flashMessage('Nový produkt byl vytvořen.', 'success');
		};

		return $grid;
	}

	/**
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentForm()
	{
		return $this->waitingProductFormFactory->create(function(Product $product){
			$this->flashMessage('Nový produkt  byl úspěšně vytvořen', 'success');
			$this->redirect('Product:edit', ['id' => $product->getId()]);
		}, $this->processedProduct);
	}
}