<?php

namespace App\AdminModule\Presenters;

use App\Admin\Forms\SignFormFactory;
use App\User\AdminManager;
use Nette\Application\UI\Form;
use Nette\Utils\ArrayHash;

class SignPresenter extends BasePresenter
{
	/** @var SignFormFactory @inject */
	public $factory;

	/**
	 * @var AdminManager
	 * @inject
	 */
	public $manager;

	public function beforeRender()
	{
		parent::beforeRender();

		$this->setLayout('signLayout');
	}

	/**
	 * Sign-in form factory.
	 * @return Form
	 */
	protected function createComponentSignInForm()
	{
		$form = $this->factory->create();
		$manager = $this->manager;

		$form->onSuccess[] = function (Form $form, ArrayHash $values) use ($manager){
			try {
				$manager->authenticate([$values->username, $values->password]);
			} catch (\Nette\Security\AuthenticationException $exception) {
				$this->flashMessage('Přihlášení se nezdařilo. Zadejte správné přihlašovací údaje.');
				$this->redirect('this');
			}

			if ($values->remember) {
				$this->getUser()->setExpiration(new \DateTime('+ 120 minute'), FALSE);
			} else {
				$this->getUser()->setExpiration(new \DateTime('+ 2 week'), TRUE);
			}

			$form->getPresenter()->redirectOnLogin();
		};
		return $form;
	}


	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('You have been signed out.');
		$this->redirect('Sign:default');
	}
}