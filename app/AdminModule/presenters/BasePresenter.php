<?php
namespace App\AdminModule\Presenters;

use App\AdminModule\Components\IAdminMenuFactory;
use App\AdminModule\Components\IStockLimitAlertsControlFactory;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\User\AdminManager;
use App\Model\Services\ApplicationConfigurationService;
use Carrooi\Security\User\User;
use DK\Menu\Item;
use Nette;
use Nette\Application\Responses\FileResponse;
use Nette\Http\IResponse;
use Ublaboo\ImageStorage\ImageStoragePresenterTrait;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{
	use ImageStoragePresenterTrait;

	/** @persistent */
	public $backlink = '';

	/** @var IAdminMenuFactory @inject */
	public $adminMenuFactory;

	/** @var IStockLimitAlertsControlFactory @inject */
	public $stockLimitAlertFactory;

	/** @var ApplicationConfigurationService @inject */
	public $applicationConfigurationService;

	/** @var ApplicationSettingsManager @inject */
	public $applicationSettingsManager;

	protected $edited;

	public function startup()
	{
		parent::startup();

		if(!$this->getUser()->isLoggedIn() && $this->getPresenter()->getName() != "Admin:Sign")
		{
			$this->backlink = $this->storeRequest();
			$this->redirect("Sign:default");
		}

		if(!$this->getUser()->isAllowed($this->getPresenterName(), $this->getAction())){
			$this->error('Nemáte oprávnění pro tuto stránku nebo akci.', IResponse::S403_FORBIDDEN);
		}
	}

	protected function beforeRender()
	{
		parent::beforeRender();

		$this->template->webTitle = $this->applicationSettingsManager->getWebsiteTitle();

		if (!$this->isLinkCurrent('Sign:*')) {
			/** @var Item $menuItem */
			$menuItem = $this['menu']->getMenu()->getLastCurrentItem();

			$this->template->title = $menuItem->getParent() instanceof Item ? $menuItem->getParent()->getTitle() : $menuItem->getTitle();
			$this->template->subtitle = $menuItem->getParent() instanceof Item ? $menuItem->getTitle() : null;
		}
	}

	/**
	 * Redirects to backlink or homepage if backlink is not set.
	 */
	public function redirectOnLogin()
	{
		$this->restoreRequest($this->backlink);

		//if no backlink is set, redirect according to role and module config
		$this->redirect('Dashboard:');
	}

	/**
	 * User fo Admin module
	 * @return User
	 */
	public function getUser()
	{
		$user = parent::getUser();
		$user->getStorage()->setNamespace(AdminManager::USER_NAMESPACE);
		return $user;
	}

	/**
	 * @return \DK\Menu\UI\Control
	 */
	protected function createComponentMenu()
	{
		return $this->adminMenuFactory->create();
	}

	protected function createComponentStockLimitAlerts($name = null)
	{
		return $this->stockLimitAlertFactory->create($this, $name);
	}

	public function sendPdfResponse($pdfFile, $fileName)
	{
		$this->sendResponse(new FileResponse($pdfFile, $fileName.'.pdf', 'application/pdf'));
	}

	protected function redrawFlashes()
	{
		$this->redrawControl('flashes');
	}

	private function getPresenterName()
	{
		$presenterName = $this->getName();
		$data = explode(":", $presenterName);

		return Nette\Utils\Strings::lower($data[1]);
	}
}