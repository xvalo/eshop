<?php

namespace App\AdminModule\Presenters;

use App\Admin\Forms\SupplierForm;
use App\AdminModule\Grids\ISupplierGridFactory;
use App\Stock\Supplier\Entity\Supplier;
use App\Stock\Supplier\SupplierService;

class SupplierPresenter extends BasePresenter
{
	/** @var ISupplierGridFactory @inject */
	public $supplierGridFactory;

	/** @var SupplierForm @inject */
	public $supplierFormFactory;

	/** @var SupplierService @inject */
	public $supplierService;

	public function actionEdit($id)
	{
		$this->edited = $this->supplierService->getSupplier((int)$id);

		if ($this->edited === null || $this->edited->isDeleted()) {
			$this->error('Požadovaný dodavatel neexistuje nebo je smazán.', IResponse::S400_BAD_REQUEST);
		}
	}

	public function renderEdit()
	{
		$this->template->supplier = $this->edited;
	}

	public function actionNew()
	{
		$this->setView('edit');
	}

	public function handleDelete($id)
	{
		$this->supplierService->removeSupplierById((int) $id);

		$this->flashMessage('Dodavatel byl úspěšně smazán', 'success');
		$this->redirect('Supplier:default');
	}

	protected function createComponentSupplierGrid()
	{
		return $this->supplierGridFactory->create();
	}

	/**
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentForm()
	{
		return $this->supplierFormFactory->create(function(Supplier $supplier){
			$this->flashMessage('Dodavatel byl úspěšně uložen', 'success');
			$this->redirect('Supplier:edit', ['id' => $supplier->getId()]);
		}, function(string $message){
			$this->flashMessage($message, 'error');
			$this->redirect('this');
		}, $this->edited);
	}
}