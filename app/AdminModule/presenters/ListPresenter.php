<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Grids\IListGridFactory;
use App\Lists\ListsService;
use App\Lists\ListsTypes;
use Nette\Application\BadRequestException;

class ListPresenter extends BasePresenter
{

	/** @var IListGridFactory @inject */
	public $gridFactory;

	/** @var ListsService @inject */
	public $listService;

	/**
	 * @var string
	 */
	private $type;

	public function actionDefault($type)
	{
		$this->type = $type;
	}

	public function handleDelete($id)
	{
		try {
			switch ($this->type) {
				case ListsTypes::LIST_PRODUCER:
					$item = $this->listService->getProducer((int) $id);
					break;
				case ListsTypes::LIST_STOCK:
					$item = $this->listService->getStock((int) $id);
					break;
				default:
					throw new BadRequestException('Unknown type');
			}
			$this->listService->removeItem($item);
			$this->flashMessage('Položka byla odstraněna.', 'success');
		} catch (\Exception $e) {
			$this->flashMessage('Položku se nepodařilo smazat, došlo k nečekané chybě.', 'danger');
		}

		if ($this->isAjax()) {
			$this['grid']->reload();
			$this->redrawFlashes();
		} else {
			$this->redirect('this');
		}
	}

	protected function createComponentGrid($name = null)
	{
		return $this->gridFactory->create($this, $name, $this->type);
	}
}