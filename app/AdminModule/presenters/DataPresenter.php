<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Grids\IStockUpdateHistoryGridFactory;
use App\AdminModule\Grids\IWatchDogGridFactory;

class DataPresenter extends BasePresenter
{
    /** @var IStockUpdateHistoryGridFactory @inject */
    public $stockUpdateHistoryGridFactory;

    /** @var IWatchDogGridFactory @inject */
    public $watchDogGridFactory;

	/**
	 * @param null $name
	 * @return \App\AdminModule\Grids\StockUpdateHistoryGrid
	 */
    protected function createComponentStockUpdateHistoryGrid($name = null)
    {
        return $this->stockUpdateHistoryGridFactory->create($this, $name);
    }

    protected function createComponentWatchDogGrid($name = null)
    {
        return $this->watchDogGridFactory->create($this, $name);
    }
}