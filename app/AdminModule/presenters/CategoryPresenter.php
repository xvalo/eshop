<?php
namespace App\AdminModule\Presenters;

use App\AdminModule\Components\IAttributeAttachControlFactory;
use App\AdminModule\Components\ICategoryAttributeDetailControlFactory;
use App\AdminModule\Forms\CategoryForm;
use App\AdminModule\Forms\ICategoryFilterForm;
use App\AdminModule\Forms\IDeliveryToCategoryFormFactory;
use App\AdminModule\Grids\ICategoryAttributeGridFactory;
use App\AdminModule\Grids\ICategoryGridFactory;
use App\Attribute\AttributeFacade;
use App\Attribute\Core\Attribute;
use App\Category\CategoryFacade;
use App\Category\Entity\Category;
use App\Category\CategoryService;
use App\Filter\FilterService;
use App\InvalidArgumentException;
use App\Order\Delivery\DeliveryService;
use Nette\Application\UI\Form;

/**
 * @property Category $edited
 */
class CategoryPresenter extends BasePresenter
{
	/** @var CategoryService @inject */
	public $categoryService;

	/** @var ICategoryGridFactory @inject */
	public $categoryGridFactory;

	/** @var CategoryForm @inject */
	public $categoryFormFactory;

	/** @var ICategoryFilterForm @inject */
	public $categoryFilterForm;

	/** @var ICategoryAttributeDetailControlFactory @inject */
	public $attributesControlFactory;

	/** @var ICategoryAttributeGridFactory @inject */
	public $categoryAttributeGridFactory;

	/** @var IAttributeAttachControlFactory @inject */
	public $attributeAttachControlFactory;

	/** @var IDeliveryToCategoryFormFactory @inject */
	public $deliveryToCategoryFormFactory;

	/** @var CategoryFacade @inject */
	public $categoryFacade;

	/** @var DeliveryService @inject */
	public $deliveryService;

	/** @var AttributeFacade @inject */
	public $attributeFacade;

	/** @var FilterService @inject */
	public $filterService;

	/** @var Attribute */
	private $attribute;

	public function beforeRender()
	{
		parent::beforeRender();

		$this->template->isEdited = isset($this->edited);
		$this->template->action = $this->getAction();
	}

	public function actionEdit($id, $attributeId = null)
	{
		$this->edited = $this->categoryService->getCategory($id);

		/*
		if ($this->edited->getParent() && !$this->edited->getParent()->isActive()) {
			$this['form']['active']->setDisabled(true);
		}
		*/

		if (is_numeric($attributeId)) {
			try {
				$this->attribute = $this->attributeFacade->getAttribute(intval($attributeId));
			} catch (InvalidArgumentException $e) {
				$this->flashMessage('Požadovaná vlastnost nebyla u kategorie nalezena', 'danger');
				$this->redirect('edit', ['id' => $id]);
			}
		}
	}

	public function renderEdit()
	{
		$this->template->category = $this->edited;
	}

	public function actionNew()
	{
		$this->setView('edit');
	}

	public function actionAttributes($id, $attributeId = null)
	{
		$this->template->category = $this->edited = $this->categoryService->getCategory($id);

		if (is_numeric($attributeId)) {
			try {
				$this->attribute = $this->attributeFacade->getAttribute(intval($attributeId));
			} catch (InvalidArgumentException $e) {
				$this->flashMessage('Požadovaná vlastnost nebyla u kategorie nalezena', 'danger');
				$this->redirect('edit', ['id' => $id]);
			}
		}
	}

	public function actionDelivery($id)
	{
		$this->template->category = $this->edited = $this->categoryService->getCategory($id);
	}

	public function actionFilters($id)
	{
		$this->template->category = $this->edited = $this->categoryService->getCategory($id);
	}

	public function handleDelete($id)
	{
		$this->categoryService->deleteCategory($id);
		if ($this->isAjax()) {
			$this['grid']->redrawControl();
		} else {
			$this->redirect('default');
		}
	}

	public function handleSort($item_id, $prev_id, $next_id, $parent_id)
	{
		$this->categoryService->changeCategoriesOrder($item_id, $prev_id, $next_id, $parent_id);

		$this['grid']->redrawControl();
	}

	protected function createComponentForm()
	{
		return $this->categoryFormFactory->create(function(Category $category){
			$this->redirect('edit', $category->getId());
		}, $this->edited);
	}

	/**
	 * @param null $name
	 * @return \App\AdminModule\Grids\CategoryGrid
	 */
	protected function createComponentGrid($name = null)
	{
		return $this->categoryGridFactory->create($this, $name);
	}

	protected function createComponentAttributeDetailControl()
	{
		return $this->attributesControlFactory->create($this->edited, $this->attribute);
	}

	protected function createComponentAttributesGrid()
	{
		return $this->categoryAttributeGridFactory->create($this->edited);
	}

	protected function createComponentAttributeAttach()
	{
		return $this->attributeAttachControlFactory->create($this->edited);
	}

	protected function createComponentDeliveryToCategoryForm()
	{
		$form = $this->deliveryToCategoryFormFactory->create();

		$form->onSuccess[] = function (Form $form) {
			try {
				$values = $form->getValues(true);
				$this->categoryService->setDeliveriesAvailableInCategory($values['delivery'], $this->edited);
				$this->flashMessage('Nastavení bylo uloženo', 'success');
			} catch (\Exception $e) {
				$this->flashMessage('Nastala nečekaná chyba a změny se nepodařilo uložit. Kontaktujte prosím podporu.', 'danger');
			}

			$this->redirect('this');
		};

		$form->setDefaults([
			'delivery' => $this->deliveryService->getCategoryAllowedDeliveryIds($this->edited)
		]);

		return $form;
	}

	protected function createComponentFilterForm()
	{
		$form = $this->categoryFilterForm->create();

		$form->onSuccess[] = function (Form $form) {
			try {
				$values = $form->getValues(true);
				$this->filterService->setFiltersToCategory($values['filters'], $this->edited);
				$this->flashMessage('Nastavení bylo uloženo', 'success');
			} catch (\Exception $e) {
				$this->flashMessage('Nastala nečekaná chyba a změny se nepodařilo uložit. Kontaktujte prosím podporu.', 'danger');
			}

			$this->redirect('this');
		};

		$form->setDefaults([
			'filters' => $this->filterService->getCategoryFiltersIds($this->edited)
		]);

		return $form;
	}
}