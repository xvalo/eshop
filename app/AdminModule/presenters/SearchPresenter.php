<?php

namespace App\AdminModule\Presenters;


use App\Category\CategoryService;
use App\Order\AdminOrderService;
use App\Product\ProductService;

class SearchPresenter extends BasePresenter
{
    /** @var AdminOrderService @inject */
    public $adminOrderService;

    /** @var ProductService @inject */
    public $productService;

    /** @var CategoryService @inject */
    public $categoryService;


    /** @var string */
    private $query;
    
    public function actionDefault($q)
    {
        $this->query = $q;
    }

    public function renderDefault()
    {
        $this->template->products = $this->productService->searchProductsAndVariants($this->query, false);
        $this->template->orders = $this->adminOrderService->searchOrders($this->query);
        $this->template->categories = $this->categoryService->searchCategories($this->query);
    }
}