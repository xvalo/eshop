<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Forms\IPageFormFactory;
use App\AdminModule\Grids\IPageGridFactory;
use App\Page\Entity\Page;
use App\Page\PageService;
use Nette\Application\UI\Form;

/**
 * Class PagePresenter
 * @package App\AdminModule\Presenters
 * @property Page $edited
 */
class PagePresenter extends BasePresenter
{

	/** @var PageService @inject */
	public $pageService;

	/** @var IPageGridFactory @inject */
	public $pageGridFactory;

	/** @var IPageFormFactory @inject */
	public $pageFormFactory;

	public function actionEdit($id)
	{
		try {
			$this->edited = $this->pageService->getPage((int) $id);
		} catch (\Exception $e) {
			$this->error();
		}
	}

	protected function createComponentGrid($name = null)
	{
		return $this->pageGridFactory->create($this, $name);
	}

	protected function createComponentForm($name = null)
	{
		$form = $this->pageFormFactory->create($this, $name);

		$form->onSuccess[] = function(Form $form){
			try {
				$this->pageService->updatePage($this->edited, $form->getValues(true));
				$this->flashMessage('Stránka byla uložena.', 'success');
			} catch (\Exception $e) {
				$this->flashMessage('Při ukládání došlo k chybě.', 'danger');
			}
			$this->redirect('this');
		};

		$form->setDefaults([
			'title' => $this->edited->getTitle(),
			'url' => $this->edited->getUrl(),
			'seoTitle' => $this->edited->getSeoTitle(),
			'seoKeywords' => $this->edited->getSeoKeywords(),
			'content' => $this->edited->getContent(),
			'active' => $this->edited->isActive(),
			'inMenu' => $this->edited->isInMenu(),
			'inFooter' => $this->edited->isInFooter()
		]);

		return $form;
	}

}