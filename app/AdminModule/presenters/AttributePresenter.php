<?php

namespace App\AdminModule\Presenters;

use App\AdminModule\Components\IAttributeDetailControlFactory;
use App\AdminModule\Forms\AttributeForm;
use App\AdminModule\Grids\IAttributesListGridFactory;
use App\AdminModule\Grids\IAttributeVariantGridFactory;
use App\Attribute\AttributeFacade;
use App\Attribute\AttributeService;
use App\Attribute\Core\Attribute;
use Nette\Http\IResponse;

/**
 * @property Attribute $edited
 */
class AttributePresenter extends BasePresenter
{
	/** @var IAttributesListGridFactory @inject */
	public $attributesListGridFactory;

	/** @var IAttributeDetailControlFactory @inject */
	public $attributeDetailControlFactory;

	/** @var IAttributeVariantGridFactory @inject */
	public $attributeVariantsGridFactory;

	/** @var AttributeForm @inject */
	public $attributeFormFactory;

	/** @var AttributeService @inject */
	public $attributeService;

	/** @var AttributeFacade @inject */
	public $attributeFacade;

	/** @var Attribute */
	private $attribute;

	public function beforeRender()
	{
		parent::beforeRender();

		$this->template->isEdited = isset($this->edited);
		$this->template->action = $this->getAction();
	}

	public function actionEdit($id)
	{
		$this->edited = $this->attributeService->getAttribute((int)$id);

		if ($this->edited === null || $this->edited->isDeleted()) {
			$this->error('Požadovaná vlastnost neexistuje nebo je smazána.', IResponse::S400_BAD_REQUEST);
		}
	}

	public function renderEdit()
	{
		$this->template->attribute = $this->edited;
	}

	public function actionNew()
	{
		$this->setView('edit');
	}

	public function actionVariants($id)
	{
		$this->edited = $this->attributeService->getAttribute((int)$id);

		if ($this->edited->hasChildren()) {
			$this->flashMessage('Tato vlastnost má podřazené vlastnosti. U těchto vlastností nelze vytvářet varianty.', 'warning');
			$this->redirect('default');
		}
	}

	public function renderVariants()
	{
		$this->template->attribute = $this->edited;
	}

	public function actionAttachedTo($id)
	{
		$this->edited = $this->attributeService->getAttribute((int)$id);
	}

	public function renderAttachedTo()
	{
		$this->template->attribute = $this->edited;
		$this->template->categories = $this->attributeFacade->getCategoriesAttributeBelongsTo($this->edited);
		$this->template->products = $this->attributeFacade->getProductsAttributeBelongsTo($this->edited);

	}

	public function handleRemoveAttribute($id)
	{
		$attribute = $this->attributeService->getAttribute((int)$id);
		$this->attributeService->removeAttribute($attribute);

		$this->flashMessage('Vlastnost byla úspěšně odstraněna', 'success');

		if ($this->isAjax()) {
			$this['grid']->reload();
		} else {
			$this->redirect('default');
		}
	}

	public function handleRemoveAttributeVariant($variantId)
	{
		$attributeVariant = $this->attributeService->getAttributeVariant((int) $variantId);
		$this->attributeService->removeAttributeVariant($attributeVariant);
		$this->flashMessage('Varianta vlastnosti byla úspěšně odstraněna', 'success');

		if ($this->isAjax()) {
			$this['grid']->reload();
		} else {
			$this->redirect('default');
		}
	}

	/**
	 * @return \App\AdminModule\Grids\AttributesListGrid
	 */
	protected function createComponentGrid()
	{
		return $this->attributesListGridFactory->create();
	}

	protected function createComponentVariantsGrid()
	{
		return $this->attributeVariantsGridFactory->create($this->edited);
	}

	protected function createComponentAttributeDetail()
	{
		return $this->attributeDetailControlFactory->create($this->edited);
	}

	/**
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentForm()
	{
		return $this->attributeFormFactory->create(function(Attribute $attribute){
			$this->flashMessage('Vlastnost byla úspěšně uložena', 'success');
			$this->redirect('Attribute:edit', ['id' => $attribute->getId()]);
		}, $this->edited);
	}
}