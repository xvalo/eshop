<?php

namespace App\AdminModule\Components;

use App\AdminModule\Forms\ICustomerGeneralMailForm;
use App\Classes\Order\OrderDeliverability;
use App\Mailing\Factories\EmailBodyFactory;
use App\Model\Services\ApplicationConfigurationService;
use App\Order\AdminOrderService;
use App\Order\MailMessage;
use App\Order\Order;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use PhpMimeMailParser\Parser;
use Tracy\Debugger;

class OrderEmailsControl extends Control
{
	/** @var ICustomerGeneralMailForm */
	private $customerGeneralMailFormFactory;

	/** @var AdminOrderService */
	private $adminOrderService;

	/** @var Order */
	private $order;

	/** @var ApplicationConfigurationService */
	private $applicationConfigurationService;

	private $templatePath = '/list.latte';
	/**
	 * @var EmailBodyFactory
	 */
	private $emailBodyFactory;

	public function __construct(
		Order $order,
		IContainer $parent = null,
		$name = null,
		AdminOrderService $adminOrderService,
		ICustomerGeneralMailForm $customerGeneralMailFormFactory,
		ApplicationConfigurationService $applicationConfigurationService,
		EmailBodyFactory $emailBodyFactory
	){
		parent::__construct($parent, $name);

		$this->order = $order;
		$this->customerGeneralMailFormFactory = $customerGeneralMailFormFactory;
		$this->adminOrderService = $adminOrderService;
		$this->applicationConfigurationService = $applicationConfigurationService;
		$this->emailBodyFactory = $emailBodyFactory;
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . $this->templatePath);

		$this->template->hasUnavailableProducts = count($this->order->getUnavailableProducts()) > 0;
		$this->template->orderIsCanceled = $this->order->isCanceled();
		$this->template->isOrderInvoicable = OrderDeliverability::isOrderByDeliveryInvoicable($this->order->getPaymentType());
		$this->template->isOrderPaid = $this->order->isPayed();
		$this->template->orderNo = $this->order->getOrderNo();
		$this->template->emails = $this->processEmails($this->order->getMailMessages());

		$this->template->render();
	}

	public function handleSendOrderRejectedMail()
	{
		try {
			$this->adminOrderService->sendOrderRejectedMail($this->order);
			$this->getPresenter()->flashMessage('Email byl úspěšně odeslán.', 'success');
		} catch (\Exception $e) {
			Debugger::log($e);
			$this->getPresenter()->flashMessage('Při odesílání emailu došlo k chybě.', 'danger');
		}

		$this->redirect('this');
	}

	public function handleSendOrderNotPaidMail()
	{
		try {
			$this->adminOrderService->sendOrderNotPaidMail($this->order);
			$this->getPresenter()->flashMessage('Email byl úspěšně odeslán.', 'success');
		} catch (\Exception $e) {
			Debugger::log($e);
			$this->getPresenter()->flashMessage('Při odesílání emailu došlo k chybě.', 'danger');
		}

		$this->redirect('this');
	}

	protected function createComponentUnavailableProductsMailForm($name = null)
	{
		$form = $this->customerGeneralMailFormFactory->create($this, $name);

		$form->onSuccess[] = function (Form $form) {
			try {
				$values = $form->getValues();
				if ($form['submitUnavailableProduct']->isSubmittedBy()) {
					$this->adminOrderService->sendCustomerGeneralMail($this->order, $values->body);
					$this->flashMessage('Zpráva byla odeslána.', 'success');
				}
			} catch (\Exception $e) {
				$this->flashMessage('Při odesílání došlo k chybě.', 'error');
			}

			$this->redirect('this');
		};

		$form->setDefaults([
			'body' => $this->emailBodyFactory->getUnavailableProductsMailBody($this->order->getUnavailableProducts())
		]);

		return $form;
	}

	protected function createComponentCustomerGeneralMailForm($name = null)
	{
		$form = $this->customerGeneralMailFormFactory->create($this, $name);

		$form->onSuccess[] = function (Form $form) {
			try {
				$values = $form->getValues();

				if ($form['submitGeneral']->isSubmittedBy()) {
					$this->adminOrderService->sendCustomerGeneralMail($this->order, $values->body);
					$this->flashMessage('Zpráva byla odeslána.', 'success');
				}
			} catch (\Exception $e) {
				Debugger::log($e);
				$this->flashMessage('Při odesílání došlo k chybě.', 'error');
			}

			$this->redirect('this');
		};

		$form->setDefaults([
			'body' => $this->emailBodyFactory->getGeneralMailBody()
		]);

		return $form;
	}

	private function processEmails($emails)
	{
		$processedEmails = [];

		$parser = new Parser();

		/** @var MailMessage $email */
		foreach ($emails as $email) {
			$parser->setPath($this->applicationConfigurationService->getMailsLogDirectory() . $email->getPath());

			$processedEmails[] = [
				'id' => $email->getId(),
				'typeLabel' => $email->getTypeLabel(),
				'created' => $email->getCreated(),
				'author' => $email->getAuthor()->getName() . ' ' . $email->getAuthor()->getLastName(),
				'message' => $this->getEmailBody($parser->getMessageBody('html'))
			];
		}

		return $processedEmails;
	}

	private function getEmailBody($body)
	{
		$dom = new \DOMDocument();
		$dom->loadHTML($body);
		$finder = new \DOMXPath($dom);
		$message = $finder->query('//table[@class="main"]')->item(0);
		$bodyHtml = $dom->saveHTML($message);
		$bodyHtml = preg_replace('/(class="[a-zA-Z0-9]*")/', '', $bodyHtml);

		return $bodyHtml;
	}
}