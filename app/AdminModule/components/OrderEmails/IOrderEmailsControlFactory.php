<?php

namespace App\AdminModule\Components;

use App\Order\Order;
use Nette\ComponentModel\IContainer;

interface IOrderEmailsControlFactory
{
	/**
	 * @param Order $order
	 * @param IContainer $parent
	 * @param $name
	 * @return OrderEmailsControl
	 */
	function create(Order $order, IContainer $parent, $name);
}