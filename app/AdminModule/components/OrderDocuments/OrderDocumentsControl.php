<?php

namespace App\AdminModule\Components;

use App\Classes\Order\OrderDeliverability;
use App\Classes\Order\OrderDocumentService;
use App\File\OrderDocument;
use App\Order\AdminOrderService;
use App\Order\Order;
use App\Order\OrderLine\OrderLine;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;

class OrderDocumentsControl extends Control
{
	/** @var AdminOrderService */
	private $adminOrderService;

	/** @var OrderDocumentService */
	private $orderDocumentService;

	/** @var Order */
	private $order;

	private $templatePath = '/list.latte';

	/**
	 * OrderDocumentsControl constructor.
	 * @param Order $order
	 * @param IContainer|null $parent
	 * @param null $name
	 * @param AdminOrderService $adminOrderService
	 * @param OrderDocumentService $orderDocumentService
	 */
	public function __construct(
		Order $order,
		IContainer $parent = null,
		$name = null,
		AdminOrderService $adminOrderService,
		OrderDocumentService $orderDocumentService
	){
		parent::__construct($parent, $name);

		$this->order = $order;
		$this->adminOrderService = $adminOrderService;
		$this->orderDocumentService = $orderDocumentService;
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . $this->templatePath);

		$this->template->documents = $this->order->getDocuments();
		$this->template->order = $this->order;
		$this->template->canGenerateInvoice = $this->canGenerateInvoice();
		$this->template->canGenerateBill = $this->canGenerateBill();
		$this->template->canGenerateCancelDocument = $this->canGenerateCancelDocument();
		$this->template->canBeReceiptRemoved = $this->canBeReceiptRemoved();
		$this->template->canGenerateInvoiceCorrectionDocument = $this->canGenerateInvoiceCorrectionDocument();

		$this->template->render();
	}

	public function handleGenerateInvoicePdf()
	{
		$this->orderDocumentService->createInvoiceDocument($this->order);
		$this->flashMessage('Dokument byl vygenerován.', 'success');
		$this->redirect('this');
	}

	public function handleGenerateInvoiceCancellationPdf()
	{
		$this->orderDocumentService->createCancelDocument($this->order);
		$this->flashMessage('Dokument byl vygenerován.', 'success');
		$this->redirect('this');
	}

	public function handleGenerateBillPdf()
	{
		$this->orderDocumentService->createBillDocument($this->order);
		$this->flashMessage('Dokument byl vygenerován.', 'success');
		$this->redirect('this');
	}

	public function handleGenerateOrderProductsListPdf()
	{
		$this->orderDocumentService->createProductsListDocument($this->order);
		$this->flashMessage('Dokument byl vygenerován.', 'success');
		$this->redirect('this');
	}

	public function handleDownloadDocument($documentId)
	{

		/** @var OrderDocument $document */
		$document = $this->order->getOneDocument($documentId);

		if (!$document || $document->getOrder()->getId() !== $this->order->getId()) {
			$this->flashMessage('Požadovaný dokument nebyl nalezen', 'warning');
			$this->redirect('this');
		}

		$this->presenter->sendPdfResponse($document->getPath(), $document->getNumber());
	}

	public function handleRemoveReceiptFile()
	{
		$this->adminOrderService->removeReceiptFile($this->order);
		$this->flashMessage('Doklad byl smazan.', 'success');
		$this->redirect('this');
	}

	protected function createComponentOrderVariantsCorrectionForm($name = null)
	{
		$form = new Form($this, $name);

		$orderLines = $form->addContainer('orderLines');

		/** @var OrderLine $orderLine */
		foreach ($this->order->getOrderLines() as $orderLine) {
			$label = $orderLine . ' ['.$orderLine->getCount().' ks]';
			$variantCorrection = $orderLines->addContainer($orderLine->getId());
			$variantCorrection->addInteger('toReturn', $label)
					->setAttribute('min', 0)
					->setAttribute('max', $orderLine->getCount() > 0 ?: 0)
					->setDefaultValue(0);

		}

		$form->addSubmit('save');

		$form->onSubmit[] = function(Form $form){
			$values = $form->getValues(true);
			$linesChanged = $this->adminOrderService->returnOrderLines($this->order, $values['orderLines']);
			$this->orderDocumentService->createInvoiceCorrectionDocument($this->order, $linesChanged);

			$this->flashMessage('Opravný daňový doklad byl vygenerován.', 'success');
			$this->redirect('this');
		};

		return $form;
	}

	private function canGenerateInvoice()
	{
		return OrderDeliverability::isOrderByDeliveryInvoicable($this->order->getPaymentType())
				&& !$this->order->isCanceled()
				&& !$this->order->getReceipt();
	}

	private function canGenerateBill()
	{
		return OrderDeliverability::isOrderByDeliveryBillable($this->order->getPaymentType())
				&& !$this->order->isCanceled()
				&& $this->order->getPayment()
				&& !$this->order->getReceipt();
	}

	private function canGenerateCancelDocument()
	{
		return OrderDeliverability::isOrderByDeliveryInvoicable($this->order->getPaymentType())
				&& $this->order->isCanceled()
				&& $this->order->getReceipt()
				&& !$this->order->getCancelReceipt();
	}

	private function canGenerateInvoiceCorrectionDocument()
	{
		return OrderDeliverability::isOrderByDeliveryInvoicable($this->order->getPaymentType())
				&& !$this->order->isCanceled()
				&& $this->order->getReceipt();
	}

	private function canBeReceiptRemoved()
	{
		return !$this->order->isCanceled()
				//&& !is_null($this->order->getDelivery()) && $this->order->isCreatedToday()
				&& $this->order->getReceipt();
	}
}