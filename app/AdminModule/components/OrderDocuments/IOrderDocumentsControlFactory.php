<?php

namespace App\AdminModule\Components;

use App\Order\Order;
use Nette\ComponentModel\IContainer;

interface IOrderDocumentsControlFactory
{
	/**
	 * @param Order $order
	 * @param IContainer $parent
	 * @param $name
	 * @return OrderDocumentsControl
	 */
	function create(Order $order, IContainer $parent, $name);
}