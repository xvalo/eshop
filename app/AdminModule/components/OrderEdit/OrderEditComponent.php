<?php


namespace App\AdminModule\Components;

use App\AdminModule\Forms\IOrderFormFactory;
use App\Classes\Order\AdminOrder\Cart\Cart;
use App\Classes\Order\AdminOrder\Cart\Item;
use App\Classes\Order\Creation\Factory\OrderPartsFactory;
use App\Classes\Order\Creation\OrderCreationService;
use App\Classes\Order\Creation\ValueObject\LyonessCardData;
use App\Customer\Customer;
use App\Customer\CustomerService;
use App\Order\Delivery\DeliveryService;
use App\InvalidArgumentException;
use App\Order\AdminOrderService;
use App\Order\Discount\Discount;
use App\Order\Order;
use App\Order\OrderService;
use App\Order\Payment\PaymentTypeFacade;
use App\Product\VariantRepository;
use App\Utils\Vat;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use App\Customer\DeliveryAddress;
use App\Customer\InvoiceInformation;
use Nette\Http\SessionSection;

class OrderEditComponent extends Control
{
	const ADMIN_ORDER_CART_SESSION = 'admin_order_cart';

	/** @var Order */
	private $order;

	/** @var Customer */
	private $customer;

	/**
	 * @var IProductSelectionControlFactory
	 */
	private $productSelectionControlFactory;

	/**
	 * @var IOrderFormFactory
	 */
	private $orderFormFactory;
	/**
	 * @var AdminOrderService
	 */
	private $adminOrderService;
	/**
	 * @var VariantRepository
	 */
	private $variantRepository;
	/**
	 * @var OrderService
	 */
	private $orderService;

	/** @var Cart */
	private $cart;
	/**
	 * @var CustomerService
	 */
	private $customerService;
	/**
	 * @var DeliveryService
	 */
	private $deliveryService;
	/**
	 * @var OrderPartsFactory
	 */
	private $orderPartsFactory;
	/**
	 * @var OrderCreationService
	 */
	private $orderCreationService;
	/**
	 * @var IOrderLinesControlFactory
	 */
	private $orderLinesControlFactory;
	/**
	 * @var PaymentTypeFacade
	 */
	private $paymentTypeFacade;

	public function __construct(
		Order $order = null,
		IProductSelectionControlFactory $productSelectionControlFactory,
		IOrderFormFactory $orderFormFactory,
		AdminOrderService $adminOrderService,
		OrderService $orderService,
		VariantRepository $variantRepository,
		CustomerService $customerService,
		DeliveryService $deliveryService,
		OrderPartsFactory $orderPartsFactory,
		OrderCreationService $orderCreationService,
		IOrderLinesControlFactory $orderLinesControlFactory,
		PaymentTypeFacade $paymentTypeFacade
	){
		parent::__construct();

		$this->productSelectionControlFactory = $productSelectionControlFactory;
		$this->orderFormFactory = $orderFormFactory;
		$this->adminOrderService = $adminOrderService;
		$this->variantRepository = $variantRepository;
		$this->order = $order;
		$this->orderService = $orderService;
		$this->customerService = $customerService;
		$this->deliveryService = $deliveryService;
		$this->orderPartsFactory = $orderPartsFactory;
		$this->orderCreationService = $orderCreationService;
		$this->orderLinesControlFactory = $orderLinesControlFactory;
		$this->paymentTypeFacade = $paymentTypeFacade;
	}

	public function attached($presenter)
	{
		parent::attached($presenter);

		/** @var SessionSection $sessionSection */
		$sessionSection = $presenter->getSession(self::ADMIN_ORDER_CART_SESSION);
		$sessionSection->setExpiration('+30 minutes');

		$this->cart = new Cart($sessionSection);
		$this->cart->prepare();
	}

	public function render()
	{
		$this->template->setFile(__DIR__.'/orderForm.latte');

		$this->template->isEdited = $this->order !== null;
		$this->template->items = $this->cart->getItems();
		$this->template->extraItems = $this->cart->getExtraItems();

		$totalPrice = (isset($this->order) ? $this->order->getTotalPrice() : 0) + $this->cart->getTotal();
		if ($this->order !== null && $this->order->getDiscount() > 0 && $this->cart->getDiscount() > 0) {
			$totalPrice += $this->order->getDiscount();
		}

		$this->template->totalPrice = $totalPrice;
		$this->template->currentDiscount = $this->order ? $this->order->getDiscount() : 0;
		$this->template->discount = $this->cart->getDiscount();

		$this->template->order = $this->order;

		$this->template->getLatte()->addProvider('formsStack', [$this['form']]);
		$this->template->render();
	}

	public function handleAddItem($id)
	{
		$id = (int) $id;
		$price = $this->variantRepository->findPrice($id);
		$this->cart->addRegularItem($id, $price->getPrice())
			->setName($price->getPriceName())
			->setCodes($price->getVariant()->getCodesString());

		$this->redrawControl('order-selected-items');
		$this->redrawControl('order-end');
	}

	public function handleRemoveItem($key)
	{
		$this->cart->delete(Item::REGULAR, $key);

		$this->redrawControl('order-selected-items');
		$this->redrawControl('order-end');
	}

	public function handleRemoveExtraItem($key)
	{
		$this->cart->delete(Item::EXTRA, $key);

		$this->redrawControl('order-selected-items');
		$this->redrawControl('order-extra-items');
		$this->redrawControl('order-end');
	}

	public function handleSetDiscount($discount)
	{
		$this->cart->setDiscount((float) $discount);

		$this->redrawControl('order-end');
	}

	public function handleChangeQuantity($key, $quantity)
	{
		$this->cart->update(Item::REGULAR, $key, (int) $quantity);

		$this->redrawControl('order-selected-items');
		$this->redrawControl('order-end');
	}

	public function handleLoadCustomerInfo($customer)
	{
		list($type, $id) = explode('-', $customer);

		if ($type == 'C') {
			$this->customer = $this->customerService->getOne($id);
		} elseif ($type == 'O') {
			/** @var Order $order */
			$order = $this->adminOrderService->getOne($id);
			$contactInformation = $order->getContactInformation();
			$deliveryAddress = new DeliveryAddress(
				$order->getDeliveryAddress()->getName(),
				$order->getDeliveryAddress()->getLastName(),
				$order->getDeliveryAddress()->getCompany(),
				$order->getDeliveryAddress()->getStreet(),
				$order->getDeliveryAddress()->getCity(),
				$order->getDeliveryAddress()->getZip(),
				$order->getDeliveryAddress()->getCountry()
			);
			$invoiceInformation = new InvoiceInformation(
				$contactInformation->getName(),
				$contactInformation->getLastName(),
				$order->getInvoiceInformation()->getCompany(),
				$order->getInvoiceInformation()->getIco(),
				$order->getInvoiceInformation()->getDic(),
				$order->getInvoiceInformation()->getStreet(),
				$order->getInvoiceInformation()->getCity(),
				$order->getInvoiceInformation()->getZip(),
				$order->getInvoiceInformation()->getCountry()
			);
			$this->customer = new Customer(
				$contactInformation->getEmail(),
				'',
				$contactInformation->getName(),
				$contactInformation->getLastName(),
				$contactInformation->getPhone(),
				$invoiceInformation,
				$deliveryAddress
			);

			$this->customer->setLyoneesCardId($order->getLyonessCardId());
			$this->customer->setLyoneesCardEan($order->getLyonessCardEan());
		} else {
			throw new InvalidArgumentException('Wrong type and ID');
		}

		$this->redrawControl('order-customer');
	}

	protected function createComponentForm()
	{
		$form = $this->orderFormFactory->create();

		$form->onSuccess[] = function(Form $form){
			if ($form['extraItem']['addItem']->isSubmittedBy()) {
				$data = $form->getValues(true);
				$extraItem = $data['extraItem'];

				$this->cart->addExtraItem((float)$extraItem['unitPrice'], $extraItem['count'])
					->setName($extraItem['description'])
					->setVatRate($extraItem['vat']);

				$form['extraItem']->setDefaults([
					'description' => null,
					'unitPrice' => null,
					'count' => null,
					'vat' => Vat::TYPE_21
				]);

				if ($this->presenter->isAjax()) {
					$this->presenter->payload->extraItemAdded = true;
					$this->redrawControl('order-selected-items');
					$this->redrawControl('order-extra-items');
					$this->redrawControl('order-end');
				}
			} else {
				$formData = $form->getValues(true);
				$this->saveOrder($formData, $this->order);
				$this->cart->clear();
				$this->presenter->flashMessage('Objednávka byla uložena.', 'success');
				$this->presenter->redirect('Order:detail', ['id' => $this->order->getId()]);
			}
		};

		$form->onError[] = function(Form $form){
			foreach ($form->getErrors() as $error) {
				$this->presenter->flashMessage($error, 'warning');
			}
		};

		if ($this->order) {
			$form->setDefaults($this->adminOrderService->getFormDefaults($this->order));
		}

		if ($this->customer) {
			$formDefaults = $form->getValues(true);
			$formDefaults['customer'] = $this->customer->getId();
			$formDefaults['contactInformation'] = [
				'name' => $this->customer->getInvoiceInformation()->getName(),
				'lastName' => $this->customer->getInvoiceInformation()->getLastName(),
				'email' => $this->customer->getEmail(),
				'phone' => $this->customer->getPhone(),
				'lyonessCardId' => $this->customer->getLyoneesCardId(),
				'lyonessCardEan' => $this->customer->getLyoneesCardEan()
			];
			$formDefaults['invoiceInformation'] = [
				'ico' => $this->customer->getInvoiceInformation()->getIco(),
				'dic' => $this->customer->getInvoiceInformation()->getDic(),
				'company' => $this->customer->getInvoiceInformation()->getCompany(),
				'street' => $this->customer->getInvoiceInformation()->getStreet(),
				'city' => $this->customer->getInvoiceInformation()->getCity(),
				'zip' => $this->customer->getInvoiceInformation()->getZip(),
				'country' => $this->customer->getInvoiceInformation()->getCountry()
			];
			$formDefaults['deliveryAddress'] = [
				'name' => $this->customer->getDeliveryAddress()->getName(),
				'lastName' => $this->customer->getDeliveryAddress()->getLastName(),
				'company' => $this->customer->getDeliveryAddress()->getCompany(),
				'street' => $this->customer->getDeliveryAddress()->getStreet(),
				'city' => $this->customer->getDeliveryAddress()->getCity(),
				'zip' => $this->customer->getDeliveryAddress()->getZip(),
				'country' => $this->customer->getDeliveryAddress()->getCountry()
			];

			$form->setDefaults($formDefaults);
		}

		return $form;
	}

	private function saveOrder(array $formData, Order $order = null)
	{
		$contactInformationData = $formData['contactInformation'];
		$deliveryAddressData = $formData['deliveryAddress'];
		$invoiceInformationData = $formData['invoiceInformation'];
		$delivery = isset($formData['delivery']) && intval($formData['delivery']) > 0
			? $this->deliveryService->getDelivery(intval($formData['delivery'])) : null;

		$paymentType = $delivery && isset($formData['paymentType']) && intval($formData['paymentType']) > 0
			? $this->paymentTypeFacade->getPaymentType((int)$formData['paymentType']) : null;

		$discount = $this->cart->getDiscount() > 0 ? new Discount($this->cart->getDiscount(), Discount::TYPE_AMOUNT) : null;

		$lyonessCard = new LyonessCardData(null, $contactInformationData['lyonessCardId'], $contactInformationData['lyonessCardEan']);

		unset($formData['contactInformation'], $formData['deliveryAddress'], $formData['invoiceInformation']);
		unset($contactInformationData['lyonessCardId'], $contactInformationData['lyonessCardEan']);

		if (is_null($order)) {

			$products = [];

			foreach ($this->cart->getItems() as $item) {
				$products[] = [
					'id' => $item->getId(),
					'count' => $item->getQuantity(),
					'attributes' => $item->getOptions()
				];
			}

			$customer = (isset($formData['customer']) && is_numeric($formData['customer']))
				? $this->customerService->getOne($formData['customer']) : null;

			$this->order = $this->orderCreationService->createStoreOrder(
				$this->orderPartsFactory->createContactInformation($contactInformationData['name'], $contactInformationData['lastName'], $contactInformationData['email'], $contactInformationData['phone']),
				$this->orderPartsFactory->createInvoiceInformation($invoiceInformationData['company'], $invoiceInformationData['ico'], $invoiceInformationData['dic'], $invoiceInformationData['street'], $invoiceInformationData['city'], $invoiceInformationData['zip'], $invoiceInformationData['country']),
				$this->orderPartsFactory->createDeliveryAddress($deliveryAddressData['name'], $deliveryAddressData['lastName'], $deliveryAddressData['company'], $deliveryAddressData['street'], $deliveryAddressData['city'], $deliveryAddressData['zip'], $deliveryAddressData['country']),
				$products,
				$delivery,
				$paymentType,
				$customer,
				$lyonessCard,
				$discount,
				$this->cart->getExtraItems()
			);

			if (is_null($delivery)) {
				$this->adminOrderService->setOrderShipped($this->order);
			}

		} else {
			$this->orderCreationService->update(
				$this->order,
				(int)$formData['status'],
				$contactInformationData,
				$invoiceInformationData,
				$deliveryAddressData,
				$this->cart->getItems(),
				$delivery,
				$paymentType,
				$lyonessCard,
				$discount,
				$this->cart->getExtraItems()
			);
		}
	}

	protected function createComponentOrderLines($name = null)
	{
		return $this->orderLinesControlFactory->create($this->order, $this, $name);
	}


	/*
	public function handleLoadProductSelection($priceId)
	{
		$this->selectedPrice = $this->variantRepository->findPrice((int) $priceId);

	}

	protected function createComponentProductSelection()
	{
		$control = $this->productSelectionControlFactory->create($this->selectedPrice);

		$control->onProductSelect[] = function(ProductSelectionControl $control) {

		};
	}
	*/
}