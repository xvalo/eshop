<?php

namespace App\AdminModule\Components;

use App\Order\Order;
use Nette\ComponentModel\IContainer;

interface IOrderEditControlFactory
{
	/**
	 * @param Order|null $order
	 * @return OrderEditComponent
	 */
	public function create(Order $order = null);
}