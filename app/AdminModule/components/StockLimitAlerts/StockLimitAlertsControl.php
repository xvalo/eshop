<?php

namespace App\AdminModule\Components;

use App\Stock\StockItemRepository;
use Nette\Application\UI\Control;
use Nette\ComponentModel\IContainer;

class StockLimitAlertsControl extends Control
{
	private $listTemplate = '/list.latte';
	private $menuTemplate = '/menu.latte';

	private $alerts;

	public function __construct(
		IContainer $parent = null,
		$name = null,
		StockItemRepository $repository
	){
		parent::__construct($parent, $name);

		$this->alerts = $repository->getStockItemsToAlert();
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . $this->listTemplate);

		$this->template->alertsList = $this->alerts;

		$this->template->render();
	}

	public function renderMenu()
	{
		$this->template->setFile(__DIR__ . $this->menuTemplate);

		$this->template->alertsCount = count($this->alerts);
		$this->template->alerts = $this->alerts;

		$this->template->render();
	}
}