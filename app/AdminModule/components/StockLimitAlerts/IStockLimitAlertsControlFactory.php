<?php

namespace App\AdminModule\Components;

use Nette\ComponentModel\IContainer;

interface IStockLimitAlertsControlFactory
{
	/**
	 * @param IContainer $parent
	 * @param $name
	 * @return StockLimitAlertsControl
	 */
	function create(IContainer $parent, $name);
}