<?php


namespace App\AdminModule\Components;

use App\AdminModule\Grids\IOrderLinesGridFactory;
use App\Order\Order;
use App\Order\OrderService;
use Nette\Application\UI\Control;
use Nette\ComponentModel\IContainer;

class OrderLinesControl extends Control
{
	/** @var IOrderLinesGridFactory */
	private $orderLinesGridFactory;

	/** @var OrderService */
	private $orderService;

	/** @var Order */
	private $order;

	public function __construct(
		Order $order,
		IContainer $parent,
		string $name,
		IOrderLinesGridFactory $orderLinesGridFactory,
		OrderService $orderService
	){
		parent::__construct($parent, $name);
		$this->orderLinesGridFactory = $orderLinesGridFactory;
		$this->orderService = $orderService;
		$this->order = $order;
	}

	public function render()
	{
		$this->template->setFile(__DIR__.'/orderLines.latte');
		$this->template->render();
	}

	protected function createComponentGrid($name = null)
	{
		$grid = $this->orderLinesGridFactory->create($this, $name, $this->order);
		if ($this->order->isCanceled()){
			$grid->removeColumn('state');
		} else {
			$grid->getColumn('state')->onChange[] = [$this, 'changeState'];
		}
		return $grid;
	}

	public function handleDeleteOrderLine($orderLineId)
	{
		$this->orderService->removeOrderLineFromOrder($this->order, $orderLineId);
		if ($this->presenter->isAjax()) {
			$this['grid']->reload();
		} else {
			$this->redirect('this');
		}
	}

	public function changeState($id, $newState)
	{
		$this->orderService->changeOrderLineStatus($id, ($newState != 0 ? $newState : null));
		if ($this->presenter->isAjax()) {
			$this['grid']->redrawItem($id);
		} else {
			$this->redirect('this');
		}
	}

}