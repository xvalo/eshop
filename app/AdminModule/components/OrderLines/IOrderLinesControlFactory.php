<?php

namespace App\AdminModule\Components;

use App\Order\Order;
use Nette\ComponentModel\IContainer;

interface IOrderLinesControlFactory
{
	/**
	 * @param Order $order
	 * @param IContainer $parent
	 * @param string $name
	 * @return OrderLinesControl
	 */
	public function create(Order $order, IContainer $parent, string $name);
}