<?php

namespace App\AdminModule\Components;

use App\Order\Order;
use Nette\ComponentModel\IContainer;

interface IOrderCommentsControlFactory
{
	/**
	 * @param Order $order
	 * @param IContainer $parent
	 * @param $name
	 * @return OrderCommentsControl
	 */
	function create(Order $order, IContainer $parent, $name);
}