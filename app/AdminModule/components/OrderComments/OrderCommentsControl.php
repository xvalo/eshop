<?php

namespace App\AdminModule\Components;

use App\Admin\Admin;
use App\Admin\AdminService;
use App\Order\AdminOrderService;
use App\Order\Order;
use Carrooi\Security\User\User;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;

class OrderCommentsControl extends Control
{
	/** @var AdminOrderService */
	private $adminOrderService;

	/** @var Order */
	private $order;

	/** @var Admin */
	private $admin;

	private $templatePath = '/comments.latte';

	/**
	 * OrderDocumentsControl constructor.
	 * @param Order $order
	 * @param IContainer|null $parent
	 * @param null $name
	 * @param User $user
	 * @param AdminOrderService $adminOrderService
	 * @param AdminService $adminService
	 */
	public function __construct(
		Order $order,
		IContainer $parent = null,
		$name = null,
		User $user,
		AdminOrderService $adminOrderService,
		AdminService $adminService
	){
		parent::__construct($parent, $name);

		$this->order = $order;
		$this->adminOrderService = $adminOrderService;
		$this->admin = $adminService->getAdmin($user->getId());
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . $this->templatePath);
		$this->template->comments = $this->order->getComments();
		$this->template->render();
	}

	protected function createComponentCommentForm($name = null)
	{
		$form = new Form($this, $name);

		$form->addTextArea('content')
				->setAttribute('class', 'mceEditor');
		$form->addSubmit('save');

		$form->onSubmit[] = function(Form $form){
			$values = $form->getValues(true);

			$this->adminOrderService->addComment($values['content'], $this->order, $this->admin);

			$this->flashMessage('Komentář byl úspěšně uložen.', 'success');
			$this->redirect('this');
		};

		return $form;
	}
}