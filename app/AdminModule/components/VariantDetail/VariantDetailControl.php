<?php

namespace App\AdminModule\Components;

use App\Classes\Product\Variant\CodeSupplierDuplicateException;
use App\Classes\Product\Variant\ProductVariantPriceFactory;
use App\Classes\Product\Variant\ProductVariantService;
use App\InvalidArgumentException;
use App\Lists\ListsService;
use App\Stock\Supplier\Entity\Supplier;
use App\Stock\StockManager;
use App\Product\Code\VariantCode;
use App\Product\Price\Price;
use App\Product\Variant;
use App\Product\VariantRepository;
use App\Stock\StockService;
use App\Stock\Supplier\SupplierService;
use App\Stock\VariantStockItem\VariantStockItemService;
use App\Utils\Numbers;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\ComponentModel\IContainer;
use Nette\Neon\Exception;
use Nette\Utils\ArrayHash;
use Tracy\Debugger;

class VariantDetailControl extends Control
{
	/** @var VariantRepository */
	private $variantRepository;

	/** @var ListsService */
	private $listsService;

	/** @var Variant */
	private $variant;

	/** @var ProductVariantService */
	private $productVariantService;

	/** @var VariantStockItemService */
	private $variantStockItemService;

	/** @var StockManager */
	private $stockManager;

	/** @var StockService */
	private $stockService;

	/** @var SupplierService */
	private $supplierService;

	public function __construct(
		IContainer $parent = null,
		$name = null,
		Variant $variant = null,
		VariantRepository $variantRepository,
		ListsService $listsService,
		ProductVariantService $productVariantService,
		VariantStockItemService $variantStockItemService,
		StockManager $stockManager,
		StockService $stockService,
		SupplierService $supplierService
	){
		parent::__construct($parent, $name);

		$this->variantRepository = $variantRepository;
		$this->listsService = $listsService;
		$this->variant = $variant;
		$this->productVariantService = $productVariantService;
		$this->variantStockItemService = $variantStockItemService;
		$this->stockManager = $stockManager;
		$this->stockService = $stockService;
		$this->supplierService = $supplierService;
	}

	public function render()
	{
		$this->template->setFile(__DIR__ . '/detail.latte');

		$this->template->variant = $this->variant;

		if ($this->variant) {
			$this->template->stocksStatus = $this->variantStockItemService->getVariantOnStocksStatus($this->variant);
		}

		$this->template->render();
	}

	public function handleRemoveCode($codeId)
	{
		try {
			$id = intval($codeId);
			/** @var VariantCode $variantCode */
			foreach ($this->variant->getCodes() as $variantCode) {
				if ($variantCode->getId() == $id) {
					$this->productVariantService->removeCodeFromVariant($variantCode);
				}
			}
			$this->presenter->flashMessage('Kód byl smazán', 'success');
		} catch (\Exception $exception) {
			Debugger::log($exception);
			$this->presenter->flashMessage('Kód se nepodařilo smazat', 'danger');
		}

		$this->redirectToVariant();
	}

	public function handleRemovePrice($priceId)
	{
		try {
			$price = $this->variant->getPrice(intval($priceId));
			if ($price) {
				$this->productVariantService->removePriceFromVariant($price);
				$this->presenter->flashMessage('Cena byla smazána', 'success');
			} else {
				$this->presenter->flashMessage('Zvolená cena nebyla nalezena.', 'warning');
			}

		} catch (\Exception $exception) {
			Debugger::log($exception);
			$this->presenter->flashMessage('Cenu se nepodařilo smazat', 'danger');
		}

		$this->redirectToVariant();
	}

	protected function createComponentCodeForm()
	{
		$form = new Form();

		$form->addText('code')
				->setRequired(true);
		$form->addSelect('supplier', null, $this->supplierService->getSuppliersList())
			->setPrompt('bez dodavatele');

		$form->addCheckbox('isPriceSynchronized');
		$form->addCheckbox('isAvailabilitySynchronized');

		$form->addHidden('id');
		$form->addSubmit('save');

		$form->onSubmit[] = function(Form $form){
			try {
				$values = $form->getValues();

				$variantId = strlen($values->id) > 0 ? (int)$values->id : null;

				$supplier = strlen($values->supplier) > 0 ? $this->supplierService->getSupplier((int)$values->supplier) : null;
				$code = $this->getCodeFroVariant($values->code, $supplier, $variantId);

				if ($variantId) {
					$this->productVariantService->updateVariantCode($variantId, $this->variant, $code, $supplier, $values->isPriceSynchronized, $values->isAvailabilitySynchronized);
				} else {
					$this->productVariantService->addCodeToVariant($this->variant, $code, $supplier, $values->isPriceSynchronized, $values->isAvailabilitySynchronized);
				}
				$this->presenter->flashMessage('Kód byl úspěšně uložen.');
			} catch (CodeSupplierDuplicateException $e) {
				$this->presenter->flashMessage('Kombinace zadaného kódu a dodavatele už jednou existuje. Prosím zadejte jiné.', 'danger');
			} catch (Exception $exception) {
				Debugger::log($exception);
				$this->presenter->flashMessage('Při ukládání došlo k chybě.', 'danger');
			}

			$this->redirectToVariant();
		};

		return $form;
	}

	protected function createComponentPriceForm()
	{
		$form = new Form();

		$form->addText('name');

		$form->addSelect('type', null, [Price::TYPE_REGULAR => 'normální', Price::TYPE_PRECENTAGE => 'procentuální část', Price::TYPE_SUBTRACTED => 'odečtení částky'])
				->setRequired(true);

		$form->addText('price')
			->addConditionOn($form['type'], Form::EQUAL, Price::TYPE_REGULAR)
			->addRule(Form::FILLED, 'Zadejte prosím cenu.')
			->addRule(Form::FLOAT, 'Zadávejte prosím pouze číselnou hodnotu.')
			->addRule(Form::MIN, 'Zadejte prosím cenu větší než 0.', 0)
			->addRule(Form::NOT_EQUAL, 'Cena nesmí být nulová.', 0);

		$form->addSelect('countedFrom', null, $this->getVariantPricesList($this->variant->getPrices()));
		$form->addCheckbox('synchronized');
		$form->addText('value');
		$form->addText('purchasePriceWithoutVat');

		$form->addHidden('id');
		$form->addSubmit('save');

		$form->onSubmit[] = function(Form $form){
			try {
				$values = $form->getValues();
				if ($values->id) {
					$price = $this->variant->getPrice(intval($values->id));
					$this->doPriceUpdateFromFormData($price, $values);
				} else {
					$priceObject = $this->getPriceFromData($values);
					$this->productVariantService->addPriceToVariant($this->variant, $priceObject);
				}

				$this->presenter->flashMessage('Cena byla úspěšně vytvořena.', 'success');
			} catch (\Exception $exception) {
				Debugger::log($exception);
				$this->flashMessage('Při ukládání došlo k chybě: '.$exception->getMessage(), 'danger');
			}

			$this->redirectToVariant();
		};

		return $form;
	}

	protected function createComponentStockItemForm()
	{
		$form = new Form();

		$form->addText('count');
		$form->addText('limit');
		$form->addText('position');
		$form->addCheckbox('alertEnabled');

		$form->addHidden('stockId');
		$form->addSubmit('save');

		$form->onSubmit[] = function(Form $form){
			try {
				$values = $form->getValues();
				$stock = $this->listsService->getStock(intval($values->stockId));
				$stockItem = $this->variant->getStockItem($stock);
				$currentCount = ($stockItem) ? $stockItem->getCount() : 0;
				$countChange = (int)$values->count - $currentCount;
				$this->stockManager->changeStockItemState($this->variant, $stock, $countChange, $values->position, intval($values->limit), $values->alertEnabled);

				$this->presenter->flashMessage('Změna ve skladu byla úspěšně uložena', 'success');
			} catch (\Exception $exception) {
				Debugger::log($exception);
				$this->flashMessage('Při ukládání došlo k chybě: '.$exception->getMessage(), 'danger');
			}

			$this->redirectToVariant();
		};

		return $form;
	}

	private function getPriceFromData(ArrayHash $data)
	{
		switch ($data->type) {
			case Price::TYPE_REGULAR:
				return ProductVariantPriceFactory::createRegularPrice($this->variant, Numbers::floatFromCzechNumber($data->price), $data->name, $data->synchronized, Numbers::floatFromCzechNumber($data->purchasePriceWithoutVat));
			case Price::TYPE_SUBTRACTED:
				$countedFrom = $this->variant->getPrice($data->countedFrom);
				return ProductVariantPriceFactory::createSubtractedPrice($this->variant, $countedFrom, $data->value, $data->name, Numbers::floatFromCzechNumber($data->purchasePriceWithoutVat));
			case Price::TYPE_PRECENTAGE:
				$countedFrom = $this->variant->getPrice($data->countedFrom);
				return ProductVariantPriceFactory::createPercentagePrice($this->variant, $countedFrom, $data->value, $data->name, Numbers::floatFromCzechNumber($data->purchasePriceWithoutVat));
			default:
				throw new InvalidArgumentException('Wrong price type');
		}
	}

	private function doPriceUpdateFromFormData(Price $price, ArrayHash $data)
	{
		switch ($data->type) {
			case Price::TYPE_REGULAR:
				$this->productVariantService->updateRegularPrice($price, $data->name, Numbers::floatFromCzechNumber($data->price), $data->synchronized, Numbers::floatFromCzechNumber($data->purchasePriceWithoutVat));
				break;
			case Price::TYPE_SUBTRACTED:
				$countedFrom = $this->variant->getPrice($data->countedFrom);
				$this->productVariantService->updateSubtractedPrice($price, $data->name, $countedFrom, $data->value, Numbers::floatFromCzechNumber($data->purchasePriceWithoutVat));
				break;
			case Price::TYPE_PRECENTAGE:
				$countedFrom = $this->variant->getPrice($data->countedFrom);
				$this->productVariantService->updatePercentagePrice($price, $data->name, $countedFrom, $data->value, Numbers::floatFromCzechNumber($data->purchasePriceWithoutVat));
				break;
			default:
				throw new InvalidArgumentException('Wrong price type');
		}
	}

	private function getVariantPricesList($prices)
	{
		$list = [];

		/** @var Price $price */
		foreach ($prices as $price) {
			$list[$price->getId()] = strlen($price->getName()) ? $price->getName() : ' -- základní -- ';
		}

		return $list;
	}

	private function redirectToVariant()
	{
		$this->presenter->redirect('Product:variants', ['id' => $this->variant->getProduct()->getId(), 'variantId' => $this->variant->getId()]);
	}

	private function getCodeFroVariant(string $code, Supplier $supplier = null, int $variantCodeId = null)
	{
		$prefix = $supplier && $supplier->getPrefix() && strlen($supplier->getPrefix()) > 0 ? $supplier->getPrefix() . '-' : '';

		if ($variantCodeId) {
			/** @var VariantCode $variantCode */
			foreach ($this->variant->getCodes() as $variantCode) {
				if ($variantCode->getId() == $variantCodeId) {
					if ($supplier && !$variantCode->getSupplier()) {
						$code = $prefix.$code;
					} elseif ($supplier && $variantCode->getSupplier() && $variantCode->getSupplier()->getId() != $supplier->getId()) {
						$code = preg_replace('/^' . preg_quote($variantCode->getSupplier()->getPrefix().'-', '/') . '/', $supplier->getPrefix().'-', $code);
					}
				}
			}
		} else {
			$code = $prefix.$code;
		}

		return $this->productVariantService->getCodeByString($code);
	}
}