<?php

namespace App\AdminModule\Components;


use App\Product\Variant;
use Nette\ComponentModel\IContainer;

interface IVariantDetailControlFactory
{
	/**
	 * @param IContainer $parent
	 * @param $name
	 * @param Variant|null $variant
	 * @return VariantDetailControl
	 */
	function create(IContainer $parent, $name, Variant $variant = null);
}