<?php

namespace App\AdminModule\Components;

use App\Attribute\Core\Attribute;
use App\Category\Entity\Category;

interface ICategoryAttributeDetailControlFactory
{
	/**
	 * @param Category $category
	 * @param Attribute $attribute
	 * @return CategoryAttributeDetailControl
	 */
	function create(Category $category, Attribute $attribute = null);
}