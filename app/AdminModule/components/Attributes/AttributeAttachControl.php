<?php

namespace App\AdminModule\Components;

use App\Attribute\AttributeService;
use App\Attribute\CategoryAttributeService;
use App\Attribute\ProductAttributeService;
use App\Category\Entity\Category;
use App\InvalidArgumentException;
use App\Product\Product;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class AttributeAttachControl extends Control
{
	/** @var Category|Product  */
	private $attachTo;

	/** @var AttributeService */
	private $attributeService;

	/** @var CategoryAttributeService */
	private $categoryAttributeService;

	/** @var ProductAttributeService */
	private $productAttributeService;

	public function __construct(
		$attachTo,
		AttributeService $attributeService,
		CategoryAttributeService $categoryAttributeService,
		ProductAttributeService $productAttributeService
	){
		parent::__construct();

		if (!($attachTo instanceof Category) && !($attachTo instanceof Product)) {
			throw new InvalidArgumentException('Invalid type of entity');
		}

		$this->attachTo = $attachTo;
		$this->attributeService = $attributeService;
		$this->categoryAttributeService = $categoryAttributeService;
		$this->productAttributeService = $productAttributeService;
	}

	public function render()
	{
		$this->template->setFile(__DIR__.'/attachControlDetail.latte');

		$this->template->render();
	}

	protected function createComponentForm()
	{
		$form = new Form();

		$attributes = $form->addContainer('attributes');
		foreach ($this->attributeService->getAvailableAttributesList($this->attachTo) as $id => $name) {
			$attribute = $attributes->addContainer($id);
			$attribute->addCheckbox('attach', $name);
			$attribute->addCheckbox('force', 'Povinné vybrat');
		}

		$form->addSubmit('save');

		$form->onSuccess[] = function (Form $form) {
			$values = $form->getValues(true);

			if ($this->attachTo instanceof Product) {
				$this->productAttributeService->attachAttributes($values['attributes'], $this->attachTo);
			}

			if ($this->attachTo instanceof Category) {
				$this->categoryAttributeService->attachAttributes($values['attributes'], $this->attachTo);
			}

			$this->flashMessage('Vlastnosti byly přiřazeny');
			$this->redirect('this');
		};

		return $form;
	}
}