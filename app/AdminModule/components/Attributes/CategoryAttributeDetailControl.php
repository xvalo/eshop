<?php

namespace App\AdminModule\Components;

use App\AdminModule\Grids\ICategoryAttributeVariantGridFactory;
use App\Attribute\CategoryAttributeService;
use App\Attribute\Core\Attribute;
use App\Attribute\Image\ImageManager;
use App\Category\Entity\Category;
use Nette\Application\UI\Form;
use Ublaboo\ImageStorage\ImageStorage;

class CategoryAttributeDetailControl extends AttributeControl
{
	/** @var CategoryAttributeService  */
	private $categoryAttributeService;

	/** @var Category */
	private $category;

	/** @var ImageManager */
	private $imageManager;

	/** @var ICategoryAttributeVariantGridFactory */
	private $attributeVariantGridFactory;

	public function __construct(
		Category $category,
		Attribute $attribute = null,
		ImageManager $imageManager,
		ImageStorage $imageStorage,
		CategoryAttributeService $categoryAttributeService,
		ICategoryAttributeVariantGridFactory $attributeVariantGridFactory
	){
		parent::__construct($attribute, $imageStorage);

		$this->categoryAttributeService = $categoryAttributeService;
		$this->category = $category;
		$this->imageManager = $imageManager;
		$this->attributeVariantGridFactory = $attributeVariantGridFactory;
	}

	protected function createComponentVariantsGrid()
	{
		return $this->attributeVariantGridFactory->create($this->attribute, $this->category);
	}

	public function handleRemoveAttributeVariant($id)
	{
		try{
			$this->categoryAttributeService->removeAttributeVariant(intval($id), $this->category);
			$this->flashMessage('Varianta byla úspěšně smazána.', 'success');
		} catch (\Exception $e) {
			$this->flashMessage('Variantu se nepodařilo smazat. Při mazání došlo k chybě.', 'error');
		}

		$this->redirect('this');
	}

	protected function createComponentExistingVariants()
	{
		$form = new Form();

		$attributes = $form->addContainer('variants');
		foreach ($this->categoryAttributeService->getAvailableAttributeVariantsList($this->attribute, $this->category) as $id => $name) {
			$attribute = $attributes->addContainer($id);
			$attribute->addCheckbox('attach', $name);
			$attribute->addText('price', 'Cena');
		}

		$form->addSubmit('save');

		$form->onSuccess[] = function (Form $form) {
			$values = $form->getValues(true);

			$variants = array_filter($values['variants'], function($data){
				return $data['attach'] === true;
			});

			$this->categoryAttributeService->attachVariants($this->attribute, $this->category, $variants);

			$this->flashMessage('Vlastnosti byly přiřazeny');
			$this->redirect('this');
		};

		return $form;
	}

	protected function createComponentBulkPriceSetup()
	{
		$form = new Form();

		$form->addText('price', 'Cena')
			->setRequired('Zadejte cenu, která se má nastavit')
			->addRule(Form::MIN, 'Nesmíte zadat zápornou hodnotu.', 0)
			->setType('number');

		$form->addSubmit('save');

		$form->onSuccess[] = function (Form $form) {
			$values = $form->getValues(true);

			$price = (float) $values['price'];

			$this->categoryAttributeService->setPriceToAllCategoryAttributeVariants($this->attribute, $this->category, $price);

			$this->flashMessage('Cena byla nastavena');
			$this->redirect('this');
		};

		return $form;
	}


}