<?php

namespace App\AdminModule\Components;

use App\Category\Entity\Category;
use App\Product\Product;

interface IAttributeAttachControlFactory
{
	/**
	 * @param Product|Category $attachTo
	 * @return AttributeAttachControl
	 */
	public function create($attachTo);
}