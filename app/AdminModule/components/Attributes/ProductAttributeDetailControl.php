<?php

namespace App\AdminModule\Components;

use App\AdminModule\Grids\IProductAttributeVariantGridFactory;
use App\Attribute\Core\Attribute;
use App\Attribute\Image\ImageManager;
use App\Attribute\ProductAttributeService;
use Nette\Application\UI\Form;
use Ublaboo\ImageStorage\ImageStorage;
use App\Product\Product;

class ProductAttributeDetailControl extends AttributeControl
{
	/** @var ProductAttributeService  */
	private $productAttributeService;

	/** @var Product */
	private $product;

	/** @var ImageManager */
	private $imageManager;

	/** @var IProductAttributeVariantGridFactory */
	private $attributeVariantGridFactory;

	public function __construct(
		Product $product,
		Attribute $attribute = null,
		ImageManager $imageManager,
		ImageStorage $imageStorage,
		ProductAttributeService $productAttributeService,
		IProductAttributeVariantGridFactory $attributeVariantGridFactory
	){
		parent::__construct($attribute, $imageStorage);

		$this->productAttributeService = $productAttributeService;
		$this->product = $product;
		$this->imageManager = $imageManager;
		$this->attributeVariantGridFactory = $attributeVariantGridFactory;
	}
	public function handleRemoveAttributeVariant($id)
	{
		try{
			$this->productAttributeService->removeAttributeVariant(intval($id));
			$this->flashMessage('Varianta byla úspěšně smazána.', 'success');
		} catch (\Exception $e) {
			$this->flashMessage('Variantu se nepodařilo smazat. Při mazání došlo k chybě.', 'error');
		}

		$this->redirect('this');
	}

	protected function createComponentVariantsGrid()
	{
		return $this->attributeVariantGridFactory->create($this->attribute, $this->product);
	}

	protected function createComponentExistingVariants()
	{
		$form = new Form();

		$attributes = $form->addContainer('variants');
		foreach ($this->productAttributeService->getAvailableAttributeVariantsList($this->attribute, $this->product) as $id => $name) {
			$attribute = $attributes->addContainer($id);
			$attribute->addCheckbox('attach', $name);
			$attribute->addText('price', 'Cena');
		}

		$form->addSubmit('save');

		$form->onSuccess[] = function (Form $form) {
			$values = $form->getValues(true);

			$variants = array_filter($values['variants'], function($data){
				return $data['attach'] === true;
			});

			$this->productAttributeService->attachVariants($this->attribute, $this->product, $variants);

			$this->flashMessage('Vlastnosti byly přiřazeny');
			$this->redirect('this');
		};

		return $form;
	}

	protected function createComponentBulkPriceSetup()
	{
		$form = new Form();

		$form->addText('price', 'Cena')
			->setRequired('Zadejte cenu, která se má nastavit')
			->addRule(Form::MIN, 'Nesmíte zadat zápornou hodnotu.', 0)
			->setType('number');

		$form->addSubmit('save');

		$form->onSuccess[] = function (Form $form) {
			$values = $form->getValues(true);

			$price = (float) $values['price'];

			$productAttributeVariants = $this->attribute->getProductRelation($this->product)->getVariants()->toArray();
			$this->productAttributeService->setPriceToProductAttributeVariants($productAttributeVariants, $price);

			$this->flashMessage('Cena byla nastavena');
			$this->redirect('this');
		};

		return $form;
	}
}