<?php

namespace App\AdminModule\Components;

use App\Attribute\AttributeFacade;
use App\Attribute\AttributeService;
use App\Attribute\Core\Attribute;
use App\Attribute\Image\ImageManager;
use Nette\Application\UI\Form;
use Ublaboo\ImageStorage\ImageStorage;


class AttributeDetailControl extends AttributeControl
{
	/** @var Attribute */
	protected $attribute;

	/** @var ImageStorage */
	private $imageStorage;

	/** @var ImageManager */
	private $imageManager;

	/** @var AttributeFacade */
	private $attributeFacade;

	/** @var AttributeService */
	private $attributeService;

	public function __construct(
		Attribute $attribute,
		ImageStorage $imageStorage,
		ImageManager $imageManager,
		AttributeFacade $attributeFacade,
		AttributeService $attributeService
	){
		parent::__construct($attribute, $imageStorage);

		$this->attribute = $attribute;
		$this->imageStorage = $imageStorage;
		$this->imageManager = $imageManager;
		$this->attributeFacade = $attributeFacade;
		$this->attributeService = $attributeService;
	}

	public function handleRemoveAttributeVariant($id)
	{
		// TODO: Implement handleRemoveAttributeVariant() method.
	}

	protected function createComponentAttributeVariantForm()
	{
		$form = $this->createAttributeVariantForm();

		$form->onSuccess[] = function(Form $form) {
			$attribute = null;
			try {
				$values = $form->getValues();
				$image = ($values->notChangeImage)
					?: (($values->image->hasFile()) ? $this->imageManager->createImage($values->image) : null);
				if (intval($values->id) > 0) {
					$attributeVariant = $this->attributeFacade->getAttributeVariant((int) $values->id);
					$this->attributeService->updateAttributeVariant($attributeVariant, $values->name, floatval($values->price), $values->isDiscount, $values->isNews, $values->isActive, $image);
				} else {
					$this->attributeService->createAttributeVariant($this->attribute, $values->name, floatval($values->price), $values->isDiscount, $values->isNews, $values->isActive, $image);
				}
				$this->flashMessage('Varianta byla úspěšně uložena');
			} catch (\Exception $e){
				Debugger::log($e);
				$this->flashMessage('Při ukládání varianty vlastnosti nastala chyba. Zkuste to prosím znovu.');
			}
			$this->redirect('this');
		};

		return $form;
	}
}