<?php


namespace App\AdminModule\Components;

use App\Attribute\Core\Attribute;

interface IAttributeDetailControlFactory
{
	/**
	 * @param Attribute $attribute
	 * @return AttributeDetailControl
	 */
	public function create(Attribute $attribute);
}