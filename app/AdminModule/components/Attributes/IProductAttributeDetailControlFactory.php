<?php

namespace App\AdminModule\Components;

use App\Attribute\Core\Attribute;
use App\Product\Product;

interface IProductAttributeDetailControlFactory
{
	/**
	 * @param Product $product
	 * @param Attribute $attribute
	 * @return ProductAttributeDetailControl
	 */
	function create(Product $product, Attribute $attribute = null);
}