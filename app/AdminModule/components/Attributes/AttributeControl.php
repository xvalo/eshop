<?php

namespace App\AdminModule\Components;

use App\Attribute\Core\Attribute;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Ublaboo\ImageStorage\ImageStorage;

abstract class AttributeControl extends Control
{

	/** @var Attribute */
	protected $attribute;

	/** @var ImageStorage */
	private $imageStorage;

	public function __construct(
		Attribute $attribute = null,
		ImageStorage $imageStorage
	){
		parent::__construct();

		$this->attribute = $attribute;
		$this->imageStorage = $imageStorage;
	}

	abstract public function handleRemoveAttributeVariant($id);

	public function render()
	{
		$this->template->setFile(__DIR__ . '/detail.latte');
		$this->template->imageStorage = $this->imageStorage;

		$this->template->attribute = $this->attribute;

		$this->template->render();
	}
}