<?php

namespace App\AdminModule\Components;

use App\Product\Price\Price;

interface IProductSelectionControlFactory
{
	/**
	 * @param Price $price
	 * @return ProductSelectionControl
	 */
	public function create(Price $price);
}