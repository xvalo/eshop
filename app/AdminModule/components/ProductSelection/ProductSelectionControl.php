<?php


namespace App\AdminModule\Components;

use App\Product\Price\Price;
use App\Product\Product;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class ProductSelectionControl extends Control
{
	/** @var Price */
	private $selectedPrice;

	/** @var Product */
	private $product;

	/** @var callable[]  */
	public $onProductSelect = [];

	public function __construct(Price $price)
	{
		parent::__construct();
		$this->selectedPrice = $price;
		$this->product = $price->getVariant()->getProduct();
	}

	public function render()
	{
		$this->template->setFile(__DIR__.'/detail.latte');
		$this->template->product = $this->product;
	}

	protected function createComponentForm()
	{
		$form = new Form();

		$form->addSelect('prices');
		$form->addInteger('amount');

		$form->onSubmit[] = [$this, 'processForm'];

		return $form;
	}

	public function processForm(Form $form)
	{

		$this->onProductSelect($this);
	}
}