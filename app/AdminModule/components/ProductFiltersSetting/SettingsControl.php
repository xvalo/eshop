<?php

namespace App\AdminModule\Components\ProductFiltersSetting;

use App\Filter\Entity\Filter;
use App\Filter\Entity\Value;
use App\Filter\FilterFacade;
use App\Filter\FilterService;
use App\Product\Product;
use App\Product\Variant;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class SettingsControl extends Control
{
	/** @var Product */
	private $product;
	
	/** @var FilterService */
	private $filterService;

	/** @var array|Variant[] */
	private $variants;

	private $filtersValuesList;
	/**
	 * @var FilterFacade
	 */
	private $filterFacade;

	public function __construct(
		Product $product,
		FilterService $filterService,
		FilterFacade $filterFacade
	){
		parent::__construct();
		$this->product = $product;
		$this->filterService = $filterService;

		$this->prepareVariants();
		$this->filterFacade = $filterFacade;
	}

	public function render()
	{
		$this->template->variants = $this->variants;

		$this->template->setFile(__DIR__.'/filtersSettings.latte');
		$this->template->render();
	}

	protected function createComponentForm()
	{
		$form = new Form();

		$variants = $form->addContainer('variants');
		$filters = $this->filterService->getCategoryFilters($this->product->getCategory());
		$filtersValuesList = [];
		$defaults = [
			'variants' => []
		];

		/** @var Filter $filter */
		foreach ($filters as $filter) {
			$filtersValuesList[$filter->getId()] = [
				'name' => $filter->getName(),
				'values' => []
			];

			/** @var Value $value */
			foreach ($this->filterFacade->getFilterValues($filter) as $value) {
				$filtersValuesList[$filter->getId()]['values'][$value->getId()] = $value->getValue();
			}
		}

		/** @var Variant $variant */
		foreach ($this->variants as $id => $variant) {
			$item = $variants->addContainer($id);
			$defaults['variants'][$id] = $this->filterService->getVariantFiltersDefaultValues($variant, array_keys($filtersValuesList));

			foreach ($filtersValuesList as $filterId => $filterData) {
				$item->addSelect($filterId, $filterData['name'], $filterData['values'])
					->setPrompt('–– vyberte hodnotu ---');
			}
		}
		
		$form->addSubmit('save');

		$form->onSuccess[] = function(Form $form) {
			try {
				$variants = $form->getValues(true)['variants'];
				foreach ($variants as $id => $filters) {
					$this->filterService->setFilterValuesToProductVariant($filters, $this->variants[$id]);
				}
				$this->presenter->flashMessage('Nastavení filtrů bylo uloženo', 'success');
			} catch (\Exception $e){
				throw $e;
				$this->presenter->flashMessage('Při ukládání došlo k chybě', 'danger');
			}

			$this->presenter->redirect('this');
		};

		$form->setDefaults($defaults);

		return $form;
	}

	private function prepareVariants()
	{
		/** @var Variant $variant */
		foreach ($this->product->getPriceVariants() as $variant) {
			$this->variants[$variant->getId()] = $variant;
		}
	}
}