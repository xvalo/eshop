<?php

namespace App\AdminModule\Components\ProductFiltersSetting;

use App\Product\Product;

interface ISettingsControlFactory
{
	/**
	 * @param Product $product
	 * @return SettingsControl
	 */
	public function create(Product $product);
}