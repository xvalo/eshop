<?php

namespace App\AdminModule\Components;

use App\Admin\Forms\AdminSearchFactory;
use DK\Menu\Menu;
use DK\Menu\UI\Control;
use Nette\Application\UI\Form;

class AdminMenuControl extends Control
{
    /**
     * @var AdminSearchFactory
     */
    private $factory;

    public function __construct(Menu $menu, AdminSearchFactory $factory)
    {
        parent::__construct($menu);

        $this->factory = $factory;
    }

    protected function createComponentSearchForm()
    {
        $form = $this->factory->create();

        $form->onSuccess[] = function (Form $form) {
            $values = $form->getValues();
            $this->presenter->redirect('Search:default', ['q' => $values->search]);
        };

        return $form;
    }
}

interface IAdminMenuFactory
{
    /** @return AdminMenuControl */
    public function create();
}