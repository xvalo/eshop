<?php

namespace App\AdminModule\Components\RelatedProducts;

use App\Product\Product;
use App\Product\ProductService;
use App\Product\Related\RelatedProductService;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Ublaboo\ImageStorage\ImageStorage;

class RelatedProductsControl extends Control
{
	/** @var Product */
	private $product;

	/** @var RelatedProductService */
	private $relatedProductService;

	/** @var ImageStorage */
	private $imageStorage;

	/** @var ProductService */
	private $productService;

	public function __construct(
		Product $product,
		RelatedProductService $relatedProductService,
		ProductService $productService,
		ImageStorage $imageStorage
	){
		parent::__construct();
		$this->product = $product;
		$this->relatedProductService = $relatedProductService;
		$this->imageStorage = $imageStorage;
		$this->productService = $productService;
	}

	public function handleRemoveRelation($id)
	{
		$this->relatedProductService->removeRelation((int) $id);

		if ($this->presenter->isAjax()) {
			$this->redrawControl('related-products-list');
		} else {
			$this->redirect('this');
		}
	}

	public function handleCreateRelation($productId)
	{
		$relatedProduct = $this->productService->getProduct((int)$productId);
		$this->relatedProductService->createProductRelation($relatedProduct, $this->product);

		if ($this->presenter->isAjax()) {
			$this->redrawControl('related-products-list');
		} else {
			$this->redirect('this');
		}
	}

	protected function createComponentForm()
	{
		$form = new Form();

		$form->addText('product')
			->setAttribute('class', 'form-control product-select-finder')
			->setAttribute('data-search-link', $this->presenter->link('//:Api:Product:searchProducts'))
			->setAttribute('data-create-relation-link', $this->link('createRelation!'));
		$form->addSubmit('save');

		return $form;
	}

	public function render()
	{
		$this->template->productRelations = $this->relatedProductService->getProductRelations($this->product);

		$this->template->imageStorage = $this->imageStorage;
		$this->template->setFile(__DIR__.'/list.latte');
		$this->template->render();
	}
}