<?php

namespace App\AdminModule\Components\RelatedProducts;

use App\Product\Product;

interface IRelatedProductsControlFactory
{
	/**
	 * @param Product $product
	 * @return RelatedProductsControl
	 */
	public function create(Product $product);
}