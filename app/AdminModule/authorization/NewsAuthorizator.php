<?php

namespace App\AdminModule\Authorization;

use App\Classes\Admin\PrivilegeCodes;
use App\Classes\Admin\Role;
use Carrooi\Security\Authorization\IResourceAuthorizator;
use Carrooi\Security\User\User;

class NewsAuthorizator implements IResourceAuthorizator
{

	/**
	 * @return array|string
	 */
	public function getActions()
	{
		return '*';
	}

	/**
	 * @param \Carrooi\Security\User\User $user
	 * @param string $action
	 * @param mixed $data
	 * @return bool
	 */
	public function isAllowed(User $user, $action, $data = null)
	{
		return $user->isInRole(Role::SUPER_ADMIN) || in_array(PrivilegeCodes::NEWS, $user->getIdentity()->privileges);
	}
}