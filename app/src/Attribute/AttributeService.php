<?php

namespace App\Attribute;

use App\Attribute\Core\Attribute;
use App\Attribute\Core\AttributeVariant;
use App\Category\Entity\Category;
use App\File\Image;
use App\InvalidArgumentException;
use App\Product\Product;
use Nette\SmartObject;
use Tracy\Debugger;

class AttributeService
{
	use SmartObject;

	/** @var AttributeFacade */
	protected $attributeFacade;

	public function __construct(AttributeFacade $attributeFacade)
	{
		$this->attributeFacade = $attributeFacade;
	}

	/**
	 * @param int $id
	 * @return Attribute|null
	 */
	public function getAttribute(int $id)
	{
		return $this->attributeFacade->getAttribute($id);
	}

	/**
	 * @param int $id
	 * @return AttributeVariant|null
	 */
	public function getAttributeVariant(int $id)
	{
		return $this->attributeFacade->getAttributeVariant($id);
	}

	/**
	 * @param string $name
	 * @param string $title
	 * @param string $description
	 * @param Attribute|null $parent
	 * @return Attribute
	 * @throws \Exception
	 */
	public function createAttribute(string $name, string $title, string $description, Attribute $parent = null): Attribute
	{
		return $this->attributeFacade->saveAttribute(AttributeFactory::createAttribute($name, $title, $description, $parent));
	}

	/**
	 * @param Attribute $attribute
	 * @param string $name
	 * @param float $price
	 * @param bool $isDiscount
	 * @param bool $isNews
	 * @param bool $isActive
	 * @param Image|null|bool $image
	 * @return Attribute
	 * @throws \Exception
	 */
	public function createAttributeVariant(Attribute $attribute, string $name, float $price, bool $isDiscount, bool $isNews, bool $isActive, $image = true): Attribute
	{
		$image = $image !== true ? $image : null;

		$attributeVariant = AttributeFactory::createAttributeVariant($attribute, $name, $price, $image, $isDiscount, $isNews);
		if($isActive) {
			$attributeVariant->activate();
		}

		$attribute->addVariant($attributeVariant);
		$this->attributeFacade->saveAttribute($attribute);

		return $attribute;
	}

	public function updateAttributeVariant(AttributeVariant $attributeVariant, string $name, float $price, bool $isDiscount, bool $isNews, bool $isActive, $image = true)
	{
		$attributeVariant->setName($name);
		$attributeVariant->setPrice($price);
		$attributeVariant->setIsDiscount($isDiscount);
		$attributeVariant->setIsNews($isNews);
		if ($isActive) {
			$attributeVariant->activate();
		} else {
			$attributeVariant->deactivate();
		}

		if ($image !== true) {
			$attributeVariant->setImage($image);
		}

		$this->attributeFacade->saveAttribute($attributeVariant->getAttribute());
	}


	public function updateAttribute(Attribute $attribute, $entity, string $name, string $description, Attribute $parent = null)
	{
		$attribute->setName($name);
		$attribute->setDescription($description);
		$attribute->setParent($parent);

		$this->attributeFacade->saveAttribute($attribute);
	}

	public function getAvailableAttributesList($attachedTo)
	{
		if ($attachedTo instanceof Category) {
			$availableAttributes = $this->attributeFacade->getAvailableAttributesListForCategory($attachedTo);
		} elseif ($attachedTo instanceof Product) {
			$availableAttributes = $this->attributeFacade->getAvailableAttributesListForProduct($attachedTo);
		} else {
			throw new InvalidArgumentException('Invalid type of entity');
		}

		$list = [];

		/** @var Attribute $attribute */
		foreach ($availableAttributes as $data) {
			$list[$data['id']] = $data['name'];
		}

		return $list;
	}

	public function removeAttribute(Attribute $attribute, bool $saveChanges = true)
	{
		$attribute->delete();

		$categories = $this->attributeFacade->getCategoriesAttributeBelongsTo($attribute);
		$products = $this->attributeFacade->getProductsAttributeBelongsTo($attribute);

		if (count($categories) > 0) {
			$ids = array_column($categories, 'id');
			$this->attributeFacade->setAttributeCategoryState($attribute, $ids, false, null, true);
		}

		if (count($products) > 0) {
			$ids = array_column($products, 'id');
			$this->attributeFacade->setAttributeProductState($attribute, $ids, false, null, true);
		}

		foreach ($attribute->getVariants() as $variant) {
			$this->removeAttributeVariant($variant, false);
		}

		foreach ($attribute->getChildren() as $attribute) {
			$this->removeAttribute($attribute, false);
		}

		if ($saveChanges) {
			$this->attributeFacade->saveAttribute($attribute);
		}
	}

	public function removeAttributeVariant(AttributeVariant $attributeVariant, bool $saveChanges = true)
	{
		$attributeVariant->delete();

		$this->attributeFacade->deleteAttributeVariantFromCategory($attributeVariant);
		$this->attributeFacade->deleteAttributeVariantFromProduct($attributeVariant);

		if ($saveChanges) {
			$this->attributeFacade->saveAttributeVariant($attributeVariant);
		}
	}

}