<?php

namespace App\Attribute;

use App\Attribute\Category\CategoryAttribute;
use App\Attribute\Core\Attribute;
use App\Attribute\Product\ProductAttribute;
use App\Category\Entity\Category;
use App\Product\Product;
use Doctrine\ORM\QueryBuilder;
use Kdyby;
use Kdyby\Doctrine\QueryObject;

class AttributeQuery extends QueryObject
{

	/** @var array|\Closure[] */
	private $filter = [];

	public function forCategory(Category $category)
	{
		$this->filter[] = function(QueryBuilder $qb) use ($category) {
			$qb->innerJoin(CategoryAttribute::class, 'CA', Kdyby\Doctrine\Dql\Join::WITH, 'CA.attribute = A.id');

			$qb->andWhere($qb->expr()->eq('CA.category', ':category'));
			$qb->andWhere($qb->expr()->eq('CA.deleted', ':relDeleted'));
			$qb->andWhere($qb->expr()->eq('CA.active', ':relActive'));

			$qb->setParameters([
				'category' => $category,
				'relDeleted' => false,
				'relActive' => true
			]);
		};

		return $this;
	}

	public function forProduct(Product $product)
	{
		$this->filter[] = function(QueryBuilder $qb) use ($product) {
			$qb->innerJoin(ProductAttribute::class, 'PA', Kdyby\Doctrine\Dql\Join::WITH, 'PA.attribute = A.id');

			$qb->andWhere($qb->expr()->eq('PA.product', ':product'));
			$qb->andWhere($qb->expr()->eq('PA.deleted', ':relDeleted'));
			$qb->andWhere($qb->expr()->eq('PA.active', ':relActive'));

			$qb->setParameters([
				'product' => $product,
				'relDeleted' => false,
				'relActive' => true
			]);
		};

		return $this;
	}

	/**
	 * @param \Kdyby\Persistence\Queryable $repository
	 * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
	 */
	protected function doCreateQuery(Kdyby\Persistence\Queryable $repository)
	{
		$qb = $repository->createQueryBuilder()
				->select('A')
				->from(Attribute::class, 'A');

		$qb->andWhere($qb->expr()->eq('A.deleted', ':deleted'));
		$qb->andWhere($qb->expr()->eq('A.active', ':active'));

		$qb->setParameters([
			'deleted' => false,
			'active' => true
		]);

		foreach ($this->filter as $modifier) {
			$modifier($qb);
		}

		return $qb;
	}
}