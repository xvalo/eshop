<?php

namespace App\Attribute\Image;

use App\File\Image;
use App\File\ImageRepository;
use App\File\ImageService;
use App\Model\Services\ApplicationConfigurationService;
use App\Product\Product;
use Nette\Http\FileUpload;
use Nette\SmartObject;

class ImageManager
{
	use SmartObject;

	/** @var string */
	private $storageName;

	/** @var ImageService */
	private $imageService;

	/** @var ImageRepository */
	private $imageRepository;

	public function __construct(
		ImageService $imageService,
		ImageRepository $imageRepository,
		ApplicationConfigurationService $applicationConfigurationService
	){
		$this->imageService = $imageService;
		$this->imageRepository = $imageRepository;
		$this->storageName = $applicationConfigurationService->getAttributeImagesStorage();
	}

	/**
	 * @param FileUpload $data
	 * @return Image
	 * @throws \Nette\Utils\UnknownImageFileException
	 */
	public function createImage(FileUpload $data)
	{
		return $this->imageService->createImage($data, $this->storageName, false);
	}

	/**
	 * @param Product $product
	 * @param $imageId
	 * @throws \Exception
	 */
	public function removeImage(Product $product, $imageId)
	{
		/** @var Image $image */
		$image = $this->imageRepository->find($imageId);

		if ($image->getId() == $product->getMainImage()->getId()) {
			$product->setMainImage(null);
		}

		$product->removeImage($image);
		$this->imageService->removeImage($image);
	}
}