<?php

namespace App\Attribute;

use App\Attribute\Category\CategoryAttribute;
use App\Attribute\Core\Attribute;
use App\Attribute\Core\AttributeVariant;
use App\Attribute\Product\CategoryAttributeVariant;
use App\Attribute\Product\ProductAttribute;
use App\Attribute\Product\ProductAttributeVariant;
use App\Category\Entity\Category;
use App\InvalidArgumentException;
use App\Product\Product;
use Doctrine\DBAL\Connection;
use Kdyby\Doctrine\EntityManager;

class AttributeFacade
{
	/** @var \Kdyby\Doctrine\EntityRepository  */
	private $attributeRepository;

	/** @var \Kdyby\Doctrine\EntityRepository  */
	private $attributeVariantRepository;

	/** @var EntityManager */
	private $em;

	public function __construct(
		EntityManager $em
	){
		$this->em = $em;
		$this->attributeRepository = $em->getRepository(Attribute::class);
		$this->attributeVariantRepository = $this->em->getRepository(AttributeVariant::class);
	}

	/**
	 * @param int $id
	 * @return null|Attribute
	 */
	public function getAttribute(int $id)
	{
		return $this->attributeRepository->find($id);
	}

	/**
	 * @param int $id
	 * @return null|AttributeVariant
	 */
	public function getAttributeVariant(int $id)
	{
		return $this->attributeVariantRepository->find($id);
	}

	public function getAttributeAllVariants(Attribute $attribute)
	{
		return $this->attributeVariantRepository->findBy(['attribute' => $attribute, 'active' => true, 'deleted' => false]);
	}

	/**
	 * @param int $id
	 * @return null|ProductAttribute
	 */
	public function getProductAttribute(int $id)
	{
		return $this->em->getRepository(ProductAttribute::class)->find($id);
	}

	/**
	 * @param int $id
	 * @return null|ProductAttributeVariant
	 */
	public function getAttributeVariantProduct(int $id)
	{
		return $this->em->getRepository(ProductAttributeVariant::class)->find($id);
	}

	/**
	 * @param int $id
	 * @return null|CategoryAttributeVariant
	 */
	public function getAttributeVariantCategory(int $id)
	{
		return $this->em->getRepository(CategoryAttributeVariant::class)->find($id);
	}

	public function getAttributes()
	{
		return $this->attributeRepository->findBy(['deleted' => false]);
	}

	public function getGridSource()
	{
		return $this->attributeRepository->createQueryBuilder('A')
					->where('A.deleted = 0');
	}

	public function getAttributeVariantsGridSource(Attribute $attribute)
	{
		$qb = $this->em->createQueryBuilder()
			->select('AV')
			->from(AttributeVariant::class, 'AV');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('AV.attribute', ':attribute'),
				$qb->expr()->eq('AV.deleted', ':deleted')
			)
		);

		$qb->setParameters([
			'attribute' => $attribute,
			'deleted' => false
		]);

		return $qb;
	}

	/**
	 * @param Attribute $attribute
	 * @param Product $product
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	public function getProductAttributeVariantsGridSource(Attribute $attribute, Product $product)
	{
		$qb = $this->em->createQueryBuilder()
				->select('PAV')
				->from(ProductAttributeVariant::class, 'PAV')
				->join('PAV.productAttribute', 'PA');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('PA.attribute', ':attribute'),
				$qb->expr()->eq('PA.product', ':product'),
				$qb->expr()->eq('PAV.deleted', ':deleted')
			)
		);

		$qb->setParameters([
			'attribute' => $attribute,
			'product' => $product,
			'deleted' => false
		]);

		return $qb;
	}

	public function getCategoryAttributeVariantsGridSource(Attribute $attribute, Category $category)
	{
		$qb = $this->em->createQueryBuilder()
			->select('CAV')
			->from(CategoryAttributeVariant::class, 'CAV')
			->join('CAV.categoryAttribute', 'CA');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('CA.attribute', ':attribute'),
				$qb->expr()->eq('CA.category', ':category'),
				$qb->expr()->eq('CAV.deleted', ':deleted')
			)
		);

		$qb->setParameters([
			'attribute' => $attribute,
			'category' => $category,
			'deleted' => false
		]);

		return $qb;
	}

	/**
	 * @param Product $product
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	public function getGridSourceForProduct(Product $product)
	{
		$qb = $this->em->createQueryBuilder()
			->select('A')
			->from(Attribute::class, 'A')
			->join('A.products', 'PA');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('PA.product', ':product'),
				$qb->expr()->eq('PA.deleted', ':deleted')
			)
		);

		$qb->setParameters([
			'product' => $product,
			'deleted' => false
		]);

		return $qb;
	}

	public function getGridSourceForCategory(Category $category)
	{
		$qb = $this->em->createQueryBuilder()
			->select('A')
			->from(Attribute::class, 'A')
			->join('A.categories', 'CA');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('CA.category', ':category'),
				$qb->expr()->eq('CA.deleted', ':deleted')
			)
		);

		$qb->setParameters([
			'category' => $category,
			'deleted' => false
		]);

		return $qb;
	}

	/**
	 * @param Attribute $attribute
	 * @return Attribute
	 * @throws \Exception
	 */
	public function saveAttribute(Attribute $attribute)
	{
		if (!$attribute->getId()) {
			$this->em->persist($attribute);
		}

		$this->em->flush();
		return $attribute;
	}

	/**
	 * @param AttributeVariant $attributeVariant
	 * @return AttributeVariant
	 * @throws \Exception
	 */
	public function saveAttributeVariant(AttributeVariant $attributeVariant)
	{
		if (!$attributeVariant->getId()) {
			$this->em->persist($attributeVariant);
		}

		$this->em->flush();
		return $attributeVariant;
	}

	public function getAvailableParentAttributesList()
	{
		return $this->attributeRepository->findPairs(['deleted' => 0, 'parent' => null], 'name');
	}

	public function getProductAttributes(Product $product)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('PA')
			->from(ProductAttribute::class, 'PA')
			->join('PA.attribute', 'A')
			->where(
				$qb->expr()->andX(
					$qb->expr()->eq('A.deleted', ':attributeRemoved'),
					$qb->expr()->isNull('A.parent'),
					$qb->expr()->eq('PA.product', ':product'),
					$qb->expr()->eq('PA.deleted', ':productAttributeRemoved'),
					$qb->expr()->eq('PA.active', ':active')
				)
			);

		$qb->setParameters([
			'attributeRemoved' => false,
			'product' => $product,
			'productAttributeRemoved' => false,
			'active' => true
		]);

		return $qb->getQuery()->execute();
	}



	public function isAttachedToCategory(Attribute $attribute, Category $category)
	{
		$qb = $this->em->createQueryBuilder()
				->select('COUNT(CA)')
				->from(CategoryAttribute::class, 'CA');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('CA.attribute', ':attribute'),
				$qb->expr()->eq('CA.category', ':category'),
				$qb->expr()->eq('CA.deleted', ':deleted')
			)
		);

		$qb->setParameters([
			'attribute' => $attribute,
			'category' => $category,
			'deleted' => false
		]);

		return $qb->getQuery()->getSingleScalarResult() > 0;
	}

	public function isAttachedToProduct(Attribute $attribute, Product $product)
	{
		$qb = $this->em->createQueryBuilder()
			->select('COUNT(PA)')
			->from(ProductAttribute::class, 'PA');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('PA.attribute', ':attribute'),
				$qb->expr()->eq('PA.product', ':product'),
				$qb->expr()->eq('PA.deleted', ':deleted')
			)
		);

		$qb->setParameters([
			'attribute' => $attribute,
			'product' => $product,
			'deleted' => false
		]);

		return $qb->getQuery()->getSingleScalarResult() > 0;
	}

	public function getProductsAttributeBelongsTo(Attribute $attribute)
	{
		$qb = $this->em->createQueryBuilder()
			->select('P.id, P.name')
			->from(ProductAttribute::class, 'PA')
			->join('PA.product', 'P');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('PA.attribute', ':attribute'),
				$qb->expr()->eq('PA.deleted', ':deleted')
			)
		);

		$qb->setParameters([
			'attribute' => $attribute,
			'deleted' => false
		]);

		return $qb->getQuery()->execute();
	}

	public function getCategoriesAttributeBelongsTo(Attribute $attribute)
	{
		$qb = $this->em->createQueryBuilder()
			->select('C.id, C.name')
			->from(CategoryAttribute::class, 'CA')
			->join('CA.category', 'C');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('CA.attribute', ':attribute'),
				$qb->expr()->eq('CA.deleted', ':deleted')
			)
		);

		$qb->setParameters([
			'attribute' => $attribute,
			'deleted' => false
		]);

		return $qb->getQuery()->execute();
	}

	public function getAvailableAttributesListForCategory(Category $category)
	{
		$query = '
			SELECT A.id, A.name 
			FROM attribute AS A
			WHERE A.deleted = ? AND A.parent_id IS NULL AND A.id NOT IN (
				SELECT CA.attribute_id 
				FROM category_attribute AS CA
				WHERE CA.category_id = ? AND CA.deleted = ? GROUP BY CA.attribute_id
			)
			';

		$params = [0, $category->getId(), 0];
		$types = [\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT];

		return $this->em->getConnection()->executeQuery($query, $params,$types)->fetchAll();
	}

	public function getAvailableAttributesListForProduct(Product $product)
	{
		$query = '
			SELECT A.id, A.name 
			FROM attribute AS A
			WHERE A.deleted = ? AND A.parent_id IS NULL AND A.id NOT IN (
				SELECT PA.attribute_id 
				FROM product_attribute AS PA
				WHERE PA.product_id = ? AND PA.deleted = ? GROUP BY PA.attribute_id
			)
			';

		$params = [0, $product->getId(), 0];
		$types = [\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT];

		return $this->em->getConnection()->executeQuery($query, $params,$types)->fetchAll();
	}

	public function getAvailableAttributeVariantsListForProduct(Attribute $attribute, Product $product)
	{
		$query = '
			SELECT AV.id, AV.name 
			FROM attribute_variant AS AV
			WHERE AV.attribute_id = ? AND AV.deleted = ? AND AV.id NOT IN (
				SELECT PAV.attribute_variant_id
				FROM product_attribute_variant AS PAV
				JOIN product_attribute AS PA ON PAV.product_attribute_id = PA.id
				WHERE   PA.product_id = ?
					AND PA.attribute_id = ?
					AND PA.deleted = ?
					AND PAV.deleted = ?
				GROUP BY PAV.attribute_variant_id
			)
			';

		$params = [$attribute->getId(), 0, $product->getId(), $attribute->getId(), 0, 0];
		$types = [\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT];

		return $this->em->getConnection()->executeQuery($query, $params, $types)->fetchAll();
	}

	public function getAvailableAttributeVariantsListForCategory(Attribute $attribute, Category $category)
	{
		$query = '
			SELECT AV.id, AV.name 
			FROM attribute_variant AS AV
			WHERE AV.attribute_id = ? AND AV.deleted = ? AND AV.id NOT IN (
				SELECT CAV.attribute_variant_id
				FROM category_attribute_variant AS CAV
				JOIN category_attribute AS CA ON CAV.category_attribute_id = CA.id
				WHERE   CA.category_id = ?
					AND CA.attribute_id = ?
					AND CA.deleted = ?
					AND CAV.deleted = ?
				GROUP BY CAV.attribute_variant_id
			)
			';

		$params = [$attribute->getId(), 0, $category->getId(), $attribute->getId(), 0, 0];
		$types = [\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT];

		return $this->em->getConnection()->executeQuery($query, $params, $types)->fetchAll();
	}

	public function setAttributeProductState(Attribute $attribute, array $productsIds, bool $isActive = null, bool $isForced = null, bool $isRemoved = null)
	{
		$arguments = [];
		$types = [];

		if ($isForced === null && $isActive === null && $isRemoved === null) {
			throw new InvalidArgumentException('At least one change of state has to be set.');
		}

		$query = 'UPDATE product_attribute SET ';

		if ($isActive !== null) {
			$active = $isActive ? 1 : 0;
			$query .= 'active = ? ';
			$arguments[] = $active;
			$types[] = \PDO::PARAM_INT;
		}

		if ($isForced !== null) {
			$forced = $isForced ? 1 : 0;
			$query .= (count($arguments) > 0 ? ', ' : '').'forced = ? ';
			$arguments[] = $forced;
			$types[] = \PDO::PARAM_INT;
		}

		if ($isRemoved !== null) {
			$removed = $isRemoved ? 1 : 0;
			$query .= (count($arguments) > 0 ? ', ' : '').'deleted = ? ';
			$arguments[] = $removed;
			$types[] = \PDO::PARAM_INT;
		}

		$query .= ' WHERE attribute_id = ? AND product_id IN (?)';
		$arguments[] = $attribute->getId();
		$types[] = \PDO::PARAM_INT;
		$arguments[] = $productsIds;
		$types[] = Connection::PARAM_INT_ARRAY;

		return $this->em->getConnection()->executeUpdate($query, $arguments, $types);
	}

	/*
	public function setAttributeVariantProductState(AttributeVariant $attributeVariant, array $products)
	{
		$this->em->getConnection()->beginTransaction(); // suspend auto-commit
		$productAttributeRepository = $this->em->getRepository(ProductAttribute::class);
		$productRepository = $this->em->getRepository(Product::class);
		try {
			foreach ($products as $productId => $data) {

				$productAttribute = $productAttributeRepository->findOneBy(['attribute' => $attributeVariant->getAttribute(), 'product' => $productRepository->find($productId)]);

				$qb = $this->em->createQueryBuilder()
						->update(ProductAttributeVariant::class, 'PAV');
				if ($data['removed'] !== null) {
					$qb->set('PAV.deleted', ':deleted')
						->setParameter('deleted', $data['removed']);
				}
				if ($data['price'] !== null) {
					$qb->set('PAV.price', ':price')
						->setParameter('price', $data['price']);
				}

				$qb->where(
					$qb->expr()->andX(
						$qb->expr()->eq('PAV.attributeVariant', ':variant'),
						$qb->expr()->eq('PAV.productAttribute', ':attribute')
					)
				);

				$qb->setParameter('variant', $attributeVariant);
				$qb->setParameter('attribute', $productAttribute);
			}
			$this->em->flush();
			$this->em->getConnection()->commit();
		} catch (\Exception $e) {
			$this->em->getConnection()->rollBack();
			throw $e;
		}
	}
	*/

	public function deleteAttributeVariantFromProduct(AttributeVariant $attributeVariant, array $productAttributesIds = [])
	{
		$query = 'UPDATE product_attribute_variant SET deleted = ? WHERE attribute_variant_id = ?';

		$arguments = [
			1,
			$attributeVariant->getId()
		];

		$types = [
			\PDO::PARAM_INT,
			\PDO::PARAM_INT
		];

		if (count($productAttributesIds) > 0) {
			$query .= ' AND product_attribute_id IN (?)';
			$arguments[] = $productAttributesIds;
			$types[] = Connection::PARAM_INT_ARRAY;
		}

		return $this->em->getConnection()->executeUpdate($query, $arguments, $types);
	}

	public function deleteAttributeVariantFromCategory(AttributeVariant $attributeVariant, array $categoryAttributesIds = [])
	{
		$query = 'UPDATE category_attribute_variant SET deleted = ? WHERE attribute_variant_id = ?';

		$arguments = [
			1,
			$attributeVariant->getId()
		];
		$types = [
			\PDO::PARAM_INT,
			\PDO::PARAM_INT
		];

		if (count($categoryAttributesIds) > 0) {
			$query .= ' AND category_attribute_id IN (?)';
			$arguments[] = $categoryAttributesIds;
			$types[] = Connection::PARAM_INT_ARRAY;
		}

		return $this->em->getConnection()->executeUpdate($query, $arguments, $types);
	}

	public function setAttributeCategoryState(Attribute $attribute, array $categoriesIds, bool $isActive = null, bool $isForced = null, bool $isRemoved = null)
	{
		$arguments = [];
		$types = [];

		if ($isForced === null && $isActive === null && $isRemoved === null) {
			throw new InvalidArgumentException('At least one change of state has to be set.');
		}

		$query = 'UPDATE category_attribute SET ';

		if ($isActive !== null) {
			$active = $isActive ? 1 : 0;
			$query .= 'active = ? ';
			$arguments[] = $active;
			$types[] = \PDO::PARAM_INT;
		}

		if ($isForced !== null) {
			$forced = $isForced ? 1 : 0;
			$query .= (count($arguments) > 0 ? ', ' : '').'forced = ? ';
			$arguments[] = $forced;
			$types[] = \PDO::PARAM_INT;
		}

		if ($isRemoved !== null) {
			$removed = $isRemoved ? 1 : 0;
			$query .= (count($arguments) > 0 ? ', ' : '').'deleted = ? ';
			$arguments[] = $removed;
			$types[] = \PDO::PARAM_INT;
		}

		$query .= ' WHERE attribute_id = ? AND category_id IN (?)';
		$arguments[] = $attribute->getId();
		$types[] = \PDO::PARAM_INT;
		$arguments[] = $categoriesIds;
		$types[] = Connection::PARAM_INT_ARRAY;

		return $this->em->getConnection()->executeUpdate($query, $arguments, $types);
	}

}