<?php

namespace App\Attribute\Product;

use App\Attribute\Core\AttributeVariant;
use App\Model\BaseEntity;
use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ProductAttributeVariant extends BaseEntity
{
	use RemovableTrait;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Attribute\Product\ProductAttribute", inversedBy="attributeVariants")
	 * @ORM\JoinColumn(name="product_attribute_id", referencedColumnName="id")
	 * @var ProductAttribute
	 */
	private $productAttribute;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Attribute\Core\AttributeVariant")
	 * @ORM\JoinColumn(name="attribute_variant_id", referencedColumnName="id")
	 * @var AttributeVariant
	 */
	private $attributeVariant;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
	private $price;

	public function __construct(ProductAttribute $productAttribute, AttributeVariant $attributeVariant, float $price)
	{
		$this->productAttribute = $productAttribute;
		$this->attributeVariant = $attributeVariant;
		$this->price = $price;
	}

	/**
	 * @return ProductAttribute
	 */
	public function getProductAttribute(): ProductAttribute
	{
		return $this->productAttribute;
	}

	/**
	 * @return AttributeVariant
	 */
	public function getAttributeVariant(): AttributeVariant
	{
		return $this->attributeVariant;
	}

	/**
	 * @return float
	 */
	public function getPrice(): float
	{
		return $this->price;
	}

	/**
	 * @param float $price
	 */
	public function setPrice(float $price)
	{
		$this->price = $price;
	}
}