<?php

namespace App\Attribute\Product;

use App\Attribute\Core\Attribute;
use App\Attribute\Core\AttributeVariant;
use App\InvalidArgumentException;
use App\Model\BaseEntity;
use App\Product\Product;
use App\Traits\ActivityTrait;
use App\Traits\RemovableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ProductAttribute extends BaseEntity
{
	use ActivityTrait;
	use RemovableTrait;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Product\Product")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
	 * @var Product
	 */
	private $product;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Attribute\Core\Attribute", inversedBy="products")
	 * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
	 * @var Attribute
	 */
	private $attribute;

	/**
	 * @ORM\OneToMany(targetEntity="App\Attribute\Product\ProductAttributeVariant", mappedBy="productAttribute", cascade={"persist"})
	 * @var ProductAttributeVariant[]
	 */
	private $attributeVariants;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	protected $forced = false;

	public function __construct(Product $product, Attribute $attribute, bool $forced)
	{
		$this->product = $product;
		$this->attribute = $attribute;
		$this->forced = $forced;
		$this->attributeVariants = new ArrayCollection();
	}

	/**
	 * @return bool
	 */
	public function isForced(): bool
	{
		return $this->forced;
	}

	/**
	 * @param bool $forced
	 */
	public function setForced(bool $forced)
	{
		$this->forced = $forced;
	}

	/**
	 * @return Product
	 */
	public function getProduct(): Product
	{
		return $this->product;
	}

	/**
	 * @return Attribute
	 */
	public function getAttribute(): Attribute
	{
		return $this->attribute;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getVariants()
	{
		$criteria = Criteria::create()->where(
			Criteria::expr()->eq('deleted', false)
		);
		return $this->attributeVariants->matching($criteria);
	}

	/**
	 * @param AttributeVariant $attributeVariant
	 * @param float|null $price
	 */
	public function addAttributeVariant(AttributeVariant $attributeVariant, float $price = null)
	{
		$this->attributeVariants->add(
			new ProductAttributeVariant(
				$this,
				$attributeVariant,
				$price?:$attributeVariant->getPrice()
			)
		);
	}

	/**
	 * @param AttributeVariant $attributeVariant
	 * @param float|null $price
	 */
	public function changeVariantPrice(AttributeVariant $attributeVariant, float $price = null)
	{
		$productAttributeVariant = $this->getProductAttributeVariant($attributeVariant);
		$productAttributeVariant->setPrice($price);
	}

	/**
	 * @param AttributeVariant $attributeVariant
	 */
	public function removeAttributeVariant(AttributeVariant $attributeVariant)
	{
		$productAttributeVariant = $this->getProductAttributeVariant($attributeVariant);
		$productAttributeVariant->delete();
	}

	/**
	 * @param AttributeVariant $attributeVariant
	 * @return ProductAttributeVariant
	 */
	private function getProductAttributeVariant(AttributeVariant $attributeVariant): ProductAttributeVariant
	{
		$criteria = Criteria::create()->where(
			Criteria::expr()->eq('attributeVariant', $attributeVariant)
		);
		/** @var ProductAttributeVariant $productAttributeVariant */
		$productAttributeVariant = $this->getVariants()->matching($criteria)->first();

		if (!$productAttributeVariant) {
			throw new InvalidArgumentException('Given Attribute Variant is not related to this attribute and product');
		}

		return $productAttributeVariant;
	}
}