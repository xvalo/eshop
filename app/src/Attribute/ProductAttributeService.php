<?php

namespace App\Attribute;

use App\Attribute\Core\Attribute;
use App\Attribute\Core\AttributeVariant;
use App\Attribute\Product\ProductAttribute;
use App\Attribute\Product\ProductAttributeVariant;
use App\Category\CategoryFacade;
use App\Product\Image\ImageResizeManager;
use App\Product\Product;
use Ublaboo\ImageStorage\ImageStorage;

class ProductAttributeService
{
	/*** @var CategoryFacade */
	private $categoryFacade;

	/** @var ImageStorage */
	private $imageStorage;

	/** @var ImageResizeManager */
	private $imageResizeManager;

	/** @var AttributeFacade */
	private $attributeFacade;

	public function __construct(
		AttributeFacade $attributeFacade,
		CategoryFacade $categoryFacade,
		ImageStorage $imageStorage,
		ImageResizeManager $imageResizeManager
	){
		$this->categoryFacade = $categoryFacade;
		$this->imageStorage = $imageStorage;
		$this->imageResizeManager = $imageResizeManager;
		$this->attributeFacade = $attributeFacade;
	}

	/**
	 * @param array $attributes
	 * @param Product $product
	 * @throws \Exception
	 */
	public function attachAttributes(array $attributes, Product $product)
	{
		foreach ($attributes as $id => $attributeData) {
			if ($attributeData['attach']) {
				$attribute = $this->attributeFacade->getAttribute((int)$id);
				$attribute->attachToProduct($product, $attributeData['force']);
				if ($attribute->hasChildren()) {
					/** @var Attribute $child */
					foreach ($attribute->getChildren() as $child) {
						$child->attachToProduct($product, $attributeData['force']);
					}
				}
				$this->attributeFacade->saveAttribute($attribute);
			}
		}
	}

	/**
	 * @param Attribute $attribute
	 * @param Product $product
	 * @param array $variants
	 * @throws \Exception
	 */
	public function attachVariants(Attribute $attribute, Product $product, array $variants)
	{
		foreach ($variants as $id => $data) {
			$attributeVariant = $this->attributeFacade->getAttributeVariant((int)$id);
			$productAttribute = $attribute->getProductRelation($product);
			$productAttribute->addAttributeVariant($attributeVariant, (float)$data['price']);
		}

		$this->attributeFacade->saveAttribute($attribute);
	}

	/**
	 * @param Attribute $attribute
	 * @param Product $product
	 */
	public function removeAttribute(Attribute $attribute, Product $product)
	{
		$this->attributeFacade->setAttributeProductState($attribute, [$product->getId()], null, null, true);
	}

	public function removeAttributeVariant(int $attributeVariantProductId)
	{
		$productAttributeVariant = $this->attributeFacade->getAttributeVariantProduct($attributeVariantProductId);
		$this->attributeFacade->deleteAttributeVariantFromProduct($productAttributeVariant->getAttributeVariant(), [$productAttributeVariant->getProductAttribute()->getId()]);
	}

	public function changeAttributeState(Attribute $attribute, Product $product, bool $isActive, bool $isForced)
	{
		$this->attributeFacade->setAttributeProductState($attribute, [$product->getId()], $isActive, $isForced);
	}

	public function getProductAttributes(Product $product)
	{
		return $this->attributeFacade->getProductAttributes($product);
	}

	/**
	 * @param int $id
	 * @return ProductAttributeVariant|null
	 */
	public function getProductAttributeVariant(int $id)
	{
		return $this->attributeFacade->getAttributeVariantProduct($id);
	}

	public function getAvailableAttributeVariantsList(Attribute $attribute, Product $product)
	{
		$availableVariants = $this->attributeFacade->getAvailableAttributeVariantsListForProduct($attribute, $product);

		$list = [];

		/** @var Attribute $attribute */
		foreach ($availableVariants as $data) {
			$list[$data['id']] = $data['name'];
		}

		return $list;
	}

	public function setPriceToProductAttributeVariants(array $variants, float $price)
	{
		/** @var ProductAttributeVariant $variant */
		foreach ($variants as $variant) {
			$variant->setPrice($price);
		}

		$this->attributeFacade->saveAttribute($variant->getAttributeVariant()->getAttribute());
	}

	/**
	 * @param int $productAttributeId
	 * @return array
	 */
	public function getProductAttributeVariants($productAttributeId)
	{
		$data = [
			'id' => $productAttributeId
		];

		$productAttribute = $this->attributeFacade->getProductAttribute((int) $productAttributeId);

		if ($productAttribute->getAttribute()->hasChildren()) {
			$data['categories'] = [];
			$product = $productAttribute->getProduct();

			/** @var Attribute $child */
			foreach ($productAttribute->getAttribute()->getChildren() as $child) {
				$subAttributeData = [
					'name' => (string)$child,
					'items' => []
				];
				/** @var ProductAttributeVariant $variant */
				foreach ($this->attributeFacade->getProductAttributeVariantsGridSource($child, $product)->getQuery()->execute() as $variant) {
					$subAttributeData['items'][] = [
						"id" => $variant->getId(),
						"title" => $variant->getAttributeVariant()->getName(),
						"price" => $variant->getPrice(),
						"imgSrc" => $variant->getAttributeVariant()->getImage() ? $this->imageResizeManager->createAttributeThumbImage($variant->getAttributeVariant()->getImage()->getRelativePath()) : null,
						"imgSrcPreview" => $variant->getAttributeVariant()->getImage() ? $variant->getAttributeVariant()->getImage()->getRelativePath() : null
					];
				}

				$data['categories'][] = $subAttributeData;
			}


		} else {
			$data['items'] = [];
			/** @var ProductAttributeVariant $variant */
			foreach ($productAttribute->getVariants() as $variant) {
				$data['items'][] = [
					"id" => $variant->getId(),
					"title" => $variant->getAttributeVariant()->getName(),
					"price" => $variant->getPrice(),
					"imgSrc" => $variant->getAttributeVariant()->getImage() ? $this->imageResizeManager->createAttributeThumbImage($variant->getAttributeVariant()->getImage()->getRelativePath()) : null,
					"imgSrcPreview" => $variant->getAttributeVariant()->getImage() ? $variant->getAttributeVariant()->getImage()->getRelativePath() : null
				];
			}
		}

		return $data;
	}
}