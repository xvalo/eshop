<?php

namespace App\Attribute\Core;

use App\File\Image;
use App\Model\BaseEntity;
use App\Traits\ActivityTrait;
use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class AttributeVariant extends BaseEntity
{
	use ActivityTrait;
	use RemovableTrait;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Attribute\Core\Attribute", inversedBy="variants")
	 * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
	 * @var Attribute
	 */
	private $attribute;

	/**
	 * @ORM\Column(type="string", length=60)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
	private $price;

	/**
	 * @ORM\OneToOne(targetEntity="\App\File\Image", cascade={"persist"})
	 * @ORM\JoinColumn(name="image_id", referencedColumnName="id", nullable=true)
	 * @var Image|null
	 */
	private $image;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $isDiscount = false;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $isNews = false;

	public function __construct(Attribute $attribute, string $name, float $price, Image $image = null, bool $isDiscount = false, bool $isNews = false)
	{
		$this->attribute = $attribute;
		$this->name = $name;
		$this->price = $price;
		$this->image = $image;
		$this->isDiscount = $isDiscount;
		$this->isNews = $isNews;
	}

	/**
	 * @return Attribute
	 */
	public function getAttribute(): Attribute
	{
		return $this->attribute;
	}

	/**
	 * @param Attribute $attribute
	 */
	public function setAttribute(Attribute $attribute)
	{
		$this->attribute = $attribute;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return float
	 */
	public function getPrice(): float
	{
		return $this->price;
	}

	/**
	 * @param float $price
	 */
	public function setPrice(float $price)
	{
		$this->price = $price;
	}

	/**
	 * @return Image|null
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * @param Image|null $image
	 */
	public function setImage(Image $image = null)
	{
		$this->image = $image;
	}

	/**
	 * @return bool
	 */
	public function isDiscount(): bool
	{
		return $this->isDiscount;
	}

	/**
	 * @param bool $isDiscount
	 */
	public function setIsDiscount(bool $isDiscount)
	{
		$this->isDiscount = $isDiscount;
	}

	/**
	 * @return bool
	 */
	public function isNews(): bool
	{
		return $this->isNews;
	}

	/**
	 * @param bool $isNews
	 */
	public function setIsNews(bool $isNews)
	{
		$this->isNews = $isNews;
	}

}