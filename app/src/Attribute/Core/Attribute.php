<?php

namespace App\Attribute\Core;

use App\Attribute\Category\CategoryAttribute;
use App\Attribute\Product\ProductAttribute;
use App\Attribute\StringParameterTooLongException;
use App\Category\Entity\Category;
use App\InvalidArgumentException;
use App\Model\BaseEntity;
use App\Product\Product;
use App\Traits\RemovableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;
use Nette\Utils\Strings;

/**
 * @ORM\Entity
 */
class Attribute extends BaseEntity
{
	use RemovableTrait;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Attribute\Core\Attribute", inversedBy="children")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
	 * @var Attribute
	 */
	private $parent;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Attribute\Core\Attribute", mappedBy="parent")
	 * @var Attribute[]
	 */
	private $children;

	/**
	 * @ORM\Column(type="string", length=60)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=60, nullable=true)
	 * @var string|null
	 */
	private $title;

	/**
	 * @ORM\Column(type="string", length=200, nullable=true)
	 * @var string
	 */
	private $description;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Attribute\Core\AttributeVariant", mappedBy="attribute", cascade={"persist"})
	 * @var AttributeVariant[]
	 */
	private $variants;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Attribute\Product\ProductAttribute", mappedBy="attribute", cascade={"persist"})
	 * @var ProductAttribute[]
	 */
	private $products;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Attribute\Category\CategoryAttribute", mappedBy="attribute", cascade={"persist"})
	 * @var CategoryAttribute[]
	 */
	private $categories;

	public function __construct(string $name, string $title = null, string $description = null, Attribute $parent = null)
	{
		$this->name = $name;
		$this->description = $description;
		$this->parent = $parent;
		$this->title = $title;
		$this->children = new ArrayCollection();
		$this->variants = new ArrayCollection();
		$this->products = new ArrayCollection();
		$this->categories = new ArrayCollection();
	}

	/**
	 * @return Attribute|null
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * @param Attribute|null $parent
	 */
	public function setParent(Attribute $parent = null)
	{
		$this->parent = $parent;
	}

	public function getChildren()
	{
		return $this->children;
	}

	/**
	 * @return bool
	 */
	public function hasChildren(): bool
	{
		return !$this->children->isEmpty();
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getCompleteName(): string
	{
		return $this->getParent() !== null ? $this->getParent()->getName() . ' ('.$this->getName().')' : $this->getName();
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		if (Strings::length($name) > 60) {
			throw new StringParameterTooLongException();
		}

		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title)
	{
		$this->title = $title;
	}

	/**
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string|null $description
	 */
	public function setDescription(string $description = null)
	{
		if ($description !== null && Strings::length($description) > 200) {
			throw new StringParameterTooLongException();
		}

		$this->description = $description;
	}

	/**
	 * @return ReadOnlyCollectionWrapper
	 */
	public function getVariants()
	{
		$criteria = Criteria::create()->where(
			Criteria::expr()->andX(
				Criteria::expr()->eq('deleted', 0)
			)
		);

		return new ReadOnlyCollectionWrapper($this->variants->matching($criteria));
	}

	/**
	 * @param AttributeVariant $attributeVariant
	 */
	public function addVariant(AttributeVariant $attributeVariant)
	{
		$this->variants->add($attributeVariant);
	}

	/**
	 * @param AttributeVariant $attributeVariant
	 */
	public function removeVariant(AttributeVariant $attributeVariant)
	{
		/**
		 * @var int $key
		 * @var AttributeVariant $variant
		 */
		foreach ($this->variants as $key => $variant) {
			if ($variant->getName() == $attributeVariant->getName() && $variant->getAttribute() == $attributeVariant->getAttribute()) {
				$this->variants->remove($key);
				return;
			}
		}
	}

	public function attachToProduct(Product $product, bool $isForced)
	{
		$this->products->add(new ProductAttribute($product, $this, $isForced));
	}

	public function attachToCategory(Category $category, bool $isForced)
	{
		$this->categories->add(new CategoryAttribute($category, $this, $isForced));
	}

	/**
	 * @param Product $product
	 * @return ProductAttribute|null
	 */
	public function getProductRelation(Product $product)
	{
		$criteria = Criteria::create();
		return $this->products->matching(
			$criteria->where(
				Criteria::expr()->andX(
					Criteria::expr()->eq('product', $product),
					Criteria::expr()->eq('deleted', false)
				)
			)
		)->first();
	}

	/**
	 * @param Category $category
	 * @return CategoryAttribute|null
	 */
	public function getCategoryRelation(Category $category)
	{
		$criteria = Criteria::create();
		return $this->categories->matching($criteria->where(Criteria::expr()->eq('category', $category)))->first();
	}

	/**
	 * @param Category|Product $entity
	 * @return bool
	 */
	public function isForced($entity)
	{
		if ($entity instanceof Product) {
			$product = $this->getProductRelation($entity);
			return $product->isForced();
		}

		if ($entity instanceof Category) {
			$category = $this->getCategoryRelation($entity);
			return $category->isForced();
		}

		throw new InvalidArgumentException('Invalid entity type or Attribute is not related to this entity');
	}

	/**
	 * @param Category|Product $entity
	 * @return bool
	 */
	public function isActive($entity)
	{
		if ($entity instanceof Product) {
			$product = $this->getProductRelation($entity);
			return $product->isActive();
		}

		if ($entity instanceof Category) {
			$category = $this->getCategoryRelation($entity);
			return $category->isActive();
		}

		throw new InvalidArgumentException('Invalid entity type or Attribute is not related to this entity');
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		if ($this->getTitle() !== null && $this->getTitle() !== "") {
			return $this->getTitle();
		}
		return $this->getCompleteName();
	}
}