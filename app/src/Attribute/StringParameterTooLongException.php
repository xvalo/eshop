<?php

namespace App\Attribute;

use App\ArgumentOutOfRangeException;

class StringParameterTooLongException extends ArgumentOutOfRangeException
{

}