<?php

namespace App\Attribute\Order;

use App\Attribute\Core\AttributeVariant;
use App\Order\Order;
use App\Order\OrderLine\OrderLine;
use App\Product\Product;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class AttributeVariantOrderLine extends OrderLine
{

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Attribute\Core\AttributeVariant")
	 * @ORM\JoinColumn(name="attribute_variant_id", referencedColumnName="id")
	 * @var AttributeVariant
	 */
	private $attributeVariant;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Product\Product")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
	 * @var Product
	 */
	private $product;

	public function __construct(Order $order, AttributeVariant $attributeVariant, Product $product, int $count)
	{
		parent::__construct($order, $attributeVariant->getPrice(), $count, $product->getVat());

		$this->attributeVariant = $attributeVariant;
		$this->product = $product;
	}

	/**
	 * @return AttributeVariant
	 */
	public function getAttributeVariant(): AttributeVariant
	{
		return $this->attributeVariant;
	}

	/**
	 * @param AttributeVariant $attributeVariant
	 */
	public function setAttributeVariant(AttributeVariant $attributeVariant)
	{
		$this->attributeVariant = $attributeVariant;
	}

	/**
	 * @return Product
	 */
	public function getProduct(): Product
	{
		return $this->product;
	}

	/**
	 * @param Product $product
	 */
	public function setProduct(Product $product)
	{
		$this->product = $product;
	}

	public function __toString()
	{
		return $this->attributeVariant->getName();
	}
}