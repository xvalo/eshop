<?php

namespace App\Attribute;

use App\Attribute\Core\Attribute;
use App\Attribute\Core\AttributeVariant;
use App\File\Image;

class AttributeFactory
{
	/**
	 * @param string $name
	 * @param string $title
	 * @param string $description
	 * @param Attribute|null $parent
	 * @return Attribute
	 */
	public static function createAttribute(string $name, string $title, string $description, Attribute $parent = null): Attribute
	{
		return new Attribute($name, $title, $description, $parent);
	}

	/**
	 * @param Attribute $attribute
	 * @param string $name
	 * @param float $price
	 * @param Image|null $image
	 * @param bool $isDiscount
	 * @param bool $isNews
	 * @return AttributeVariant
	 */
	public static function createAttributeVariant(Attribute $attribute, string $name, float $price, Image $image = null, bool $isDiscount = false, bool $isNews = false): AttributeVariant
	{
		return new AttributeVariant($attribute, $name, $price, $image, $isDiscount, $isNews);
	}
}