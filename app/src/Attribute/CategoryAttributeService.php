<?php

namespace App\Attribute;

use App\Attribute\Core\Attribute;
use App\Attribute\Core\AttributeVariant;
use App\Attribute\Product\CategoryAttributeVariant;
use App\Attribute\Product\ProductAttributeVariant;
use App\Category\CategoryFacade;
use App\Category\Entity\Category;
use App\Product\Product;
use App\Product\ProductRepository;

class CategoryAttributeService
{
	/** @var CategoryFacade */
	private $categoryFacade;

	/** @var AttributeFacade */
	private $attributeFacade;

	/** @var ProductRepository */
	private $productRepository;

	/** @var ProductAttributeService */
	private $productAttributeService;

	public function __construct(
		CategoryFacade $categoryFacade,
		AttributeFacade $attributeFacade,
		ProductRepository $productRepository,
		ProductAttributeService $productAttributeService
	){
		$this->categoryFacade = $categoryFacade;
		$this->attributeFacade = $attributeFacade;
		$this->productRepository = $productRepository;
		$this->productAttributeService = $productAttributeService;
	}

	/**
	 * @param array $attributes
	 * @param Category $category
	 * @throws \Exception
	 */
	public function attachAttributes(array $attributes, Category $category)
	{
		$categoriesToAttach = $this->categoryFacade->getCategoryTreeChildrenIds($category);
		$categoriesToAttach[] = $category->getId();

		foreach($categoriesToAttach as $categoryId) {
			$category = $this->categoryFacade->getCategory((int) $categoryId);
			foreach ($attributes as $id => $attributeData) {
				if ($attributeData['attach']) {
					$attribute = $this->attributeFacade->getAttribute((int)$id);
					$attribute->attachToCategory($category, $attributeData['force']);
					if ($attribute->hasChildren()) {
						/** @var Attribute $child */
						foreach ($attribute->getChildren() as $child) {
							$child->attachToProduct($category, $attributeData['force']);
						}
					}
					$this->attributeFacade->saveAttribute($attribute);
				} else {
					unset($attributes[$id]);
				}
			}

			$products = $this->productRepository->getProductsInCategory($category);
			if (count($products)) {
				foreach ($products as $product) {
					$this->productAttributeService->attachAttributes($attributes, $product);
				}
			}
		}
	}

	/**
	 * @param Attribute $attribute
	 * @param Category $category
	 * @param array $variants
	 * @throws \Exception
	 */
	public function attachVariants(Attribute $attribute, Category $category, array $variants)
	{
		$categoriesToAttach = $this->categoryFacade->getCategoryTreeChildrenIds($category);
		$categoriesToAttach[] = $category->getId();

		foreach($categoriesToAttach as $categoryId) {
			$category = $this->categoryFacade->getCategory((int)$categoryId);
			foreach ($variants as $id => $data) {
				$attributeVariant = $this->attributeFacade->getAttributeVariant((int)$id);
				$categoryAttribute = $attribute->getCategoryRelation($category);
				$categoryAttribute->addAttributeVariant($attributeVariant, (float)$data['price']);
			}

			$products = $this->productRepository->getProductsInCategory($category);
			if (count($products)) {
				foreach ($products as $product) {
					$this->productAttributeService->attachVariants($attribute, $product, $variants);
				}
			}
		}

		$this->attributeFacade->saveAttribute($attribute);
	}

	/**
	 * @param Attribute $attribute
	 * @param Category $category
	 */
	public function removeAttribute(Attribute $attribute, Category $category)
	{
		$categoriesIds = $this->categoryFacade->getCategoryTreeChildrenIds($category);
		$categoriesIds[] = $category->getId();
		$this->attributeFacade->setAttributeCategoryState($attribute, $categoriesIds, null, null, true);

		$productsIds = $this->productRepository->getProductsIdsByCategories($categoriesIds);
		$this->attributeFacade->setAttributeProductState($attribute, $productsIds, null, null, true);

	}

	public function removeAttributeVariant(int $attributeVariantId, Category $category)
	{
		$attributeVariant = $this->attributeFacade->getAttributeVariantCategory($attributeVariantId)->getAttributeVariant();
		$categoriesIds = $this->categoryFacade->getCategoryTreeChildrenIds($category);
		$categoriesIds[] = $category->getId();

		$categoryAttributeIds = $productAttributeIds = [];

		foreach($categoriesIds as $categoryId) {
			$category = $this->categoryFacade->getCategory((int)$categoryId);
			$categoryAttribute = $attributeVariant->getAttribute()->getCategoryRelation($category);
			$categoryAttributeIds[] = $categoryAttribute->getId();

			$products = $this->productRepository->getProductsInCategory($category);
			if (count($products)) {
				/** @var Product $product */
				foreach ($products as $product) {
					$productAttributeIds[] = $attributeVariant->getAttribute()->getProductRelation($product)->getId();
				}
			}

		}

		$this->attributeFacade->deleteAttributeVariantFromCategory($attributeVariant, $categoryAttributeIds);
		$this->attributeFacade->deleteAttributeVariantFromProduct($attributeVariant, $productAttributeIds);
	}

	public function changeAttributeState(Attribute $attribute, Category $category, bool $isActive, bool $isForced)
	{
		$categoriesIds = $this->categoryFacade->getCategoryTreeChildrenIds($category);
		$categoriesIds[] = $category->getId();
		$this->attributeFacade->setAttributeCategoryState($attribute, $categoriesIds, $isActive, $isForced);

		$productsIds = $this->productRepository->getProductsIdsByCategories($categoriesIds);
		$this->attributeFacade->setAttributeProductState($attribute, $productsIds, $isActive, $isForced);
	}

	public function getAvailableAttributeVariantsList(Attribute $attribute, Category $category)
	{
		$availableVariants = $this->attributeFacade->getAvailableAttributeVariantsListForCategory($attribute, $category);

		$list = [];

		/** @var Attribute $attribute */
		foreach ($availableVariants as $data) {
			$list[$data['id']] = $data['name'];
		}

		return $list;
	}

	public function setPriceToAllCategoryAttributeVariants(Attribute $attribute, Category $category, float $price)
	{
		$categoriesIds = $this->categoryFacade->getCategoryTreeChildrenIds($category);
		$categoriesIds[] = $category->getId();

		foreach ($categoriesIds as $id) {
			$category = $this->categoryFacade->getCategory((int) $id);
			$categoryRelation = $attribute->getCategoryRelation($category);

			if (!$categoryRelation) {
				continue;
			}

			/** @var CategoryAttributeVariant $variant */
			foreach ($categoryRelation->getVariants() as $variant) {
				$variant->setPrice($price);
			}

			/** @var Product $product */
			foreach ($category->getProducts() as $product) {
				$productRelation = $attribute->getProductRelation($product);

				if (!$productRelation) {
					continue;
				}
				/** @var ProductAttributeVariant $variant */
				foreach ($productRelation->getVariants() as $variant) {
					$variant->setPrice($price);
				}
			}
		}

		$this->attributeFacade->saveAttribute($attribute);
	}
}