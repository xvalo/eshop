<?php

namespace App\Attribute\Product;

use App\Attribute\Category\CategoryAttribute;
use App\Attribute\Core\AttributeVariant;
use App\Model\BaseEntity;
use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class CategoryAttributeVariant extends BaseEntity
{
	use RemovableTrait;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Attribute\Category\CategoryAttribute", inversedBy="attributeVariants")
	 * @ORM\JoinColumn(name="category_attribute_id", referencedColumnName="id")
	 * @var CategoryAttribute
	 */
	private $categoryAttribute;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Attribute\Core\AttributeVariant")
	 * @ORM\JoinColumn(name="attribute_variant_id", referencedColumnName="id")
	 * @var AttributeVariant
	 */
	private $attributeVariant;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
	private $price;

	public function __construct(CategoryAttribute $categoryAttribute, AttributeVariant $attributeVariant, float $price)
	{
		$this->categoryAttribute = $categoryAttribute;
		$this->attributeVariant = $attributeVariant;
		$this->price = $price;
	}

	/**
	 * @return CategoryAttribute
	 */
	public function getCategoryAttribute(): CategoryAttribute
	{
		return $this->categoryAttribute;
	}

	/**
	 * @return AttributeVariant
	 */
	public function getAttributeVariant(): AttributeVariant
	{
		return $this->attributeVariant;
	}

	/**
	 * @return float
	 */
	public function getPrice(): float
	{
		return $this->price;
	}

	/**
	 * @param float $price
	 */
	public function setPrice(float $price)
	{
		$this->price = $price;
	}
}