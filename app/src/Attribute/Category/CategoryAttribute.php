<?php

namespace App\Attribute\Category;

use App\Attribute\Core\Attribute;
use App\Attribute\Core\AttributeVariant;
use App\Attribute\Product\CategoryAttributeVariant;
use App\Category\Entity\Category;
use App\InvalidArgumentException;
use App\Model\BaseEntity;
use App\Traits\ActivityTrait;
use App\Traits\RemovableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class CategoryAttribute extends BaseEntity
{
	use ActivityTrait;
	use RemovableTrait;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Category\Entity\Category")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
	 * @var Category
	 */
	private $category;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Attribute\Core\Attribute", inversedBy="categories")
	 * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
	 * @var Attribute
	 */
	private $attribute;

	/**
	 * @ORM\OneToMany(targetEntity="App\Attribute\Product\CategoryAttributeVariant", mappedBy="categoryAttribute", cascade={"persist"})
	 * @var CategoryAttributeVariant[]
	 */
	private $attributeVariants;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	protected $forced = false;

	public function __construct(Category $category, Attribute $attribute, bool $forced)
	{
		$this->category = $category;
		$this->attribute = $attribute;
		$this->forced = $forced;
		$this->attributeVariants = new ArrayCollection();
	}

	/**
	 * @return bool
	 */
	public function isForced(): bool
	{
		return $this->forced;
	}

	/**
	 * @param bool $forced
	 */
	public function setForced(bool $forced)
	{
		$this->forced = $forced;
	}

	/**
	 * @return Category
	 */
	public function getCategory(): Category
	{
		return $this->category;
	}

	/**
	 * @return Attribute
	 */
	public function getAttribute(): Attribute
	{
		return $this->attribute;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getVariants()
	{
		$criteria = Criteria::create()->where(
			Criteria::expr()->eq('deleted', false)
		);
		return $this->attributeVariants->matching($criteria);
	}

	/**
	 * @param AttributeVariant $attributeVariant
	 * @param float|null $price
	 */
	public function addAttributeVariant(AttributeVariant $attributeVariant, float $price = null)
	{
		$this->attributeVariants->add(
			new CategoryAttributeVariant(
				$this,
				$attributeVariant,
				$price?:$attributeVariant->getPrice()
			)
		);
	}

	/**
	 * @param AttributeVariant $attributeVariant
	 * @param float|null $price
	 */
	public function changeVariantPrice(AttributeVariant $attributeVariant, float $price = null)
	{
		$categoryAttributeVariant = $this->getCategoryAttributeVariant($attributeVariant);
		$categoryAttributeVariant->setPrice($price);
	}

	/**
	 * @param AttributeVariant $attributeVariant
	 */
	public function removeAttributeVariant(AttributeVariant $attributeVariant)
	{
		$categoryAttributeVariant = $this->getCategoryAttributeVariant($attributeVariant);
		$categoryAttributeVariant->delete();
	}

	/**
	 * @param AttributeVariant $attributeVariant
	 * @return CategoryAttributeVariant
	 */
	private function getCategoryAttributeVariant(AttributeVariant $attributeVariant): CategoryAttributeVariant
	{
		$criteria = Criteria::create()->where(
			Criteria::expr()->eq('attributeVariant', $attributeVariant)
		);
		/** @var CategoryAttributeVariant $categoryAttributeVariant */
		$categoryAttributeVariant = $this->getVariants()->matching($criteria)->first();

		if (!$categoryAttributeVariant) {
			throw new InvalidArgumentException('Given Attribute Variant is not related to this attribute and category');
		}

		return $categoryAttributeVariant;
	}
}