<?php

namespace App\Page;

use App\Page\Entity\Page;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class PageFacade
{
	use SmartObject;

	/*** @var EntityManager */
	private $em;

	/** @var \Kdyby\Doctrine\EntityRepository  */
	private $pageRepository;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$this->pageRepository = $this->em->getRepository(Page::class);
	}

	/**
	 * @param int $id
	 * @return null|Page
	 */
	public function getPageById(int $id)
	{
		return $this->pageRepository->find($id);
	}

	public function getPageByUrl(string $url)
	{
		return $this->pageRepository->findOneBy(['url' => $url]);
	}

	public function getGridSource()
	{
		return $this->pageRepository->createQueryBuilder('P');
	}

	/**
	 * @param Page $page
	 * @throws \Exception
	 */
	public function savePage(Page $page)
	{
		if (!$page->getId()) {
			$this->em->persist($page);
		}

		$this->em->flush();
	}

	public function getActivePages()
	{
		$qb = $this->em->createQueryBuilder();
		return $qb->select('P')
				->from(Page::class, 'P')
				->where(
					$qb->expr()->andX(
						$qb->expr()->eq('P.active', ':active'),
						$qb->expr()->orX(
							$qb->expr()->eq('P.inFooter', ':inFooter'),
							$qb->expr()->eq('P.inMenu', ':inMenu')
						)
					)
				)
				->setParameters([
					'inMenu' => true,
					'inFooter' => true,
					'active' => true
				])
				->orderBy('P.pageOrder', 'ASC')
				->getQuery()->getResult();
	}

	/**
	 * @return array
	 */
	public function getHeaderPages(): array
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('P.title, P.url')
			->from(Page::class, 'P')
			->where(
				$qb->expr()->andX(
					$qb->expr()->eq('P.active', ':active'),
					$qb->expr()->eq('P.inMenu', ':inMenu')
				)
			)
			->setParameters([
				'inMenu' => true,
				'active' => true
			])
			->orderBy('P.pageOrder', 'ASC');

		return $qb->getQuery()->getArrayResult();
	}

	/**
	 * @return array
	 */
	public function getFooterPages(): array
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('P.title, P.url')
			->from(Page::class, 'P')
			->where(
				$qb->expr()->andX(
					$qb->expr()->eq('P.active', ':active'),
					$qb->expr()->eq('P.inFooter', ':inFooter')
				)
			)
			->setParameters([
				'inFooter' => true,
				'active' => true
			])
			->orderBy('P.pageOrder', 'ASC');

		return $qb->getQuery()->getArrayResult();
	}
}