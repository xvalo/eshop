<?php


namespace App\Page;

use App\Page\Entity\Page;
use Nette\SmartObject;

class PageService
{
	use SmartObject;

	/** @var PageFacade */
	private $pageFacade;

	public function __construct(PageFacade $pageFacade)
	{
		$this->pageFacade = $pageFacade;
	}

	public function getPage($key)
	{
		if (is_numeric($key)) {
			return $this->pageFacade->getPageById((int) $key);
		} else if (is_string($key)) {
			return $this->pageFacade->getPageByUrl($key);
		}

		throw new NoPageFoundException('Page was not found');
	}

	public function updatePage(Page $page, array $data)
	{
		$page->setTitle($data['title']);
		$page->setContent($data['content']);
		$page->setInMenu($data['inMenu']);
		$page->setInFooter($data['inFooter']);
		$page->setSeoTitle($data['seoTitle']);
		$page->setSeoKeywords($data['seoKeywords']);

		if ($data['active']) {
			$page->activate();
		} else {
			$page->deactivate();
		}

		$this->pageFacade->savePage($page);
	}
}