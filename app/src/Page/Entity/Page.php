<?php

namespace App\Page\Entity;

use App\Model\BaseEntity;
use App\Traits\ActivityTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Page extends BaseEntity
{
	use ActivityTrait;

	/**
	 * @ORM\Column(type="string", length=100)
	 * @var string
	 */
	private $title;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	private $content;

	/**
	 * @ORM\Column(type="string", length=100)
	 * @var string
	 */
	private $url;

	/**
	 * @ORM\Column(type="string", length=100)
	 * @var string
	 */
	private $seoTitle;

	/**
	 * @ORM\Column(type="string", length=100)
	 * @var string
	 */
	private $seoKeywords;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $inMenu;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $inFooter;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $pageOrder;

	public function __construct(string $title, string $content, string $url, string $seoTitle, string $seoKeywords, bool $inMenu, bool $inFooter)
	{
		$this->title = $title;
		$this->content = $content;
		$this->url = $url;
		$this->seoTitle = $seoTitle;
		$this->seoKeywords = $seoKeywords;
		$this->inMenu = $inMenu;
		$this->inFooter = $inFooter;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title)
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getContent(): string
	{
		return urldecode($this->content);
	}

	/**
	 * @param string $content
	 */
	public function setContent(string $content)
	{
		$this->content = urlencode($content);
	}

	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return $this->url;
	}

	/**
	 * @param string $url
	 */
	public function setUrl(string $url)
	{
		$this->url = $url;
	}

	/**
	 * @return string
	 */
	public function getSeoTitle(): string
	{
		return $this->seoTitle;
	}

	/**
	 * @param string $seoTitle
	 */
	public function setSeoTitle(string $seoTitle)
	{
		$this->seoTitle = $seoTitle;
	}

	/**
	 * @return string
	 */
	public function getSeoKeywords(): string
	{
		return $this->seoKeywords;
	}

	/**
	 * @param string $seoKeywords
	 */
	public function setSeoKeywords(string $seoKeywords)
	{
		$this->seoKeywords = $seoKeywords;
	}

	/**
	 * @return bool
	 */
	public function isInMenu(): bool
	{
		return $this->inMenu ?: false;
	}

	/**
	 * @param bool $inMenu
	 */
	public function setInMenu(bool $inMenu)
	{
		$this->inMenu = $inMenu;
	}

	/**
	 * @return int
	 */
	public function getPageOrder(): int
	{
		return $this->pageOrder;
	}

	/**
	 * @param int $pageOrder
	 */
	public function setPageOrder(int $pageOrder)
	{
		$this->pageOrder = $pageOrder;
	}

	/**
	 * @return bool
	 */
	public function isInFooter(): bool
	{
		return $this->inFooter ?: false;
	}

	/**
	 * @param bool $inFooter
	 */
	public function setInFooter(bool $inFooter)
	{
		$this->inFooter = $inFooter;
	}
}