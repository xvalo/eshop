<?php


namespace App\Page;

use App\InvalidArgumentException;

class NoPageFoundException extends InvalidArgumentException
{

}