<?php

namespace App\Cart;

use App\Attribute\Product\ProductAttributeVariant;
use App\Cart\Entity\Cart;
use App\Cart\Entity\Item;
use App\Product\Price\Price;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class CartFacade
{
	use SmartObject;

	/** @var EntityManager */
	private $em;

	private $cartRepository;

	/** @var \Kdyby\Doctrine\EntityRepository  */
	private $cartItemsRepository;

	public function __construct(EntityManager $entityManager)
	{
		$this->em = $entityManager;
		$this->cartRepository = $this->em->getRepository(Cart::class);
		$this->cartItemsRepository = $this->em->getRepository(Item::class);
	}

	/**
	 * @param string $sessionId
	 * @return Cart|null
	 */
    public function getCartBySessionId(string $sessionId)
    {
        return $this->cartRepository->findOneBy([ 'sessionId' => $sessionId ]);
    }

	/**
	 * @param Cart $cart
	 * @return array
	 */
    public function getUnprocessedCartItems(Cart $cart)
    {
        return $this->cartItemsRepository->findBy([ 'cart' => $cart, 'processed' => false ]);
    }

	/**
	 * @param Cart $cart
	 * @throws \Exception
	 */
	public function saveCart(Cart $cart)
	{
		if (!$cart->getId()) {
			$this->em->persist($cart);
		}

		$this->em->flush();
	}

	/**
	 * @param Cart $cart
	 * @param Price $price
	 * @param string $hash
	 * @return Item|null
	 */
	public function getCartItemByPrice(Cart $cart, Price $price, string $hash)
	{
		return $this->cartItemsRepository->findOneBy([
			'cart' => $cart,
			'price' => $price,
			'processed' => false,
			'hashKey' => $hash
		]);
	}
}