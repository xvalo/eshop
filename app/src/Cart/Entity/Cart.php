<?php

namespace App\Cart\Entity;

use App\Customer\Customer;
use App\InvalidArgumentException;
use App\Lists\Delivery;
use App\Model\BaseEntity;
use App\Order\Payment\Entity\PaymentType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="cart",
 *     indexes={
 *		    @ORM\Index(name="session_index", columns={"session_id"})
 *     },
 *     uniqueConstraints={
 *          @ORM\UniqueConstraint(name="cart_session_unique",columns={"session_id"})
 *     }
 * )
 */
class Cart extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="\App\Customer\Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=true)
     * @var Customer
     */
    private $customer;

    /**
     * @ORM\Column(type="string", length=100)
     * @var string
     */
    private $sessionId;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
	private $totalPrice = 0;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $totalCount = 0;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Cart\Entity\Item", mappedBy="cart", indexBy="id", cascade={"persist"}, orphanRemoval=true)
	 */
	private $items;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Lists\Delivery")
	 * @ORM\JoinColumn(name="delivery_id", referencedColumnName="id", nullable=true)
	 * @var Delivery
	 */
	private $delivery;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\Payment\Entity\PaymentType")
	 * @ORM\JoinColumn(name="payment_type_id", referencedColumnName="id", nullable=true)
	 * @var PaymentType
	 */
	private $paymentType;

	/**
	 * @ORM\OneToOne(targetEntity="\App\Cart\Entity\AppliedDiscount", cascade={"persist"}, orphanRemoval=true)
	 * @ORM\JoinColumn(name="discount_id", referencedColumnName="id", nullable=true)
	 * @var AppliedDiscount|null
	 */
	private $discount;

	/**
	 * Cart constructor.
	 * @param string $sessionId
	 * @param Customer|null $customer
	 */
    public function __construct(string $sessionId, Customer $customer = null)
    {
        $this->sessionId = $sessionId;
        $this->customer = $customer;
        $this->items = new ArrayCollection();
    }

    /**
     * @return Customer|null
     */
    public function getCustomer()
    {
        return $this->customer;
    }

	/**
	 * @param Customer $customer
	 */
	public function setCustomer(Customer $customer)
	{
		$this->customer = $customer;
    }
    /**
     * @return string
     */
    public function getSessionId(): string
    {
        return $this->sessionId;
    }

	/**
	 * @param string $sessionId
	 */
    public function setSessionId(string $sessionId)
    {
        $this->sessionId = $sessionId;
    }

	/**
	 * @return float
	 */
	public function getTotalPrice(): float
	{
		return $this->totalPrice;
	}

	/**
	 * @param float $totalPrice
	 */
	public function setTotalPrice(float $totalPrice)
	{
		$this->totalPrice = $totalPrice;
	}

	/**
	 * @return int
	 */
	public function getTotalCount(): int
	{
		return $this->totalCount;
	}

	/**
	 * @param int $totalCount
	 */
	public function setTotalCount(int $totalCount)
	{
		$this->totalCount = $totalCount;
	}

	/**
	 * @return ReadOnlyCollectionWrapper
	 */
	public function getItems()
	{
		$condition = Criteria::create()->where(Criteria::expr()->eq('processed', false));
		return new ReadOnlyCollectionWrapper($this->items->matching($condition));
	}

	/**
	 * @param Item $item
	 */
	public function addItem(Item $item)
	{
		$this->items->add($item);
	}

	/**
	 * @param Item $item
	 */
	public function removeItem(Item $item)
	{
		$this->items->removeElement($item);
	}

	/**
	 * @param int $id
	 * @return Item
	 */
	public function getItem(int $id): Item
	{
		if (isset($this->items[$id])) {
			return $this->items[$id];
		}

		throw new InvalidArgumentException("Cart doesn't contain item with ID $id");
	}

	/**
	 * @return Delivery|null
	 */
	public function getDelivery()
	{
		return $this->delivery;
	}

	/**
	 * @param Delivery|null $delivery
	 */
	public function setDelivery(Delivery $delivery = null)
	{
		$this->delivery = $delivery;
	}

	/**
	 * @return PaymentType|null
	 */
	public function getPaymentType()
	{
		return $this->paymentType;
	}

	/**
	 * @param PaymentType|null $paymentType
	 */
	public function setPaymentType(PaymentType $paymentType = null)
	{
		$this->paymentType = $paymentType;
	}

	/**
	 * @return AppliedDiscount|null
	 */
	public function getDiscount()
	{
		return $this->discount;
	}

	/**
	 * @param AppliedDiscount|null $discount
	 */
	public function setDiscount(AppliedDiscount $discount = null)
	{
		$this->discount = $discount;
	}

}