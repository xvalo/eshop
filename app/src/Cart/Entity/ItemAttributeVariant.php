<?php

namespace App\Cart\Entity;

use App\Attribute\Product\ProductAttributeVariant;
use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="cart_item_attribute_variant"
 * )
 */
class ItemAttributeVariant extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Cart\Entity\Item", inversedBy="attributesVariants")
	 * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
	 * @var Item
	 */
	private $item;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Attribute\Product\ProductAttributeVariant")
	 * @ORM\JoinColumn(name="attribute_variant_id", referencedColumnName="id")
	 * @var ProductAttributeVariant
	 */
	private $productAttributeVariant;

	public function __construct(Item $item, ProductAttributeVariant $attributeVariant)
	{
		$this->item = $item;
		$this->productAttributeVariant = $attributeVariant;
	}

	/**
	 * @return ProductAttributeVariant
	 */
	public function getProductAttributeVariant(): ProductAttributeVariant
	{
		return $this->productAttributeVariant;
	}
}