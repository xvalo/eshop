<?php

namespace App\Cart\Entity;

use App\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table(name="cart_applied_discount")
 */
class AppliedDiscount
{
	use Identifier;

	const TYPE_PERCENTAGE = 'P';
	const TYPE_AMOUNT = 'A';

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
	private $value;

	/**
	 * @ORM\Column(type="string", length=2)
	 * @var string
	 */
	private $type;

	public function __construct(string $type, float $value)
	{
		if (!$this->isCorretType($type)) {
			throw new InvalidArgumentException('Incorrect discount type');
		}
		$this->value = $value;
		$this->type = $type;
	}

	/**
	 * @return float
	 */
	public function getValue(): float
	{
		return $this->value;
	}

	/**
	 * @param float $value
	 */
	public function setValue(float $value)
	{
		$this->value = $value;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType(string $type)
	{
		if (!$this->isCorretType($type)) {
			throw new InvalidArgumentException('Incorrect discount type');
		}
		$this->type = $type;
	}

	/**
	 * @param float $price
	 * @return float
	 */
	public function getDiscountPrice(float $price): float
	{
		return $this->getType() === self::TYPE_PERCENTAGE
			? floor($price * ($this->getValue()/100))
			: $this->getValue() ;
	}

	private function isCorretType(string $type)
	{
		return in_array($type, [self::TYPE_PERCENTAGE, self::TYPE_AMOUNT]);
	}
}