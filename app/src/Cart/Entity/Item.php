<?php

namespace App\Cart\Entity;

use App\Attribute\Product\ProductAttributeVariant;
use App\Model\BaseEntity;
use App\Product\Price\Price;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="cart_items",
 *     indexes={
 *		    @ORM\Index(name="item_kash_key", columns={"hash_key"})
 *     }
 * )
 */
class Item extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Cart\Entity\Cart", inversedBy="items")
	 * @ORM\JoinColumn(name="cart_id", referencedColumnName="id")
	 * @var Cart
	 */
	private $cart;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Product\Price\Price")
	 * @ORM\JoinColumn(name="variant_price_id", referencedColumnName="id")
	 * @var Price
	 */
	private $price;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $count;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $processed = false;

	/**
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 */
	private $created;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Cart\Entity\ItemAttributeVariant", mappedBy="item", indexBy="id", cascade={"persist"}, orphanRemoval=true)
	 */
	private $attributesVariants;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
	private $itemUnitPrice;


	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	private $hashKey;

	/**
	 * Item constructor.
	 * @param Cart $cart
	 * @param Price $price
	 * @param int $count
	 * @param ProductAttributeVariant[] $attributeVariants
	 */
	public function __construct(Cart $cart, Price $price, int $count, array $attributeVariants = [])
	{
		$this->cart = $cart;
		$this->count = $count;
		$this->price = $price;
		$this->created = new \DateTime();
		$this->attributesVariants = new ArrayCollection();

		$this->itemUnitPrice = $price->getPrice();

		/** @var ProductAttributeVariant $attributeVariant */
		foreach ($attributeVariants as $attributeVariant) {
			$this->attributesVariants->add(new ItemAttributeVariant($this, $attributeVariant));
			$this->itemUnitPrice += $attributeVariant->getPrice();
		}

		$this->hashKey = self::countHash($cart, $price, $attributeVariants);
	}

	/**
	 * @return Cart
	 */
	public function getCart(): Cart
	{
		return $this->cart;
	}

	/**
	 * @return Price
	 */
	public function getPrice(): Price
	{
		return $this->price;
	}

	/**
	 * @return int
	 */
	public function getCount(): int
	{
		return $this->count;
	}

	/**
	 * @param int $count
	 */
	public function setCount(int $count)
	{
		$this->count = $count;
	}

	/**
	 * @param int $count
	 */
	public function add(int $count)
	{
		$this->count += $count;
	}

	/**
	 * @return bool
	 */
	public function isProcessed(): bool
	{
		return $this->processed;
	}

	public function setProcessed()
	{
		$this->processed = true;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreated(): \DateTime
	{
		return $this->created;
	}

	public function getAttributeVariants()
	{
		return $this->attributesVariants;
	}

	/**
	 * @return float
	 */
	public function getItemUnitPrice(): float
	{
		return $this->itemUnitPrice;
	}

	/**
	 * @return float
	 */
	public function getItemTotalPrice(): float
	{
		return $this->getCount() * $this->getItemUnitPrice();
	}

	/**
	 * @return string
	 */
	public function getHashKey(): string
	{
		return $this->hashKey;
	}

	/**
	 * @param Cart $cart
	 * @param Price $price
	 * @param array $attributeVariants
	 * @return string
	 */
	public static function countHash(Cart $cart, Price $price, array $attributeVariants = []): string
	{
		$attributesIds = [];
		/** @var ProductAttributeVariant $attributeVariant */
		foreach ($attributeVariants as $attributeVariant) {
			$attributesIds[] = $attributeVariant->getAttributeVariant()->getId();
		}

		return sha1($cart->getId() . '-' . $price->getId() . '-' . implode('|', $attributesIds));
	}
}