<?php

namespace App\Cart;

use App\Attribute\Product\ProductAttributeVariant;
use App\Cart\Entity\AppliedDiscount;
use App\Cart\Entity\Cart;
use App\Cart\CartFacade;
use App\Cart\Entity\Item;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\Customer\Customer;
use App\Customer\CustomerRepository;
use App\InvalidArgumentException;
use App\Lists\Delivery;
use App\Lists\DeliveryRepository;
use App\Order\Payment\Entity\PaymentType;
use App\Order\Payment\PaymentTypeFacade;
use App\Product\Price\Price;
use App\Utils\Numbers;
use Nette\Http\Session;
use Nette\Security\User;
use Nette\SmartObject;

class CartManager
{
	use SmartObject;

	/* session section to maintain cart during login process */
	const CART_TRANSITION_SESSION = 'cartTransitionSession';
	const CART_EXTRA_ITEMS_SESSION = 'cartExtraItems';

	/** @var CartFacade */
	private $cartFacade;

	/** @var Cart */
	private $cart;

	/** @var string */
	private $sessionId;

	/** @var Session */
	private $session;

	/** @var User */
	private $currentUser;

	/** @var CustomerRepository */
	private $customerRepository;

	/** @var int */
	private $vat;

	/** @var DeliveryRepository */
	private $deliveryRepository;

	/** @var null|Customer  */
	private $customer;

	/** @var PaymentTypeFacade */
	private $paymentTypeFacade;

	function __construct(
		CartFacade $cartFacade,
		CustomerRepository $customerRepository,
		Session $session,
		User $currentUser,
		ApplicationSettingsManager $applicationSettingsManager,
		DeliveryRepository $deliveryRepository,
		PaymentTypeFacade $paymentTypeFacade
	){
		$this->cartFacade = $cartFacade;
		$this->session = $session;
		$this->sessionId = $session->getId();

		$this->currentUser = $currentUser;

		$this->manageCartSessionUpdate();
		$this->customerRepository = $customerRepository;

		$this->vat = $applicationSettingsManager->getDefaultVat();
		$this->deliveryRepository = $deliveryRepository;
		$this->paymentTypeFacade = $paymentTypeFacade;
	}


	public function manageCartSessionUpdate()
	{
		if(!$this->session->hasSection(self::CART_TRANSITION_SESSION)){
			$this->cart = $this->cartFacade->getCartBySessionId($this->sessionId);
			if (!$this->cart) {
				$this->cart = new Cart($this->sessionId, $this->customer);
				$this->cartFacade->saveCart($this->cart);
			}
			return;
		}

		$cartSession = $this->session->getSection(self::CART_TRANSITION_SESSION);
		$this->updateCartSessionId($this->sessionId, $cartSession->oldSessionId);

		$cartSession->remove();
	}

	/**
	 * Updates session id for cart.
	 * Used to maintain cart content when user logs in during order process.
	 * @param string $oldSessionId session id
	 * @param string $newSessionId session id
	 * @throws \Exception
	 */
	public function updateCartSessionId(string $newSessionId, string $oldSessionId)
	{
		$this->cart = $this->cartFacade->getCartBySessionId($oldSessionId);
		$this->cart = $this->cart ?: new Cart($newSessionId, $this->customer);

		$this->cart->setSessionId($newSessionId);
		$this->cartFacade->saveCart($this->cart);
	}

	public function getItems()
	{
		return $this->cart->getItems();
	}


	/**
	 * @param $id
	 * @return Item|NULL
	 */
	public function getItem($id)
	{
		return $this->cart->getItem((int) $id);
	}


	/**
	 * Returns total count of all items in cart.
	 * @return int
	 */
	public function getTotalUnits(): int
	{
		return $this->cart->getTotalCount();
	}


	/**
	 * @return float
	 */
	public function getItemsTotalPrice(): float
	{
		return $this->cart->getTotalPrice();
	}

	/**
	 * @return float
	 */
	public function getTotalPriceWithoutVat(): float
	{
		return Numbers::priceWithoutVat($this->getItemsTotalPrice(), $this->vat);
	}

	/**
	 * @return float
	 */
	public function getTotalPrice(): float
	{
		$additionalPrice = 0;

		$isFreeDeliveryPrice = $this->cart->getDelivery()
			&& $this->cart->getDelivery()->getFreePriceLimit() > 0
			&& $this->getItemsTotalPrice() > $this->cart->getDelivery()->getFreePriceLimit();

		$additionalPrice += $this->cart->getDelivery() && !$isFreeDeliveryPrice ? $this->cart->getDelivery()->getPrice()  : 0;
		$additionalPrice += $this->cart->getPaymentType() && !$isFreeDeliveryPrice ? $this->cart->getDelivery()->getPaymentTypePrice($this->cart->getPaymentType()) : 0;

		return $this->getItemsTotalPrice() + $additionalPrice;
	}

	/**
	 * @return float
	 */
	public function getTotalPriceWithDiscount(): float
	{
		return $this->getTotalPrice() - $this->getDiscount();
	}

	/**
	 * @param array $items
	 * @return void
	 * @throws \Exception
	 */
	public function updateItemsCount(array $items)
	{
		$cartItems = $this->cart->getItems();

		/** @var Item $item */
		foreach($cartItems as $item){
			if ($items[$item->getId()] <= 0) {
				$this->removeItem($item->getId());
			} else {
				$item->setCount((int) $items[$item->getId()]);
			}
		}
	}

	public function updateCartState()
	{
		$itemsTotalPrice = $itemsTotalCount = 0;

		/** @var Item $item */
		foreach ($this->cart->getItems() as $item) {
			if (!$item->isProcessed()) {
				$itemsTotalCount += $item->getCount();
				$itemsTotalPrice += $item->getItemTotalPrice();
			}
		}

		$this->cart->setTotalCount($itemsTotalCount);
		$this->cart->setTotalPrice($itemsTotalPrice);

		if(!$this->customer && $this->currentUser->isLoggedIn()){
			$this->customer = $this->customerRepository->find($this->currentUser->getId());
			$this->cart->setCustomer($this->customer);
		}

		$this->cartFacade->saveCart($this->cart);
	}

	/**
	 * Removes item from cart.
	 * @param $id
	 * @return void
	 * @throws \Exception
	 */
	public function removeItem($id)
	{
		$item = $this->cart->getItem((int) $id);
		$this->cart->removeItem($item);
		$this->updateCartState();
	}

	/**
	 * @param Price $price
	 * @param ProductAttributeVariant[] $attributesVariants
	 * @param int $count
	 * @return Item|null
	 * @throws \Exception
	 */
	public function add(Price $price, array $attributesVariants = [], int $count = 1)
	{
		$cartitemHash = Item::countHash($this->cart, $price, $attributesVariants);
		$cartItem = $this->cartFacade->getCartItemByPrice($this->cart, $price, $cartitemHash);

		if ($cartItem !== null) {
			$cartItem->add($count);
		} else {
			$cartItem = new Item($this->cart, $price, $count, $attributesVariants);
			$this->cart->addItem($cartItem);
		}

		$this->updateCartState();
		return $cartItem;
	}

	/**
	 * @param Delivery|int|string $delivery
	 */
	public function setDelivery($delivery)
	{
		if (!$delivery instanceof Delivery) {
			$delivery = $this->deliveryRepository->find((int)$delivery);
		}

		$this->cart->setDelivery($delivery);
	}

	/**
	 * @param PaymentType|int|string $paymentType
	 */
	public function setPaymentType($paymentType)
	{
		if (!$paymentType instanceof PaymentType) {
			$paymentType = $this->paymentTypeFacade->getPaymentType((int)$paymentType);
		}

		$this->cart->setPaymentType($paymentType);
	}

	/**
	 * @return Delivery|null
	 */
	public function getDelivery()
	{
		return $this->cart->getDelivery();
	}

	/**
	 * @return PaymentType|null
	 */
	public function getPaymentType()
	{
		return $this->cart->getPaymentType();
	}

	/**
	 * @param float $value
	 * @param string $type
	 */
	public function setDiscount(float $value, string $type)
	{
		if ($value < 0) {
			throw new InvalidArgumentException('Discount cannot be lower than zero');
		}

		if ($this->isDiscount()) {
			$this->cart->getDiscount()->setType($type);
			$this->cart->getDiscount()->setValue($value);
		} else {
			$this->cart->setDiscount(new AppliedDiscount($type, $value));
		}
	}

	/**
	 * @return float
	 */
	public function getDiscount(): float
	{
		if (!$this->isDiscount()) {
			return false;
		}
		return $this->cart->getDiscount()->getDiscountPrice($this->getItemsTotalPrice());
	}

	public function removeDiscount()
	{
		$this->cart->setDiscount(null);
	}

	/**
	 * @return AppliedDiscount|null
	 */
	public function getAppliedDiscountObject()
	{
		return $this->cart->getDiscount();
	}

	/**
	 * @return bool
	 */
	public function isDiscount(): bool
	{
		return $this->cart->getDiscount() !== null && $this->cart->getDiscount()->getValue() > 0;
	}

	/**
	 * @param Item $cartItem
	 * @throws \Exception
	 */
	public function markItemProcessed(Item $cartItem)
	{
		$cartItem->setProcessed();
	}

	/**
	 * @return bool
	 */
	public function hasItems()
	{
		return $this->getTotalUnits() > 0;
	}

	public function clearCart()
	{
		/** @var Item $item */
		foreach($this->getItems() as $item){
			$item->setProcessed();
		}

		$this->setPaymentType(null);
		$this->setDelivery(null);
		$this->removeDiscount();

		$this->updateCartState();
	}

}
