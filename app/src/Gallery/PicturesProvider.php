<?php

namespace App\Gallery;

use App\File\Image;
use App\File\ImageRepository;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class PicturesProvider
{
	use SmartObject;

	/**
	 * @var ImageRepository
	 */
	private $repository;

	/**
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em, ImageRepository $repository)
	{
		$this->repository = $repository;
		$this->em = $em;
	}

	/**
	 * @param int $limit
	 * @param int $offset
	 * @param string|null $searchPhrase
	 * @return array
	 */
	public function getPictures(int $limit, int $offset = 0, string $searchPhrase = null): array
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('I.id, I.name, I.path, COUNT(P.id) AS productsCount')
			->from(Image::class, 'I')
			->leftJoin('I.products', 'P')
			->groupBy('I.id')
			->orderBy('I.id', 'DESC')
			->setMaxResults($limit)
			->setFirstResult($offset);
		
		if ($searchPhrase) {
			$qb->where('I.name LIKE :search')
				->setParameter('search', "%$searchPhrase%");
		}

		return $qb->getQuery()->getArrayResult();
	}

	/**
	 * @param string|null $searchPhrase
	 * @return int
	 * @throws \Doctrine\ORM\NoResultException
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function getPicturesCount(string $searchPhrase = null): int
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('COUNT(I.id)')
			->from(Image::class, 'I');

		if ($searchPhrase) {
			$qb->where('I.name LIKE :search')
				->setParameter('search', "%$searchPhrase%");
		}

		return $qb->getQuery()->getSingleScalarResult();
	}

	/**
	 * @param int $id
	 * @return null|Image
	 */
	public function getImage(int $id)
	{
		return $this->repository->find($id);
	}
}