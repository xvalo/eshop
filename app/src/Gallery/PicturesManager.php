<?php

namespace App\Gallery;

use App\File\Image;
use App\File\ImageRepository;
use App\File\ImageService;
use App\Model\Services\ApplicationConfigurationService;
use Nette\Http\FileUpload;
use Nette\SmartObject;

class PicturesManager
{
	use SmartObject;

	/**
	 * @var ImageRepository
	 */
	private $imageRepository;

	/**
	 * @var ImageService
	 */
	private $imageService;

	/**
	 * @var ApplicationConfigurationService
	 */
	private $applicationConfigurationService;

	public function __construct(
		ImageRepository $imageRepository,
		ImageService $imageService,
		ApplicationConfigurationService $applicationConfigurationService
	){
		$this->imageRepository = $imageRepository;
		$this->imageService = $imageService;
		$this->applicationConfigurationService = $applicationConfigurationService;
	}

	/**
	 * @param int $id
	 * @param string $name
	 * @return Image
	 */
	public function updateImageName(int $id, string $name): Image
	{
		/** @var Image $image */
		$image = $this->imageRepository->find($id);
		$image->setName($name);
		$this->imageRepository->saveImage($image);

		return $image;
	}

	public function saveImage(FileUpload $fileUpload, string $name)
	{
		$image = $this->imageService->createImage(
			$fileUpload,
			$this->applicationConfigurationService->getDefaultImageStorage(),
			$this->applicationConfigurationService->isWatermarkAllowed(),
			$name
		);

		$this->imageRepository->saveImage($image);
	}

	public function removeImage(Image $image)
	{

	}
}