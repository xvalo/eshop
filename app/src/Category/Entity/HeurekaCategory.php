<?php

namespace App\Category\Entity;

use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="heureka_category_list"
 * )
 */
class HeurekaCategory
{
	use RemovableTrait;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @var integer
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @var string|null
	 */
	private $fullName;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Category\Entity\HeurekaCategory")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
	 * @var HeurekaCategory|null
	 */
	private $parent;

	public function __construct(int $id, string $name, string $fullName = null, HeurekaCategory $parent = null)
	{
		$this->id = $id;
		$this->name = $name;
		$this->fullName = $fullName;
		$this->parent = $parent;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getFullName()
	{
		return $this->fullName;
	}

	/**
	 * @param string $fullName
	 */
	public function setFullName(string $fullName = null)
	{
		$this->fullName = $fullName;
	}

	/**
	 * @return HeurekaCategory|null
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * @param HeurekaCategory|null $parent
	 */
	public function setParent(HeurekaCategory $parent = null)
	{
		$this->parent = $parent;
	}


}