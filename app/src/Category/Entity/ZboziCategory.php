<?php

namespace App\Category\Entity;

use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="zbozi_category_list"
 * )
 */
class ZboziCategory
{
	use RemovableTrait;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @var integer
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $categoryText;

	public function __construct(int $id, string $name, string $categoryText)
	{
		$this->id = $id;
		$this->name = $name;
		$this->categoryText = $categoryText;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getCategoryText()
	{
		return $this->categoryText;
	}

	/**
	 * @param string $categoryText
	 */
	public function setCategoryText(string $categoryText)
	{
		$this->categoryText = $categoryText;
	}
}