<?php

namespace App\Category\Entity;

use App\Traits\ActivityTrait;
use App\Traits\RemovableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;
use Nette\Utils\Strings;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="category",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="category_url_unique",columns={"parent_id", "url"})},
 *     indexes={@ORM\Index(name="cat_url_idx", columns={"url"})}
 * )
 */
class Category
{
	const MAX_CAT_LEVEL = 4;

	use MagicAccessors;
	use Identifier;
	use ActivityTrait;
	use RemovableTrait;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	private $description;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Category\Entity\Category", inversedBy="children")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
	 */
	private $parent;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Category\Entity\Category", mappedBy="parent")
	 * @ORM\OrderBy({"levelOrder" = "ASC"})
	 */
	private $children;

	/**
	 * @ORM\Column(type="string", length=150)
	 * @var string
	 */
	private $seoTitle;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $seoDescription;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $url;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Product\Product", mappedBy="category")
	 */
	private $products;

	/**
	 * @ORM\Column(type="string", length=50)
	 * @var string
	 */
	private $postscript;

	/**
	 * @ORM\Column(type="integer", length=5)
	 */
	private $levelOrder;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Category\Entity\OldUrl", mappedBy="category", cascade={"persist"})
	 */
	private $oldUrls;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Category\Entity\ZboziCategory")
	 * @ORM\JoinColumn(name="zbozi_category_id", referencedColumnName="id", nullable=true)
	 * @var ZboziCategory|null
	 */
	private $zboziCategory;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Category\Entity\HeurekaCategory")
	 * @ORM\JoinColumn(name="heureka_zbozi_id", referencedColumnName="id", nullable=true)
	 * @var HeurekaCategory|null
	 */
	private $heurekaCategory;

	/**
	 * Category constructor.
	 * @param string $name
	 * @param string $postscript
	 * @param string $description
	 * @param Category|null $parent
	 * @param string $seoTitle
	 * @param string $seoDescription
	 */
	public function __construct(string $name, string $postscript, string $description, Category $parent = null, string $seoTitle = '', $seoDescription = '', ZboziCategory $zboziCategory = null, HeurekaCategory $heurekaCategory = null)
	{
		$this->name = $name;
		$this->description = $description;
		$this->parent = $parent;
		$this->seoTitle = $seoTitle;
		$this->seoDescription = $seoDescription;
		$this->url = Strings::webalize($name);
		$this->postscript = $postscript;

		$this->heurekaCategory = $heurekaCategory;
		$this->zboziCategory = $zboziCategory;

		$this->children = new ArrayCollection();
		$this->products = new ArrayCollection();
		$this->oldUrls = new ArrayCollection();
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @return Category
	 */
	public function getParent()
	{
		return $this->parent;
	}

	public function getSeoTitle()
	{
		return $this->seoTitle;
	}

	public function getSeoDescription()
	{
		return $this->seoDescription;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @param mixed $parent
	 */
	public function setParent($parent)
	{
		$this->parent = $parent;
	}

	/**
	 * @param string $seoTitle
	 */
	public function setSeoTitle($seoTitle)
	{
		$this->seoTitle = $seoTitle;
	}

	/**
	 * @param string $seoDescription
	 */
	public function setSeoDescription($seoDescription)
	{
		$this->seoDescription = $seoDescription;
	}

	public function setUrl($url)
	{
		$this->url = $url;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	public function getChildren()
	{
		return new ReadOnlyCollectionWrapper($this->children);
	}

	public function hasChildren()
	{
		return !$this->children->isEmpty();
	}

	public function getProducts()
	{
		return new ReadOnlyCollectionWrapper($this->products);
	}

	public function getProductsCount()
	{
		return $this->products->count();
	}

	/**
	 * @return string
	 */
	public function getPostscript()
	{
		if (!is_null($this->postscript) && strlen($this->postscript) > 0) {
			return $this->postscript;
		}

		if ($this->getParent() != null) {
			return $this->getParent()->getPostscript();
		}

		return '';
	}

	/**
	 * @param string $postscript
	 */
	public function setPostscript($postscript)
	{
		$this->postscript = $postscript;
	}

	/**
	 * @return mixed
	 */
	public function getLevelOrder()
	{
		return $this->levelOrder;
	}

	/**
	 * @param mixed $levelOrder
	 */
	public function setLevelOrder($levelOrder)
	{
		$this->levelOrder = $levelOrder;
	}

	public function addOldUrl(OldUrl $url)
	{
		$this->oldUrls->add($url);
	}

	/**
	 * @return ZboziCategory|null
	 */
	public function getZboziCategory()
	{
		return $this->zboziCategory;
	}

	/**
	 * @param ZboziCategory|null $zboziCategory
	 */
	public function setZboziCategory(ZboziCategory $zboziCategory = null)
	{
		$this->zboziCategory = $zboziCategory;
	}

	/**
	 * @return HeurekaCategory|null
	 */
	public function getHeurekaCategory()
	{
		return $this->heurekaCategory;
	}

	/**
	 * @param HeurekaCategory|null $heurekaCategory
	 */
	public function setHeurekaCategory(HeurekaCategory $heurekaCategory = null)
	{
		$this->heurekaCategory = $heurekaCategory;
	}

}