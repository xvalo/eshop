<?php

namespace App\Category\Entity;

use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="category_old_url",
 *     indexes={@ORM\Index(name="cat_old_url_idx", columns={"url"})}
 * )
 */
class OldUrl extends BaseEntity
{
    /**
     * @ORM\ManyToOne(targetEntity="\App\Category\Entity\Category", inversedBy="oldUrls")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    public function __construct(Category $category, $url)
    {
        $this->category = $category;
        $this->url = $url;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }


}