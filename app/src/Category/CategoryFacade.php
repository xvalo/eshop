<?php

namespace App\Category;


use App\Category\Entity\Category;
use App\Product\Product;
use Doctrine\ORM\Query\ResultSetMapping;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class CategoryFacade
{
	use SmartObject;

	/** @var EntityManager */
	private $em;

	private $categoryRepository;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$this->categoryRepository = $this->em->getRepository(Category::class);
	}

	/**
	 * @param int $id
	 * @return null|Category
	 */
	public function getCategory(int $id)
	{
		return $this->categoryRepository->find($id);
	}

	/**
	 * @param array $criteria
	 * @return Category[]
	 */
	public function getCategoriesBy(array $criteria): array
	{
		return $this->categoryRepository->findBy($criteria);
	}

	/**
	 * @param string $url
	 * @return mixed
	 * @throws \Doctrine\ORM\NoResultException
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function getByUrl(string $url)
	{
		$qb = $this->em->createQueryBuilder()
			->select('C')
			->from(Category::class, 'C')
			->leftJoin('C.oldUrls', 'U');

		$where = $qb->expr()->andX(
			$qb->expr()->eq('C.deleted', ':deleted'),
			$qb->expr()->eq('C.active', ':active'),
			$qb->expr()->orX(
				$qb->expr()->like('C.url', ':url'),
				$qb->expr()->like('U.url', ':url')
			)
		);

		$qb->where($where);

		$qb->setParameters([
			'url' => $url,
			'deleted' => false,
			'active' => true
		]);

		return $qb->getQuery()->getSingleResult();
	}

	/**
	 * @param Category $category
	 * @throws \Exception
	 */
	public function saveCategory(Category $category)
	{
		if (!$category->getId()) {
			$this->em->persist($category);
		}

		$this->em->flush();
	}

	/**
	 * @param bool $onlyActive
	 * @return mixed
	 */
	public function getCategoriesList(bool $onlyActive = true)
	{
		$qb = $this->categoryRepository->createQueryBuilder();

		$qb->select('C.id, C.name, C.url, IDENTITY(C.parent) AS parentId')
			->from(Category::class, 'C');

		$qb->where($qb->expr()->eq('C.deleted', ':deleted'))
			->addOrderBy($qb->expr()->asc('C.parent'))
			->addOrderBy($qb->expr()->asc('C.levelOrder'));

		$parameters = [
			'deleted' => false
		];

		if ($onlyActive) {
			$qb->andWhere($qb->expr()->eq('C.active', ':active'));
			$parameters['active'] = true;
		}

		$qb->setParameters($parameters);

		return $qb->getQuery()->execute();
	}

	public function getMenuItemsList()
	{
		$qb = $this->em->createQueryBuilder()
			->select(['c.id AS id', 'c.name AS name', 'c.url AS url', 'IDENTITY(c.parent) AS parent'])
			->from(Category::class, 'c');

		$where = $qb->expr()->andX(
			$qb->expr()->eq('c.active', 1),
			$qb->expr()->eq('c.deleted', 0)
		);


		$qb->where($where)
			->orderBy('c.parent', 'ASC')
			->addOrderBy('c.levelOrder', 'ASC');

		return $qb->getQuery()->execute();
	}

	public function getCategoryParentsIds(Category $category)
	{
		$rsm = new ResultSetMapping();
		$rsm->addEntityResult(Category::class, 'C2');
		$rsm->addFieldResult('C2', 'id', 'id');

		$query = $this->em->createNativeQuery(
		'SELECT C2.id
				FROM (
					SELECT @r AS _id, (SELECT @r := parent_id FROM category WHERE id = _id) AS parent_id
					FROM (SELECT @r := ?) vars, category m WHERE @r <> 0
				) C1
				JOIN category C2 ON C1.parent_id = C2.id',
			$rsm
		);
		$query->setParameter(1, $category->getId());

		$queryResult = $query->getArrayResult();
		$result = [];

		foreach ($queryResult as $row) {
			$result[] = $row['id'];
		}

		return $result;

	}

	public function getCategoryTreeChildrenIds(Category $category)
	{
		/*
		$rsm = new ResultSetMapping();
		$rsm->addScalarResult('result', 'result');


		$query = $this->em->createNativeQuery(
			'SELECT GROUP_CONCAT(lv SEPARATOR \',\') AS result FROM (
					SELECT @pv := (SELECT GROUP_CONCAT(id SEPARATOR \',\') FROM category WHERE parent_id IN (@pv)) AS lv FROM category
					JOIN
					(SELECT @pv := ?)tmp
					WHERE parent_id IN (@pv)) a',
			$rsm
		);


		$query = $this->em->createNativeQuery(
			'SELECT id FROM (
					SELECT * FROM category ORDER BY parent_id, id) category_sorted, 
						(SELECT @pv := ?) initialisation WHERE FIND_IN_SET(parent_id, @pv) AND LENGTH(@pv := CONCAT(@pv, \',\', id))
			',
			$rsm
		);
		$query->setParameter(1, $category->getId());
*/
		$result = $this->em->getConnection()->executeQuery('SELECT id FROM (
					SELECT * FROM category ORDER BY parent_id, id) category_sorted, 
						(SELECT @pv := '.$category->getId().') initialisation WHERE FIND_IN_SET(parent_id, @pv) AND LENGTH(@pv := CONCAT(@pv, \',\', id))
			')->fetchAll();

		return array_column($result, 'id');
	}

	/**
	 * @param Category|null $parent
	 * @return int
	 * @throws \Doctrine\ORM\NoResultException
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function getNewCategoryPosition(Category $parent = null): int
	{
		$qb = $this->em->createQueryBuilder()
				->select('MAX(C.levelOrder)')
				->from(Category::class, 'C');

		if ($parent instanceof Category) {
			$where = $qb->expr()->eq('C.parent', ':parent');
			$qb->setParameter('parent', $parent);
		} else {
			$where = $qb->expr()->isNull('C.parent');
		}

		$qb->where($where);

		$position = $qb->getQuery()->getSingleScalarResult();
		return $position ? (int)$position + 1 : 1;

	}

	public function search($q)
	{
		$qb = $this->em->createQueryBuilder();

		$qb->select(['c.id AS id', 'c.name AS name'])
			->from(Category::class, 'c');

		$where = $qb->expr()->orX(
			$qb->expr()->like('c.name', ':phrase'),
			$qb->expr()->like('c.seoTitle', ':phrase'),
			$qb->expr()->like('c.seoDescription', ':phrase'),
			$qb->expr()->like('c.postscript', ':phrase')
		);

		$qb->where($where);

		$qb->setParameter('phrase', '%'.$q.'%');

		return $qb->getQuery()->execute();
	}

	public function getCategoriesCount()
	{
		return $this->categoryRepository->countBy(['deleted' => 0]);
	}

	public function getActiveChildren(Category $category)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('partial C.{id,name,url}')
			->from(Category::class, 'C')
			->where(
				$qb->expr()->andX(
					$qb->expr()->eq('C.parent', ':parent'),
					$qb->expr()->eq('C.active', ':active'),
					$qb->expr()->eq('C.deleted', ':deleted')
				)
			);

		$qb->orderBy('C.levelOrder', 'ASC');

		$qb->setParameters([
			'parent' => $category,
			'active' => true,
			'deleted' => false
		]);

		return $qb->getQuery()->getResult();
	}

	public function getCategoryMinMaxPrice(Category $category)
	{
		$categoriesIds = $this->getCategoryTreeChildrenIds($category);
		$categoriesIds[] = $category->getId();

		$qb = $this->em->createQueryBuilder();
		$qb->select('MIN(P.minPrice) AS minPrice, MAX(P.minPrice) AS maxPrice')
			->from(Product::class, 'P')
			->where(
				$qb->expr()->in('P.category', ':categories')
			);

		$qb->setParameters([
			'categories' => $categoriesIds
		]);

		return $qb->getQuery()->getSingleResult();
	}
}