<?php

namespace App\Category;

use App\Category\Entity\Category;
use App\Model\BaseRepository;

class CategoryRepository extends BaseRepository
{
	public function search($q)
	{
		$qb = $this->em->createQueryBuilder();

		$qb->select(['c.id AS id', 'c.name AS name'])
				->from(Category::class, 'c');

		$where = $qb->expr()->orX(
				$qb->expr()->like('c.name', ':phrase'),
				$qb->expr()->like('c.seoTitle', ':phrase'),
				$qb->expr()->like('c.seoDescription', ':phrase'),
				$qb->expr()->like('c.postscript', ':phrase')
		);

		$qb->where($where);

		$qb->setParameter('phrase', '%'.$q.'%');

		return $qb->getQuery()->execute();
	}

	public function getByUrl($url)
	{
		$qb = $this->em->createQueryBuilder()
				->select('c')
				->from(Category::class, 'c')
					->leftJoin('c.oldUrls', 'u');

		$where = $qb->expr()->andX(
				$qb->expr()->eq('c.deleted', 0),
				$qb->expr()->eq('c.active', 1),
				$qb->expr()->orX(
						$qb->expr()->like('c.url', ':url'),
						$qb->expr()->like('u.url', ':url')
				)
		);

		$qb->where($where);

		$qb->setParameter('url', $url);

		return $qb->getQuery()->getSingleResult();
	}

	public function getNewCategoryPosition(Category $parent = null)
	{
		$qb = $this->em->createQueryBuilder()
				->select('MAX(c.levelOrder)')
				->from(Category::class, 'c');

		if ($parent instanceof Category) {
			$where = $qb->expr()->eq('c.parent', ':parent');
			$qb->setParameter('parent', $parent);
		} else {
			$where = $qb->expr()->isNull('c.parent');
		}

		$qb->where($where);

		$position = $qb->getQuery()->getSingleScalarResult();
		return $position ? (int)$position + 1 : 1;

	}

	public function getCategoriesCount()
	{
		return $this->repository->countBy(['deleted' => 0]);
	}

}