<?php


namespace App\Category\Heureka;


use App\Category\Entity\HeurekaCategory;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class CategoryFacade
{
	use SmartObject;

	/** @var EntityManager */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	/**
	 * @param int $id
	 * @return null|HeurekaCategory
	 */
	public function getCategory(int $id)
	{
		return $this->em->getRepository(HeurekaCategory::class)->find($id);
	}

	/**
	 * @param array|HeurekaCategory[] $categories
	 */
	public function bulkInsert(array $categories)
	{
		$batchLimit = 250;
		$counter = 0;

		foreach ($categories as $category) {
			$this->em->persist($category);
			$counter++;

			if ($counter === $batchLimit) {
				$this->em->flush();
				$this->em->clear();
			}
		}

		$this->em->flush();
		$this->em->clear();
	}

	public function bulkUpdate()
	{
		$this->em->flush();
		$this->em->clear();
	}

	public function saveCategory(HeurekaCategory $category, bool $persist = false)
	{
		if ($persist) {
			$this->em->persist($category);
		}

		$this->em->flush();
	}

	public function getCategories()
	{
		$qb = $this->em->createQueryBuilder();

		$qb->select('C')
			->from(HeurekaCategory::class, 'C')
			->where(
				$qb->expr()->eq('C.deleted', ':deleted')
			)
			->setParameters([
				'deleted' => false
			]);

		return $qb->getQuery()
			->setHint(\Doctrine\ORM\Query::HINT_INCLUDE_META_COLUMNS, true)
			->getArrayResult();
	}
}