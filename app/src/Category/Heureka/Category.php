<?php

namespace App\Category\Heureka;

use Nette\SmartObject;

class Category
{
	use SmartObject;

	/** @var int */
	private $id;

	/** @var string */
	private $name;

	/** @var string|null */
	private $fullName;

	/** @var Category|null */
	private $parent;

	public function __construct(int $id, string $name, string $fullName = null, Category $parent = null)
	{
		$this->id = $id;
		$this->name = $name;
		$this->fullName = $fullName;
		$this->parent = $parent;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return null|string
	 */
	public function getFullName()
	{
		return $this->fullName;
	}

	/**
	 * @return Category|null
	 */
	public function getParent()
	{
		return $this->parent;
	}


}