<?php

namespace App\Category\Heureka;

use App\Category\Entity\HeurekaCategory;
use Nette\SmartObject;

class CategoryService
{
	use SmartObject;

	/** @var CategoryFacade */
	private $categoryFacade;

	private $categoriesToInsert = [];
	private $categoriesToUpdate = [];

	public function __construct(
		CategoryFacade $categoryFacade
	){
		$this->categoryFacade = $categoryFacade;
	}

	public function getCategoryList()
	{
		$categories = $this->categoryFacade->getCategories();
		return $this->buildTree($categories);
	}

	/**
	 * @param int $id
	 * @return \App\Category\Entity\HeurekaCategory|null
	 */
	public function getCategory(int $id)
	{
		return $this->categoryFacade->getCategory($id);
	}

	public function getCategories()
	{
		return $this->categoryFacade->getCategories();
	}

	/**
	 * @param HeurekaCategory $category
	 * @param Category $categoryDTO
	 */
	public function updateCategory(HeurekaCategory $category, Category $categoryDTO)
	{
		$category->setName($categoryDTO->getName());
		$category->setFullName($categoryDTO->getFullName());

		$categoryParentId = $category->getParent() ? $category->getParent()->getId() : null;
		$categpryDTOParentId = $categoryDTO->getParent() ? $categoryDTO->getParent()->getId() : null;

		if ($categoryParentId !== $categpryDTOParentId) {
			$category->setParent(
				$categpryDTOParentId ? $this->categoryFacade->getCategory($categpryDTOParentId) : null
			);
		}

		$this->categoriesToUpdate[] = $category;
	}

	/**
	 * @param Category $categoryDTO
	 */
	public function createCategory(Category $categoryDTO)
	{
		$category = new HeurekaCategory(
			$categoryDTO->getId(),
			$categoryDTO->getName(),
			$categoryDTO->getFullName(),
			$categoryDTO->getParent() ? $this->categoryFacade->getCategory($categoryDTO->getId()) : null
		);

		$this->categoriesToInsert[] = $category;
	}

	public function commitChanges()
	{
		$this->categoryFacade->bulkUpdate();
		$this->categoryFacade->bulkInsert($this->categoriesToInsert);
	}

	private function buildTree(array $elements, $parentId = null)
	{
		$branch = array();

		foreach ($elements as $element) {
			if ($element['parent_id'] == $parentId) {
				$children = $this->buildTree($elements, $element['id']);
				if ($children) {
					$element['children'] = $children;
				}
				$element['disabled'] = $element['fullName'] === null;
				unset($element['deleted'], $element['parent_id']);
				$branch[$element['id']] = $element;
			}
		}

		return $branch;
	}
}