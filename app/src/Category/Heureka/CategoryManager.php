<?php

namespace App\Category\Heureka;

use Nette\SmartObject;

class CategoryManager
{
	use SmartObject;

	const FEED_URL = 'https://www.heureka.cz/direct/xml-export/shops/heureka-sekce.xml';

	/** @var CategoryService */
	private $categoryService;

	public function __construct(CategoryService $categoryService)
	{
		$this->categoryService = $categoryService;
	}

	public function runUpdate()
	{
		$xmlString = file_get_contents(self::FEED_URL);
		$data = new \SimpleXMLElement($xmlString);
		$categories = [];

		/** @var \SimpleXMLElement $item */
		foreach ($data as $item) {
			$this->processCategoryItem($item, $categories);
		}

		$this->categoryService->commitChanges();
	}

	private function processCategoryItem(\SimpleXMLElement $item, array &$saveTo, Category $parent = null)
	{
		$category = new Category(
			(int) $item->CATEGORY_ID,
			$item->CATEGORY_NAME,
			isset($item->CATEGORY_FULLNAME) ? $item->CATEGORY_FULLNAME : null,
			$parent
		);

		if ($heurekaCategory = $this->categoryService->getCategory($category->getId())) {
			$this->categoryService->updateCategory($heurekaCategory, $category);
		} else {
			$this->categoryService->createCategory($category);
		}

		if (isset($item->CATEGORY)) {
			/** @var \SimpleXMLElement $item */
			foreach ($item->CATEGORY as $item) {
				$this->processCategoryItem($item, $saveTo, $category);
			}
		}
	}
}