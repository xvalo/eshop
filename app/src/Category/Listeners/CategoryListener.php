<?php

namespace App\Category\Listeners;

use App\Category\Entity\Category;
use App\Category\Entity\OldUrl;
use App\Lists\ProducerRepository;
use Contributte\Cache\CacheFactory;
use Doctrine\ORM\Events;
use Nette\Caching\Cache;
use Nette\SmartObject;
use Nette\Utils\Strings;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Kdyby\Events\Subscriber;

class CategoryListener implements Subscriber
{
	use SmartObject;

	/** @var \Nette\Caching\Cache  */
	private $categoryMenuCache;

	/** @var Cache  */
	private $producersFrontListCache;

	public function __construct(CacheFactory $cacheFactory)
	{
		$this->categoryMenuCache = $cacheFactory->create('category');
		$this->producersFrontListCache = $cacheFactory->create(ProducerRepository::FRONT_FILTER_LIST);
	}

    public function getSubscribedEvents()
    {
        return [
            Events::onFlush
        ];
    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

	    foreach ($uow->getScheduledEntityInsertions() as $entity) {
	    	if ($entity instanceof Category) {
			    $this->categoryMenuCache->clean([Cache::TAGS => ['category-menu-items-list']]);
		    }
	    }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof Category) {
                $category = $entity;
                $changes = $uow->getEntityChangeSet($category);

                if (array_key_exists('name', $changes)) {
                    $oldName = $changes['name'][0];

                    $oldUrlObject = new OldUrl($category, Strings::webalize($oldName));
                    $em->persist($oldUrlObject);
                    $uow->computeChangeSet($em->getClassMetadata(get_class($oldUrlObject)), $oldUrlObject);

                    $category->setUrl(Strings::webalize($category->getName()));
                    $category->addOldUrl($oldUrlObject);
                }

                if ($entity->isDeleted()) {
                    foreach($category->getProducts() as $product){
                        $product->remove();
                        $uow->persist($product);
                        $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($product)), $product);
                    }
                    foreach($category->getChildren() as $child){
                        $child->delete();
	                    $child->setUrl($child->getUrl() . '-' . $child->getId() . '-deleted'); // Hack to avoid unique combinations in category
	                    $child->setLevelOrder(0);
                        $uow->persist($child);
                        $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($child)), $child);
                    }
                }

                if (!$entity->isActive()) {
                    /** @var Category $child */
                    foreach($category->getChildren() as $child){
                        $child->deactivate();
                        $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($child)), $child);
                    }
                }

                $this->categoryMenuCache->clean([Cache::TAGS => ['category-menu-items-list']]);
                $this->producersFrontListCache->clean([ Cache::TAGS => ProducerRepository::CACHE_FRONT_LIST_TAG ]);
            }
        }
    }

}