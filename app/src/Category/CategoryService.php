<?php

namespace App\Category;

use App\Category\Entity\Category;
use App\Classes\Category\CategoryMenuStructureBuilder;
use App\Order\Delivery\DeliveryService;
use Contributte\Cache\CacheFactory;
use Nette\Caching\Cache;
use Nette\Utils\ArrayHash;
use App\Category\Heureka\CategoryService as HeurekaCategoryService;
use App\Category\Zbozi\CategoryService as ZboziCategoryService;

class CategoryService
{
	/** @var CategoryFacade */
	private $categoryFacade;

	/** @var \Nette\Caching\Cache  */
	private $categoryMenuCache;

	/** @var DeliveryService */
	private $deliveryService;

	/** @var HeurekaCategoryService */
	private $heurekaCategoryService;

	/** @var ZboziCategoryService */
	private $zboziCategoryService;

	public function __construct(
		CategoryFacade $categoryFacade,
		CacheFactory $cacheFactory,
		DeliveryService $deliveryService,
		HeurekaCategoryService $heurekaCategoryService,
		ZboziCategoryService $zboziCategoryService
	){
		$this->categoryFacade = $categoryFacade;
		$this->categoryMenuCache = $cacheFactory->create('category');
		$this->deliveryService = $deliveryService;
		$this->heurekaCategoryService = $heurekaCategoryService;
		$this->zboziCategoryService = $zboziCategoryService;
	}

	/**
	 * @param $id
	 * @return null|Category
	 */
	public function getCategory($id)
	{
		return $this->categoryFacade->getCategory(intval($id));
	}

	/**
	 * @param string $url
	 * @return Category|null
	 */
	public function getCategoryByUrl(string $url)
	{
		return $this->categoryFacade->getByUrl($url);
	}

	public function deleteCategory($id)
	{
		/** @var Category $category */
		$category = $this->categoryFacade->getCategory(intval($id));
		$category->setUrl($category->getUrl() . '-' . $category->getId() . '-deleted'); // Hack to avoid unique combinations in category
		$category->setLevelOrder(0);
		$category->delete();
		$this->categoryFacade->saveCategory($category);
	}

	public function createCategory(ArrayHash $data)
	{
		$parent = $data->parent ? $this->getCategory($data->parent) : null;
		$heurekaCategory = $data->heurekaCategory !== null ? $this->heurekaCategoryService->getCategory($data->heurekaCategory) : null;
		$zboziCategory = $data->zboziCategory !== null ? $this->zboziCategoryService->getCategory($data->zboziCategory) : null;

		$category = new Category(
			$data->name,
			$data->postscript,
			$data->description,
			$parent,
			$data->seoTitle,
			$data->seoDescription,
			$zboziCategory,
			$heurekaCategory
		);

		if($data->active)
		{
			$category->activate();
		}
		else
		{
			$category->deactivate();
		}

		$position = $this->categoryFacade->getNewCategoryPosition($parent);
		$category->setLevelOrder($position);

		$this->categoryFacade->saveCategory($category);

		return $category;
	}

	public function updateCategory(Category $category, ArrayHash $data)
	{
		$parent = $data->parent ? $this->getCategory($data->parent) : null;

		$category->setName($data->name);
		$category->setPostscript($data->postscript);
		$category->setDescription($data->description);
		$category->setParent($parent);
		$category->setSeoTitle($data->seoTitle);
		$category->setSeoDescription($data->seoDescription);
		$category->setZboziCategory($data->zboziCategory !== null ? $this->zboziCategoryService->getCategory($data->zboziCategory) : null);
		$category->setHeurekaCategory($data->heurekaCategory !== null ? $this->heurekaCategoryService->getCategory($data->heurekaCategory) : null);

		if($data->active)
		{
			$category->activate();
		}
		else
		{
			$category->deactivate();
		}

		$this->categoryFacade->saveCategory($category);

		return $category;
	}

	/**
	 * @param Category $category
	 * @return array
	 */
	public function getFormDefaults(Category $category)
	{
		return [
			'name' => $category->getName(),
			'postscript' => $category->getPostscript(),
			'description' => $category->getDescription(),
			'parent' => $category->getParent() ? $category->getParent()->getId() : null,
			'seoTitle' => $category->getSeoTitle(),
			'seoDescription' => $category->getSeoDescription(),
			'active' => $category->isActive(),
			'zboziCategory' => $category->getZboziCategory() ? $category->getZboziCategory()->getId() : null,
			'heurekaCategory' => $category->getHeurekaCategory() ? $category->getHeurekaCategory()->getId() : null
		];
	}

	/**
	 * @param bool $onlyActive
	 * @return array
	 */
	public function getCategoriesMenuList(bool $onlyActive = true)
	{
		$menuCategoryStructureBuilder = new CategoryMenuStructureBuilder($this->categoryFacade->getCategoriesList($onlyActive));
		$menuCategoryStructureBuilder->buildTree();
		return $menuCategoryStructureBuilder->getMenuList();
	}

	public function getCategoryTreeList()
	{
		return $this->buildTree($this->categoryFacade->getCategoriesList());
	}

	private function buildTree(array $elements, int $parentId = null)
	{
		$branch = array();

		foreach ($elements as $element) {
			if ((int)$element['parentId'] == $parentId) {
				$children = $this->buildTree($elements, $element['id']);
				if ($children) {
					$element['children'] = $children;
				}
				$element['disabled'] = isset($element['children']);
				unset($element['url'], $element['parentId']);
				$branch[$element['id']] = $element;
			}
		}

		return $branch;
	}

	public function getCategoryMenuList()
	{
		if (!$list = $this->categoryMenuCache->load('menu-list', false)) {
			$categories = $this->categoryFacade->getCategoriesList();

			$list = [];

			foreach ($categories as $category) {
				$item = [
					'name' => $category['id'],
					'target' => ':Front:Category:default',
					'title' => $category['name'],
					'parameters' => ['category' => $category['url']],
					'items' => []
				];

				$list = $this->addItemIntoCategoriesList($list, $item, $category['id'], $category['parentId']);
			}

			$this->categoryMenuCache->save('menu-list', $list, [Cache::TAGS => ['category-menu-items-list']]);
		}

		return $list;
	}

	private function addItemIntoCategoriesList(array $list, $item, $itemId, $parent)
	{
		if (is_null($parent)) {
			$list[$itemId] = $item;
		} elseif (array_key_exists($parent, $list)) {
			$list[$parent]['items'][$itemId] = $item;
		} else {
			foreach ($list as $id => $category) {
				$category['items'] = $this->addItemIntoCategoriesList($category['items'], $item, $itemId, $parent);
				$list[$id] = $category;
			}
		}

		return $list;
	}

	public function changeCategoriesOrder($item_id, $prev_id, $next_id, $parent_id)
	{
		/** @var Category $item */
		$item = $this->getCategory($item_id);

		/**
		 * 1, Find out order of item BEFORE current item
		 */
		/** @var Category $previousItem */
		$previousItem = (!$prev_id) ? null : $this->getCategory($prev_id);

		/**
		 * 2, Find out order of item AFTER current item
		 */
		/** @var Category $nextItem */
		$nextItem = (!$next_id) ? null : $this->getCategory($next_id);

		/**
		 * 3, Find all items that have to be moved one position up
		 */
		$itemsToMoveUp = $this->categoryFacade->getCategoriesBy([
				'levelOrder <=' => ($previousItem ? $previousItem->getLevelOrder() : 0),
				'levelOrder >' => $item->getLevelOrder(),
				'parent' => $item->getParent()
		]);

		/** @var Category $category */
		foreach ($itemsToMoveUp as $category) {
			$category->setLevelOrder($category->getLevelOrder() - 1);
		}

		/**
		 * 3, Find all items that have to be moved one position down
		 */

		$itemsToMoveDown = $this->categoryFacade->getCategoriesBy([
				'levelOrder >=' => ($nextItem ? $nextItem->getLevelOrder() : 0),
				'levelOrder <' => $item->getLevelOrder(),
				'parent' => $item->getParent()
		]);

		/** @var Category $category */
		foreach ($itemsToMoveDown as $category) {
			$category->setLevelOrder($category->getLevelOrder() + 1);
		}

		/**
		 * Update current item order
		 */
		if ($previousItem) {
			$item->setLevelOrder($previousItem->getLevelOrder() + 1);
		} else if ($nextItem) {
			$item->setLevelOrder($nextItem->getLevelOrder() - 1);
		} else {
			$item->setLevelOrder(1);
		}

		$itemParentId = ($item->getParent()) ? $item->getParent()->getId() : null;
		if ($parent_id != $itemParentId) {
			$parent = ($parent_id) ? $this->getCategory($parent_id) : null;
			$item->setParent($parent);
		}

		$this->categoryFacade->saveCategory($item);
	}

	public function searchCategories($q)
	{
		return $this->categoryFacade->search($q);
	}

	public function getChildrenCategories(Category $category)
	{
		return $this->categoryFacade->getActiveChildren($category);
	}

	/**
	 * @param Category|int $category
	 * @return array
	 */
	public function getCategoryTreeIds($category): array
	{
		$category = $category instanceof Category ? $category : $this->getCategory((int) $category);

		$ids = $this->categoryFacade->getCategoryTreeChildrenIds($category);
		$ids[] = $category->getId();
		return $ids;
	}

	public function setDeliveriesAvailableInCategory(array $deliveries, Category $category)
	{
		$categories = [];

		foreach ($this->getCategoryTreeIds($category) as $id) {
			$categories[] = $this->getCategory((int) $id);
		}

		/** @var Category $category */
		foreach ($categories as $category) {
			if ($category->isDeleted()) {
				continue;
			}
			$this->deliveryService->removeDeliveriesFromCategory($category);
			foreach ($deliveries as $id) {
				$delivery = $this->deliveryService->getDelivery((int)$id);
				$this->deliveryService->allowDeliveryToCategory($delivery, $category);
			}
		}
	}
}