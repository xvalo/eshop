<?php

namespace App\Category\Zbozi;

use Nette\SmartObject;

class CategoryManager
{
	use SmartObject;

	const FEED_URL = 'https://www.zbozi.cz/static/categories.json';

	/** @var CategoryService */
	private $categoryService;

	public function __construct(CategoryService $categoryService)
	{
		$this->categoryService = $categoryService;
	}

	public function runUpdate()
	{
		$jsonString = file_get_contents(self::FEED_URL);
		$data = json_decode($jsonString, true);

		/** @var array $item */
		foreach ($data as $item) {
			$this->processCategoryItem($item);
		}

		$this->categoryService->commitChanges();
	}

	private function processCategoryItem(array $item)
	{
		if (isset($item['children'])) {
			/** @var array $item */
			foreach ($item['children'] as $item) {
				$this->processCategoryItem($item);
			}
		} else {

			$category = new Category(
				$item['id'],
				$item['name'],
				$item['categoryText']
			);

			if ($zboziCategory = $this->categoryService->getCategory($category->getId())) {
				$this->categoryService->updateCategory($zboziCategory, $category);
			} else {
				$this->categoryService->createCategory($category);
			}

		}
	}
}