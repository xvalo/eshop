<?php

namespace App\Category\Zbozi;

use App\Category\Entity\ZboziCategory;
use Nette\SmartObject;

class CategoryService
{
	use SmartObject;

	/** @var CategoryFacade */
	private $categoryFacade;

	private $categoriesToInsert = [];
	private $categoriesToUpdate = [];

	public function __construct(
		CategoryFacade $categoryFacade
	){
		$this->categoryFacade = $categoryFacade;
	}

	public function getCategoryList()
	{
		$categories = $this->categoryFacade->getCategories();
		$list = [];

		foreach ($categories as $category) {
			$list[$category['id']] = $category['categoryText'];
		}
		return $list;
	}

	/**
	 * @param int $id
	 * @return \App\Category\Entity\ZboziCategory|null
	 */
	public function getCategory(int $id)
	{
		return $this->categoryFacade->getCategory($id);
	}

	public function getCategories()
	{
		return $this->categoryFacade->getCategories();
	}

	/**
	 * @param ZboziCategory $category
	 * @param Category $categoryDTO
	 */
	public function updateCategory(ZboziCategory $category, Category $categoryDTO)
	{
		$category->setName($categoryDTO->getName());
		$category->setCategoryText($categoryDTO->getCategoryText());

		$this->categoriesToUpdate[] = $category;
	}

	/**
	 * @param Category $categoryDTO
	 */
	public function createCategory(Category $categoryDTO)
	{
		$category = new ZboziCategory(
			$categoryDTO->getId(),
			$categoryDTO->getName(),
			$categoryDTO->getCategoryText()
		);

		$this->categoriesToInsert[] = $category;
	}

	public function commitChanges()
	{
		$this->categoryFacade->bulkUpdate();
		$this->categoryFacade->bulkInsert($this->categoriesToInsert);
	}
}