<?php

namespace App\Category\Zbozi;

use Nette\SmartObject;

class Category
{
	use SmartObject;

	/** @var int */
	private $id;

	/** @var string */
	private $name;

	/** @var string */
	private $categoryText;

	public function __construct(int $id, string $name, string $categoryText)
	{
		$this->id = $id;
		$this->name = $name;
		$this->categoryText = $categoryText;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getCategoryText(): string
	{
		return $this->categoryText;
	}
}