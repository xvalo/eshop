<?php

namespace App\News\Entity;

use App\Admin\Admin;
use App\File\Image;
use App\Model\BaseEntity;
use App\Traits\ActivityTrait;
use App\Traits\IRemovable;
use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;
use Nette\Utils\Strings;

/**
 * @ORM\Entity
 */
class News extends BaseEntity implements IRemovable
{
	use ActivityTrait;
	use RemovableTrait;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $title;

	/**
	 * @ORM\Column(type="text")
	 */
	private $perex;

	/**
	 * @ORM\Column(type="text")
	 */
	private $text;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $created;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $displayedFrom;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $displayedTo;

	/**
	 * @ORM\OneToOne(targetEntity="App\File\Image", cascade={"persist"})
	 * @ORM\JoinColumn(name="image_id", referencedColumnName="id", nullable=true)
	 */
	private $mainImage;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Admin\Admin")
	 * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
	 */
	private $author;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $url;

	/**
	 * News constructor.
	 * @param $title
	 * @param $perex
	 * @param $text
	 * @param $active
	 * @param Admin $author
	 * @param $displayedFrom
	 * @param $displayedTo
	 * @param Image $mainImage
	 */
	public function __construct($title, $perex, $text, $active, Admin $author, $displayedFrom = null, $displayedTo = null, Image $mainImage = null)
	{
		$this->title = $title;
		$this->perex = $perex;
		$this->text = $text;
		$this->displayedFrom = $displayedFrom;
		$this->displayedTo = $displayedTo;
		$this->author = $author;
		$this->mainImage = $mainImage;
		$this->created = new DateTime();
		$this->url = Strings::webalize($title);

		if ($active) {
			$this->activate();
		} else {
			$this->deactivate();
		}
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getPerex()
	{
		return $this->perex;
	}

	/**
	 * @param string $perex
	 */
	public function setPerex($perex)
	{
		$this->perex = $perex;
	}

	/**
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}

	/**
	 * @param string $text
	 */
	public function setText($text)
	{
		$this->text = $text;
	}

	/**
	 * @return DateTime
	 */
	public function getCreated()
	{
		return $this->created;
	}

	/**
	 * @return DateTime
	 */
	public function getDisplayedFrom()
	{
		return $this->displayedFrom;
	}

	/**
	 * @param DateTime $displayedFrom
	 */
	public function setDisplayedFrom(DateTime $displayedFrom = null)
	{
		$this->displayedFrom = $displayedFrom;
	}

	/**
	 * @return DateTime
	 */
	public function getDisplayedTo()
	{
		return $this->displayedTo;
	}

	/**
	 * @param DateTime $displayedTo
	 */
	public function setDisplayedTo(DateTime $displayedTo = null)
	{
		$this->displayedTo = $displayedTo;
	}

	/**
	 * @return Image
	 */
	public function getMainImage()
	{
		return $this->mainImage;
	}

	/**
	 * @param Image $mainImage
	 */
	public function setMainImage(Image $mainImage)
	{
		$this->mainImage = $mainImage;
	}

	/**
	 * @return Admin
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}

}