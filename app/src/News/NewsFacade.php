<?php

namespace App\News;

use App\News\Entity\News;
use Kdyby\Doctrine\EntityManager;

class NewsFacade
{
	/** @var EntityManager */
	private $em;

	/** @var \Kdyby\Doctrine\EntityRepository  */
	private $newsRepository;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$this->newsRepository = $this->em->getRepository(News::class);
	}

	/**
	 * @param int $id
	 * @return null|News
	 */
	public function getNewsById(int $id)
	{
		return $this->newsRepository->find($id);
	}

	/**
	 * @param News $news
	 * @throws \Exception
	 */
	public function saveNews(News $news)
	{
		if (!$news->getId()) {
			$this->em->persist($news);
		}

		$this->em->flush();
	}

	public function getList()
	{
		$qb = $this->em->createQueryBuilder()
				->select('N')
				->from(News::class, 'N');

		$where = $qb->expr()->andX(
			$qb->expr()->eq('N.active', ':active'),
			$qb->expr()->eq('N.deleted', ':deleted'),
			$qb->expr()->orX(
				$qb->expr()->andX(
					$qb->expr()->isNull('N.displayedFrom'),
					$qb->expr()->isNull('N.displayedTo')
				),
				$qb->expr()->between('NOW()', 'N.displayedFrom', 'N.displayedTo')
			)
		);

		$qb->where($where);
		$qb->orderBy('N.created', 'DESC');

		$qb->setParameters([
			'active' => true,
			'deleted' => false
		]);

		return $qb->getQuery()->execute();
	}

	public function getNewsByUrl($url)
	{
		$qb = $this->em->createQueryBuilder()
			->select('N')
			->from(News::class, 'N');

		$where = $qb->expr()->andX(
			$qb->expr()->like('N.url', ':url'),
			$qb->expr()->eq('N.active', ':active'),
			$qb->expr()->eq('N.deleted', ':deleted')
		);

		$qb->where($where);

		$qb->setParameters([
			'active' => true,
			'deleted' => false,
			'url' => $url
		]);

		return $qb->getQuery()->getSingleResult();
	}

	public function getGridSource()
	{
		return $this->newsRepository->createQueryBuilder('N');
	}
}