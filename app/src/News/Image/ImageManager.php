<?php

namespace App\News\Image;

use App\File\Image;
use App\File\ImageRepository;
use App\File\ImageService;
use App\Model\Services\ApplicationConfigurationService;
use App\News\Entity\News;
use App\Product\Product;
use Nette\Http\FileUpload;
use Nette\SmartObject;

class ImageManager
{
	use SmartObject;

	/** @var string */
	private $storageName;

	/** @var ImageService */
	private $imageService;

	/** @var ImageRepository */
	private $imageRepository;

	public function __construct(
		ImageService $imageService,
		ImageRepository $imageRepository,
		ApplicationConfigurationService $applicationConfigurationService
	){
		$this->imageService = $imageService;
		$this->imageRepository = $imageRepository;
		$this->storageName = $applicationConfigurationService->getNewsImagesStorage();
	}

	/**
	 * @param FileUpload $data
	 * @return Image
	 * @throws \Nette\Utils\UnknownImageFileException
	 */
	public function createImage(FileUpload $data)
	{
		return $this->imageService->createImage($data, $this->storageName, false);
	}

	/**
	 * @param News $news
	 * @param $imageId
	 * @throws \Exception
	 */
	public function removeImage(News $news, $imageId)
	{

	}
}