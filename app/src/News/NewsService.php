<?php

namespace App\News;

use App\Admin\Admin;
use App\Admin\AdminRepository;
use App\File\Image;
use App\News\Entity\News;
use App\News\Image\ImageManager;
use Carrooi\Security\User\User;
use Doctrine\ORM\NoResultException;
use Nette\SmartObject;
use Nette\Utils\DateTime;

/**
 * Class NewsService
 * @package App\News
 * @property NewsFacade $repository
 */
class NewsService
{
	use SmartObject;

	/** @var AdminRepository */
	private $adminRepository;

	/** @var User */
	private $loggedUser;

	/** @var ImageManager */
	private $imageManager;

	/** @var NewsFacade */
	private $newsFacade;


	public function __construct(
		NewsFacade $newsFacade,
		AdminRepository $adminRepository,
		ImageManager $imageManager,
		User $loggedUser
	){
		$this->adminRepository = $adminRepository;
		$this->loggedUser = $loggedUser;
		$this->imageManager = $imageManager;
		$this->newsFacade = $newsFacade;
	}



	public function create(array $data)
	{
		/** @var Admin $admin */
		$admin = $this->adminRepository->find($this->loggedUser->getId());

		list( $from, $to ) = $this->getFromToDates( $data['displayedFromTo'] );

		/** @var Image|null $image */
		$image = $data['mainImage']->isOk()
					? $this->imageManager->createImage($data['mainImage'])
					: null;

		$news = new News(
				$data['title'],
				$data['perex'],
				$data['text'],
				$data['active'],
				$admin,
				$from,
				$to,
				$image
		);

		$this->newsFacade->saveNews($news);
	}

	/**
	 * @param News $news
	 * @param array $data
	 */
	public function update($news, array $data)
	{

		list( $from, $to ) = $this->getFromToDates( $data['displayedFromTo'] );

		$news->setTitle($data['title']);
		$news->setPerex($data['perex']);
		$news->setText($data['text']);
		$news->setDisplayedFrom($from);
		$news->setDisplayedTo($to);

		if ($data['active']) {
			$news->activate();
		} else {
			$news->deactivate();
		}
		
		if ($data['mainImage']->isOk()) {
			$image = $this->imageManager->createImage($data['mainImage']);
			$news->setMainImage($image);
		}

		$this->newsFacade->saveNews($news);
	}

	public function getFormDefaults(News $news)
	{
		$displayedFromTo = ($news->getDisplayedFrom() && $news->getDisplayedTo())
							? $news->getDisplayedFrom()->format('d.m.Y') . ' - ' . $news->getDisplayedTo()->format('d.m.Y')
							: null;
		return [
			'title' => $news->getTitle(),
			'perex' => $news->getPerex(),
			'text' => $news->getText(),
			'displayedFromTo' => $displayedFromTo,
			'active' => $news->isActive()
		];
	}

	public function getNewsList()
	{
		return $this->newsFacade->getList();
	}

	/**
	 * @param int $id
	 * @return News|bool|null
	 */
	public function getNewsById(int $id)
	{
		try {
			return $this->newsFacade->getNewsById($id);
		} catch (NoResultException $e) {
			return false;
		}
	}

	public function getNewsByUrl($url)
	{
		try {
			return $this->newsFacade->getNewsByUrl($url);
		} catch (NoResultException $e) {
			return false;
		}
	}

	private function getFromToDates($dateRange)
	{
		$from = $to = null;

		if ($dateRange && strlen($dateRange) > 0) {
			$dates = explode(' - ', $dateRange);
			$from = new DateTime($dates[0]);
			$from->setTime(0, 0, 0);
			$to = new DateTime($dates[1]);
			$to->setTime(0, 0, 0);
		}

		return [ $from, $to ];
	}
}