<?php

namespace App\Mailing;

use App\Classes\Order\OrderDeliverability;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\Customer\Customer;
use App\Lists\Delivery;
use App\Mailing\AuthorizationMail;
use App\Mailing\CustomerGeneralMail;
use App\Mailing\OrderCanceledMail;
use App\Mailing\OrderConfirmationMail;
use App\Mailing\OrderNotPaidMail;
use App\Mailing\OrderRejectedMail;
use App\Mailing\OrderShippedMail;
use App\Mailing\OrderToBePickedUpMail;
use App\Mailing\ProductDemandMail;
use App\Mailing\RestorePasswordMail;
use App\Mailing\WatchDogMail;
use App\Model\Services\ApplicationConfigurationService;
use App\Order\Order;
use App\Product\Product;
use App\Product\Variant;
use Nette\SmartObject;
use Ublaboo\Mailing\MailFactory;

class EmailManager
{
    use SmartObject;

    /** @var MailFactory */
    private $factory;

    /** @var ApplicationConfigurationService */
    private $applicationConfigurationService;

    /** @var ApplicationSettingsManager */
	private $applicationSettingsManager;

	/** @var array  */
	private $baseParams;

	public function __construct(
	    MailFactory $factory,
	    ApplicationConfigurationService $applicationConfigurationService,
		ApplicationSettingsManager $applicationSettingsManager
    ){
        $this->factory = $factory;
        $this->applicationConfigurationService = $applicationConfigurationService;
		$this->applicationSettingsManager = $applicationSettingsManager;

		$this->baseParams = [
			'applicationSettings' => $applicationSettingsManager,
			'from' => $this->applicationSettingsManager->getWebsiteTitle() . ' <' . $this->applicationSettingsManager->getMainEmail() . '>'
		];
	}

    public function sendAuthorizationEmail(Customer $customer)
    {
        $params = [
            'to' => $customer->getEmail(),
            'subject' => 'Potvrzení registrace - ' . $this->applicationSettingsManager->getWebsiteTitle(),
            'hash' => $customer->getAuthorizationHash()
        ];

        $mail = $this->factory->createByType(AuthorizationMail::class, $params + $this->baseParams);
        $mail->send();
    }

    public function sendRestorePasswordEmail(Customer $customer, $password)
    {
        $params = [
            'to' => $customer->getEmail(),
            'subject' => 'Obnovení hesla - ' . $this->applicationSettingsManager->getWebsiteTitle(),
            'password' => $password
        ];

        $mail = $this->factory->createByType(RestorePasswordMail::class, $params + $this->baseParams);
        $mail->send();
    }

    public function sendOrderConfirmationEmail(Order $order)
    {
        $slovakDelivery = in_array($order->getDelivery()->getId(), $this->applicationConfigurationService->getSlovakDeliveries());

        $params = [
            'to' => $order->getContactInformation()->getEmail(),
	        'bcc' => $this->applicationSettingsManager->getOrderConfirmationCopyEmail(),
            'subject' => 'Potvrzení objednávky č. '.$order->getOrderNo().' - ' . $this->applicationSettingsManager->getWebsiteTitle(),
            'order' => $order,
            'slovakDelivery' => $slovakDelivery,
            'transferPayment' => $order->getPaymentType()->getId() == OrderDeliverability::TRANSFER_PAYMENT,
	        'czechPostBoxBinding' => $this->applicationConfigurationService->getCzechPostBoxDeliveryBinding(),
	        'czechPostOfficeBinding' => $this->applicationConfigurationService->getCzechPostOfficeDeliveryBinding()
        ];

        $mail = $this->factory->createByType(OrderConfirmationMail::class, $params + $this->baseParams);
        $mail->send();
    }

    public function sendOrderShippedEmail(Order $order)
    {
    	$delivery = $order->getDelivery();
    	$url = null;

    	if ($delivery && $delivery->getTrackUrl() && strlen($delivery->getTrackUrl())) {
			$url = str_replace(Delivery::TRACK_CODE_SUBSTRING, $order->getPackageNo(), $delivery->getTrackUrl());
	    }


        $params = [
            'to' => $order->getContactInformation()->getEmail(),
            'subject' => 'Objednávka odeslána - ' . $this->applicationSettingsManager->getWebsiteTitle(),
            'order' => $order,
	        'url' => $url
        ];

        $mail = $this->factory->createByType(OrderShippedMail::class, $params + $this->baseParams);
        $mail->send();
    }

    public function sendOrderToBePickedUpEmail(Order $order)
    {
        $params = [
            'to' => $order->getContactInformation()->getEmail(),
            'subject' => 'Objednávka k vyzvednutí - ' . $this->applicationSettingsManager->getWebsiteTitle(),
            'order' => $order
        ];

        $mail = $this->factory->createByType(OrderToBePickedUpMail::class, $params + $this->baseParams);
        $mail->send();
    }

    public function sendCustomerGeneralEmail(Order $order, $messageBody)
    {
        $params = [
            'to' => $order->getContactInformation()->getEmail(),
            'subject' => 'Zpráva k objednávce č. '.$order->getOrderNo().' - ' . $this->applicationSettingsManager->getWebsiteTitle(),
            'messageBody' => $messageBody
        ];

        $mail = $this->factory->createByType(CustomerGeneralMail::class, $params + $this->baseParams);
        $mail->send();
    }

    public function sendOrderCanceledEmail(Order $order)
    {
        $params = [
            'to' => $order->getContactInformation()->getEmail(),
            'subject' => 'Storno objednávky č. '.$order->getOrderNo().' - ' . $this->applicationSettingsManager->getWebsiteTitle(),
            'order' => $order,
	        'czechPostBoxBinding' => $this->applicationConfigurationService->getCzechPostBoxDeliveryBinding(),
	        'czechPostOfficeBinding' => $this->applicationConfigurationService->getCzechPostOfficeDeliveryBinding()
        ];

        $mail = $this->factory->createByType(OrderCanceledMail::class, $params + $this->baseParams);
        $mail->send();
    }

	public function sendOrderRejectedEmail(Order $order)
	{
		$params = [
			'to' => $order->getContactInformation()->getEmail(),
			'subject' => 'Nepřijatá objednávka č. '.$order->getOrderNo().' - ' . $this->applicationSettingsManager->getWebsiteTitle(),
			'order' => $order,
		];

		$mail = $this->factory->createByType(OrderRejectedMail::class, $params + $this->baseParams);
		$mail->send();
	}

	public function sendOrderNotPaidEmail(Order $order)
	{
		$params = [
			'to' => $order->getContactInformation()->getEmail(),
			'subject' => 'Neuhrazená objednávka č. '.$order->getOrderNo().' - ' . $this->applicationSettingsManager->getWebsiteTitle(),
			'order' => $order,
		];

		$mail = $this->factory->createByType(OrderNotPaidMail::class, $params + $this->baseParams);
		$mail->send();
	}

    public function sendWatchDogProductAvailableEmail(Variant $priceVariant, $email)
    {
        $product = $priceVariant->getProduct();
        $params = [
            'to' => $email,
            'subject' => 'Sledovaný produkt je opět dostupný - ' . $this->applicationSettingsManager->getWebsiteTitle(),
            'productName' => $product->getName() . ' - ' . $priceVariant->getName(),
            'urlArgs' => $product->getUrlArgs()
        ];

        $mail = $this->factory->createByType(WatchDogMail::class, $params + $this->baseParams);
        $mail->send();
    }

	public function sendProductDemandEmail(Product $product, string $customer, string $email, string $phone, string $message)
	{
		$productDemandEmailTos = explode(';', $this->applicationSettingsManager->getProductDemandEmailsTo());

		if (empty($productDemandEmailTos)) {
			$productDemandEmailTos = [ $this->applicationSettingsManager->getMainEmail() ];
		}

		$params = [
			'productNo' => $product->getNumber(),
			'productName' => $product->getName(),
			'customer' => [
				'name' => $customer,
				'email' => $email,
				'phone' => $phone,
				'message' => $message
			],
			'to' => $productDemandEmailTos,
			'subject' => "{$this->applicationSettingsManager->getWebsiteTitle()} - nová poptávka na produkt {$product->getName()}",
		];

		$mail = $this->factory->createByType(ProductDemandMail::class, $params + $this->baseParams);
		$mail->send();
	}

	public function getLastSentEmailFilePath()
	{
		$logDirectory = $this->applicationConfigurationService->getMailsLogDirectory();
		$dir = $logDirectory.'/'.date('Y').'/'.date('Y-m').'/'.date('Y-m-d');

		$files = [];

		foreach (scandir($dir) as $file) {
			if ($file != '.' && $file != '..') {
				$files[] = $dir . '/' . $file;
			}
		}

		$files = array_combine($files, array_map('filectime', $files));
		arsort($files);
		return str_replace($logDirectory, '', key($files));
    }
}