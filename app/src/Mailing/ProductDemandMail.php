<?php

namespace App\Mailing;

use Nette;
use Ublaboo\Mailing\IComposableMail;
use Ublaboo\Mailing\Mail;

class ProductDemandMail extends Mail implements IComposableMail
{
	/**
	 * @param  Nette\Mail\Message $message
	 * @param  mixed $params
	 * @return mixed
	 */
	public function compose(Nette\Mail\Message $message, $params = null)
	{
		$to = array_shift($params['to']);

		$message->setFrom($params['from']);
		$message->addTo($to);

		if (!empty($params['to'])) {
			foreach ($params['to'] as $copy) {
				$message->addCc($copy);
			}
		}

		$message->setSubject($params['subject']);
	}
}