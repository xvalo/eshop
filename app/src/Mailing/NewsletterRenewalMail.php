<?php

namespace App\Mailing;

use Nette;
use Ublaboo\Mailing\IComposableMail;
use Ublaboo\Mailing\Mail;

class NewsletterRenewalMail extends Mail implements IComposableMail
{
	/**
	 * @param  Nette\Mail\Message $message
	 * @param  mixed $params
	 * @return mixed
	 */
	public function compose(Nette\Mail\Message $message, $params = null)
	{
		$message->setFrom($params['from']);
		$message->addTo($params['to']);
		$message->setSubject($params['subject']);
	}
}