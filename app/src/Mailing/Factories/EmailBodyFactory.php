<?php

namespace App\Mailing\Factories;


use App\Core\Application\Settings\ApplicationSettingsManager;
use Latte\Engine;
use Nette\SmartObject;

class EmailBodyFactory
{
	use SmartObject;

	/** @var ApplicationSettingsManager */
	private $applicationSettingsManager;

	public function __construct(ApplicationSettingsManager $applicationSettingsManager)
	{
		$this->applicationSettingsManager = $applicationSettingsManager;
	}

	public function getGeneralMailBody()
	{
		$template = new Engine();
		$params = [
			'webTitle' => $this->applicationSettingsManager->getWebsiteTitle()
		];
		return $template->renderToString(__DIR__ . '/templates/generalMailBody.latte', $params);
	}

	public function getUnavailableProductsMailBody($orderLines)
	{
		$template = new Engine();
		$params = [
			'webTitle' => $this->applicationSettingsManager->getWebsiteTitle(),
			'orderLines' => $orderLines
		];
		return $template->renderToString(__DIR__ . '/templates/unavailableProductsMailBody.latte', $params);
	}
}