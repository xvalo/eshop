<?php

namespace App\Mailing;

class OrderEmailType
{
	const CREATED = 1;
	const CANCELED = 2;
	const GENERAL = 3;
	const SHIPPED = 4;
	const TO_BE_PICKED = 5;
	const REJECTED = 6;
	const NOT_PAID = 7;

	public static $labels = [
		self::CREATED => 'Vytvořena',
		self::CANCELED => 'Storno',
		self::GENERAL => 'Obecná zpráva',
		self::SHIPPED => 'Odesláno',
		self::TO_BE_PICKED => 'K vyzvednutí',
		self::REJECTED => 'Nepřevzata',
		self::NOT_PAID => 'Neuhrazena'
	];
}