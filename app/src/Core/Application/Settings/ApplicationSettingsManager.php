<?php

namespace App\Core\Application\Settings;

use App\Classes\Eet\ReceiptSender;
use App\Core\Application\Configuration\ApplicationConfigurationRepository;
use App\Lists\SystemVariable;
use App\Lists\SystemVariableRepository;
use Contributte\Cache\CacheFactory;
use Nette\Caching\Cache;
use Nette\Utils\Strings;

class ApplicationSettingsManager
{

	/** @var SystemVariableRepository */
	private $systemVariableRepository;

	/** @var ApplicationConfigurationRepository */
	private $applicationConfigurationRepository;

	/** @var \Nette\Caching\Cache  */
	private $systemVariableCache;

	public function __construct(
		SystemVariableRepository $systemVariableRepository,
		ApplicationConfigurationRepository $applicationConfigurationRepository,
		CacheFactory $cacheFactory
	){
		$this->systemVariableRepository = $systemVariableRepository;
		$this->applicationConfigurationRepository = $applicationConfigurationRepository;
		$this->systemVariableCache = $cacheFactory->create('system-variable');
	}

	/**
	 * @return bool
	 * @throws NotExistingSystemVariableException
	 */
	public function isEetProductionMode(): bool
	{
		return $this->applicationConfigurationRepository->getParameter(['eetMode']) == ReceiptSender::ENVIRONMENT_PROD
				&& intval($this->getEetProductionMode()) == 1;
	}

	/**
	 * @param $method
	 * @param $arguments
	 * @return mixed|string
	 */
	public function __call($method, $arguments)
	{
		$op = Strings::substring($method, 0, 3);
		$key = Strings::firstLower(Strings::substring($method, 3));

		if ($op === 'get') {
			return $this->getVariableFromCache($key);
		}

		if ($op === 'set') {
			$this->setSystemVariableValue($key, $arguments[0]);
		}
	}

	// ------------ PRIVATE ---------------

	/**
	 * @param string $key
	 * @return string
	 * @throws NotExistingSystemVariableException
	 */
	private function getSystemVariableValue(string $key): string
	{
		/** @var SystemVariable $systemVariable */
		$systemVariable = $this->systemVariableRepository->getVariableByKey($key);

		if (!$systemVariable) {
			throw new NotExistingSystemVariableException('System variable is not set up in database.');
		}

		return $systemVariable->getValue();
	}

	/**
	 * @param string $key
	 * @param $value
	 */
	private function setSystemVariableValue(string $key, $value)
	{
		/** @var SystemVariable $systemVariable */
		$systemVariable = $this->systemVariableRepository->getVariableByKey($key);

		if (!$systemVariable) {
			throw new NotExistingSystemVariableException('System variable is not set up in database.');
		}

		$systemVariable->setValue($value);
	}

	private function getVariableFromCache(string $variableName)
	{
		if (!$variable = $this->systemVariableCache->load($variableName, false)) {
			$variable = $this->getSystemVariableValue($variableName);
			$this->systemVariableCache->save($variableName, $variable, [Cache::TAGS => ['system-settings-variables']]);
		}

		return $variable;

	}
}