<?php

namespace App\Core\Application\Settings;

use App\InvalidStateException;

class NotExistingSystemVariableException extends InvalidStateException
{

}