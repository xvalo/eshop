<?php

namespace App\Model\Services;

use App\Core\Application\Configuration\ApplicationConfigurationRepository;
use App\Core\Application\Settings\ApplicationSettingsManager;
use Nette\SmartObject;

class ApplicationConfigurationService
{
	use SmartObject;

	/** @var ApplicationConfigurationRepository */
	private $applicationConfigurationRepository;

	/** @var ApplicationSettingsManager */
	private $applicationSettingsManager;

	public function __construct(
		ApplicationConfigurationRepository $applicationConfigurationRepository,
		ApplicationSettingsManager $applicationSettingsManager
	){
		$this->applicationConfigurationRepository = $applicationConfigurationRepository;
		$this->applicationSettingsManager = $applicationSettingsManager;
	}

	public function getSoldOutProductAvailabilityState()
	{
		return $this->applicationConfigurationRepository->getParameter(['App', 'stock', 'soldOutProductState']);
	}

	public function getAvailabilityColors()
	{
		return $this->applicationConfigurationRepository->getParameter(['App', 'availabilityColors']);
	}

	public function getHeurekaAppId()
	{
		return $this->applicationConfigurationRepository->getParameter(['App', 'heureka', 'appId']);
	}

	/**
	 * @return string
	 */
	public function getSupplierSourceFeedTempDirectory(): string
	{
		return $this->applicationConfigurationRepository->getParameter(['suppliersSynchronization', 'source', 'tempDirectory']);
	}

	// ------------- Mailing settings -----------------

	public function getMailsLogDirectory()
	{
		return $this->applicationConfigurationRepository->getParameter(['mailing', 'logDir']);
	}

	// ------------- Delivery types settings -----------------

	public function getDeliveriesWithTransferPayments()
	{
		return $this->applicationConfigurationRepository->getParameter(['App', 'delivery', 'transferPayment']);
	}

	public function getSlovakDeliveries()
	{
		return $this->applicationConfigurationRepository->getParameter(['App', 'delivery', 'slovak']);
	}

	public function getGlsDeliveries()
	{
		return $this->applicationConfigurationRepository->getParameter(['App', 'delivery', 'gls']);
	}

	public function getCzechPostDeliveries()
	{
		return $this->applicationConfigurationRepository->getParameter(['App', 'delivery', 'cp']);
	}

	public function getPersonalPickupDeliveries()
	{
		return $this->applicationConfigurationRepository->getParameter(['App', 'delivery', 'pickUpPersonal']);
	}

	// ------------- Documents repositories settings -----------------

	public function getPdfDirectoryPath()
	{
		return $this->applicationConfigurationRepository->getParameter(['documents', 'pdfDirectory']);
	}

	public function getStockFilesDirectoryPath()
	{
		return $this->applicationConfigurationRepository->getParameter(['documents', 'stockFiles']);
	}

	public function getInventoryValuationFilesDirectoryPath()
	{
		return $this->applicationConfigurationRepository->getParameter(['documents', 'inventoryValuationFiles']);
	}

	public function getDocumentFooterText()
	{
		return $this->applicationConfigurationRepository->getParameter(['App', 'documentSettings', 'footerText']);
	}

	public function getDocumentContractorAddress()
	{
		return $this->applicationConfigurationRepository->getParameter(['App', 'documentSettings', 'contractorAddress']);
	}

	public function getDocumentOfficeAddress()
	{
		return $this->applicationConfigurationRepository->getParameter(['App', 'documentSettings', 'officeAddress']);
	}

	// ------------- Feeds settings ----------------------

	public function getGoogleMerchantFeedPath()
	{
		return $this->applicationConfigurationRepository->getParameter(['feeds', 'googleMerchant', 'path']);
	}

	public function getGoogleMerchantFeedData()
	{
		return $this->applicationConfigurationRepository->getParameter(['feeds', 'googleMerchant', 'data']);
	}

	// ------------- Images settings ----------------------

	public function isWatermarkAllowed()
	{
		return $this->applicationConfigurationRepository->getParameter(['images', 'watermark', 'allowed']) === true
				&& $this->applicationConfigurationRepository->getParameter(['images', 'watermark', 'path']) !== null;
	}

	public function getWatermarkImagePath()
	{
		return $this->applicationConfigurationRepository->getParameter(['images', 'watermark', 'path']);
	}

	public function getProductImagesStorage()
	{
		return $this->getImageStorage('product');
	}

	public function getAttributeImagesStorage()
	{
		return $this->getImageStorage('attribute');
	}

	public function getNewsImagesStorage()
	{
		return $this->getImageStorage('news');
	}

	public function getDefaultImageStorage()
	{
		return $this->getImageStorage();
	}

	private function getImageStorage(string $storageName = 'default')
	{
		$storage = $storageName !== 'default' ? $this->applicationConfigurationRepository->getParameter(['images', $storageName, 'storage']) : null;
		return $storage === null ? $this->applicationConfigurationRepository->getParameter(['images', 'defaultStorage']) : $storage;
	}
	
	// ------------- Delivery settings getters -----------------

	public function getCzechPostBoxDeliveryBinding()
	{
		return (int)$this->applicationConfigurationRepository->getParameter(['delivery', 'deliveryBinding', 'branch']);
	}

	public function getCzechPostOfficeDeliveryBinding()
	{
		return (int)$this->applicationConfigurationRepository->getParameter(['delivery', 'deliveryBinding', 'office']);
	}

	// ------------- EET settings getters -----------------

	public function getDic()
	{
		return $this->applicationConfigurationRepository->getParameter(['eet', $this->getEetSettingsNamespace(), 'dic']);
	}

	public function getCertificatePassword()
	{
		return $this->applicationConfigurationRepository->getParameter(['eet', $this->getEetSettingsNamespace(), 'password']);
	}

	public function getEetPubCertificate()
	{
		return $this->applicationConfigurationRepository->getParameter(['eet', $this->getEetSettingsNamespace(), 'certificates', 'public']);
	}

	public function getEetPrivCertificate()
	{
		return $this->applicationConfigurationRepository->getParameter(['eet', $this->getEetSettingsNamespace(), 'certificates', 'private']);
	}

	public function getEetSchema()
	{
		return $this->applicationConfigurationRepository->getParameter(['eet', $this->getEetSettingsNamespace(), 'schema']);
	}

	public function getShopId()
	{
		return $this->applicationConfigurationRepository->getParameter(['eet', $this->getEetSettingsNamespace(), 'shopID']);
	}

	public function getCashRegisterId()
	{
		return $this->applicationConfigurationRepository->getParameter(['eet', $this->getEetSettingsNamespace(), 'cashRegisterID']);
	}

	public function isEetVerificationMode()
	{
		return $this->applicationConfigurationRepository->getParameter(['eet', $this->getEetSettingsNamespace(), 'verificationMode']);
	}

	// --------- PRIVATE FUNCTIONS ------------

	private function getEetSettingsNamespace()
	{
		return $this->applicationSettingsManager->isEetProductionMode() ? 'production' : 'playground';
	}
}
