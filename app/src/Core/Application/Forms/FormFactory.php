<?php

namespace App\Core\Forms;

use Nette;
use Nette\Application\UI\Form;


class FormFactory
{
	use Nette\SmartObject;

	/**
	 * @return Form
	 */
	public function create()
	{
		$form = new Form;

		$renderer = $form->getRenderer();

		$form->getElementPrototype()->class('form-horizontal'); //ajax

		$renderer->wrappers['controls']['container'] = 'div class="form-body"';
		$renderer->wrappers['pair']['container'] = 'div class=form-group';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['control']['.required'] = '';
		$renderer->wrappers['control']['.text'] = 'form-control';
		$renderer->wrappers['control']['.submit'] = 'btn btn-success';
		$renderer->wrappers['control']['container'] = 'div class=col-md-9';
		$renderer->wrappers['label']['container'] = 'div class="col-md-3 control-label"';
		$renderer->wrappers['label']['requiredsuffix'] = '<span class="required"> * </span>';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';

		return $form;
	}
}
