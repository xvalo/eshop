<?php

namespace App\Core\Application\Listeners;

use App\Lists\SystemVariable;
use Contributte\Cache\CacheFactory;
use Doctrine\ORM\Events;
use Kdyby\Events\Subscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Nette\Caching\Cache;

class SystemVariableListener implements Subscriber
{

	/** @var \Nette\Caching\Cache  */
	private $systemVariableCache;

	public function __construct(CacheFactory $cacheFactory)
	{
		$this->systemVariableCache = $cacheFactory->create('system-variable');
	}

	public function getSubscribedEvents()
	{
		return [
			Events::onFlush
		];
	}

	public function onFlush(OnFlushEventArgs $eventArgs)
	{
		$em = $eventArgs->getEntityManager();
		$uow = $em->getUnitOfWork();

		foreach ($uow->getScheduledEntityInsertions() as $entity) {
			if ($entity instanceof SystemVariable) {
				$this->systemVariableCache->clean([Cache::TAGS => ['system-settings-variables']]);
			}
		}

		foreach ($uow->getScheduledEntityUpdates() as $entity) {
			if ($entity instanceof SystemVariable) {
				$this->systemVariableCache->clean([Cache::TAGS => ['system-settings-variables']]);
			}
		}
	}
}