<?php

namespace App\Core\Socials\TwitterCard;

class TwitterCardDataObject
{
	/**
	 * @var string
	 */
	private $type = 'summary';

	/**
	 * @var string
	 */
	private $title;

	/**
	 * @var string
	 */
	private $description;

	/**
	 * @var string|null
	 */
	private $imageUrl;

	/**
	 * @var string|null
	 */
	private $site;


	public function __construct(string $title, string $description, string $imageUrl = null, string $site = null)
	{
		$this->title = $title;
		$this->description = $description;
		$this->imageUrl = $imageUrl;
		$this->site = $site;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return $this->description;
	}

	/**
	 * @return null|string
	 */
	public function getImageUrl()
	{
		return $this->imageUrl;
	}

	/**
	 * @return null|string
	 */
	public function getSite()
	{
		return $this->site;
	}
}