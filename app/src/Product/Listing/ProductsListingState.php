<?php

namespace App\Product\Listing;

use App\Product\ProductDTO;

class ProductsListingState
{
	/** @var ProductsListingPagination */
	private $pagination;

	/** @var ProductDTO[]|array */
	private $products;

	public function __construct(ProductsListingPagination $pagination, array $products)
	{
		$this->pagination = $pagination;
		$this->products = $products;
	}

	/**
	 * @return ProductsListingPagination
	 */
	public function getPagination(): ProductsListingPagination
	{
		return $this->pagination;
	}

	/**
	 * @return ProductDTO[]|array
	 */
	public function getProducts()
	{
		return $this->products;
	}
}