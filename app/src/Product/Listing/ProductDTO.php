<?php

namespace App\Product\Listing;

class ProductDTO
{

	/**
	 * @var string
	 */
	private $name;
	/**
	 * @var string
	 */
	private $url;
	/**
	 * @var string
	 */
	private $imagePath;
	/**
	 * @var int
	 */
	private $generalAvailability;
	/**
	 * @var float
	 */
	private $normalPrice;
	/**
	 * @var float
	 */
	private $minPrice;
	/**
	 * @var bool
	 */
	private $isNews;
	/**
	 * @var bool
	 */
	private $isSpecialOffer;
	/**
	 * @var bool
	 */
	private $hasMorePrices;
	/**
	 * @var string
	 */
	private $annotation;

	public function __construct(string $name, string $url, string $imagePath, int $generalAvailability, float $normalPrice, float $minPrice, bool $isNews, bool $isSpecialOffer, bool $hasMorePrices, string $annotation)
	{

		$this->name = $name;
		$this->url = $url;
		$this->imagePath = $imagePath;
		$this->generalAvailability = $generalAvailability;
		$this->normalPrice = $normalPrice;
		$this->minPrice = $minPrice;
		$this->isNews = $isNews;
		$this->isSpecialOffer = $isSpecialOffer;
		$this->hasMorePrices = $hasMorePrices;
		$this->annotation = $annotation;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return $this->url;
	}

	/**
	 * @return string
	 */
	public function getImagePath(): string
	{
		return $this->imagePath;
	}

	/**
	 * @return int
	 */
	public function getGeneralAvailability(): int
	{
		return $this->generalAvailability;
	}

	/**
	 * @return float
	 */
	public function getNormalPrice(): float
	{
		return $this->normalPrice;
	}

	/**
	 * @return float
	 */
	public function getMinPrice(): float
	{
		return $this->minPrice;
	}

	/**
	 * @return bool
	 */
	public function isNews(): bool
	{
		return $this->isNews;
	}

	/**
	 * @return bool
	 */
	public function isSpecialOffer(): bool
	{
		return $this->isSpecialOffer;
	}

	/**
	 * @return bool
	 */
	public function isHasMorePrices(): bool
	{
		return $this->hasMorePrices;
	}

	/**
	 * @return string
	 */
	public function getAnnotation(): string
	{
		return $this->annotation;
	}
}