<?php

namespace App\Product\Listing;

class ProductsListingPagination
{
	/** @var int */
	private $page;

	/** @var int */
	private $pagesCount;

	/** @var int */
	private $itemsCount;

	/** @var int */
	private $itemsPerPage;

	public function __construct(int $itemsCount, int $itemsPerPage, int $page, int $pagesCount)
	{
		$this->page = $page;
		$this->pagesCount = $pagesCount;
		$this->itemsCount = $itemsCount;
		$this->itemsPerPage = $itemsPerPage;
	}

	/**
	 * @return int
	 */
	public function getPage(): int
	{
		return $this->page;
	}

	/**
	 * @return int
	 */
	public function getPagesCount(): int
	{
		return $this->pagesCount;
	}

	/**
	 * @return int
	 */
	public function getItemsCount(): int
	{
		return $this->itemsCount;
	}

	/**
	 * @return int
	 */
	public function getItemsPerPage(): int
	{
		return $this->itemsPerPage;
	}
}