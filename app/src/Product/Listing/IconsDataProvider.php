<?php

namespace App\Product\Listing;

use App\Product\Product;
use Nette\SmartObject;
use App\Product\ProductDTO;

class IconsDataProvider
{
	use SmartObject;

	/**
	 * @param ProductDTO $product
	 * @return array
	 */
	public function getIcons(ProductDTO $product): array
	{
		$icons = [];

		if ($product->isSpecialOffer()) {
			$icons[] = [
				'type' => 'akce',
				'text' => 'Akce'
			];
		}

		if ($product->isNews()) {
			$icons[] = [
				'type' => 'novinka',
				'text' => 'Novinka'
			];
		}

		if ($product->getGeneralAvailability() == 1) {
			$icons[] = [
				'type' => 'skladem',
				'text' => 'Skladem'
			];
		}


		// TODO: Výprodej
		// TODO: Doprava zdarma

		return $icons;
	}

}