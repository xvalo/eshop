<?php

namespace App\Product\Listing\Queries;

use App\Filter\Entity\VariantFilterValue;
use App\Product\Listing\Filters\ProductFilter;
use App\Product\Product;
use Doctrine;
use Kdyby;

class ProductQuery extends Kdyby\Doctrine\QueryObject
{
    /** @var callable[] */
    private $filters = [];

    public function filtered(ProductFilter $filter)
    {
        $this->filters[] = function (Doctrine\ORM\QueryBuilder $qb) use ($filter) {

            if ($categories = $filter->getCategories()) {
                $qb->andWhere($qb->expr()->in('P.category', ':categories'))
                                ->setParameter('categories', $categories);
            }

            if ($producers = $filter->getProducers()) {
                $qb->andWhere($qb->expr()->in('P.producer', ':producers'))
                        ->setParameter('producers', $producers);
            }

            if ($filter->isSpecialOffer()) {
                $qb->andWhere('P.specialOffer = 1');
            }

            if ($filter->isOnlyAvailable()) {
            	$qb->andWhere(
                    	$qb->expr()->eq('A.productOrderable', true)
                    );
            }

            if ($filter->isMainPage()) {
                $qb->andWhere($qb->expr()->eq('P.mainPage', 1));
            }

            if ($phrase = $filter->getPhrase()) {
                $qb->andWhere(
                    $qb->expr()->orX(
                        $qb->expr()->like('P.name', ':phrase'),
                        $qb->expr()->like('V.name', ':phrase'),
                        $qb->expr()->like('P.number', ':phrase'),
	                    $qb->expr()->like('P.annotation', ':phrase'),
	                    $qb->expr()->like('P.description', ':phrase'),
	                    $qb->expr()->like('C.name', ':phrase'),
	                    $qb->expr()->like('C.description', ':phrase')
                    )
                )->setParameter('phrase', '%'.$phrase.'%');
            }

            if ($values = $filter->getFiltersValues()) {
            	$counter = 1;

	            foreach ($values as $filterId => $valuesIds) {

		            $qb->leftJoin(
		            	VariantFilterValue::class,
			            "VFV{$counter}",
			            Kdyby\Doctrine\Dql\Join::WITH,
			            "V = VFV{$counter}.productVariant AND VFV{$counter}.filter = :filter{$counter}"
		            );

		            $valuesConditions = [];
		            foreach ($valuesIds as $valueId) {
		            	$valuesConditions[] = "VFV{$counter}.filterValue = {$valueId}";
		            }

		            $qb->andWhere(
			            $qb->expr()->eq("VFV{$counter}.filter", ":filter{$counter}"),
			            $qb->expr()->orX()->addMultiple($valuesConditions)
		            )->setParameter(":filter{$counter}", $filterId);
		            $counter++;
	            }

            }

            if ($filter->getMinPrice() !== null) {
            	$qb->andWhere(
            		$qb->expr()->gte('P.minPrice', ':minPrice')
	            )->setParameter('minPrice', $filter->getMinPrice());
            }

	        if ($filter->getMaxPrice() !== null) {
		        $qb->andWhere(
			        $qb->expr()->lte('P.minPrice', ':maxPrice')
		        )->setParameter('maxPrice', $filter->getMaxPrice());
	        }

            switch ($filter->getSort()) {
                case $filter::SORT_BY_PRICE_ASC:
                    $qb->orderBy('P.minPrice', 'ASC');
                    break;
                case $filter::SORT_BY_PRICE_DESC:
                    $qb->orderBy('P.minPrice', 'DESC');
                    break;
                case $filter::SORT_BY_LATEST:
                    $qb->orderBy('P.id', 'DESC');
                    break;
                default:
                    $qb->orderBy('A.priority', 'ASC')
                        ->addOrderBy('P.id', 'ASC');
                    break;
            }
        };

        return $this;
    }

    /**
     * @param \Kdyby\Persistence\Queryable $repository
     * @return \Doctrine\ORM\Query|\Doctrine\ORM\QueryBuilder
     */
    protected function doCreateQuery(Kdyby\Persistence\Queryable $repository)
    {
        $qb = $repository->createQueryBuilder()
                            ->select('NEW App\Product\ProductDTO(P.id, P.name, P.url, C.url, I.path, A.id, MIN(V.normalPrice), P.minPrice, P.news, P.specialOffer, COUNT(PR.id), P.annotation)')
                            ->from(Product::class, 'P')
                                ->join('P.category', 'C')
	                            ->leftJoin('P.mainImage', 'I')
	                            ->join('P.variants', 'V')
	                            ->join('V.prices', 'PR')
							    ->join('P.generalAvailability', 'A')
                            ->where('P.deleted = 0')
                            ->andWhere('P.active = 1')
                            ->andWhere('C.active = 1')
                            ->groupBy('P.id');

        foreach ($this->filters as $filter) {
            call_user_func($filter, $qb);
        }

        return $qb;
    }

	protected function doCreateCountQuery(Kdyby\Persistence\Queryable $repository)
	{
		$qb = $repository->createQueryBuilder()
				->select('COUNT(DISTINCT P.id)')
				->from(Product::class, 'P')
					->join('P.category', 'C')
					->leftJoin('P.mainImage', 'I')
					->join('P.variants', 'V')
					->join('P.generalAvailability', 'A')
				->where('P.deleted = 0')
					->andWhere('P.active = 1')
					->andWhere('C.active = 1');

		foreach ($this->filters as $filter) {
			call_user_func($filter, $qb);
		}

		return $qb;
    }
}