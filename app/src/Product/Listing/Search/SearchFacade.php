<?php

namespace App\Product\Listing\Search;

use App\Product\Product;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class SearchFacade
{
	use SmartObject;

	/**
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	/*
	 * TODO: Dokončit fulltext vyhledávání jak to jen bude možné.
	 * Zdroje: https://stackoverflow.com/questions/4226893/mysql-fulltext-on-multiple-tables
	 *      https://forum.nette.org/cs/28795-doctrine-2-syntax-error-u-funkce-match-against
	 *      https://stackoverflow.com/questions/23448266/add-a-fulltext-index-in-doctrine-2-using-annotations
	public function searchFulltext(string $phrase)
	{

		$qb = $this->em->createQueryBuilder();
		$qb->select('P.id, MATCH(P.id) AGAINST(:phrase) AS Pscore')
			->from(Product::class, 'P')
				->join('P.category', 'C')
				->join('P.variants', 'V')
			->where(
				$qb->expr()->andX(
					$qb->expr()->eq('P.deleted', ':deleted'),
					$qb->expr()->eq('P.active', ':active'),
					$qb->expr()->eq('C.active', ':active')
				)
			);

		$qb->setParameters([
			'phrase' => $phrase,
			'active' => true,
			'deleted' => false
		]);

		return $qb->getQuery()->execute();
	}
	*/
}