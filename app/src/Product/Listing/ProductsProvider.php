<?php

namespace App\Product\Listing;

use App\Category\CategoryService;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\Filter\FilterService;
use App\Lists\Producer;
use App\Product\Availability\AvailabilityProvider;
use App\Product\Listing\Filters\ProductFilter;
use App\Product\Listing\Queries\ProductQuery;
use App\Product\ProductService;
use Nette\Application\LinkGenerator;
use Nette\SmartObject;
use Nette\Utils\Paginator;
use Ublaboo\ImageStorage\ImageStorage;

class ProductsProvider
{
	use SmartObject;

	/** @var ImageStorage */
	private $imageStorage;

	/** @var ProductService */
	private $productService;

	/** @var CategoryService */
	private $categoryService;

	/** @var ApplicationSettingsManager */
	private $applicationSettingsManager;

	/** @var Paginator */
	private $paginator;

	/** @var LinkGenerator */
	private $linkGenerator;

	/** @var IconsDataProvider */
	private $iconsDataProvider;

	/** @var array */
	private $availabilityList;

	/** @var FilterService */
	private $filterService;

	public function __construct(
		ImageStorage $imageStorage,
		ProductService $productService,
		CategoryService $categoryService,
		ApplicationSettingsManager $applicationSettingsManager,
		LinkGenerator $linkGenerator,
		IconsDataProvider $iconsDataProvider,
		AvailabilityProvider $availabilityProvider,
		FilterService $filterService
	){
		$this->imageStorage = $imageStorage;
		$this->productService = $productService;
		$this->categoryService = $categoryService;
		$this->applicationSettingsManager = $applicationSettingsManager;
		$this->linkGenerator = $linkGenerator;
		$this->iconsDataProvider = $iconsDataProvider;
		$this->availabilityList = $availabilityProvider->getList();
		$this->filterService = $filterService;
	}

	/**
	 * @return ProductsListingState
	 * @throws \Nette\Application\UI\InvalidLinkException
	 * @throws \Ublaboo\ImageStorage\ImageResizeException
	 */
	public function getHomepageProducts(): ProductsListingState
	{
		$query = $this->getQuery( ProductFilter::createForHomepage() );
		$products = $this->getFilteredProducts($query);

		return new ProductsListingState(
			$this->getPaginatorState(),
			$this->processResult($products)
		);
	}

	/**
	 * @param int $page
	 * @param string $sort
	 * @param bool $specialOffer
	 * @param bool $onlyAvailable
	 * @return ProductsListingState
	 * @throws \Nette\Application\UI\InvalidLinkException
	 * @throws \Ublaboo\ImageStorage\ImageResizeException
	 */
	public function getNewsProducts(int $page, string $sort, bool $specialOffer, bool $onlyAvailable): ProductsListingState
	{
		$query = $this->getQuery( ProductFilter::createForNews($sort, $specialOffer, $onlyAvailable) );
		$products = $this->getFilteredProducts($query, $page);

		return new ProductsListingState(
			$this->getPaginatorState(),
			$this->processResult($products)
		);
	}

	/**
	 * @param int $page
	 * @param Producer $producer
	 * @param string $sort
	 * @param bool $specialOffer
	 * @param bool $onlyAvailable
	 * @return ProductsListingState
	 * @throws \Nette\Application\UI\InvalidLinkException
	 * @throws \Ublaboo\ImageStorage\ImageResizeException
	 */
	public function getProducerProducts(int $page, Producer $producer, string $sort, bool $specialOffer, bool $onlyAvailable): ProductsListingState
	{
		$query = $this->getQuery( ProductFilter::createForProducer($producer, $sort, $specialOffer, $onlyAvailable) );
		$products = $this->getFilteredProducts($query, $page);

		return new ProductsListingState(
			$this->getPaginatorState(),
			$this->processResult($products)
		);
	}

	/**
	 * @param int $page
	 * @param string $sort
	 * @param int $category
	 * @param bool $specialOffer
	 * @param bool $onlyAvailable
	 * @param array $filterValues
	 * @param array $producers
	 * @param array|null $minMaxPrice
	 * @return ProductsListingState
	 * @throws \Nette\Application\UI\InvalidLinkException
	 * @throws \Ublaboo\ImageStorage\ImageResizeException
	 */
	public function getCategoryProducts(int $page, string $sort, int $category, bool $specialOffer, bool $onlyAvailable, array $filterValues, array $producers, array $minMaxPrice = null): ProductsListingState
	{
		$categories = $category !== false ? $this->categoryService->getCategoryTreeIds($category) : [];

		$filtersData = !empty($filterValues) ? $this->filterService->getFiltersIdsForFiltering($filterValues) : [];

		$query = $this->getQuery( ProductFilter::createForCategory($sort, $categories, $specialOffer, $onlyAvailable, $filtersData, $producers, $minMaxPrice['min'], $minMaxPrice['max']) );
		$products = $this->getFilteredProducts($query, $page);

		return new ProductsListingState(
			$this->getPaginatorState(),
			$this->processResult($products)
		);
	}

	/**
	 * @param int $page
	 * @param string $phrase
	 * @param string $sort
	 * @param bool $specialOffer
	 * @param bool $onlyAvailable
	 * @return ProductsListingState
	 * @throws \Nette\Application\UI\InvalidLinkException
	 * @throws \Ublaboo\ImageStorage\ImageResizeException
	 */
	public function getSearchProducts(int $page, string $phrase, string $sort, bool $specialOffer, bool $onlyAvailable): ProductsListingState
	{
		$query = $this->getQuery( ProductFilter::createForSearch($phrase, $sort, $specialOffer, $onlyAvailable) );
		$products = $this->getFilteredProducts($query, $page);

		return new ProductsListingState(
			$this->getPaginatorState(),
			$this->processResult($products)
		);
	}

	/**
	 * @return ProductsListingPagination
	 */
	private function getPaginatorState(): ProductsListingPagination
	{
		return new ProductsListingPagination(
			$this->paginator->getItemCount(),
			$this->paginator->getItemsPerPage(),
			$this->paginator->getPage(),
			$this->paginator->getPageCount()
		);
	}

	/**
	 * @param ProductFilter $filter
	 * @return ProductQuery
	 */
	private function getQuery(ProductFilter $filter): ProductQuery
	{
		return (new ProductQuery())->filtered($filter);
	}

	/**
	 * @param ProductQuery $query
	 * @param int $page
	 * @return Paginator
	 */
	private function getPaginator(ProductQuery $query, int $page = 1): Paginator
	{
		$this->paginator = new Paginator();
		$this->paginator->setItemCount($this->productService->getFilteredProductsCount($query));
		$this->paginator->setPage($page);
		$this->paginator->setItemsPerPage($this->applicationSettingsManager->getProductsPaginationLimit());

		return $this->paginator;
	}

	/**
	 * @param ProductQuery $query
	 * @param int $page
	 * @return array
	 */
	private function getFilteredProducts(ProductQuery $query, int $page = 1): array
	{
		$result = $this->productService
			->getFilteredProducts($query)
			->setUseOutputWalkers(false)
			->setFetchJoinCollection(false)
			->applyPaginator($this->getPaginator($query, $page));

		return $result->toArray();
	}

	/**
	 * @param array $result
	 * @return array
	 * @throws \Nette\Application\UI\InvalidLinkException
	 * @throws \Ublaboo\ImageStorage\ImageResizeException
	 */
	private function processResult(array $result): array
	{
		$products = [];

		/** @var \App\Product\ProductDTO $product */
		foreach ($result as $product) {
			$products[] = [
				'id' => $product->getId(),
				'name' => $product->getName(),
				'url' => $this->linkGenerator->link('Front:Product:default', [$product->getCategoryUrl(), $product->getUrl()]),
				'image' => '/'.$this->imageStorage->fromIdentifier([$product->getImagePath(), '240x240', 'fit', 70])->createLink(),
				'availability' => $product->getGeneralAvailability(),
				'availabilityData' => [
					'title' => $this->availabilityList[$product->getGeneralAvailability()]['title'],
					'className' => $this->availabilityList[$product->getGeneralAvailability()]['class'],
					'description' => $this->availabilityList[$product->getGeneralAvailability()]['description'],
					'isProductOrderable' => $this->availabilityList[$product->getGeneralAvailability()]['isProductOrderable']
				],
				'wasPrice' => $product->getNormalPrice(),
				'currentPrice' => $product->getMinPrice(),
				'isNews' => $product->isNews(),
				'specialOffer' => $product->isSpecialOffer(),
				'annotation' => $product->getAnnotation(),
				'hasMorePrices' => $product->hasMorePrices(),
				'icons' => $this->iconsDataProvider->getIcons($product)
			];
		}

		return $products;
	}

}