<?php

namespace App\Product\Listing\Filters;

use App\Filter\Entity\Value;
use App\Lists\Producer;

class ProductFilter
{
	const SORT_DEFAULT = 'default';
    const SORT_BY_LATEST = 'latest';
    const SORT_BY_PRICE_ASC = 'price-asc';
    const SORT_BY_PRICE_DESC = 'price-desc';

    /** @var string */
    private $sort;

    /** @var array|int[] */
    private $categories;

    /** @var boolean */
    private $specialOffer = false;

    /** @var boolean */
    private $onlyAvailable = false;

    /** @var boolean */
    private $mainPage = false;

    /** @var string|null */
    private $phrase;

	/** @var array|Value[] */
	private $filtersValues;

	/** @var array */
	private $producers;

	/** @var int|null */
	private $minPrice = null;

	/** @var int|null */
	private $maxPrice = null;

	/**
	 * @return ProductFilter
	 */
	public static function createForHomepage(): ProductFilter
	{
		return new static(self::SORT_DEFAULT, [], false, false, true);
	}

	/**
	 * @param string $sort
	 * @param array $categories
	 * @param bool $specialOffer
	 * @param bool $onlyAvailable
	 * @param array $filterValues
	 * @param array $producers
	 * @param int|null $minPrice
	 * @param int|null $maxPrice
	 * @return ProductFilter
	 */
	public static function createForCategory(
		string $sort,
		array $categories,
		bool $specialOffer,
		bool $onlyAvailable,
		array $filterValues,
		array $producers,
		int $minPrice = null,
		int $maxPrice = null
	): ProductFilter
	{
		return new static($sort, $categories, $specialOffer, $onlyAvailable, false, null, $filterValues, $producers, $minPrice, $maxPrice);
	}

	/**
	 * @param string $phrase
	 * @param string $sort
	 * @param bool $specialOffer
	 * @param bool $onlyAvailable
	 * @return ProductFilter
	 */
	public static function createForSearch(string $phrase, string $sort, bool $specialOffer, bool $onlyAvailable): ProductFilter
	{
		return new static($sort, [], $specialOffer, $onlyAvailable, false, $phrase, []);
	}

	/**
	 * @param string $sort
	 * @param bool $specialOffer
	 * @param bool $onlyAvailable
	 * @return ProductFilter
	 */
	public static function createForNews(string $sort, bool $specialOffer, bool $onlyAvailable): ProductFilter
	{
		return new static($sort, [], $specialOffer, $onlyAvailable, false);
	}

	/**
	 * @param Producer $producer
	 * @param string $sort
	 * @param bool $specialOffer
	 * @param bool $onlyAvailable
	 * @return ProductFilter
	 */
	public static function createForProducer(Producer $producer, string $sort, bool $specialOffer, bool $onlyAvailable): ProductFilter
	{
		return new static($sort, [], $specialOffer, $onlyAvailable, false, null, [], [ $producer->getId() ]);
	}

	public function __construct(
		string $sort,
		array $categories,
		bool $specialOffer,
		bool $onlyAvailable,
		bool $mainPage,
		string $phrase = null,
		array $filtersValues = [],
		array $producers = [],
		int $minPrice = null,
		int $maxPrice = null
	){
		$this->sort = $sort;
		$this->categories = $categories;
		$this->specialOffer = $specialOffer;
		$this->onlyAvailable = $onlyAvailable;
		$this->mainPage = $mainPage;
		$this->phrase = $phrase !== '' ? $phrase : null;
		$this->filtersValues = $filtersValues;
		$this->producers = $producers;
		$this->minPrice = $minPrice;
		$this->maxPrice = $maxPrice;
	}

    /**
     * @return string|null
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @return array
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return bool
     */
    public function isSpecialOffer()
    {
        return $this->specialOffer;
    }

    /**
     * @return bool
     */
    public function isOnlyAvailable()
    {
        return $this->onlyAvailable;
    }

    /**
     * @return bool
     */
    public function isMainPage()
    {
        return $this->mainPage;
    }

    /**
     * @return string|null
     */
    public function getPhrase()
    {
        return $this->phrase;
    }

	/**
	 * @return Value[]|array
	 */
	public function getFiltersValues()
	{
		return $this->filtersValues;
	}

	/**
	 * @return array|null
	 */
	public function getProducers()
	{
		return $this->producers;
	}

	/**
	 * @return int|null
	 */
	public function getMinPrice()
	{
		return $this->minPrice;
	}

	/**
	 * @return int|null
	 */
	public function getMaxPrice()
	{
		return $this->maxPrice;
	}

}