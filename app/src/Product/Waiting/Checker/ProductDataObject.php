<?php

namespace App\Product\Waiting\Checker;

class ProductDataObject
{
	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var float
	 */
	private $price;

	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var float|null
	 */
	private $purchasePrice;

	/**
	 * @var string|null
	 */
	private $imageUrl;

	/**
	 * @var string|null
	 */
	private $description;

	/**
	 * @var string|null
	 */
	private $url;

	/**
	 * @var string|null
	 */
	private $ean;

	/**
	 * @var string|null
	 */
	private $producer;

	/**
	 * @var array
	 */
	private $images;

	public function __construct(string $name, float $price, string $code, array $images = [], string $description = null, string $url = null, string $ean = null, string $producer = null, float $purchasePrice = null)
	{
		$this->name = $name;
		$this->price = $price;
		$this->images = $images;
		$this->description = $description;
		$this->url = $url;
		$this->code = $code;
		$this->ean = $ean;
		$this->producer = $producer;
		$this->purchasePrice = $purchasePrice;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return float
	 */
	public function getPrice(): float
	{
		return $this->price;
	}

	/**
	 * @return string
	 */
	public function getCode(): string
	{
		return $this->code;
	}

	/**
	 * @return null|string
	 */
	public function getImageUrl()
	{
		return $this->imageUrl;
	}

	/**
	 * @return null|string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @return null|string
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @return null|string
	 */
	public function getEan()
	{
		return $this->ean;
	}

	/**
	 * @return null|string
	 */
	public function getProducer()
	{
		return $this->producer;
	}
	/**
	 * @return float|null
	 */
	public function getPurchasePrice()
	{
		return $this->purchasePrice;
	}

	/**
	 * @return array
	 */
	public function getImages(): array
	{
		return $this->images;
	}

	public function getFirstImage()
	{
		if (!empty($this->images)) {
			return $this->images[0];
		}

		return false;
	}


	public function __wakeup()
	{
		if ($this->imageUrl !== null) {
			$this->images[] = $this->imageUrl;
		}
	}
}