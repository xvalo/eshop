<?php

namespace App\Product\Waiting\Checker;

use App\Stock\Supplier\Entity\Supplier;
use App\Product\VariantRepository;
use App\Product\Waiting\Entity\Product;
use App\Product\Waiting\WaitingProductFacade;
use App\Stock\Supplier\SupplierService;
use Nette\SmartObject;

abstract class BaseProductChecker
{
	use SmartObject;

	/** @var Supplier */
	private $supplier;

	/** @var string  */
	private $source;

	/** @var ProductDataObject[] */
	private $loadedData;
	
	/** @var WaitingProductFacade */
	private $waitingProductFacade;
	
	/** @var VariantRepository */
	private $variantRepository;

	public function __construct(
		int $supplierId,
		string $sourceUrl,
		WaitingProductFacade $waitingProductFacade,
		VariantRepository $variantRepository,
		SupplierService $supplierService
	){
		$this->supplier = $supplierService->getSupplier($supplierId);
		$this->source = $sourceUrl;
		$this->waitingProductFacade = $waitingProductFacade;
		$this->variantRepository = $variantRepository;
	}

	abstract protected function loadData();

	public function init()
	{
		$this->loadData();
	}

	public function getSource()
	{
		return $this->source;
	}

	public function uploadWaitingProducts(array $products)
	{
		/** @var ProductDataObject $dataObject */
		foreach($products as $dataObject) {
			$this->waitingProductFacade->saveWaitingProduct(
				new Product(
					$this->getSupplier(),
					$dataObject
				)
			);
		}
	}

	/**
	 * @return Supplier|null
	 */
	protected function getSupplier()
	{
		return $this->supplier;
	}

	protected function addNewItem(ProductDataObject $item)
	{
		$this->loadedData[$item->getCode()] = $item;
	}

	/**
	 * @return ProductDataObject[]
	 */
	protected function getUnprocessedItems()
	{
		$existingCodes = array_map(function($item){
			return $item['code'];
		}, $this->variantRepository->getAllCodes());

		$codesInWaitingList = array_map(function($item){
			return $item['code'];
		}, $this->waitingProductFacade->getCodesInWaitingList($this->supplier));

		return array_filter($this->loadedData, function(ProductDataObject $item, $key) use ($existingCodes, $codesInWaitingList) {
			return !in_array($item->getCode(), $existingCodes) && !in_array($item->getCode(), $codesInWaitingList);
		}, ARRAY_FILTER_USE_BOTH);
	}


	/**
	 * @param string $code
	 * @return string
	 */
	protected function getCodeWithPrefix(string $code): string
	{
		return ($this->getSupplier()->getPrefix() && strlen($this->getSupplier()->getPrefix()) > 0)
			? $this->getSupplier()->getPrefix() . '-' . $code
			: $code;
	}


}