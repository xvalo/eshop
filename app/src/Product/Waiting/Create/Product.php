<?php

namespace App\Product\Waiting\Create;

use App\Stock\Supplier\Entity\Supplier;
use Nette\SmartObject;

class Product
{
	use SmartObject;

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var int
	 */
	private $producer;

	/**
	 * @var string
	 */
	private $annotation;

	/**
	 * @var string
	 */
	private $description;

	/**
	 * @var int
	 */
	private $category;

	/**
	 * @var int
	 */
	private $vat;

	/**
	 * @var Supplier
	 */
	private $supplier;

	/**
	 * @var array
	 */
	private $images;

	public function __construct(string $name, string $annotation, string $description, int $category, int $vat, Supplier $supplier, array $images = [], int $producer = null)
	{
		$this->name = $name;
		$this->producer = $producer;
		$this->annotation = $annotation;
		$this->description = $description;
		$this->category = $category;
		$this->vat = $vat;
		$this->supplier = $supplier;
		$this->images = $images;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return int
	 */
	public function getProducer(): int
	{
		return $this->producer;
	}

	/**
	 * @return string
	 */
	public function getAnnotation(): string
	{
		return $this->annotation;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return $this->description;
	}

	/**
	 * @return int
	 */
	public function getCategory(): int
	{
		return $this->category;
	}

	/**
	 * @return int
	 */
	public function getVat(): int
	{
		return $this->vat;
	}

	/**
	 * @return Supplier
	 */
	public function getSupplier(): Supplier
	{
		return $this->supplier;
	}

	/**
	 * @return array
	 */
	public function getImages(): array
	{
		return $this->images;
	}
}