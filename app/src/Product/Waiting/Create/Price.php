<?php

namespace App\Product\Waiting\Create;

use Nette\SmartObject;

class Price
{
	use SmartObject;

	/**
	 * @var string|null
	 */
	private $name;

	/**
	 * @var float
	 */
	private $price;

	/**
	 * @var float|null
	 */
	private $purchasePrice;

	public function __construct(float $price, string $name = null, float $purchasePrice = null)
	{
		$this->name = $name;
		$this->price = $price;
		$this->purchasePrice = $purchasePrice;
	}

	/**
	 * @return null|string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return float
	 */
	public function getPrice(): float
	{
		return $this->price;
	}

	/**
	 * @return float|null
	 */
	public function getPurchasePrice()
	{
		return $this->purchasePrice;
	}


}