<?php

namespace App\Product\Waiting\Create;

use Nette\SmartObject;

class Variant
{
	use SmartObject;

	/**
	 * @var string|null
	 */
	private $name;

	/**
	 * @var string|null
	 */
	private $ean;

	/**
	 * @var float|null
	 */
	private $normalPrice;

	public function __construct(string $name = null, string $ean = null, float $normalPrice = null)
	{
		$this->name = $name;
		$this->ean = $ean;
		$this->normalPrice = $normalPrice;
	}

	/**
	 * @return null|string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return null|string
	 */
	public function getEan()
	{
		return $this->ean;
	}

	/**
	 * @return float|null
	 */
	public function getNormalPrice()
	{
		return $this->normalPrice;
	}
}