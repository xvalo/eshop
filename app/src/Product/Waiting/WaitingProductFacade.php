<?php

namespace App\Product\Waiting;

use App\Stock\Supplier\Entity\Supplier;
use App\Product\Waiting\Entity\Product;
use Kdyby\Doctrine\EntityManager;

class WaitingProductFacade
{
	/** @var \Kdyby\Doctrine\EntityRepository  */
	private $waitingProductRepository;

	/** @var EntityManager */
	private $em;

	public function __construct(EntityManager $entityManager)
	{
		$this->em = $entityManager;
		$this->waitingProductRepository = $entityManager->getRepository(Product::class);
	}

	/**
	 * @param int $id
	 * @return null|Product
	 */
	public function getProduct(int $id)
	{
		return $this->waitingProductRepository->find($id);
	}

	public function getUnprocessedWaitingProductsQB()
	{
		$qb = $this->waitingProductRepository->createQueryBuilder('W');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('W.processed', ':processed'),
				$qb->expr()->eq('W.deleted', ':deleted')
			)
		);

		$qb->setParameters([
			'deleted' => false,
			'processed' => false
		]);

		return $qb;
	}

	public function getCodesInWaitingList(Supplier $supplier)
	{
		$qb = $this->waitingProductRepository->createQueryBuilder()
				->select('W.code')
				->from(Product::class, 'W');

		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('W.supplier', ':supplier')
			)
		);

		$qb->setParameters([
			'supplier' => $supplier
		]);

		return $qb->getQuery()->getArrayResult();
	}

	/**
	 * @param Product $product
	 * @throws \Exception
	 */
	public function saveWaitingProduct(Product $product)
	{
		if (!$product->getId()) {
			$this->em->persist($product);
		}

		$this->em->flush();
	}
}