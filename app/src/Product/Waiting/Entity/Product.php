<?php

namespace App\Product\Waiting\Entity;

use App\Stock\Supplier\Entity\Supplier;
use App\Model\BaseEntity;
use App\Product\Waiting\Checker\ProductDataObject;
use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="waiting_product"
 * )
 */
class Product extends BaseEntity
{
	use RemovableTrait;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Stock\Supplier\Entity\Supplier")
	 * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id")
	 * @var Supplier
	 */
	private $supplier;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
	private $data;

	/**
	 * @ORM\Column(type="string", length=40)
	 * @var string
	 */
	private $code;

	/**
	 * @ORM\Column(type="boolean")
	 * @var bool
	 */
	private $processed = false;

	public function __construct(Supplier $supplier, ProductDataObject $dataObject)
	{
		$this->supplier = $supplier;
		$this->data = serialize($dataObject);
		$this->code = $dataObject->getCode();
	}

	/**
	 * @return Supplier
	 */
	public function getSupplier(): Supplier
	{
		return $this->supplier;
	}

	/**
	 * @return ProductDataObject
	 */
	public function getData(): ProductDataObject
	{
		return unserialize($this->data);
	}

	/**
	 * @return string
	 */
	public function getCode(): string
	{
		return $this->code;
	}

	/**
	 * @return bool
	 */
	public function isProcessed(): bool
	{
		return $this->processed;
	}

	public function setProcessed()
	{
		$this->processed = true;
	}

}