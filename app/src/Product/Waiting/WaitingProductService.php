<?php

namespace App\Product\Waiting;

use App\Classes\Product\Variant\ProductVariantPriceFactory;
use App\Classes\Product\Variant\ProductVariantService;
use App\Product\Availability\AvailabilityProvider;
use App\Product\Image\ImageManager;
use App\Product\ProductService;
use App\Product\Waiting\Create\Price;
use App\Product\Waiting\Create\Product;
use App\Product\Waiting\Create\Variant;
use App\Stock\Supplier\SupplierService;
use App\Utils\Numbers;
use Nette\SmartObject;
use App\Product\Waiting\Entity\Product as WaitingProduct;

class WaitingProductService
{
	use SmartObject;

	/**
	 * @var WaitingProductFacade
	 */
	private $waitingProductFacade;

	/**
	 * @var ProductVariantService
	 */
	private $productVariantService;

	/**
	 * @var AvailabilityProvider
	 */
	private $availabilityProvider;

	/**
	 * @var SupplierService
	 */
	private $supplierService;

	/**
	 * @var ProductService
	 */
	private $productService;

	/**
	 * @var ImageManager
	 */
	private $imageManager;

	public function __construct(
		WaitingProductFacade $waitingProductFacade,
		ProductVariantService $productVariantService,
		AvailabilityProvider $availabilityProvider,
		SupplierService $supplierService,
		ProductService $productService,
		ImageManager $imageManager
	){
		$this->waitingProductFacade = $waitingProductFacade;
		$this->productVariantService = $productVariantService;
		$this->availabilityProvider = $availabilityProvider;
		$this->supplierService = $supplierService;
		$this->productService = $productService;
		$this->imageManager = $imageManager;
	}

	/**
	 * @param int $id
	 * @return Entity\Product|null
	 */
	public function getWaitingProduct(int $id)
	{
		return $this->waitingProductFacade->getProduct($id);
	}

	public function createNewProduct(Product $productData, Variant $variantData, Price $priceData, string $code)
	{
		$product = $this->productService->createProduct($productData);
		$variant = $this->productVariantService->createVariant(
			$product,
			$variantData->getName(),
			$this->availabilityProvider->getOutOfStockStatus(),
			$variantData->getNormalPrice(),
			$variantData->getEan()
		);

		$prefix = $productData->getSupplier() && $productData->getSupplier()->getPrefix() && strlen($productData->getSupplier()->getPrefix()) > 0 ? $productData->getSupplier()->getPrefix() . '-' : '';
		$code = $this->productVariantService->getCodeByString($prefix.$code);

		$this->productVariantService->addCodeToVariant(
			$variant,
			$code,
			$productData->getSupplier(),
			true,
			true
		);

		$this->productVariantService->addPriceToVariant(
			$variant,
			ProductVariantPriceFactory::createRegularPrice(
				$variant,
				Numbers::floatFromCzechNumber($priceData->getPrice()),
				$priceData->getName(),
				true,
				Numbers::floatFromCzechNumber($priceData->getPurchasePrice()))
		);

		foreach ($productData->getImages() as $imageUrl) {
			$this->imageManager->attachImageFromSource($product, $imageUrl);
		}

		return $product;
	}

	/**
	 * @param WaitingProduct $product
	 * @throws \Exception
	 */
	public function setProductProcessed(WaitingProduct $product)
	{
		$product->setProcessed();
		$this->waitingProductFacade->saveWaitingProduct($product);
	}

	/**
	 * @param int $id
	 * @throws \Exception
	 */
	public function removeWaitingProduct(int $id)
	{
		$product = $this->waitingProductFacade->getProduct($id);
		$product->delete();
		$this->waitingProductFacade->saveWaitingProduct($product);
	}
}