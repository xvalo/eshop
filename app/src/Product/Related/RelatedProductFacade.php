<?php

namespace App\Product\Related;

use App\Product\Product;
use App\Product\Related\Entity\RelatedProduct;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class RelatedProductFacade
{
	use SmartObject;

	/** @var EntityManager */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;

	}

	public function getRelatedForProduct(Product $product)
	{
		return $this->em->getRepository(RelatedProduct::class)->findBy(['product' => $product]);
	}

	public function getRelation(Product $product, Product $relatedTo)
	{
		return $this->em->getRepository(RelatedProduct::class)->findOneBy(['product' => $relatedTo, 'relatedProduct' => $product]);
	}

	public function saveRelatedProduct(RelatedProduct $product)
	{
		if ($product->getId() === null) {
			$this->em->persist($product);
		}

		$this->em->flush();
		return $product;
	}

	public function removeRelation(int $relationId)
	{
		$relation = $this->em->getRepository(RelatedProduct::class)->find($relationId);
		$this->em->remove($relation);
		$this->em->flush();
	}
}