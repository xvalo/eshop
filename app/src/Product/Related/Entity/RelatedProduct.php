<?php

namespace App\Product\Related\Entity;

use App\Model\BaseEntity;
use App\Product\Product;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class RelatedProduct extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="App\Product\Product")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
	 * @var Product
	 */
	private $product;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Product\Product")
	 * @ORM\JoinColumn(name="related_product_id", referencedColumnName="id")
	 * @var Product
	 */
	private $relatedProduct;

	public function __construct(Product $product, Product $relatedTo)
	{
		$this->product = $relatedTo;
		$this->relatedProduct = $product;
	}

	/**
	 * @return Product
	 */
	public function getProduct(): Product
	{
		return $this->product;
	}

	/**
	 * @return Product
	 */
	public function getRelatedProduct(): Product
	{
		return $this->relatedProduct;
	}
}