<?php

namespace App\Product\Related;

use App\Product\Product;
use App\Product\Related\Entity\RelatedProduct;
use Nette\Application\LinkGenerator;
use Nette\SmartObject;
use Ublaboo\ImageStorage\ImageStorage;

class RelatedProductService
{
	use SmartObject;

	/** @var RelatedProductFacade */
	private $relatedProductFacade;

	/** @var LinkGenerator */
	private $linkGenerator;

	/** @var ImageStorage */
	private $imageStorage;

	public function __construct(
		RelatedProductFacade $relatedProductFacade,
		LinkGenerator $linkGenerator,
		ImageStorage $imageStorage
	){
		$this->relatedProductFacade = $relatedProductFacade;
		$this->linkGenerator = $linkGenerator;
		$this->imageStorage = $imageStorage;
	}

	public function createProductRelation(Product $product, Product $relatedTo)
	{
		if (!$this->relatedProductFacade->getRelation($product, $relatedTo)) {
			return $this->relatedProductFacade->saveRelatedProduct(new RelatedProduct($product, $relatedTo));
		}

		return false;
	}

	public function getProductRelations(Product $product)
	{
		return $this->relatedProductFacade->getRelatedForProduct($product);
	}

	public function getProductRelatedProducts(Product $product)
	{
		$products = [];
		/** @var RelatedProduct $relation */
		foreach($this->getProductRelations($product) as $relation) {
			$product = $relation->getRelatedProduct();
			if (!$product->isActive()) {
				continue;
			}

			$products[] = [
				'name' => $product->getName(),
				'url' => $this->linkGenerator->link('Front:Product:default', [$product->getCategory()->getUrl(), $product->getUrl()]),
				'image' => '/'.$this->imageStorage->fromIdentifier([$product->getMainImage()->getPath(), '240x240', 'fit', 70])->createLink(),
				'availability' => $product->getGeneralAvailability(),
				'wasPrice' => $product->getNormalPrice(),
				'currentPrice' => $product->getMinPrice(),
				'annotation' => $product->getAnnotation(),
				'hasMorePrices' => count($product->getPriceVariants()) > 0,
				'icons' => []
			];
		}

		return $products;
	}

	public function removeRelation(int $id)
	{
		$this->relatedProductFacade->removeRelation($id);
	}
}