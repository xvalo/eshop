<?php

namespace App\Classes\Product\Reserved;

use App\Order\OrderLine\ProductOrderLine;
use App\Product\Variant;
use App\ReservedProduct\ReservedProduct;
use App\ReservedProduct\ReservedProductRepository;
use App\Stock\StockItem;

class ReservedProductService
{
	/**
	 * @var ReservedProductRepository
	 */
	private $reservedProductRepository;

	public function __construct(ReservedProductRepository $reservedProductRepository)
	{
		$this->reservedProductRepository = $reservedProductRepository;
	}

	public function createReservation(Variant $product, ProductOrderLine $orderLine, StockItem $stockItem, int $count)
	{
		$reservation = new ReservedProduct($product, $orderLine, $stockItem, $count);
		$this->reservedProductRepository->persist($reservation);
		$this->reservedProductRepository->flush();
	}

	public function deactivateReservation(ProductOrderLine $orderLine)
	{
		$deactivatedReservations = [];
		/** @var ReservedProduct $reservation */
		foreach ($this->reservedProductRepository->findBy(['orderLine' => $orderLine, 'active' => 1]) as $reservation) {
			$reservation->deactivate();
			$deactivatedReservations[] = $reservation;
		}

		return $deactivatedReservations;
	}

	public function getReservationsByProduct()
	{
		$reservations = $this->reservedProductRepository->getReservedProducts();
		$reservationsList = [];

		/** @var ReservedProduct $reservation */
		foreach ($reservations as $reservation) {
			/** @var Variant $product */
			$product = $reservation->getProduct();
			if (!array_key_exists($product->getId(), $reservationsList)) {
				$reservationsList[$product->getId()] = [
					'id' => $product->getId(),
					'code' => $product->getCodesString(),
					'name' => $product->getVariantName(),
					'count' => $reservation->getCount(),
					'ordersList' => []
				];
			} else {
				$reservationsList[$product->getId()]['count'] += $reservation->getCount();
			}

			if (!array_key_exists($reservation->getOrderLine()->getOrder()->getId(), $reservationsList[$product->getId()]['ordersList'])) {
				$reservationsList[$product->getId()]['ordersList'][$reservation->getOrderLine()->getOrder()->getId()] = [
					'id' => $reservation->getOrderLine()->getOrder()->getId(),
					'orderNo' => $reservation->getOrderLine()->getOrder()->getOrderNo(),
					'created' => $reservation->getCreated(),
					'count' => 0,
					'stocks' => []
				];
			}

			$reservationsList[$product->getId()]['ordersList'][$reservation->getOrderLine()->getOrder()->getId()]['count'] += $reservation->getCount();
			$reservationsList[$product->getId()]['ordersList'][$reservation->getOrderLine()->getOrder()->getId()]['stocks'][$reservation->getStockItem()->getStock()->getId()] = $reservation->getStockItem()->getStock()->getName();
		}

		return $reservationsList;
	}

}