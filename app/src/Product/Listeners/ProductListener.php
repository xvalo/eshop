<?php

namespace App\Product\Listeners;

use App\Lists\ProducerRepository;
use App\Product\Availability\AvailabilityProvider;
use App\Product\OldUrl;
use App\Product\Price\PercentagePrice;
use App\Product\Price\Price;
use App\Product\Price\SubtractedPrice;
use App\Product\Variant;
use App\Product\Product;
use App\Utils\Numbers;
use App\Utils\Vat;
use Contributte\Cache\CacheFactory;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\UnitOfWork;
use Nette\Caching\Cache;
use Nette\SmartObject;
use Nette\Utils\Strings;
use Doctrine\ORM\Events;
use Kdyby\Events\Subscriber;

class ProductListener implements Subscriber
{
	use SmartObject;

	/** @var AvailabilityProvider */
	private $availabilityProvider;

	/** @var Cache  */
	private $producersFrontListCache;

	public function __construct(
		AvailabilityProvider $availabilityProvider,
		CacheFactory $cacheFactory
	){
		$this->availabilityProvider = $availabilityProvider;
		$this->producersFrontListCache = $cacheFactory->create(ProducerRepository::FRONT_FILTER_LIST);
	}

    public function getSubscribedEvents()
    {
        return [
            Events::preUpdate,
            Events::onFlush
        ];
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        if ($eventArgs->getEntity() instanceof Product) {
            /** @var Product $product */
            $product = $eventArgs->getEntity();
            if ($eventArgs->hasChangedField('name')) {
                $oldUrl = $product->getUrl();
                $oldUrlObject = new OldUrl($product, $oldUrl);
                $product->setUrl(Strings::webalize($eventArgs->getNewValue('name')));
                $product->addOldUrl($oldUrlObject);
            }
        }
    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof Product) {
                $entity = $this->updateProductState($entity, $em, $uow);
                $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($entity)), $entity);
	            $this->producersFrontListCache->clean([Cache::TAGS => ProducerRepository::CACHE_FRONT_LIST_TAG]);
            }

            if ($entity instanceof Variant) {
                $entity = $this->updateProductState($entity->getProduct(), $em, $uow);
                $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($entity)), $entity);
            }

            if ($entity instanceof Price) {
	            $product = $this->updateProductState($entity->getVariant()->getProduct(), $em, $uow);
	            $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($product)), $product);

	            $price = $this->recountPrice($entity);
	            $price = $this->recountPriceWithoutVat($price);
	            $price = $this->recountPurchasePrice($price);
	            $price = $this->countMargin($price);
	            $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($price)), $price);
            }
        }

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof Product) {
                $product = $this->updateProductState($entity, $em, $uow);

                $changes = $uow->getEntityChangeSet($entity);

                if (array_key_exists('name', $changes)) {
                    $oldName = $changes['name'][0];

                    $oldUrlObject = new OldUrl($product, Strings::webalize($oldName));
                    $em->persist($oldUrlObject);
                    $uow->computeChangeSet($em->getClassMetadata(get_class($oldUrlObject)), $oldUrlObject);

                    $product->setUrl(Strings::webalize($product->getName()));
                    $product->addOldUrl($oldUrlObject);
                }
                $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($product)), $product);
	            $this->producersFrontListCache->clean([Cache::TAGS => ProducerRepository::CACHE_FRONT_LIST_TAG]);
            }

            if ($entity instanceof Variant) {
                $product = $this->updateProductState($entity->getProduct(), $em, $uow);
                $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($product)), $product);
            }

            if ($entity instanceof Price) {
	            $price = $this->recountPrice($entity);
	            $price = $this->recountPriceWithoutVat($price);
	            $price = $this->recountPurchasePrice($price);
	            $price = $this->countMargin($price);
	            $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($price)), $price);

	            $product = $this->updateProductState($entity->getVariant()->getProduct(), $em, $uow);
	            $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($product)), $product);
            }
        }
    }

    private function updateProductState(Product $product, EntityManager $em, UnitOfWork $uow)
    {
        $product->setUrl(Strings::webalize($product->getName()));

        $availability = $this->availabilityProvider->getOutOfStockStatus();
        $minPrice = PHP_INT_MAX;
        $normalPrice = 0;

        $variants = $em->getRepository(Variant::class)->findBy(['product' => $product, 'deleted' => false]);

        /** @var Variant $variant */
        foreach ($variants as $variant) {
            if (AvailabilityProvider::hasHigherPriority($availability, $variant->getAvailability())) {
                $availability = $variant->getAvailability();
            }
            foreach ($variant->getPrices() as $price) {
            	$price = $this->recountPrice($price);
	            $uow->recomputeSingleEntityChangeSet($em->getClassMetadata(get_class($price)), $price);
	            if($price->getPrice() < $minPrice) {
		            $minPrice = $price->getPrice();
	            }
            }
            if ($variant->getNormalPrice() && $variant->getNormalPrice() > $normalPrice) {
                $normalPrice = $variant->getNormalPrice();
            }
        }

        $product->setGeneralAvailability($availability);
        $product->setMinPrice($minPrice !== PHP_INT_MAX ? $minPrice : 0);
        $product->setNormalPrice($normalPrice);

        return $product;
    }

	private function recountPriceWithoutVat(Price $price)
	{
		$vat = Vat::countVat($price->getPrice(), $price->getVariant()->getProduct()->getVat());
		$priceWithoutVat = $price->getPrice() - $vat;
		$price->setPriceWithoutVat($priceWithoutVat);

		return $price;
    }

	private function recountPurchasePrice(Price $price)
	{
		$vat = round($price->getPurchasePriceWithoutVat() * ($price->getVariant()->getProduct()->getVat() / 100), 2);
		$purchasePrice = $price->getPurchasePriceWithoutVat() + $vat;
		$price->setPurchasePrice($purchasePrice);

		return $price;
    }

	private function countMargin(Price $price)
	{
		if ($price->getPriceWithoutVat() && $price->getPurchasePriceWithoutVat()) {
			$price->setMargin(Numbers::countPriceMargin($price->getPriceWithoutVat(), $price->getPurchasePriceWithoutVat()));
		}

		return $price;
	}

	private function recountPrice(Price $price)
	{
		if ($price instanceof SubtractedPrice) {
			$price->setPrice($price->getCountedFrom()->getPrice() - $price->getSubtracted());
		}

		if ($price instanceof PercentagePrice) {
			$price->setPrice($price->getCountedFrom()->getPrice() * $price->getPercentage());
		}

		return $price;
	}
}