<?php

namespace App\Product\Availability;

use App\Product\Availability\Entity\Availability;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class AvailabilityFacade
{
	use SmartObject;

	const POSITION_ON_STOCK = 1;
	const POSITION_OUT_OFF_STOCK = 2;

	/** @var EntityManager */
	private $em;

	/** @var \Kdyby\Doctrine\EntityRepository  */
	private $availabilityRepository;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$this->availabilityRepository = $em->getRepository(Availability::class);
	}

	/**
	 * @param Availability $availability
	 * @throws \Exception
	 */
	public function saveAvailability(Availability $availability)
	{
		if ($availability->getId() === null) {
			$this->em->persist($availability);
		}

		$this->em->flush();
	}

	public function getGridSource()
	{
		return $this->availabilityRepository->createQueryBuilder('A')
					->orderBy('A.priority', 'ASC');
	}

	public function saveNewAvailability(Availability $availability)
	{
		/** @var Availability $lowestPriorityAvailability */
		$lowestPriorityAvailability = $this->getAvailabilityByPosition(AvailabilityFacade::POSITION_OUT_OFF_STOCK);

		$priority = $lowestPriorityAvailability->getPriority();
		$availability->setPriority($priority);
		$lowestPriorityAvailability->setPriority($priority + 1);

		$this->em->persist($availability);

		$this->em->flush();
	}

	/**
	 * @return array
	 */
	public function getList()
	{
		return $this->getGridSource()->getQuery()->getResult();
	}

	/**
	 * @param int $id
	 * @return null|Availability
	 */
	public function getAvailability(int $id)
	{
		return $this->availabilityRepository->find($id);
	}

	/**
	 * @param int $position
	 * @return Availability|null
	 */
	public function getAvailabilityByPosition(int $position)
	{
		$priorityOrder = $position === self::POSITION_ON_STOCK ? 'ASC' : 'DESC';
		return $this->availabilityRepository->findOneBy([], ['priority' => $priorityOrder]);
	}

	public function getItemsToMoveUp(Availability $availability, Availability $previousItem = null)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('A')
			->from(Availability::class, 'A')
			->where(
				$qb->expr()->andX(
					$qb->expr()->lte('A.priority', ':lowerPriority'),
					$qb->expr()->gt('A.priority', ':higherPriority')
				)
			);

		$qb->setParameters([
			'lowerPriority' => $previousItem ? $previousItem->getPriority() : 0,
			'higherPriority' => $availability->getPriority()
		]);

		return $qb->getQuery()->getResult();
	}

	public function getItemsToMoveDown(Availability $availability, Availability $nextItem = null)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('A')
			->from(Availability::class, 'A')
			->where(
				$qb->expr()->andX(
					$qb->expr()->gte('A.priority', ':higherPriority'),
					$qb->expr()->lt('A.priority', ':lowerPriority')
				)
			);

		$qb->setParameters([
			'higherPriority' => $nextItem ? $nextItem->getPriority() : 0,
			'lowerPriority' => $availability->getPriority()
		]);

		return $qb->getQuery()->getResult();
	}

	public function getCurrentPriorityMaxValue()
	{
		return $this->em->createQueryBuilder()
					->select('MAX(A.priority)')
					->from(Availability::class, 'A')
					->getQuery()
					->getSingleScalarResult();
	}
}