<?php

namespace App\Product\Availability;

use App\Model\Services\ApplicationConfigurationService;
use Nette\SmartObject;

class ColorProvider
{
	use SmartObject;

	/** @var array */
	private $colors;

	public function __construct(
		ApplicationConfigurationService $applicationConfigurationService
	){
		$this->colors = $applicationConfigurationService->getAvailabilityColors();
	}

	/**
	 * @return array
	 */
	public function getColorsList(): array
	{
		return $this->colors;
	}

	/**
	 * @param string $key
	 * @return string
	 */
	public function getAvailabilityColor(string $key): string
	{
		return $this->colors[$key]['color'];
	}

	/**
	 * @param string $key
	 * @return string
	 */
	public function getAvailabilityClass(string $key): string
	{
		return $this->colors[$key]['class'];
	}

}