<?php

namespace App\Product\Availability;

use App\Product\Availability\Entity\Availability;
use App\Product\Product;
use App\Product\Variant;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;

class AvailabilityProvider
{
	const AVAILABILITY_CACHE = 'availability';

	/** @var AvailabilityFacade  */
	private $availabilityFacade;

	/** @var Cache  */
	private $availabilityCache;

	/** @var ColorProvider */
	private $colorProvider;

	/**
	 * @var Availability
	 */
	private $onStockStatus;

	public function __construct(
		AvailabilityFacade $availabilityFacade,
		IStorage $storage,
		ColorProvider $colorProvider
	){
		$this->availabilityFacade = $availabilityFacade;
		$this->availabilityCache = new Cache($storage, self::AVAILABILITY_CACHE);
		$this->colorProvider = $colorProvider;
	}

	/**
	 * @param int $id
	 * @return null|Availability
	 */
	public function getAvailability(int $id)
	{
		return $this->availabilityFacade->getAvailability($id);
	}

	/**
	 * @return array
	 */
	public function getAvailabilities()
	{
		return $this->availabilityFacade->getList();
	}

	/**
	 * @return array
	 * @throws \Throwable
	 */
	public function getList(): array
	{
		$list = $this->availabilityCache->load('list');

		if (!$list) {
			$list = [];

			/** @var Availability $availability */
			foreach ($this->availabilityFacade->getList() as $availability) {
				$list[$availability->getId()] = [
					'id' => $availability->getId(),
					'title' => $availability->getTitle(),
					'description' => $availability->getDescription(),
					'color' => $availability->getColor(),
					'isProductOrderable' => $availability->isProductOrderable(),
					'class' => $this->colorProvider->getAvailabilityClass($availability->getColor())
				];
			}

			$this->availabilityCache->save('list', $list, [
				Cache::TAGS => ['availability/list']
			]);
		}

		return $list;
	}

	public function getSelectList()
	{
		$selectList = [];

		foreach ($this->getList() as $key => $item) {
			$selectList[$key] = $item['title'];
		}

		return $selectList;
	}

	/**
	 * @param int $id
	 * @return mixed
	 * @throws \Throwable
	 */
	public function getAvailabilityDetail(int $id)
	{
		$availabilities = $this->getList();
		return $availabilities[$id];
	}

	/**
	 * @return Availability
	 */
	public function getOnStockStatus(): Availability
	{
		if ($this->onStockStatus === null) {
			$this->onStockStatus = $this->availabilityFacade->getAvailabilityByPosition(AvailabilityFacade::POSITION_ON_STOCK);
		}

		return $this->onStockStatus;
	}

	/**
	 * @return Availability
	 */
	public function getOutOfStockStatus(): Availability
	{
		return $this->availabilityFacade->getAvailabilityByPosition(AvailabilityFacade::POSITION_OUT_OFF_STOCK);
	}

	public function getMostAvailableVariant(Product $product)
	{
		$availability = $this->getOutOfStockStatus();
		$variant = null;
		$isFirst = true;

		/** @var Variant $priceVariant */
		foreach ($product->getPriceVariants() as $priceVariant) {
			if ($isFirst) {
				$variant = $priceVariant;
				$availability = $priceVariant->getAvailability();
				$isFirst = false;

				continue;
			}

			if (self::hasHigherPriority($availability, $priceVariant->getAvailability())) {
				$variant = $priceVariant;
				$availability = $priceVariant->getAvailability();
			}
		}

		return $variant;
	}

	public static function hasHigherPriority(Availability $current, Availability $new)
	{
		return $new->getPriority() < $current->getPriority();
	}
}