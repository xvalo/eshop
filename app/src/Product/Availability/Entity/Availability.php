<?php

namespace App\Product\Availability\Entity;

use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="product_availability"
 * )
 */
class Availability
{
	use Identifier;

	/**
	 * @ORM\Column(type="string", length=30)
	 * @var string
	 */
	private $title;

	/**
	 * @ORM\Column(type="integer", length=10)
	 * @var int
	 */
	private $priority;

	/**
	 * @ORM\Column(type="string", length=160, nullable=true)
	 * @var string|null
	 */
	private $description;

	/**
	 * @ORM\Column(type="string", length=40, nullable=true)
	 * @var string|null
	 */
	private $color;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $productOrderable;

	/**
	 * @ORM\Column(type="integer", length=10, nullable=true)
	 * @var int|null
	 */
	private $heurekaAvailability;

	/**
	 * @ORM\Column(type="integer", length=10, nullable=true)
	 * @var int|null
	 */
	private $zboziAvailability;

	public function __construct(
		string $title,
		int $priority,
		bool $productOrderable,
		string $description = null,
		string $color = null,
		int $heurekaAvailability = null,
		int $zboziAvailability = null
	){
		$this->title = $title;
		$this->priority = $priority;
		$this->productOrderable = $productOrderable;
		$this->description = $description;
		$this->color = $color;
		$this->heurekaAvailability = $heurekaAvailability;
		$this->zboziAvailability = $zboziAvailability;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title)
	{
		$this->title = $title;
	}

	/**
	 * @return int
	 */
	public function getPriority(): int
	{
		return $this->priority;
	}

	/**
	 * @param int $priority
	 */
	public function setPriority(int $priority)
	{
		$this->priority = $priority;
	}

	/**
	 * @return null|string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param null|string $description
	 */
	public function setDescription(string $description = null)
	{
		$this->description = $description;
	}

	/**
	 * @return null|string
	 */
	public function getColor()
	{
		return $this->color;
	}

	/**
	 * @param null|string $color
	 */
	public function setColor(string $color = null)
	{
		$this->color = $color;
	}

	/**
	 * @return bool
	 */
	public function isProductOrderable(): bool
	{
		return $this->productOrderable;
	}

	/**
	 * @param bool $productOrderable
	 */
	public function setProductOrderable(bool $productOrderable)
	{
		$this->productOrderable = $productOrderable;
	}

	/**
	 * @return int|null
	 */
	public function getHeurekaAvailability()
	{
		return $this->heurekaAvailability;
	}

	/**
	 * @param int|null $heurekaAvailability
	 */
	public function setHeurekaAvailability(int $heurekaAvailability = null)
	{
		$this->heurekaAvailability = $heurekaAvailability;
	}

	/**
	 * @return int|null
	 */
	public function getZboziAvailability()
	{
		return $this->zboziAvailability;
	}

	/**
	 * @param int|null $zboziAvailability
	 */
	public function setZboziAvailability(int $zboziAvailability = null)
	{
		$this->zboziAvailability = $zboziAvailability;
	}

	/**
	 * @return null|int
	 */
	public function getGoogleMerchantAvailability()
	{
		return $this->getHeurekaAvailability() ?: $this->getZboziAvailability() ?: null;
	}
}