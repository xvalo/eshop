<?php

namespace App\Product\Availability;

use App\Product\Availability\Entity\Availability;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;

class AvailabilityService
{
	/** @var AvailabilityFacade */
	private $availabilityFacade;

	private $availabilityCache;

	public function __construct(
		AvailabilityFacade $availabilityFacade,
		IStorage $storage
	){
		$this->availabilityFacade = $availabilityFacade;
		$this->availabilityCache = new Cache($storage, AvailabilityProvider::AVAILABILITY_CACHE);
	}

	/**
	 * @param int $id
	 * @return null|Availability
	 */
	public function getAvailability(int $id)
	{
		return $this->availabilityFacade->getAvailability($id);
	}

	public function createAvailability(
		string $title,
		bool $productOrderable,
		string $description = null,
		string $color = null,
		int $heurekaAvailability = null,
		int $zboziAvailability = null
	){
		$availability = new Availability(
			$title,
			$this->getNewAvailabilityPriorityValue(),
			$productOrderable,
			$description,
			$color,
			$heurekaAvailability,
			$zboziAvailability
		);

		$this->availabilityFacade->saveNewAvailability($availability);
		$this->cleanCachedData();

		return $availability;
	}

	public function updateAvailability(
		Availability $availability,
		string $title,
		bool $productOrderable,
		string $description = null,
		string $color = null,
		int $heurekaAvailability = null,
		int $zboziAvailability = null
	){
		$availability->setTitle($title);
		$availability->setProductOrderable($productOrderable);
		$availability->setDescription($description);
		$availability->setColor($color);
		$availability->setHeurekaAvailability($heurekaAvailability);
		$availability->setZboziAvailability($zboziAvailability);

		$this->availabilityFacade->saveAvailability($availability);
		$this->cleanCachedData();

		return $availability;
	}

	/**
	 * @param Availability $availability
	 * @param Availability|null $previousAvailability
	 * @param Availability|null $nextAvailability
	 * @throws AvailabilityNotAvailableToMoveException
	 */
	public function changeAvailabilityPriority(Availability $availability, Availability $previousAvailability = null, Availability $nextAvailability = null)
	{
		/**
		 * Find all items that have to be moved one position up
		 */
		$itemsToMoveUp = $this->availabilityFacade->getItemsToMoveUp($availability, $previousAvailability);

		/** @var Availability $item */
		foreach ($itemsToMoveUp as $item) {
			$this->checkIsAvailabilityAbleToMove($item);
			$item->setPriority($item->getPriority() - 1);
		}

		/**
		 * Find all items that have to be moved one position down
		 */
		$itemsToMoveDown = $this->availabilityFacade->getItemsToMoveDown($availability, $nextAvailability);

		/** @var Availability $item */
		foreach ($itemsToMoveDown as $item) {
			$this->checkIsAvailabilityAbleToMove($item);
			$item->setPriority($item->getPriority() + 1);
		}

		/**
		 * Update current item order
		 */
		if ($previousAvailability) {
			$availability->setPriority($previousAvailability->getPriority() + 1);
		} else if ($nextAvailability) {
			$availability->setPriority($nextAvailability->getPriority() - 1);
		} else {
			$item->setPriority(1);
		}

		$this->availabilityFacade->saveAvailability($availability);
		$this->cleanCachedData();
	}

	/**
	 * @param Availability $availability
	 * @throws AvailabilityNotAvailableToMoveException
	 */
	public function checkIsAvailabilityAbleToMove(Availability $availability)
	{
		$highestPriorityAvailability = $this->availabilityFacade->getAvailabilityByPosition(AvailabilityFacade::POSITION_ON_STOCK);
		$lowestPriorityAvailability = $this->availabilityFacade->getAvailabilityByPosition(AvailabilityFacade::POSITION_OUT_OFF_STOCK);

		if ($availability->getId() === $lowestPriorityAvailability->getId() || $availability->getId() === $highestPriorityAvailability->getId()) {
			throw new AvailabilityNotAvailableToMoveException('This availability is not available to be moved');
		}
	}

	private function cleanCachedData()
	{
		$this->availabilityCache->clean([
			Cache::TAGS => ['availability/list']
		]);
	}

	/**
	 * @return int
	 */
	protected function getNewAvailabilityPriorityValue(): int
	{
		$currentPriority = $this->availabilityFacade->getCurrentPriorityMaxValue();
		return $currentPriority !== null && $currentPriority !== false ? (int)$currentPriority + 1 : 1;
	}
}