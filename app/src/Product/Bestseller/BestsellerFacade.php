<?php

namespace App\Product\Bestseller;

use App\Product\Product;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class BestsellerFacade
{
	use SmartObject;

	/**
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	public function getBestsellerProducts()
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('P.id, P.url, P.name, P.minPrice, C.url AS categoryUrl, I.path AS imagePath')
			->from(Product::class, 'P')
			->join('P.category', 'C')
			->join('P.mainImage', 'I')
			->where(
				$qb->expr()->eq('P.bestSeller', ':bestseller')
			);

		$qb->setParameters([
			'bestseller' => true
		]);

		return $qb->getQuery()->getArrayResult();
	}
}