<?php

namespace App\Product\Bestseller;

use Contributte\Cache\CacheFactory;
use Nette\Caching\Cache;
use Nette\SmartObject;

class BestsellerProvider
{
	use SmartObject;

	const BESTSELLER_PRODUCTS_CACHE_NAME = 'bestseller-products';

	/**
	 * @var BestsellerFacade
	 */
	private $bestsellerFacade;

	/**
	 * @var \Nette\Caching\Cache
	 */
	private $cache;

	public function __construct(BestsellerFacade $bestsellerFacade, CacheFactory $cacheFactory)
	{
		$this->bestsellerFacade = $bestsellerFacade;
		$this->cache = $cacheFactory->create(self::BESTSELLER_PRODUCTS_CACHE_NAME);
	}

	public function getRandomBestsellerProduct()
	{
		$products = $this->cache->load('products');

		if ($products === null) {
			$products = $this->bestsellerFacade->getBestsellerProducts();
			$this->cache->save('products', $products, [
				Cache::EXPIRE => '2 hours'
			]);
		}

		if(count($products) === 0){
			return null;
		}

		$productToShow = $products[mt_rand(0, count($products)-1)];

		return $productToShow;
	}
}