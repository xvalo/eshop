<?php

namespace App\Product\Feed\Heureka;

use App\Category\Entity\Category;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\Product\Product;
use App\Product\ProductRepository;
use App\Product\Variant;
use Nette\Application\LinkGenerator;
use Nette\SmartObject;
use Nette\Utils\Strings;
use XMLWriter;

class HeurekaService
{
	use SmartObject;

    /** @var ProductRepository */
    private $repository;

    /** @var XMLWriter */
    private $writer;

	/** @var ApplicationSettingsManager */
	private $applicationSettingsManager;

	/** @var string */
	private $domain;

	/** @var LinkGenerator */
	private $linkGenerator;

	public function __construct(
    	ProductRepository $repository,
	    ApplicationSettingsManager $applicationSettingsManager,
		LinkGenerator $linkGenerator
    ){
        $this->repository = $repository;
		$this->applicationSettingsManager = $applicationSettingsManager;
		$this->linkGenerator = $linkGenerator;
	}

    public function initWriter()
    {
        $this->writer = new XMLWriter();
        $this->getWriter()->openURI('heureka.xml');
        $this->getWriter()->startDocument('1.0', 'UTF-8');
        $this->getWriter()->setIndent(true);
        $this->getWriter()->startElement('SHOP');

	    $domain = $this->applicationSettingsManager->getDomain();
	    $this->domain = Strings::startsWith($domain, 'https://') ? $domain : 'https://'.$domain;
    }

    public function finalize()
    {
        $this->getWriter()->endElement();
        $this->getWriter()->endDocument();
    }

    public function generate()
    {
        /** @var Product $product */
        foreach ($this->repository->findBy(['deleted ' => 0, 'active' => 1]) as $product) {
	        /** @var Variant $variant */
	        foreach($product->getPriceVariants() as $variant) {
		        if ($variant->getPrices()->count() === 0) {
			        continue;
		        }

		        $brand = $product->getProducer() ? $product->getProducer()->getName() . ' ' : '';
		        $productName = htmlspecialchars($brand . $variant->getVariantName());
		        $category = $product->getCategory();

		        $this->getWriter()->startElement('SHOPITEM');

		        $url = $product->hasMorePrices()
			        ? $this->linkGenerator->link('Front:Product:default', ['category' => $category->getUrl(), 'url' => $product->getUrl(), 'variant' => Strings::webalize($variant->getName()) ,'variantId' => $variant->getId()])
			        : $this->linkGenerator->link('Front:Product:default', ['category' => $category->getUrl(), 'url' => $product->getUrl()]);

		        $this->getWriter()->writeElement('ITEM_ID', 'PN'.$product->getNumber().'V'.$variant->getId());
		        $this->getWriter()->writeElement('PRODUCTNAME', $productName);
		        $this->getWriter()->writeElement('PRODUCT', $productName);
		        $this->getWriter()->writeElement('URL', $url);
		        $this->getWriter()->writeElement('DESCRIPTION', htmlspecialchars(strip_tags(html_entity_decode($product->getDescription()))));
		        if ($product->getMainImage()) {
			        $this->getWriter()->writeElement('IMGURL',
				        $this->domain . $product->getMainImage()->getRelativePath());
		        }
		        $this->getWriter()->writeElement('PRICE_VAT', ($variant->getPrices()->first())->getPrice());
		        $this->getWriter()->writeElement('VAT', $product->getVat() . '%');
		        if ($product->getProducer()) {
			        $this->getWriter()->writeElement('MANUFACTURER', $product->getProducer()->getName());
		        }
		        $this->getWriter()->writeElement('ITEM_TYPE', 'new');
		        $this->getWriter()->writeElement('CATEGORYTEXT', $category->getHeurekaCategory() ? $category->getHeurekaCategory()->getFullName() : $this->getCategoryText($category));
		        $this->getWriter()->writeElement('BRAND', trim($brand));
		        if ($product->getGeneralAvailability() !== null) {
			        $this->getWriter()->writeElement('DELIVERY_DATE', $product->getGeneralAvailability()->getHeurekaAvailability());
		        }
		        $this->getWriter()->writeElement('ITEMGROUP_ID', $product->getId().$product->getNumber());

		        $this->getWriter()->endElement();
	        }
        }
    }

    /**
     * @return XMLWriter
     */
    private function getWriter(): XMLWriter
    {
        return $this->writer;
    }

	/**
	 * @param Category $category
	 * @return string
	 */
    private function getCategoryText(Category $category): string
    {
        $text = $category->getName();

        if ($category->getParent()) {
            $text = $this->getCategoryText($category->getParent()) . ' | ' . $text;
        }

        return $text;
    }
}