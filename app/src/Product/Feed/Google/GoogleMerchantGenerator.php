<?php

namespace App\Product\Feed\Google;

use App\Category\Entity\Category;
use App\Model\Services\ApplicationConfigurationService;
use App\Product\Availability\AvailabilityProvider;
use App\Product\Availability\Entity\Availability;
use Nette\Application\UI\ITemplateFactory;
use Nette\SmartObject;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\Product\Product;
use App\Product\ProductRepository;
use App\Product\Variant;
use Nette\Application\LinkGenerator;
use Nette\Utils\FileSystem;
use Nette\Utils\Strings;

class GoogleMerchantGenerator
{
	use SmartObject;

	/** @var ProductRepository */
	private $repository;

	/** @var ApplicationSettingsManager */
	private $applicationSettingsManager;

	/** @var LinkGenerator */
	private $linkGenerator;

	/** @var ITemplateFactory */
	private $templateFactory;

	/** @var ApplicationConfigurationService */
	private $applicationConfigurationService;

	/** @var string */
	private $domain;

	/** @var Availability */
	private $onStockStatus;

	public function __construct(
		ProductRepository $repository,
		ApplicationSettingsManager $applicationSettingsManager,
		ApplicationConfigurationService $applicationConfigurationService,
		LinkGenerator $linkGenerator,
		ITemplateFactory $templateFactory,
		AvailabilityProvider $availabilityProvider
	){
		$this->repository = $repository;
		$this->applicationSettingsManager = $applicationSettingsManager;
		$this->linkGenerator = $linkGenerator;
		$this->templateFactory = $templateFactory;
		$this->applicationConfigurationService = $applicationConfigurationService;

		$this->onStockStatus = $availabilityProvider->getOnStockStatus();
	}

	public function generate()
	{
		$template = $this->getTemplate();

		$template->items = $this->getProcessedItems();

		$feedPath = $this->applicationConfigurationService->getGoogleMerchantFeedPath();

		FileSystem::write($feedPath, (string) $template);
	}

	private function getTemplate()
	{
		$template = $this->templateFactory->createTemplate();
		$template->setFile(__DIR__.'/feed.latte');

		$basicInformation = $this->applicationConfigurationService->getGoogleMerchantFeedData();
		$basicInformation['url'] = $this->domain = $this->linkGenerator->link('Front:Homepage:default');
		$template->basicInformation = $basicInformation;

		return $template;
	}

	/**
	 * @return array
	 */
	private function getProcessedItems(): array
	{
		$items = array();
		$mainUrl = rtrim($this->domain, '/');

		/** @var Product $product */
		foreach ($this->repository->findBy(['deleted ' => 0, 'active' => 1]) as $product) {
			/** @var Variant $variant */
			foreach ($product->getPriceVariants() as $variant) {
				if ($variant->getPrices()->count() === 0) {
					continue;
				}

				if ($product->getMainImage() === null) {
					continue;
				}

				$category = $product->getCategory();
				$url = $product->hasMorePrices()
					? $this->linkGenerator->link('Front:Product:default', ['category' => $category->getUrl(), 'url' => $product->getUrl(), 'variant' => Strings::webalize($variant->getName()) ,'variantId' => $variant->getId()])
					: $this->linkGenerator->link('Front:Product:default', ['category' => $category->getUrl(), 'url' => $product->getUrl()]);

				$availability = $this->onStockStatus->getId() === $variant->getAvailability()->getId()
					? true
					: ($variant->getAvailability()->isProductOrderable()
						? (new \DateTime())->modify('+'.$variant->getAvailability()->getGoogleMerchantAvailability().' days')
						: false
					);

				$items[] = new Item(
					"PN{$product->getId()}V{$variant->getId()}",
					$variant->getVariantName(),
					strip_tags(html_entity_decode($product->getDescription())),
					$availability,
					$url,
					$mainUrl.$product->getMainImage()->getRelativePath(),
					($variant->getPrices()->first())->getPrice(). ' CZK',
					"PN{$product->getId()}",
					$this->getCategoryText($category),
					$product->getProducer() ? $product->getProducer()->getName() : null,
					$variant->getEan() !== null && strlen($variant->getEan()) > 0 ? $variant->getEan() : null
				);
			}
		}

		return $items;
	}

	/**
	 * @param Category $category
	 * @return string
	 */
	private function getCategoryText(Category $category): string
	{
		$text = $category->getName();

		if ($category->getParent()) {
			$text = $this->getCategoryText($category->getParent()) . ' > ' . $text;
		}

		return $text;
	}
}