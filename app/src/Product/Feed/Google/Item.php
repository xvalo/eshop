<?php

namespace App\Product\Feed\Google;

use Nette\SmartObject;

class Item
{
	use SmartObject;

	/** @var string */
	private $id;

	/** @var string */
	private $title;

	/** @var string */
	private $description;

	/** @var string */
	private $url;

	/** @var string */
	private $imageUrl;

	/** @var string */
	private $price;

	/** @var string */
	private $groupId;

	/** @var string */
	private $categoryList;

	/** @var string|null */
	private $brand;

	/** @var bool|\DateTime */
	private $availability;

	/** @var string|null */
	private $gtin;

	/** @var bool */
	private $identifierExists;

	public function __construct(
		string $id,
		string $title,
		string $description,
		$availability,
		string $url,
		string $imageUrl,
		string $price,
		string $groupId,
		string $categoryList,
		string $brand = null,
		string $gtin = null
	){
		$this->id = $id;
		$this->title = $title;
		$this->description = $description;
		$this->url = $url;
		$this->imageUrl = $imageUrl;
		$this->price = $price;
		$this->groupId = $groupId;
		$this->categoryList = $categoryList;
		$this->brand = $brand;
		$this->availability = $availability;
		$this->gtin = $gtin;
		$this->identifierExists = $gtin !== null;
	}

	/**
	 * @return string
	 */
	public function getId(): string
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return $this->description;
	}

	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return $this->url;
	}

	/**
	 * @return string
	 */
	public function getImageUrl(): string
	{
		return $this->imageUrl;
	}

	/**
	 * @return string
	 */
	public function getPrice(): string
	{
		return $this->price;
	}

	/**
	 * @return string
	 */
	public function getGroupId(): string
	{
		return $this->groupId;
	}

	/**
	 * @return string
	 */
	public function getCategoryList(): string
	{
		return $this->categoryList;
	}

	/**
	 * @return string|null
	 */
	public function getBrand()
	{
		return $this->brand;
	}

	/**
	 * @return string
	 */
	public function getAvailability()
	{
		if ($this->availability instanceof \DateTime) {
			return 'preorder';
		}

		return $this->availability === true ? 'in stock' : 'out of stock';
	}

	public function getAvailabilityDate()
	{
		return ($this->availability instanceof \DateTime) ? $this->availability->format('Y-m-d') : null;
	}

	/**
	 * @return null|string
	 */
	public function getGtin()
	{
		return $this->gtin;
	}

	/**
	 * @return bool
	 */
	public function identifierExists(): bool
	{
		return $this->identifierExists;
	}

}