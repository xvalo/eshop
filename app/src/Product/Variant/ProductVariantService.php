<?php

namespace App\Classes\Product\Variant;

use App\Lists\ListsService;
use App\Stock\Supplier\Entity\Supplier;
use App\Product\Availability\Entity\Availability;
use App\Product\Code\Code;
use App\Product\Code\VariantCode;
use App\Product\Price\PercentagePrice;
use App\Product\Price\Price;
use App\Product\Price\RegularPrice;
use App\Product\Price\SubtractedPrice;
use App\Product\Product;
use App\Product\Variant;
use App\Product\VariantRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class ProductVariantService
{
	/** @var VariantRepository */
	private $variantRepository;

	/** @var ListsService */
	private $listsService;

	public function __construct(
		VariantRepository $variantRepository,
		ListsService $listsService
	){
		$this->variantRepository = $variantRepository;
		$this->listsService = $listsService;
	}

	/**
	 * @param Product $product
	 * @param string $name
	 * @param Availability $availability
	 * @param float|null $normalPrice
	 * @param string|null $ean
	 * @return Variant
	 * @throws \Exception
	 */
	public function createVariant(Product $product, string $name, Availability $availability, float $normalPrice = null, string $ean = null): Variant
	{
		$priceVariant = new Variant($product, $name, $availability, $normalPrice, $ean);
		$product->addVariant($priceVariant);
		$this->variantRepository->saveVariant($priceVariant);

		return $priceVariant;
	}

	/**
	 * @param Variant $variant
	 * @param Code $code
	 * @param Supplier|null $supplier
	 * @param bool $priceSynchronized
	 * @param bool $availabilitySynchronized
	 */
	public function addCodeToVariant(Variant $variant, Code $code, Supplier $supplier = null, bool $priceSynchronized = false, bool $availabilitySynchronized = false)
	{
		if ($supplier && $this->variantRepository->isExistingCodeSupplierRelation($supplier, $code)) {
			throw new CodeSupplierDuplicateException('Relation in variants between this code and supplier already exists.');
		}

		$variantCode = new VariantCode($variant, $code, $supplier, $priceSynchronized, $availabilitySynchronized);
		$variant->addCode($variantCode);
		$this->variantRepository->saveVariant($variant);
	}

	/**
	 * @param int $variantCodeId
	 * @param Variant $variant
	 * @param Code $code
	 * @param Supplier|null $supplier
	 * @param bool $priceSynchronized
	 * @param bool $availabilitySynchronized
	 */
	public function updateVariantCode(int $variantCodeId, Variant $variant, Code $code, Supplier $supplier = null, bool $priceSynchronized = false, bool $availabilitySynchronized = false)
	{
		$variantCodeRelation = $this->variantRepository->getVariantCodeRelationById($variantCodeId);

		$variantCodeRelation->setCode($code);
		$variantCodeRelation->setSupplier($supplier);
		$variantCodeRelation->setAvailabilitySynchronized($availabilitySynchronized);
		$variantCodeRelation->setPriceSynchronized($priceSynchronized);
		$this->variantRepository->saveVariant($variant);

		return;
	}

	/**
	 * @param Variant $variant
	 * @param Price $price
	 */
	public function addPriceToVariant(Variant $variant, Price $price)
	{
		$variant->addPrice($price);
		$this->variantRepository->saveVariant($variant);
	}

	public function updateRegularPrice(RegularPrice $regularPrice, string $name, float $price, bool $synchronized, float $purchasePriceWithoutVat = 0)
	{
		$regularPrice->setName($name);
		$regularPrice->setPrice($price);
		$regularPrice->setPurchasePriceWithoutVat($purchasePriceWithoutVat);

		if ($synchronized) {
			$regularPrice->enableSynchronization();
		} else {
			$regularPrice->disableSynchronization();
		}

		$this->variantRepository->saveVariant($regularPrice->getVariant());
	}

	public function updatePercentagePrice(PercentagePrice $percentagePrice, string $name, Price $countedFrom, float $percentage, float $purchasePriceWithoutVat = 0)
	{
		$percentagePrice->setName($name);
		$percentagePrice->setCountedFrom($countedFrom);
		$percentagePrice->setPercentage($percentage/100);
		$percentagePrice->setPurchasePriceWithoutVat($purchasePriceWithoutVat);

		$this->variantRepository->saveVariant($percentagePrice->getVariant());
	}

	public function updateSubtractedPrice(SubtractedPrice $subtractedPrice, string $name, Price $countedFrom, float $subtracted, float $purchasePriceWithoutVat = 0)
	{
		$subtractedPrice->setName($name);
		$subtractedPrice->setCountedFrom($countedFrom);
		$subtractedPrice->setSubtracted($subtracted);
		$subtractedPrice->setPurchasePriceWithoutVat($purchasePriceWithoutVat);

		$this->variantRepository->saveVariant($subtractedPrice->getVariant());
	}

	/**
	 * @param int $id
	 * @return Price|null
	 */
	public function getPrice(int $id)
	{
		return $this->variantRepository->findPrice($id);
	}

	public function removeCodeFromVariant(VariantCode $variantCode)
	{
		$variantCode->delete();
		$this->variantRepository->saveVariant($variantCode->getVariant());
	}

	public function removePriceFromVariant(Price $price)
	{
		$price->delete();
		$this->variantRepository->saveVariant($price->getVariant());
	}

	public function getCodeByString(string $codeString)
	{
		$code = $this->variantRepository->findCode($codeString);

		if (!$code) {
			$code = new Code($codeString);
			$this->variantRepository->saveCode($code);
		}

		return $code;
	}
}