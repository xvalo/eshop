<?php

namespace App\Classes\Product\Variant;

use App\InvalidArgumentException;
use App\Product\Price\PercentagePrice;
use App\Product\Price\Price;
use App\Product\Price\RegularPrice;
use App\Product\Price\SubtractedPrice;
use App\Product\Variant;

class ProductVariantPriceFactory
{
	public static function createRegularPrice(Variant $variant, float $price, string $name = '', bool $synchronized = false, float $purchasePriceWithoutVat = 0)
	{
		return new RegularPrice($variant, $price, $name, $synchronized, $purchasePriceWithoutVat);
	}

	public static function createPercentagePrice(Variant $variant, Price $countedFrom, float $percentage, string $name = '', float $purchasePriceWithoutVat = 0)
	{
		return new PercentagePrice($variant, $countedFrom, $percentage/100, $name, $purchasePriceWithoutVat);
	}

	public static function createSubtractedPrice(Variant $variant, Price $countedFrom, float $subtracted, string $name = '', float $purchasePriceWithoutVat = 0)
	{
		return new SubtractedPrice($variant, $countedFrom, $subtracted, $name, $purchasePriceWithoutVat);
	}
}