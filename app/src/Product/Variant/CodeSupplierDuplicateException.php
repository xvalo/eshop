<?php

namespace App\Classes\Product\Variant;


use App\InvalidArgumentException;

class CodeSupplierDuplicateException extends InvalidArgumentException
{

}