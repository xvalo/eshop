<?php

namespace App\Product;

use App\Model\BaseEntity;

class ProductDTO extends BaseEntity
{

	public $name;
	public $url;
	public $categoryUrl;
	public $imagePath;
	public $generalAvailability;
	public $normalPrice;
	public $isNews;
	public $isSpecialOffer;
	public $minPrice;
	public $hasMorePrices;
	public $annotation;

	public function __construct($id, $name, $url, $categoryUrl, $imagePath, $generalAvailability, $normalPrice, $minPrice, bool $isNews, bool $isSpecialOffer, $pricesCount, $annotation)
	{
		$this->id = $id;
		$this->name = $name;
		$this->url = $url;
		$this->categoryUrl = $categoryUrl;
		$this->imagePath = $imagePath;
		$this->generalAvailability = $generalAvailability;
		$this->normalPrice = $normalPrice;
		$this->isNews = $isNews;
		$this->isSpecialOffer = $isSpecialOffer;
		$this->minPrice = (float) $minPrice;
		$this->hasMorePrices = ((int) $pricesCount) > 1;
		$this->annotation = $annotation;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return mixed
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @return mixed
	 */
	public function getCategoryUrl()
	{
		return $this->categoryUrl;
	}

	/**
	 * @return mixed
	 */
	public function getImagePath()
	{
		return $this->imagePath;
	}

	/**
	 * @return mixed
	 */
	public function getGeneralAvailability()
	{
		return $this->generalAvailability;
	}

	/**
	 * @return float|null
	 */
	public function getNormalPrice()
	{
		return $this->normalPrice > 0 ? (float)$this->normalPrice : null;
	}

	/**
	 * @return mixed
	 */
	public function isNews()
	{
		return $this->isNews;
	}

	/**
	 * @return mixed
	 */
	public function isSpecialOffer()
	{
		return $this->isSpecialOffer;
	}

	/**
	 * @return float
	 */
	public function getMinPrice(): float
	{
		return $this->minPrice;
	}

	/**
	 * @return bool
	 */
	public function hasMorePrices(): bool
	{
		return $this->hasMorePrices;
	}

	/**
	 * @return string
	 */
	public function getAnnotation()
	{
		return $this->annotation;
	}

}