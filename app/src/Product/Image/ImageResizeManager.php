<?php


namespace App\Product\Image;


use App\Core\Application\Configuration\ApplicationConfigurationRepository;
use App\File\Image;
use App\Product\Product;
use Nette\SmartObject;
use Nette\Utils\Strings;
use Ublaboo\ImageStorage\ImageStorage;

class ImageResizeManager
{
	use SmartObject;

	const IMAGE_TYPE_PRODUCT = 1;
	const IMAGE_TYPE_ATTRIBUTE = 2;

	/** @var ImageStorage */
	private $imageStorage;

	/** @var array */
	private $attributeImageSettings;

	/** @var array */
	private $productImageSettings;

	public function __construct(
		ImageStorage $imageStorage,
		ApplicationConfigurationRepository $applicationConfigurationRepository
	){
		$this->imageStorage = $imageStorage;

		$this->attributeImageSettings = $applicationConfigurationRepository->getParameter(['images', 'attribute']);
		$this->productImageSettings = $applicationConfigurationRepository->getParameter(['images', 'product']);
	}

	public function createThumbImage(string $path, int $type)
	{
		$settings = $type === self::IMAGE_TYPE_PRODUCT ? $this->productImageSettings : $this->attributeImageSettings;
		$imagePath = Strings::after($path, '/public/data/');

		return $this->resizeImage([$imagePath, $settings['thumbSize']]);
	}

	public function createProductThumbImage(string $path)
	{
		return $this->createThumbImage($path, self::IMAGE_TYPE_PRODUCT);
	}

	public function createAttributeThumbImage(string $path)
	{
		return $this->createThumbImage($path, self::IMAGE_TYPE_ATTRIBUTE);
	}

	public function createImageStandardSize(string $path, int $type)
	{
		$settings = $type === self::IMAGE_TYPE_PRODUCT ? $this->productImageSettings : $this->attributeImageSettings;
		$imagePath = Strings::after($path, '/public/data/');

		return $this->resizeImage([$imagePath, $settings['fullSize']]);
	}

	public function getProductResizedImages(Product $product)
	{
		$resizedImages = [];

		/** @var Image $image */
		foreach ($product->getImages() as $image) {
			$resizedImages[] = [
				'thumb' => $this->createProductThumbImage($image->getRelativePath()),
				'full' => $this->createImageStandardSize($image->getRelativePath(), self::IMAGE_TYPE_PRODUCT)
			];
		}

		return $resizedImages;
	}

	/**
	 * @param array $args
	 * @return string
	 * @throws \Ublaboo\ImageStorage\ImageResizeException
	 */
	private function resizeImage(array $args): string
	{
		return '/public/data/' . (string)$this->imageStorage->fromIdentifier($args);
	}

}