<?php

namespace App\Product\Image;

use App\File\Image;
use App\File\ImageRepository;
use App\File\ImageService;
use App\Model\Services\ApplicationConfigurationService;
use App\Product\Product;
use Nette\Http\FileUpload;
use Nette\SmartObject;

class ImageManager
{
	use SmartObject;

	/** @var string */
	private $storageName;

	/** @var ImageService */
	private $imageService;

	/** @var ImageRepository */
	private $imageRepository;

	public function __construct(
		ImageService $imageService,
		ImageRepository $imageRepository,
		ApplicationConfigurationService $applicationConfigurationService
	){
		$this->imageService = $imageService;
		$this->imageRepository = $imageRepository;
		$this->storageName = $applicationConfigurationService->getProductImagesStorage();
	}

	/**
	 * @param FileUpload $data
	 * @param Product $product
	 * @throws \Nette\Utils\UnknownImageFileException
	 */
	public function attachImageToProduct(FileUpload $data, Product $product)
	{
		$image = $this->imageService->createImage($data, $this->storageName, true);
		$product->addImage($image);

		if (!$product->getMainImage()) {
			$product->setMainImage($image);
		}

		$this->imageRepository->flush();
	}

	/**
	 * @param Product $product
	 * @param $sourcePath
	 * @throws \Nette\Utils\UnknownImageFileException
	 */
	public function attachImageFromSource(Product $product, $sourcePath)
	{
		$image = $this->imageService->createImageFromSource($sourcePath, $this->storageName, true);

		$product->addImage($image);
		$product->setMainImage($image);
		$this->imageRepository->flush();
	}

	/**
	 * @param Product $product
	 * @param $imageId
	 * @throws \Exception
	 */
	public function removeImageFromProduct(Product $product, int $imageId)
	{
		/** @var Image $image */
		$image = $this->imageRepository->find($imageId);

		if ($product->getMainImage() !== null && $image->getId() == $product->getMainImage()->getId()) {
			$product->setMainImage(null);
		}

		$this->imageRepository->removeProductImageRelation($product, $image);
		$this->imageRepository->flush();
	}
}