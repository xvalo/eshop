<?php

namespace App\Product\Image\Entity;

use App\File\Image;
use App\Model\BaseEntity;
use App\Product\Product;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="products_images"
 * )
 */
class ProductImage extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Product\Product", inversedBy="images")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
	 */
	private $product;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\File\Image", cascade={"persist"}, inversedBy="products")
	 * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
	 */
	private $image;

	/**
	 * @ORM\Column(type="string", length=160,  nullable=true)
	 */
	private $name;

	public function __construct(Product $product, Image $image, string $name = null)
	{
		$this->product = $product;
		$this->image = $image;
		$this->name = $name;
	}

	/**
	 * @return Product
	 */
	public function getProduct()
	{
		return $this->product;
	}

	/**
	 * @return Image
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	public function setName(string $name)
	{
		$this->name = $name;
	}

	public function getRelativePath()
	{
		return $this->image->getRelativePath();
	}
}