<?php

namespace App\Classes\Order\Creation\OrderLine;


use App\Attribute\Product\ProductAttributeVariant;
use App\Lists\Delivery;
use App\Order\Delivery\Entity\CzechPostBranch;
use App\Order\Delivery\Entity\CzechPostOffice;
use App\Order\Order;
use App\Order\OrderLine\CardPaymentOrderLine;
use App\Order\OrderLine\DeliveryOrderLine;
use App\Order\OrderLine\DiscountOrderLine;
use App\Order\OrderLine\ExtraItemOrderLine;
use App\Order\OrderLine\OrderLine;
use App\Order\OrderLine\OrderLineReturned;
use App\Order\OrderLine\PaymentTypeOrderLine;
use App\Order\OrderLine\ProductOrderLine;
use App\Order\Payment\Entity\PaymentType;
use App\Product\Price\Price;

class OrderLineFactory
{
	/**
	 * @param Order $order
	 * @param Price $price
	 * @param int $count
	 * @param ProductAttributeVariant[] $attributes
	 * @return ProductOrderLine
	 */
	public static function createProductOrderLine(Order $order, Price $price, int $count, array $attributes = []): ProductOrderLine
	{
		return new ProductOrderLine($order, $price, $count, $attributes);
	}

	/**
	 * @param Order $order
	 * @param Delivery|null $delivery
	 * @param bool $freeDelivery
	 * @param CzechPostBranch|null $czechPostBox
	 * @param CzechPostOffice|null $czechPostOffice
	 * @return DeliveryOrderLine
	 */
	public static function createDeliveryOrderLine(Order $order, Delivery $delivery = null, bool $freeDelivery = false, CzechPostBranch $czechPostBox = null, CzechPostOffice $czechPostOffice = null): DeliveryOrderLine
	{
		return new DeliveryOrderLine($order, $delivery, $freeDelivery, $czechPostBox, $czechPostOffice);
	}

	/**
	 * @param Order $order
	 * @param PaymentType $paymentType
	 * @param float $price
	 * @return PaymentTypeOrderLine
	 */
	public static function createPaymentTypeOrderLine(Order $order, PaymentType $paymentType, float $price): PaymentTypeOrderLine
	{
		return new PaymentTypeOrderLine($order, $paymentType, $price);
	}

	/**
	 * @param Order $order
	 * @param float $discount
	 * @return DiscountOrderLine
	 */
	public static function createDiscountOrderLine(Order $order, float $discount): DiscountOrderLine
	{
		return new DiscountOrderLine($order, $discount);
	}

	/**
	 * @param Order $order
	 * @return CardPaymentOrderLine
	 */
	public static function createCardPaymentOrderLine(Order $order): CardPaymentOrderLine
	{
		return new CardPaymentOrderLine($order);
	}

	/**
	 * @param OrderLine $orderLine
	 * @param int $count
	 * @return OrderLineReturned
	 */
	public static function createReturnedOrderLine(OrderLine $orderLine, int $count): OrderLineReturned
	{
		return new OrderLineReturned($orderLine, $count);
	}

	/**
	 * @param Order $order
	 * @param float $unitPrice
	 * @param int $count
	 * @param int $vat
	 * @param string $description
	 * @return ExtraItemOrderLine
	 */
	public static function createExtraItemOrderLine(Order $order, float $unitPrice, int $count, int $vat, string $description)
	{
		return new ExtraItemOrderLine($order, $unitPrice, $count, $vat, $description);
	}
}