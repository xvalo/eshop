<?php

namespace App\Classes\Order\Creation;

use App\Classes\Order\AdminOrder\Cart\Item;
use App\Classes\Order\Creation\Factory\OrderPartsFactory;
use App\Classes\Order\Creation\OrderLine\OrderLineFactory;
use App\Classes\Order\Creation\ValueObject\LyonessCardData;
use App\Classes\Order\OrderSource;
use App\Classes\Product\Reserved\ReservedProductService;
use App\Customer\Customer;
use App\Model\Services\ApplicationConfigurationService;
use App\Order\Delivery\DeliveryService;
use App\Lists\Delivery;
use App\Stock\StockManager;
use App\Order\ContactInformation;
use App\Order\DeliveryAddress;
use App\Order\Discount\Discount;
use App\Order\InvoiceInformation;
use App\Order\Order;
use App\Order\OrderLine\ExtraItemOrderLine;
use App\Order\OrderLine\ProductOrderLine;
use App\Order\OrderLineState;
use App\Order\OrderRepository;
use App\Order\Payment\Entity\PaymentType;
use App\Product\Availability\AvailabilityProvider;
use App\Product\Price\Price;
use App\Product\VariantRepository;
use App\Stock\StockItem;
use Nette\SmartObject;

class OrderCreationService
{
	use SmartObject;

	/** @var OrderRepository */
	private $orderRepository;

	/** @var VariantRepository */
	private $variantRepository;

	/** @var OrderPartsFactory */
	private $orderPartsFactory;

	/** @var ReservedProductService */
	private $reservedProductService;

	/** @var StockManager */
	private $stockManager;

	/** @var DeliveryService */
	private $deliveryService;

	/** @var ApplicationConfigurationService */
	private $applicationConfigurationService;

	/** @var AvailabilityProvider */
	private $availabilityProvider;

	public function __construct(
		OrderRepository $orderRepository,
		VariantRepository $variantRepository,
		OrderPartsFactory $orderPartsFactory,
		ReservedProductService $reservedProductService,
		StockManager $stockManager,
		DeliveryService $deliveryService,
		ApplicationConfigurationService $applicationConfigurationService,
		AvailabilityProvider $availabilityProvider
	){
		$this->orderRepository = $orderRepository;
		$this->variantRepository = $variantRepository;
		$this->orderPartsFactory = $orderPartsFactory;
		$this->reservedProductService = $reservedProductService;
		$this->stockManager = $stockManager;
		$this->deliveryService = $deliveryService;
		$this->applicationConfigurationService = $applicationConfigurationService;
		$this->availabilityProvider = $availabilityProvider;
	}

	public function createEshopOrder(
		ContactInformation $contactInformation,
		InvoiceInformation $invoiceInformation,
		DeliveryAddress $deliveryAddress,
		array $products,
		Delivery $delivery,
		PaymentType $paymentType,
		Customer $customer = null,
		string $customerNote = null,
		LyonessCardData $lyonessCardData = null,
		$specialDelivery = null,
		Discount $discount = null
	): Order
	{
		$order = $this->create(
			OrderSource::ESHOP,
			$contactInformation,
			$invoiceInformation,
			$products,
			$deliveryAddress,
			$delivery,
			$paymentType,
			$customer,
			$discount,
			$customerNote ?: '',
			$lyonessCardData,
			[],
			$specialDelivery
		);

		//$this->updateStockItems($order);

		return $order;
	}

	public function createStoreOrder(
		ContactInformation $contactInformation,
		InvoiceInformation $invoiceInformation,
		DeliveryAddress $deliveryAddress,
		array $products,
		Delivery $delivery = null,
		PaymentType $paymentType = null,
		Customer $customer = null,
		LyonessCardData $lyonessCardData = null,
		Discount $discount = null,
		array $extraItems = [],
		$specialDelivery = null
	): Order
	{
		$order = $this->create(
			OrderSource::STORE,
			$contactInformation,
			$invoiceInformation,
			$products,
			$deliveryAddress,
			$delivery,
			$paymentType,
			$customer,
			$discount,
			'',
			$lyonessCardData,
			$extraItems,
			$specialDelivery
		);

		//$this->updateStockItems($order, false);

		return $order;
	}

	public function create(
		int $source,
		ContactInformation $contactInformation,
		InvoiceInformation $invoiceInformation,
		array $products,
		DeliveryAddress $deliveryAddress = null,
		Delivery $delivery = null,
		PaymentType $paymentType = null,
		Customer $customer = null,
		Discount $discount = null,
		string $customerNote = '',
		LyonessCardData $lyonessCardData = null,
		array $extraItems = [],
		$specialDelivery = null
	) : Order
	{
		if (!OrderSource::isCorrectSource($source)) {
			throw new InvalidOrderCreationSourceException('Given order source is not supported.');
		}

		$order = new Order($source, $contactInformation, $invoiceInformation, $deliveryAddress, $customer, $customerNote);

		foreach ($products as $data) {
			/** @var Price $price */
			$price = $this->variantRepository->findPrice((int)$data['id']);
			$order->addOrderLine(OrderLineFactory::createProductOrderLine($order, $price, $data['count'], $data['attributes']));
		}

		/** @var Item $item */
		foreach ($extraItems as $item) {
			$order->addOrderLine(OrderLineFactory::createExtraItemOrderLine($order, $item->getPrice(), $item->getQuantity(), $item->getVatRate(), $item->getName()));
		}

		if ($delivery) {
			$freeDelivery = $this->deliveryService->isOrderAvailableForFreeDelivery($order->getProducts(), $delivery);

			$czechPostBox = $delivery->getId() == $this->applicationConfigurationService->getCzechPostBoxDeliveryBinding() ? $specialDelivery : null;
			$czechPostOffice = $delivery->getId() == $this->applicationConfigurationService->getCzechPostOfficeDeliveryBinding() ? $specialDelivery : null;

			$order->addOrderLine(OrderLineFactory::createDeliveryOrderLine($order, $delivery, $freeDelivery, $czechPostBox, $czechPostOffice));
			$order->addOrderLine(OrderLineFactory::createPaymentTypeOrderLine($order, $paymentType, $freeDelivery ? 0 : $delivery->getPaymentTypePrice($paymentType)));
		}

		if ($discount) {
			if ($discount->getType() === Discount::TYPE_PERCENTAGE) {
				$discountAmount = 0;
				foreach ($order->getOrderLines() as $orderLine) {
					if ($orderLine instanceof ProductOrderLine || $orderLine instanceof ExtraItemOrderLine) {
						$discountAmount += $orderLine->getCount() * $orderLine->getUnitPrice();
					}
				}
				$discountAmount *= ($discount->getValue()/100);
			} else {
				$discountAmount = $discount->getValue();
			}
			$order->addOrderLine(OrderLineFactory::createDiscountOrderLine($order, floor($discountAmount)));
		}

		if ($lyonessCardData) {
			if ($lyonessCardData->getBirthDay()) {
				$order->setLyoneesCard();
			}
			$order->setBirthDay($lyonessCardData->getBirthDay());
			$order->setLyonessCardEan($lyonessCardData->getCardEan());
			$order->setLyonessCardId($lyonessCardData->getCardId());
		}

		$this->orderRepository->persist($order);
		$this->orderRepository->flush();

		return $order;
	}

	public function update(
		Order $order,
		int $status,
		array $contactInformation,
		array $invoiceInformation,
		array $deliveryAddress,
		array $products,
		Delivery $delivery = null,
		PaymentType $paymentType = null,
		LyonessCardData $lyonessCardData = null,
		Discount $discount = null,
		array $extraItems = []
	): Order
	{
		$order->setStatus($status);
		$this->setDataToObject($order->getContactInformation(), $contactInformation);
		$this->setDataToObject($order->getInvoiceInformation(), $invoiceInformation);

		if ($order->getDeliveryAddress()) {
			$this->setDataToObject($order->getDeliveryAddress(), $deliveryAddress);
		} else {
			$order->setDeliveryAddress($this->orderPartsFactory->createDeliveryAddress($deliveryAddress['name'], $deliveryAddress['lastName'], $deliveryAddress['company'], $deliveryAddress['street'], $deliveryAddress['city'], $deliveryAddress['zip'], $deliveryAddress['country']));
		}

		/** @var Item $item */
		foreach ($products as $item) {
			/** @var Price $price */
			$price = $this->variantRepository->findPrice($item->getId());
			$order->addOrderLine(OrderLineFactory::createProductOrderLine($order, $price, $item->getQuantity(), $item->getOptions()));
		}

		/** @var Item $item */
		foreach ($extraItems as $item) {
			$order->addOrderLine(OrderLineFactory::createExtraItemOrderLine($order, $item->getPrice(), $item->getQuantity(), $item->getVatRate(), $item->getName()));
		}

		if(
			(!$order->getDelivery() && !is_null($delivery))
			|| ($order->getDelivery() && is_null($delivery))
			|| ($order->getDelivery() && $order->getDelivery()->getId() != $delivery->getId())
		){
			if (!$order->setDelivery($delivery)) {
				$order->addOrderLine(OrderLineFactory::createDeliveryOrderLine($order, $delivery, $this->deliveryService->isOrderAvailableForFreeDelivery($order->getProducts(), $delivery)));
			}
			$order->removeShipped();
		}

		if ($order->getDelivery() && $paymentType) {
			$order->setPaymentType($paymentType, $order->getDelivery()->getPaymentTypePrice($paymentType));
		} else {
			$order->setPaymentType(null);
		}


		if ($discount !== null && !$order->setDiscount($discount->getValue())) {
			$order->addOrderLine(OrderLineFactory::createDiscountOrderLine($order, $discount->getValue()));
		}

		if ($lyonessCardData) {
			$order->setLyonessCardId($lyonessCardData->getCardId());
			$order->setLyonessCardEan($lyonessCardData->getCardEan());
		}

		$this->orderRepository->flush();

		return $order;
	}

	private function setDataToObject($object, array $data)
	{
		foreach($data as $key => $item){
			if(is_array($item)){
				$newItem = $object->__get($key);
				$this->setDataToObject($newItem, $item);
			}else {
				$object->__set($key, $item);
			}
		}

		return;
	}

	public function updateStockItems(Order $order, bool $createReservation = true)
	{
		/** @var ProductOrderLine $productOrderLine */
		foreach ($order->getProducts() as $productOrderLine) {
			if ($productOrderLine->getPrice()->getVariant()->getAvailability()->getId() == $this->availabilityProvider->getOnStockStatus()->getId()) {
				$remainingCount = $productOrderLine->getCount();
				$product = $productOrderLine->getPrice()->getVariant();

				/** @var StockItem $stockItem */
				foreach ($product->getNonZeroCountStockItems() as $stockItem) {
					if ($stockItem->getCount() >= $remainingCount) {
						$countToReserve = $remainingCount;
						$remainingCount = 0;
					} else {
						$countToReserve = $stockItem->getCount();
						$remainingCount -= $stockItem->getCount();
					}

					$this->stockManager->sellStockItem($product, $stockItem->getStock(), $order, $countToReserve);
					if ($createReservation) {
						$productOrderLine->setState(OrderLineState::IN_STOCK);
						$this->reservedProductService->createReservation($product, $productOrderLine, $stockItem, $countToReserve);
					}

					if ($remainingCount == 0) {
						break;
					}
				}

				if ($remainingCount > 0) {
					$this->stockManager->sellStockItem($product, $this->stockManager->getMainStock(), $order, $remainingCount);
					if ($createReservation) {
						$productOrderLine->setState(OrderLineState::IN_STOCK);
						$this->reservedProductService->createReservation($product, $productOrderLine, $product->getStockItem($this->stockManager->getMainStock()), $remainingCount);
					}
				}
			} elseif ($productOrderLine->getState() == OrderLineState::IN_STOCK) {
				$productOrderLine->setState(null);
			}
		}
	}
}