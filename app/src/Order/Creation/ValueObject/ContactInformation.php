<?php

namespace App\Classes\Order\Creation\ValueObject;

final class ContactInformation
{
	/** @var string */
	private $name;

	/** @var string */
	private $lastName;

	/** @var string */
	private $email;

	/** @var string */
	private $phone;

	public function __construct(string $name, string $lastName, string $email, string $phone)
	{
		$this->name = $name;
		$this->lastName = $lastName;
		$this->email = $email;
		$this->phone = $phone;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getLastName(): string
	{
		return $this->lastName;
	}

	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getPhone(): string
	{
		return $this->phone;
	}
}