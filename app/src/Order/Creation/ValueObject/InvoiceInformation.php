<?php

namespace App\Classes\Order\Creation\ValueObject;

final class InvoiceInformation
{
	/** @var string */
	private $company;

	/** @var string */
	private $ico;

	/** @var string */
	private $dic;

	/** @var string */
	private $street;

	/** @var string */
	private $city;

	/** @var string */
	private $zip;

	/** @var int */
	private $country;

	public function __construct(string $company, string $ico, string $dic, string $street, string $city, string $zip, int $country)
	{
		$this->company = $company;
		$this->ico = $ico;
		$this->dic = $dic;
		$this->street = $street;
		$this->city = $city;
		$this->zip = $zip;
		$this->country = $country;
	}

	/**
	 * @return string
	 */
	public function getCompany(): string
	{
		return $this->company;
	}

	/**
	 * @return string
	 */
	public function getIco(): string
	{
		return $this->ico;
	}

	/**
	 * @return string
	 */
	public function getDic(): string
	{
		return $this->dic;
	}

	/**
	 * @return string
	 */
	public function getStreet(): string
	{
		return $this->street;
	}

	/**
	 * @return string
	 */
	public function getCity(): string
	{
		return $this->city;
	}

	/**
	 * @return string
	 */
	public function getZip(): string
	{
		return $this->zip;
	}

	/**
	 * @return int
	 */
	public function getCountry(): int
	{
		return $this->country;
	}
}