<?php

namespace App\Classes\Order\Creation\ValueObject;

final class DeliveryAddress
{
	/** @var string */
	private $name;

	/** @var string */
	private $lastName;

	/** @var string */
	private $company;

	/** @var string */
	private $street;

	/** @var string */
	private $city;

	/** @var string */
	private $zip;

	/** @var int */
	private $country;

	public function __construct(string $name, string $lastName, string $company, string $street, string $city, string $zip, int $country)
	{
		$this->name = $name;
		$this->lastName = $lastName;
		$this->company = $company;
		$this->street = $street;
		$this->city = $city;
		$this->zip = $zip;
		$this->country = $country;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return string
	 */
	public function getLastName(): string
	{
		return $this->lastName;
	}

	/**
	 * @return string
	 */
	public function getCompany(): string
	{
		return $this->company;
	}

	/**
	 * @return string
	 */
	public function getStreet(): string
	{
		return $this->street;
	}

	/**
	 * @return string
	 */
	public function getCity(): string
	{
		return $this->city;
	}

	/**
	 * @return string
	 */
	public function getZip(): string
	{
		return $this->zip;
	}

	/**
	 * @return int
	 */
	public function getCountry(): int
	{
		return $this->country;
	}
}