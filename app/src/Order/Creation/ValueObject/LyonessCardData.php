<?php

namespace App\Classes\Order\Creation\ValueObject;


class LyonessCardData
{
	/** @var string */
	private $birthDay;

	/** @var string */
	private $cardId;

	/** @var string */
	private $cardEan;

	public function __construct(string $birthDay = null, string $cardId = null, string $cardEan = null)
	{
		$this->birthDay = $birthDay;
		$this->cardId = $cardId;
		$this->cardEan = $cardEan;
	}

	/**
	 * @return string|null
	 */
	public function getBirthDay()
	{
		return $this->birthDay;
	}

	/**
	 * @return string|null
	 */
	public function getCardId()
	{
		return $this->cardId;
	}

	/**
	 * @return string|null
	 */
	public function getCardEan()
	{
		return $this->cardEan;
	}
}