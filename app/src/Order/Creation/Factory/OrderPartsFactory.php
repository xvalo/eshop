<?php

namespace App\Classes\Order\Creation\Factory;


use App\Order\ContactInformation;
use App\Order\DeliveryAddress;
use App\Order\InvoiceInformation;

final class OrderPartsFactory
{
	public function createContactInformation(string $name, string $lastName, string $email, string $phone): ContactInformation
	{
		return new ContactInformation($name, $lastName, $email, $phone);
	}

	public function createDeliveryAddress(string $name, string $lastName, string $company, string $street, string $city, string $zip, int $country): DeliveryAddress
	{
		foreach (func_get_args() as $arg) {
			if (strlen($arg) > 0) {
				return new DeliveryAddress($name, $lastName, $company, $street, $city, $zip, $country);
			}
		}

		return null;
	}

	public function createInvoiceInformation(string $company, string $ico, string $dic, string $street, string $city, string $zip, int $country): InvoiceInformation
	{
		return new InvoiceInformation($company, $ico, $dic, $street, $city, $zip, $country);
	}
}