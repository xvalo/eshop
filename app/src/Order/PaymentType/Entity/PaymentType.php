<?php

namespace App\Order\Payment\Entity;

use App\Model\BaseEntity;
use App\Traits\ActivityTrait;
use App\Traits\IRemovable;
use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class PaymentType extends BaseEntity implements IRemovable
{
    use RemovableTrait;
    use ActivityTrait;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

	/**
	 * @ORM\Column(type="integer", length=5)
	 */
	private $priority;

    public function __construct(string $name, int $priority)
    {
        $this->name = $name;
        $this->priority = $priority;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

	/**
	 * @return int
	 */
	public function getPriority(): int
	{
		return $this->priority;
	}

	/**
	 * @param int $priority
	 */
	public function setPriority(int $priority)
	{
		$this->priority = $priority;
	}
}