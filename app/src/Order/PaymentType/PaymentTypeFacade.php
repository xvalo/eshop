<?php

namespace App\Order\Payment;

use App\Order\Payment\Entity\PaymentType;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\QueryBuilder;

class PaymentTypeFacade
{
	/** @var EntityManager */
	private $em;

	/** @var \Kdyby\Doctrine\EntityRepository */
	private $paymentTypeRepository;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$this->paymentTypeRepository = $this->em->getRepository(PaymentType::class);
	}

	/**
	 * @param int $id
	 * @return null|PaymentType
	 */
	public function getPaymentType(int $id)
	{
		return $this->paymentTypeRepository->find($id);
	}

	/**
	 * @return array
	 */
	public function getPaymentTypesList(): array
	{
		return $this->paymentTypeRepository->findPairs(['active' => true, 'deleted' => false], 'name');
	}

	/**
	 * @return QueryBuilder
	 */
	public function getGridSource(): QueryBuilder
	{
		return $this->paymentTypeRepository->createQueryBuilder('P')
					->where('P.deleted = :deleted')
					->setParameter('deleted', false);
	}

	/**
	 * @param PaymentType $paymentType
	 * @throws \Exception
	 */
	public function savePaymentType(PaymentType $paymentType)
	{
		if (!$paymentType->getId()) {
			$this->em->persist($paymentType);
		}

		$this->em->flush();
	}

	/**
	 * @return mixed
	 * @throws \Doctrine\ORM\NoResultException
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function getHighestPriority()
	{
		return $this->paymentTypeRepository->createQueryBuilder('P')
					->select('MAX(P.priority)')
					->where('P.deleted = :deleted')
					->setParameter('deleted', false)
					->getQuery()->getSingleScalarResult();
	}
}