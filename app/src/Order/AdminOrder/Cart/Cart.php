<?php

namespace App\Classes\Order\AdminOrder\Cart;

use Nette\Http\SessionSection;
use Nette\SmartObject;
use Ramsey\Uuid\Uuid;

class Cart
{
	use SmartObject;

	/**
	 * @var SessionSection
	 */
	private $session;

	public function __construct(SessionSection $sessionSection)
	{
		$this->session = $sessionSection;
	}

	/**
	 * Returns all items in cart
	 * @return Item[]
	 */
	public function getItems()
	{
		return $this->session->items[Item::REGULAR];
	}

	public function getExtraItems()
	{
		return $this->session->items[Item::EXTRA];
	}

	/**
	 * @param string $type
	 * @param string $key
	 * @return Item|null
	 */
	public function getItem(string $type, string $key)
	{
		return $this->itemExists($type, $key) ? $this->session->items[$type][$key] : NULL;
	}

	/**
	 * @param string $type
	 * @param string $key
	 * @return bool
	 */
	public function itemExists(string $type, string $key)
	{
		return array_key_exists($key, $this->session->items[$type]);
	}

	/**
	 * @param int $id
	 * @param float $price
	 * @param int $quantity
	 * @param array $options
	 * @return Item|null
	 */
	public function addRegularItem(int $id, float $price, int $quantity = 1, array $options = [])
	{
		return $this->addItem(Item::REGULAR, $id, $price, $quantity, $options);
	}

	/**
	 * @param float $price
	 * @param int $quantity
	 * @return Item|null
	 */
	public function addExtraItem(float $price, int $quantity = 1)
	{
		return $this->addItem(Item::EXTRA, time(), $price, $quantity);
	}

	/**
	 * @param string $type
	 * @param int $id
	 * @param float $price
	 * @param int $quantity
	 * @param array $options
	 * @return Item|null
	 */
	public function addItem(string $type, int $id, float $price, int $quantity = 1, array $options = [])
	{
		$key = $this->createKey($id, $options);
		$item = $this->getItem($type, $key);
		if ($item) {
			$item->addQuantity($quantity);
		} else {
			$item = new Item($type, $id, $price, $quantity, $options);
			$this->session->items[$type][$key] = $item;
		}
		return $item;
	}

	/**
	 * @param string $type
	 * @param string $key
	 * @param int $quantity
	 */
	public function update(string $type, string $key, int $quantity)
	{
		if ($quantity <= 0) {
			$this->delete($key);
		} else {
			/** @var Item $item */
			$item = $this->itemExists($type, $key) ? $this->session->items[$type][$key] : NULL;
			if ($item) {
				$item->setQuantity($quantity);
			}
		}
	}

	/**
	 * @param string $type
	 * @param string $key
	 */
	public function delete(string $type, string $key)
	{
		if($this->itemExists($type, $key)) {
			unset($this->session->items[$type][$key]);
		}
	}

	/**
	 * @param float $discount
	 */
	public function setDiscount(float $discount)
	{
		if ($discount < 0) {
			$discount = 0;
		}
		$this->session->discount = $discount;
	}

	/**
	 * @return float
	 */
	public function getDiscount()
	{
		return isset($this->session->discount) ? $this->session->discount : 0.0;
	}

	public function clear()
	{
		$this->session->items[Item::REGULAR] = array();
		$this->session->items[Item::EXTRA] = array();
		$this->session->discount = 0.0;
	}

	public function prepare()
	{
		if(!isset($this->session->items)) {
			$this->session->items[Item::REGULAR] = array();
			$this->session->items[Item::EXTRA] = array();
		}

		if(!isset($this->session->discount)) {
			$this->session->discount = 0.0;
		}
	}

	/**
	 * @return bool
	 */
	public function isEmpty()
	{
		return $this->getQuantity() == 0;
	}


	/**
	 * @return float|int
	 */
	public function getTotal()
	{
		$total = 0;

		/** @var Item $item */
		foreach($this->getItems() as $item) {
			$total += $item->getTotal();
		}

		/** @var Item $item */
		foreach($this->getExtraItems() as $item) {
			$total += $item->getTotal();
		}

		return $total - $this->getDiscount();
	}

	/**
	 * @return float|int
	 */
	public function getTotalWithoutVat()
	{
		$total = 0;

		/** @var Item $item */
		foreach($this->getItems() as $item) {
			$total += $item->getTotalWithoutVat();
		}

		/** @var Item $item */
		foreach($this->getExtraItems() as $item) {
			$total += $item->getTotalWithoutVat();
		}

		return $total;
	}

	/**
	 * @return int
	 */
	public function getQuantity()
	{
		$quantity = 0;

		/** @var Item $item */
		foreach($this->getItems() as $item) {
			$quantity += $item->getQuantity();
		}

		/** @var Item $item */
		foreach($this->getExtraItems() as $item) {
			$quantity += $item->getQuantity();
		}

		return $quantity;
	}

	/**
	 * @param int $id
	 * @param array $options
	 * @return string
	 */
	private function createKey(int $id, array $options = [])
	{
		$options = (array)$options;
		sort($options);
		return md5($id . '-' . serialize($options));
	}
}