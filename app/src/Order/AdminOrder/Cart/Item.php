<?php

namespace App\Classes\Order\AdminOrder\Cart;

use Nette\SmartObject;

class Item
{
	use SmartObject;

	const REGULAR = 'R';
	const EXTRA = 'E';
	
	/** @var int */
	private $id;

	/** @var string */
	private $type;

	/** @var string */
	private $name;

	/** @var string */
	private $codes;

	/** @var string */
	private $link;

	/** @var mixed */
	private $linkArgs = [];

	/** @var float */
	private $price;

	/** @var float */
	private $priceWithoutVat;

	/** @var int */
	private $pricePrecision = 2;

	/** @var int */
	private $vatRate;

	/** @var mixed */
	private $quantity;

	/** @var string */
	private $unit;

	/** @var string */
	private $availability;

	/** @var string */
	private $image;

	/** @var array */
	private $options = [];

	/** @var array */
	private $data = [];


	function __construct(string $type, int $id, float $price = NULL, int $quantity = 1, array $options = [])
	{
		$this->type = $type;
		$this->id = $id;
		$this->price = $price;
		$this->quantity = $quantity;
		$this->options = $options;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 * @return Item
	 */
	public function setName(string $name): Item
	{
		$this->name = $name;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCodes(): string
	{
		return $this->codes;
	}

	/**
	 * @param string $codes
	 * @return Item
	 */
	public function setCodes(string $codes): Item
	{
		$this->codes = $codes;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLink(): string
	{
		return $this->link;
	}

	/**
	 * @param string $link
	 * @return Item
	 */
	public function setLink(string $link): Item
	{
		$this->link = $link;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getLinkArgs()
	{
		return $this->linkArgs;
	}

	/**
	 * @param mixed $linkArgs
	 * @return Item
	 */
	public function setLinkArgs($linkArgs)
	{
		$this->linkArgs = $linkArgs;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getPricePrecision(): int
	{
		return $this->pricePrecision;
	}

	/**
	 * @param int $pricePrecision
	 * @return Item
	 */
	public function setPricePrecision(int $pricePrecision): Item
	{
		$this->pricePrecision = $pricePrecision;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getVatRate(): int
	{
		return $this->vatRate;
	}

	/**
	 * @param int $vatRate
	 * @return Item
	 */
	public function setVatRate(int $vatRate): Item
	{
		$this->vatRate = $vatRate;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * @param mixed $quantity
	 * @return Item
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getUnit(): string
	{
		return $this->unit;
	}

	/**
	 * @param string $unit
	 * @return Item
	 */
	public function setUnit(string $unit): Item
	{
		$this->unit = $unit;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getAvailability(): string
	{
		return $this->availability;
	}

	/**
	 * @param string $availability
	 * @return Item
	 */
	public function setAvailability(string $availability): Item
	{
		$this->availability = $availability;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getImage(): string
	{
		return $this->image;
	}

	/**
	 * @param string $image
	 * @return Item
	 */
	public function setImage(string $image): Item
	{
		$this->image = $image;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getOptions(): array
	{
		return $this->options;
	}

	/**
	 * @param array $options
	 * @return Item
	 */
	public function setOptions(array $options): Item
	{
		$this->options = $options;
		return $this;
	}

	/**
	 * @return array
	 */
	public function getData(): array
	{
		return $this->data;
	}

	/**
	 * @param array $data
	 * @return Item
	 */
	public function setData(array $data): Item
	{
		$this->data = $data;
		return $this;
	}


	public function addQuantity($quantity)
	{
		$this->quantity += $quantity;
		return $this;
	}


	public function getPrice()
	{
		if ($this->price) {
			return $this->price;
		}
		return round($this->priceWithoutVat + ($this->priceWithoutVat / 100 * $this->vatRate), $this->pricePrecision);
	}


	public function getTotal()
	{
		return $this->getPrice() * $this->quantity;
	}


	public function getPriceWithoutVat()
	{
		if ($this->priceWithoutVat) {
			return $this->priceWithoutVat;
		}
		return round(($this->price / (100 + $this->vatRate)) * 100, $this->pricePrecision);
	}


	public function getTotalWithoutVat()
	{
		return $this->getPriceWithoutVat() * $this->quantity;
	}

}