<?php

namespace App\Classes\Order;

use App\Order\Payment\Entity\PaymentType;

class OrderDeliverability
{
	const CASH_ON_DELIVERY = 1;
	const TRANSFER_PAYMENT = 2;
	const PERSONAL_PAYMENT = 3;

	public static function isOrderByDeliveryInvoicable(PaymentType $paymentType = null)
	{
		$paymentTypes = [
			self::CASH_ON_DELIVERY,
			self::TRANSFER_PAYMENT
		];

		return !is_null($paymentType) && in_array($paymentType->getId(), $paymentTypes);
	}

	public static function isOrderByDeliveryBillable(PaymentType $paymentType = null)
	{
		$paymentTypes = [
			self::PERSONAL_PAYMENT
		];

		return is_null($paymentType) || in_array($paymentType->getId(), $paymentTypes);
	}
}