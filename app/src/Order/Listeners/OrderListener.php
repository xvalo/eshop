<?php

namespace App\Order\Listeners;

use App\Core\Application\Settings\ApplicationSettingsManager;
use App\Order\Order;
use App\Order\OrderLine\DeliveryOrderLine;
use App\Order\OrderLine\OrderLine;
use App\Order\OrderLine\ProductOrderLine;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\UnitOfWork;
use Kdyby\Events\Subscriber;
use Nette\SmartObject;
use Nette\Utils\DateTime;

class OrderListener implements Subscriber
{
	use SmartObject;

	/** @var ApplicationSettingsManager */
	private $applicationSettingsManager;

	/** @var EntityManager */
	private $entityManager;

	/** @var UnitOfWork */
	private $uow;

	public function __construct(
		ApplicationSettingsManager $applicationSettingsManager
	){
		$this->applicationSettingsManager = $applicationSettingsManager;
	}

    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
            Events::onFlush
        ];
    }

    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();
        $this->entityManager = $eventArgs->getEntityManager();

        if($entity instanceof Order){
            $order = $entity;
            $order = $this->checkDeliveryFreePriceLimit($order);
            $order->setTotalPrice( $this->countOrderTotalPrice($order) );

	        $orderNo = $this->applicationSettingsManager->getNextOrderNumber();
            $order->setOrderNo($orderNo);
            $this->applicationSettingsManager->setNextOrderNumber(++$orderNo);
        }
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();
        if($entity instanceof Order) {
            $entity->setUpdated(new DateTime());
        }
    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {
	    $this->entityManager = $eventArgs->getEntityManager();
        $this->uow = $this->entityManager->getUnitOfWork();

        foreach ($this->uow->getScheduledEntityUpdates() as $entity) {
            if($entity instanceof Order){
                $order = $entity;
	            $order = $this->checkDeliveryFreePriceLimit($order);
                $order->setTotalPrice( $this->countOrderTotalPrice($order) );
	            $this->uow->recomputeSingleEntityChangeSet($this->entityManager->getClassMetadata(get_class($order)), $order);
            }

            if ($entity instanceof OrderLine) {
                $changes = $this->uow->getEntityChangeSet($entity);
                if (isset($changes['order']) && is_null($changes['order'][1])) {
                    /** @var Order $order */
                    $order = $changes['order'][0];
                } else {
                    $order = $entity->getOrder();
                }
	            $order = $this->checkDeliveryFreePriceLimit($order);
                $order->setTotalPrice($this->countOrderTotalPrice($order));
	            $this->uow->recomputeSingleEntityChangeSet($this->entityManager->getClassMetadata(get_class($order)), $order);
            }
        }
        
        foreach ($this->uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof OrderLine) {
                $order = $entity->getOrder();
	            $order = $this->checkDeliveryFreePriceLimit($order);
                $order->setTotalPrice( $this->countOrderTotalPrice($order) );
	            $this->uow->recomputeSingleEntityChangeSet($this->entityManager->getClassMetadata(get_class($order)), $order);
            }
        }

    }

	private function checkDeliveryFreePriceLimit(Order $order)
	{
		$productsTotalPrice = 0;

		/** @var ProductOrderLine $product */
		foreach ($order->getProducts() as $product) {
			$productsTotalPrice += $product->getCount() * $product->getUnitPrice();
		}

		/** @var OrderLine $orderLine */
		foreach ($order->getOrderLines() as $orderLine) {
			if ($orderLine instanceof DeliveryOrderLine) {
				$orderLine->updatePriceWithFreePriceLimit($productsTotalPrice);
				if ($orderLine->getId() !== null) {
					$this->uow->recomputeSingleEntityChangeSet($this->entityManager->getClassMetadata(get_class($orderLine)), $orderLine);
				}
			}
		}

		return $order;
    }

    private function countOrderTotalPrice(Order $order)
    {
        $price = 0;
        foreach($order->getOrderLines() as $orderLine){
            $price += $orderLine->getCount() * $orderLine->getUnitPrice();
        }

		return $price;
    }

}