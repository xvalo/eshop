<?php


namespace App\Order\Discount;


use App\InvalidArgumentException;
use Nette\SmartObject;

class Discount
{
	use SmartObject;

	const TYPE_PERCENTAGE = 'P';
	const TYPE_AMOUNT = 'A';

	/** @var float */
	private $value;

	/** @var string */
	private $type;

	public function __construct(float $value, string $type)
	{
		if ($type !== self::TYPE_AMOUNT && $type !== self::TYPE_PERCENTAGE) {
			throw new InvalidArgumentException('Wrong type');
		}

		$this->value = $value;
		$this->type = $type;
	}

	/**
	 * @return float
	 */
	public function getValue(): float
	{
		return $this->value;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}
}