<?php

namespace App\Classes\Order\Customer;

use App\Customer\Customer;
use App\Order\Order;
use App\Order\OrderRepository;

class OrdersService
{
	/** @var OrderRepository */
	private $orderRepository;

	public function __construct(OrderRepository $orderRepository)
	{
		$this->orderRepository = $orderRepository;
	}

	public function getCustomerOrders(Customer $customer)
	{
		return $this->orderRepository->findBy(['customer' => $customer], ['created' => 'DESC']);
	}

	/**
	 * @param Customer $customer
	 * @param int $orderId
	 * @return bool|Order
	 */
	public function getOrderDetail(Customer $customer, int $orderId)
	{
		$order = $this->orderRepository->findOneBy([
			'id' => $orderId,
			'customer' => $customer
		]);

		if ($order instanceof Order) {
			return $order;
		}

		return false;
	}
}