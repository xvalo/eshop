<?php

namespace App\Order\Pohoda\Entity;

use App\InvalidArgumentException;
use Doctrine\Common\Collections\ArrayCollection;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="pohoda_import")
 */
class Import
{
	const IMPORT_TYPE_INVOICE = 'I';
	const IMPORT_TYPE_BILL = 'B';
	const IMPORT_TYPE_ORDER = 'O';

	use Identifier;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $count;

	/**
	 * @ORM\ManyToMany(targetEntity="\App\Order\Order")
	 * @ORM\JoinTable(
	 *     name="pohoda_import_order",
	 *     joinColumns={@ORM\JoinColumn(name="import_id", referencedColumnName="id")},
	 *     inverseJoinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")}
	 * )
	 */
	private $orders;

	/**
	 * @ORM\Column(type="string")
	 * @var string
	 */
	private $type;

	/**
	 * @ORM\Column(type="datetime")
	 * @var \DateTime
	 */
	private $created;

	public function __construct(array $orders, string $type)
	{
		$this->orders = new ArrayCollection();
		$this->count = count($orders);
		$this->created = new \DateTime();
		$this->type = $type;

		if (!in_array($type, [self::IMPORT_TYPE_BILL, self::IMPORT_TYPE_INVOICE, self::IMPORT_TYPE_ORDER])) {
			throw new InvalidArgumentException('Wrong import type');
		}

		foreach ($orders as $order) {
			$this->orders->add($order);
		}
	}

	/**
	 * @return int
	 */
	public function getCount(): int
	{
		return $this->count;
	}

	/**
	 * @return array
	 */
	public function getOrders(): array
	{
		return $this->orders;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreated(): \DateTime
	{
		return $this->created;
	}
}