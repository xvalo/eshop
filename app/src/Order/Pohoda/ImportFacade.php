<?php

namespace App\Order\Pohoda;

use App\File\OrderDocumentType;
use App\Order\Order;
use App\Order\Pohoda\Entity\Import;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class ImportFacade
{
	use SmartObject;

	private $em;

	public function __construct(EntityManager $entityManager)
	{
		$this->em = $entityManager;
	}

	public function getImportedOrdersIds(string $type)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('O.id')
			->from(Import::class, 'I')
			->join('I.orders', 'O')
			->where(
				$qb->expr()->andX(
					$qb->expr()->eq('I.type', ':type')
				)
			)
			->setParameters([
				'type' => $type
			]);

		return $qb->getQuery()->getArrayResult();
	}

	public function getOrdersForInvoiceImport(array $excludedOrdersIds)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('O')
			->from(Order::class, 'O')
			->join('O.documents', 'D');

		$where = $qb->expr()->andX(
			$qb->expr()->isNull('O.canceled'),
			$qb->expr()->eq('D.deleted', ':documentDeleted'),
			$qb->expr()->eq('D.type', ':documentType')
		);

		$parameters = [
			'documentDeleted' => false,
			'documentType' => OrderDocumentType::INVOICE
		];

		if (!empty($excludedOrdersIds)) {
			$where->add(
				$qb->expr()->notIn('O.id', ':excluded')
			);

			$parameters['excluded'] = $excludedOrdersIds;
		}

		$qb->where($where)
			->setParameters($parameters);

		return $qb->getQuery()->execute();
	}

	public function getOrdersForBillImport(array $excludedOrdersIds)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('O')
			->from(Order::class, 'O')
			->join('O.documents', 'D');

		$where = $qb->expr()->andX(
			$qb->expr()->isNull('O.canceled'),
			$qb->expr()->eq('D.deleted', ':documentDeleted'),
			$qb->expr()->eq('D.type', ':documentType')
		);

		$parameters = [
			'documentDeleted' => false,
			'documentType' => OrderDocumentType::BILL
		];

		if (!empty($excludedOrdersIds)) {
			$where->add(
				$qb->expr()->notIn('O.id', ':excluded')
			);

			$parameters['excluded'] = $excludedOrdersIds;
		}

		$qb->where($where)
			->setParameters($parameters);

		return $qb->getQuery()->execute();
	}

	public function getOrdersForImport(array $excludedOrdersIds)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('O')
			->from(Order::class, 'O');

		$where = $qb->expr()->andX(
			$qb->expr()->isNull('O.canceled')
		);

		$parameters = [];

		if (!empty($excludedOrdersIds)) {
			$where->add(
				$qb->expr()->notIn('O.id', ':excluded')
			);

			$parameters['excluded'] = $excludedOrdersIds;
		}

		$qb->where($where)
			->setParameters($parameters);

		return $qb->getQuery()->execute();
	}

	public function setOrdersImported(array $orders, string $type)
	{
		$import = new Import($orders, $type);
		$this->em->persist($import);
		$this->em->flush();
	}
}