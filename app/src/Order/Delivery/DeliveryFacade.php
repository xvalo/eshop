<?php

namespace App\Order\Delivery;

use App\Category\Entity\Category;
use App\Lists\Delivery;
use App\Order\Delivery\Entity\DeliveryInCategoryAllowed;
use App\Order\Delivery\Entity\DeliveryPaymentType;
use App\Order\Payment\Entity\PaymentType;
use Kdyby\Doctrine\Dql\Join;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\QueryBuilder;

class DeliveryFacade
{
	/** @var EntityManager */
	private $em;

	/** @var \Kdyby\Doctrine\EntityRepository */
	private $deliveryRepository;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$this->deliveryRepository = $this->em->getRepository(Delivery::class);
	}

	/**
	 * @param int $id
	 * @return null|Delivery
	 */
	public function getDelivery(int $id)
	{
		return $this->deliveryRepository->find($id);
	}

	public function getDeliveries(array $where = [], array $orderBy = [])
	{
		return $this->deliveryRepository->findBy($where, $orderBy);
	}

	public function getList()
	{
		return $this->deliveryRepository->findPairs(['deleted' => 0], 'name');
	}

	/**
	 * @return QueryBuilder
	 */
	public function getGridSource(): QueryBuilder
	{
		return $this->deliveryRepository->createQueryBuilder('D')
			->where('D.deleted = :deleted')
			->orderBy('D.priority')
			->setParameter('deleted', false);
	}

	/**
	 * @param Delivery $delivery
	 * @throws \Exception
	 */
	public function saveDelivery(Delivery $delivery)
	{
		if (!$delivery->getId()) {
			$this->em->persist($delivery);
		}

		$this->em->flush();
	}

	public function saveDeliveryToCategory(DeliveryInCategoryAllowed $entity)
	{
		if (!$entity->getId()) {
			$this->em->persist($entity);
		}

		$this->em->flush();
	}

	public function removeDeliveriesFromCategory(Category $category)
	{
		foreach( $this->em->getRepository(DeliveryInCategoryAllowed::class)->findBy(['category' => $category]) as $entity) {
			$this->em->remove($entity);
		}
		$this->em->flush();
	}

	public function removePaymentTypeFromDelivery(Delivery $delivery, PaymentType $paymentType)
	{
		$deliveryPaymentType = $this->em->getRepository(DeliveryPaymentType::class)->findOneBy(['delivery' => $delivery, 'paymentType' => $paymentType]);
		$this->em->remove($deliveryPaymentType);
		$this->em->flush();
	}

	public function getHighestPriority()
	{
		return $this->deliveryRepository->createQueryBuilder('D')
					->select('MAX(D.priority)')
					->where('D.deleted = 0')
					->getQuery()->getSingleScalarResult();
	}

	public function getDeliveriesAllowedToCategory(Category $category)
	{
		$qb = $this->em->createQueryBuilder()
				->select('D')
				->from(Delivery::class, 'D')
				->join(DeliveryInCategoryAllowed::class, 'DC', Join::WITH, 'DC.delivery = D');

		$qb->where(
			$qb->expr()->eq('DC.category', ':category')
		);

		$qb->orderBy('D.priority');

		$qb->setParameter('category', $category);

		return $qb->getQuery()->execute();
	}
}