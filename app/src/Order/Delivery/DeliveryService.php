<?php

namespace App\Order\Delivery;

use App\Category\Entity\Category;
use App\Lists\Delivery;
use App\Order\Delivery\Entity\DeliveryInCategoryAllowed;
use App\Order\OrderLine\ProductOrderLine;
use App\Order\Payment\Entity\PaymentType;
use App\Order\Payment\PaymentTypeFacade;

class DeliveryService
{
	/** @var DeliveryFacade */
	private $deliveryFacade;

	/** @var PaymentTypeFacade */
	private $paymentTypeFacade;

	public function __construct(
		DeliveryFacade $deliveryFacade,
		PaymentTypeFacade $paymentTypeFacade
	){
		$this->deliveryFacade = $deliveryFacade;
		$this->paymentTypeFacade = $paymentTypeFacade;
	}

	/**
	 * @param int $id
	 * @return null|Delivery
	 */
	public function getDelivery(int $id)
	{
		return $this->deliveryFacade->getDelivery((int)$id);
	}

	public function getDeliveries()
	{
		return $this->deliveryFacade->getDeliveries(['deleted' => 0], ['priority' => 'ASC']);
	}

	public function getDeliveriesList()
	{
		return $this->deliveryFacade->getList();
	}

	/**
	 *
	 * @param Category[] $categories
	 * @param float $cartPrice
	 * @return array
	 */
	public function getCartAvailableDeliveries(array $categories, float $cartPrice): array
	{
		$list = [];
		$allowedDeliveries = [];
		/** @var Delivery[] $mostWeightDelivery */
		$mostWeightDelivery = [];

		/** @var Category $category */
		foreach ($categories as $category) {
			$deliveries = $this->deliveryFacade->getDeliveriesAllowedToCategory($category);

			/** @var Delivery $delivery */
			foreach ($deliveries as $delivery) {
				if (empty($mostWeightDelivery) || $delivery->getWeight() >= current($mostWeightDelivery)->getWeight()) {
					$mostWeightDelivery = [ $delivery ];
				} elseif (!empty($mostWeightDelivery) && $delivery->getWeight() == current($mostWeightDelivery)->getWeight()) {
					$mostWeightDelivery[] = $delivery;
				}
			}

			if (empty($allowedDeliveries)) {
				$allowedDeliveries = $deliveries;
				continue;
			}

			$allowedDeliveries = array_uintersect($allowedDeliveries, $deliveries, function($d1, $d2) {
				return $d1->getId() === $d2->getId() ? 0 : -1;
			});

		}

		$allowedDeliveries += $mostWeightDelivery;


		/** @var Delivery $delivery */
		foreach ($allowedDeliveries as $delivery) {
			$list[$delivery->getId()] = [
				'name' => $delivery->getName(),
				'price' => ($delivery->getFreePriceLimit() > 0 && $delivery->getFreePriceLimit() <= $cartPrice) ? 0 : $delivery->getPrice(),
				'hasFreePrice' => ($delivery->getFreePriceLimit() > 0),
				'freePriceLimit' => $delivery->getFreePriceLimit()
			];
		}

		return $list;
	}

	/**
	 * @param Delivery $delivery
	 * @param PaymentType|int $paymentType
	 * @param float $price
	 * @throws \Exception
	 */
	public function setPaymentTypeToDelivery(Delivery $delivery, $paymentType, float $price = 0)
	{
		if ( !($paymentType instanceof PaymentType) ) {
			$paymentType = $this->paymentTypeFacade->getPaymentType((int) $paymentType);
		}

		if ($delivery->isPaymentTypeAttached($paymentType)) {
			$delivery->changePriceForPaymentType($paymentType, $price);
		} else {
			$delivery->addPaymentType($paymentType, $price);
		}
		$this->deliveryFacade->saveDelivery($delivery);
	}

	public function removePaymentTypeFromDelivery(Delivery $delivery, $paymentType)
	{
		if ( !($paymentType instanceof PaymentType) ) {
			$paymentType = $this->paymentTypeFacade->getPaymentType((int) $paymentType);
		}

		if ($delivery->isPaymentTypeAttached($paymentType)) {
			$this->deliveryFacade->removePaymentTypeFromDelivery($delivery, $paymentType);
		}
	}

	public function sortPriorities(int $itemId, int $prevItemId, int $nextItemId)
	{
		/** @var Delivery $item */
		$item = $this->deliveryFacade->getDelivery((int)$itemId);

		/**
		 * 1, Find out order of item BEFORE current item
		 */
		/** @var Delivery $previousItem */
		$previousItem = (!$prevItemId) ? null : $this->deliveryFacade->getDelivery((int)$prevItemId);

		/**
		 * 2, Find out order of item AFTER current item
		 */
		/** @var Delivery $nextItem */
		$nextItem = (!$nextItemId) ? null : $this->deliveryFacade->getDelivery((int)$nextItemId);

		/**
		 * 3, Find all items that have to be moved one position up
		 */
		$itemsToMoveUp = $this->deliveryFacade->getDeliveries([
			'priority <=' => ($previousItem ? $previousItem->getPriority() : 0),
			'priority >' => $item->getPriority()
		]);

		/** @var Delivery $delivery */
		foreach ($itemsToMoveUp as $delivery) {
			$delivery->setPriority($delivery->getPriority() - 1);
		}

		/**
		 * 3, Find all items that have to be moved one position down
		 */

		$itemsToMoveDown = $this->deliveryFacade->getDeliveries([
			'priority >=' => ($nextItem ? $nextItem->getPriority() : 0),
			'priority <' => $item->getPriority()
		]);

		/** @var Delivery $delivery */
		foreach ($itemsToMoveDown as $delivery) {
			$delivery->setPriority($delivery->getPriority() + 1);
		}

		/**
		 * Update current item order
		 */
		if ($previousItem) {
			$item->setPriority($previousItem->getPriority() + 1);
		} else if ($nextItem) {
			$item->setPriority($nextItem->getPriority() - 1);
		} else {
			$item->setPriority(1);
		}

		$this->deliveryFacade->saveDelivery($item);
	}

	/**
	 * @param ProductOrderLine[] $products
	 * @param Delivery $delivery
	 * @return bool
	 */
	public function isOrderAvailableForFreeDelivery($products, Delivery $delivery): bool
	{
		if ($delivery->getFreePriceLimit() <= 0) {
			return false;
		}

		$totalPrice = 0;

		/** @var ProductOrderLine $product */
		foreach ($products as $product) {
			$totalPrice += $product->getCount() * $product->getUnitPrice();
		}

		return $totalPrice > $delivery->getFreePriceLimit();
	}

	public function allowDeliveryToCategory(Delivery $delivery, Category $category)
	{
		$this->deliveryFacade->saveDeliveryToCategory(new DeliveryInCategoryAllowed($delivery, $category));
	}

	public function removeDeliveriesFromCategory(Category $category)
	{
		$this->deliveryFacade->removeDeliveriesFromCategory($category);
	}

	public function getCategoryAllowedDeliveryIds(Category $category)
	{
		return array_map(function($item) {
			return $item->getId();
		},$this->deliveryFacade->getDeliveriesAllowedToCategory($category));
	}
}