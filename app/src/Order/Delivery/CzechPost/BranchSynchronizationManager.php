<?php

namespace App\Order\Delivery\CzechPost;

use App\Order\Delivery\CzechPostFacade;
use App\Order\Delivery\Entity\CzechPostBranch;
use Nette\DI\Container;
use Nette\SmartObject;

class BranchSynchronizationManager
{
	use SmartObject;

	/** @var CzechPostFacade */
	private $facade;

	/** @var string */
	private $sourceXml;

	public function __construct(
		CzechPostFacade $facade,
		Container $container
	){
		$this->facade = $facade;
		$this->sourceXml = $container->getParameters()['delivery']['czechPost']['branchesSource'];
	}

	public function run()
	{
		ini_set('memory_limit', '-1');
		$xml = simplexml_load_file($this->sourceXml, null, LIBXML_NOCDATA);
		$json = json_encode($xml);
		$data = json_decode($json, true);

		if (is_array($data['row'])) {
			foreach($data['row'] as $item) {
				if ($branch = $this->facade->getBranchByZip($item['PSC'])) {
					$branch->setZip($item['PSC']);
					$branch->setName($item['NAZEV']);
					$branch->setAddress($item['ADRESA']);
					$branch->setCity($item['OBEC']);
					$branch->setCityPart($item['C_OBCE']);
				} else {
					$branch = new CzechPostBranch(
						$item['PSC'],
						$item['NAZEV'],
						$item['ADRESA'],
						$item['OBEC'],
						$item['C_OBCE']
					);
				}

				$this->facade->saveBranch($branch);
			}
		}

		return true;
	}
}