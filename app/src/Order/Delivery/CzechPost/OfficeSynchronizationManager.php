<?php

namespace App\Order\Delivery\CzechPost;

use App\Order\Delivery\CzechPostFacade;
use App\Order\Delivery\Entity\CzechPostOffice;
use Nette\DI\Container;
use Nette\SmartObject;

class OfficeSynchronizationManager
{
	use SmartObject;

	/** @var CzechPostFacade */
	private $facade;

	/** @var string */
	private $sourceXml;

	public function __construct(
		CzechPostFacade $facade,
		Container $container
	){
		$this->facade = $facade;
		$this->sourceXml = $container->getParameters()['delivery']['czechPost']['officesSource'];
	}

	public function run()
	{
		ini_set('memory_limit', '-1');
		$xml = simplexml_load_file($this->sourceXml, null, LIBXML_NOCDATA);
		$json = json_encode($xml);
		$data = json_decode($json, true);

		if (is_array($data['row'])) {
			foreach($data['row'] as $item) {
				if ($office = $this->facade->getOfficeByZip($item['PSC'])) {
					$office->setZip($item['PSC']);
					$office->setName($item['NAZ_PROV']);
					$office->setDistrict($item['OKRES']);
					$office->setAddress($item['ADRESA']);
					$office->setCity($item['OBEC']);
					$office->setCityPart($item['C_OBCE']);
				} else {
					$office = new CzechPostOffice(
						$item['PSC'],
						$item['NAZ_PROV'],
						$item['OKRES'],
						$item['ADRESA'],
						$item['OBEC'],
						$item['C_OBCE']
					);
				}

				$this->facade->saveOffice($office);
				unset($office, $item);
			}
		}

		return true;
	}
}