<?php

namespace App\Order\Delivery\Entity;

use App\Lists\Delivery;
use App\Model\BaseEntity;
use App\Order\Payment\Entity\PaymentType;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="delivery2payment_type",
 *     uniqueConstraints={
 *        @ORM\UniqueConstraint(name="delivery_payment_unique",
 *            columns={"delivery_id", "payment_type_id"})
 *    }
 * )
 */
class DeliveryPaymentType extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Lists\Delivery", inversedBy="paymentTypes")
	 * @ORM\JoinColumn(name="delivery_id", referencedColumnName="id")
	 * @var Delivery
	 */
	private $delivery;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Order\Payment\Entity\PaymentType", inversedBy="deliveries")
	 * @ORM\JoinColumn(name="payment_type_id", referencedColumnName="id")
	 * @var PaymentType
	 */
	private $paymentType;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
	private $price;

	public function __construct(Delivery $delivery, PaymentType $paymentType, float $price)
	{
		$this->delivery = $delivery;
		$this->paymentType = $paymentType;
		$this->price = $price;
	}

	/**
	 * @return Delivery
	 */
	public function getDelivery(): Delivery
	{
		return $this->delivery;
	}

	/**
	 * @return PaymentType
	 */
	public function getPaymentType(): PaymentType
	{
		return $this->paymentType;
	}

	/**
	 * @return float
	 */
	public function getPrice(): float
	{
		return $this->price;
	}

	/**
	 * @param float $price
	 */
	public function setPrice(float $price)
	{
		$this->price = $price;
	}
}