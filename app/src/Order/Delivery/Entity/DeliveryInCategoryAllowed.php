<?php

namespace App\Order\Delivery\Entity;

use App\Category\Entity\Category;
use App\Lists\Delivery;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="delivery_category_allowed",
 *     uniqueConstraints={
 *        @ORM\UniqueConstraint(name="delivery_category_unique",
 *            columns={"delivery_id", "category_id"})
 *    }
 * )
 */
class DeliveryInCategoryAllowed
{
	use Identifier;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Lists\Delivery")
	 * @ORM\JoinColumn(name="delivery_id", referencedColumnName="id")
	 * @var Delivery
	 */
	private $delivery;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Category\Entity\Category")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
	 * @var Delivery
	 */
	private $category;

	public function __construct(Delivery $delivery, Category $category)
	{
		$this->delivery = $delivery;
		$this->category = $category;
	}

	/**
	 * @return Delivery
	 */
	public function getDelivery(): Delivery
	{
		return $this->delivery;
	}

	/**
	 * @return Delivery
	 */
	public function getCategory(): Delivery
	{
		return $this->category;
	}
}