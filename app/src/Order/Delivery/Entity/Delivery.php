<?php

namespace App\Lists;

use App\InvalidArgumentException;
use App\Model\BaseEntity;
use App\Order\Delivery\Entity\DeliveryPaymentType;
use App\Order\Payment\Entity\PaymentType;
use App\Traits\IRemovable;
use App\Traits\RemovableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Collections\ReadOnlyCollectionWrapper;

/**
 * @ORM\Entity
 */
class Delivery extends BaseEntity implements IRemovable
{
    use RemovableTrait;

    const TRACK_CODE_SUBSTRING = '[[code]]';

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", scale=2, precision=10)
     * @var float
     */
    private $price;

	/**
	 * @ORM\Column(type="decimal", scale=2, precision=10)
	 * @var float
	 */
    private $freePriceLimit;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @var string|null
	 */
    private $trackUrl;

    /**
     * @ORM\Column(type="integer", length=5)
     * @var int
     */
    private $priority;

	/**
	 * @ORM\OneToMany(targetEntity="\App\Order\Delivery\Entity\DeliveryPaymentType", indexBy="paymentType", mappedBy="delivery", cascade={"persist"})
	 */
    private $paymentTypes;

    /**
     * @ORM\Column(type="integer", length=5)
     * @var int
     */
    private $weight;

	/**
	 * @param string $name
	 * @param float $price
	 * @param int $priority
	 * @param float $freePriceLimit
	 * @param string|null $trackUrl
	 * @param int $weight
	 */
    public function __construct(string $name, float $price, int $priority, float $freePriceLimit = 0, string $trackUrl = null, int $weight = 0)
    {
        $this->name = $name;
        $this->price = $price;
	    $this->freePriceLimit = $freePriceLimit;
	    $this->trackUrl = $trackUrl;
	    $this->paymentTypes = new ArrayCollection();
	    $this->priority = $priority;
	    $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority(int $priority)
    {
        $this->priority = $priority;
    }

	/**
	 * @return float
	 */
	public function getFreePriceLimit(): float
	{
		return $this->freePriceLimit;
	}

	/**
	 * @param float $freePriceLimit
	 */
	public function setFreePriceLimit(float $freePriceLimit)
	{
		$this->freePriceLimit = $freePriceLimit;
	}

	/**
	 * @return string|null
	 */
	public function getTrackUrl()
	{
		return $this->trackUrl;
	}

	/**
	 * @param string|null $trackUrl
	 */
	public function setTrackUrl(string $trackUrl = null)
	{
		$this->trackUrl = $trackUrl;
	}

	/**
	 * @return int
	 */
	public function getWeight(): int
	{
		return $this->weight;
	}

	/**
	 * @param int $weight
	 */
	public function setWeight(int $weight)
	{
		$this->weight = $weight;
	}

	public function getPaymentTypes()
	{
		return new ReadOnlyCollectionWrapper($this->paymentTypes);
	}

	/**
	 * @param bool $applyFreeDelivery
	 * @param float $productsPrice
	 * @return array
	 */
	public function getPaymentTypesList(bool $applyFreeDelivery = false, float $productsPrice = 0)
	{
		$list = [];

		/** @var DeliveryPaymentType $item */
		foreach ($this->getPaymentTypes()->toArray() as $item) {
			$list[$item->getPaymentType()->getId()] = [
				'id' => $item->getPaymentType()->getId(),
				'name' => $item->getPaymentType()->getName(),
				'price' => $applyFreeDelivery && ($this->getFreePriceLimit() > 0 && $this->getFreePriceLimit() <= $productsPrice) ? 0 : $item->getPrice()
			];
		}

		return $list;
	}

	public function addPaymentType(PaymentType $paymentType, float $price = 0)
	{
		if ($this->isPaymentTypeAttached($paymentType)) {
			throw new InvalidArgumentException('Given Payment Type is already attached to the Delivery');
		}

		$this->paymentTypes->add(new DeliveryPaymentType($this, $paymentType, $price));
	}

	public function removePaymentType(PaymentType $paymentType)
	{
		if (!$this->isPaymentTypeAttached($paymentType)) {
			throw new InvalidArgumentException('Given Payment Type is not attached to the Delivery');
		}

		$toRemove = $this->getItemByPaymentType($paymentType);
		$this->paymentTypes->removeElement($toRemove);
	}

	public function changePriceForPaymentType(PaymentType $paymentType, float $price = 0)
	{
		if (!$this->isPaymentTypeAttached($paymentType)) {
			throw new InvalidArgumentException('Given Payment Type is not attached to the Delivery');
		}

		/** @var DeliveryPaymentType $item */
		$item = $this->getItemByPaymentType($paymentType);
		$item->setPrice($price);
	}

	/**
	 * @param PaymentType $paymentType
	 * @return bool
	 */
	public function isPaymentTypeAttached(PaymentType $paymentType)
	{
		return $this->paymentTypes->exists(function($id, $item) use ($paymentType) {
			return $item->getPaymentType()->getId() == $paymentType->getId();
		});
	}

	/**
	 * @param PaymentType $paymentType
	 * @return float
	 */
	public function getPaymentTypePrice(PaymentType $paymentType)
	{
		$item = $this->getItemByPaymentType($paymentType);
		return $item ? $item->getPrice() : 0;
	}

	/**
	 * @param PaymentType $paymentType
	 * @return DeliveryPaymentType
	 */
	private function getItemByPaymentType(PaymentType $paymentType)
	{
		return $this->paymentTypes->filter(function ($item) use ($paymentType) {
			return $item->getPaymentType()->getId() == $paymentType->getId();
		})->first();
	}

}