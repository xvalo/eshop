<?php

namespace App\Order\Delivery\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table{
 *  name="cp_post_office",
 *  uniqueConstraints={@ORM\UniqueConstraint(name="zip_code", columns={"zip"})}
 * }
 */
class CzechPostOffice
{
	use Identifier;

	/**
	 * @ORM\Column(type="string", length=6)
	 * @var string
	 */
	private $zip;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $district;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $address;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $city;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $cityPart;

	public function __construct(string $zip, string $name, string $district, string $address, string $city, string $cityPart)
	{
		$this->zip = $zip;
		$this->name = $name;
		$this->district = $district;
		$this->address = $address;
		$this->city = $city;
		$this->cityPart = $cityPart;
	}

	/**
	 * @return string
	 */
	public function getZip(): string
	{
		return $this->zip;
	}

	/**
	 * @param string $zip
	 */
	public function setZip(string $zip)
	{
		$this->zip = $zip;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getDistrict(): string
	{
		return $this->district;
	}

	/**
	 * @param string $district
	 */
	public function setDistrict(string $district)
	{
		$this->district = $district;
	}

	/**
	 * @return string
	 */
	public function getAddress(): string
	{
		return $this->address;
	}

	/**
	 * @param string $address
	 */
	public function setAddress(string $address)
	{
		$this->address = $address;
	}

	/**
	 * @return string
	 */
	public function getCity(): string
	{
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity(string $city)
	{
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getCityPart(): string
	{
		return $this->cityPart;
	}

	/**
	 * @param string $cityPart
	 */
	public function setCityPart(string $cityPart)
	{
		$this->cityPart = $cityPart;
	}
}