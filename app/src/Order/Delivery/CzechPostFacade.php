<?php

namespace App\Order\Delivery;

use App\Order\Delivery\Entity\CzechPostBranch;
use App\Order\Delivery\Entity\CzechPostOffice;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class CzechPostFacade
{
	use SmartObject;

	/** @var EntityManager */
	private $em;

	private $branchRepository;

	private $officeRepository;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$this->branchRepository = $em->getRepository(CzechPostBranch::class);
		$this->officeRepository = $em->getRepository(CzechPostOffice::class);
	}

	/**
	 * @param CzechPostOffice $office
	 * @throws \Exception
	 */
	public function saveOffice(CzechPostOffice $office)
	{
		if ($office->getId() === null) {
			$this->em->persist($office);
		}

		$this->em->flush();
	}

	/**
	 * @param CzechPostBranch $branch
	 * @throws \Exception
	 */
	public function saveBranch(CzechPostBranch $branch)
	{
		if ($branch->getId() === null) {
			$this->em->persist($branch);
		}

		$this->em->flush();
	}

	/**
	 * @param int $id
	 * @return CzechPostBranch|null
	 */
	public function getBranchById(int $id)
	{
		return $this->branchRepository->find($id);
	}

	/**
	 * @param string $zip
	 * @return CzechPostBranch|null
	 */
	public function getBranchByZip(string $zip)
	{
		return $this->branchRepository->findOneBy(['zip' => $zip]);
	}


	/**
	 * @param int $id
	 * @return null|CzechPostOffice
	 */
	public function getOfficeById(int $id)
	{
		return $this->officeRepository->find($id);
	}

	/**
	 * @param string $zip
	 * @return CzechPostOffice|null
	 */
	public function getOfficeByZip(string $zip)
	{
		return $this->officeRepository->findOneBy(['zip' => $zip]);
	}

	public function getBranchByQueryString(string $queryString)
	{
		$qb = $this->branchRepository->createQueryBuilder('B');
		$qb->where(
			$qb->expr()->orX(
				$qb->expr()->like('B.zip', ':query'),
				$qb->expr()->like('B.address', ':query')
			)
		)->setParameter('query', '%'.$queryString.'%')
			->orderBy('B.city', 'ASC')
			->addOrderBy('B.cityPart', 'ASC');

		return $qb->getQuery()->getArrayResult();
	}

	public function getOfficeByQueryString(string $queryString)
	{
		$qb = $this->officeRepository->createQueryBuilder('O');
		$qb->where(
			$qb->expr()->orX(
				$qb->expr()->like('O.zip', ':query'),
				$qb->expr()->like('O.city', ':query'),
				$qb->expr()->like('O.cityPart', ':query')
			)
		)->setParameter('query', '%'.$queryString.'%')
		->orderBy('O.city', 'ASC')
		->addOrderBy('O.cityPart', 'ASC');

		return $qb->getQuery()->getArrayResult();
	}
}