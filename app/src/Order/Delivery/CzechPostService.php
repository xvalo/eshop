<?php

namespace App\Order\Delivery;

use Nette\SmartObject;

class CzechPostService
{
	use SmartObject;

	/** @var CzechPostFacade */
	private $czechPostFacade;

	public function __construct(CzechPostFacade $czechPostFacade)
	{
		$this->czechPostFacade = $czechPostFacade;
	}

	public function getBox(int $id)
	{
		return $this->czechPostFacade->getBranchById($id);
	}

	public function getOffice(int $id)
	{
		return $this->czechPostFacade->getOfficeById($id);
	}

	public function getBoxes(string $queryString)
	{
		$branches = [];

		foreach ($this->czechPostFacade->getBranchByQueryString($queryString) as $branch) {
			$branches[$branch['id']] = $branch['address'];
		}

		return $branches;
	}

	public function getOffices(string $queryString)
	{
		$offices = [];

		foreach ($this->czechPostFacade->getOfficeByQueryString($queryString) as $office) {
			$offices[$office['id']] = $office['address'];
		}

		return $offices;
	}
}