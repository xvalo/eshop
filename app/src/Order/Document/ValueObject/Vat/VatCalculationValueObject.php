<?php

namespace App\Order\Document\ValueObject\Vat;

use App\InvalidArgumentException;
use App\Utils\Vat;

class VatCalculationValueObject
{
	/** @var ItemCalculation */
	private $vat0Calculation;

	/** @var ItemCalculation */
	private $vat10Calculation;

	/** @var ItemCalculation */
	private $vat15Calculation;

	/** @var ItemCalculation */
	private $vat21Calculation;

	public function __construct()
	{
		$this->vat0Calculation = new ItemCalculation();
		$this->vat10Calculation = new ItemCalculation();
		$this->vat15Calculation = new ItemCalculation();
		$this->vat21Calculation = new ItemCalculation();
	}

	/**
	 * @param int $rate
	 * @return ItemCalculation
	 */
	public function getVatCalculation(int $rate): ItemCalculation
	{
		$availableLevels = [
			Vat::TYPE_0,
			Vat::TYPE_10,
			Vat::TYPE_15,
			Vat::TYPE_21
		];

		if (!in_array($rate, $availableLevels)) {
			throw new InvalidArgumentException('VAT level is not correct');
		}

		return $this->{'vat'.$rate.'Calculation'};
	}

	/**
	 * @param int $rate
	 * @param float $baseChange
	 * @param float $vatChange
	 */
	public function updateVatCalculation(int $rate, float $baseChange, float $vatChange)
	{
		$this->getVatCalculation($rate)->addValue($baseChange, $vatChange);
	}

	/**
	 * @return array
	 */
	public function getAllVatCalculations(): array
	{
		return [
			Vat::TYPE_0 => $this->getVatCalculation(Vat::TYPE_0),
			Vat::TYPE_10 => $this->getVatCalculation(Vat::TYPE_10),
			Vat::TYPE_15 => $this->getVatCalculation(Vat::TYPE_15),
			Vat::TYPE_21 => $this->getVatCalculation(Vat::TYPE_21)
		];
	}
}