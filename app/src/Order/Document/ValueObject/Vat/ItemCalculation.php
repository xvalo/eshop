<?php

namespace App\Order\Document\ValueObject\Vat;

class ItemCalculation
{
	/** @var float */
	private $base;

	/** @var float */
	private $vat;

	public function __construct(float $base = 0.0, float $vat = 0.0)
	{
		$this->base = $base;
		$this->vat = $vat;
	}

	/**
	 * @return float
	 */
	public function getBase(): float
	{
		return $this->base;
	}

	/**
	 * @return float
	 */
	public function getVat(): float
	{
		return $this->vat;
	}

	/**
	 * @param float $base
	 * @param float $vat
	 */
	public function addValue(float $base, float $vat)
	{
		$this->base += $base;
		$this->vat += $vat;
	}

	/**
	 * @return float
	 */
	public function getTotalValue(): float
	{
		return $this->getBase() + $this->getVat();
	}
}