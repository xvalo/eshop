<?php

namespace App\Order\Document\ValueObject;

use App\Order\Order;

class OrderInfoValueObject
{
	/** @var string */
	private $number;

	/** @var Order */
	private $order;

	public function __construct(string $number, Order $order)
	{
		$this->number = $number;
		$this->order = $order;
	}

	/**
	 * @return string
	 */
	public function getNumber(): string
	{
		return $this->number;
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}
}