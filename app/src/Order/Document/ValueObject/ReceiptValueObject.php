<?php

namespace App\Order\Document\ValueObject;

use App\Order\Order;
use Doctrine\Common\Collections\Collection;

class ReceiptValueObject
{
	/** @var string */
	private $number;

	/** @var Order */
	private $order;

	/** @var Collection|null */
	private $changes = null;

	public function __construct(string $number, Order $order, Collection $changes = null)
	{
		$this->number = $number;
		$this->order = $order;
		$this->changes = $changes;
	}

	/**
	 * @return string
	 */
	public function getNumber(): string
	{
		return $this->number;
	}

	/**
	 * @return Order
	 */
	public function getOrder(): Order
	{
		return $this->order;
	}

	/**
	 * @return Collection|null
	 */
	public function getChanges()
	{
		return $this->changes;
	}
}