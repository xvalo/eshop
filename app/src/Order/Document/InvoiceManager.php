<?php

namespace App\Order\Document;

use App\Order\Document\ValueObject\ReceiptValueObject;

class InvoiceManager extends OrderDocumentBaseManager
{
	/**
	 * @param ReceiptValueObject $valueObject
	 * @return string
	 */
	public function createPDF(ReceiptValueObject $valueObject): string
	{
		$this->pdfManager->setTemplate(__DIR__.'/template/invoicePdf.latte');

		$dir = $this->baseDirectory . $valueObject->getOrder()->getId() . '/';

		if(!file_exists($dir)){
			mkdir($dir);
		}

		$orderLines = $this->processOrderLines($valueObject->getOrder()->getOrderLines());

		$this->pdfManager->setValue('documentTypeTitle', 'Faktura');
		$this->pdfManager->setValue('order', $valueObject->getOrder());
		$this->pdfManager->setValue('number', $valueObject->getNumber());
		$this->pdfManager->setValue('paymentFormName', $valueObject->getOrder()->getPaymentType()->getName());
		$this->pdfManager->setValue('orderLines', $orderLines);
		$this->pdfManager->setValue('totals', $this->getTotalsLine($orderLines));
		$this->pdfManager->setValue('totalVat', $this->totalVat);
		$this->pdfManager->setValue('vatCalculation', $this->vatCalculation->getAllVatCalculations());
		$this->setDocumentDates();
		$this->setDocumentBaseVariables();

		return $this->pdfManager->savePdf($dir, $valueObject->getNumber());
	}

}