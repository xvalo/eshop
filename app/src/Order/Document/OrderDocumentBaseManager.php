<?php

namespace App\Order\Document;

use App\Common\Document\PdfManager;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\Model\Services\ApplicationConfigurationService;
use App\Order\Document\ValueObject\Vat\VatCalculationValueObject;
use App\Order\OrderLine\DeliveryOrderLine;
use App\Order\OrderLine\ExtraItemOrderLine;
use App\Order\OrderLine\OrderLine;
use App\Order\OrderLine\OrderLineReturned;
use App\Order\OrderLine\ProductOrderLine;
use App\Utils\Vat;
use Doctrine\Common\Collections\Collection;
use Nette\SmartObject;
use Nette\Utils\DateTime;

abstract class OrderDocumentBaseManager
{
	use SmartObject;

	/** @var VatCalculationValueObject */
	protected $vatCalculation;

	/** @var float */
	protected $totalVat = 0;

	/** @var PdfManager */
	protected $pdfManager;

	/** @var string */
	protected $baseDirectory;

	/** @var ApplicationSettingsManager */
	protected $applicationSettingsManager;

	/** @var ApplicationConfigurationService */
	protected $applicationConfigurationService;

	public function __construct(
		string $baseDirectory,
		PdfManager $pdfManager,
		ApplicationSettingsManager $applicationSettingsManager,
		ApplicationConfigurationService $applicationConfigurationService
	){
		$this->baseDirectory = $baseDirectory;
		$this->pdfManager = $pdfManager;
		$this->vatCalculation = new VatCalculationValueObject();
		$this->applicationSettingsManager = $applicationSettingsManager;
		$this->applicationConfigurationService = $applicationConfigurationService;
	}

	protected function getOrderLine($name, $vatRate, $unitPrice, $quantity = 1, $isLineSubtracted = false)
	{
		$quantity = ($isLineSubtracted) ? (-1) * $quantity : $quantity;
		$totalLinePrice = $unitPrice * $quantity;
		$vatPrice = Vat::countVat($totalLinePrice, $vatRate);

		$orderLine =  [
			'name' => $name,
			'rate' => $vatRate,
			'totalPrice' => $totalLinePrice,
			'variantVat' => $vatPrice,
			'variantPriceWithoutVat' => $totalLinePrice - $vatPrice,
			'quantity' => $quantity,
			'unitPrice' => $unitPrice
		];

		$this->totalVat += $orderLine['variantVat'];
		$this->vatCalculation->updateVatCalculation($vatRate, $orderLine['variantPriceWithoutVat'], $orderLine['variantVat']);

		return $orderLine;
	}

	protected function getTotalsLine(array $orderLines)
	{
		$totals = [
			'totalWithoutVat' => 0,
			'vat' => 0,
			'totalPrice' => 0
		];

		foreach ($orderLines as $line) {
			$totals['totalWithoutVat'] += $line['variantPriceWithoutVat'];
			$totals['vat'] += $line['variantVat'];
			$totals['totalPrice'] += $line['totalPrice'];
		}

		return $totals;
	}

	protected function setDocumentDates()
	{
		$dateInvoiceCreated = new DateTime();
		$this->pdfManager->setValue('createdDate', $dateInvoiceCreated);
		$this->pdfManager->setValue('payDate', $dateInvoiceCreated->modifyClone('+14 days'));
	}

	protected function setDocumentBaseVariables()
	{
		$this->pdfManager->setValue('webTitle', $this->applicationSettingsManager->getWebsiteTitle());
		$this->pdfManager->setValue('mainEmail', $this->applicationSettingsManager->getMainEmail());
		$this->pdfManager->setValue('ico', $this->applicationSettingsManager->getIco());
		$this->pdfManager->setValue('dic', $this->applicationSettingsManager->getDic());
		$this->pdfManager->setValue('phone', $this->applicationSettingsManager->getContactPhone());
		$this->pdfManager->setValue('mainUrl', $this->applicationSettingsManager->getDomain());
		$this->pdfManager->setValue('bankAccountNumber', $this->applicationSettingsManager->getBankAccount());
		$this->pdfManager->setValue('footerText', $this->applicationConfigurationService->getDocumentFooterText());
		$this->pdfManager->setValue('contractorAddress', $this->applicationConfigurationService->getDocumentContractorAddress());
		$this->pdfManager->setValue('officeAddress', $this->applicationConfigurationService->getDocumentOfficeAddress());

		$this->pdfManager->setValue('shopID', $this->applicationConfigurationService->getShopId());
		$this->pdfManager->setValue('cashRegisterID', $this->applicationConfigurationService->getCashRegisterId());
	}

	protected function processOrderLines(Collection $orderLines, bool $isLineSubtracted = false): array
	{
		$processed = [];

		/** @var OrderLine $line */
		foreach ($orderLines as $line){
			$vat = Vat::TYPE_21;
			$title = (string)$line;

			if ($line instanceof ProductOrderLine) {
				$vat = $line->getPrice()->getVariant()->getProduct()->getVat();
				$title = $line->getTitle();
			} elseif ($line instanceof DeliveryOrderLine) {
				if (!$line->getDelivery()) {
					continue;
				}
			} elseif ($line instanceof OrderLineReturned) {
				$isLineSubtracted = true;
			} elseif ($line instanceof ExtraItemOrderLine) {
				$vat = $line->getVat();
			}

			$processed[] = $this->getOrderLine(
				$title,
				$vat,
				$line->getUnitPrice(),
				$line->getCount(),
				$isLineSubtracted
			);
		}

		return $processed;
	}

	protected function setValue($name, $value)
	{
		$this->pdfManager->setValue($name, $value);
	}

	protected function setTemplate($template)
	{
		$this->pdfManager->setTemplate($template);
	}
}