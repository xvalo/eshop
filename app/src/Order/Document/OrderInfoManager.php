<?php

namespace App\Order\Document;

use App\Order\Document\ValueObject\OrderInfoValueObject;

class OrderInfoManager extends OrderDocumentBaseManager
{
	/**
	 * @param OrderInfoValueObject $valueObject
	 * @return string
	 */
	public function createPDF(OrderInfoValueObject $valueObject): string
	{
		$this->pdfManager->setTemplate(__DIR__.'/template/orderInfoPdf.latte');

		$dir = $this->baseDirectory . $valueObject->getOrder()->getId() . '/';

		if(!file_exists($dir)){
			mkdir($dir);
		}

		$orderLines = $this->processOrderLines($valueObject->getOrder()->getOrderLines());

		$this->pdfManager->setValue('documentTypeTitle', 'Soupis objednávky');
		$this->pdfManager->setValue('order', $valueObject->getOrder());
		$this->pdfManager->setValue('number', $valueObject->getNumber());
		$this->pdfManager->setValue('paymentFormName', $valueObject->getOrder()->getPaymentType()->getName());
		$this->pdfManager->setValue('orderLines', $orderLines);
		$this->pdfManager->setValue('totals', $this->getTotalsLine($orderLines));
		$this->setDocumentDates();
		$this->setDocumentBaseVariables();

		return $this->pdfManager->savePdf($dir, $valueObject->getNumber());
	}

}