<?php

namespace App\Classes\Order;

use App\Classes\Order\Creation\OrderCreationService;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\File\OrderDocument;
use App\File\OrderDocumentType;
use App\Lists\SystemVariableRepository;
use App\Managers\BillManager;
use App\Model\Services\ApplicationConfigurationService;
use App\Order\Document\InvoiceCancellationManager;
use App\Order\Document\InvoiceCorrectionManager;
use App\Order\Document\InvoiceManager;
use App\Order\Document\OrderInfoManager;
use App\Order\Document\ValueObject\OrderInfoValueObject;
use App\Order\Document\ValueObject\ReceiptValueObject;
use App\Order\Order;
use Doctrine\Common\Collections\Collection;

class OrderDocumentService
{
	/** @var SystemVariableRepository */
	private $systemVariableRepository;

	/** @var ApplicationConfigurationService */
	private $applicationConfigurationService;

	/** @var InvoiceManager */
	private $invoiceManager;

	/** @var InvoiceCorrectionManager */
	private $invoiceCorrectionManager;

	/** @var InvoiceCancellationManager */
	private $invoiceCancellationManager;

	/** @var BillManager */
	private $billManager;

	/** @var OrderInfoManager */
	private $orderInfoManager;
	/**
	 * @var ApplicationSettingsManager
	 */
	private $applicationSettingsManager;
	/**
	 * @var OrderCreationService
	 */
	private $orderCreationService;

	public function __construct(
		SystemVariableRepository $systemVariableRepository,
		ApplicationConfigurationService $applicationConfigurationService,
		InvoiceManager $invoiceManager,
		InvoiceCorrectionManager $invoiceCorrectionManager,
		InvoiceCancellationManager $invoiceCancellationManager,
		BillManager $billManager,
		OrderInfoManager $orderInfoManager,
		ApplicationSettingsManager $applicationSettingsManager,
		OrderCreationService $orderCreationService
	){
		$this->systemVariableRepository = $systemVariableRepository;
		$this->applicationConfigurationService = $applicationConfigurationService;
		$this->invoiceManager = $invoiceManager;
		$this->invoiceCorrectionManager = $invoiceCorrectionManager;
		$this->invoiceCancellationManager = $invoiceCancellationManager;
		$this->billManager = $billManager;
		$this->orderInfoManager = $orderInfoManager;
		$this->applicationSettingsManager = $applicationSettingsManager;
		$this->orderCreationService = $orderCreationService;
	}
	
	public function createBillDocument(Order $order, string $documentNumber = null)
	{
		if (!$documentNumber) {
			$documentNumber = $this->applicationSettingsManager->getNextBillNumber();
		}

		$filePath = $this->billManager->createPDF(new ReceiptValueObject($documentNumber, $order));

		$order->addDocument(new OrderDocument($documentNumber, $order, $filePath, OrderDocumentType::BILL));
		$this->applicationSettingsManager->setNextBillNumber(++$documentNumber);

		$this->orderCreationService->updateStockItems($order, false);

		$this->systemVariableRepository->flush();
	}

	public function createInvoiceDocument(Order $order, string $documentNumber = null)
	{
		if (!$documentNumber) {
			$documentNumber = $this->applicationSettingsManager->getNextInvoiceNumber();
		}
		$filePath = $this->invoiceManager->createPDF(new ReceiptValueObject($documentNumber, $order));

		$order->addDocument(new OrderDocument($documentNumber, $order, $filePath, OrderDocumentType::INVOICE));
		$this->applicationSettingsManager->setNextInvoiceNumber(++$documentNumber);

		$this->orderCreationService->updateStockItems($order, false);

		$this->systemVariableRepository->flush();
	}

	public function createInvoiceCorrectionDocument(Order $order, Collection $orderChangedLines)
	{
		$documentNumber = $this->applicationSettingsManager->getNextInvoiceCorrectionNumber();
		$filePath = $this->invoiceCorrectionManager->createPDF(new ReceiptValueObject($documentNumber, $order, $orderChangedLines));

		$order->addDocument(new OrderDocument($documentNumber, $order, $filePath, OrderDocumentType::CORRECTIVE));
		$this->applicationSettingsManager->setNextInvoiceCorrectionNumber(++$documentNumber);

		$this->systemVariableRepository->flush();
	}

	public function createCancelDocument(Order $order)
	{
		$documentNumber = $this->applicationSettingsManager->getNextInvoiceNumber();
		$filePath = $this->invoiceCancellationManager->createPDF(new ReceiptValueObject($documentNumber, $order));

		$order->addDocument(new OrderDocument($documentNumber, $order, $filePath, OrderDocumentType::CANCEL));
		$this->applicationSettingsManager->setNextInvoiceNumber(++$documentNumber);

		$this->systemVariableRepository->flush();
	}

	public function createProductsListDocument(Order $order)
	{
		$documentNumber = 'OP-'.time();
		$filePath = $this->orderInfoManager->createPDF(new OrderInfoValueObject($documentNumber, $order));

		$order->addDocument(new OrderDocument($documentNumber, $order, $filePath, OrderDocumentType::PRODUCTS_LIST));

		$this->systemVariableRepository->flush();
	}
}