<?php

namespace App\Managers;

use App\Order\Document\OrderDocumentBaseManager;
use App\Order\Document\ValueObject\ReceiptValueObject;

class BillManager extends OrderDocumentBaseManager
{
	
	/**
	 * @param ReceiptValueObject $valueObject
	 * @return string
	 */
	public function createPDF(ReceiptValueObject $valueObject): string
	{
		$this->setTemplate(__DIR__.'/template/billPdf.latte');

		$dir = $this->baseDirectory . $valueObject->getOrder()->getId() . '/';

		if(!file_exists($dir)){
			mkdir($dir);
		}

		$orderLines = $this->processOrderLines($valueObject->getOrder()->getOrderLines());

		$this->setValue('order', $valueObject->getOrder());
		$this->setValue('number', $valueObject->getNumber());
		$this->setValue('orderLines', $orderLines);
		$this->setValue('totals', $this->getTotalsLine($orderLines));
		$this->setValue('totalVat', $this->totalVat);
		$this->setValue('vatCalculation', $this->vatCalculation->getAllVatCalculations());
		$this->setValue('createdDate', $valueObject->getOrder()->getLastProcessedEetReceipt()->getSendDate());
		$this->setDocumentBaseVariables();

		return $this->pdfManager->savePdf($dir, $valueObject->getNumber());
	}
}