<?php

namespace App\Order\Document;

use App\Order\Document\ValueObject\ReceiptValueObject;

class InvoiceCancellationManager extends OrderDocumentBaseManager
{
	/**
	 * @param ReceiptValueObject $valueObject
	 * @return string
	 */
	public function createPDF(ReceiptValueObject $valueObject): string
	{
		$this->setTemplate(__DIR__.'/template/invoicePdf.latte');

		$dir = $this->baseDirectory . $valueObject->getOrder()->getId() . '/';

		if(!file_exists($dir)){
			mkdir($dir);
		}

		$orderLines = $this->processOrderLines($valueObject->getOrder()->getOrderLines(),true);

		$this->setValue('documentTypeTitle', 'Faktura');
		$this->setValue('cancellation', true);
		$this->setValue('order', $valueObject->getOrder());
		$this->setValue('number', $valueObject->getNumber());
		$this->setValue('paymentFormName', $valueObject->getOrder()->getDelivery()->getName());
		$this->setValue('orderLines', $orderLines);
		$this->setValue('totals', $this->getTotalsLine($orderLines));
		$this->setValue('totalVat', $this->totalVat);
		$this->setValue('vatCalculation', $this->vatCalculation->getAllVatCalculations());
		$this->setDocumentDates();
		$this->setDocumentBaseVariables();

		return $this->pdfManager->savePdf($dir, $valueObject->getNumber());
	}

}