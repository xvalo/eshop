<?php

namespace App\Order\Heureka;

use App\Model\Services\ApplicationConfigurationService;
use App\Order\Order;
use App\Order\OrderLine\ProductOrderLine;
use Heureka\ShopCertification;
use Nette\SmartObject;
use Tracy\Debugger;

class OrderSender
{
	use SmartObject;

	private $shopCertificationObject;

	public function __construct(ApplicationConfigurationService $configurationService)
	{
		$this->shopCertificationObject = new ShopCertification($configurationService->getHeurekaAppId());
	}

	/**
	 * @param Order $order
	 */
	public function sendOrderLog(Order $order)
	{
		try {
			$this->shopCertificationObject->setEmail($order->getContactInformation()->getEmail());
			$this->shopCertificationObject->setOrderId((int)$order->getOrderNo());

			/** @var ProductOrderLine $productOrderLine */
			foreach ($order->getProducts() as $productOrderLine) {
				$variant = $productOrderLine->getPrice()->getVariant();
				$id = 'PN'.$variant->getProduct()->getNumber().'V'.$variant->getId();
				$this->shopCertificationObject->addProductItemId($id);
			}

			$this->shopCertificationObject->logOrder();
		} catch (\Exception $e) {
			Debugger::log($e, 'heureka_error');
		}
	}
}