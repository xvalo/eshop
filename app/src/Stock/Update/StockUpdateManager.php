<?php

namespace App\Stock\Update;

use App\Model\Services\ApplicationConfigurationService;
use App\Product\Availability\AvailabilityProvider;
use App\Stock\Supplier\Entity\Supplier;
use App\Stock\Update\Source\Download\SourceFileManager;
use App\Stock\Update\Source\History\SourceUpdateHistoryFacade;
use App\Stock\Update\Source\Reader\ReaderFactory;
use App\Stock\Update\Source\VariantStateUpdate\StateUpdateManager;
use Nette\SmartObject;
use Nette\Utils\Strings;

class StockUpdateManager
{
	use SmartObject;

	/**
	 * @var SourceFileManager
	 */
	private $sourceFileManager;

	/**
	 * @var ApplicationConfigurationService
	 */
	private $applicationConfigurationService;

	/**
	 * @var StateUpdateManager
	 */
	private $stateUpdateManager;

	/**
	 * @var ReaderFactory
	 */
	private $readerFactory;

	/**
	 * @var AvailabilityProvider
	 */
	private $availabilityProvider;

	/**
	 * @var SourceUpdateHistoryFacade
	 */
	private $sourceUpdateHistoryFacade;

	public function __construct(
		ApplicationConfigurationService $applicationConfigurationService,
		SourceFileManager $sourceFileManager,
		StateUpdateManager $stateUpdateManager,
		ReaderFactory $readerFactory,
		AvailabilityProvider $availabilityProvider,
		SourceUpdateHistoryFacade $sourceUpdateHistoryFacade
	){
		$this->sourceFileManager = $sourceFileManager;
		$this->applicationConfigurationService = $applicationConfigurationService;
		$this->stateUpdateManager = $stateUpdateManager;
		$this->readerFactory = $readerFactory;
		$this->availabilityProvider = $availabilityProvider;
		$this->sourceUpdateHistoryFacade = $sourceUpdateHistoryFacade;
	}

	public function initCaches()
	{
		$this->stateUpdateManager->initCodesToVariantsMapping();
		$this->stateUpdateManager->initUpdatesTable();
	}

	public function clearCaches()
	{
		$this->stateUpdateManager->clearCache();
	}

	public function synchronizeSupplier(Supplier $supplier)
	{
		if ($supplier->isSynchronized() === false) {
			return false;
		}

		$sourceFile = $this->prepareSupplierSourceFile( $supplier );

		$reader = $this->readerFactory->getReader($supplier);
		$reader->setDataSource($sourceFile);
		$reader->init();

		while ($item = $reader->read()) {
			$code = $this->getCodeWithPrefix($item->getCode(), $supplier);
			$variantId = $this->stateUpdateManager->getVariantByCode($code, $supplier);

			if ($variantId === null) {
				continue;
			}

			$this->stateUpdateManager->saveVariantChange($variantId, $supplier, $item->getAvailability(), $item->getPrice());
		}

		$this->sourceFileManager->removeSourceFile($sourceFile);
	}

	public function processChangedItems()
	{
		foreach($this->stateUpdateManager->getCurrentState() as $variant => $data) {
			if ((int)$data['availability'] === $this->availabilityProvider->getOnStockStatus()->getId()) {
				continue;
			}

			$supplierToSet = $minAvailability = $minPrice = null;

			foreach($data['supplierState'] as $supplier => $state) {
				$availabilityToSet = !empty($state)
					? $state['availability']
					: $this->availabilityProvider->getOutOfStockStatus()->getId();
				$priceToSet = !empty($state)
					? $state['price']
					: null;

				if (
					$minAvailability === null
					|| $minAvailability > $availabilityToSet
					|| ( $minAvailability === $availabilityToSet && $priceToSet !== null && $minPrice > $priceToSet )
				) {
					$minAvailability = $availabilityToSet;
					if ($data['isPriceSynchronized'][$supplier]) {
						$minPrice = $priceToSet;
					}
					$supplierToSet = $supplier;

					continue;
				}
			}

			$minPrice = $minPrice === null ? (float)$data['price'] : $minPrice;

			unset($data['isUpdated']);

			if ($minPrice !== (float)$data['price'] || $minAvailability !== (int)$data['availability'] ) {
				$this->sourceUpdateHistoryFacade->updateVariantPrice($variant, $data['priceId'], $minAvailability, $minPrice, $supplierToSet, json_encode($data));
			}
		}
	}

	/**
	 * @param string $code
	 * @param Supplier $supplier
	 * @return string
	 */
	protected function getCodeWithPrefix(string $code, Supplier $supplier): string
	{
		return ($supplier->getPrefix() && strlen($supplier->getPrefix()) > 0) ? $supplier->getPrefix() . '-' . $code : $code;
	}

	/**
	 * @param Supplier $supplier
	 * @return string
	 */
	private function prepareSupplierSourceFile(Supplier $supplier): string
	{
		$sourceTempDirectory = $this->applicationConfigurationService->getSupplierSourceFeedTempDirectory();
		$sourceDownloadedFileName = Strings::webalize($supplier->getName());
		$dataFilePath = $sourceTempDirectory . $sourceDownloadedFileName;

		return $this->sourceFileManager->downloadSourceFile($supplier->getSource(), $dataFilePath);
	}
}