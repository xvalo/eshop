<?php

namespace App\Stock\Update\Source\Download;

use App\FileNotFoundException;

class StockSourceUnavailableException extends FileNotFoundException
{

}