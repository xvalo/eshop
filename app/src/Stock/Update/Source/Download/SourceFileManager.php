<?php

namespace App\Stock\Update\Source\Download;

use App\FileNotFoundException;
use Nette\IOException;
use Nette\SmartObject;
use Nette\Utils\FileSystem;

class SourceFileManager
{
	use SmartObject;

	/**
	 * @param string $source
	 * @param string $path
	 * @return string
	 */
	public function downloadSourceFile(string $source, string $path): string
	{
		try {
			$content = FileSystem::read($source);
		} catch (IOException $e) {
			throw new StockSourceUnavailableException('Given source file is not reachable');
		}

		if ($content === false) {
			throw new StockSourceUnavailableException('Given source file doesn\'t containt any data or they are corrupted');
		}

		FileSystem::write($path, $content);
		$extension = $this->getFileExtension(mime_content_type($path));
		$newPath = $path.'.'.$extension;

		FileSystem::rename($path, $newPath);

		return $newPath;
	}

	/**
	 * @param string $path
	 */
	public function removeSourceFile(string $path)
	{
		if (!file_exists($path)) {
			throw new FileNotFoundException('Source file doesn\'t exists');
		}

		FileSystem::delete($path);
	}

	/**
	 * @param string $mimeType
	 * @return string
	 */
	private function getFileExtension(string $mimeType): string
	{
		$mimeTypes = [
			'application/xml' => 'xml',
			'text/csv' => 'csv'
		];

		return $mimeTypes[$mimeType];
	}
}