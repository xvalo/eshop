<?php

namespace App\Stock\Update\Source\Reader;

interface ISupplierReader
{
	/**
	 * @param string $file
	 */
	public function setDataSource(string $file);

	/**
	 * @return Item|false
	 */
	public function read();

	public function init();

	/**
	 * @param int|string $supplierAvailability
	 * @return int
	 */
	public function countSystemAvailability($supplierAvailability): int;
}