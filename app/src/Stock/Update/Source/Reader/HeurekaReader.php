<?php

namespace App\Stock\Update\Source\Reader;

class HeurekaReader extends BaseReader
{
	/**
	 * @return Item|false
	 */
	public function read()
	{
		while ($this->xmlReader->read()) {
			if ($this->xmlReader->nodeType == \XMLReader::ELEMENT && $this->xmlReader->name == 'SHOPITEM') {
				$shopItem = new \SimpleXMLElement($this->xmlReader->readOuterXml());

				return new Item(
					(string)$shopItem->ITEM_ID,
					$this->countSystemAvailability((int)$shopItem->DELIVERY_DATE),
					(float)$shopItem->PRICE_VAT
				);
			}
		}

		return false;
	}

	/**
	 * @param int|string $supplierAvailability
	 * @return int
	 */
	public function countSystemAvailability($supplierAvailability): int
	{
		if ($supplierAvailability <= 3) {
			return 2;
		}

		if ($supplierAvailability <= 7) {
			return 3;
		}

		return 4;
	}
}