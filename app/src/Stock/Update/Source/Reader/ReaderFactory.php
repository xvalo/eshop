<?php

namespace App\Stock\Update\Source\Reader;

use App\Stock\Supplier\Entity\Supplier;

class ReaderFactory
{
	/**
	 * @var array|BaseReader[]
	 */
	private $readers;

	/**
	 * @var HeurekaReader
	 */
	private $heurekaReader;

	public function __construct(array $readers, HeurekaReader $heurekaReader)
	{
		$this->readers = $readers;
		$this->heurekaReader = $heurekaReader;
	}

	/**
	 * @param Supplier $supplier
	 * @return BaseReader
	 */
	public function getReader(Supplier $supplier): BaseReader
	{
		if ($supplier->isHeurekaSource()) {
			$reader = clone $this->heurekaReader;
		} elseif (isset($this->readers[$supplier->getId()]) && $this->readers[$supplier->getId()] instanceof BaseReader) {
			$reader = $this->readers[$supplier->getId()];
		} else {
			throw new ReaderNotFoundException('Reader class for supplier '.$supplier->getName().' is  not set in configuration');
		}

		return $reader;
	}
}