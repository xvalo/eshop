<?php

namespace App\Stock\Update\Source\Reader;

class Item
{

	/**
	 * @var string
	 */
	private $code;

	/**
	 * @var int
	 */
	private $availability;

	/**
	 * @var float
	 */
	private $price;

	public function __construct(string $code, int $availability, float $price)
	{
		$this->code = $code;
		$this->availability = $availability;
		$this->price = $price;
	}

	/**
	 * @return string
	 */
	public function getCode(): string
	{
		return $this->code;
	}

	/**
	 * @return int
	 */
	public function getAvailability(): int
	{
		return $this->availability;
	}

	/**
	 * @return float
	 */
	public function getPrice(): float
	{
		return $this->price;
	}
}