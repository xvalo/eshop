<?php

namespace App\Stock\Update\Source\Reader;

use App\InvalidStateException;

class ReaderSourceNotAvailableException extends InvalidStateException
{

}