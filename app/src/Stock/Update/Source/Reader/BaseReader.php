<?php

namespace App\Stock\Update\Source\Reader;

use App\Product\Availability\AvailabilityProvider;

abstract class BaseReader implements ISupplierReader
{
	/**
	 * @var string
	 */
	protected $sourcePath;

	/**
	 * @var \XMLReader
	 */
	protected $xmlReader;

	/**
	 * @var AvailabilityProvider
	 */
	protected $availabilityProvider;

	public function __construct(AvailabilityProvider $availabilityProvider)
	{
		$this->availabilityProvider = $availabilityProvider;
	}

	/**
	 * @param string $file
	 */
	public function setDataSource(string $file)
	{
		$this->sourcePath = $file;
	}

	public function init()
	{
		if ($this->sourcePath === null || !file_exists($this->sourcePath)) {
			throw new ReaderSourceNotAvailableException('Reader source either doesn\'t exist or is not available.');
		}

		$this->xmlReader = new \XMLReader();
		$this->xmlReader->open($this->sourcePath);
	}
}