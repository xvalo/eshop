<?php

namespace App\Stock\Update\Source\VariantStateUpdate;

use App\Product\VariantRepository;
use App\Stock\Supplier\Entity\Supplier;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\SmartObject;

class StateUpdateManager
{
	use SmartObject;

	const STORAGE_NAME = 'stock-update-variants-state';
	const VARIANTS_CODES_MAPPING_TABLE = 'variants-codes-mapping';

	/**
	 * @var VariantRepository
	 */
	private $variantRepository;

	/**
	 * @var Cache
	 */
	private $updateStorageCache;

	/**
	 * @var Cache
	 */
	private $variantsCodesMappingCache;

	private $variantsCodesData;

	public function __construct(
		VariantRepository $variantRepository,
		IStorage $storage
	){
		$this->variantRepository = $variantRepository;

		$this->updateStorageCache = new Cache($storage, self::STORAGE_NAME);
		$this->variantsCodesMappingCache = new Cache($storage, self::VARIANTS_CODES_MAPPING_TABLE);
	}

	public function initCodesToVariantsMapping()
	{
		if ($this->variantsCodesData === null) {
			$this->variantsCodesData = $this->variantRepository->getVariantsCodesToSynchronize();
		}

		foreach ($this->variantsCodesData as $data) {
			$code = $data['code'];
			if (!$row = $this->variantsCodesMappingCache->load($code, false)) {
				$row = [];
			}

			$row[$data['supplierId']] = $data['variantId'];
			$this->variantsCodesMappingCache->save($code, $row);
		}

	}

	public function initUpdatesTable()
	{
		if ($this->variantsCodesData === null) {
			$this->variantsCodesData = $this->variantRepository->getVariantsCodesToSynchronize();
		}

		$table = [];
		foreach ($this->variantsCodesData as $data) {
			$variant = $data['variantId'];
			if (!array_key_exists($variant, $table)) {
				$table[$variant] = [
					'priceId' => $data['priceId'],
					'price' => $data['priceValue'],
					'availability' => $data['availability'],
					'isPriceSynchronized' => [],
					'supplierState' => []
				];
			}

			$table[$variant]['supplierState'][$data['supplierId']] = [];
			$table[$variant]['isPriceSynchronized'][$data['supplierId']] = $data['priceSynchronized'];

		}

		$this->updateStorageCache->save('table', $table);
	}

	/**
	 * @param string $code
	 * @param Supplier $supplier
	 * @return int|null
	 */
	public function getVariantByCode(string $code, Supplier $supplier)
	{
		$code = $this->variantsCodesMappingCache->load($code);

		if ($code !== null && isset($code[$supplier->getId()])) {
			return $code[$supplier->getId()];
		}

		return null;
	}

	/**
	 * @param int $variantId
	 * @param Supplier $supplier
	 * @param int $availability
	 * @param float $price
	 * @throws \Throwable
	 */
	public function saveVariantChange(int $variantId, Supplier $supplier, int $availability, float $price)
	{
		$stateTable = $this->updateStorageCache->load('table');

		$stateTable[$variantId]['supplierState'][$supplier->getId()]['availability'] = $availability;
		$stateTable[$variantId]['supplierState'][$supplier->getId()]['price'] = $price;

		$this->updateStorageCache->save('table', $stateTable);
	}

	public function getCurrentState()
	{
		return $this->updateStorageCache->load('table');
	}

	public function clearCache()
	{
		$this->updateStorageCache->clean([ Cache::ALL => true]);
		$this->variantsCodesMappingCache->clean([ Cache::ALL => true]);
	}

}