<?php

namespace App\Stock\Update\Source\History;

use App\Product\Availability\Entity\Availability;
use App\Product\Price\Price;
use App\Product\Variant;
use App\Stock\Supplier\Entity\Supplier;
use Kdyby\Doctrine\EntityManager;

class SourceUpdateHistoryFacade
{
	/**
	 * @var EntityManager
	 */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	public function updateVariantPrice(int $variantId, int $priceVariantId, int $availability, float $price, int $supplier, string $decisionState)
	{
		/** @var Availability $availability */
		$availability = $this->em->getRepository(Availability::class)->find($availability);
		/** @var Variant $variant */
		$variant = $this->em->getRepository(Variant::class)->find($variantId);
		/** @var Price $variantPrice */
		$variantPrice = $this->em->getRepository(Price::class)->find($priceVariantId);
		/** @var Supplier $supplier */
		$supplier = $this->em->getRepository(Supplier::class)->find($supplier);

		$variant->setAvailability($availability);
		$variantPrice->setPrice($price);

		$stockStateUpdateHistory = new StockStateUpdateHistory($variantPrice, $availability, $price, $supplier, $decisionState);

		$this->em->persist($stockStateUpdateHistory);
		$this->em->flush();

	}

	/**
	 * @return \Kdyby\Doctrine\QueryBuilder
	 */
	public function getSourceHistoryQueryBuilder(): \Kdyby\Doctrine\QueryBuilder
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('H')
			->from(StockStateUpdateHistory::class, 'H')
			->join('H.priceVariant', 'PV')
			->join('PV.variant', 'V')
			->join('V.product', 'P')
			->groupBy('H.id');

		return $qb;
	}
}