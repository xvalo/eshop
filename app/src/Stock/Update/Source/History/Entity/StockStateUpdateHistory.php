<?php

namespace App\Stock\Update\Source\History;

use App\Product\Availability\Entity\Availability;
use App\Stock\Supplier\Entity\Supplier;
use App\Product\Price\Price;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use App\Model\BaseEntity;
use Nette\Utils\Json;

/**
 * @ORM\Entity
 */
class StockStateUpdateHistory extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="\App\Product\Price\Price")
	 * @ORM\JoinColumn(name="variant_price_id", referencedColumnName="id")
	 * @var Price
	 */
    private $priceVariant;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Product\Availability\Entity\Availability")
	 * @ORM\JoinColumn(name="availability_id", referencedColumnName="id")
	 * @var Availability
	 */
    private $availability;

    /**
     * @ORM\Column(type="decimal", scale=2, precision=10)
     */
    private $price;

	/**
	 * @ORM\ManyToOne(targetEntity="\App\Stock\Supplier\Entity\Supplier")
	 * @ORM\JoinColumn(name="supplier_id", referencedColumnName="id")
	 * @var Supplier
	 */
	private $supplier;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    private $created;

	/**
	 * @ORM\Column(type="text")
	 * @var string
	 */
    private $decisionState;

    public function __construct(Price $priceVariant, Availability $availability, float $price, Supplier $supplier, string $decisionState)
    {
        $this->created = new DateTime();
        $this->availability = $availability;
        $this->price = $price;
        $this->supplier = $supplier;
        $this->priceVariant = $priceVariant;
        $this->decisionState = $decisionState;
    }

    /**
     * @return Availability
     */
    public function getAvailability(): Availability
    {
        return $this->availability;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return Supplier
     */
    public function getSupplier(): Supplier
    {
        return $this->supplier;
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @return Price
     */
    public function getPriceVariant(): Price
    {
        return $this->priceVariant;
    }

	/**
	 * @return string
	 */
	public function getDecisionState(): string
	{
		return $this->decisionState;
	}

	/**
	 * @return array
	 * @throws \Nette\Utils\JsonException
	 */
	public function getParsedDecisionState(): array
	{
		return Json::decode($this->decisionState, Json::FORCE_ARRAY);
	}
}