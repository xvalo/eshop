<?php

namespace App\Stock\VariantStockItem;

use App\Lists\ListsService;
use App\Lists\Stock;
use App\Product\Variant;
use App\Stock\StockItem;
use App\Stock\StockItemRepository;

class VariantStockItemService
{
	/** @var StockItemRepository */
	private $stockItemRepository;

	/** @var ListsService */
	private $listsService;

	public function __construct(
		StockItemRepository $stockItemRepository,
		ListsService $listsService
	)
	{
		$this->stockItemRepository = $stockItemRepository;
		$this->listsService = $listsService;
	}

	public function getVariantOnStocksStatus(Variant $variant)
	{
		$stocks = $this->listsService->getStocksList();
		$existingItems = $variant->getStocksItems();
		$stocksStatus = [];

		/** @var StockItem $item */
		foreach ($existingItems as $item) {
			/** @var Stock $stock */
			$stock = $item->getStock();
			$stocksStatus[$stock->getId()] = [
				'stock' => $stock->getName(),
				'stockItem' => $item
			];

			unset($stocks[$stock->getId()]);
		}

		foreach ($stocks as $stockId => $stockName) {
			$stocksStatus[$stockId] = [
				'stock' => $stockName,
				'stockItem' => null
			];
		}

		return $stocksStatus;
	}
}