<?php

namespace App\Stock\VariantStockItem\Session;

use App\Utils\Numbers;
use App\Utils\Vat;
use Nette\Http\Session;
use Nette\Http\SessionSection;

class StockOperationManager
{
	const STOCK_OPERATION_SECTION = 'stock-operation';

	const SECTION_PART_DATE = 'date';
	const SECTION_PART_STOCK = 'stock';
	const SECTION_PART_VARIANTS = 'variants';

	const OPERATION_TYPE_IN = 'in';
	const OPERATION_TYPE_OUT = 'out';
	const OPERATION_TYPE_OFF = 'off';

	/** @var SessionSection */
	private $section;

	/** @var array $defaults Default values of new bill */
	private $defaults = [
		self::SECTION_PART_DATE => null,
		self::SECTION_PART_STOCK => null,
		self::SECTION_PART_VARIANTS => []
	];

	public function __construct(Session $session)
	{
		$this->section = $session->getSection(self::STOCK_OPERATION_SECTION);
	}

	public function setDate(string $operationType, string $date)
	{
		$this->setData($operationType, self::SECTION_PART_DATE, $date);
	}

	public function setStock(string $operationType, int $stockID)
	{
		$this->setData($operationType, self::SECTION_PART_STOCK, $stockID);
	}

	public function addVariant(string $operationType, StockOperationVariantData $data)
	{
		$savedVariants = $this->getData($operationType, self::SECTION_PART_VARIANTS);

		if (is_null($savedVariants)) {
			$savedVariants = [];
		}

		if (isset($savedVariants[$data->getId()])) {
			$variant = $savedVariants[$data->getId()];
			$variant['count'] += 1;
			unset($savedVariants[$data->getId()]);
		} else {
			$variant = [
				'id' => $data->getId(),
				'code' => $data->getCode(),
				'name' => $data->getName(),
				'count' => $data->getCount(),
				'purchasePriceWithoutVat' => $data->getPurchasePriceWithoutVat(),
				'price' => $data->getPrice(),
				'priceWithoutVat' => $data->getPriceWithoutVat(),
				'margin' => $data->getMargin(),
				'vat' => $data->getVat()
			];

		}
		$savedVariants = [$data->getId() => $variant] + $savedVariants;

		$this->setData($operationType, self::SECTION_PART_VARIANTS, $savedVariants);
	}

	public function updateVariants(string $operationType, array $counts, array $purchasePrices, array $prices)
	{
		$savedVariants = $this->getData($operationType, self::SECTION_PART_VARIANTS);

		foreach ($savedVariants as $id => $variant) {
			if (isset($counts[$id])) {
				$savedVariants[$id]['count'] = $counts[$id];
			}

			if (isset($purchasePrices[$id])) {
				$savedVariants[$id]['purchasePriceWithoutVat'] = $purchasePrices[$id];
			}

			if (isset($prices[$id])) {
				$savedVariants[$id]['price'] = $prices[$id];
				$vat = Vat::countVat($savedVariants[$id]['price'], $savedVariants[$id]['vat']);
				$savedVariants[$id]['priceWithoutVat'] = $savedVariants[$id]['price'] - $vat;
			}

			$savedVariants[$id]['margin'] = Numbers::countPriceMargin($savedVariants[$id]['priceWithoutVat'], $savedVariants[$id]['purchasePriceWithoutVat']);
		}

		$this->setData($operationType, self::SECTION_PART_VARIANTS, $savedVariants);
	}

	public function getDate(string $operationType)
	{
		return $this->getData($operationType, self::SECTION_PART_DATE) ?: '';
	}

	public function getStock(string $operationType)
	{
		return $this->getData($operationType, self::SECTION_PART_STOCK);
	}

	public function getVariants(string $operationType)
	{
		return $this->getData($operationType, self::SECTION_PART_VARIANTS) ?: [];
	}

	public function removeVariant(string $operationType, int $priceId)
	{
		$savedVariants = $this->getData($operationType, self::SECTION_PART_VARIANTS);

		if (isset($savedVariants[$priceId])) {
			unset($savedVariants[$priceId]);
		}

		$this->setData($operationType, self::SECTION_PART_VARIANTS, $savedVariants);
	}

	/**
	 * Resets session into default state
	 */
	public function reset(string $operationType)
	{
		foreach ($this->defaults as $key => $defaultValue) {
			$this->setData($operationType, $key, $defaultValue);
		}
	}

	public function exists(string $operationType)
	{
		return $this->section->offsetExists($operationType);
	}

	/////////////////// PRIVATE METHODS ///////////////////////

	private function setData(string $operationType, $key, $data)
	{
		$operationTypeData = $this->getOperationTypeData($operationType);
		$operationTypeData[$key] = $data;
		$this->section->offsetSet($operationType, $operationTypeData);
	}

	private function getData(string $operationType, $key)
	{
		$operationTypeData = $this->getOperationTypeData($operationType);

		if (isset($operationTypeData[$key])) {
			return $operationTypeData[$key];
		}

		return null;
	}

	private function getOperationTypeData(string $operationType)
	{
		if (!$this->section->offsetExists($operationType)) {
			$this->section->offsetSet($operationType, []);
		}

		return $this->section->offsetGet($operationType);
	}
}