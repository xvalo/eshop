<?php

namespace App\Stock\VariantStockItem\Session;

use App\Utils\Numbers;
use App\Utils\Vat;
use Nette\Object;

class StockOperationVariantData extends Object
{
	/** @var int */
	private $id;

	/** @var string */
	private $code;

	/** @var string */
	private $name;

	/** @var int */
	private $count;

	/** @var float */
	private $purchasePriceWithoutVat;

	/** @var float */
	private $price;

	/** @var int */
	private $vat;

	public function __construct(
		int $id,
		string $code,
		string $name,
		int $count,
		float $purchasePriceWithoutVat,
		float $price,
		int $vat
	){
		$this->id = $id;
		$this->code = $code;
		$this->name = $name;
		$this->count = $count;
		$this->purchasePriceWithoutVat = $purchasePriceWithoutVat;
		$this->price = $price;
		$this->vat = $vat;
	}

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getCode(): string
	{
		return $this->code;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @return int
	 */
	public function getCount(): int
	{
		return $this->count;
	}

	/**
	 * @return float
	 */
	public function getPurchasePriceWithoutVat(): float
	{
		return $this->purchasePriceWithoutVat;
	}

	/**
	 * @return float
	 */
	public function getMargin(): float
	{
		return Numbers::countPriceMargin($this->getPriceWithoutVat(), $this->getPurchasePriceWithoutVat());
	}

	/**
	 * @return float
	 */
	public function getPrice(): float
	{
		return $this->price;
	}

	/**
	 * @return float
	 */
	public function getPriceWithoutVat(): float
	{
		$vat = Vat::countVat($this->price, $this->vat);
		return $this->price - $vat;
	}

	/**
	 * @return int
	 */
	public function getVat(): int
	{
		return $this->vat;
	}
}