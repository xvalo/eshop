<?php

namespace App\Stock;

use App\Admin\Admin;
use App\Admin\AdminRepository;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\InvalidArgumentException;
use App\Lists\Stock;
use App\Lists\StockRepository;
use App\Model\Services\ApplicationConfigurationService;
use App\Order\Order;
use App\Product\Availability\AvailabilityProvider;
use App\Product\Availability\Entity\Availability;
use App\Product\Price\Price;
use App\Product\Variant;
use App\Product\VariantRepository;
use App\Stock\Document\StockFileManager;
use App\Stock\Document\ValueObject\StockFileValueObject;
use App\Stock\VariantStockItem\Session\StockOperationManager;
use App\Utils\Numbers;
use Carrooi\Security\User\User;
use Nette\SmartObject;
use Nette\Utils\DateTime;

class StockManager
{
	use SmartObject;

    const CHANGE_ADD = 1;
    const CHANGE_SUBTRACT = -1;

    /** @var VariantRepository */
    private $repository;

    /** @var StockChangeRepository */
    private $stockChangeRepository;

    /** @var StockItemRepository */
    private $stockItemRepository;

    /** @var StockFileRepository */
    private $stockFileRepository;

    /** @var StockFileManager */
    private $stockFileManager;

    /** @var  */
    private $user;
	/**
	 * @var AdminRepository
	 */
	private $adminRepository;

	/** @var  Stock */
	private $mainStock;
	/**
	 * @var ApplicationSettingsManager
	 */
	private $applicationSettingsManager;

	/** @var Availability */
	private $soldOutProductAvailabilityState;

	/** @var AvailabilityProvider */
	private $availabilityProvider;

	public function __construct(
	    VariantRepository $repository,
	    AdminRepository $adminRepository,
	    User $user,
	    StockChangeRepository $stockChangeRepository,
	    StockItemRepository $stockItemRepository,
	    StockFileRepository $stockFileRepository,
	    StockFileManager $stockFileManager,
	    StockRepository $stockRepository,
		AvailabilityProvider $availabilityProvider,
		ApplicationSettingsManager $applicationSettingsManager,
		ApplicationConfigurationService $applicationConfigurationService
    ) {
        $this->repository = $repository;
        $this->stockChangeRepository = $stockChangeRepository;
        $this->stockItemRepository = $stockItemRepository;
        $this->stockFileRepository = $stockFileRepository;

        $this->stockFileManager = $stockFileManager;
        $this->user = $user;
	    $this->adminRepository = $adminRepository;

	    $this->mainStock = $stockRepository->getMainStock();
	    $this->applicationSettingsManager = $applicationSettingsManager;
	    $this->soldOutProductAvailabilityState = $availabilityProvider->getAvailability($applicationConfigurationService->getSoldOutProductAvailabilityState());
		$this->availabilityProvider = $availabilityProvider;
	}

	/**
	 * @param Variant $product
	 * @param Stock $stock
	 * @param $countChange
	 * @param $position
	 * @param $limit
	 * @param $enableAlert
	 * @param bool $commit
	 */
    public function changeStockItemState(Variant $product, Stock $stock, $countChange, $position, $limit, $enableAlert = null, $commit = true)
    {
        $stockItem = ($countChange >= 0)
                        ? $this->addQuantity($product, $stock, abs($countChange))
                        : $this->subQuantity($product, $stock, abs($countChange));

        $stockItem->setPosition($position);
        $stockItem->setLimitToAlert($limit);

        if (!is_null($enableAlert)) {
	        if ($enableAlert) {
		        $stockItem->enableAlert();
	        } else {
		        $stockItem->disableAlert();
	        }
        }

        $this->createStockChange($stockItem, (int)$countChange);

        if ($commit) {
            $this->stockItemRepository->flush();
        }
    }

    public function sellStockItem(Variant $product, Stock $stock, Order $order, $countChange, $commit = true)
    {
        $stockItem = $this->subQuantity($product, $stock, $countChange);
        $this->createStockChange($stockItem, (-1)*(int)$countChange, $order);

        if ($commit) {
            $this->stockItemRepository->flush();
        }
    }

	public function returnStockItem(Variant $product, Stock $stock, Order $order, $countChange, $commit = true)
	{
		$stockItem = $this->addQuantity($product, $stock, $countChange);
		$this->createStockChange($stockItem, (int)$countChange, $order);

		if ($commit) {
			$this->stockItemRepository->flush();
		}
    }

    public function bulkStockStateUpdate(Stock $stock, array $quantities, array $purchasePrices, array $prices, $changeType)
    {
    	$changes = [];

        foreach ($quantities as $itemId => $quantity) {
        	$selectedPrice = $this->repository->findPrice($itemId);
            /** @var Variant $product */
            $product = $selectedPrice->getVariant();

            if($changeType == self::CHANGE_ADD) {
                $stockItem = $this->addQuantity($product, $stock, $quantity);
            } elseif ($changeType == self::CHANGE_SUBTRACT) {
                $stockItem = $this->subQuantity($product, $stock, $quantity);
            } else {
                throw new InvalidArgumentException;
            }

            $purchasePrice = isset($purchasePrices[$itemId]) ? floatval($purchasePrices[$itemId]) : null;
            if ($purchasePrice) {
	            /** @var Price $price */
	            foreach ($product->getPrices() as $productPrice) {
		            $productPrice->setPurchasePriceWithoutVat($purchasePrice);
	            }
            }

	        $price = isset($prices[$itemId]) ? floatval($prices[$itemId]) : null;
            if ($price) {
	            $selectedPrice->setPrice($price);
            }

            $this->createStockChange($stockItem, $changeType*$quantity);
            $changes[] = [
            	'product' => $product,
	            'quantity' => $quantity,
	            'purchasePrice' => $purchasePrice,
	            'price' => $price,
	            'margin' => isset($purchasePrice) ? Numbers::countPriceMargin($selectedPrice->getPriceWithoutVat(), $purchasePrice) : null
            ];
        }

        $this->stockItemRepository->flush();
        return $changes;
    }

    /**
     * @return array|null
     */
    public function getProductsOnStockLimit()
    {
		$items = $this->repository->getProductsOnStockLimit();

		if (count($items) == 0) {
			return null;
		}

		return [
			'count' => count($items),
			'items' => $items
		];
    }

	/**
	 * @param array $items
	 * @param string $type
	 * @param Stock $stock
	 * @param DateTime|null $created
	 * @return StockFile
	 */
    public function generateFile(array $items, string $type, Stock $stock, DateTime $created = null): StockFile
    {
	    $number = $this->getFileNumber($type);

	    $fileCreated = $created ?: new DateTime();

        $pdf = $this->stockFileManager->createPDF( new StockFileValueObject($number, $type, $stock, $items, $fileCreated) );

        $stockFile = new StockFile($stock, $this->getChangeCreator(), $pdf, $type);
        $this->updateFileNumber($type, ++$number);

        $this->stockFileRepository->persist($stockFile);
        $this->stockFileRepository->flush();

        return $stockFile;
    }

	/**
	 * @return Stock|null
	 */
	public function getMainStock(): Stock
	{
		return $this->mainStock;
    }

    /////////////////// PRIVATE METHODS ///////////////////////////////

    private function addQuantity(Variant $variant, Stock $stock, $quantity)
    {
        $stockItem = $this->getStockItem($variant, $stock);
        $stockItem->addItems($quantity);

        $productStockCount = (int)$this->stockItemRepository->getProductStockCount($variant);
        if (($productStockCount + $quantity) > 0) {
	        $variant->setAvailability($this->availabilityProvider->getOnStockStatus());
        }

        return $stockItem;
    }

    private function subQuantity(Variant $product, Stock $stock, $quantity)
    {
        $stockItem = $this->getStockItem($product, $stock);
	    $stockItem->subtractItems($quantity);

	    if (!$product->isAvailableOnStock()) {
		    $product->setAvailability($this->soldOutProductAvailabilityState);
	    }

	    return $stockItem;
    }

	/**
	 * @param Variant $product
	 * @param Stock $stock
	 * @return StockItem
	 */
    private function getStockItem(Variant $product, Stock $stock)
    {
        $stockItem = $product->getStocksItems()->filter(function(StockItem $item) use ($stock){
            return $item->getStock()->getId() == $stock->getId();
        })->first();

        if ( !($stockItem instanceof StockItem) ) {
            $stockItem = new StockItem($product, $stock);
            $product->addStockItem($stockItem);
            $this->stockItemRepository->persist($stockItem);
        }

        return $stockItem;
    }

    private function createStockChange(StockItem $stockItem, $changeCount, Order $order = null)
    {
        $stockChange = new StockChange($stockItem, $changeCount, $this->getChangeCreator(), $order);
        $this->stockChangeRepository->persist($stockChange);
    }

	/**
	 * @return Admin|null
	 */
	private function getChangeCreator()
	{
		if ($this->user->isLoggedIn()) {
			return $this->adminRepository->find($this->user->getId());
		}

		return null;
    }

	/**
	 * @param string $type
	 * @return string
	 */
	private function getFileNumber(string $type): string
	{
		switch ($type) {
			case StockOperationManager::OPERATION_TYPE_IN:
				return $this->applicationSettingsManager->getNextStockInFileNumber();
			case StockOperationManager::OPERATION_TYPE_OUT:
				return $this->applicationSettingsManager->getNextStockOutFileNumber();
			case StockOperationManager::OPERATION_TYPE_OFF:
				return $this->applicationSettingsManager->getNextStockOffFileNumber();
		}
	}

	/**
	 * @param string $type
	 * @param string $number
	 */
	private function updateFileNumber(string $type, string $number)
	{
		switch ($type) {
			case StockOperationManager::OPERATION_TYPE_IN:
				$this->applicationSettingsManager->setNextStockInFileNumber($number);
				break;
			case StockOperationManager::OPERATION_TYPE_OUT:
				$this->applicationSettingsManager->setNextStockOutFileNumber($number);
				break;
			case StockOperationManager::OPERATION_TYPE_OFF:
				$this->applicationSettingsManager->setNextStockOffFileNumber($number);
				break;
		}
	}
}