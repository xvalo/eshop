<?php


namespace App\Stock\Supplier;

use App\Classes\Stock\Supplier\MissingSupplierSourceException;
use App\Classes\Stock\Supplier\SupplierFacade;
use App\Stock\Supplier\Entity\Supplier;
use Tracy\Debugger;

class SupplierService
{
	/** @var SupplierFacade */
	private $supplierFacade;

	public function __construct(SupplierFacade $supplierFacade)
	{
		$this->supplierFacade = $supplierFacade;
	}

	/**
	 * @param int $id
	 * @return Supplier|null
	 */
	public function getSupplier(int $id)
	{
		return $this->supplierFacade->getSupplierById($id);
	}

	/**
	 * @param string $name
	 * @param string|null $prefix
	 * @param string|null $note
	 * @param bool $synchronized
	 * @param string|null $source
	 * @param bool $heurekaSource
	 * @param bool $isActive
	 * @return Supplier
	 * @throws \Exception
	 */
	public function createSupplier(
		string $name,
		string $prefix = null,
		string $note = null,
		bool $synchronized = false,
		string $source = null,
		bool $heurekaSource = false,
		bool $isActive = false
	): Supplier
	{
		if ($synchronized === true && ($source === null || strlen($source) === 0)) {
			throw new MissingSupplierSourceException('Synchronization is "turned on" but no source available');
		}

		$supplier = new Supplier(
			$name,
			$prefix,
			$note,
			$synchronized,
			$source,
			$heurekaSource
		);

		if ($isActive) {
			$supplier->activate();
		}

		$this->supplierFacade->saveSupplier($supplier);

		return $supplier;
	}

	/**
	 * @param Supplier $supplier
	 * @param string $name
	 * @param string|null $prefix
	 * @param string|null $note
	 * @param bool $synchronized
	 * @param string|null $source
	 * @param bool $heurekaSource
	 * @param bool $isActive
	 * @return Supplier
	 * @throws \Exception
	 */
	public function updateSupplier(
		Supplier $supplier,
		string $name,
		string $prefix = null,
		string $note = null,
		bool $synchronized = false,
		string $source = null,
		bool $heurekaSource = false,
		bool $isActive = false
	): Supplier
	{
		$supplier->setName($name);
		$supplier->setPrefix(
			strlen($prefix) !== 0 ? $prefix : null
		);
		$supplier->setNote(
			strlen($note) !== 0 ? $note : null
		);

		if ($synchronized === true && ($source === null || strlen($source) === 0)) {
			throw new MissingSupplierSourceException('Synchronization is "turned on" but no source available');
		}

		$supplier->setSynchronized($synchronized);
		$supplier->setSource(
			strlen($source) !== 0 ? $source : null
		);
		$supplier->setHeurekaSource($heurekaSource);

		$isActive ? $supplier->activate() : $supplier->deactivate();


		$this->supplierFacade->saveSupplier($supplier);

		return $supplier;
	}

	/**
	 * @param int $id
	 * @return bool
	 */
	public function removeSupplierById(int $id)
	{
		$supplier = $this->supplierFacade->getSupplierById($id);

		return $this->removeSupplier($supplier);
	}

	/**
	 * @param Supplier $supplier
	 * @return bool
	 */
	public function removeSupplier(Supplier $supplier)
	{
		$supplier->delete();

		try {
			$this->supplierFacade->saveSupplier($supplier);
		} catch (\Exception $e) {
			Debugger::log($e);
			return false;
		}

		return true;
	}

	public function getSuppliersList()
	{
		return $this->supplierFacade->getList();
	}
}