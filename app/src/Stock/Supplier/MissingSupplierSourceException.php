<?php


namespace App\Classes\Stock\Supplier;


use App\InvalidStateException;

class MissingSupplierSourceException extends InvalidStateException
{

}