<?php

namespace App\Classes\Stock\Supplier;

use App\Stock\Supplier\Entity\Supplier;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class SupplierFacade
{
	use SmartObject;

	/** @var EntityManager */
	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	/**
	 * @param int $id
	 * @return null|Supplier
	 */
	public function getSupplierById(int $id)
	{
		return $this->em->getRepository(Supplier::class)->find($id);
	}

	/**
	 * @param Supplier $supplier
	 * @throws \Exception
	 */
	public function saveSupplier(Supplier $supplier)
	{
		if ($supplier->getId() === null) {
			$this->em->persist($supplier);
		}

		$this->em->flush();
	}

	/**
	 * @return \Kdyby\Doctrine\QueryBuilder
	 */
	public function getGridSource()
	{
		$qb = $this->em->getRepository(Supplier::class)->createQueryBuilder('S');

		$qb->where(
			$qb->expr()->eq('S.deleted', ':deleted')
		)->setParameters([
			'deleted' => false
		]);

		return $qb;
	}

	/**
	 * @return array
	 */
	public function getList(): array
	{
		return $this->em->getRepository(Supplier::class)->findPairs(['deleted' => false], 'name');
	}
}