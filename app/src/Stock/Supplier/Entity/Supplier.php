<?php

namespace App\Stock\Supplier\Entity;

use App\Model\BaseEntity;
use App\Traits\ActivityTrait;
use App\Traits\IRemovable;
use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Supplier extends BaseEntity implements IRemovable
{
	use RemovableTrait;
	use ActivityTrait;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=10, nullable=true)
	 * @var string
	 */
	private $prefix;

	/**
	 * @ORM\Column(type="string", length=250, nullable=true)
	 * @var string
	 */
	private $note;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $synchronized;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @var string
	 */
	private $source;

	/**
	 * @ORM\Column(type="boolean")
	 * @var boolean
	 */
	private $heurekaSource;

	/**
	 * @param string $name
	 * @param string|null $prefix
	 * @param string|null $note
	 * @param bool $synchronized
	 * @param string|null $source
	 * @param bool $heurekaSource
	 */
	public function __construct(
		string $name,
		string $prefix = null,
		string $note = null,
		bool $synchronized = false,
		string $source = null,
		bool $heurekaSource = false
	){
		$this->name = $name;
		$this->prefix = $prefix;
		$this->note = $note;
		$this->synchronized = $synchronized;
		$this->source = $source;
		$this->heurekaSource = $heurekaSource;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return string|null
	 */
	public function getPrefix()
	{
		return $this->prefix;
	}

	/**
	 * @param string $prefix
	 */
	public function setPrefix(string $prefix = null)
	{
		$this->prefix = $prefix;
	}

	/**
	 * @return string|null
	 */
	public function getNote()
	{
		return $this->note;
	}

	/**
	 * @param string $note
	 */
	public function setNote(string $note = null)
	{
		$this->note = $note;
	}

	/**
	 * @return bool
	 */
	public function isSynchronized(): bool
	{
		return $this->synchronized;
	}

	/**
	 * @param bool $synchronized
	 */
	public function setSynchronized(bool $synchronized)
	{
		$this->synchronized = $synchronized;
	}

	/**
	 * @return string
	 */
	public function getSource()
	{
		return $this->source;
	}

	/**
	 * @param string|null $source
	 */
	public function setSource(string $source = null)
	{
		$this->source = $source;
	}

	/**
	 * @return bool
	 */
	public function isHeurekaSource(): bool
	{
		return $this->heurekaSource;
	}

	/**
	 * @param bool $heurekaSource
	 */
	public function setHeurekaSource(bool $heurekaSource)
	{
		$this->heurekaSource = $heurekaSource;
	}

	/**
	 * @return string
	 */
	public function __toString(): string
	{
		return $this->getName();
	}
}