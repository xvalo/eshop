<?php

namespace App\Classes\Stock\Supplier;

use App\Stock\Supplier\Entity\Supplier;
use App\Product\Code\Code;
use App\Product\Code\VariantCode;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Kdyby\Events\Subscriber;
use Doctrine\ORM\Events;

class SupplierListener implements Subscriber
{
	public function getSubscribedEvents()
	{
		return [
			Events::onFlush
		];
	}

	public function onFlush(OnFlushEventArgs $eventArgs)
	{
		$em = $eventArgs->getEntityManager();
		$uow = $em->getUnitOfWork();

		foreach ($uow->getScheduledEntityUpdates() as $entity) {
			if ($entity instanceof Supplier) {
				$changeSet = $uow->getEntityChangeSet($entity);
				if (isset($changeSet['prefix'])) {
					$oldPrefix = $changeSet['prefix'][0];
					$newPrefix = $changeSet['prefix'][1];

					$variantsCodes = $em->getRepository(VariantCode::class)->findBy(['supplier' => $entity]);
					/** @var VariantCode $variantCode */
					foreach ($variantsCodes as $variantCode) {
						$code = $variantCode->getCode()->getCode();
						if ($oldPrefix && strlen($oldPrefix) > 0) {
							$code = preg_replace('/^' . preg_quote($oldPrefix.'-', '/') . '/', '', $code);
							//$code = str_replace($oldPrefix.'-', '', $code);
						}

						$code = ltrim($code, '-');
						$prefix = $newPrefix !== '' && $newPrefix !== null ? $newPrefix . '-' : '';

						$newCode = new Code($prefix . $code);
						$uow->persist($newCode);
						$variantCode->setCode($newCode);
					}
				}

				$uow->computeChangeSets();
			}
		}
	}
}