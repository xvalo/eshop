<?php

namespace App\Stock\Document\ValueObject;

class InventoryItemValueObject
{
	/** @var string */
	private $variantCodes;

	/** @var string */
	private $productNumber;

	/** @var string */
	private $variantName;

	/** @var float */
	private $price;

	/** @var float|null */
	private $purchasePrice;

	/** @var float|null */
	private $purchasePriceWithoutVat;

	/** @var float|null */
	private $priceWithoutVat;

	/** @var int */
	private $count;

	/** @var string */
	private $stockName;

	public function __construct(
		string $variantCodes,
		string $productNumber,
		string $variantName,
		float $price,
		int $count,
		string $stockName,
		float $purchasePrice = null,
		float $purchasePriceWithoutVat = null,
		float $priceWithoutVat = null
	){
		$this->variantCodes = $variantCodes;
		$this->productNumber = $productNumber;
		$this->variantName = $variantName;
		$this->purchasePrice = $purchasePrice;
		$this->purchasePriceWithoutVat = $purchasePriceWithoutVat;
		$this->price = $price;
		$this->priceWithoutVat = $priceWithoutVat;
		$this->count = $count;
		$this->stockName = $stockName;
	}

	/**
	 * @return string
	 */
	public function getVariantCodes(): string
	{
		return $this->variantCodes;
	}

	/**
	 * @return string
	 */
	public function getProductNumber(): string
	{
		return $this->productNumber;
	}

	/**
	 * @return string
	 */
	public function getVariantName(): string
	{
		return $this->variantName;
	}

	/**
	 * @return float|null
	 */
	public function getPurchasePrice()
	{
		return $this->purchasePrice;
	}

	/**
	 * @return float|null
	 */
	public function getPurchasePriceWithoutVat()
	{
		return $this->purchasePriceWithoutVat;
	}

	/**
	 * @return float
	 */
	public function getPrice(): float
	{
		return $this->price;
	}

	/**
	 * @return float|null
	 */
	public function getPriceWithoutVat()
	{
		return $this->priceWithoutVat;
	}

	/**
	 * @return int
	 */
	public function getCount(): int
	{
		return $this->count;
	}

	/**
	 * @return string
	 */
	public function getStockName(): string
	{
		return $this->stockName;
	}
}