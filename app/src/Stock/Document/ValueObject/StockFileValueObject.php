<?php

namespace App\Stock\Document\ValueObject;

use App\Lists\Stock;
use Nette\Utils\DateTime;

class StockFileValueObject
{
	/** @var string */
	private $number;

	/** @var string */
	private $fileType;

	/** @var Stock */
	private $stock;

	/** @var array */
	private $stockChanges;

	/** @var DateTime */
	private $created;

	/**
	 * StockFileValueObject constructor.
	 * @param string $number
	 * @param string $fileType
	 * @param Stock $stock
	 * @param array $stockChanges
	 */
	public function __construct(string $number, string $fileType, Stock $stock, array $stockChanges, DateTime $created)
	{
		$this->number = $number;
		$this->fileType = $fileType;
		$this->stock = $stock;
		$this->stockChanges = $stockChanges;
		$this->created = $created;
	}

	/**
	 * @return string
	 */
	public function getNumber(): string
	{
		return $this->number;
	}

	/**
	 * @return string
	 */
	public function getFileType(): string
	{
		return $this->fileType;
	}

	/**
	 * @return Stock
	 */
	public function getStock(): Stock
	{
		return $this->stock;
	}

	/**
	 * @return array
	 */
	public function getStockChanges(): array
	{
		return $this->stockChanges;
	}

	/**
	 * @return DateTime
	 */
	public function getCreated(): DateTime
	{
		return $this->created;
	}
}