<?php

namespace App\Stock\Document\ValueObject;

class InventoryValuationData
{
	/** @var InventoryItemValueObject[] */
	private $inventoryItems = [];

	/** @var float */
	private $sumPurchasePriceWithoutVat = 0.0;

	/** @var float */
	private $sumPurchasePrice = 0.0;

	/** @var float */
	private $sumSalePriceWithoutVat = 0.0;

	/** @var float */
	private $sumSalePrice = 0.0;

	/**
	 * @return InventoryItemValueObject[]
	 */
	public function getInventoryItems(): array
	{
		return $this->inventoryItems;
	}

	/**
	 * @return float
	 */
	public function getSumPurchasePriceWithoutVat(): float
	{
		return $this->sumPurchasePriceWithoutVat;
	}

	/**
	 * @return float
	 */
	public function getSumPurchasePrice(): float
	{
		return $this->sumPurchasePrice;
	}

	/**
	 * @return float
	 */
	public function getSumSalePriceWithoutVat(): float
	{
		return $this->sumSalePriceWithoutVat;
	}

	/**
	 * @return float
	 */
	public function getSumSalePrice(): float
	{
		return $this->sumSalePrice;
	}

	/**
	 * @param InventoryItemValueObject $item
	 */
	public function addInventoryItem(InventoryItemValueObject $item)
	{
		$this->inventoryItems[] = $item;
	}

	/**
	 * @param float $price
	 */
	public function addPurchasePrice(float $price)
	{
		$this->sumPurchasePrice += $price;
	}

	/**
	 * @param float $price
	 */
	public function addPurchasePriceWithoutVat(float $price)
	{
		$this->sumPurchasePriceWithoutVat += $price;
	}

	/**
	 * @param float $price
	 */
	public function addSalePrice(float $price)
	{
		$this->sumSalePrice += $price;
	}

	/**
	 * @param float $price
	 */
	public function addSalePriceWithoutVat(float $price)
	{
		$this->sumSalePriceWithoutVat += $price;
	}
}