<?php

namespace App\Managers;

use App\Common\Document\PdfManager;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\Stock\Document\ValueObject\InventoryValuationData;
use Nette\Utils\DateTime;

class InventoryValuationManager
{
	/** @var string */
	private $directory;

	/** @var PdfManager */
	private $pdfManager;

	/** @var ApplicationSettingsManager */
	private $applicationSettingsManager;

	public function __construct(
		string $directory,
		PdfManager $pdfManager,
		ApplicationSettingsManager $applicationSettingsManager
	){
		$this->directory = $directory;
		$this->pdfManager = $pdfManager;
		$this->applicationSettingsManager = $applicationSettingsManager;
	}

	/**
	 * @param string $fileName
	 * @param InventoryValuationData $data
	 * @param DateTime $dateOfEvaluation
	 * @return string
	 */
	public function createPDF(string $fileName, InventoryValuationData $data, DateTime $dateOfEvaluation): string
	{
		$this->pdfManager->setTemplate(__DIR__.'/template/inventoryValuation.latte');

		if (!file_exists($this->directory)) {
			mkdir($this->directory);
		}

		$this->pdfManager->setValue('valuationData', $data);
		$this->pdfManager->setValue('created', $dateOfEvaluation->format('d. m. Y'));
		$this->pdfManager->setValue('webTitle', $this->applicationSettingsManager->getWebsiteTitle());

		return $this->pdfManager->savePdf($this->directory, $fileName);
	}
}