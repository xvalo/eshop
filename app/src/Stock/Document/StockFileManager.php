<?php

namespace App\Stock\Document;

use App\Common\Document\PdfManager;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\Lists\Stock;
use App\Stock\Document\ValueObject\StockFileValueObject;
use App\Stock\VariantStockItem\Session\StockOperationManager;
use Nette\SmartObject;

class StockFileManager
{
	use SmartObject;

	/** @var string */
	private $directory;

	/** @var PdfManager */
	private $pdfManager;

	/** @var ApplicationSettingsManager */
	private $applicationSettingsManager;

	public function __construct(
		string $directory,
		PdfManager $pdfManager,
		ApplicationSettingsManager $applicationSettingsManager
	){
		$this->directory = $directory;
		$this->pdfManager = $pdfManager;
		$this->applicationSettingsManager = $applicationSettingsManager;
	}

	public function createPDF(StockFileValueObject $valueObject)
	{
		$this->pdfManager->setTemplate(__DIR__.'/template/stockFilePdf.latte');

		$this->pdfManager->setValue('fileTitle', $this->getFileTitle($valueObject->getFileType()));
		$this->pdfManager->setValue('stock', $valueObject->getStock());
		$this->pdfManager->setValue('number', $valueObject->getNumber());
		$this->pdfManager->setValue('items', $valueObject->getStockChanges());
		$this->pdfManager->setValue('date', $valueObject->getCreated()->format('d. m. Y'));
		$this->pdfManager->setValue('webTitle', $this->applicationSettingsManager->getWebsiteTitle());

		$fileName = $this->generateName($valueObject->getFileType());
		$this->pdfManager->savePdf($this->getStockDir($valueObject->getStock()), $fileName);

		return $fileName.'.pdf';
	}

    private function generateName($type)
    {
        return str_pad(mt_rand(1, 10000), 5, '0', STR_PAD_RIGHT) . '-' .time() . '-' . $type;
    }

    private function getFileTitle($type)
    {
        switch ($type) {
            case StockOperationManager::OPERATION_TYPE_IN:
                return 'Příjemka zboží';
            case StockOperationManager::OPERATION_TYPE_OUT:
                return 'Výdejka zboží';
            case StockOperationManager::OPERATION_TYPE_OFF:
                return 'Odpis zboží';
        }
    }

    private function getStockDir(Stock $stock)
    {
        $dir = $this->directory . $stock->getId() .'/';

        if(!file_exists($dir)){
            mkdir($dir);
        }

        return $dir;
    }
}