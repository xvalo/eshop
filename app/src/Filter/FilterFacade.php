<?php

namespace App\Filter;

use App\Category\Entity\Category;
use App\Filter\Entity\CategoryFilter;
use App\Filter\Entity\Filter;
use App\Filter\Entity\Value;
use App\Filter\Entity\VariantFilterValue;
use App\Product\Variant;
use Doctrine\ORM\Query\Expr\Join;
use Kdyby\Doctrine\EntityManager;
use Nette\SmartObject;

class FilterFacade
{
	use SmartObject;

	/** @var EntityManager */
	private $em;

	/** @var \Kdyby\Doctrine\EntityRepository */
	private $filterRepository;

	/** @var \Kdyby\Doctrine\EntityRepository */
	private $valueRepository;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
		$this->filterRepository = $em->getRepository(Filter::class);
		$this->valueRepository = $em->getRepository(Value::class);
	}

	/**
	 * @param int $id
	 * @return null|Filter
	 */
	public function getFilter(int $id)
	{
		return  $this->filterRepository->find($id);
	}

	/**
	 * @param int $id
	 * @return null|Value
	 */
	public function getValue(int $id)
	{
		return $this->valueRepository->find($id);
	}

	/**
	 * @param Filter $filter
	 * @param Variant $variant
	 * @return VariantFilterValue|null
	 */
	public function getVariantFilterValue(Filter $filter, Variant $variant)
	{
		return $this->em->getRepository(VariantFilterValue::class)->findOneBy(['filter' => $filter, 'productVariant' => $variant]);
	}

	public function saveFilter(Filter $filter)
	{
		if ($filter->getId() === null) {
			$this->em->persist($filter);
		}

		$this->em->flush();
	}

	public function saveFilterValue(Value $value)
	{
		if ($value->getId() === null) {
			$this->em->persist($value);
		}

		$this->em->flush();
	}

	public function saveVariantValue(VariantFilterValue $value)
	{
		if ($value->getId() === null) {
			$this->em->persist($value);
		}

		$this->em->flush();
	}

	public function getGridSource()
	{
		return $this->filterRepository->createQueryBuilder('F')
				->where('F.deleted = 0');
	}

	public function getFilterValuesGridSource(Filter $filter)
	{
		$qb = $this->valueRepository->createQueryBuilder('V');
		$qb->where(
			$qb->expr()->andX(
				$qb->expr()->eq('V.filter', ':filter'),
				$qb->expr()->eq('V.deleted', ':deleted')
			)
		)->orderBy('V.sequence');

		$qb->setParameters([
			'filter' => $filter,
			'deleted' => false
		]);

		return $qb;
	}

	public function getFiltersList()
	{
		return $this->filterRepository->findPairs(['deleted' => false], 'name');
	}

	public function saveCategoryFilter(CategoryFilter $categoryFilter)
	{
		if ($categoryFilter->getId() === null) {
			$this->em->persist($categoryFilter);
		}

		$this->em->flush();
	}

	public function removeAllFiltersFromCategory(Category $category)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->delete(CategoryFilter::class, 'CF')
			->where($qb->expr()->eq('CF.category', ':category'))
			->setParameter('category', $category);

		return $qb->getQuery()->execute();
	}

	public function getCategoryFilters(Category $category)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('F')
			->from(Filter::class, 'F')
			->join(CategoryFilter::class, 'CF', Join::WITH, 'CF.filter = F')
			->where($qb->expr()->eq('CF.category', ':category'))
			->setParameter('category', $category);

		return $qb->getQuery()->execute();
	}

	public function getAvailableFiltersForCategories(array $categoriesIds)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('F.id, F.name', 'F.title')
			->from(Filter::class, 'F')
			->join(CategoryFilter::class, 'CF', Join::WITH, 'CF.filter = F')
			->where(
				$qb->expr()->andX(
					$qb->expr()->in('F.deleted', ':deleted'),
					$qb->expr()->in('CF.category', ':categories')
				)
			)
			->groupBy('F.id')
			->setParameters([
				'deleted' => false,
				'categories' => $categoriesIds
			]);

		return $qb->getQuery()->execute();
	}

	public function getVariantValues(Variant $variant)
	{
		return $this->em->getRepository(VariantFilterValue::class)->findBy(['productVariant' => $variant]);
	}

	public function getItemsToMoveUp(Value $value, Value $previousItem = null)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('V')
			->from(Value::class, 'V')
			->where(
				$qb->expr()->andX(
					$qb->expr()->lte('V.sequence', ':lower'),
					$qb->expr()->gt('V.sequence', ':higher'),
					$qb->expr()->eq('V.filter', ':filter')
				)
			);

		$qb->setParameters([
			'lower' => $previousItem ? $previousItem->getSequence() : 0,
			'higher' => $value->getSequence(),
			'filter' => $value->getFilter()
		]);

		return $qb->getQuery()->getResult();
	}

	public function getItemsToMoveDown(Value $value, Value $nextItem = null)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('V')
			->from(Value::class, 'V')
			->where(
				$qb->expr()->andX(
					$qb->expr()->gte('V.sequence', ':higher'),
					$qb->expr()->lt('V.sequence', ':lower'),
					$qb->expr()->eq('V.filter', ':filter')
				)
			);

		$qb->setParameters([
			'higher' => $nextItem ? $nextItem->getSequence() : 0,
			'lower' => $value->getSequence(),
			'filter' => $value->getFilter()
		]);

		return $qb->getQuery()->getResult();
	}

	/**
	 * @param Filter $filter
	 * @return null|int
	 * @throws \Doctrine\ORM\NoResultException
	 * @throws \Doctrine\ORM\NonUniqueResultException
	 */
	public function getValueMaxSequenceNumber(Filter $filter)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('MAX(V.sequence)')
			->from(Value::class, 'V')
			->where($qb->expr()->eq('V.filter', ':filter'))
			->setParameter('filter', $filter);

		return $qb->getQuery()->getSingleScalarResult();
	}

	public function getFilterValues(Filter $filter)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('V')
			->from(Value::class, 'V')
			->where(
				$qb->expr()->andX(
					$qb->expr()->eq('V.filter', ':filter'),
					$qb->expr()->eq('V.deleted', ':deleted')
				)
			)
			->orderBy('V.sequence', 'ASC');

		$qb->setParameters([
			'filter' => $filter,
			'deleted' => false
		]);

		return $qb->getQuery()->getResult();
	}

	public function removeFilterValuesFromProducts(Filter $filter)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->delete(VariantFilterValue::class, 'VFV')
			->where(
				$qb->expr()->eq('VFV.filter', ':filter')
			)
			->setParameter('filter', $filter);

		return $qb->getQuery()->execute();
	}

	public function removeFilterFromCategories(Filter $filter)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->delete(CategoryFilter::class, 'CF')
			->where(
				$qb->expr()->eq('CF.filter', ':filter')
			)
			->setParameter('filter', $filter);

		return $qb->getQuery()->execute();
	}

	public function removeValueFromProducts(Value $value)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->delete(VariantFilterValue::class, 'VFV')
			->where(
				$qb->expr()->eq('VFV.filterValue', ':value')
			)
			->setParameter('value', $value);

		return $qb->getQuery()->execute();
	}

	public function getFiltersIdsByValues(array $valuesIds)
	{
		$qb = $this->em->createQueryBuilder();
		$qb->select('IDENTITY(V.filter) AS filterId, V.id AS valueId')
			->from(Value::class, 'V')
			->where(
				$qb->expr()->in('V', ':values')
			);

		$qb->setParameter('values', $valuesIds);

		return $qb->getQuery()->getArrayResult();
	}
}