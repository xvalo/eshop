<?php

namespace App\Filter\Entity;

use App\Model\BaseEntity;
use App\Traits\ActivityTrait;
use App\Traits\RemovableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Filter extends BaseEntity
{
	use ActivityTrait;
	use RemovableTrait;
	
	/**
	 * @ORM\Column(type="string", length=50)
	 * @var string
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=50)
	 * @var string
	 */
	private $title;

	/**
	 * @ORM\Column(type="string", length=200, nullable=true)
	 * @var string
	 */
	private $description;

	/**
	 * @ORM\OneToMany(targetEntity="App\Filter\Entity\Value", mappedBy="filter", cascade={"persist"})
	 * @var Value[]
	 */
	private $values;

	public function __construct(string $name, string $title, string $description = null)
	{
		$this->name = $name;
		$this->values = new ArrayCollection();
		$this->title = $title;
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getValues()
	{
		return $this->values;
	}

	/**
	 * @param string $value
	 * @param int $sequence
	 */
	public function addValue(string $value, int $sequence)
	{
		$this->values->add(new Value($value, $sequence, $this));
	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle(string $title)
	{
		$this->title = $title;
	}

	/**
	 * @return string|null
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $description
	 */
	public function setDescription(string $description)
	{
		$this->description = $description;
	}
}