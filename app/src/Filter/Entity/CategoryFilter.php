<?php

namespace App\Filter\Entity;

use App\Category\Entity\Category;
use App\Model\BaseEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class CategoryFilter extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="App\Filter\Entity\Filter")
	 * @ORM\JoinColumn(name="filter_id", referencedColumnName="id")
	 * @var Filter
	 */
	private $filter;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Category\Entity\Category")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
	 * @var Category
	 */
	private $category;

	public function __construct(Filter $filter, Category $category)
	{
		$this->filter = $filter;
		$this->category = $category;
	}

	/**
	 * @return Filter
	 */
	public function getFilter(): Filter
	{
		return $this->filter;
	}

	/**
	 * @return Category
	 */
	public function getCategory(): Category
	{
		return $this->category;
	}
}