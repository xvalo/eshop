<?php


namespace App\Filter\Entity;

use App\Model\BaseEntity;
use App\Traits\RemovableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Value extends BaseEntity
{
	use RemovableTrait;

	/**
	 * @ORM\Column(type="string", length=50)
	 * @var string
	 */
	private $value;

	/**
	 * @ORM\Column(type="integer")
	 * @var int
	 */
	private $sequence;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Filter\Entity\Filter", inversedBy="values")
	 * @ORM\JoinColumn(name="filter_id", referencedColumnName="id")
	 * @var Filter
	 */
	private $filter;

	public function __construct(string $value, int $sequence, Filter $filter)
	{
		$this->value = $value;
		$this->sequence = $sequence;
		$this->filter = $filter;
	}

	/**
	 * @return string
	 */
	public function getValue(): string
	{
		return $this->value;
	}

	/**
	 * @param string $value
	 */
	public function setValue(string $value)
	{
		$this->value = $value;
	}

	/**
	 * @return int
	 */
	public function getSequence(): int
	{
		return $this->sequence;
	}

	/**
	 * @param int $sequence
	 */
	public function setSequence(int $sequence)
	{
		$this->sequence = $sequence;
	}

	/**
	 * @return Filter
	 */
	public function getFilter(): Filter
	{
		return $this->filter;
	}
}