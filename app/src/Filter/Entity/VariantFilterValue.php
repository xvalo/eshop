<?php


namespace App\Filter\Entity;

use App\Model\BaseEntity;
use App\Product\Variant;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class VariantFilterValue extends BaseEntity
{
	/**
	 * @ORM\ManyToOne(targetEntity="App\Product\Variant")
	 * @ORM\JoinColumn(name="product_variant_id", referencedColumnName="id")
	 * @var Variant
	 */
	private $productVariant;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Filter\Entity\Filter")
	 * @ORM\JoinColumn(name="filter_id", referencedColumnName="id")
	 * @var Filter
	 */
	private $filter;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Filter\Entity\Value")
	 * @ORM\JoinColumn(name="filter_value_id", referencedColumnName="id")
	 * @var Value
	 */
	private $filterValue;

	public function __construct(Variant $variant, Filter $filter)
	{
		$this->filter = $filter;
		$this->productVariant = $variant;
	}

	/**
	 * @param Value $filterValue
	 */
	public function setFilterValue(Value $filterValue)
	{
		$this->filterValue = $filterValue;
	}

	/**
	 * @return Variant
	 */
	public function getProductVariant(): Variant
	{
		return $this->productVariant;
	}

	/**
	 * @return Filter
	 */
	public function getFilter(): Filter
	{
		return $this->filter;
	}

	/**
	 * @return Value
	 */
	public function getFilterValue(): Value
	{
		return $this->filterValue;
	}
}