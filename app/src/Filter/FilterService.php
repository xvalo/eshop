<?php

namespace App\Filter;

use App\Category\CategoryFacade;
use App\Category\Entity\Category;
use App\Filter\Entity\CategoryFilter;
use App\Filter\Entity\Filter;
use App\Filter\Entity\Value;
use App\Filter\Entity\VariantFilterValue;
use App\Product\Variant;
use Nette\Application\BadRequestException;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\SmartObject;

class FilterService
{
	use SmartObject;

	const CATEGORY_FILTERS_CACHE_NAME = 'category-filters';

	/** @var FilterFacade */
	private $filterFacade;

	/** @var CategoryFacade */
	private $categoryFacade;

	/** @var Cache */
	private $cache;

	public function __construct(
		FilterFacade $filterFacade,
		CategoryFacade $categoryFacade,
		IStorage $storage
	){
		$this->filterFacade = $filterFacade;
		$this->categoryFacade = $categoryFacade;
		$this->cache = new Cache($storage, self::CATEGORY_FILTERS_CACHE_NAME);

	}

	/**
	 * @param int $id
	 * @return Filter|null
	 */
	public function getFilter(int $id)
	{
		return $this->filterFacade->getFilter($id);
	}

	/**
	 * @param int $id
	 * @return Value|null
	 */
	public function getFilterValue(int $id)
	{
		return $this->filterFacade->getValue($id);
	}

	/**
	 * @return array
	 */
	public function getFiltersList()
	{
		return $this->filterFacade->getFiltersList();
	}

	/**
	 * @param array $filters
	 * @param Category $category
	 */
	public function setFiltersToCategory(array $filters, Category $category)
	{
		$categoriesIds = $this->categoryFacade->getCategoryTreeChildrenIds($category);
		$categoriesIds[] = $category->getId();

		foreach ($categoriesIds as $categoryId) {
			$category = $this->categoryFacade->getCategory((int) $categoryId);

			$this->filterFacade->removeAllFiltersFromCategory($category);

			foreach ($filters as $filterId) {
				$filter = $this->getFilter((int)$filterId);
				$this->filterFacade->saveCategoryFilter(new CategoryFilter($filter, $category));
			}
		}

		$this->cache->clean([Cache::TAGS => ['filters']]);
	}

	/**
	 * @param Category $category
	 * @return mixed
	 */
	public function getCategoryFilters(Category $category)
	{
		return $this->filterFacade->getCategoryFilters($category);
	}

	public function getCategoryFiltersIds(Category $category)
	{
		$ids = [];

		/** @var Filter $filter */
		foreach ($this->getCategoryFilters($category) as $filter) {
			$ids[] = $filter->getId();
		}

		return $ids;
	}

	public function setFilterValuesToProductVariant(array $filtersValues, Variant $variant)
	{
		foreach ($filtersValues as $filterId => $valueId) {
			if ($valueId === null) {
				continue;
			}
			$filter = $this->filterFacade->getFilter((int) $filterId);
			$value = $this->filterFacade->getValue((int) $valueId);

			$variantFilterValue = $this->filterFacade->getVariantFilterValue($filter, $variant) ?: new VariantFilterValue($variant, $filter);
			$variantFilterValue->setFilterValue($value);
			$this->filterFacade->saveVariantValue($variantFilterValue);
		}

		$this->cache->clean([Cache::TAGS => ['values']]);
	}

	public function getVariantFiltersDefaultValues(Variant $variant, array $availableFilterIds)
	{
		$filterValues = $this->filterFacade->getVariantValues($variant);
		$defaults = [];

		/** @var VariantFilterValue $value */
		foreach ($filterValues as $value) {
			if (!in_array($value->getFilter()->getId(), $availableFilterIds)) {
				continue;
			}
			$defaults[$value->getFilter()->getId()] = $value->getFilterValue()->getId();
		}

		return $defaults;
	}

	public function getAvailableFilters(Category $category)
	{
		$cacheKey = 'category-filters-list-'.$category->getId();
		$filtersList = $this->cache->load($cacheKey);

		if ($filtersList === null) {
			$categoriesIds = $this->categoryFacade->getCategoryTreeChildrenIds($category);
			$categoriesIds[] = $category->getId();

			$filtersList = [];

			foreach($this->filterFacade->getAvailableFiltersForCategories($categoriesIds) as $filterData) {
				$filtersList[$filterData['id']] = isset($filterData['title']) && strlen($filterData['title']) > 0 ? $filterData['title'] : $filterData['name'];
			}
			$this->cache->save($cacheKey, $filtersList, [ Cache::TAGS => ['filters'] ]);
		}

		return $filtersList;
	}

	public function getAvailableFiltersValues(Category $category)
	{
		$cacheKey = 'category-filters-values-'.$category->getId();
		$values = $this->cache->load($cacheKey);

		if ($values === null) {
			$categoriesIds = $this->categoryFacade->getCategoryTreeChildrenIds($category);
			$categoriesIds[] = $category->getId();
			$values = [];

			$filtersIds = array_column($this->filterFacade->getAvailableFiltersForCategories($categoriesIds), 'id');

			foreach ($filtersIds as $id) {
				$filter = $this->filterFacade->getFilter((int)$id);
				/** @var Value $value */
				foreach ($this->filterFacade->getFilterValues($filter) as $value) {
					$values[$id][$value->getId()] = $value->getValue();
				}
			}

			$this->cache->save($cacheKey, $values, [ Cache::TAGS => ['values'] ]);
		}

		return $values;
	}

	public function getMinMaxPrice(Category $category)
	{
		$categoryMinMaxPrice = $this->categoryFacade->getCategoryMinMaxPrice($category);

		return [
			'min' => (int) $categoryMinMaxPrice['minPrice'],
			'max' => (int) $categoryMinMaxPrice['maxPrice']
		];
	}

	/**
	 * @param Filter $filter
	 */
	public function removeFilter(Filter $filter)
	{
		$this->filterFacade->removeFilterValuesFromProducts($filter);
		$this->filterFacade->removeFilterFromCategories($filter);

		$filter->delete();
		/** @var Value $value */
		foreach ($filter->getValues() as $value) {
			$value->delete();
		}
		$this->filterFacade->saveFilter($filter);

		$this->cache->clean([Cache::TAGS => ['filters', 'values']]);
	}

	public function removeFilterValue(Filter $filter, Value $value)
	{
		if ($value->getFilter()->getId() !== $filter->getId()) {
			throw new BadRequestException();
		}

		$this->filterFacade->removeValueFromProducts($value);
		$value->delete();
		$this->filterFacade->saveFilterValue($value);

		$this->cache->clean([Cache::TAGS => ['filters', 'values']]);
	}

	public function changeFilterValuesOrder(Value $item, Value $previousItem = null, Value $nextItem = null)
	{
		$itemsToMoveUp = $this->filterFacade->getItemsToMoveUp($item, $previousItem);

		/** @var Value $value */
		foreach ($itemsToMoveUp as $value) {
			$value->setSequence($value->getSequence() - 1);
		}

		$itemsToMoveDown = $this->filterFacade->getItemsToMoveDown($item, $nextItem);

		/** @var Value $value */
		foreach ($itemsToMoveDown as $value) {
			$value->setSequence($value->getSequence() + 1);
		}

		if ($previousItem) {
			$item->setSequence($previousItem->getSequence() + 1);
		} else if ($nextItem) {
			$item->setSequence($nextItem->getSequence() - 1);
		} else {
			$item->setSequence(1);
		}

		$this->filterFacade->saveFilterValue($item);
		$this->cache->clean([Cache::TAGS => ['filters', 'values']]);
	}

	/**
	 * @param array $filteredValuesIds
	 * @return array
	 */
	public function getFiltersIdsForFiltering(array $filteredValuesIds): array
	{
		$filtersIdsWithValues = $this->filterFacade->getFiltersIdsByValues($filteredValuesIds);
		$filters = [];

		foreach($filtersIdsWithValues as $filterData) {
			$filters[(int)$filterData['filterId']][] = (int)$filterData['valueId'];
		}

		return $filters;
	}
}