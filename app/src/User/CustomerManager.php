<?php

namespace App\User;

use App\Customer\Customer;
use App\Customer\CustomerRepository;
use Nette\Security\Passwords;
use Nette;
use Nette\Security\User;

class CustomerManager
{
	use Nette\SmartObject;

	const USER_NAMESPACE = 'FRONT';

	const    COLUMN_ID = 'id',
			COLUMN_LOGIN = 'login',
			COLUMN_PASSWORD_HASH = 'password';

	/**
	 * @var CustomerRepository
	 */
	private $repository;

	/**
	 * @var User
	 */
	private $user;

	/**
	 * UserManager constructor.
	 * @param User $user
	 * @param CustomerRepository $repository
	 */
	public function __construct(User $user, CustomerRepository $repository)
	{
		$this->user = $user;
		$this->repository = $repository;
	}


	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($email, $password) = $credentials;

		/** @var Customer $user */
		$user = $this->repository->getCustomerByEmail($email);

		if (!$user) {
			throw new Nette\Security\AuthenticationException('Zadejte prosím správné přihlašovací údaje.');
		} elseif (!$user->isActive()) {
			throw new Nette\Security\AuthenticationException('E-mailová adresa nebyla ještě potvrzena.');
		} elseif (strlen($user->getPassword()) == 32) {
			if (!$this->verifyOldPassword($password, $user->getPassword())) {
				throw new Nette\Security\AuthenticationException('Zadejte prosím správné přihlašovací údaje.');
			}
			$user->setPassword(Passwords::hash($password));
			$this->repository->persist($user);
		} elseif (!Passwords::verify($password, $user->getPassword())) {
			$user->setPassword(Passwords::hash($password));
			$this->repository->persist($user);
			throw new Nette\Security\AuthenticationException('Zadejte prosím správné přihlašovací údaje.');
		} elseif (Passwords::needsRehash($user->getPassword())) {
			$user->setPassword(Passwords::hash($password));
			$this->repository->persist($user);
		}

		$this->repository->flush();

		$data = [
			'name' => $user->getName(),
			'lastName' => $user->getLastName()
		];

		$this->user->getStorage()->setNamespace(self::USER_NAMESPACE);
		$this->user->getStorage()->setIdentity(new Nette\Security\Identity($user->getId(), null, $data));
		$this->user->getStorage()->setAuthenticated(true);
	}

	private function verifyOldPassword($password, $hash)
	{
		return $hash === md5($password."ezc");
	}
}

