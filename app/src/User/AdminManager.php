<?php

namespace App\User;

use App\Admin\Admin;
use App\Admin\AdminRepository;
use App\Admin\Privilege;
use App\Classes\Admin\Role;
use Nette\Security\Passwords;
use Nette;
use Nette\Security\User;

class AdminManager
{
	use Nette\SmartObject;

	const USER_NAMESPACE = 'ADMIN';

	/** @var User */
	private $user;

	/** @var AdminRepository */
	private $repository;

	/**
	 * UserManager constructor.
	 * @param User $user
	 * @param AdminRepository $repository
	 */
	public function __construct(User $user, AdminRepository $repository)
	{
		$this->user = $user;
		$this->repository = $repository;
	}


	/**
	 * Performs an authentication.
	 * @param array $credentials
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;

		/** @var Admin $row */
		$row = $this->repository->getUserByLogin($username);

		if (!$row) {
			throw new Nette\Security\AuthenticationException('The username is incorrect.');

		}
		elseif (strlen($row->getPassword()) == 32)
		{
			if(!$this->verifyOldPassword($password, $row->getPassword())){
				throw new Nette\Security\AuthenticationException('The password is incorrect.');
			}

			$row->setPasswordHash(Passwords::hash($password));
			$this->repository->persist($row);
		}
		elseif (!Passwords::verify($password, $row->getPassword()))
		{
			throw new Nette\Security\AuthenticationException('The password is incorrect.');
		}
		elseif (Passwords::needsRehash($row->getPassword()))
		{
			$row->setPasswordHash(Passwords::hash($password));
			$this->repository->persist($row);
		}
		$this->repository->flush();
		$row->unsetPassword();

		$data = [
			'id' => $row->getId(),
			'login' => $row->getLogin(),
			'name' => $row->getName(),
			'lastName' => $row->getLastName(),
			'privileges' => []
		];

		if (!$row->isSuperAdmin()) {
			$roles = [Role::ADMIN];

			/** @var Privilege $privilege */
			foreach ($row->getPrivileges() as $privilege) {
				$data['privileges'][] = $privilege->getCode();
			}
		} else {
			$roles = [Role::SUPER_ADMIN];
		}

		$this->user->getStorage()->setNamespace(self::USER_NAMESPACE);
		$this->user->getStorage()->setIdentity(new Nette\Security\Identity($row->getId(), $roles, $data));
		$this->user->getStorage()->setAuthenticated(TRUE);
	}

}
