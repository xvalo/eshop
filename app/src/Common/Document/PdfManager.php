<?php

namespace App\Common\Document;

use App\InvalidStateException;
use Joseki\Application\Responses\PdfResponse;
use Latte\Engine;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\SmartObject;

class PdfManager
{
	use SmartObject;

	/** @var string */
	private $pdfDirectory;

    /** @var Template */
    private $template;

	/**
	 * PdfManager constructor.
	 * @param string $pdfDirectory
	 */
    public function __construct(string $pdfDirectory)
    {
        $latte = new Engine();
        $this->template = new Template($latte);
        $this->template->addFilter(null, '\App\Utils\Filters::common');
	    $this->pdfDirectory = $pdfDirectory;
    }

	/**
	 * @param string $filePath
	 */
    public function setTemplate(string $filePath)
    {
        $this->template->setFile($filePath);
    }

	/**
	 * @param string $name
	 * @param $data
	 */
    public function setValue(string $name, $data)
    {
        $this->template->$name = $data;
    }

	/**
	 * @param string $directory
	 * @param string|null $fileName
	 * @return string
	 * @throws InvalidStateException
	 */
    public function savePdf(string $directory, string $fileName = null): string
    {
    	if (is_null($this->template->getFile())) {
		    throw new InvalidStateException('PDF Manager is missing template.');
	    }

        $this->template->basePath = WWW_DIR;
        $pdf = new PdfResponse($this->template);
        return $pdf->save($directory, $fileName);
    }

	/**
	 * @param string $path
	 */
	public function setDirectory(string $path)
	{
		$this->pdfDirectory = $path;
    }

	/**
	 * @return string
	 */
    public function getDirectory(): string
    {
        return $this->pdfDirectory;
    }
}