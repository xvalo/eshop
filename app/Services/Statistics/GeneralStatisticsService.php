<?php

namespace App\Services\Statistics;


use App\Category\CategoryFacade;
use App\Customer\CustomerRepository;
use App\Product\ProductRepository;

class GeneralStatisticsService
{
	/** @var ProductRepository */
	private $productRepository;

	/** @var CustomerRepository */
	private $customerRepository;

	/** @var CategoryFacade */
	private $categoryFacade;

	public function __construct(
		ProductRepository $productRepository,
		CategoryFacade $categoryFacade,
		CustomerRepository $customerRepository
	)
	{
		$this->productRepository = $productRepository;
		$this->customerRepository = $customerRepository;
		$this->categoryFacade = $categoryFacade;
	}

	public function getProductsCount()
	{
		return $this->productRepository->getProductsCount();
	}

	public function getCategoriesCount()
	{
		return $this->categoryFacade->getCategoriesCount();
	}

	public function getRegisteredCustomersCount()
	{
		return $this->customerRepository->getRegisteredCustomersCount();
	}

	public function getCustomersCount()
	{
		return $this->customerRepository->getCustomersCount();
	}
}