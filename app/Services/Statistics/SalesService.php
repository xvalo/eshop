<?php

namespace App\Services\Statistics;

use App\Classes\Order\OrderSalesStatisticsPeriod;
use App\Classes\Order\OrderSource;
use App\Order\OrderRepository;

class SalesService
{

	/** @var OrderRepository */
	private $orderRepository;

	public function __construct(
		OrderRepository $orderRepository
	)
	{
		$this->orderRepository = $orderRepository;
	}


	public function getMonthEshopSales()
	{
		return $this->processData($this->orderRepository->getOrdersSales(OrderSource::ESHOP, OrderSalesStatisticsPeriod::MONTH));
	}

	public function getMonthStoreSales()
	{
		return $this->processData($this->orderRepository->getOrdersSales(OrderSource::STORE, OrderSalesStatisticsPeriod::MONTH));
	}

	public function getQuarterEshopSales()
	{
		return $this->processData($this->orderRepository->getOrdersSales(OrderSource::ESHOP, OrderSalesStatisticsPeriod::QUARTER));
	}

	public function getQuarterStoreSales()
	{
		return $this->processData($this->orderRepository->getOrdersSales(OrderSource::STORE, OrderSalesStatisticsPeriod::QUARTER));
	}

	public function getYearEshopSales()
	{
		return $this->processData($this->orderRepository->getOrdersSales(OrderSource::ESHOP, OrderSalesStatisticsPeriod::YEAR));
	}

	public function getYearStoreSales()
	{
		return $this->processData($this->orderRepository->getOrdersSales(OrderSource::STORE, OrderSalesStatisticsPeriod::YEAR));
	}

	private function processData($data)
	{
		$result = [];

		foreach ($data as $item) {
			$result[] = [
				'period' => $item['period'],
				'amount' => $item['orders']
			];
		}

		return $result;
	}
}