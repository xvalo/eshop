<?php

namespace App\Services\SiteMap;

use App\Category\CategoryFacade;
use App\Category\Entity\Category;
use App\Core\Application\Settings\ApplicationSettingsManager;
use App\Lists\ProducerRepository;
use App\Page\Entity\Page;
use App\Page\PageFacade;
use App\Product\Product;
use App\Product\ProductRepository;
use Nette\Application\LinkGenerator;
use Nette\SmartObject;
use Nette\Utils\Strings;

class SiteMapService
{
	use SmartObject;

	/** @var ProducerRepository */
	private $producerRepository;

	/** @var \Sitemap  */
	private $siteMapGenerator;

	/** @var ProductRepository */
	private $productRepository;

	/** @var CategoryFacade */
	private $categoryFacade;

	/** @var PageFacade  */
	private $pageFacade;

	/** @var ApplicationSettingsManager */
	private $applicationSettingsManager;
	/**
	 * @var LinkGenerator
	 */
	private $linkGenerator;

	public function __construct(
		ProductRepository $productRepository,
		ProducerRepository $producerRepository,
		CategoryFacade $categoryFacade,
		PageFacade $pageFacade,
		ApplicationSettingsManager $applicationSettingsManager,
		LinkGenerator $linkGenerator
	){
		$this->producerRepository = $producerRepository;
		$this->productRepository = $productRepository;
		$this->categoryFacade = $categoryFacade;
		$this->pageFacade = $pageFacade;
		$this->applicationSettingsManager = $applicationSettingsManager;
		$this->linkGenerator = $linkGenerator;
	}

	private function init()
	{
		//$domain = $this->applicationSettingsManager->getDomain();
		//$domain = Strings::startsWith($domain, 'https://') ? $domain : 'https://'.$domain;
		$this->siteMapGenerator = new \Sitemap('');
	}

	public function generateSiteMap()
	{
		$this->init();
		$this->removeSiteMapFile();
		$this->setStaticUrls();
		$this->setPagesUrls();
		$this->setCategoriesUrls();
		$this->setProductsUrls();
		$this->siteMapGenerator->endSitemap();
	}

	private function setStaticUrls()
	{
		$this->siteMapGenerator->addItem($this->linkGenerator->link('Front:Homepage:default'), 1, 'weekly');
		$this->siteMapGenerator->addItem($this->linkGenerator->link('Front:Homepage:contact'));
		$this->siteMapGenerator->addItem($this->linkGenerator->link('Front:Event:default'));
		$this->siteMapGenerator->addItem($this->linkGenerator->link('Front:Producer:default'));
		$this->siteMapGenerator->addItem($this->linkGenerator->link('Front:NewProducts:default'));
		
	}

	private function setPagesUrls()
	{
		/** @var Page $page */
		foreach ($this->pageFacade->getActivePages() as $page) {
			$this->siteMapGenerator->addItem($this->linkGenerator->link('Front:Page:default', ['page' => $page]), 0.6, 'monthly');
		}
	}

	private function setCategoriesUrls()
	{
		/** @var Category $category */
		foreach ($this->categoryFacade->getCategoriesList() as $category) {
			$this->siteMapGenerator->addItem(
				$this->linkGenerator->link('Front:Category:default', ['category' => $category['url']]),
				0.9,
				'weekly'
			);
		}
	}

	private function setProductsUrls()
	{
		/** @var Product $product */
		foreach ($this->productRepository->findBy(['active' => 1, 'deleted' => 0]) as $product) {
			$this->siteMapGenerator->addItem(
				$this->linkGenerator->link('Front:Product:default', ['category' => $product->getCategory()->getUrl(), 'url' => $product->getUrl()]),
				0.8,
				'weekly'
			);
		}
	}

	private function removeSiteMapFile()
	{
		if (file_exists(WWW_DIR.'/sitemap.xml')) {
			unlink(WWW_DIR . '/sitemap.xml');
		}
	}

}
